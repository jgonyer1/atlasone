﻿using System.Collections.Generic;
using System.Xml.Linq;
using DCGWordConvert.Helpers;
using DCGWordConvert.Style;
using DCGWordConvert.Transform;

namespace DCGWordConvert
{
    /// <summary>
    /// A managed representation of a Word document containing DocumentStyles and DocumentElements as well as metadata
    /// </summary>
    public class Document
    {
        #region Fields

        private readonly Manifest _manifest;

        internal Dictionary<string, string> _references = new Dictionary<string, string>();

        #endregion Fields

        #region Constructor

        internal Document(DefinedStylesLookup definedStyles, DocumentElement bodyElement, Manifest manifest)
        {
            DefinedStyles = definedStyles;
            BodyElement = bodyElement;
            _manifest = manifest; // used in the constructor for debugging purposes
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// The styles defined in the document
        /// </summary>
        public DefinedStylesLookup DefinedStyles { get; }

        /// <summary>
        /// The root element of the document
        /// </summary>
        public DocumentElement BodyElement { get; }

        #endregion Properties
    }
}
