﻿namespace DCGWordConvert.Style
{
    /// <summary>
    /// The underline style
    /// </summary>
    public enum UnderlineType : byte
    {
        /// <summary>
        /// No underline
        /// </summary>
        None,

        /// <summary>
        /// A single line
        /// </summary>
        Single,

        /// <summary>
        /// A thick single line
        /// </summary>
        Thick,

        /// <summary>
        /// Two lines
        /// </summary>
        Double,

        /// <summary>
        /// A dashed dotted line
        /// </summary>
        Dotted,

        /// <summary>
        /// A dashed single line
        /// </summary>
        Dashed,

        /// <summary>
        /// A single wavy line
        /// </summary>
        Wave,

        /// <summary>
        /// A thick wavy line
        /// </summary>
        WaveHeavy,

        /// <summary>
        /// Two wavy lines
        /// </summary>
        WaveDouble
    }
}