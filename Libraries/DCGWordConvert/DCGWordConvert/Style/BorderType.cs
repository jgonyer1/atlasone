﻿namespace DCGWordConvert.Style
{
    /// <summary>
    /// The type of an element border
    /// </summary>
    public enum BorderType : byte
    {
        /// <summary>
        /// No border
        /// </summary>
        None,

        /// <summary>
        /// A single, solid line
        /// </summary>
        Single,

        /// <summary>
        /// A double solid line
        /// </summary>
        Double,

        /// <summary>
        /// A single dotted line
        /// </summary>
        Dotted,

        /// <summary>
        /// A single dashed line
        /// </summary>
        Dashed,

        /// <summary>
        /// A set of lines which causes the content to look recessed
        /// </summary>
        Inset,

        /// <summary>
        /// A set of lines which causes the content to look raised
        /// </summary>
        Outset,

        /// <summary>
        /// A single wavy line
        /// </summary>
        Wave,

        /// <summary>
        /// A double wavy line
        /// </summary>
        WaveDouble
    }
}