﻿namespace DCGWordConvert.Style
{
    public enum DocumentStyleProperty : byte
    {
        IsBold,
        IsItalic,
        Underline,
        IsStrikeThrough,

        ListType,
        ListStartAt,

        HorizontalAlignment,
        VerticalAlignment,

        IndentLeft,
        IndentHanging,

        ForegroundColor,
        BackgroundColor,

        FontSize,
        FontFaceName,

        BorderTop,
        BorderBottom,
        BorderLeft,
        BorderRight
    }
}
