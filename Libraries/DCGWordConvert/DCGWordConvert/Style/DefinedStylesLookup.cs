﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

using DCGWordConvert.Helpers;

namespace DCGWordConvert.Style
{
    /// <summary>
    /// A lookup class for all the defined (named) styles in a Word document
    /// </summary>
    public class DefinedStylesLookup : IEnumerable<DocumentStyle>
    {
        #region Fields

        private readonly IDictionary<string, DocumentStyle> _namedStyles = new Dictionary<string, DocumentStyle>();

        #endregion Fields

        #region Constructors and Factories

        internal static DefinedStylesLookup FromXml(XDocument document)
        {
            XElement defaultRunElement = document.XPathSelectElement("/w:styles/w:docDefaults/w:rPrDefault/w:rPr", Namespaces.Resolver);
            XElement defaultParagraphElement = document.XPathSelectElement("/w:styles/w:docDefaults/w:pPrDefault/w:pPr", Namespaces.Resolver);

            DocumentStyle defaultStyle = DocumentStyle.Merge(DocumentStyle.FromXml(defaultRunElement), DocumentStyle.FromXml(defaultParagraphElement));

            IEnumerable<XElement> namedStyleElements = document.XPathSelectElements("/w:styles/w:style[@w:styleId]", Namespaces.Resolver);
            Dictionary<string, DocumentStyle> namedStyles = new Dictionary<string, DocumentStyle>();

            foreach (XElement element in namedStyleElements)
            {
                string name = element.Attribute(Namespaces.W.styleId).Value;
                string basedOnStyleName = element.Element(Namespaces.W.basedOn)?.Attribute(Namespaces.W.val)?.Value;

                DocumentStyle namedStyle = DocumentStyle.Merge(
                    name, basedOnStyleName,
                    DocumentStyle.FromXml(element.Element(Namespaces.W.rPr)),
                    DocumentStyle.FromXml(element.Element(Namespaces.W.numPr)),
                    DocumentStyle.FromXml(element.Element(Namespaces.W.pPr)),
                    DocumentStyle.FromXml(element.Element(Namespaces.W.tblPr))
                );

                namedStyles.Add(name, namedStyle);
            }

            return new DefinedStylesLookup(defaultStyle, namedStyles);
        }

        private DefinedStylesLookup(DocumentStyle defaultStyle, IDictionary<string, DocumentStyle> namedStyles)
        {
            _namedStyles = namedStyles;
            Default = defaultStyle;
        }

        #endregion Constructors and Factories

        #region Properties and Getters

        /// <summary>
        /// The default style defined in a Word document
        /// </summary>
        public DocumentStyle Default { get; }

        /// <summary>
        /// Tries to get a defined style by name
        /// </summary>
        /// <param name="name">The name of the defined style to get</param>
        /// <param name="style">The outputted style if one exists, null otherwise</param>
        /// <returns>True if a defined style with the given name exists, false otherwise</returns>
        public bool TryGetStyle(string name, out DocumentStyle style)
        {
            if(name is null)
            {
                style = null;
                return false;
            }

            if (!_namedStyles.TryGetValue(name, out style))
                return false;

            return true;
        }

        /// <summary>
        /// Tries to get 
        /// </summary>
        /// <param name="style"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the name parameter is null</exception>
        public IReadOnlyCollection<DocumentStyle> TryGetCascading(DocumentStyle style)
        {
            if (style is null) throw new ArgumentNullException(nameof(style));

            return TryGetCascading(style.Name);
        }

        /// <summary>
        /// Gets a hierarchy of styles starting with and including the style with the given name
        /// </summary>
        /// <param name="name">The name of the style to start with</param>
        /// <returns>
        /// A collection of styles using the BasedOnName property as a chain, in decendent-first order including the style with the given name
        /// </returns>
        /// <remarks>If the name parameter is null or no defined style exists with that name, an empty collection is returned</remarks>
        public IReadOnlyCollection<DocumentStyle> TryGetCascading(string name)
        {
            List<DocumentStyle> results = new List<DocumentStyle>();

            while(! string.IsNullOrEmpty(name))
            {
                if (!_namedStyles.TryGetValue(name, out DocumentStyle value)) break;
                results.Add(value);

                name = value.BasedOnStyleName;
            }

            return results;
        }

        #endregion Properties and Getters

        #region IEnumerable

        /// <summary>
        /// Gets an enumerator to iterate through all defined styles
        /// </summary>
        /// <returns>An enumerator which iterates through all defined styles</returns>
        public IEnumerator<DocumentStyle> GetEnumerator() => _namedStyles.Values.GetEnumerator();

        /// <summary>
        /// Gets an enumerator to iterate through all defined styles
        /// </summary>
        /// <returns>An enumerator which iterates through all defined styles</returns>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        #endregion IEnumerable
    }
}