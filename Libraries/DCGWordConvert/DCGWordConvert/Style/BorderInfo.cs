﻿using System;
using System.Drawing;
using System.Xml.Linq;

using DCGWordConvert.Helpers;

namespace DCGWordConvert.Style
{
    /// <summary>
    /// Information about a border combining width, type, and color
    /// </summary>
    public struct BorderInfo
    {
        #region Constructors and Factories

        /// <summary>
        /// A convenience property for no border
        /// </summary>
        public static BorderInfo None => new BorderInfo(0, BorderType.None, Color.Transparent);

        internal static bool FromElement(XElement element, out BorderInfo borderInfo)
        {
            if(element == null)
            {
                borderInfo = default(BorderInfo);
                return false;
            }

            int width = int.Parse(element.Attribute(Namespaces.W.sz).Value) / 4;

            string borderTypeName = element.Attribute(Namespaces.W.val).Value;
            BorderType borderType;
            
            switch (borderTypeName)
            {
                case "none":
                case "nil":
                    borderType = BorderType.None;
                    break;

                case "single":
                case "thick":
                    borderType = BorderType.Single;
                    break;

                case "double":
                case "triple":
                case "thickThinLargeGap":
                case "thickThinMediumGap":
                case "thickThinSmallGap":
                case "thinThickLargeGap":
                case "thinThickMediumGap":
                case "thinThickSmallGap":
                case "thinThickThinLargeGap":
                case "thinThickThinMediumGap":
                case "thinThickThinSmallGap":
                    borderType = BorderType.Double;
                    break;

                case "dotted":
                case "dotDash":
                case "dotDotDash":
                    borderType = BorderType.Dotted;
                    break;

                case "dashed":
                case "dashDotStroked":
                case "dashSmallGap":
                    borderType = BorderType.Dashed;
                    break;

                case "inset":
                case "threeDEngrave":
                    borderType = BorderType.Inset;
                    break;

                case "outset":
                case "threeDEmboss":
                    borderType = BorderType.Outset;
                    break;

                case "wave":
                    borderType = BorderType.Wave;
                    break;

                case "doubleWave":
                    borderType = BorderType.WaveDouble;
                    break;

                default:
                    throw new NotSupportedException($"{nameof(BorderType)} of {borderTypeName} is not supported");
            }

            Color color = DocumentStyle.ToColor(element.Attribute(Namespaces.W.color).Value, true);

            borderInfo = new BorderInfo(width, borderType, color);

            return true;
        }

        /// <summary>
        /// Creates a new BorderInfo with the given values
        /// </summary>
        /// <param name="width">The width of the border in pixels</param>
        /// <param name="type">The type of border</param>
        /// <param name="color">The color of the border</param>
        public BorderInfo(int width, BorderType type, Color color)
        {
            Width = width;
            Type = type;
            Color = color;
        }

        #endregion Constructors and Factories

        #region Properties

        /// <summary>
        /// The width of the border in pixels
        /// </summary>
        public int Width { get; }

        /// <summary>
        /// The type of border
        /// </summary>
        public BorderType Type { get; }

        /// <summary>
        /// The color of the border
        /// </summary>
        public Color Color { get; }

        #endregion Properties
    }
}