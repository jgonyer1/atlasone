﻿namespace DCGWordConvert.Style
{
    /// <summary>
    /// The numbering style associated with a list
    /// </summary>
    public enum ListType : byte
    {
        /// <summary>
        /// No numbers or bullets
        /// </summary>
        None,

        /// <summary>
        /// Full circles
        /// </summary>
        BulletFull,

        /// <summary>
        /// Hollow circles
        /// </summary>
        BulletHollow,

        /// <summary>
        /// Arrows
        /// </summary>
        BulletArrow,

        /// <summary>
        /// User-defined
        /// </summary>
        BulletOther,

        /// <summary>
        /// User-defined
        /// </summary>
        IncrementalOther,

        /// <summary>
        /// Incrementing numbers
        /// </summary>
        Number,

        /// <summary>
        /// Uppercase letters
        /// </summary>
        LetterUpper,

        /// <summary>
        /// Lowercase letters
        /// </summary>
        LetterLower,

        /// <summary>
        /// Uppercase Roman numerals
        /// </summary>
        RomanUpper,

        /// <summary>
        /// Lowercase Roman numerals
        /// </summary>
        RomanLower
    }
}
