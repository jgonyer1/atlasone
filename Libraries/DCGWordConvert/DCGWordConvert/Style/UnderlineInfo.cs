﻿using System;
using System.Drawing;
using System.Xml.Linq;

using DCGWordConvert.Helpers;

namespace DCGWordConvert.Style
{
    /// <summary>
    /// Information about a border combining type and color
    /// </summary>
    public struct UnderlineInfo
    {
        #region Constructors and Factories

        /// <summary>
        /// A convenience property for no underline
        /// </summary>
        public static UnderlineInfo None => new UnderlineInfo(UnderlineType.None, Color.Transparent);

        internal static bool FromElement(XElement element, out UnderlineInfo underlineInfo)
        {
            if(element == null)
            {
                underlineInfo = default(UnderlineInfo);
                return false;
            }

            string underlineTypeName = element.Attribute(Namespaces.W.val).Value;
            Color color = DocumentStyle.ToColor(element.Attribute(Namespaces.W.color).Value, true);

            UnderlineType underlineType;
            
            switch (underlineTypeName)
            {
                case "none":
                case "words":
                    underlineType = UnderlineType.None;
                    break;

                case "single":
                case "thick":
                    underlineType = UnderlineType.Single;
                    break;

                case "double":
                    underlineType = UnderlineType.Double;
                    break;

                case "dotted":
                case "dotDash":
                case "dotDotDash":
                case "dottedHeavy":
                    underlineType = UnderlineType.Dotted;
                    break;

                case "dash":
                case "dashDotHeavy":
                case "dashedHeavy":
                case "dashLong":
                case "dashLongHeavy":
                case "dashDotDotHeavy":
                    underlineType = UnderlineType.Dashed;
                    break;

                case "wave":
                case "wavyHeavy":
                    underlineType = UnderlineType.Wave;
                    break;

                case "doubleWave":
                    underlineType = UnderlineType.WaveDouble;
                    break;

                default:
                    throw new NotSupportedException($"{nameof(UnderlineType)} of {underlineTypeName} is not supported");
            }

            underlineInfo = new UnderlineInfo(underlineType, color);

            return true;
        }

        /// <summary>
        /// Creates a new UnderlineInfo with the given values
        /// </summary>
        /// <param name="type">The type of underline</param>
        /// <param name="color">The color of the underline</param>
        public UnderlineInfo(UnderlineType type, Color color)
        {
            Type = type;
            Color = color;
        }

        #endregion Constructors and Factories

        #region Properties

        /// <summary>
        /// The type of underline
        /// </summary>
        public UnderlineType Type { get; }

        /// <summary>
        /// The color of the underline
        /// </summary>
        public Color Color { get; }

        #endregion Properties
    }
}