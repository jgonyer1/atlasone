﻿namespace DCGWordConvert.Style
{
    /// <summary>
    /// The horizontal alignment of text within a container
    /// </summary>
    public enum HorizontalAlignment : byte
    {
        /// <summary>
        /// The text is left aligned within the container
        /// </summary>
        Left,

        /// <summary>
        /// The text is center aligned within the container
        /// </summary>
        Center,

        /// <summary>
        /// The text is right aligned within the container
        /// </summary>
        Right,

        /// <summary>
        /// The text is justified within the container
        /// </summary>
        Justify
    }
}
