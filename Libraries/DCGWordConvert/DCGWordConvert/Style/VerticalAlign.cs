﻿namespace DCGWordConvert.Style
{
    /// <summary>
    /// The vertical alignment of content within a container
    /// </summary>
    public enum VerticalAlignment : byte
    {
        /// <summary>
        /// The content is aligned to the top of the container
        /// </summary>
        Top,

        /// <summary>
        /// The content is aligned to the middle of the container
        /// </summary>
        Middle,

        /// <summary>
        /// The content is aligned to the bottom of the container
        /// </summary>
        Bottom,

        /// <summary>
        /// The content is aligned to the baseline of its parent
        /// </summary>
        Baseline,

        /// <summary>
        /// The content is aligned with the subscript baseline of its parent
        /// </summary>
        Subscript,

        /// <summary>
        /// The content is aligned with the superscript baseline of its parent
        /// </summary>
        Superscript
    }
}
