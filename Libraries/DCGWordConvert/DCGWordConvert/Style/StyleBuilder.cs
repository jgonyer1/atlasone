﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using DCGWordConvert.Transform;

namespace DCGWordConvert.Style
{
    /// <summary>
    /// A predicate which determines which CSS properties and are included in output. 
    /// </summary>
    /// <param name="propertyName">The name of the property to check</param>
    /// <param name="propertyValues">The values associated with the property</param>
    /// <returns>True to include the property in output, false to exclude it</returns>
    public delegate bool CSSPredicate(string propertyName, IReadOnlyCollection<string> propertyValues);

    /// <summary>
    /// A predicate which determines which attributes are applied to an XElement
    /// </summary>
    /// <param name="attributeName">The name of the attribute to check</param>
    /// <param name="existingValue">The value of the existing attribute with the given name, null if no such value exists</param>
    /// <param name="newValue">The value of the new attribute with the given name</param>
    /// <returns>True to apply the attribute, false otherwise</returns>
    public delegate bool AttributePredicate(string attributeName, string existingValue, string newValue);

    /// <summary>
    /// Provides methods for translating DocumentStyles into css styles and attribute values
    /// </summary>
    public class StyleBuilder
    {
        #region Fields

        private readonly Dictionary<string, HashSet<string>> _styling;
        private readonly Dictionary<string, string> _attributes;

        #endregion Fields

        #region Constructor

        internal StyleBuilder(DocumentElement contextElement)
        {
            _styling = new Dictionary<string, HashSet<string>>(StringComparer.InvariantCultureIgnoreCase);
            _attributes = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            ContextElement = contextElement;
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// The element to be styled. Can be null in the case of defined styles
        /// </summary>
        public DocumentElement ContextElement { get; }

        #endregion Properties

        #region Styling

        /// <summary>
        /// Adds styling under the given property name, unioning the values with what already exists
        /// </summary>
        /// <param name="propertyName">The name of the property to place values under</param>
        /// <param name="values">An array of values which are made distinct and unioned with what exists</param>
        /// <returns>True if the styling existed prior to the add, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the propertyName parameter is null, or any of the values are null</exception>
        /// <remarks>
        /// <para>The property name and values are case-insensitive for the purposes of equality</para>
        /// <para>If the values parameter is empty, no changes are made</para>
        /// </remarks>
        public bool AddStyling(string propertyName, params string[] values) => addReplaceStyling(propertyName, false, values);

        /// <summary>
        /// Replaces styling under the given property name, eliminating any values which existed before
        /// </summary>
        /// <param name="propertyName">The name of the property to place values under</param>
        /// <param name="values">An array of values which are made distinct</param>
        /// <returns>True if the styling existed prior to the add, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the propertyName parameter is null, or any of the values are null</exception>
        /// <remarks>
        /// <para>The property name and values are case-insensitive for the purposes of equality</para>
        /// <para>If the values parameter is empty, the property is removed</para>
        /// </remarks>
        public bool ReplaceStyling(string propertyName, params string[] values) => addReplaceStyling(propertyName, true, values);

        private bool addReplaceStyling(string propertyName, bool isReplace, params string[] values)
        {
            if (propertyName is null) throw new ArgumentNullException(nameof(propertyName));
            if(values.Any(v => v is null)) throw new ArgumentNullException(nameof(values));

            bool result;

            if (!_styling.TryGetValue(propertyName, out HashSet<string> existingValues))
            {
                if (!values.Any()) return false;

                existingValues = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
                _styling.Add(propertyName, existingValues);
                result = false;
            }
            else
            {
                result = true;
            }

            if(! values.Any())
            {
                if(isReplace && result)
                {
                    _styling.Remove(propertyName);
                }

                return result;
            }

            if (isReplace) existingValues.Clear();

            existingValues.UnionWith(values);

            return result;
        }

        /// <summary>
        /// Gets the values associated with a given property name
        /// </summary>
        /// <param name="propertyName">The name of the property to get the values for</param>
        /// <returns>A collection of values stored under the property name if it exists, an empty collection otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the property name parameter is null</exception>
        public IReadOnlyCollection<string> GetStyling(string propertyName)
        {
            if (!_styling.TryGetValue(propertyName, out HashSet<string> existingValues))
                return new List<string>();

            return existingValues.ToList();
        }

        /// <summary>
        /// Removes styling under the given property name, eliminating any values which existed before if they intersect with the given values
        /// </summary>
        /// <param name="propertyName">The name of the property to remove values from</param>
        /// <param name="values">An array of values to be removed which are made distinct</param>
        /// <returns>True if the styling existed prior to the add, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the propertyName parameter is null, or any of the values are null</exception>
        /// <remarks>
        /// <para>The property name and values are case-insensitive for the purposes of equality</para>
        /// <para>If the values parameter is empty, the property is removed</para>
        /// </remarks>
        public bool RemoveStyling(string propertyName, params string[] values)
        {
            if (propertyName is null) throw new ArgumentNullException(nameof(propertyName));
            if (values.Any(v => v is null)) throw new ArgumentNullException(nameof(values));

            if (!values.Any()) return _styling.Remove(propertyName);

            if (!_styling.TryGetValue(propertyName, out HashSet<string> existingValues)) return false;

            existingValues.ExceptWith(values);

            if (!existingValues.Any()) _styling.Remove(propertyName);

            return true;
        }

        #endregion Styling

        #region Attributes

        /// <summary>
        /// Adds an attribute to the context element, updating any attribute under the name which might have existed
        /// </summary>
        /// <param name="name">The name of the attribute to add</param>
        /// <param name="value">The value of the attribute to add</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the name or value parameters are null</exception>
        /// <remarks>
        /// <para>Attribute names are case-insensitive for the purposes of equality</para>
        /// <para>If the ContextElement is null, attribute can still be manipulated but won't affect the rendered output</para>
        /// </remarks>
        public void AddAttribute(string name, string value)
        {
            if (name is null) throw new ArgumentNullException(nameof(name));
            if (value is null) throw new ArgumentNullException(nameof(value));

            _attributes[name] = value;
        }

        /// <summary>
        /// Gets an attribute with the given name
        /// </summary>
        /// <param name="name">The name of the attribute to get</param>
        /// <returns>The value of the attribute or null if the attribute doesn't exist</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the name parameter is null</exception>
        /// <remarks>
        /// <para>Attribute names are case-insensitive for the purposes of equality</para>
        /// <para>If the ContextElement is null, attribute can still be manipulated but won't affect the rendered output</para>
        /// </remarks>
        public string GetAttribute(string name)
        {
            if (name is null) throw new ArgumentNullException(nameof(name));

            if (!_attributes.TryGetValue(name, out string value))
                return null;

            return value;
        }

        /// <summary>
        /// Removes an attribute with the given name
        /// </summary>
        /// <param name="name">The name of the attribute to remove</param>
        /// <returns>True if the attribute existed prior to the remove, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the name parameter is null</exception>
        /// <remarks>
        /// <para>Attribute names are case-insensitive for the purposes of equality</para>
        /// <para>If the ContextElement is null, attribute can still be manipulated but won't affect the rendered output</para>
        /// </remarks>
        public bool RemoveAttribute(string name)
        {
            if (name is null) throw new ArgumentNullException(nameof(name));

            return _attributes.Remove(name);
        }

        #endregion Attributes

        #region Helpers

        /// <summary>Returns a CSS representation of the styling</summary>
        /// <param name="propertySeparator">A string value to place between all CSS properties</param>
        /// <param name="predicate">A predicate to test properties against. If the predicate returns false, the property is not included</param>
        /// <returns>A CSS representation of the style</returns>
        public string ToCSS(string propertySeparator = null, CSSPredicate predicate = null)
        {
            propertySeparator = propertySeparator ?? " ";

            StringBuilder sb = new StringBuilder();

            foreach(KeyValuePair<string, HashSet<string>> kvp in _styling)
            {
                if (!ReferenceEquals(null, predicate) && !predicate(kvp.Key, kvp.Value.ToList())) continue;

                sb.Append(kvp.Key);
                sb.Append(": ");
                sb.Append(string.Join(" ", kvp.Value));
                sb.Append(";");
                sb.Append(propertySeparator);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Adds or replaces a (css) style attribute on the element and writes the CSS representation of the given style into it
        /// </summary>
        /// <param name="element">The element to create a style attribute for</param>
        /// <param name="replaceExisting">When true, any existing style attribute is overwritten, when false, the method will return false if a style attribute already exists</param>
        /// <param name="predicate">A predicate to test properties against. If the predicate returns false, the property is not included</param>
        /// <returns>True if the style attribute was added or updated, false otherwise</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the element parameter is null</exception>
        public bool ApplyStyleAttribute(XElement element, bool replaceExisting, CSSPredicate predicate = null)
        {
            if (element is null) throw new ArgumentNullException(nameof(element));

            XName styleAttributeName = "style";

            if (!replaceExisting && !ReferenceEquals(null, element.Attribute(styleAttributeName))) return false;

            string css = ToCSS(" ", predicate);

            if (string.IsNullOrWhiteSpace(css)) return false;

            element.SetAttributeValue(styleAttributeName, css);

            return true;
        }

        /// <summary>
        /// Adds any attributes in the StyleBuilder to an XElement
        /// </summary>
        /// <param name="element">The element to add the attribute(s) to</param>
        /// <param name="replaceExisting">When true, any existing attributes which have the same name as any attributes in the StyleBuilder are replaced</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the element parameter is null</exception>
        public void ApplyAttributes(XElement element, bool replaceExisting)
        {
            if (element is null) throw new ArgumentNullException(nameof(element));

            foreach(KeyValuePair<string, string> kvp in _attributes)
            {
                if (!replaceExisting && !ReferenceEquals(null, element.Attribute(kvp.Key))) continue;

                element.SetAttributeValue(kvp.Key, kvp.Value);
            }
        }

        /// <summary>
        /// Adds any attributes in the StyleBuilder to an XElement
        /// </summary>
        /// <param name="element">The element to add the attribute(s) to</param>
        /// <param name="predicate">A predicate to test attributes against. If the predicate returns true, the attribute is applied, otherwise it isn't</param>
        /// <exception cref="System.ArgumentNullException">Thrown if the element parameter is null</exception>
        public void ApplyAttributes(XElement element, AttributePredicate predicate = null)
        {
            if (element is null) throw new ArgumentNullException(nameof(element));

            foreach(KeyValuePair<string,string> kvp in _attributes)
            {
                string existingValue = element.Attribute(kvp.Key)?.Value;

                if (!predicate(kvp.Key, existingValue, kvp.Value)) continue;

                element.SetAttributeValue(kvp.Key, kvp.Value);
            }
        }

        #endregion Helpers
    }
}