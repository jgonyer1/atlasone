﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Xml.Linq;

using DCGWordConvert.Helpers;

namespace DCGWordConvert.Style
{
    /// <summary>
    /// A dictionary-like object which contains anonymous and named style sets
    /// </summary>
    public class DocumentStyle : IEquatable<DocumentStyle>
    {
        #region Fields

        private static readonly SHA256Managed __sha256;
        private static readonly Regex __hexMatcher;
        private readonly Dictionary<DocumentStyleProperty, object> _stylesByProperty;

        #endregion Fields

        #region Factory

        /// <summary>
        /// Merges two or more styles with the styles defined earlier in the parameters list taking precedence in the case of conflicts
        /// </summary>
        /// <param name="name">An optional name for the style</param>
        /// <param name="basedOnStyleName">The name of the style that the style is based on</param>
        /// <param name="styles">A parameter list of styles to merge.</param>
        /// <returns>
        /// <para>If no styles are specified, an empty DocumentStyle</para>
        /// <para>If one style is specified, a copy</para>
        /// <para>Otherwise a DocumentStyle containing a union of all specified keys with earlier-passed styles taking precedence</para>
        /// </returns>
        public static DocumentStyle Merge(string name, string basedOnStyleName, params DocumentStyle[] styles)
        {
            Dictionary<DocumentStyleProperty, object> result = new Dictionary<DocumentStyleProperty, object>();

            foreach (DocumentStyle style in styles)
            {
                if (style is null) continue;

                result = result.MergeWith(style._stylesByProperty);
            }

            return new DocumentStyle(result, name, basedOnStyleName);
        }

        /// <summary>
        /// Merges two or more styles with the styles defined earlier in the parameters list taking precedence in the case of conflicts
        /// </summary>
        /// <param name="styles">A parameter list of styles to merge.</param>
        /// <returns>
        /// <para>If no styles are specified, an empty DocumentStyle</para>
        /// <para>If one style is specified, a copy</para>
        /// <para>Otherwise a DocumentStyle containing a union of all specified keys with earlier-passed styles taking precedence</para>
        /// </returns>

        public static DocumentStyle Merge(params DocumentStyle[] styles) => Merge(null, null, styles);

        internal static DocumentStyle FromXml(XElement styleElement)
        {
            Dictionary<DocumentStyleProperty, object> results = new Dictionary<DocumentStyleProperty, object>();

            if (styleElement is null) return null;

            #region Bold / Italics / Underline / Strikethrough

            if (!ReferenceEquals(null, styleElement.Element(Namespaces.W.b)))
            {
                // todo jdk - can something be "unbold" or is that a new run?
                results[DocumentStyleProperty.IsBold] = true;
            }

            if (!ReferenceEquals(null, styleElement.Element(Namespaces.W.i)))
            {
                // todo jdk - can something be "unitalic" or is that a new run?
                results[DocumentStyleProperty.IsItalic] = true;
            }

            string underlineTypeName = styleElement.Element(Namespaces.W.u)?.Attribute(Namespaces.W.val).Value;
            Color underlineColor = ToColor(styleElement.Element(Namespaces.W.u)?.Attribute(Namespaces.W.color)?.Value);

            switch (underlineTypeName)
            {
                case null:
                    break;

                case "none":
                    results[DocumentStyleProperty.Underline] = UnderlineInfo.None;
                    break;

                case "single":
                case "words":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.Single, underlineColor);
                    break;

                case "thick":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.Thick, underlineColor);
                    break;

                case "double":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.Double, underlineColor);
                    break;

                case "dotted":
                case "dotDash":
                case "dotDotDash":
                case "dottedHeavy":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.Dotted, underlineColor);
                    break;

                case "dash":
                case "dashDotDotHeavy":
                case "dashDotHeavy":
                case "dashedHeavy":
                case "dashLong":
                case "dashLongHeavy":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.Dashed, underlineColor);
                    break;

                case "wave":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.Wave, underlineColor);
                    break;

                case "wavyHeavy":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.WaveHeavy, underlineColor);
                    break;

                case "wavyDouble":
                    results[DocumentStyleProperty.Underline] = new UnderlineInfo(UnderlineType.WaveDouble, underlineColor);
                    break;
               
                default:
                    throw new NotSupportedException($"{nameof(UnderlineType)} of {underlineTypeName} is not supported");
            }

            if (!ReferenceEquals(null, styleElement.Element(Namespaces.W.strike)))
            {
                // todo jdk - can something be "unitalic" or is that a new run?
                results[DocumentStyleProperty.IsStrikeThrough] = true;
            }

            #endregion Bold / Italics / Underline / Strikethrough

            #region Horizontal Align

            string halignName = styleElement.Element(Namespaces.W.jc)?.Attribute(Namespaces.W.val).Value;

            switch (halignName)
            {
                case null:
                    break;

                case "left":
                    results[DocumentStyleProperty.HorizontalAlignment] = Style.HorizontalAlignment.Left;
                    break;

                case "center":
                    results[DocumentStyleProperty.HorizontalAlignment] = Style.HorizontalAlignment.Center;
                    break;

                case "right":
                    results[DocumentStyleProperty.HorizontalAlignment] = Style.HorizontalAlignment.Right;
                    break;

                case "both":
                case "justify":
                    results[DocumentStyleProperty.HorizontalAlignment] = Style.HorizontalAlignment.Justify;
                    break;

                default:
                    throw new NotSupportedException($"{nameof(HorizontalAlignment)} of {halignName} is not supported");
            }

            #endregion Horizontal Align

            #region Vertical Align

            string valignName = styleElement.Element(Namespaces.W.vertAlign)?.Attribute(Namespaces.W.val).Value ??
                                styleElement.Element(Namespaces.W.vAlign)?.Attribute(Namespaces.W.val).Value;

            switch (valignName)
            {
                case null:
                    break;

                case "top":
                    results[DocumentStyleProperty.VerticalAlignment] = Style.VerticalAlignment.Top;
                    break;

                case "center":
                    results[DocumentStyleProperty.VerticalAlignment] = Style.VerticalAlignment.Middle;
                    break;

                case "bottom":
                    results[DocumentStyleProperty.VerticalAlignment] = Style.VerticalAlignment.Bottom;
                    break;

                case "baseline":
                    results[DocumentStyleProperty.VerticalAlignment] = Style.VerticalAlignment.Baseline;
                    break;

                case "superscript":
                    results[DocumentStyleProperty.VerticalAlignment] = Style.VerticalAlignment.Superscript;
                    break;

                case "subscript":
                    results[DocumentStyleProperty.VerticalAlignment] = Style.VerticalAlignment.Subscript;
                    break;

                default:
                    throw new NotSupportedException($"{nameof(VerticalAlignment)} of {valignName} is not supported");
            }

            #endregion Vertical Align

            #region Foreground Color

            Color fgColor = ToColor(styleElement.Element(Namespaces.W.color)?.Attribute(Namespaces.W.val).Value);

            if (fgColor == Color.Transparent)
            {
                fgColor = ToColor(styleElement.Element(Namespaces.W.shd)?.Attribute(Namespaces.W.color).Value);
            }

            if (fgColor != Color.Transparent)
            {
                results[DocumentStyleProperty.ForegroundColor] = fgColor;
            }

            #endregion Foreground Color

            #region Background Color

            Color bgColor = ToColor(styleElement.Element(Namespaces.W.highlight)?.Attribute(Namespaces.W.val).Value);

            if (bgColor == Color.Transparent)
            {
                bgColor = ToColor(styleElement.Element(Namespaces.W.shd)?.Attribute(Namespaces.W.fill).Value);
            }

            if (bgColor != Color.Transparent)
            {
                results[DocumentStyleProperty.BackgroundColor] = bgColor;
            }

            #endregion Background Color

            int size;

            #region Font

            if (int.TryParse(styleElement.Element(Namespaces.W.szCs)?.Attribute(Namespaces.W.val).Value ?? styleElement.Element(Namespaces.W.sz)?.Attribute(Namespaces.W.val).Value, out size))
            {
                results[DocumentStyleProperty.FontSize] = size;
            }

            //public string FontFaceName { get; internal set; }

            #endregion Font

            #region List

            string numberFormatName = styleElement.Element(Namespaces.W.numFmt)?.Attribute(Namespaces.W.val).Value;

            if (!string.IsNullOrEmpty(numberFormatName))
            {
                switch (numberFormatName)
                {
                    case "upperRoman":
                        results[DocumentStyleProperty.ListType] = Style.ListType.RomanUpper;
                        break;

                    case "lowerRoman":
                        results[DocumentStyleProperty.ListType] = Style.ListType.RomanLower;
                        break;

                    case "upperLetter":
                        results[DocumentStyleProperty.ListType] = Style.ListType.LetterUpper;
                        break;

                    case "lowerLetter":
                        results[DocumentStyleProperty.ListType] = Style.ListType.LetterLower;
                        break;

                    case "none":
                        results[DocumentStyleProperty.ListType] = Style.ListType.None;
                        break;

                    case "bullet":
                        results[DocumentStyleProperty.ListType] = Style.ListType.BulletFull;
                        break;

                    default:
                        results[DocumentStyleProperty.ListType] = Style.ListType.Number;
                        break;
                }
            }

            int listStartAt = int.Parse(styleElement.Element(Namespaces.W.start)?.Attribute(Namespaces.W.val).Value ?? "-1");

            if (listStartAt > -1)
            {
                results[DocumentStyleProperty.ListStartAt] = listStartAt;
            }

            #endregion List

            #region Indent and Margins

            XElement indentElement = styleElement.Element(Namespaces.W.ind);

            if (int.TryParse(indentElement?.Attribute(Namespaces.W.left)?.Value, out size))
            {
                results[DocumentStyleProperty.IndentLeft] = size;
            }

            if (int.TryParse(indentElement?.Attribute(Namespaces.W.hanging)?.Value, out size))
            {
                results[DocumentStyleProperty.IndentHanging] = size;
            }

            #endregion Indent and Margins

            #region Table

            XElement bordersElement = styleElement.Element(Namespaces.W.tblBorders);

            if (bordersElement != null)
            {
                BorderInfo borderInfo;

                if (BorderInfo.FromElement(bordersElement.Element(Namespaces.W.top), out borderInfo))
                {
                    results.Add(DocumentStyleProperty.BorderTop, borderInfo);
                }

                if (BorderInfo.FromElement(bordersElement.Element(Namespaces.W.bottom), out borderInfo))
                {
                    results.Add(DocumentStyleProperty.BorderBottom, borderInfo);
                }

                if (BorderInfo.FromElement(bordersElement.Element(Namespaces.W.left), out borderInfo))
                {
                    results.Add(DocumentStyleProperty.BorderLeft, borderInfo);
                }

                if (BorderInfo.FromElement(bordersElement.Element(Namespaces.W.right), out borderInfo))
                {
                    results.Add(DocumentStyleProperty.BorderRight, borderInfo);
                }
            }

            #endregion Table

            return new DocumentStyle(results);
        }

        #endregion Factory

        #region Constructors

        static DocumentStyle()
        {
            __sha256 = new SHA256Managed();
            __hexMatcher = new Regex(@"^[0-9a-fA-F]{6}$");
        }

        internal DocumentStyle(IDictionary<DocumentStyleProperty, object> styles, string name = null, string basedOnStyleName = null)
        {
            Name = name;
            BasedOnStyleName = basedOnStyleName;

            _stylesByProperty = styles.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            GeneratedID = generateID(_stylesByProperty);
        }

        private static Guid generateID(IDictionary<DocumentStyleProperty, object> styles)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryWriter bw = new BinaryWriter(ms);

                bw.Write(styles.Count);

                foreach (DocumentStyleProperty existingKey in styles.Keys.OrderBy(k => k.ToString()))
                {
                    bw.Write((byte)existingKey);
                }

                foreach (KeyValuePair<DocumentStyleProperty, object> kvp in styles.OrderBy(kvp => kvp.Key.ToString()))
                {
                    Color color;

                    switch (kvp.Key)
                    {
                        case DocumentStyleProperty.IsBold:
                        case DocumentStyleProperty.IsItalic:
                        case DocumentStyleProperty.IsStrikeThrough:
                            bw.Write((bool)kvp.Value);
                            break;

                        case DocumentStyleProperty.HorizontalAlignment:
                        case DocumentStyleProperty.VerticalAlignment:
                        case DocumentStyleProperty.ListType:
                            bw.Write((byte)kvp.Value);
                            break;

                        case DocumentStyleProperty.ListStartAt:
                        case DocumentStyleProperty.IndentLeft:
                        case DocumentStyleProperty.IndentHanging:
                        case DocumentStyleProperty.FontSize:
                            bw.Write((int)kvp.Value);
                            break;

                        case DocumentStyleProperty.ForegroundColor:
                        case DocumentStyleProperty.BackgroundColor:
                            color = (Color)kvp.Value;
                            bw.Write(color.R);
                            bw.Write(color.G);
                            bw.Write(color.B);
                            break;

                        case DocumentStyleProperty.FontFaceName:
                            bw.Write((string)kvp.Value);
                            break;

                        case DocumentStyleProperty.Underline:
                            UnderlineInfo ui = (UnderlineInfo)kvp.Value;
                            bw.Write((byte)ui.Type);
                            bw.Write(ui.Color.R);
                            bw.Write(ui.Color.G);
                            bw.Write(ui.Color.B);
                            break;

                        case DocumentStyleProperty.BorderTop:
                        case DocumentStyleProperty.BorderBottom:
                        case DocumentStyleProperty.BorderLeft:
                        case DocumentStyleProperty.BorderRight:
                            BorderInfo bi = (BorderInfo)kvp.Value;
                            bw.Write(bi.Width);
                            bw.Write((byte)bi.Type);
                            bw.Write(bi.Color.R);
                            bw.Write(bi.Color.G);
                            bw.Write(bi.Color.B);
                            break;

                        default:
                            throw new InvalidOperationException($"Cannot serialize {nameof(DocumentStyleProperty)} {kvp.Key}");
                    }
                }

                ms.Seek(0, SeekOrigin.Begin);

                return new Guid(__sha256.ComputeHash(ms).Take(16).ToArray());
            }
        }

        #endregion Constructors

        #region Misc Properties

        /// <summary>
        /// A unique id generated from the style keys and values within the DocumentStyle irrespective of defined name or based on name
        /// </summary>
        public Guid GeneratedID { get; }

        /// <summary>
        /// An optional name for the style
        /// </summary>
        public string Name { get; }
        
        /// <summary>
        /// An optional name of the style the style extends
        /// </summary>
        public string BasedOnStyleName { get; }

        #endregion Misc Properties

        #region Style Properties

        /// <summary>
        /// Whether or not the text is bolded
        /// </summary>
        public bool? IsBold => getValueOrDefaultStruct<bool>(DocumentStyleProperty.IsBold);

        /// <summary>
        /// Whether or not the text is italicized
        /// </summary>
        public bool? IsItalic => getValueOrDefaultStruct<bool>(DocumentStyleProperty.IsItalic);

        /// <summary>
        /// The style and color of text underline
        /// </summary>
        public UnderlineInfo? Underline => getValueOrDefaultStruct<UnderlineInfo>(DocumentStyleProperty.Underline);

        /// <summary>
        /// Whether or not the text is struck through
        /// </summary>
        public bool? IsStrikeThrough => getValueOrDefaultStruct<bool>(DocumentStyleProperty.IsStrikeThrough);

        /// <summary>
        /// The type of list the text is part of
        /// </summary>
        public ListType? ListType => getValueOrDefaultStruct<ListType>(DocumentStyleProperty.ListType);

        /// <summary>
        /// The 1-based number that the list starts counting from
        /// </summary>
        public int? ListStartAt => getValueOrDefaultStruct<int>(DocumentStyleProperty.ListStartAt);

        /// <summary>
        /// The horizontal alignment of content within the parent container
        /// </summary>
        public HorizontalAlignment? HorizontalAlignment => getValueOrDefaultStruct<HorizontalAlignment>(DocumentStyleProperty.HorizontalAlignment);

        /// <summary>
        /// The vertical alignment of content within the parent container
        /// </summary>
        public VerticalAlignment? VerticalAlignment => getValueOrDefaultStruct<VerticalAlignment>(DocumentStyleProperty.VerticalAlignment);

        /// <summary>
        /// The left indent of the text, in pixels
        /// </summary>
        public int? IndentLeft => getValueOrDefaultStruct<int>(DocumentStyleProperty.IndentLeft);

        /// <summary>
        /// The hanging indent of the text, in pixels
        /// </summary>
        public int? IndentHanging => getValueOrDefaultStruct<int>(DocumentStyleProperty.IndentHanging);

        /// <summary>
        /// The foreground color of the text
        /// </summary>
        public Color? ForegroundColor => getValueOrDefaultStruct<Color>(DocumentStyleProperty.ForegroundColor);

        /// <summary>
        /// The background color of the text
        /// </summary>
        public Color? BackgroundColor => getValueOrDefaultStruct<Color>(DocumentStyleProperty.BackgroundColor);

        /// <summary>
        /// The font size of the text
        /// </summary>
        public int? FontSize => getValueOrDefaultStruct<int>(DocumentStyleProperty.FontSize);

        /// <summary>
        /// The font face name of the text
        /// </summary>
        public string FontFaceName => getValueOrDefaultClass<string>(DocumentStyleProperty.FontFaceName);

        /// <summary>
        /// The width, color, and style of the top border of the container
        /// </summary>
        public BorderInfo? BorderTop => getValueOrDefaultStruct<BorderInfo>(DocumentStyleProperty.BorderTop);

        /// <summary>
        /// The width, color, and style of the bottom border of the container
        /// </summary>
        public BorderInfo? BorderBottom => getValueOrDefaultStruct<BorderInfo>(DocumentStyleProperty.BorderBottom);

        /// <summary>
        /// The width, color, and style of the left border of the container
        /// </summary>
        public BorderInfo? BorderLeft => getValueOrDefaultStruct<BorderInfo>(DocumentStyleProperty.BorderLeft);

        /// <summary>
        /// The width, color, and style of the right border of the container
        /// </summary>
        public BorderInfo? BorderRight => getValueOrDefaultStruct<BorderInfo>(DocumentStyleProperty.BorderRight);

        private T? getValueOrDefaultStruct<T>(DocumentStyleProperty key) where T : struct
        {
            if (_stylesByProperty.TryGetValue(key, out object result))
                return (T?)result;

            return null;
        }

        private T getValueOrDefaultClass<T>(DocumentStyleProperty key) where T : class
        {
            if (_stylesByProperty.TryGetValue(key, out object result))
                return (T) result;

            return null;
        }

        #endregion Style Properties

        #region Equality

        /// <summary>
        /// Compares two DocumentStyles for equality
        /// </summary>
        /// <param name="a">The first DocumentStyle to compare</param>
        /// <param name="b">The second DocumentStyle to compare</param>
        /// <returns>
        /// True if
        /// <para>Both DocumentStyles are null</para>
        /// <para>Both DocumentStyles are non-null and contain the exact same keys and values</para>
        /// </returns>
        public static bool operator == (DocumentStyle a, DocumentStyle b)
        {
            if(ReferenceEquals(a, b)) return true;
            if(ReferenceEquals(a, null)) return false;
            return a.Equals(b);
        }

        /// <summary>
        /// Compares two DocumentStyles for inequality
        /// </summary>
        /// <param name="a">The first DocumentStyle to compare</param>
        /// <param name="b">The second DocumentStyle to compare</param>
        /// <returns>
        /// False if
        /// <para>Both DocumentStyles are null</para>
        /// <para>Both DocumentStyles are non-null and contain the exact same keys and values</para>
        /// </returns>
        public static bool operator != (DocumentStyle a, DocumentStyle b)
        {
            return !(a == b);
        }

        /// <summary>
        /// Compares a DocumentStyles to the invocant for equality
        /// </summary>
        /// <param name="other">The DocumentStyle to compare</param>
        /// <returns>
        /// True if
        /// <para>Other is not null</para>
        /// <para>Both DocumentStyles are non-null and contain the exact same keys and values</para>
        /// </returns>
        public bool Equals(DocumentStyle other)
        {
            if (other is null) return false;

            return GeneratedID == other.GeneratedID && Name == other.Name;
        }

        public override int GetHashCode() => GeneratedID.GetHashCode();

        public override bool Equals(object obj)
        {
            return base.Equals(obj as DocumentStyle);
        }

        #endregion Equality

        #region Helpers

        internal static Color ToColor(string value, bool autoIsBlack = false)
        {
            if (string.IsNullOrWhiteSpace(value) || value == "auto")
            {
                return autoIsBlack ? Color.Black : Color.Transparent;
            }

            if (__hexMatcher.IsMatch(value))
            {
                byte[] rgb = Enumerable.Range(0, 3).Select(idx => Convert.ToByte(value.Substring(idx * 2, 2), 16)).ToArray();
                return Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            }

            return Color.FromName(value);
        }

        /// <summary>
        /// Converts a color object to a CSS color
        /// </summary>
        /// <param name="color">The color to transform</param>
        /// <param name="useRGB">When true, the color will be returned in rgb(r,g,b) form, otherwise it will be returned in #RRGGBB hex form</param>
        /// <returns>The rgb or hex representation of the color or, if the color is transparent, the word "transparent"</returns>
        /// <remarks>Even well-known colors (e.g. red) are returned in hex or rgb format</remarks>
        public static string GenerateCSSColor(Color color, bool useRGB)
        {
            if (color == Color.Transparent) return "transparent";

            return useRGB
                ? $"rgb({color.R}, {color.G}, {color.B})"
                : $"#{color.R.ToString("x2")}{color.G.ToString("x2")}{color.B.ToString("x2")}";
        }

        #endregion Helpers

        public override string ToString() => string.Join("; ", _stylesByProperty.Select(kvp => $"{kvp.Key}: {kvp.Value}"));
    }
}
