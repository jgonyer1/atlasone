﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;

using DCGWordConvert.Style;
using DCGWordConvert.Transform;

namespace DCGWordConvert.Conversion
{
    public class StyleBuilderFactory
    {
        private const string TEXT_DECORATION = "text-decoration";

        public StyleBuilder GenerateStyleBuilder(DocumentStyle style, DocumentElement contextElement = null)
        {
            StyleBuilder result = new StyleBuilder(contextElement);

            if (style is null) return result;

            if (style.IsBold.HasValue) GenerateBold(result);
            if (style.IsItalic.HasValue) GenerateItalic(result);
            if (style.Underline.HasValue) GenerateUnderline(style.Underline.Value, result);
            if (style.IsStrikeThrough.HasValue) GenerateStrikeThrough(result);
            if (style.HorizontalAlignment.HasValue) GenerateHorizontalAlignment(style.HorizontalAlignment.Value, result);
            if (style.VerticalAlignment.HasValue) GenerateVerticalAlignment(style.VerticalAlignment.Value, result);
            if (style.ForegroundColor.HasValue) GenerateForegroundColor(style.ForegroundColor.Value, result);
            if (style.BackgroundColor.HasValue) GenerateBackgroundColor(style.BackgroundColor.Value, result);
            if (style.FontSize.HasValue) GenerateFontSize(style.FontSize.Value, result);
            if (!ReferenceEquals(null, style.FontFaceName)) GenerateFontFace(style.FontFaceName, result);
            if (style.BorderTop.HasValue) GenerateBorder(style.BorderTop.Value, DocumentStyleProperty.BorderTop, result);
            if (style.BorderBottom.HasValue) GenerateBorder(style.BorderBottom.Value, DocumentStyleProperty.BorderBottom, result);
            if (style.BorderLeft.HasValue) GenerateBorder(style.BorderBottom.Value, DocumentStyleProperty.BorderLeft, result);
            if (style.BorderRight.HasValue) GenerateBorder(style.BorderRight.Value, DocumentStyleProperty.BorderRight, result);
            if (style.ListType.HasValue) GenerateListType(style.ListType.Value, result);
            if (style.ListStartAt.HasValue) GenerateListStartAt(style.ListStartAt.Value, result);
            if (style.IndentLeft.HasValue) GenerateIndent(style.IndentLeft.Value, DocumentStyleProperty.IndentLeft, result);
            if (style.IndentHanging.HasValue) GenerateIndent(style.IndentHanging.Value, DocumentStyleProperty.IndentHanging, result);

            return result;
        }
    
        #region Visitor Methods

        /// <summary>Generates bold css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateBold(StyleBuilder styling) => styling.AddStyling("font-weight", "bold");

        /// <summary>Generates italic css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateItalic(StyleBuilder styling) => styling.AddStyling("font-style", "oblique");

        /// <summary>Generates underline css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateUnderline(UnderlineInfo underlineInfo, StyleBuilder styling)
        {
            string cssTextDecorationStyle;

            switch (underlineInfo.Type)
            {
                case UnderlineType.None:
                    return;

                case UnderlineType.Single:
                case UnderlineType.Thick:
                    cssTextDecorationStyle = "solid";
                    break;

                case UnderlineType.Double:
                    cssTextDecorationStyle = "double";
                    break;

                case UnderlineType.Dotted:
                    cssTextDecorationStyle = "dotted";
                    break;

                case UnderlineType.Dashed:
                    cssTextDecorationStyle = "dashed";
                    break;

                case UnderlineType.Wave:
                case UnderlineType.WaveHeavy:
                case UnderlineType.WaveDouble:
                    cssTextDecorationStyle = "wavy";
                    break;

                default:
                    cssTextDecorationStyle = "solid";
                    break;
            }

            // underline needs to play well with line-through, so it should be at the end of the list
            List<string> withExisting = styling.GetStyling(TEXT_DECORATION).ToList();

            string cssColor = underlineInfo.Color == Color.Transparent
                ? string.Empty
                : DocumentStyle.GenerateCSSColor(underlineInfo.Color, true);

            withExisting.Add($"underline {cssTextDecorationStyle} {cssColor}");

            styling.ReplaceStyling(TEXT_DECORATION, withExisting.ToArray());
        }

        /// <summary>Generates strike-through/line-through css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateStrikeThrough(StyleBuilder styling)
        {
            // underline needs to play well with line-through, so it should be at the start of the list
            List<string> withExisting = styling.GetStyling(TEXT_DECORATION).ToList();

            withExisting.Insert(0, "line-through");

            styling.ReplaceStyling(TEXT_DECORATION, withExisting.ToArray());
        }

        /// <summary>Generates horizontal alignment css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateHorizontalAlignment(HorizontalAlignment halign, StyleBuilder styling)
        {
            string cssTextAlign;

            switch (halign)
            {
                case HorizontalAlignment.Left:
                    cssTextAlign = "left";
                    break;

                case HorizontalAlignment.Center:
                    cssTextAlign = "center";
                    break;

                case HorizontalAlignment.Right:
                    cssTextAlign = "right";
                    break;

                case HorizontalAlignment.Justify:
                    cssTextAlign = "justify";
                    break;

                default:
                    return;
            }

            styling.AddStyling("text-align", cssTextAlign);
        }

        /// <summary>Generates vertical alignment css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateVerticalAlignment(VerticalAlignment valign, StyleBuilder styling)
        {
            string cssVerticalAlign;

            switch (valign)
            {
                case VerticalAlignment.Top:
                    cssVerticalAlign = "top";
                    break;

                case VerticalAlignment.Middle:
                    cssVerticalAlign = "middle";
                    break;

                case VerticalAlignment.Bottom:
                    cssVerticalAlign = "bottom";
                    break;

                case VerticalAlignment.Baseline:
                    cssVerticalAlign = "baseline";
                    break;

                case VerticalAlignment.Subscript:
                    cssVerticalAlign = "sub";
                    break;

                case VerticalAlignment.Superscript:
                    cssVerticalAlign = "super";
                    break;

                default:
                    return;
            }

            styling.AddStyling("vertical-align", cssVerticalAlign);
        }

        /// <summary>Generates bold css styling</summary>
        /// <param name="contextElement">The element which the style is being generated on, may be null for defined styles</param>
        /// <returns>A dictionary of style properties and values, null to return no styling</returns>
        protected virtual void GenerateBorder(BorderInfo borderInfo, DocumentStyleProperty key, StyleBuilder styling)
        {
            string borderTypeCSSName;

            switch (borderInfo.Type)
            {
                case BorderType.None:
                    borderTypeCSSName = "none";
                    break;

                case BorderType.Single:
                case BorderType.Wave:
                    borderTypeCSSName = "solid";
                    break;

                case BorderType.Double:
                case BorderType.WaveDouble:
                    borderTypeCSSName = "double";
                    break;

                case BorderType.Dotted:
                    borderTypeCSSName = "dotted";
                    break;

                case BorderType.Dashed:
                    borderTypeCSSName = "dashed";
                    break;
                case BorderType.Inset:
                    borderTypeCSSName = "inset";
                    break;

                case BorderType.Outset:
                    borderTypeCSSName = "outset";
                    break;

                default:
                    borderTypeCSSName = "solid";
                    break;
            }

            styling.ReplaceStyling(
                $"border-{key.ToString().ToLowerInvariant().Replace("border", "")}",
                $"{borderTypeCSSName} {borderInfo.Width}px {DocumentStyle.GenerateCSSColor(borderInfo.Color, true)}"
            );
        }

        protected virtual void GenerateForegroundColor(Color color, StyleBuilder styling) => styling.ReplaceStyling("color", DocumentStyle.GenerateCSSColor(color, true));

        protected virtual void GenerateBackgroundColor(Color color, StyleBuilder styling) => styling.ReplaceStyling("background-color", DocumentStyle.GenerateCSSColor(color, true));

        protected virtual void GenerateFontFace(string fontName, StyleBuilder styling)
        {
            throw new NotImplementedException();
        }

        protected virtual void GenerateFontSize(int points, StyleBuilder styling) => styling.ReplaceStyling("font-size", $"{points}pt");

        protected virtual void GenerateListType(ListType type, StyleBuilder styling)
        {
            string cssListStyleType;

            switch (type)
            {
                case ListType.None:
                    cssListStyleType = "none";
                    break;

                case ListType.BulletFull:
                    cssListStyleType = "disc";
                    break;

                case ListType.BulletHollow:
                case ListType.BulletArrow:
                case ListType.BulletOther:
                    cssListStyleType = "circle";
                    break;

                case ListType.IncrementalOther:
                case ListType.Number:
                    cssListStyleType = "decimal";
                    break;

                case ListType.LetterUpper:
                    cssListStyleType = "upper-alpha";
                    break;

                case ListType.LetterLower:
                    cssListStyleType = "lower-alpha";
                    break;

                case ListType.RomanUpper:
                    cssListStyleType = "upper-roman";
                    break;

                case ListType.RomanLower:
                    cssListStyleType = "lower-roman";
                    break;

                default:
                    return;
            }

            styling.AddStyling("list-style-type", cssListStyleType);
        }

        protected virtual void GenerateListStartAt(int startAt, StyleBuilder styling)
        {
            // do nothing, this is handled elsewhere
            // todo jdk
        }

        protected virtual void GenerateIndent(int value, DocumentStyleProperty key, StyleBuilder styling)
        {
            switch(key)
            {
                case DocumentStyleProperty.IndentLeft:
                    styling.ReplaceStyling("margin-left", $"{value}px");
                    break;

                // otherwise do nothing
            }            
        }

        #endregion Visitor Methods

        #region Helper Methods

        public virtual void SetClass(DocumentElement documentElement, XElement xElement, DefinedStylesLookup definedStylesLookup)
        {
            if (documentElement.LinkedStyle is null) return;

            IEnumerable<string> classNames = definedStylesLookup.TryGetCascading(documentElement.LinkedStyle).Select(s => s.Name);

            xElement.Add(new XAttribute("class", string.Join(" ", classNames.Where(c => c != null))));
        }

        #endregion Helper Methods
    }
}