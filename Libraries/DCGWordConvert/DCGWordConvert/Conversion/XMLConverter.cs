﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Xml.Linq;

using DCGWordConvert.Style;
using DCGWordConvert.Transform;

namespace DCGWordConvert.Conversion
{
    /// <summary>
    /// Base class for converting Word documents into XML or HTML using a visitor pattern
    /// </summary>
    public class XMLConverter
    {
        #region Fields and Constants

        private readonly StyleBuilderFactory _stylebuilderfactory;

        #endregion Fields and Constants

        #region Constructors

        /// <summary>
        /// Creates a new XMLConverter
        /// </summary>
        /// <param name="styleBuilderFactory">An optional style builder factory passed into the override methods</param>
        public XMLConverter(StyleBuilderFactory styleBuilderFactory = null)
        {
            _stylebuilderfactory = styleBuilderFactory;
        }

        #endregion Constructors

        #region Public

        /// <summary>
        /// Processes a loaded Word document and returns an XML representation of it
        /// </summary>
        /// <param name="document">The document to convert</param>
        /// <returns>An HTML representation of the Word document</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the document parameter is null</exception>
        public XNode Process(Document document)
        {
            if (document is null) throw new ArgumentNullException(nameof(document));

            return Process(document.BodyElement, _stylebuilderfactory, document);
        }

        #endregion Public

        #region Vistor Methods

        /// <summary>
        /// Processes an element in a Word document and, by default, dispatches processing to specific translator methods depending on the element type
        /// </summary>
        /// <param name="element">The element to convert</param>
        /// <param name="styleBuilderFactory">The StyleBuilderFactory associated with the current XMLConverter</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated element</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode Process(DocumentElement element, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            switch (element.Type)
            {
                case DocumentElementType.Body:
                    return ProcessBody(element, styleBuilderFactory, document);

                case DocumentElementType.TextRun:
                    return ProcessTextRun((TextRunElement)element, styleBuilderFactory, document);

                case DocumentElementType.Paragraph:
                    return ProcessParagraph(element, styleBuilderFactory, document);

                case DocumentElementType.BookmarkTarget:
                    return ProcessBookmarkTarget((BookmarkTargetElement)element, styleBuilderFactory, document);

                case DocumentElementType.Table:
                    return ProcessTable((TableElement)element, styleBuilderFactory, document);

                case DocumentElementType.TableRow:
                    return ProcessTableRow(element, styleBuilderFactory, document);

                case DocumentElementType.TableCell:
                    return ProcessTableCell((TableCellElement)element, styleBuilderFactory, document);

                case DocumentElementType.List:
                    return ProcessList((ListElement)element, styleBuilderFactory, document);

                case DocumentElementType.ListItem:
                    return ProcessListItem((ListItemElement)element, styleBuilderFactory, document);

                case DocumentElementType.Image:
                    return ProcessImage((ImageElement)element, styleBuilderFactory, document);

                case DocumentElementType.LogMessage:
                    return ProcessLogMessage((LogMessageElement)element, document);

                default:
                    throw new NotSupportedException($"Element type {element.Type} is not supported");
            }
        }

        /// <summary>
        /// Processes the body of the Word document
        /// </summary>
        /// <param name="body">The body element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated body</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessBody(DocumentElement body, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            XElement result = new XElement("div", new XAttribute("class", "wordDocBody"));

            foreach (DocumentElement child in body.Children)
            {
                result.Add(Process(child, styleBuilderFactory, document));
            }

            return result;
        }

        /// <summary>
        /// Processes a list within a Word document
        /// </summary>
        /// <param name="list">The list element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated list</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessList(ListElement list, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            string elementName;

            switch (list.DefinedAnonymousStyle?.ListType)
            {
                case ListType.IncrementalOther:
                case ListType.Number:
                case ListType.LetterUpper:
                case ListType.LetterLower:
                case ListType.RomanUpper:
                case ListType.RomanLower:
                    elementName = "ol";
                    break;

                default:
                    elementName = "ul";
                    break;
            }

            return CreateNodeWithStyling(list, styleBuilderFactory, document, elementName, true, true);
        }

        /// <summary>
        /// Processes a list item within a Word document
        /// </summary>
        /// <param name="listItem">The list item element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated list item</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessListItem(ListItemElement listItem, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            XElement liElement = CreateNodeWithStyling(listItem, styleBuilderFactory, document, "li", true, true);

            if (listItem.IsFauxBlock)
            {
                liElement.SetAttributeValue("class", (liElement.Attribute("class")?.Value ?? "") + " faux-block");
            }

            if (!listItem.IsFauxBlock && listItem.ContinueAt.HasValue)
            {
                liElement.SetAttributeValue("value", listItem.ContinueAt);
            }

            return liElement;
        }

        /// <summary>
        /// Processes a paragraph within a Word document
        /// </summary>
        /// <param name="paragraph">The paragraph element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated paragraph</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessParagraph(DocumentElement paragraph, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            return CreateNodeWithStyling(paragraph, styleBuilderFactory, document, "p", true, true);
        }

        /// <summary>
        /// Processes a text run (span) within a Word document
        /// </summary>
        /// <param name="run">The run element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated run</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessTextRun(TextRunElement textRun, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            XElement spanElement = CreateNodeWithStyling(textRun, styleBuilderFactory, document, "span", true, true);

            // subscript and superscript are special cases
            switch (textRun.DefinedAnonymousStyle?.VerticalAlignment)
            {
                case VerticalAlignment.Subscript:
                    spanElement.Add(new XElement("sub", new XText(textRun.TextValue)));
                    break;

                case VerticalAlignment.Superscript:
                    spanElement.Add(new XElement("sup", new XText(textRun.TextValue)));
                    break;

                default:
                    spanElement.Add(new XText(textRun.TextValue));
                    break;
            }

            return spanElement;
        }

        /// <summary>
        /// Processes a table within a Word document
        /// </summary>
        /// <param name="table">The table element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated table</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessTable(TableElement table, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            XElement tableElement = CreateNodeWithStyling(table, styleBuilderFactory, document, "table", false, false);

            XElement tbody = new XElement("tbody");
            tableElement.Add(tbody);

            tbody.Add(table.Children.Select(c => Process(c, styleBuilderFactory, document)));

            return tableElement;
        }

        /// <summary>
        /// Processes a table row within a Word document
        /// </summary>
        /// <param name="tableRow">The table row element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated table row</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessTableRow(DocumentElement tableRow, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            return CreateNodeWithStyling(tableRow, styleBuilderFactory, document, "tr", true, true);
        }

        /// <summary>
        /// Processes a table cell within a Word document
        /// </summary>
        /// <param name="tableCell">The list element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated table cell</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessTableCell(TableCellElement tableCell, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            if (tableCell.IsSkipped)
            {
                return new XComment("Skipped table cell");
            }

            XElement tdElement = CreateNodeWithStyling(tableCell, styleBuilderFactory, document, "td", false, false);

            if (tableCell.ColumnSpan > 1)
            {
                tdElement.Add(new XAttribute("colspan", tableCell.ColumnSpan));
            }

            if (tableCell.RowSpan > 1)
            {
                tdElement.Add(new XAttribute("rowspan", tableCell.RowSpan));
            }

            tdElement.Add(tableCell.Children.Select(c => Process(c, styleBuilderFactory, document)));

            return tdElement;
        }

        /// <summary>
        /// Processes a bookmark target within a Word document
        /// </summary>
        /// <param name="bookmark">The bookmark element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated bookmark</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessBookmarkTarget(BookmarkTargetElement bookmark, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            return new XElement("a", new XAttribute("name", bookmark.Name), string.Empty); // string empty to force closing tag
        }

        /// <summary>
        /// Processes a hyperlink within a Word document
        /// </summary>
        /// <param name="hyperlink">The hyperlink element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated hyperlink</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessHyperlink(BookmarkTargetElement hyperlink, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes a page break within a Word document
        /// </summary>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated pagebreak</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessPageBreak(StyleBuilderFactory styleBuilderFactory, DocumentElement document)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes an image or EMF file within a Word document
        /// </summary>
        /// <param name="image">The image element to be translated</param>
        /// <param name="styleBuilderFactory">A reference to the StyleBuilderFactory passed in the XMLConverter constructor</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated image</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessImage(ImageElement image, StyleBuilderFactory styleBuilderFactory, Document document)
        {
            string base64 = Convert.ToBase64String(image.Bytes);
            string prefix = image.Format == ImageFormat.Jpeg ? "data:image/jpeg;base64," : "data:image/png;base64,";

            XElement imgElement = CreateNodeWithStyling(image, styleBuilderFactory, document, "img", false, false);

            imgElement.Add(new XAttribute("src", prefix + base64));

            return imgElement;
        }

        /// <summary>
        /// Processes a log message for problems translating portions of a Word document
        /// </summary>
        /// <param name="message">The message element to be translated</param>
        /// <param name="document">A reference to the current Word document</param>
        /// <returns>An XNode representing the translated message</returns>
        /// <remarks>Returning null will not write anything into the target document for the DocumentElement</remarks>
        protected virtual XNode ProcessLogMessage(LogMessageElement message, Document document)
        {
            return new XComment(message.LogMessage);
        }

        #endregion Vistor Methods

        #region Helper Methods

        protected XElement CreateNodeWithStyling(DocumentElement source, StyleBuilderFactory styleBuilderFactory, Document document, XName nodeName, bool autoClose, bool includeChildren)
        {
            XElement result = new XElement(nodeName);

            if (!ReferenceEquals(null, styleBuilderFactory))
            {
                StyleBuilder styling = styleBuilderFactory.GenerateStyleBuilder(source.DefinedAnonymousStyle, contextElement: source);
                styling.ApplyStyleAttribute(result, false);
                styling.ApplyAttributes(result, false);
                styleBuilderFactory.SetClass(source, result, document.DefinedStyles);
            }

            if(includeChildren)
            {
                result.Add(source.Children.Select(c => Process(c, styleBuilderFactory, document)));
            }

            if (autoClose)
            {
                result.Add(string.Empty);
            }

            return result;
        }

        #endregion Helper Methods
    }
}
