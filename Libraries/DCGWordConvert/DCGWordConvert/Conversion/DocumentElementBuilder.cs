﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;

using DCGWordConvert.Helpers;
using DCGWordConvert.Style;
using DCGWordConvert.Transform;

namespace DCGWordConvert.Conversion
{
    internal class DocumentElementBuilder
    {
        #region Fields

        private static readonly Regex __styleMatcher;

        private readonly DefinedStylesLookup _definedStylesLookup;
        private readonly NumberingLookup _numberingLookup;
        private readonly IReadOnlyDictionary<string, Reference> _references;
        private readonly Manifest _manifest;

        #endregion Fields

        #region Constructors

        static DocumentElementBuilder()
        {
            __styleMatcher = new Regex(@"(?<property>\w+):\s*(?<value>\d+(\.\d+)?)(?<unit>pt|px)(;|$)");
        }

        public DocumentElementBuilder(DefinedStylesLookup definedStylesLookup, NumberingLookup numberingLookup, IReadOnlyDictionary<string, Reference> references, Manifest manifest)
        {
            _definedStylesLookup = definedStylesLookup;
            _numberingLookup = numberingLookup;
            _references = references;
            _manifest = manifest;
        }

        #endregion Constructors

        #region Process

        internal DocumentElement Process(XElement element)
        {
            DocumentElement result = process(element, false, null);

            return result;
        }

        private DocumentElement process(XElement element, bool ignoreUndefined, IReadOnlyCollection<DocumentElement> hierarchy)
        {
            string localName = element.Name.LocalName.ToLower();

            switch (localName)
            {
                case "body":
                    return processContainer(element, new List<DocumentElement>(), DocumentElementType.Body);

                case "r":
                    return processTextRun(element, hierarchy);

                case "p":
                    return processContainer(element, hierarchy, DocumentElementType.Paragraph);

                case "bookmarkstart":
                    return processBookmarkTarget(element);

                case "pict":
                    return processPict(element);

                case "drawing":
                    return processDrawing(element);

               //case "lastRenderedPageBreak":
               //    throw new NotImplementedException();

                case "tbl":
                    return processTable(element, hierarchy);

                case "tr":
                    return processRow(element, hierarchy);

                case "tc":
                    return processContainer(element, hierarchy, DocumentElementType.TableCell);

                default:
                    return ignoreUndefined ? null : new LogMessageElement($"Element type '{localName}' is not supported");
            }
        }

        #endregion Process

        #region Text

        private IEnumerable<DocumentElement> collapseTextRuns(IEnumerable<DocumentElement> siblings)
        {
            IEnumerator<DocumentElement> enumerator = siblings.GetEnumerator();

            TextRunElement currentTextRun = null;

            while(enumerator.MoveNext())
            {
                DocumentElement currentChild = enumerator.Current;

                if(currentChild.Type != DocumentElementType.TextRun)
                {
                    yield return currentChild;
                    currentChild = null;
                    continue;
                }

                if(currentTextRun is null)
                {
                    currentTextRun = (TextRunElement)currentChild;
                    continue;
                }

                if(currentChild.DefinedAnonymousStyle == currentTextRun.DefinedAnonymousStyle && currentChild.LinkedStyle == currentTextRun.LinkedStyle && ! currentChild.Children.Any() && ! currentTextRun.Children.Any())
                {
                    TextRunElement collapseRun = (TextRunElement)currentChild;

                    if (string.IsNullOrEmpty(collapseRun.TextValue)) continue;

                    currentTextRun.CollapseWith(collapseRun);

                    continue;
                }

                yield return currentTextRun;
                currentTextRun = (TextRunElement) currentChild;
            }

            if (currentTextRun is null) yield break;

            yield return currentTextRun;
        }

        private TextRunElement processTextRun(XElement element, IReadOnlyCollection<DocumentElement> hierarchy)
        {
            XName explicitStyleElementName = Namespaces.W.rPr;

            (DocumentStyle classStyle, DocumentStyle explicitStyle, XElement styleElement) = getElementStyleInfo(element, Namespaces.W.rStyle, explicitStyleElementName);

            XElement textElement = element.Element(Namespaces.W.t);
            bool preserveWhitespace = string.Equals("preserve", element.Attribute(XNamespace.Xml + "space")?.Value);

            string text = element.Element(Namespaces.W.t)?.Value ?? string.Empty;

            TextRunElement result = new TextRunElement(preserveWhitespace, text)
            {
                DefinedAnonymousStyle = explicitStyle,
                LinkedStyle = classStyle
            };

            IEnumerable<XElement> childElements = element.Elements().Where(e => !e.Name.Equals(Namespaces.W.rPr) && ! e.Name.Equals(Namespaces.W.t));

            hierarchy = new[] { result }.Concat(hierarchy).ToList();

            foreach (XElement childElement in childElements)
            {
                result.AddChild(process(childElement, true, hierarchy));
            }

            return result;
        }

        #endregion Text

        #region Container

        private DocumentElement processContainer(XElement element, IReadOnlyCollection<DocumentElement> hierarchy, DocumentElementType containerType)
        {
            XName explicitStyleElementName;
            XName linkedStyleElementName;
            
            switch(containerType)
            {
                case DocumentElementType.TableCell:
                    explicitStyleElementName = Namespaces.W.tcPr;
                    linkedStyleElementName = Namespaces.W.tcStyle;
                    break;

                default:
                    explicitStyleElementName = Namespaces.W.pPr;
                    linkedStyleElementName = Namespaces.W.pStyle;
                    break;
            }

            (DocumentStyle classStyle, DocumentStyle explicitStyle, XElement styleElement) = getElementStyleInfo(element, linkedStyleElementName, explicitStyleElementName);

            ListItemInfo listItemInfo = getListItemInfo(styleElement?.Element(Namespaces.W.numPr));

            DocumentElement result;

            if (containerType == DocumentElementType.TableCell)
            {
                int columnIndex = hierarchy.First().Children.Count;

                TableElement tableElement = hierarchy.SkipWhile(h => h.Type != DocumentElementType.Table).FirstOrDefault() as TableElement;

                int rowIndex = tableElement.Children.Count;

                bool isSkipped = tableElement.TableSpanLookup.DoSkip(rowIndex, columnIndex);

                if (!int.TryParse(styleElement.Element(Namespaces.W.gridSpan)?.Attribute(Namespaces.W.val).Value, out int columnSpan))
                {
                    columnSpan = 1;
                }

                int rowSpan = tableElement.TableSpanLookup.RowSpan(rowIndex, columnIndex);

                result = new TableCellElement(columnSpan, rowSpan, isSkipped)
                {
                    DefinedAnonymousStyle = explicitStyle,
                    LinkedStyle = classStyle
                };
            }
            else
            {
                result = new DocumentElement(containerType)
                {
                    LinkedStyle = classStyle,
                    DefinedAnonymousStyle = explicitStyle,
                    ListItemInfo = listItemInfo
                };
            }

            hierarchy = new[] { result }.Concat(hierarchy).ToList();

            List<DocumentElement> processedChildren = element.Elements().Where(c => c.Name != explicitStyleElementName).Select(el => process(el, true, hierarchy)).ToList();

            List<IGrouping<ListItemInfo, DocumentElement>> listGroupedChildren = processedChildren.Where(c => ! ReferenceEquals(null, c)).GroupByContiguous(c => c.ListItemInfo).ToList();

            Stack<ListElement> listStack = new Stack<ListElement>();

            for(int i=0; i<listGroupedChildren.Count; i++)
            {
                IGrouping<ListItemInfo, DocumentElement> currentGroup = listGroupedChildren[i];

                // if not a rework list ...
                if (currentGroup.Key is null)
                {
                    // ... and not in a list, just add it to the parent
                    if(! listStack.Any())
                    {
                        collapseTextRuns(currentGroup).ForEach(result.AddChild);
                        continue;
                    }

                    // ... if it's the last group ...
                    if(i == listGroupedChildren.Count - 1)
                    {
                        // ... pop everything
                        listStack.Clear();
                        collapseTextRuns(currentGroup).ForEach(result.AddChild);
                        continue;
                    }

                    // ... otherwise, add it as a faux paragraph list item
                    foreach(DocumentElement currentChild in collapseTextRuns(currentGroup))
                    {
                        ListItemElement newListItem = new ListItemElement(true, null);
                        newListItem.AddChild(currentChild);
                        listStack.Peek().AddChild(newListItem);
                    }

                    continue;
                }

                Func<ListElement> getCurrentParent = () => listStack.Any() ? listStack.Peek() : null;

                ListElement currentParent = getCurrentParent();

                // pop
                if(! ReferenceEquals(null, currentParent))
                {
                    // if the list id is different ...
                    if(currentGroup.Key.ListID != currentParent.ListID)
                    {
                        // ... treat it as a hard reset
                        listStack.Clear();
                        currentParent = null;
                    }

                    // ... if the indent is less or it's a reset
                    else if(currentGroup.Key.IndentationLevel < currentParent.IndentationLevel || currentGroup.Key.IsReset)
                    {
                        do
                        {
                            listStack.Pop();
                            currentParent = getCurrentParent();
                        }
                        while (!ReferenceEquals(null, currentParent) && currentParent.IndentationLevel > currentGroup.Key.IndentationLevel + (currentGroup.Key.IsReset ? 1 : 0));
                    }
                }

                // push
                if (currentParent is null)
                {
                    currentParent = new ListElement(currentGroup.Key.ListID, currentGroup.Key.IndentationLevel)
                    {
                        DefinedAnonymousStyle = DocumentStyle.Merge(currentGroup.First().DefinedAnonymousStyle, currentGroup.Key.NumberedListStyle),
                        LinkedStyle = currentGroup.First().LinkedStyle
                    };

                    result.AddChild(currentParent);
                    listStack.Push(currentParent);
                }
                else if (currentGroup.Key.IndentationLevel > currentParent.IndentationLevel)
                {
                    currentParent = new ListElement(currentGroup.Key.ListID, currentGroup.Key.IndentationLevel)
                    {
                        DefinedAnonymousStyle = DocumentStyle.Merge(currentGroup.First().DefinedAnonymousStyle, currentGroup.Key.NumberedListStyle),
                        LinkedStyle = currentGroup.First().LinkedStyle
                    };

                    ListItemElement containerListItem = new ListItemElement(true, null);

                    containerListItem.AddChild(currentParent);

                    listStack.Peek().AddChild(containerListItem);
                    listStack.Push(currentParent);
                }

                bool isLastChildFaux = ((currentParent.Children.LastOrDefault() as ListItemElement)?.IsFauxBlock).GetValueOrDefault();

                int? continueAt = isLastChildFaux
                    ? (int?)currentParent.Children.Cast<ListItemElement>().Count(li => !li.IsFauxBlock) + 1
                    : null;

                foreach (DocumentElement currentChild in currentGroup)
                {
                    ListItemElement newListItem = new ListItemElement(false, continueAt);
                    continueAt = null;
                    newListItem.AddChild(currentChild);
                    currentChild.ListItemInfo = null;
                    currentParent.AddChild(newListItem);
                }
            }

            return result;
        }

        private ListItemInfo getListItemInfo(XElement listReferenceElement)
        {
            if (listReferenceElement is null) return null;

            int listIndentationLevel = int.Parse(listReferenceElement.Element(Namespaces.W.ilvl).Attribute(Namespaces.W.val).Value);
            int listID = int.Parse(listReferenceElement.Element(Namespaces.W.numId).Attribute(Namespaces.W.val).Value);

            DocumentStyle numberedListStyle = _numberingLookup.GetStyleByIDAndIndentationLevel(listID, listIndentationLevel);

            return new ListItemInfo(listIndentationLevel, listID, false, numberedListStyle);
        }

        #endregion Container

        #region Bookmarks and Hyperlinks

        private BookmarkTargetElement processBookmarkTarget(XElement element)
        {
            string name = element.Attribute(Namespaces.W.name).Value;

            return new BookmarkTargetElement(name);
        }

        #endregion Bookmarks and Hyperlinks

        #region Images

        private DocumentElement processPict(XElement element)
        {
            // for points, we're going to assume one point = one pixel

            XElement shapeElement = element.Element(Namespaces.V.shape);

            if(shapeElement == null)
            {
                return new LogMessageElement("Unable to find shape inside of picture");
            }

            Size? size = null;
            MatchCollection matches;

            string style = shapeElement.Attribute("style").Value;

            if (! string.IsNullOrEmpty(style) && (matches = __styleMatcher.Matches(style)).Count > 0)
            {
                double width = 0;
                double height = 0;
                
                foreach(Match match in matches)
                {
                    double val = double.Parse(match.Groups["value"].Value);

                    switch(match.Groups["property"].Value)
                    {
                        case "width":
                            width = val;
                            break;

                        case "height":
                            height = val;
                            break;
                    }
                }

                size = new Size((int)width, (int)height);
            }

            string referenceID = shapeElement.Element(Namespaces.V.imagedata)?.Attribute(Namespaces.R2.id).Value;

            return getImageData(referenceID, size);
        }

        private DocumentElement processDrawing(XElement element)
        {
            XElement blipElement = element.XPathSelectElement(".//a:graphic/a:graphicData/pic:pic/pic:blipFill/a:blip", Namespaces.Resolver);

            if (blipElement == null)
            {
                return new LogMessageElement("Unable to find blip inside of drawing");
            }

            string referenceID = blipElement.Attribute(Namespaces.R2.embed).Value;

            return getImageData(referenceID, null);
        }

        private DocumentElement getImageData(string referenceID, Size? size)
        {
            bool isSuccess = false;
            Stream stream = null;
            Reference reference = null;

            try
            {
                if (!string.IsNullOrEmpty(referenceID) && _references.TryGetValue(referenceID, out reference))
                {
                    (isSuccess, stream) = _manifest.TryGetStream(reference.Value);
                }

                if (!isSuccess)
                {
                    return new LogMessageElement($"Unable to find image reference {referenceID}");
                }

                var imageData = reference.Value.ToLowerInvariant().EndsWith(".emf")
                    ? getImageFromEMFStream(stream, size)
                    : getImageFromStream(stream, size);

                return new ImageElement(referenceID, imageData.Width, imageData.Height, imageData.Format, imageData.Data);
            }
            finally
            {
                stream?.Dispose();
            }
        }

        private static (ImageFormat Format, int Width, int Height, byte[] Data) getImageFromStream(Stream stream, Size? size)
        {
            using (Image image = Image.FromStream(stream))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    ImageFormat outFormat = image.RawFormat == ImageFormat.Jpeg ? ImageFormat.Jpeg : ImageFormat.Png;

                    if (size.HasValue)
                    {
                        using (Bitmap canvas = new Bitmap(size.Value.Width, size.Value.Height))
                        {
                            using (Graphics graphics = Graphics.FromImage(canvas))
                            {
                                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                                graphics.DrawImage(image, 0, 0, canvas.Width, canvas.Height);

                                canvas.Save(outStream, outFormat);
                            }
                        }
                    }
                    else
                    {
                        image.Save(outStream, outFormat);
                    }

                    return (outFormat, size == null ? image.Width : size.Value.Width, size == null ? image.Height : size.Value.Height, outStream.ToArray());
                }
            }
        }

        private static (ImageFormat Format, int Width, int Height, byte[] Data) getImageFromEMFStream(Stream stream, Size? size)
        {
            const int MAX_WIDTH_HEIGHT = 6000;

            Metafile emf = new Metafile(stream);
            {
                Size constrainedSize = ImageHelpers.GetConstrainedSize(new Size(size?.Width ?? emf.Width, size?.Height ?? emf.Height), MAX_WIDTH_HEIGHT);

                using (Bitmap canvas = new Bitmap(constrainedSize.Width, constrainedSize.Height))
                {
                    using (Graphics graphics = Graphics.FromImage(canvas))
                    {
                        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                        graphics.DrawImage(emf, 0, 0, canvas.Width, canvas.Height);

                        using (MemoryStream outStream = new MemoryStream())
                        {
                            canvas.Save(outStream, ImageFormat.Png);
                            return (ImageFormat.Png, canvas.Width, canvas.Height, outStream.ToArray());
                        }
                    }
                }
            }
        }

        #endregion Images

        #region Tables

        private TableElement processTable(XElement element, IReadOnlyCollection<DocumentElement> hierarchy)
        {
            XName explicitStyleElementName = Namespaces.W.tblPr;

            (DocumentStyle classStyle, DocumentStyle explicitStyle, XElement styleElement) = getElementStyleInfo(element, Namespaces.W.tblStyle, explicitStyleElementName);

            int?[] columnWidths = element
                .Element(Namespaces.W.tblGrid)
                .Elements(Namespaces.W.gridCol)
                .Select(el =>
                {
                    string w = el.Attribute(Namespaces.W.w)?.Value;

                    if (!int.TryParse(w, out int width))
                        return (int?)null;

                    return width;
                })
                .ToArray();

            XElement[] rowElements = element.Elements(Namespaces.W.tr).ToArray();

            TableElement result = new TableElement(columnWidths, new TableSpanLookup(element))
            {
                DefinedAnonymousStyle = explicitStyle,
                LinkedStyle = classStyle
            };

            hierarchy = new[] { result }.Concat(hierarchy).ToList();

            foreach (XElement rowElement in rowElements)
            {
                result.AddChild(process(rowElement, true, hierarchy));
            }

            return result;
        }

        private DocumentElement processRow(XElement element, IReadOnlyCollection<DocumentElement> hierarchy)
        {
            XName explicitStyleElementName = Namespaces.W.trPr;

            (DocumentStyle classStyle, DocumentStyle explicitStyle, XElement styleElement) = getElementStyleInfo(element, Namespaces.W.trStyle, explicitStyleElementName);

            XElement[] cellElements = element.Elements(Namespaces.W.tc).ToArray();

            DocumentElement result = new DocumentElement(DocumentElementType.TableRow)
            {
                DefinedAnonymousStyle = explicitStyle,
                LinkedStyle = classStyle
            };

            hierarchy = new[] { result }.Concat(hierarchy).ToList();

            foreach (XElement cellElement in cellElements)
            {
                result.AddChild(process(cellElement, true, hierarchy));
            }

            return result;
        }

        #endregion Tables

        #region Helpers

        private (DocumentStyle ClassStyle, DocumentStyle ExplicitStyle, XElement StyleElement) getElementStyleInfo(XElement element, XName classElementName, XName styleElementName)
        {
            XElement styleElement = element.Element(styleElementName);

            DocumentStyle explicitStyle = DocumentStyle.FromXml(styleElement?.Element(Namespaces.W.rPr));

            string linkedStyleName = styleElement?.Element(classElementName)?.Attribute(Namespaces.W.val).Value;

            explicitStyle = DocumentStyle.Merge(explicitStyle, DocumentStyle.FromXml(styleElement));

            _definedStylesLookup.TryGetStyle(linkedStyleName, out DocumentStyle classStyle);

            return (classStyle, explicitStyle, styleElement);
        }

        #endregion Helpers
    }
}
