﻿using System.Text.RegularExpressions;

namespace DCGWordConvert.Transform
{
    /// <summary>
    /// Represents a text run (span)
    /// </summary>
    public class TextRunElement : DocumentElement
    {
        private static readonly Regex __denormalizedSpaceMatcher;

        static TextRunElement()
        {
            __denormalizedSpaceMatcher = new Regex(@"(^\s|\s{2,}|\s$)");
        }

        internal TextRunElement(bool preserveWhitespace, string textValue) : base(DocumentElementType.TextRun)
        {
            TextValue = textValue;
            PreserveWhitespace = preserveWhitespace;
        }

        /// <summary>
        /// The text within the TextRun
        /// </summary>
        public string TextValue { get; private set; }

        /// <summary>
        /// When true, leading and trailing whitespace should be preserved
        /// </summary>
        public bool PreserveWhitespace { get; private set; }

        internal void CollapseWith(TextRunElement nextElement)
        {
            TextValue += nextElement.TextValue;
            PreserveWhitespace = __denormalizedSpaceMatcher.IsMatch(TextValue);
        }

        public override string ToString() => $" <{Type} PreserveWhitespace\"{PreserveWhitespace}\">{TextValue}</{Type}>";
    }
}
