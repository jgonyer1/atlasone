﻿namespace DCGWordConvert.Transform
{
    /// <summary>
    /// The type of DocumentElement
    /// </summary>
    public enum DocumentElementType : byte
    {
        /// <summary>
        /// The root of the Word document
        /// </summary>
        Body,
        
        /// <summary>
        /// A paragraph
        /// </summary>
        Paragraph,

        /// <summary>
        /// A text run (span)
        /// </summary>
        TextRun,

        /// <summary>
        /// A table
        /// </summary>
        Table,

        /// <summary>
        /// A row within a table
        /// </summary>
        TableRow,

        /// <summary>
        /// A cell within a table
        /// </summary>
        TableCell,

        /// <summary>
        /// An ordered or unordered list
        /// </summary>
        List,

        /// <summary>
        /// An item within a list
        /// </summary>
        ListItem,

        /// <summary>
        /// An image or EMF file
        /// </summary>
        Image,

        /// <summary>
        /// The target of a bookmark
        /// </summary>
        BookmarkTarget,

        /// <summary>
        /// An inline-logged conversion error message
        /// </summary>
        LogMessage
    }
}
