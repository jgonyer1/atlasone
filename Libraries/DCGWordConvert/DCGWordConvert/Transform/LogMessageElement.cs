﻿namespace DCGWordConvert.Transform
{
    /// <summary>
    /// Allows in-document logging of translation errors
    /// </summary>
    public class LogMessageElement : DocumentElement
    {
        internal LogMessageElement(string logMessage) : base(DocumentElementType.LogMessage)
        {
            LogMessage = logMessage;
        }

        /// <summary>
        /// The message to log
        /// </summary>
        public string LogMessage { get; }

        public override string ToString() => $"<{Type}>{LogMessage}</{Type}>";
    }
}
