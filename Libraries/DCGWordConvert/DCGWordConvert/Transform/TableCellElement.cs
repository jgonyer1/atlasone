﻿namespace DCGWordConvert.Transform
{
    /// <summary>
    /// Represents a table cell
    /// </summary>
    public class TableCellElement : DocumentElement
    {
        internal TableCellElement(int columnSpan, int rowSpan, bool isSkipped) : base(DocumentElementType.TableCell)
        {
            ColumnSpan = columnSpan;
            RowSpan = rowSpan;
            IsSkipped = isSkipped;
        }

        /// <summary>
        /// The 1-based number of columns the cell covers
        /// </summary>
        public int ColumnSpan { get; }

        /// <summary>
        /// The 1-based number of rows the cell covers
        /// </summary>
        public int RowSpan { get; }

        /// <summary>
        /// When true, the cell shouldn't be rendered because it is being covered by another cell with a ColumnSpan or Rowspan greater than 1
        /// </summary>
        public bool IsSkipped { get; }

        public override string ToString() => $"<{Type} ColumnSpan=\"{ColumnSpan}\" RowSpan=\"{RowSpan}\" IsSkipped=\"{IsSkipped}\">{{{Children.Count}}}</{Type}>";
    }
}
