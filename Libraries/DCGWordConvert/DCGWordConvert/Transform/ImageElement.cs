﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

using DCGWordConvert.Helpers;

namespace DCGWordConvert.Transform
{
    /// <summary>
    /// An image or EMF file within a Word document
    /// </summary>
    public class ImageElement : DocumentElement
    {
        internal ImageElement(string referenceID, int width, int height, ImageFormat format, byte[] bytes) : base(DocumentElementType.Image)
        {
            ReferenceID = referenceID;
            Width = width;
            Height = height;
            Format = format;
            Bytes = bytes;
        }

        /// <summary>
        /// The id of the file reference
        /// </summary>
        public string ReferenceID { get; }

        /// <summary>
        /// The width of the image, either directly specified or computed
        /// </summary>
        public int Width { get; }

        /// <summary>
        /// The height of an image, either directly specified or computed
        /// </summary>
        public int Height { get; }

        /// <summary>
        /// The image's format, EMF files and non-JPEGs are converted to PNGs
        /// </summary>
        public ImageFormat Format { get; }

        /// <summary>
        /// The bytes representing the file
        /// </summary>
        public byte[] Bytes { get; }

        /// <summary>
        /// Gets an ImageElement copy which is resized to to fit within the max dimension given, keeping the same aspect ratio
        /// </summary>
        /// <param name="maxDimension">The maximum size, in pixels, of any side of the image</param>
        /// <returns>A copy of the given ImageElement constrained to a maximum size</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if the maxDimension argument is non-positive</exception>
        public ImageElement GetResized(int maxDimension)
        {
            if (maxDimension >= Width && maxDimension >= Height)
            {
                return new ImageElement(ReferenceID, Width, Height, Format, Bytes.ToArray());
            }

            Size constrainedSize = ImageHelpers.GetConstrainedSize(new Size(Width, Height), maxDimension);

            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(Bytes, 0, Bytes.Length);
                stream.Seek(0, SeekOrigin.Begin);

                using (Image image = Image.FromStream(stream))
                {
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        using (Bitmap canvas = new Bitmap(constrainedSize.Width, constrainedSize.Height))
                        {
                            using (Graphics graphics = Graphics.FromImage(canvas))
                            {
                                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                                graphics.DrawImage(image, 0, 0, canvas.Width, canvas.Height);

                                canvas.Save(outStream, Format);
                            }
                        }

                        return new ImageElement(ReferenceID, constrainedSize.Width, constrainedSize.Height, Format, outStream.ToArray());
                    }
                }
            }
        }

        public override string ToString() => $"<{Type} ReferenceID=\"{ReferenceID}\" Format=\"{Format}\" Width=\"{Width}\" Height=\"{Height}\"/>";
    }
}
