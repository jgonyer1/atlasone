﻿namespace DCGWordConvert.Transform
{
    /// <summary>
    /// An ordered or unordered list within a Word document
    /// </summary>
    public class ListElement : DocumentElement
    {
        internal ListElement(int listID, int indentationLevel) : base(DocumentElementType.List)
        {
            ListID = listID;
            IndentationLevel = indentationLevel;
        }

        /// <summary>
        /// The id of the list, defined in the Word document
        /// </summary>
        public int ListID { get; }

        /// <summary>
        /// The zero-based indentation level within the given list
        /// </summary>
        public int IndentationLevel { get; }

        public override string ToString() => $"<{Type} ListID=\"{ListID}\" IndentationLevel=\"{IndentationLevel}\">{{{Children.Count}}}<{Type}>";
    }
}
