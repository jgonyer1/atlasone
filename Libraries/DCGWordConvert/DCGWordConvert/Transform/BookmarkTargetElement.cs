﻿namespace DCGWordConvert.Transform
{
    /// <summary>
    /// Represents an in-document anchor point for bookmark navigation
    /// </summary>
    public class BookmarkTargetElement : DocumentElement
    {
        internal BookmarkTargetElement(string name) : base(DocumentElementType.BookmarkTarget)
        {
            Name = name;
        }
        
        /// <summary>
        /// The name of the bookmark target
        /// </summary>
        public string Name { get; }

        public override string ToString() => $"<{Type} Name=\"{Name}\"/>";
    }
}
