﻿using System.Collections.Generic;
using System.Linq;

using DCGWordConvert.Helpers;

namespace DCGWordConvert.Transform
{
    /// <summary>
    /// Represents a table
    /// </summary>
    public class TableElement : DocumentElement
    {
        internal TableElement(IEnumerable<int?> columnWidths, TableSpanLookup tableSpanLookup) : base(DocumentElementType.Table)
        {
            ColumnWidths = columnWidths.ToArray();
            TableSpanLookup = tableSpanLookup;
        }

        /// <summary>
        /// The number of rows in the table (synonymous with the number of children in the table)
        /// </summary>
        public int RowsCount => Children.Count;

        /// <summary>
        /// The number of columns in the table
        /// </summary>
        public int ColumnsCount => ColumnWidths.Count;

        /// <summary>
        /// A collection of specified column widths defined in the document
        /// </summary>
        /// <remarks>These can be used to render the table fairly accurately to its Word document representation</remarks>
        public IReadOnlyCollection<int?> ColumnWidths { get; }

        internal TableSpanLookup TableSpanLookup { get; }

        public override string ToString() => $"<{Type}>{{{RowsCount} x {ColumnsCount}}}</{Type}>";
    }
}
