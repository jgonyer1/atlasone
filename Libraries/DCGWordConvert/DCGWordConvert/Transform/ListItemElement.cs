﻿namespace DCGWordConvert.Transform
{

    public class ListItemElement : DocumentElement
    {
        internal ListItemElement(bool isFauxBlock, int? continueAt) : base(DocumentElementType.ListItem)
        {
            IsFauxBlock = isFauxBlock;
            ContinueAt = continueAt;
        }

        public bool IsFauxBlock { get; }
        public int? ContinueAt { get; }

        public override string ToString() => $"<{Type} IsFauxBlock=\"{IsFauxBlock}\">{{{Children.Count}}}<{Type}>";
    }
}
