﻿using System;
using System.Collections.Generic;
using System.Linq;

using DCGWordConvert.Helpers;
using DCGWordConvert.Style;

namespace DCGWordConvert.Transform
{
    /// <summary>
    /// The base class of a component of a Word document
    /// </summary>
    public class DocumentElement
    {
        private readonly List<DocumentElement> _children;

        #region Constructor

        //internal DocumentElement(DocumentElement cloneFrom, DocumentElementType newType)
        //{
        //    Type = newType;
        //    _children = cloneFrom._children.Where(c => c != null && c.Parent == this).ToList();
        //    Parent = cloneFrom.Parent;
        //    LinkedStyle = cloneFrom.LinkedStyle;
        //    DefinedAnonymousStyle = cloneFrom.DefinedAnonymousStyle;
        //}

        internal DocumentElement(DocumentElementType type) 
        {
            Type = type;
            _children = new List<DocumentElement>();
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// The type of DocumentElement
        /// </summary>
        public DocumentElementType Type { get; }

        /// <summary>
        /// A readonly collection of Children of this DocumentElement in document order
        /// </summary>
        public IReadOnlyCollection<DocumentElement> Children => _children.Where(c => c != null && c.Parent == this).ToList();

        /// <summary>
        /// The parent of the DocumentElement, null if the DocumentElement is the root
        /// </summary>
        public DocumentElement Parent { get; private set; }

        /// <summary>
        /// The DocumentStyle which is linked to this element by name, comparable to a css class
        /// </summary>
        public DocumentStyle LinkedStyle { get; internal set; }

        /// <summary>
        /// The DocumentStyle which is defined directly on the element, comparable to the style attribute of an HTML element
        /// </summary>
        public DocumentStyle DefinedAnonymousStyle { get; internal set; }

        /// <summary>
        /// A DocumentStyle produced from merging the Linked and Defined Anonymous styles
        /// </summary>
        public DocumentStyle MergedStyle => DocumentStyle.Merge(DefinedAnonymousStyle, LinkedStyle);

        internal ListItemInfo ListItemInfo { get; set; }

        #endregion Properties

        #region Tree

        /// <summary>
        /// Walks up the DocumentElement tree starting at the current element's parent, returning the first item which matches the predicate
        /// </summary>
        /// <param name="predicate">A test to run for each ancestor in the DocumentElement's hierarchy</param>
        /// <returns>
        /// The first ancestor of DocumentElement which causes the predicate to return true,
        /// null if the DocumentElement is the root or no elements satisfy the predicate
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the predicate parameter is null</exception>
        public DocumentElement WalkParentsUntil(Func<DocumentElement, bool> predicate)
        {
            if (predicate is null) throw new ArgumentNullException(nameof(predicate));

            DocumentElement result = Parent;

            while (result != null)
            {
                if (predicate(result)) return result;
                result = result.Parent;
            }

            return null;
        }

        /// <summary>
        /// Walks up the DocumentElement tree starting at the current element's parent, returning the first item which is of the given type
        /// </summary>
        /// <param name="type">The type to compare an ancestor to</param>
        /// <returns>
        /// The first ancestor of DocumentElement which is of the requested type,
        /// null if the DocumentElement is the root or no elements are of the requested type
        /// </returns>
        public DocumentElement WalkParentsUntil(DocumentElementType type)
        {
            DocumentElement result = Parent;

            while (result != null)
            {
                if (result.Type == type) return result;
                result = result.Parent;
            }

            return null;
        }

        /// <summary>
        /// Gets the first sibling of the the given DocumentElement.
        /// If the DocumentElement has no parent or is the first child, null is returned
        /// </summary>
        public DocumentElement FirstSibling
        {
            get
            {
                DocumentElement result = Parent?.Children.FirstOrDefault();

                if (ReferenceEquals(result, this)) return null;

                return result;
            }
        }

        /// <summary>
        /// Gets the last sibling of the the given DocumentElement.
        /// If the DocumentElement has no parent or is the last child, null is returned
        /// </summary>
        public DocumentElement LastSibling
        {
            get
            {
                DocumentElement result = Parent?.Children.LastOrDefault();

                if (ReferenceEquals(result, this)) return null;

                return result;
            }
        }

        /// <summary>
        /// Gets the previous sibling of the the given DocumentElement.
        /// If the DocumentElement has no parent or is the first child, null is returned
        /// </summary>
        public DocumentElement PreviousSibling
        {
            get
            {
                if (Parent == null) return null;

                return Parent.Children.Reverse().SkipWhile(c => !ReferenceEquals(c, this)).Take(2).LastOrDefault();
            }
        }

        /// <summary>
        /// Gets the next sibling of the the given DocumentElement.
        /// If the DocumentElement has no parent or is the last child, null is returned
        /// </summary>
        public DocumentElement NextSibling
        {
            get
            {
                if (Parent == null) return null;

                return Parent.Children.SkipWhile(c => !ReferenceEquals(c, this)).Take(2).LastOrDefault();
            }
        }

        #endregion Tree

        internal void AddChild(DocumentElement newChild)
        {
            if (newChild is null) return;

            if (!ReferenceEquals(null, newChild.Parent))
            {
                throw new InvalidOperationException("An attempt was made to put an element under multiple parents producing a corrupt document");
            }

            newChild.Parent = this;

            _children.Add(newChild);
        }

        public override string ToString() => $"<{Type}>{{{Children.Count}}}</{Type}>";
    }
}
