﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.XPath;

using DCGWordConvert.Conversion;
using DCGWordConvert.Helpers;
using DCGWordConvert.Style;
using DCGWordConvert.Transform;

namespace DCGWordConvert
{
    /// <summary>
    /// Provides functionality for translating a Word document into a DocumentElement tree
    /// </summary>
    public static class DocumentReader
    {
        /// <summary>
        /// Reads a Word document from a stream
        /// </summary>
        /// <param name="stream">The stream to read from</param>
        /// <returns>A Word document</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the stream parameter is null</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if the various portions of the Word document can't be extracted</exception>
        /// <exception cref="System.InvalidOperationException">Thrown if various portions of the Word document can't be mapped</exception>
        public static Document ReadFrom(Stream stream)
        {
            const string DOCUMENT_DIRECTORY = "word";

            if (stream is null) throw new ArgumentNullException(nameof(stream));

            Manifest manifest = Manifest.FromCompressedStream(stream);

            var getDocumentResult = manifest.TryGetDocument($"{DOCUMENT_DIRECTORY}/_rels/document.xml.rels");

            if (!getDocumentResult.IsSuccess) throw new FileNotFoundException("Cannot find relationships mapping document");

            Dictionary<string, Reference> references = new Dictionary<string, Reference>();

            Reference styles = null;
            Reference numbering = null;

            foreach(Reference reference in Reference.FromXml(DOCUMENT_DIRECTORY, getDocumentResult.Document))
            {
                switch(reference.Type)
                {
                    case ReferenceType.Styles:
                        styles = reference;
                        break;

                    case ReferenceType.Numbering:
                        numbering = reference;
                        break;

                    case ReferenceType.Hyperlink:
                    case ReferenceType.Image:
                        references.Add(reference.ID, reference);
                        break;
                }
            }

            if(styles is null) throw new InvalidOperationException("Cannot find styles reference in relationships mapping");
            getDocumentResult = manifest.TryGetDocument(styles.Value);

            if (!getDocumentResult.IsSuccess) throw new FileNotFoundException("Cannot find styles mapping document");

            DefinedStylesLookup definedStyles = DefinedStylesLookup.FromXml(getDocumentResult.Document);

            NumberingLookup numberingLookup;

            if (!ReferenceEquals(numbering, null))
            {
                getDocumentResult = manifest.TryGetDocument(numbering.Value);
                if (numbering is null) throw new InvalidOperationException("Cannot find numbering reference in relationships mapping");
                if (!getDocumentResult.IsSuccess) throw new FileNotFoundException("Cannot find numbering mapping document");
                numberingLookup = NumberingLookup.FromXml(getDocumentResult.Document);
            }
            else
            {
                numberingLookup = null;
            }

            getDocumentResult = manifest.TryGetDocument($"{DOCUMENT_DIRECTORY}/document.xml");

            if (!getDocumentResult.IsSuccess) throw new FileNotFoundException("Cannot find actual document");

            DocumentElementBuilder builder = new DocumentElementBuilder(definedStyles, numberingLookup, references, manifest);

            DocumentElement bodyElement = builder.Process(getDocumentResult.Document.XPathSelectElement("/w:document/w:body", Namespaces.Resolver));

            return new Document(definedStyles, bodyElement, manifest);
        }

        /// <summary>
        /// Reads a Word document given its file name
        /// </summary>
        /// <param name="filename">The full name of the file to read</param>
        /// <returns>A Word document</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if the filename parameter is null</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if the various portions of the Word document can't be extracted</exception>
        /// <exception cref="System.InvalidOperationException">Thrown if various portions of the Word document can't be mapped</exception>
        public static Document ReadFrom(string filename)
        {
            if (filename is null) throw new ArgumentNullException(nameof(filename));

            FileInfo fi = new FileInfo(filename);

            if (!fi.Exists) throw new FileNotFoundException($"The file {filename} doesn't exist");

            using (FileStream fs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return ReadFrom(fs);
            }
        }
    }
}
