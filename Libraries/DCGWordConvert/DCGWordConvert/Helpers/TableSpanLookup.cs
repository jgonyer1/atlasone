﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace DCGWordConvert.Helpers
{
    internal class TableSpanLookup
    {
        #region Fields

        private readonly List<List<bool>> _cellMerge = new List<List<bool>>();

        #endregion Fields

        #region Factories

        internal TableSpanLookup(XElement tableElement)
        {
            foreach (XElement trElement in tableElement.Elements(Namespaces.W.tr))
            {
                List<bool> cells = new List<bool>();

                foreach (XElement tcElement in trElement.Elements(Namespaces.W.tc))
                {
                    XElement mergeElement = tcElement.Element(Namespaces.W.tcPr)?.Element(Namespaces.W.vMerge);
                    int gridSpan = int.Parse(tcElement.Element(Namespaces.W.tcPr)?.Element(Namespaces.W.gridSpan)?.Attribute(Namespaces.W.val).Value ?? "1");

                    bool isMerge = mergeElement != null && !string.Equals("restart", mergeElement.Attribute(Namespaces.W.val)?.Value);
                    
                    for (int i = 0; i < gridSpan; i++)
                    {
                        cells.Add(isMerge);
                    }
                }

                _cellMerge.Add(cells);
            }
        }

        #endregion Factories

        #region Getters

        public int RowSpan(int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || rowIndex >= _cellMerge.Count)
            {
                throw new ArgumentOutOfRangeException($"Row {rowIndex} does not exist");
            }

            List<bool> rowCells = _cellMerge[rowIndex];

            if (columnIndex < 0 || columnIndex >= rowCells.Count)
            {
                throw new ArgumentOutOfRangeException($"Column {columnIndex} does not exist in row {rowIndex}");
            }

            if (rowCells[columnIndex]) return 1;

            int result = 1;

            for(int r = rowIndex+1; r < _cellMerge.Count; r++)
            {
                if (! _cellMerge[r][columnIndex]) break;

                result++;
            }

            return result;
        }

        public bool DoSkip(int rowIndex, int columnIndex)
        {
            if(rowIndex < 0 || rowIndex >= _cellMerge.Count)
            {
                throw new ArgumentOutOfRangeException($"Row {rowIndex} does not exist");
            }

            List<bool> rowCells = _cellMerge[rowIndex];

            if (columnIndex < 0 || columnIndex >= rowCells.Count)
            {
                throw new ArgumentOutOfRangeException($"Column {columnIndex} does not exist in row {rowIndex}");
            }

            return rowCells[columnIndex];
        }

        #endregion Getters
    }
}
