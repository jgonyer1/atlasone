﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using DCGWordConvert.Style;

namespace DCGWordConvert.Helpers
{
    internal class NumberingLookup
    {
        private readonly Dictionary<int, List<DocumentStyle>> _abstractStyles = new Dictionary<int, List<DocumentStyle>>();
        private readonly Dictionary<int, int> _numberMapping = new Dictionary<int, int>();

        internal static NumberingLookup FromXml(XDocument document)
        {
            return new NumberingLookup(document.Elements().First());
        }

        private NumberingLookup(XElement numberingElement)
        {
            foreach (XElement abstractNumElement in numberingElement.Elements(Namespaces.W.abstractNum))
            {
                int id = int.Parse(abstractNumElement.Attribute(Namespaces.W.abstractNumId).Value);
                bool isRestartNumbering = string.Equals("1", abstractNumElement.Attribute(Namespaces.W15.restartNumberingAfterBreak)?.Value);

                List<DocumentStyle> abstractNumberingStyles = new List<DocumentStyle>();
                _abstractStyles.Add(id, abstractNumberingStyles);

                foreach (XElement lvlElement in abstractNumElement.Elements(Namespaces.W.lvl))
                {
                    int indentLevel = int.Parse(lvlElement.Attribute(Namespaces.W.ilvl).Value);

                    DocumentStyle numberStyle = DocumentStyle.FromXml(lvlElement);

                    numberStyle = DocumentStyle.Merge(numberStyle, DocumentStyle.FromXml(lvlElement.Element(Namespaces.W.pPr)));

                    abstractNumberingStyles.Insert(indentLevel, numberStyle);
                }
            }

            foreach(XElement numElement in numberingElement.Elements(Namespaces.W.num))
            {
                int sourceID = int.Parse(numElement.Attribute(Namespaces.W.numId).Value);
                int targetID = int.Parse(numElement.Element(Namespaces.W.abstractNumId).Attribute(Namespaces.W.val).Value);

                _numberMapping.Add(sourceID, targetID);
            }
        }

        public DocumentStyle GetStyleByIDAndIndentationLevel(int id, int indentationLevel)
        {
            if (indentationLevel < 0) throw new ArgumentOutOfRangeException("Indentation level cannot be less than 1");

            if (!_numberMapping.TryGetValue(id, out int abstractNumId))
                throw new KeyNotFoundException($"List reference {id} does not exist");

            if(!_abstractStyles.TryGetValue(abstractNumId, out List<DocumentStyle> styleDictionaries))
                throw new KeyNotFoundException($"List reference {id} does not exist");

            return styleDictionaries.Count > indentationLevel ? styleDictionaries[indentationLevel] : styleDictionaries.Last();
        }
    }
}
