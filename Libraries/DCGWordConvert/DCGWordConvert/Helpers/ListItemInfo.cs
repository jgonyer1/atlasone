﻿using System;
using DCGWordConvert.Style;

namespace DCGWordConvert.Helpers
{
    internal class ListItemInfo : IEquatable<ListItemInfo>
    {
        #region Constructor

        public ListItemInfo(int indentationLevel, int listID, bool isReset, DocumentStyle numberedListStyle)
        {
            IndentationLevel = indentationLevel;
            ListID = listID;
            IsReset = isReset;
            NumberedListStyle = numberedListStyle;
        }

        #endregion Constructor

        #region Properties

        public int IndentationLevel { get; }
        public int ListID { get; }
        public bool IsReset { get; }
        public DocumentStyle NumberedListStyle { get; }

        #endregion Properties

        #region Equality

        public bool Equals(ListItemInfo other)
        {
            if (other is null) return false;

            return ListID == other.ListID && IndentationLevel == other.IndentationLevel;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ListItemInfo);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (ListID * 397) ^ (IndentationLevel * 37);
            }
        }

        #endregion Equality

        public override string ToString() => $"{{{ListID}}} >> {IndentationLevel} [{(IsReset ? "R" : "")}]";
    }
}
