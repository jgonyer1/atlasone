﻿namespace DCGWordConvert.Helpers
{
    internal enum ReferenceType : byte
    {
        Image,
        Settings,
        Hyperlink,
        Styles,
        Numbering,
        Theme,
        WebSettings,
        FontTable
    }
}
