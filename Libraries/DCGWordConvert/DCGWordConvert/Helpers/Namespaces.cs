﻿using System.Xml;
using System.Xml.Linq;

namespace DCGWordConvert.Helpers
{
    internal static class Namespaces
    {
        public static readonly IXmlNamespaceResolver Resolver;

        static Namespaces()
        {
            XmlNamespaceManager resolver = new XmlNamespaceManager(new NameTable());

            resolver.AddNamespace(nameof(W).ToLower(), W.NS.NamespaceName);
            resolver.AddNamespace(nameof(R).ToLower(), R.NS.NamespaceName);
            resolver.AddNamespace(nameof(R2).ToLower(), R2.NS.NamespaceName);
            resolver.AddNamespace(nameof(A).ToLower(), A.NS.NamespaceName);
            resolver.AddNamespace(nameof(Pic).ToLower(), Pic.NS.NamespaceName);
            resolver.AddNamespace(nameof(V).ToLower(), V.NS.NamespaceName);
            resolver.AddNamespace(nameof(WP).ToLower(), WP.NS.NamespaceName);
            resolver.AddNamespace(nameof(W15).ToLower(), W15.NS.NamespaceName);

            Resolver = resolver;
        }

        public static class W
        {
            public static readonly XNamespace NS = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

            public static readonly XName styleId = NS + "styleId";
            public static readonly XName basedOn = NS + "basedOn";

            public static readonly XName p = NS + "p";
            public static readonly XName pPr = NS + "pPr";
            public static readonly XName pStyle = NS + "pStyle";

            public static readonly XName ind = NS + "ind";

            public static readonly XName r = NS + "r";
            public static readonly XName rPr = NS + "rPr";
            public static readonly XName rStyle = NS + "rStyle";

            public static readonly XName numPr = NS + "numPr";

            public static readonly XName b = NS + "b";
            public static readonly XName i = NS + "i";
            public static readonly XName u = NS + "u";
            public static readonly XName strike = NS + "strike";

            public static readonly XName sz = NS + "sz";
            public static readonly XName szCs = NS + "szCs";
            public static readonly XName w = NS + "w";

            public static readonly XName t = NS + "t";
            public static readonly XName tbl = NS + "tbl";
            public static readonly XName tblPr = NS + "tblPr";
            public static readonly XName tblStyle = NS + "tblStyle";
            public static readonly XName tblGrid = NS + "tblGrid";
            public static readonly XName gridCol = NS + "gridCol";
            public static readonly XName gridSpan = NS + "gridSpan";
            public static readonly XName vMerge = NS + "vMerge";
            public static readonly XName tblBorders = NS + "tblBorders";
            public static readonly XName tr = NS + "tr";
            public static readonly XName trPr = NS + "trPr";
            public static readonly XName trStyle = NS + "trStyle";
            public static readonly XName tc = NS + "tc";
            public static readonly XName tcPr = NS + "tcPr";
            public static readonly XName tcStyle = NS + "tcStyle";

            public static readonly XName top = NS + "top";
            public static readonly XName bottom = NS + "bottom";
            public static readonly XName left = NS + "left";
            public static readonly XName right = NS + "right";
            public static readonly XName hanging = NS + "hanging";

            public static readonly XName jc = NS + "jc";
            public static readonly XName lvlJc = NS + "lvlJc";
            public static readonly XName vAlign = NS + "vAlign";
            public static readonly XName vertAlign = NS + "vertAlign";

            public static readonly XName color = NS + "color";
            public static readonly XName shd = NS + "shd";
            public static readonly XName highlight = NS + "highlight";
            public static readonly XName fill = NS + "fill";

            public static readonly XName abstractNum = NS + "abstractNum";
            public static readonly XName abstractNumId = NS + "abstractNumId";
            public static readonly XName num = NS + "num";
            public static readonly XName numId = NS + "numId";
            public static readonly XName numFmt = NS + "numFmt";
            public static readonly XName start = NS + "start";
            public static readonly XName lvl = NS + "lvl";
            public static readonly XName ilvl = NS + "ilvl";

            public static readonly XName val = NS + "val";
            public static readonly XName name = NS + "name";
            public static readonly XName bookmarkStart = NS + "bookmarkStart";
        }

        public static class R
        {
            public static readonly XNamespace NS = "http://schemas.openxmlformats.org/package/2006/relationships";

            public static readonly XName Relationships = NS + "Relationships";
            public static readonly XName Relationship = NS + "Relationship";
        }

        public static class R2
        {
            public static readonly XNamespace NS = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";

            public static readonly XName embed = NS + "embed";
            public static readonly XName id = NS + "id";
        }

        public static class A
        {
            public static readonly XNamespace NS = "http://schemas.openxmlformats.org/drawingml/2006/main";
        }

        public static class Pic
        {
            public static readonly XNamespace NS = "http://schemas.openxmlformats.org/drawingml/2006/picture";
        }

        public static class V
        {
            public static readonly XNamespace NS = "urn:schemas-microsoft-com:vml";

            public static readonly XName shape = NS + "shape";
            public static readonly XName imagedata = NS + "imagedata";
            public static readonly XName id = NS + "id";
        }

        public static class WP
        {
            public static readonly XNamespace NS = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";
        }

        public static class W15
        {
            public static readonly XNamespace NS = "http://schemas.microsoft.com/office/word/2012/wordml";

            public static readonly XName restartNumberingAfterBreak = NS + "restartNumberingAfterBreak";
        }
    }
}