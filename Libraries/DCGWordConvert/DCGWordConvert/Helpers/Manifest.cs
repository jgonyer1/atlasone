﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

using Ionic.Zip;

namespace DCGWordConvert.Helpers
{
    internal class Manifest
    {
        #region Fields

        private readonly Dictionary<string, XDocument> _xmlManifest = new Dictionary<string, XDocument>();
        private readonly Dictionary<string, byte[]> _binaryManifest = new Dictionary<string, byte[]>();

        #endregion Fields

        #region Factory

        public static Manifest FromCompressedStream(Stream stream)
        {
            Dictionary<string, XDocument> xmlManifest = new Dictionary<string, XDocument>();
            Dictionary<string, byte[]> binaryManifest = new Dictionary<string, byte[]>();

            byte[] buffer = new byte[ushort.MaxValue];

            using (ZipFile zf = ZipFile.Read(stream))
            {
                foreach (ZipEntry ze in zf)
                {
                    using (Ionic.Crc.CrcCalculatorStream reader = ze.OpenReader())
                    {
                        string lowerFileName = ze.FileName.ToLowerInvariant();

                        if (lowerFileName.EndsWith(".xml") || lowerFileName.EndsWith(".xml.rels"))
                        {
                            xmlManifest.Add(ze.FileName, XDocument.Load(reader));
                        }
                        else
                        {
                            byte[] fileBytes = new byte[reader.Length];
                            int pos = 0;
                            int read;

                            while ((read = reader.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                Array.Copy(buffer, 0, fileBytes, pos, read);
                                pos += read;
                            }

                            binaryManifest.Add(ze.FileName, fileBytes);

                            bool gigalame = false;

                            if(gigalame)
                            {
                                using (FileStream fs = File.OpenWrite(@"C:\Users\JKeith\Desktop\Lame\lame.emf"))
                                {
                                    fs.Write(fileBytes, 0, fileBytes.Length);
                                }
                            }
                        }
                    }
                }
            }

            return new Manifest(xmlManifest, binaryManifest);
        }

        private Manifest(Dictionary<string, XDocument> xmlManifest, Dictionary<string, byte[]> binaryManifest)
        {
            _xmlManifest = xmlManifest;
            _binaryManifest = binaryManifest;
        }

        #endregion Factory

        #region Getters

        internal (bool IsSuccess, XDocument Document) TryGetDocument(string filename)
        {
            if (!_xmlManifest.TryGetValue(filename, out XDocument result))
                return (false, null);

            return (true, result);
        }

        internal (bool IsSuccess, Stream Stream) TryGetStream(string filename)
        {
            if (!_binaryManifest.TryGetValue(filename, out byte[] bytes))
                return (false, null);

            MemoryStream ms = new MemoryStream(bytes.Length);
            ms.Write(bytes, 0, bytes.Length);
            ms.Seek(0, SeekOrigin.Begin);

            return (true, ms);
        }

        #endregion Getters
    }
}
