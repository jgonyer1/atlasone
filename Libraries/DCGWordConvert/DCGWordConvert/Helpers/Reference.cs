﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace DCGWordConvert.Helpers
{
    internal class Reference : IEquatable<Reference>
    {
        #region Factories and Constructors

        public static IEnumerable<Reference> FromXml(string documentDirectory, XDocument document)
        {
            foreach(XElement relationship in document.Element(Namespaces.R.Relationships).Elements(Namespaces.R.Relationship))
            {
                string id = relationship.Attribute(XName.Get("Id")).Value;
                string target = relationship.Attribute(XName.Get("Target")).Value;

                ReferenceType type;

                switch (relationship.Attribute(XName.Get("Type")).Value)
                {
                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image":
                        type = ReferenceType.Image;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings":
                        type = ReferenceType.Settings;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink":
                        type = ReferenceType.Hyperlink;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles":
                        type = ReferenceType.Styles;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering":
                        type = ReferenceType.Numbering;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme":
                        type = ReferenceType.Theme;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings":
                        type = ReferenceType.WebSettings;
                        break;

                    case "http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable":
                        type = ReferenceType.FontTable;
                        break;

                    default:
                        continue;
                }

                if (!string.IsNullOrEmpty(documentDirectory) && type != ReferenceType.Hyperlink)
                {
                    target = documentDirectory + "/" + target;
                }

                yield return new Reference(type, id, target);
            }
        }

        public Reference(ReferenceType type, string id, string value)
        {
            Type = type;
            ID = id;
            Value = value;
        }

        #endregion Factories and Constructors

        #region Properties

        public ReferenceType Type { get; }
        public string ID { get; }
        public string Value { get; }

        #endregion Properties

        #region IEquatable

        public bool Equals(Reference other)
        {
            if (other is null) return false;

            return Type == other.Type && ID == other.ID && Value == other.Value;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Type.GetHashCode() * 11) ^ (ID.GetHashCode() * 37) ^ Value.GetHashCode();
            }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as Reference);
        }

        #endregion IEquatable
    }
}
