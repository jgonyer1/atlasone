﻿using System;
using System.Drawing;

namespace DCGWordConvert.Helpers
{
    public static class ImageHelpers
    {
        public static Size GetConstrainedSize(Size original, int maxDimension)
        {
            if (maxDimension <= 0)
            {
                throw new ArgumentOutOfRangeException($"{maxDimension} parameter must be a positive number");
            }

            double widthRatio = original.Width < maxDimension ? 1 : maxDimension / (double)original.Width;
            double heightRatio = original.Height < maxDimension ? 1 : maxDimension / (double)original.Height;

            int newWidth;
            int newHeight;

            if (widthRatio < heightRatio)
            {
                newWidth = maxDimension;
                newHeight = (int)(widthRatio * original.Height);
            }
            else if (heightRatio <= widthRatio && heightRatio < 1)
            {
                newHeight = maxDimension;
                newWidth = (int)(heightRatio * original.Width);
            }
            else
            {
                newWidth = original.Width;
                newHeight = original.Height;
            }

            return new Size(newWidth, newHeight);
        }
    }
}
