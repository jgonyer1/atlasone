﻿using System;
using System.Collections.Generic;

namespace DCGWordConvert.Helpers
{
    internal static class DictionaryExtensions
    {
        public static Dictionary<TKey, TValue> MergeWith<TKey, TValue>(this Dictionary<TKey, TValue> @base, Dictionary<TKey, TValue> @override)
        {
            if (@base is null && @override is null) return null;

            Dictionary<TKey, TValue> result = new Dictionary<TKey, TValue>(@base ?? @override);

            if (@base is null || @override is null) return result;

            foreach (KeyValuePair<TKey, TValue> kvp in @override)
            {
                if (result.ContainsKey(kvp.Key)) continue;

                result.Add(kvp.Key, kvp.Value);
            }

            return result;
        }
    }
}