﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DCGWordConvert.Helpers
{
    internal static class EnumerableExtensions
    {
        #region GroupByContiguous

        private class Grouping<TKey, TValue> : IGrouping<TKey, TValue>
        {
            private readonly List<TValue> _values;

            public Grouping(TKey key, IEnumerable<TValue> values)
            {
                Key = key;
                _values = values.ToList();
            }

            public TKey Key { get; }

            public IEnumerator<TValue> GetEnumerator() => _values.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public static IEnumerable<IReadOnlyCollection<TValue>> GroupByContiguous<TValue>(this IEnumerable<TValue> source, IEqualityComparer<TValue> equalityComparer)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (equalityComparer is null) throw new ArgumentNullException(nameof(equalityComparer));

            List<TValue> currentGrouping = null;

            IEnumerator<TValue> enumerator = source.GetEnumerator();

            while(enumerator.MoveNext())
            {
                if(currentGrouping is null)
                {
                    currentGrouping = new List<TValue> { enumerator.Current };
                    continue;
                }

                if(equalityComparer.Equals(currentGrouping.Last(), enumerator.Current))
                {
                    currentGrouping.Add(enumerator.Current);
                    continue;
                }

                yield return currentGrouping;
                currentGrouping = new List<TValue> { enumerator.Current };
            }

            if(! ReferenceEquals(currentGrouping, null)) yield return currentGrouping;
        }

        public static IEnumerable<IGrouping<TKey, TValue>> GroupByContiguous<TKey, TValue>(this IEnumerable<TValue> source, Func<TValue, TKey> grouper, IEqualityComparer<TKey> equalityComparer = null)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (grouper is null) throw new ArgumentNullException(nameof(grouper));

            TKey currentKey = default(TKey);
            List<TValue> currentGrouping = null;

            Func<TKey, TKey, bool> localComparer;

            if (equalityComparer is null)
            {
                localComparer = (x, y) => ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
            }
            else
            {
                localComparer = (x, y) => equalityComparer.Equals(x, y);
            }

            IEnumerator<TValue> enumerator = source.GetEnumerator();

            while (enumerator.MoveNext())
            {
                TKey nextKey = grouper(enumerator.Current);

                if (currentGrouping is null)
                {
                    currentKey = nextKey;
                    currentGrouping = new List<TValue> { enumerator.Current };
                    continue;
                }

                if(localComparer(currentKey, nextKey))
                {
                    currentGrouping.Add(enumerator.Current);
                    continue;
                }

                yield return new Grouping<TKey, TValue>(currentKey, currentGrouping);

                currentKey = nextKey;
                currentGrouping = new List<TValue> { enumerator.Current };
            }

            if (ReferenceEquals(currentGrouping, null)) yield break;

            yield return new Grouping<TKey, TValue>(currentKey, currentGrouping);
        }

        #endregion GroupByContiguous

        #region ForEach

        public static void ForEach<TValue>(this IEnumerable<TValue> source, Action<TValue> operation)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (operation is null) throw new ArgumentNullException(nameof(operation));

            foreach(TValue item in source)
            {
                operation(item);
            }
        }

        public static void ForEach<TValue>(this IEnumerable<TValue> source, Action<TValue, int> operation)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (operation is null) throw new ArgumentNullException(nameof(operation));

            int index = 0;

            foreach (TValue item in source)
            {
                operation(item, index);
                index++;
            }
        }

        public static void ForEach<TValue>(this IEnumerable<TValue> source, Action<TValue, int, bool> operation)
        {
            if (source is null) throw new ArgumentNullException(nameof(source));
            if (operation is null) throw new ArgumentNullException(nameof(operation));

            IEnumerator<TValue> enumerator = source.GetEnumerator();

            bool hasItems = enumerator.MoveNext();

            if (!hasItems) return;

            TValue previous = enumerator.Current;

            int index = 0;

            while(enumerator.MoveNext())
            {
                operation(previous, index, false);
                previous = enumerator.Current;
                index++;
            }

            operation(previous, index, true);
        }

        #endregion ForEach
    }
}