﻿using System.Data.Entity;
using Atlas.Global.Data.Mapping;
using Atlas.Global.Data.Migrations;
using Atlas.Global.Model;

namespace Atlas.Global.Data
{
    public class GlobalContext : DbContext
    {
        public DbSet<Consolidation> Consolidations { get; set; }
        public DbSet<Model.Institution> Institutions { get; set; }
        public DbSet<Package> Packages { get; set; }

        public DbSet<Report> Reports { get; set; }
        public DbSet<Footnote> Footnotes { get; set; }
        public DbSet<OneChart> OneCharts { get; set; }
        public DbSet<TwoChart> TwoCharts { get; set; }
        public DbSet<SimCompare> SimCompares { get; set; }
        public DbSet<Eve> Eves { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<BalanceSheetCompare> BalanceSheetCompares { get; set; }
        public DbSet<BalanceSheetMix> BalanceSheetMixes { get; set; }
        public DbSet<StaticGap> StaticGaps { get; set; }
        public DbSet<FundingMatrix> FundingMatrices { get; set; }
        public DbSet<LoanCapFloor> LoanCapFloors { get; set; }
        public DbSet<SplitFootnote> SplitFootnotes { get; set; }
        public DbSet<TimeDepositMigration> TimeDepositMigrations { get; set; }
        public DbSet<CoreFundingUtilization> CoreFundingUtilizations { get; set; }
        public DbSet<YieldCurveShift> YieldCurveShifts { get; set; }

        public DbSet<SimulationType> SimulationTypes { get; set; }
        public DbSet<ScenarioType> ScenarioTypes { get; set; }
        public DbSet<AccountingType> AccountingTypes { get; set; }
        public DbSet<BalanceSheetGroup> BalanceSheetGroups { get; set; }
        public DbSet<PeriodSmoother> PeriodSmoother { get; set; }
        public DbSet<AccountTypeOrder> AccountTypeOrder { get; set; }
        public DbSet<ClassificationOrder> ClassificationOrder { get; set; }
        public DbSet<CoreFundingLookup> CoreFundingLookup { get; set; }
        public DbSet<WebRateScenarioType> WebRateScenarioTypes { get; set; }
      
        public DbSet<File> Files { get; set; }


        public GlobalContext() : base() { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new AtlasDbInitializer());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GlobalContext, Configuration>());
            modelBuilder.Configurations.Add(new ScenarioTypeMap());
            modelBuilder.Configurations.Add(new SimulationTypeTypeMap());
            modelBuilder.Configurations.Add(new AccountingTypeMap());
            modelBuilder.Configurations.Add(new BalanceSheetGroupMap());
            modelBuilder.Configurations.Add(new TwoChartMap());
            modelBuilder.Configurations.Add(new SimCompareMap());
            modelBuilder.Configurations.Add(new ConsolidationInstitutionMap());
            modelBuilder.Configurations.Add(new ChartMap());
            modelBuilder.Configurations.Add(new BalanceSheetCompareMap());
            modelBuilder.Configurations.Add(new StaticGapMap());
            modelBuilder.Configurations.Add(new TimeDepositMigrationMap());
            modelBuilder.Configurations.Add(new FundingMatrixMap());
            modelBuilder.Configurations.Add(new CoreFundingUtilizationMap());
            modelBuilder.Configurations.Add(new YieldCurveShiftMap());
            modelBuilder.Configurations.Add(new PeriodSmootherMap());
            modelBuilder.Configurations.Add(new AccountTypeOrderMap());
            modelBuilder.Configurations.Add(new ClassificationOrderMap());
            modelBuilder.Configurations.Add(new CoreFundingLookupMap());
            modelBuilder.Configurations.Add(new WebRateScenarioTypeMap());
        }
    }
}


