using System;
using System.Collections.Generic;

namespace Atlas.Users.Data.Models
{
    public partial class SecurityInfo
    {
        public string userID { get; set; }
        public string DBName { get; set; }
        public Nullable<int> Analyst { get; set; }
    }
}
