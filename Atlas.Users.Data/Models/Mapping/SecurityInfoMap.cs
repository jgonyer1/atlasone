using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Atlas.Users.Data.Models.Mapping
{
    public class SecurityInfoMap : EntityTypeConfiguration<SecurityInfo>
    {
        public SecurityInfoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.userID, t.DBName });

            // Properties
            this.Property(t => t.userID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DBName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SecurityInfo");
            this.Property(t => t.userID).HasColumnName("userID");
            this.Property(t => t.DBName).HasColumnName("DBName");
            this.Property(t => t.Analyst).HasColumnName("Analyst");
        }
    }
}
