using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Atlas.Users.Data.Models.Mapping;

namespace Atlas.Users.Data.Models
{
    public partial class DDWMContext : DbContext
    {
        static DDWMContext()
        {
            Database.SetInitializer<DDWMContext>(null);
        }

        public DDWMContext() : base() { }

        public DbSet<SecurityInfo> SecurityInfoes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SecurityInfoMap());
        }
    }
}
