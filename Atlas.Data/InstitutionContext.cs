﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using Atlas.Institution.Data.Mapping;
using Atlas.Institution.Model;
using Atlas.Institution.Model.Atlas;
using Atlas.Institution.Model.Helpers;

namespace Atlas.Institution.Data
{
    public class InstitutionContext : DbContext
    {
        //atlas data
        public DbSet<Information> Information { get; set; }
        public DbSet<Simulation> Simulations { get; set; }

        //lookup tables for basis
        public DbSet<CategoryType> CategoryTypes { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<Classification> Classifications { get; set; }

        //basis data
        public DbSet<Scenario> Scenarios { get; set; }

        public InstitutionContext() : base() { }
        public InstitutionContext(string connectionString) :
            base(connectionString) { }


        //Previously Global Data
        public DbSet<Package> Packages { get; set; }

        public DbSet<Report> Reports { get; set; }
        public DbSet<Footnote> Footnotes { get; set; }
        public DbSet<OneChart> OneCharts { get; set; }
        public DbSet<TwoChart> TwoCharts { get; set; }
        public DbSet<SimCompare> SimCompares { get; set; }
        public DbSet<SimCompareSimulationType> SimCompareSimulationTypes { get; set; }
        public DbSet<Eve> Eves { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<BalanceSheetCompare> BalanceSheetCompares { get; set; }
        public DbSet<BalanceSheetMix> BalanceSheetMixes { get; set; }
        public DbSet<BasicSurplusReport> BasicSurplusReports { get; set; }
        public DbSet<InterestRatePolicyGuidelines> InterestRatePolicyGuidelines { get; set; }
        public DbSet<StaticGap> StaticGaps { get; set; }
        public DbSet<SimulationSummary> SimulationSummaries { get; set; }
        public DbSet<FundingMatrix> FundingMatrices { get; set; }
        public DbSet<LoanCapFloor> LoanCapFloors { get; set; }
        public DbSet<SplitFootnote> SplitFootnotes { get; set; }
        public DbSet<TimeDepositMigration> TimeDepositMigrations { get; set; }
        public DbSet<CoreFundingUtilization> CoreFundingUtilizations { get; set; }
        public DbSet<CoreFundingLookup> CoreFundingLookups { get; set; }
        public DbSet<YieldCurveShift> YieldCurveShifts { get; set; }
        public DbSet<HistoricalBalanceSheetNII> HistoricalBalanceSheetNIIs { get; set; }
        public DbSet<HistoricalBalanceSheetNIISimulation> HistoricalBalanceSheetNIISimulation { get; set; }
        public DbSet<HistoricalBalanceSheetNIIScenario> HistoricalBalanceSheetNIIScenario { get; set; }
        public DbSet<HistoricalBalanceSheetNIITopFootNotes> HistoricalBalanceSheetNIIsTopFootNotes { get; set; }
        public DbSet<HistoricalBalanceSheetNIIBottomFootNotes> HistoricalBalanceSheetNIIsBottomFootNotes { get; set; }
        public DbSet<PrepaymentDetails> PrepaymentDetails { get; set; }
        public DbSet<SummaryOfResults> SummaryOfResults { get; set; }
        public DbSet<MarginalCostOfFunds> MarginalCostOfFunds { get; set; }


        public DbSet<SimulationType> SimulationTypes { get; set; }
        public DbSet<ScenarioType> ScenarioTypes { get; set; }
        public DbSet<AccountingType> AccountingTypes { get; set; }
        public DbSet<BalanceSheetGroup> BalanceSheetGroups { get; set; }
        public DbSet<PeriodSmoother> PeriodSmoother { get; set; }
        public DbSet<AccountTypeOrder> AccountTypeOrder { get; set; }
        public DbSet<ClassificationOrder> ClassificationOrder { get; set; }
        public DbSet<WebRateScenarioType> WebRateScenarioTypes { get; set; }

        //Defined Grouping Junk
        public DbSet<DefinedGrouping> DefinedGroupings { get; set; }
        public DbSet<DefinedGroupingGroup> DefinedGroupingGroups { get; set; }
        public DbSet<DefinedGroupingClassification> DefinedGroupingClassifications { get; set; }

        //Basic Surplus Tables
        public DbSet<BasicSurplusMarketValue> BasicSurplusMarketValues { get; set; }
        public DbSet<BasicSurplusCollateralType> BasicSurplusCollateralTypes { get; set; }
        public DbSet<BasicSurplusSecuredLiabilitiesType> BasicSurplusSecuredLiabilitiesTypes { get; set; }
        public DbSet<CollateralType> CollateralTypes { get; set; }
        public DbSet<SecuredLiabilitiesType> SecuredLiabilitiesTypes { get; set; }
        public DbSet<BasicSurplusAdmin> BasicSurplusAdmins { get; set; }
        public DbSet<BasicSurplusAdminBorrowingCapacity> BasicSurplusAdminBorrowingCapacities { get; set; }
        public DbSet<BasicSurplusAdminBrokeredDeposit> BasicSurplusAdminBrokeredDeposits { get; set; }
        public DbSet<BasicSurplusAdminFootnote> BasicSurplusAdminFootnotes { get; set; }
        public DbSet<BasicSurplusAdminLiquidAsset> BasicSurplusAdminLiquidAssets { get; set; }
        public DbSet<BasicSurplusAdminLoanCollateral> BasicSurplusAdminLoanCollaterals { get; set; }
        public DbSet<BasicSurplusAdminLoan> BasicSurplusAdminLoans { get; set; }
        public DbSet<BasicSurplusAdminMisc> BasicSurplusAdminMiscs { get; set; }
        public DbSet<BasicSurplusAdminOtherLiquidity> BasicSurplusAdminOtherLiquidities { get; set; }
        public DbSet<BasicSurplusAdminOtherSecured> BasicSurplusAdminOtherSecureds { get; set; }
        public DbSet<BasicSurplusAdminOvernightFund> BasicSurplusAdminOvernightFunds { get; set; }
        //public DbSet<BasicSurplusAdminRow> BasicSurplusAdminRows { get; set; } //Not directly used, just a base class for other "row" types
        public DbSet<BasicSurplusAdminSecuredBorrowing> BasicSurplusAdminSecuredBorrowings { get; set; }
        public DbSet<BasicSurplusAdminSecurityCollateral> BasicSurplusAdminSecurityCollaterals { get; set; }
        public DbSet<BasicSurplusAdminUnsecuredBorrowing> BasicSurplusAdminUnsecuredBorrowings { get; set; }
        public DbSet<BasicSurplusAdminVolatileLiability> BasicSurplusAdminVolatileLiabilities { get; set; }

        //Lookback Tables
        public DbSet<LBCustomLayout> LBCustomLayouts { get; set; }
        public DbSet<LBCustomLayoutRowItem> LBCustomLayoutRowItems { get; set; }
        public DbSet<LBCustomLayoutClassification> LBCustomLayoutClassifications { get; set; }
        public DbSet<LBCustomType> LBCustomTypes { get; set; }
        public DbSet<LBLookback> LBLookbacks { get; set; }
        public DbSet<LBLookbackGroup> LBLookbackGroup { get; set; }
        public DbSet<LBLookbackData> LBLookbackData { get; set; }
        public DbSet<LookbackReport> LookbackReport { get; set; }
        public DbSet<LookbackReportSection> LookbackReportSection { get; set; }
        public DbSet<LookbackType> LookbackType { get; set; }

        //Executive Risk Summary
        public DbSet<ExecutiveRiskSummary> ExecutiveRiskSummaries { get; set; }
        public DbSet<ExecutiveRiskSummaryIRRSection> ExecutiveRiskSummaryIRRSections { get; set; }
        public DbSet<ExecutiveRiskSummaryIRRDet> ExecutiveRiskSummaryIRRDets { get; set; }
        public DbSet<ExecutiveRiskSummaryIRRScenario> ExecutiveRiskSummaryIRRScenarios { get; set; }

        //Look Up For Balance Sheet Mix
        public DbSet<BalanceSheetMixLookup> BalanceSheetMixLookups { get; set; }


        //Cover Page
        public DbSet<CoverPage> CoverPages { get; set; }

        //Eve Assumptions
        public DbSet<EveNevAssumptions> EveNevAssumptions { get; set; }

        //Cashflow Report
        public DbSet<CashflowReport> CashflowReports { get; set; }
        //Net Cashflow Report
        public DbSet<NetCashflow> NetCashflows { get; set; }
        //Summary Rate
        public DbSet<SummaryRate> SummaryRates { get; set; }
        //Summary Results
        //public DbSet<SummaryOfResults> SummaryResults { get; set; }
        //ASC825 Report
        public DbSet<ASC825BalanceSheet> ASC825BalanceSheets { get; set; }
        //ASC825 Datasources
        public DbSet<ASC825DataSource> ASC825DataSources { get; set; }
        //ASC 825 Discount Rate
        public DbSet<ASC825DiscountRate> ASC825DiscountRates { get; set; }
        //ASC 825 Worksheet
        public DbSet<ASC825Worksheet> ASC825Worksheets { get; set; }
        //ASC 825 MarketValue 
        public DbSet<ASC825MarketValue> ASC825MarketValues { get; set; }
        //Rate Change Matrix
        public DbSet<RateChangeMatrix> RateChangeMatrices { get; set; }
        //Investment Portfolio Valuation
        public DbSet<InvestmentPortfolioValuation> InvestmentPortfolioValuations { get; set; }
        //Investment Detail
        public DbSet<InvestmentDetail> InvestmentDetails { get; set; }
        //Investment Errors
        public DbSet<InvestmentError> InvestmentErrors { get; set; }
        //Time Deposit Groupings
        public DbSet<TimeDepositGrouping> TimeDepositGroupings { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<FileSummary> FileSummaries { get; set; }

        //Policy Junk
        public DbSet<Policy> Policies { get; set; }
        public DbSet<LiquidityPolicy> LiquidityPolicies { get; set; }
        public DbSet<LiquidityPolicySetting> LiquidityPolicySettings { get; set; }

        //Model Setup
        public DbSet<ModelSetup> ModelSetups { get; set; }

        //ASC825 Assumption Methodoligies
        public DbSet<ASC825AssumptionMethods> ASC825AssumptionMethods { get; set; }

        //Assumption Methods
        public DbSet<AssumptionMethods> AssumptionMethods { get; set; }

        //Liability Pricing
        public DbSet<LiabilityPricing> LiabilityPricings { get; set; }

        //Liability Pricing Analysis
        public DbSet<LiabilityPricingAnalysis> LiabilityPricingAnalysis { get; set; }

        //Liquidy Projection
        public DbSet<LiquidityProjection> LiquidityProjections { get; set; }

        //Detailed Simulaiton Assumptions
        public DbSet<DetailedSimulationAssumption> DetailedSimulationAssumptions { get; set; }

        //NII REcon
        public DbSet<NIIRecon> NIIRecons { get; set; }
        public DbSet<NIIReconOrder> NIIReconOrder { get; set; }

        //Capital Analysis
        public DbSet<CapitalAnalysis> CapitalAnalysis { get; set; }
        public DbSet<SectionPage> SectionPages { get; set; }

        public DbSet<NCUAHelper> NCUAHelper { get; set; }

        public DbSet<TimeDepositMigrationDefaultView> TimeDepositMigrationDefaultView { get; set; }

        public DbSet<TimeDepositMigrationTermBuckets> TimeDepositMigrationTermBuckets { get; set; }


        public DbSet<InventoryLiquidityResources> InventoryLiquidityResources { get; set; }
        public DbSet<InventoryLiquidityResourcesTier> InventoryLiquidityResourcesTier { get; set; }
        public DbSet<InventoryLiquidityResourcesTierDetail> InventoryLiquidityResourcesTierDetail { get; set; }
        public DbSet<InventoryLiquidityResourcesTierDetailCollection> InventoryLiquidityResourcesTierDetailCollection { get; set; }

        // https://stackoverflow.com/questions/3504660/decimal-precision-and-scale-in-ef-code-first
        private void setModelPrecision(DbModelBuilder modelBuilder)
        {
            // for all classes in the same assembly as DecimalPrecisionAttribute ...
            foreach (Type classType in Assembly.GetAssembly(typeof(DecimalPrecisionAttribute)).GetTypes().Where(t => t.IsClass))
            {
                // ... for all public instance properties which have a DecimalPrecisionAttribute
                foreach (PropertyInfo property in classType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    DecimalPrecisionAttribute decimalPrecisionAttribute;
                    
                    try
                    {
                        decimalPrecisionAttribute = property.GetCustomAttribute<DecimalPrecisionAttribute>(true);
                    }
                    catch(ArgumentOutOfRangeException aoorex)
                    {
                        throw new InvalidOperationException(
                            "Cannot apply DecimalPrecisionAttribute to property " + classType.FullName + "." + property.Name +
                            " during DbContext.OnModelCreating because " + aoorex.Message//,
                         //   aoorex
                        );
                    }

                    if (ReferenceEquals(null, decimalPrecisionAttribute)) continue;

                    Type decimalType = property.PropertyType;

                    if(decimalType != typeof(decimal) && decimalType != typeof(decimal?))
                    {
                        throw new NotSupportedException(
                            "Cannot apply DecimalPrecisionAttribute to property " + classType.FullName + "." + property.Name +
                            " during DbContext.OnModelCreating because the property does not return one of: decimal, decimal?"
                        );
                    }

                    // EntityTypeConfiguration<CLASSTYPE> modelBuilder.Entity<CLASSTYPE>()
                    object entityConfig = modelBuilder.GetType().GetMethod("Entity").MakeGenericMethod(classType).Invoke(modelBuilder, null);

                    // c => c.{PROPERTYNAME}
                    ParameterExpression paramExpression = Expression.Parameter(classType, "c");
                    Expression propertyExpression = Expression.Property(paramExpression, property.Name);
                    LambdaExpression lambdaExpression = Expression.Lambda(propertyExpression, true, new ParameterExpression[] { paramExpression });

                    // Expression<Func<{CLASSTYPE}, {DECIMALTYPE}>>
                    Type expectedParameterType = typeof(Expression<>).MakeGenericType(typeof(Func<,>).MakeGenericType(classType, decimalType));

                    // DecimalPropertyConfiguration EntityTypeConfiguration<CLASSTYPE>.Property(Expression<Func<{CLASSTYPE}, {DECIMALTYPE}>>)
                    MethodInfo propertyMethod = entityConfig.GetType().GetMethod("Property", new[] { expectedParameterType });

                    DecimalPropertyConfiguration propertyConfiguration = propertyMethod.Invoke(entityConfig, new[] { lambdaExpression }) as DecimalPropertyConfiguration;
                    propertyConfiguration.HasPrecision(decimalPrecisionAttribute.Precision, decimalPrecisionAttribute.Scale);
                }
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<InstitutionContext, Configuration>());
            modelBuilder.Configurations.Add(new AccountTypeMap());
            modelBuilder.Configurations.Add(new CategoryTypeMap());
            modelBuilder.Configurations.Add(new ClassificationMap());
            modelBuilder.Configurations.Add(new ScenarioMap());

            //Previous global Data
            modelBuilder.Configurations.Add(new ScenarioTypeMap());
            modelBuilder.Configurations.Add(new SimulationTypeTypeMap());
            modelBuilder.Configurations.Add(new AccountingTypeMap());
            modelBuilder.Configurations.Add(new BalanceSheetGroupMap());
            modelBuilder.Configurations.Add(new TwoChartMap());
            modelBuilder.Configurations.Add(new SimCompareMap());
            modelBuilder.Configurations.Add(new ChartSimulationTypeMap());
            modelBuilder.Configurations.Add(new ConsolidationInstitutionMap());
            modelBuilder.Configurations.Add(new ChartMap());
            modelBuilder.Configurations.Add(new BalanceSheetCompareMap());
            modelBuilder.Configurations.Add(new InterestRatePolicyGuidelinesDateMap());
            modelBuilder.Configurations.Add(new StaticGapMap());
            modelBuilder.Configurations.Add(new TimeDepositMigrationMap());
            modelBuilder.Configurations.Add(new FundingMatrixMap());
            modelBuilder.Configurations.Add(new CoreFundingUtilizationMap());
            modelBuilder.Configurations.Add(new CoreFundingLookupMap());
            modelBuilder.Configurations.Add(new YieldCurveShiftMap());
            modelBuilder.Configurations.Add(new PeriodSmootherMap());
            modelBuilder.Configurations.Add(new AccountTypeOrderMap());
            modelBuilder.Configurations.Add(new ClassificationOrderMap());
            modelBuilder.Configurations.Add(new WebRateScenarioTypeMap());
            modelBuilder.Configurations.Add(new HistoricalBalanceSheetNIIMap());

            //Basic Surplus Junk
            modelBuilder.Configurations.Add(new BasicSurplusCollateralTypeMap());

            modelBuilder.Configurations.Add(new BalanceSheetMixLookupMap());

            modelBuilder.Configurations.Add(new PolicyMap());

            //New Mappings For Report Inheritence spoofing
            modelBuilder.Configurations.Add(new ASC825WorksheetMap());
            modelBuilder.Configurations.Add(new ASC825AssumptionMethodsMap());
            modelBuilder.Configurations.Add(new ASC825BalanceSheetMap());
            modelBuilder.Configurations.Add(new ASC825DataSourceMap());
            modelBuilder.Configurations.Add(new ASC825DiscountRateMap());
            modelBuilder.Configurations.Add(new AssumptionMethodsMap());
            modelBuilder.Configurations.Add(new BalanceSheetMixMap());
            modelBuilder.Configurations.Add(new BasicSurplusReportMap());
            modelBuilder.Configurations.Add(new CapitalAnalysisMap());
            modelBuilder.Configurations.Add(new CashflowReportMap());
            modelBuilder.Configurations.Add(new DetailedSimulationAssumptionMap());
            modelBuilder.Configurations.Add(new DocumentMap());
            modelBuilder.Configurations.Add(new EveMap());
            modelBuilder.Configurations.Add(new InvestmentDetailMap());
            modelBuilder.Configurations.Add(new InvestmentErrorMap());
            modelBuilder.Configurations.Add(new InvestmentPortfolioValuationMap());
            modelBuilder.Configurations.Add(new LiabilityPricingAnalysisMap());
            modelBuilder.Configurations.Add(new LiquidityProjectionMap());
            modelBuilder.Configurations.Add(new LiquidityProjectionDateOffsetMap());
            modelBuilder.Configurations.Add(new LoanCapFloorMap());

            modelBuilder.Configurations.Add(new NIIReconMap());
            modelBuilder.Configurations.Add(new NetCashflowMap());
            modelBuilder.Configurations.Add(new OneChartMap());
            modelBuilder.Configurations.Add(new PrepaymentDetailsMap());
            modelBuilder.Configurations.Add(new RateChangeMatrixMap());
            modelBuilder.Configurations.Add(new ASC825MarketValueMap());
            modelBuilder.Configurations.Add(new SummaryRateMap());
            modelBuilder.Configurations.Add(new SimulationSummaryMap());
            modelBuilder.Configurations.Add(new CoverPageMap());
            modelBuilder.Configurations.Add(new SummaryOfResultsMap());
            modelBuilder.Configurations.Add(new EveNevAssumptionsMap());
            modelBuilder.Configurations.Add(new SectionPageMap());
            modelBuilder.Configurations.Add(new InterestRatePolicyGuidelinesMap());

            modelBuilder.Configurations.Add(new LookbackReportMap());
            modelBuilder.Configurations.Add(new ExecutiveRiskSummaryMap());
            modelBuilder.Configurations.Add(new ExecutiveRiskSummaryIRRScenarioMap());
            modelBuilder.Configurations.Add(new ExecutiveRiskSummaryDateOffsetMap());
            modelBuilder.Configurations.Add(new MarginalCostOfFundsMap());

            modelBuilder.Configurations.Add(new InventoryLiquidityResourcesMap());

            modelBuilder.Configurations.Add(new FileSummaryMap());

            setModelPrecision(modelBuilder);
        }
    }
}