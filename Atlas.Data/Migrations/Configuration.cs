﻿using System;
using System.Data.Entity.Migrations;
using Atlas.Institution.Model;
using System.Text;
using System.Data.SqlClient;

namespace Atlas.Institution.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<InstitutionContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(InstitutionContext context)
        {
            context.CategoryTypes.AddOrUpdate(b => b.Id,
                new CategoryType { Id = 0, ShortName = "asset", Name = "Asset" },
                new CategoryType { Id = 1, ShortName = "liability", Name = "Liability" },
                new CategoryType { Id = 2, ShortName = "total1", Name = "I1 Intermediate Total" },
                new CategoryType { Id = 3, ShortName = "total2", Name = "I2 Intermediate Total" },
                new CategoryType { Id = 4, ShortName = "total3", Name = "I3 Intermediate Total" },
                new CategoryType { Id = 5, ShortName = "sub", Name = "Sub Account" },
                new CategoryType { Id = 6, ShortName = "master", Name = "Master Account" },
                new CategoryType { Id = 7, ShortName = "grandtotal", Name = "Grand Total" }
                );

            context.AccountTypes.AddOrUpdate(c => c.Id,
                //Assets
                new AccountType { Id = 000, Name = "Other" },
                new AccountType { Id = 001, Name = "Investment" },
                new AccountType { Id = 002, Name = "Loan" },
                new AccountType { Id = 003, Name = "Non-Maturity Deposit" },
                new AccountType { Id = 004, Name = "Equity" },
                new AccountType { Id = 005, Name = "Capital Debt" },
                new AccountType { Id = 006, Name = "Borrowing" },
                new AccountType { Id = 007, Name = "Off Balance Sheet (A)" },
                new AccountType { Id = 008, Name = "Time Deposit" },
                new AccountType { Id = 009, Name = "Off Balance Sheet (L)" }
                );

            context.Classifications.AddOrUpdate(s => new { s.IsAsset, s.AccountTypeId, s.RbcTier },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 000, BalanceSheetGroupId = 001, Name = "Other Investment" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 001, BalanceSheetGroupId = 001, Name = "Agency" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 002, BalanceSheetGroupId = 001, Name = "Corporate" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 003, BalanceSheetGroupId = 001, Name = "CMO" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 004, BalanceSheetGroupId = 001, Name = "MBS" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 005, BalanceSheetGroupId = 001, Name = "Muni" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 006, BalanceSheetGroupId = 001, Name = "Overnight" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 007, BalanceSheetGroupId = 001, Name = "Stock" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 008, BalanceSheetGroupId = 001, Name = "Treasury" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 009, BalanceSheetGroupId = 001, Name = "Trust Preferred" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 010, BalanceSheetGroupId = 001, Name = "Unrealized G/L" },
                new Classification { IsAsset = true, AccountTypeId = 001, RbcTier = 011, BalanceSheetGroupId = 001, Name = "Other Liquid Asset" },

                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 000, BalanceSheetGroupId = 002, Name = "Other Loan" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 001, BalanceSheetGroupId = 002, Name = "Commercial RE" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 002, BalanceSheetGroupId = 002, Name = "C & I" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 003, BalanceSheetGroupId = 002, Name = "Installments" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 004, BalanceSheetGroupId = 002, Name = "Residential" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 005, BalanceSheetGroupId = 002, Name = "Agriculture" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 006, BalanceSheetGroupId = 002, Name = "Agriculture RE" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 007, BalanceSheetGroupId = 002, Name = "Residential - Constr" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 008, BalanceSheetGroupId = 002, Name = "Commercial RE - Constr" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 009, BalanceSheetGroupId = 002, Name = "Home Equity" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 010, BalanceSheetGroupId = 002, Name = "Gov & Agy Guaranteed" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 011, BalanceSheetGroupId = 002, Name = "Adjustments" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 012, BalanceSheetGroupId = 002, Name = "Loan Loss Reserve" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 013, BalanceSheetGroupId = 002, Name = "Commercial Leasing" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 014, BalanceSheetGroupId = 002, Name = "Land" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 015, BalanceSheetGroupId = 002, Name = "Auto" },
                new Classification { IsAsset = true, AccountTypeId = 002, RbcTier = 016, BalanceSheetGroupId = 002, Name = "Municipals" },


                new Classification { IsAsset = true, AccountTypeId = 000, RbcTier = 000, BalanceSheetGroupId = 003, Name = "Other Asset" },
                new Classification { IsAsset = true, AccountTypeId = 000, RbcTier = 001, BalanceSheetGroupId = 003, Name = "Intangible" },
                new Classification { IsAsset = true, AccountTypeId = 000, RbcTier = 002, BalanceSheetGroupId = 003, Name = "Loan Loss Reserve" },
                new Classification { IsAsset = true, AccountTypeId = 000, RbcTier = 003, BalanceSheetGroupId = 003, Name = "Fixed Assets" },
                new Classification { IsAsset = true, AccountTypeId = 000, RbcTier = 004, BalanceSheetGroupId = 003, Name = "BOLI" },
                new Classification { IsAsset = true, AccountTypeId = 000, RbcTier = 005, BalanceSheetGroupId = 003, Name = "OREO" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 000, BalanceSheetGroupId = 004, Name = "Other Non-Mat Deposit" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 001, BalanceSheetGroupId = 004, Name = "DDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 002, BalanceSheetGroupId = 004, Name = "Public DDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 003, BalanceSheetGroupId = 004, Name = "NOW" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 004, BalanceSheetGroupId = 004, Name = "Public NOW" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 005, BalanceSheetGroupId = 004, Name = "Savings" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 006, BalanceSheetGroupId = 004, Name = "Public Savings" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 007, BalanceSheetGroupId = 004, Name = "MMDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 008, BalanceSheetGroupId = 004, Name = "Public MMDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 009, BalanceSheetGroupId = 004, Name = "Online DDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 010, BalanceSheetGroupId = 004, Name = "Online NOW" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 011, BalanceSheetGroupId = 004, Name = "Online Savings" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 012, BalanceSheetGroupId = 004, Name = "Online MMDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 013, BalanceSheetGroupId = 004, Name = "Brokered NOW" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 014, BalanceSheetGroupId = 004, Name = "Brokered MMDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 015, BalanceSheetGroupId = 004, Name = "Brokered Reciprocal MMDA" },
                new Classification { IsAsset = false, AccountTypeId = 003, RbcTier = 016, BalanceSheetGroupId = 004, Name = "Brokered One-Way MMDA" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 000, BalanceSheetGroupId = 005, Name = "Other Time Deposit" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 001, BalanceSheetGroupId = 005, Name = "CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 002, BalanceSheetGroupId = 005, Name = "Public CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 003, BalanceSheetGroupId = 005, Name = "Jumbo CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 004, BalanceSheetGroupId = 005, Name = "Public Jumbo CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 005, BalanceSheetGroupId = 005, Name = "Brokered CD " },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 006, BalanceSheetGroupId = 005, Name = "National CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 007, BalanceSheetGroupId = 005, Name = "Online CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 008, BalanceSheetGroupId = 005, Name = "Online Jumbo CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 009, BalanceSheetGroupId = 005, Name = "Brokered Reciprocal CD" },
                new Classification { IsAsset = false, AccountTypeId = 008, RbcTier = 010, BalanceSheetGroupId = 005, Name = "Brokered One-Way CD" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 000, BalanceSheetGroupId = 006, Name = "Other Borrowings" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 001, BalanceSheetGroupId = 006, Name = "FHLB" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 002, BalanceSheetGroupId = 006, Name = "Unsecured Retail" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 003, BalanceSheetGroupId = 006, Name = "Secured Retail" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 004, BalanceSheetGroupId = 006, Name = "Unsecured Wholesale" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 005, BalanceSheetGroupId = 006, Name = "Secured Wholesale" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 006, BalanceSheetGroupId = 006, Name = "Unsecured Repo Wholesale" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 007, BalanceSheetGroupId = 006, Name = "Secured Repo Wholesale" },
                new Classification { IsAsset = false, AccountTypeId = 006, RbcTier = 008, BalanceSheetGroupId = 006, Name = "FRB" },
                new Classification { IsAsset = false, AccountTypeId = 000, RbcTier = 000, BalanceSheetGroupId = 007, Name = "Other Liability" },
                new Classification { IsAsset = false, AccountTypeId = 005, RbcTier = 000, BalanceSheetGroupId = 008, Name = "Other Capital Debt" },
                new Classification { IsAsset = false, AccountTypeId = 005, RbcTier = 001, BalanceSheetGroupId = 008, Name = "Tier 1" },
                new Classification { IsAsset = false, AccountTypeId = 005, RbcTier = 002, BalanceSheetGroupId = 008, Name = "Tier 2" },
                new Classification { IsAsset = false, AccountTypeId = 005, RbcTier = 003, BalanceSheetGroupId = 008, Name = "Tangible" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 000, BalanceSheetGroupId = 008, Name = "Other Equity" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 001, BalanceSheetGroupId = 008, Name = "Equity Tier 1" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 002, BalanceSheetGroupId = 008, Name = "Equity Tier 2" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 003, BalanceSheetGroupId = 008, Name = "Tangible" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 004, BalanceSheetGroupId = 008, Name = "Trust Preferred Tier 1" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 005, BalanceSheetGroupId = 008, Name = "Trust Preferred Tier 2" },
                new Classification { IsAsset = false, AccountTypeId = 004, RbcTier = 006, BalanceSheetGroupId = 008, Name = "Unrealized G/L (L)" },
                new Classification { IsAsset = true, AccountTypeId = 007, RbcTier = 001, BalanceSheetGroupId = 009, Name = "Swap (A)" },
                new Classification { IsAsset = true, AccountTypeId = 007, RbcTier = 002, BalanceSheetGroupId = 009, Name = "Cap (A)" },
                new Classification { IsAsset = true, AccountTypeId = 007, RbcTier = 003, BalanceSheetGroupId = 009, Name = "Floor (A)" },
                new Classification { IsAsset = false, AccountTypeId = 009, RbcTier = 000, BalanceSheetGroupId = 009, Name = "Other Off Bal Sheet (L)" },
                new Classification { IsAsset = false, AccountTypeId = 009, RbcTier = 001, BalanceSheetGroupId = 009, Name = "Swap (L)" },
                new Classification { IsAsset = false, AccountTypeId = 009, RbcTier = 002, BalanceSheetGroupId = 009, Name = "Cap (L)" },
                new Classification { IsAsset = false, AccountTypeId = 009, RbcTier = 003, BalanceSheetGroupId = 009, Name = "Floor (L)" }
                );




            context.ScenarioTypes.AddOrUpdate(s => s.Id,
              new ScenarioType { Id = 01, FileName = "Base", Priority = 01, IsEve = false, IsActive = true, RateMovement = 0, Name = "Base" },
              new ScenarioType { Id = 02, FileName = "D300", Priority = 08, IsEve = false, IsActive = false, RateMovement = -300, Name = "Down 300BP" },
              new ScenarioType { Id = 03, FileName = "D200", Priority = 07, IsEve = false, IsActive = false, RateMovement = -200, Name = "Down 200BP" },
              new ScenarioType { Id = 04, FileName = "D100", Priority = 06, IsEve = false, IsActive = true, RateMovement = -100, Name = "Down 100BP" },
              new ScenarioType { Id = 05, FileName = "U100", Priority = 05, IsEve = false, IsActive = false, RateMovement = 100, Name = "Up 100BP" },
              new ScenarioType { Id = 06, FileName = "U200", Priority = 04, IsEve = false, IsActive = true, RateMovement = 200, Name = "Up 200BP" },
              new ScenarioType { Id = 07, FileName = "U300", Priority = 03, IsEve = false, IsActive = false, RateMovement = 300, Name = "Up 300BP" },
              new ScenarioType { Id = 08, FileName = "U400", Priority = 02, IsEve = false, IsActive = false, RateMovement = 400, Name = "Up 400BP" },
              new ScenarioType { Id = 09, FileName = "M400", Priority = 10, IsEve = false, IsActive = true, RateMovement = 400, Name = "Up 400BP 24M" },
              new ScenarioType { Id = 10, FileName = "F200", Priority = 18, IsEve = false, IsActive = false, RateMovement = 200, Name = "Flat Up 200BP" },
              new ScenarioType { Id = 11, FileName = "F400", Priority = 15, IsEve = false, IsActive = false, RateMovement = 400, Name = "Flat Up 400BP" },
              new ScenarioType { Id = 12, FileName = "F500", Priority = 14, IsEve = false, IsActive = true, RateMovement = 500, Name = "Flat Up 500BP" },
              new ScenarioType { Id = 13, FileName = "W500", Priority = 20, IsEve = false, IsActive = true, RateMovement = 500, Name = "Delay Flat Up 500BP" },
              new ScenarioType { Id = 14, FileName = "P100", Priority = 25, IsEve = false, IsActive = false, RateMovement = -100, Name = "Steep Down 100BP" },
              new ScenarioType { Id = 15, FileName = "P200", Priority = 26, IsEve = false, IsActive = false, RateMovement = -200, Name = "Steep Down 200BP" },
              new ScenarioType { Id = 16, FileName = "T200", Priority = 23, IsEve = false, IsActive = false, RateMovement = 200, Name = "Steep Up 200BP" },
              new ScenarioType { Id = 17, FileName = "S300", Priority = 36, IsEve = false, IsActive = false, RateMovement = -300, Name = "Shock Down 300BP" },
              new ScenarioType { Id = 18, FileName = "S200", Priority = 35, IsEve = false, IsActive = false, RateMovement = -200, Name = "Shock Down 200BP" },
              new ScenarioType { Id = 19, FileName = "S100", Priority = 34, IsEve = false, IsActive = true, RateMovement = -100, Name = "Shock Down 100BP" },
              new ScenarioType { Id = 20, FileName = "K100", Priority = 33, IsEve = false, IsActive = true, RateMovement = 100, Name = "Shock Up 100BP" },
              new ScenarioType { Id = 21, FileName = "K200", Priority = 32, IsEve = false, IsActive = true, RateMovement = 200, Name = "Shock Up 200BP" },
              new ScenarioType { Id = 22, FileName = "K300", Priority = 31, IsEve = false, IsActive = true, RateMovement = 300, Name = "Shock Up 300BP" },
              new ScenarioType { Id = 23, FileName = "K400", Priority = 30, IsEve = false, IsActive = true, RateMovement = 400, Name = "Shock Up 400BP" },
              new ScenarioType { Id = 24, FileName = "Junk", Priority = 50, IsEve = true, IsActive = true, RateMovement = -400, Name = "-400BP" },
              new ScenarioType { Id = 25, FileName = "Junk", Priority = 49, IsEve = true, IsActive = true, RateMovement = -300, Name = "-300BP" },
              new ScenarioType { Id = 26, FileName = "Junk", Priority = 48, IsEve = true, IsActive = true, RateMovement = -200, Name = "-200BP" },
              new ScenarioType { Id = 27, FileName = "Junk", Priority = 47, IsEve = true, IsActive = true, RateMovement = -150, Name = "-150BP" },
              new ScenarioType { Id = 28, FileName = "Junk", Priority = 46, IsEve = true, IsActive = true, RateMovement = -100, Name = "-100BP" },
              new ScenarioType { Id = 29, FileName = "Junk", Priority = 45, IsEve = true, IsActive = true, RateMovement = -50, Name = "-50BP" },
              new ScenarioType { Id = 30, FileName = "Junk", Priority = 44, IsEve = true, IsActive = true, RateMovement = 0, Name = "0 Shock" },
              new ScenarioType { Id = 31, FileName = "Junk", Priority = 43, IsEve = true, IsActive = true, RateMovement = 50, Name = "+50BP" },
              new ScenarioType { Id = 32, FileName = "Junk", Priority = 42, IsEve = true, IsActive = true, RateMovement = 100, Name = "+100BP" },
              new ScenarioType { Id = 33, FileName = "Junk", Priority = 41, IsEve = true, IsActive = true, RateMovement = 150, Name = "+150BP" },
              new ScenarioType { Id = 34, FileName = "Junk", Priority = 40, IsEve = true, IsActive = true, RateMovement = 200, Name = "+200BP" },
              new ScenarioType { Id = 35, FileName = "Junk", Priority = 39, IsEve = true, IsActive = true, RateMovement = 300, Name = "+300BP" },
              new ScenarioType { Id = 36, FileName = "Junk", Priority = 38, IsEve = true, IsActive = true, RateMovement = 400, Name = "+400BP" },
              new ScenarioType { Id = 37, FileName = "YCTW", Priority = 29, IsEve = false, IsActive = true, RateMovement = 99999, Name = "Yield Curve Twist" },
              new ScenarioType { Id = 38, FileName = "D400", Priority = 09, IsEve = false, IsActive = false, RateMovement = -400, Name = "Down 400BP" },
              new ScenarioType { Id = 39, FileName = "M100", Priority = 13, IsEve = false, IsActive = false, RateMovement = 100, Name = "Up 100BP 24M" },
              new ScenarioType { Id = 40, FileName = "M200", Priority = 12, IsEve = false, IsActive = false, RateMovement = 200, Name = "Up 200BP 24M" },
              new ScenarioType { Id = 41, FileName = "M300", Priority = 11, IsEve = false, IsActive = false, RateMovement = 300, Name = "Up 300BP 24M" },
              new ScenarioType { Id = 42, FileName = "F100", Priority = 19, IsEve = false, IsActive = false, RateMovement = 100, Name = "Flat Up 100BP" },
              new ScenarioType { Id = 43, FileName = "F300", Priority = 16, IsEve = false, IsActive = false, RateMovement = 300, Name = "Flat Up 300BP" },
              new ScenarioType { Id = 44, FileName = "P300", Priority = 27, IsEve = false, IsActive = false, RateMovement = 300, Name = "Steep Down 300BP" },
              new ScenarioType { Id = 45, FileName = "P400", Priority = 28, IsEve = false, IsActive = false, RateMovement = -400, Name = "Steep Down 400BP" },
              new ScenarioType { Id = 46, FileName = "T100", Priority = 24, IsEve = false, IsActive = false, RateMovement = 100, Name = "Steep Up 100BP" },
              new ScenarioType { Id = 47, FileName = "T300", Priority = 22, IsEve = false, IsActive = false, RateMovement = 300, Name = "Steep Up 300BP" },
              new ScenarioType { Id = 48, FileName = "T400", Priority = 21, IsEve = false, IsActive = false, RateMovement = 400, Name = "Steep Up 400BP" },
              new ScenarioType { Id = 49, FileName = "S400", Priority = 37, IsEve = false, IsActive = false, RateMovement = -400, Name = "Shock Down 400BP" },
              new ScenarioType { Id = 50, FileName = "F224", Priority = 17, IsEve = false, IsActive = false, RateMovement = -200, Name = "Flat Up 200BP 24M" }


              );


            context.SimulationTypes.AddOrUpdate(s => s.Id,
                new SimulationType { Id = 001, Name = "Base" },
                new SimulationType { Id = 002, Name = "EVE/NEV" },
                new SimulationType { Id = 003, Name = "ST: NMD Migration" },
                new SimulationType { Id = 004, Name = "ST: Alt NMD Betas" },
                new SimulationType { Id = 005, Name = "ST: Alt TD Sensitivity" },
                new SimulationType { Id = 006, Name = "ST: Alt EVE/NEV – Decreased Avg Life on NMD" },
                new SimulationType { Id = 007, Name = "ST: Alt EVE/NEV – Increased Avg Life on NMD" },
                new SimulationType { Id = 008, Name = "ST: Investment Prepayment" },
                new SimulationType { Id = 009, Name = "ST: BKFS Prepayment" },
                new SimulationType { Id = 010, Name = "ST: Non-BKFS Prepayment" },
                new SimulationType { Id = 011, Name = "ST: Loan Pricing" },
                new SimulationType { Id = 012, Name = "ST: Loan Pricing - Rising Rates" },
                new SimulationType { Id = 013, Name = "ASC825" }
               );

            context.AccountingTypes.AddOrUpdate(c => c.Id,
                //Assets
                new AccountingType { Id = 001, Name = "Asset" },
                new AccountingType { Id = 002, Name = "Liability" },
                new AccountingType { Id = 003, Name = "Equity" },
                new AccountingType { Id = 004, Name = "Off Balance Sheet Asset" },
                new AccountingType { Id = 005, Name = "Off Balance Sheet Liability" }
                );

            context.BalanceSheetGroups.AddOrUpdate(c => c.Id,
                //Assets
                new BalanceSheetGroup { Id = 001, AccountingTypeId = 001, Name = "Investment" },
                new BalanceSheetGroup { Id = 002, AccountingTypeId = 001, Name = "Loan" },
                new BalanceSheetGroup { Id = 003, AccountingTypeId = 001, Name = "Other Asset" },
                new BalanceSheetGroup { Id = 004, AccountingTypeId = 002, Name = "Non-Maturity Deposit" },
                new BalanceSheetGroup { Id = 005, AccountingTypeId = 002, Name = "Time Deposit" },
                new BalanceSheetGroup { Id = 006, AccountingTypeId = 002, Name = "Borrowing" },
                new BalanceSheetGroup { Id = 007, AccountingTypeId = 002, Name = "Other Liability" },
                new BalanceSheetGroup { Id = 008, AccountingTypeId = 003, Name = "Equity" },
                new BalanceSheetGroup { Id = 009, AccountingTypeId = 004, Name = "Other" }
                );


            context.PeriodSmoother.AddOrUpdate(c => c.Period, new PeriodSmoother { Period = 1 },
            new PeriodSmoother { Period = 2 },
            new PeriodSmoother { Period = 3 },
            new PeriodSmoother { Period = 4 },
            new PeriodSmoother { Period = 5 },
            new PeriodSmoother { Period = 6 },
            new PeriodSmoother { Period = 7 },
            new PeriodSmoother { Period = 8 },
            new PeriodSmoother { Period = 9 },
            new PeriodSmoother { Period = 10 },
            new PeriodSmoother { Period = 11 },
            new PeriodSmoother { Period = 12 },
            new PeriodSmoother { Period = 13 },
            new PeriodSmoother { Period = 14 },
            new PeriodSmoother { Period = 15 },
            new PeriodSmoother { Period = 16 },
            new PeriodSmoother { Period = 17 },
            new PeriodSmoother { Period = 18 },
            new PeriodSmoother { Period = 19 },
            new PeriodSmoother { Period = 20 },
            new PeriodSmoother { Period = 21 },
            new PeriodSmoother { Period = 22 },
            new PeriodSmoother { Period = 23 },
            new PeriodSmoother { Period = 24 },
            new PeriodSmoother { Period = 25 },
            new PeriodSmoother { Period = 26 },
            new PeriodSmoother { Period = 27 },
            new PeriodSmoother { Period = 28 },
            new PeriodSmoother { Period = 29 },
            new PeriodSmoother { Period = 30 },
            new PeriodSmoother { Period = 31 },
            new PeriodSmoother { Period = 32 },
            new PeriodSmoother { Period = 33 },
            new PeriodSmoother { Period = 33 },
            new PeriodSmoother { Period = 34 },
            new PeriodSmoother { Period = 35 },
            new PeriodSmoother { Period = 36 },
            new PeriodSmoother { Period = 37 },
            new PeriodSmoother { Period = 38 },
            new PeriodSmoother { Period = 39 },
            new PeriodSmoother { Period = 40 },
            new PeriodSmoother { Period = 41 },
            new PeriodSmoother { Period = 42 },
            new PeriodSmoother { Period = 43 },
            new PeriodSmoother { Period = 43 },
            new PeriodSmoother { Period = 44 },
            new PeriodSmoother { Period = 45 },
            new PeriodSmoother { Period = 46 },
            new PeriodSmoother { Period = 47 },
            new PeriodSmoother { Period = 48 },
            new PeriodSmoother { Period = 49 },
            new PeriodSmoother { Period = 50 },
            new PeriodSmoother { Period = 51 },
            new PeriodSmoother { Period = 52 },
            new PeriodSmoother { Period = 53 },
            new PeriodSmoother { Period = 53 },
            new PeriodSmoother { Period = 54 },
            new PeriodSmoother { Period = 55 },
            new PeriodSmoother { Period = 56 },
            new PeriodSmoother { Period = 57 },
            new PeriodSmoother { Period = 58 },
            new PeriodSmoother { Period = 59 },
            new PeriodSmoother { Period = 60 },
            new PeriodSmoother { Period = 61 },
            new PeriodSmoother { Period = 62 },
            new PeriodSmoother { Period = 63 },
            new PeriodSmoother { Period = 63 },
            new PeriodSmoother { Period = 64 },
            new PeriodSmoother { Period = 65 },
            new PeriodSmoother { Period = 66 },
            new PeriodSmoother { Period = 67 },
            new PeriodSmoother { Period = 68 },
            new PeriodSmoother { Period = 69 },
            new PeriodSmoother { Period = 70 },
            new PeriodSmoother { Period = 71 },
            new PeriodSmoother { Period = 72 },
            new PeriodSmoother { Period = 73 },
            new PeriodSmoother { Period = 73 },
            new PeriodSmoother { Period = 74 },
            new PeriodSmoother { Period = 75 },
            new PeriodSmoother { Period = 76 },
            new PeriodSmoother { Period = 77 },
            new PeriodSmoother { Period = 78 },
            new PeriodSmoother { Period = 79 },
            new PeriodSmoother { Period = 80 },
            new PeriodSmoother { Period = 81 },
            new PeriodSmoother { Period = 82 },
            new PeriodSmoother { Period = 83 },
            new PeriodSmoother { Period = 83 },
            new PeriodSmoother { Period = 84 },
            new PeriodSmoother { Period = 85 },
            new PeriodSmoother { Period = 86 },
            new PeriodSmoother { Period = 87 },
            new PeriodSmoother { Period = 88 },
            new PeriodSmoother { Period = 89 },
            new PeriodSmoother { Period = 90 },
            new PeriodSmoother { Period = 91 },
            new PeriodSmoother { Period = 92 },
            new PeriodSmoother { Period = 93 },
            new PeriodSmoother { Period = 93 },
            new PeriodSmoother { Period = 94 },
            new PeriodSmoother { Period = 95 },
            new PeriodSmoother { Period = 96 },
            new PeriodSmoother { Period = 97 },
            new PeriodSmoother { Period = 98 },
            new PeriodSmoother { Period = 99 },
            new PeriodSmoother { Period = 100 },
            new PeriodSmoother { Period = 101 },
            new PeriodSmoother { Period = 102 },
            new PeriodSmoother { Period = 103 },
            new PeriodSmoother { Period = 103 },
            new PeriodSmoother { Period = 104 },
            new PeriodSmoother { Period = 105 },
            new PeriodSmoother { Period = 106 },
            new PeriodSmoother { Period = 107 },
            new PeriodSmoother { Period = 108 },
            new PeriodSmoother { Period = 109 });


            context.AccountTypeOrder.AddOrUpdate(c => new { c.IsAsset, c.Acc_Type },
                new AccountTypeOrder { IsAsset = 1, Acc_Type = 1, Order = 1 },
                new AccountTypeOrder { IsAsset = 1, Acc_Type = 2, Order = 2 },
                new AccountTypeOrder { IsAsset = 1, Acc_Type = 0, Order = 3 },
                new AccountTypeOrder { IsAsset = 1, Acc_Type = 7, Order = 4 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 3, Order = 1 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 8, Order = 2 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 6, Order = 3 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 5, Order = 4 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 0, Order = 5 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 4, Order = 6 },
                new AccountTypeOrder { IsAsset = 0, Acc_Type = 9, Order = 7 }
                );

            context.ClassificationOrder.AddOrUpdate(c => new { c.Acc_Type, c.RBC_Tier },

                //Other Assets
                new ClassificationOrder { Acc_Type = 0, RBC_Tier = 2, Order = 0 },
                new ClassificationOrder { Acc_Type = 0, RBC_Tier = 3, Order = 1 },
                new ClassificationOrder { Acc_Type = 0, RBC_Tier = 5, Order = 2 },
                new ClassificationOrder { Acc_Type = 0, RBC_Tier = 4, Order = 3 },
                new ClassificationOrder { Acc_Type = 0, RBC_Tier = 1, Order = 4 },

                //Investments
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 6, Order = 0 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 11, Order = 1 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 8, Order = 2 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 1, Order = 3 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 4, Order = 4 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 3, Order = 5 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 5, Order = 6 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 2, Order = 7 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 7, Order = 8 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 9, Order = 9 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 0, Order = 10 },
                new ClassificationOrder { Acc_Type = 1, RBC_Tier = 10, Order = 11 },


                //Loans
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 4, Order = 0 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 7, Order = 1 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 2, Order = 2 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 1, Order = 3 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 8, Order = 4 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 13, Order = 5 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 14, Order = 6 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 5, Order = 7 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 6, Order = 8 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 9, Order = 9 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 3, Order = 10 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 15, Order = 11 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 10, Order = 12 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 16, Order = 13 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 0, Order = 14 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 11, Order = 15 },
                new ClassificationOrder { Acc_Type = 2, RBC_Tier = 12, Order = 16 },


                //Off Balance Sheet Assets 
                new ClassificationOrder { Acc_Type = 7, RBC_Tier = 1, Order = 0 },
                new ClassificationOrder { Acc_Type = 7, RBC_Tier = 2, Order = 1 },
                new ClassificationOrder { Acc_Type = 7, RBC_Tier = 3, Order = 2 },
                new ClassificationOrder { Acc_Type = 7, RBC_Tier = 0, Order = 3 },


                //Other Liabilities
                new ClassificationOrder { Acc_Type = 0, RBC_Tier = 0, Order = 0 },

                //Non Maturity Deposits
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 1, Order = 0 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 2, Order = 1 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 9, Order = 2 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 3, Order = 3 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 4, Order = 4 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 10, Order = 5 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 13, Order = 6 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 5, Order = 7 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 6, Order = 8 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 11, Order = 9 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 7, Order = 10 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 8, Order = 11 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 12, Order = 12 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 15, Order = 13 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 16, Order = 14 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 14, Order = 15 },
                new ClassificationOrder { Acc_Type = 3, RBC_Tier = 0, Order = 16 },

                //Equity
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 7, Order = 0 },
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 1, Order = 1 },
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 2, Order = 2 },
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 4, Order = 3 },
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 5, Order = 4 },
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 6, Order = 5 },
                new ClassificationOrder { Acc_Type = 4, RBC_Tier = 0, Order = 6 },

                //Capital Debt
                new ClassificationOrder { Acc_Type = 5, RBC_Tier = 2, Order = 0 },
                new ClassificationOrder { Acc_Type = 5, RBC_Tier = 0, Order = 1 },

                //Borrowings
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 2, Order = 0 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 3, Order = 1 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 4, Order = 2 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 5, Order = 3 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 1, Order = 4 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 8, Order = 5 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 6, Order = 6 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 7, Order = 7 },
                new ClassificationOrder { Acc_Type = 6, RBC_Tier = 0, Order = 8 },

                //Time Deposits
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 1, Order = 0 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 2, Order = 1 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 3, Order = 2 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 4, Order = 3 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 7, Order = 4 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 8, Order = 5 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 6, Order = 6 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 9, Order = 7 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 10, Order = 8 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 5, Order = 9 },
                new ClassificationOrder { Acc_Type = 8, RBC_Tier = 0, Order = 10 },



                //Off Balance Sheet Liabilities
                new ClassificationOrder { Acc_Type = 9, RBC_Tier = 1, Order = 0 },
                new ClassificationOrder { Acc_Type = 9, RBC_Tier = 2, Order = 1 },
                new ClassificationOrder { Acc_Type = 9, RBC_Tier = 3, Order = 2 },
                new ClassificationOrder { Acc_Type = 9, RBC_Tier = 0, Order = 3 }

                );

            //Web Rate Scenario Types
            context.WebRateScenarioTypes.AddOrUpdate(s => s.Id,
                 new WebRateScenarioType { Id = 01, Name = "Down 100BP", ScenNumber = "000" },
                 new WebRateScenarioType { Id = 02, Name = "Base Scenario", ScenNumber = "001" },
                 new WebRateScenarioType { Id = 03, Name = "Up 100BP", ScenNumber = "002" },
                 new WebRateScenarioType { Id = 04, Name = "Up 200BP", ScenNumber = "003" },
                 new WebRateScenarioType { Id = 05, Name = "Up 200BP Flat", ScenNumber = "004" },
                 new WebRateScenarioType { Id = 06, Name = "Up 400BP Flat (24 Months)", ScenNumber = "005" },
                 new WebRateScenarioType { Id = 07, Name = "Down 200BP", ScenNumber = "006" },
                 new WebRateScenarioType { Id = 08, Name = "Up 400BP", ScenNumber = "007" },
                 new WebRateScenarioType { Id = 09, Name = "Steepening Down 200BP", ScenNumber = "008" },
                 new WebRateScenarioType { Id = 10, Name = "Up 500BP Flat (24 Months - Delayed)", ScenNumber = "009" },
                 new WebRateScenarioType { Id = 11, Name = "Steepening Down 100BP", ScenNumber = "010" },
                 new WebRateScenarioType { Id = 12, Name = "Up 300BP", ScenNumber = "011" },
                 new WebRateScenarioType { Id = 13, Name = "Up 400BP (24 Months)", ScenNumber = "012" },
                 new WebRateScenarioType { Id = 14, Name = "Up 500BP Flat (24 Months)", ScenNumber = "013" },
                 new WebRateScenarioType { Id = 15, Name = "Down 300BP", ScenNumber = "014" },
                 new WebRateScenarioType { Id = 16, Name = "Shock Down 100BP", ScenNumber = "015" },
                 new WebRateScenarioType { Id = 17, Name = "Shock Down 200BP", ScenNumber = "016" },
                 new WebRateScenarioType { Id = 18, Name = "Shock Down 300BP", ScenNumber = "017" },
                 new WebRateScenarioType { Id = 19, Name = "Shock Up 100BP", ScenNumber = "018" },
                 new WebRateScenarioType { Id = 20, Name = "Shock Up 200BP", ScenNumber = "019" },
                 new WebRateScenarioType { Id = 21, Name = "Shock Up 300BP", ScenNumber = "020" },
                 new WebRateScenarioType { Id = 22, Name = "Shock Up 400BP", ScenNumber = "021" },
                 new WebRateScenarioType { Id = 23, Name = "Yield Curve Twist", ScenNumber = "022" }
                 );


            context.CoreFundingLookups.AddOrUpdate(c => new { c.IsAsset, c.Acc_Type, c.RBC_Tier },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 0, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 1, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 2, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 3, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 4, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 5, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 6, Name = "Equity" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 0, RBC_Tier = 0, Name = "Other Liability" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 1, Name = "DDA" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 2, Name = "DDA" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 9, Name = "DDA" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 3, Name = "NOW" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 4, Name = "NOW" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 10, Name = "NOW" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 5, Name = "Savings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 6, Name = "Savings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 11, Name = "Savings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 7, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 8, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 13, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 12, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 15, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 16, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 0, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 1, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 3, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 2, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 4, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 7, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 8, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 6, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 9, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 10, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 5, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 0, Name = "Other Deposits" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 0, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 1, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 2, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 3, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 4, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 5, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 6, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 7, Name = "Borrowings" },
            new CoreFundingLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 8, Name = "Borrowings" }
            );




            //Basic Surplus Stuff
            context.SecuredLiabilitiesTypes.AddOrUpdate(c => new { c.Id },
                new SecuredLiabilitiesType { Id = 1, Name = "FHLB" },
                new SecuredLiabilitiesType { Id = 2, Name = "Fed Discount/Other Secured" },
                new SecuredLiabilitiesType { Id = 3, Name = "Wholesale Repos" },
                new SecuredLiabilitiesType { Id = 4, Name = "Retail Repos/Sweeps" },
                new SecuredLiabilitiesType { Id = 5, Name = "Municipal Deposits" },
                new SecuredLiabilitiesType { Id = 6, Name = "Other" },
                new SecuredLiabilitiesType { Id = 7, Name = "FHLB Letter of Credit" }
            );

            context.BalanceSheetMixLookups.AddOrUpdate(c => new { c.IsAsset, c.Acc_Type, c.RBC_Tier },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 1, ParentName = "Investments", Name = "Agencies", Priority = 1 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 8, ParentName = "Investments", Name = "Agencies", Priority = 1 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 3, ParentName = "Investments", Name = "MBS/CMO", Priority = 2 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 4, ParentName = "Investments", Name = "MBS/CMO", Priority = 2 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 5, ParentName = "Investments", Name = "Municipals", Priority = 3 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 6, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 11, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 2, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 7, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 0, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 9, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 1, RBC_Tier = 10, ParentName = "Investments", Name = "Other", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 7, RBC_Tier = 0, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 7, RBC_Tier = 1, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 7, RBC_Tier = 2, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 7, RBC_Tier = 3, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 0, RBC_Tier = 0, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 0, RBC_Tier = 1, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 0, RBC_Tier = 2, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 0, RBC_Tier = 3, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 0, RBC_Tier = 4, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 1, Acc_Type = 0, RBC_Tier = 5, ParentName = "Other Assets", Name = "Other Assets", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 1, ParentName = "Non-Maturity", Name = "DDA", Priority = 1 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 2, ParentName = "Non-Maturity", Name = "DDA", Priority = 1 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 9, ParentName = "Non-Maturity", Name = "DDA", Priority = 1 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 3, ParentName = "Non-Maturity", Name = "NOW", Priority = 2 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 4, ParentName = "Non-Maturity", Name = "NOW", Priority = 2 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 10, ParentName = "Non-Maturity", Name = "NOW", Priority = 2 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 6, ParentName = "Non-Maturity", Name = "Savings", Priority = 3 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 5, ParentName = "Non-Maturity", Name = "Savings", Priority = 3 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 11, ParentName = "Non-Maturity", Name = "Savings", Priority = 3 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 7, ParentName = "Non-Maturity", Name = "MMDA", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 8, ParentName = "Non-Maturity", Name = "MMDA", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 2, RBC_Tier = 12, ParentName = "Non-Maturity", Name = "MMDA", Priority = 4 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 0, ParentName = "Non-Maturity", Name = "Other", Priority = 5 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 1, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 3, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 2, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 4, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 7, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 8, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 0, ParentName = "Time Deposits", Name = "Time Deposits", Priority = 6 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 13, ParentName = "Brokered Deposits", Name = "Brokered Deposits", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 16, ParentName = "Brokered Deposits", Name = "Brokered Deposits", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 3, RBC_Tier = 14, ParentName = "Brokered Deposits", Name = "Brokered Deposits", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 10, ParentName = "Brokered Deposits", Name = "Brokered Deposits", Priority = 10 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 8, RBC_Tier = 5, ParentName = "Brokered Deposits", Name = "Brokered Deposits", Priority = 10 },

                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 1, ParentName = "Borrowings", Name = "FHLB", Priority = 11 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 2, ParentName = "Borrowings", Name = "Repo-Retail", Priority = 12 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 3, ParentName = "Borrowings", Name = "Repo-Retail", Priority = 13 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 6, ParentName = "Borrowings", Name = "Repo-Whole", Priority = 14 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 7, ParentName = "Borrowings", Name = "Repo-Whole", Priority = 14 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 0, ParentName = "Borrowings", Name = "Other", Priority = 15 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 4, ParentName = "Borrowings", Name = "Other", Priority = 15 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 5, ParentName = "Borrowings", Name = "Other", Priority = 15 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 6, RBC_Tier = 8, ParentName = "Borrowings", Name = "Other", Priority = 15 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 1, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 2, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 3, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 4, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 5, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 0, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 4, RBC_Tier = 6, ParentName = "Equity", Name = "Equity", Priority = 16 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 0, RBC_Tier = 0, ParentName = "Other Liabilities", Name = "Other Liabilities", Priority = 17 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 9, RBC_Tier = 0, ParentName = "Other Liabilities", Name = "Other Liabilities", Priority = 17 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 9, RBC_Tier = 1, ParentName = "Other Liabilities", Name = "Other Liabilities", Priority = 17 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 9, RBC_Tier = 2, ParentName = "Other Liabilities", Name = "Other Liabilities", Priority = 17 },
                new BalanceSheetMixLookup { IsAsset = 0, Acc_Type = 9, RBC_Tier = 3, ParentName = "Other Liabilities", Name = "Other Liabilities", Priority = 17 }
                );



            context.NIIReconOrder.AddOrUpdate(c => new { c.IsAsset, c.Acc_Type, c.RBC_Tier, c.Priority },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 6, Priority = 1 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 11, Priority = 2 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 8, Priority = 3 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 1, Priority = 4 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 4, Priority = 5 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 3, Priority = 6 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 5, Priority = 7 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 2, Priority = 8 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 7, Priority = 9 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 9, Priority = 10 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 0, Priority = 11 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 1, RBC_Tier = 10, Priority = 12 },

                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 4, Priority = 13 }, // Residential
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 7, Priority = 14 }, // Residential - Constr
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 2, Priority = 15 }, // C & I
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 1, Priority = 16 },// Commercial RE
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 8, Priority = 17 },//  Commercial RE -Constr
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 13, Priority = 18 },//  Commercial LEasing
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 14, Priority = 19 },// Land
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 5, Priority = 20 },//  Agriculture
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 6, Priority = 21 }, // Agriculture RE
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 9, Priority = 22 },// Home Equity
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 3, Priority = 23 }, // Installments
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 15, Priority = 24 }, //  Auto
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 10, Priority = 25 }, // Gov Agy
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 16, Priority = 26 },// Municipals
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 0, Priority = 27 }, // Other Loan
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 11, Priority = 28 }, // Adjustments
                new NIIReconOrder { IsAsset = 1, Acc_Type = 2, RBC_Tier = 12, Priority = 29 }, // Loan Loss Reserve
                new NIIReconOrder { IsAsset = 1, Acc_Type = 0, RBC_Tier = 2, Priority = 30 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 0, RBC_Tier = 1, Priority = 31 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 0, RBC_Tier = 3, Priority = 32 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 0, RBC_Tier = 4, Priority = 33 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 0, RBC_Tier = 5, Priority = 34 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 0, RBC_Tier = 0, Priority = 35 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 7, RBC_Tier = 1, Priority = 36 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 7, RBC_Tier = 2, Priority = 37 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 7, RBC_Tier = 3, Priority = 38 },
                new NIIReconOrder { IsAsset = 1, Acc_Type = 7, RBC_Tier = 0, Priority = 39 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 1, Priority = 40 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 2, Priority = 41 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 9, Priority = 42 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 3, Priority = 43 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 4, Priority = 44 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 10, Priority = 45 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 13, Priority = 46 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 5, Priority = 47 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 6, Priority = 48 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 11, Priority = 49 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 7, Priority = 50 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 8, Priority = 51 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 12, Priority = 52 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 15, Priority = 53 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 16, Priority = 54 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 14, Priority = 55 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 3, RBC_Tier = 0, Priority = 56 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 1, Priority = 57 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 2, Priority = 58 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 3, Priority = 59 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 4, Priority = 60 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 7, Priority = 61 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 8, Priority = 62 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 5, Priority = 63 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 9, Priority = 64 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 10, Priority = 65 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 6, Priority = 66 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 8, RBC_Tier = 0, Priority = 67 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 2, Priority = 68 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 3, Priority = 69 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 4, Priority = 70 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 5, Priority = 71 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 1, Priority = 72 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 8, Priority = 73 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 6, Priority = 74 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 7, Priority = 75 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 6, RBC_Tier = 0, Priority = 76 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 0, RBC_Tier = 0, Priority = 77 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 4, RBC_Tier = 1, Priority = 78 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 4, RBC_Tier = 2, Priority = 79 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 4, RBC_Tier = 4, Priority = 80 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 4, RBC_Tier = 5, Priority = 81 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 4, RBC_Tier = 6, Priority = 82 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 4, RBC_Tier = 0, Priority = 83 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 9, RBC_Tier = 1, Priority = 84 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 9, RBC_Tier = 2, Priority = 85 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 9, RBC_Tier = 3, Priority = 86 },
                new NIIReconOrder { IsAsset = 0, Acc_Type = 9, RBC_Tier = 0, Priority = 87 }

              );

            StringBuilder sqlQuery = new StringBuilder();
            string database = context.Database.Connection.Database;
            sqlQuery.AppendLine(" DECLARE @databaseName VARCHAR(100); ");
            sqlQuery.AppendLine(" SET @databaseName = '" + database + "' ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" --Stores The Table Name for loop ");
            sqlQuery.AppendLine(" DECLARE @TABLE_NAME VARCHAR(200); ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" --Holds Sql For Each Table Command ");
            sqlQuery.AppendLine(" DECLARE @SQL VARCHAR(MAX); ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" DECLARE Basis_Tables CURSOR ");
            sqlQuery.AppendLine(" FOR SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_NAME like 'Basis_%' AND TABLE_NAME NOT IN ('Basis_Bank', 'Basis_ALS'); ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" OPEN Basis_Tables ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" FETCH NEXT ");
            sqlQuery.AppendLine(" FROM Basis_Tables ");
            sqlQuery.AppendLine(" INTO @TABLE_NAME ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" WHILE @@FETCH_STATUS = 0 ");
            sqlQuery.AppendLine(" BEGIN ");
            sqlQuery.AppendLine(" 	SET @SQL = 'USE [' + @databaseName + '];' ");
            sqlQuery.AppendLine("     SET @SQL = @SQL + ' GRANT  REFERENCES ,  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[' + @TABLE_NAME +']  TO [DCG_Analyst];'; ");
            sqlQuery.AppendLine("     SET @SQL = @SQL + ' GRANT  REFERENCES ,  SELECT ,  UPDATE ,  INSERT ,  DELETE  ON [dbo].[' + @TABLE_NAME +']  TO [DCG_Client]; '; ");
            sqlQuery.AppendLine(" 	Begin Try ");
            sqlQuery.AppendLine(" 		EXECUTE(@SQL); ");
            sqlQuery.AppendLine(" 	End Try ");
            sqlQuery.AppendLine(" 	Begin Catch ");
            sqlQuery.AppendLine(" 	End Catch ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" 	FETCH NEXT ");
            sqlQuery.AppendLine(" 	FROM Basis_Tables ");
            sqlQuery.AppendLine(" 	INTO @TABLE_NAME ");
            sqlQuery.AppendLine(" END -- BASIS_TABLES Cursor ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" CLOSE BASIS_TABLES; ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" DEALLOCATE BASIS_TABLES; ");
            using (SqlConnection sqlCon = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                SqlCommand sqlCmd = new SqlCommand(sqlQuery.ToString(), sqlCon);
                try
                {
                    sqlCon.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlCon.Close();
                }
                catch (Exception ex)
                {
                    sqlCon.Close();
                }
            }

        }
    }
}