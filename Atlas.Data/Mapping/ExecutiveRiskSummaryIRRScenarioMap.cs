﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  ExecutiveRiskSummaryIRRScenarioMap : EntityTypeConfiguration<ExecutiveRiskSummaryIRRScenario>
    {
        public ExecutiveRiskSummaryIRRScenarioMap()
        {
            

            // A Two Income Graph Report has excatly one left Income Graph.
            // An Income graph can be a "left income graph" on many Two Income Graph Reports
            HasRequired(t => t.ScenarioType)
                .WithMany()
                .HasForeignKey(t => t.ScenarioTypeId)
                .WillCascadeOnDelete(false);

        }
    }
}
