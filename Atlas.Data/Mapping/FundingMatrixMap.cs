﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  FundingMatrixMap : EntityTypeConfiguration< FundingMatrix>
    {
        public FundingMatrixMap()
        {

            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            ////Simulation Mapping
            //HasRequired(t => t.LeftSimulationType)
            //    .WithMany()
            //    .HasForeignKey(t => t.LeftSimulationTypeId)
            //    .WillCascadeOnDelete(false);

            //HasRequired(t => t.RightSimulationType)
            //    .WithMany()
            //    .HasForeignKey(t => t.RightSimulationTypeId)
            //    .WillCascadeOnDelete(false);

            ////Scenario Mapping
            //HasRequired(t => t.LeftScenarioType)
            //    .WithMany()
            //    .HasForeignKey(t => t.LeftScenarioTypeId)
            //    .WillCascadeOnDelete(false);

            //HasRequired(t => t.RightScenarioType)
            //    .WithMany()
            //    .HasForeignKey(t => t.RightScenarioTypeId)
            //    .WillCascadeOnDelete(false);
        }
    }
}
