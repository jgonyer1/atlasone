﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class LiquidityProjectionMap : EntityTypeConfiguration<LiquidityProjection>
    {
        public LiquidityProjectionMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            /*  HasRequired(t => t.SimulationType)
                  .WithMany()
                  .HasForeignKey(t => t.SimulationType)
                  .WillCascadeOnDelete(false);*/

            //HasRequired(t => t.BasicSurplusAdmin)
            //    .WithMany()
            //    .HasForeignKey(t => t.BasicSurplusAdminId)
            //    .WillCascadeOnDelete(false);


            //    HasRequired(t => t.BasicSurplusOffset)
            //    .WithMany()
            //    .HasForeignKey(t => t.BasicSurplusOffsetId)
            //    .WillCascadeOnDelete(false);

        }
    }
}
