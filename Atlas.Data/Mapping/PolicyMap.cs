﻿ using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    class PolicyMap : EntityTypeConfiguration<Policy>
    {
        public PolicyMap()
        {

            HasRequired(s => s.NIISimulationType)
                .WithMany()
                .HasForeignKey(s=> s.NIISimulationTypeId)
                .WillCascadeOnDelete(false);
            
            HasRequired(s => s.EveSimulationType)
                          .WithMany()
                          .HasForeignKey(s => s.EveSimulationTypeId)
                          .WillCascadeOnDelete(false);
            
        }
    }
}
