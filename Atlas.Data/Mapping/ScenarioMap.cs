﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    public class ScenarioMap : EntityTypeConfiguration<Scenario>
    {
        public ScenarioMap()
        {
            // Attendance has a composite key: SessionId and PersonId
            HasKey(i => new { i.SimulationId, i.Number });

            HasRequired(c => c.Simulation)
                .WithMany(s => s.Scenarios)
                .HasForeignKey(c => c.SimulationId)
                .WillCascadeOnDelete(false);
        
        }
    }
}

