﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    class InterestRatePolicyGuidelinesDateMap : EntityTypeConfiguration<InterestRatePolicyGuidelinesDate>
    {
        public InterestRatePolicyGuidelinesDateMap()
        {
            HasRequired(t => t.EVESimulationType)
                .WithMany()
                .HasForeignKey(t => t.EVESimulationTypeId)
                .WillCascadeOnDelete(false);
            HasRequired(t => t.NIISimulationType)
                .WithMany()
                .HasForeignKey(t => t.NIISimulationTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
