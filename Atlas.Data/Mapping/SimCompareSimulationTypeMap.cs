﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  SimCompareSimulationTypeMap : EntityTypeConfiguration<SimCompareSimulationType>
    {
        public SimCompareSimulationTypeMap()
        {
            

            // A Two Income Graph Report has excatly one left Income Graph.
            // An Income graph can be a "left income graph" on many Two Income Graph Reports
            HasRequired(t => t.SimulationType)
                .WithMany()
                .HasForeignKey(t => t.SimulationTypeId)
                .WillCascadeOnDelete(false);

        }
    }
}
