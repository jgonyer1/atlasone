﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  HistoricalBalanceSheetNIIMap : EntityTypeConfiguration<HistoricalBalanceSheetNII>
    {
        public HistoricalBalanceSheetNIIMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            HasRequired(t => t.LeftChart)
               .WithMany()
               .HasForeignKey(t => t.LeftChartId)
               .WillCascadeOnDelete(false);


            HasRequired(t => t.RightChart)
                .WithMany()
                .HasForeignKey(t => t.RightChartId)
                .WillCascadeOnDelete(false);
        }
    }
}
