﻿ using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    class ClassificationOrderMap : EntityTypeConfiguration<ClassificationOrder>
    {
        public ClassificationOrderMap()
        {
            // Attendance has a composite key: SessionId and PersonId
            HasKey(i => new { i.Acc_Type, i.RBC_Tier });
        }
    }
}
