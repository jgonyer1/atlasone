﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    public class CategoryTypeMap : EntityTypeConfiguration<CategoryType>
    {
        public CategoryTypeMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
