﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class BasicSurplusCollateralTypeMap : EntityTypeConfiguration<BasicSurplusCollateralType>
    {
        public BasicSurplusCollateralTypeMap()
        {
            HasRequired(t => t.CollateralType)
                .WithMany()
                .HasForeignKey(t => t.CollateralTypeId)
                .WillCascadeOnDelete(false);
        }
    }
}
