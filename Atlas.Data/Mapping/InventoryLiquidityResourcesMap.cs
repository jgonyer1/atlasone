﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  InventoryLiquidityResourcesMap : EntityTypeConfiguration<InventoryLiquidityResources>
    {
        public InventoryLiquidityResourcesMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // A Two Income Graph Report has excatly one left Income Graph.
        }
    }
}
