﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class InvestmentPortfolioValuationMap : EntityTypeConfiguration<InvestmentPortfolioValuation>
    {
        public InvestmentPortfolioValuationMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

         
        }
    }
}
