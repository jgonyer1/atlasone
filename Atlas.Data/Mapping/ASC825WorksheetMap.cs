﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class ASC825WorksheetMap : EntityTypeConfiguration<ASC825Worksheet>
    {
        public ASC825WorksheetMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

         
        }
    }
}
