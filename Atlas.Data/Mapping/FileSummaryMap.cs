﻿using System;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model.Atlas;

namespace Atlas.Institution.Data.Mapping
{
    public class FileSummaryMap : EntityTypeConfiguration<FileSummary>
    {
        public FileSummaryMap()
        {
            HasKey(fs => fs.FileID);
            Property(fs => fs.OwnerType).IsRequired();
            Property(fs => fs.OwnerID).IsRequired();
            Property(fs => fs.UploadName).IsOptional();
            Property(fs => fs.ContentType).IsRequired();
            Property(fs => fs.Size).IsRequired();
            Property(fs => fs.DateUploaded).IsRequired();
        }
    }
}
