﻿ using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace  Atlas.Institution.Data.Mapping
{
    class BalanceSheetGroupMap : EntityTypeConfiguration<BalanceSheetGroup>
    {
        public BalanceSheetGroupMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
