﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  CoreFundingUtilizationMap : EntityTypeConfiguration< CoreFundingUtilization >
    {
        public CoreFundingUtilizationMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //Simulation Mapping
            HasRequired(t => t.SimulationType)
                .WithMany()
                .HasForeignKey(t => t.SimulationTypeId)
                .WillCascadeOnDelete(false);

        }
    }
}
