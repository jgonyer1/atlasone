﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  BalanceSheetCompareMap : EntityTypeConfiguration< BalanceSheetCompare>
    {
        public BalanceSheetCompareMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // A Two Income Graph Report has excatly one left Income Graph.
            // An Income graph can be a "left income graph" on many Two Income Graph Reports
            HasRequired(t => t.LeftSimulationType)
                .WithMany()
                .HasForeignKey(t => t.LeftSimulationTypeId)
                .WillCascadeOnDelete(false);

            // A Two Income Graph Report has excatly one right Income Graph.
            // An Income graph can be a "right income graph" on many Two Income Graph Reports
            HasRequired(t => t.RightSimulationType)
                .WithMany()
                .HasForeignKey(t => t.RightSimulationTypeId)
                .WillCascadeOnDelete(false);

            // A Two Income Graph Report has excatly one left Income Graph.
            // An Income graph can be a "left income graph" on many Two Income Graph Reports
            HasRequired(t => t.LeftScenarioType)
                .WithMany()
                .HasForeignKey(t => t.LeftScenarioTypeId)
                .WillCascadeOnDelete(false);

            // A Two Income Graph Report has excatly one right Income Graph.
            // An Income graph can be a "right income graph" on many Two Income Graph Reports
            HasRequired(t => t.RightScenarioType)
                .WithMany()
                .HasForeignKey(t => t.RightScenarioTypeId)
                .WillCascadeOnDelete(false);

        }
    }
}
