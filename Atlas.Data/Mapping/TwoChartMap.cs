﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class TwoChartMap : EntityTypeConfiguration<TwoChart>
    {

        public TwoChartMap()
        {

            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // A Two Income Graph Report has excatly one left Income Graph.
            // An Income graph can be a "left income graph" on many Two Income Graph Reports
            HasRequired(t => t.LeftChart)
                .WithMany()
                .HasForeignKey(t => t.LeftChartId)
                .WillCascadeOnDelete(false);

            // A Two Income Graph Report has excatly one right Income Graph.
            // An Income graph can be a "right income graph" on many Two Income Graph Reports
            HasRequired(t => t.RightChart)
                .WithMany()
                .HasForeignKey(t => t.RightChartId)
                .WillCascadeOnDelete(false);
        }
    }
}
