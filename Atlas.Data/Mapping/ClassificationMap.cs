﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    public class ClassificationMap : EntityTypeConfiguration<Classification>
    {
        public ClassificationMap()
        {
            HasKey(i => new { i.IsAsset, i.AccountTypeId, i.RbcTier });
        }
    }
}
