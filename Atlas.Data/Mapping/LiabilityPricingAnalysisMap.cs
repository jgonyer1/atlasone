﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class LiabilityPricingAnalysisMap : EntityTypeConfiguration<LiabilityPricingAnalysis>
    {
        public LiabilityPricingAnalysisMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
