﻿ using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    class AccountingTypeMap : EntityTypeConfiguration<AccountingType>
    {
        public AccountingTypeMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
