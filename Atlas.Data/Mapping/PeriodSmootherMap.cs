﻿ using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    class PeriodSmootherMap : EntityTypeConfiguration<PeriodSmoother>
    {
        public PeriodSmootherMap()
        {
            // Attendance has a composite key: SessionId and PersonId
            HasKey(i => new { i.Period });
        }
    }
}
