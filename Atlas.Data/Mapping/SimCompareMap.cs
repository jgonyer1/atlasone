﻿using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Data.Mapping
{
    class  SimCompareMap : EntityTypeConfiguration< SimCompare>
    {
        public SimCompareMap()
        {
            Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //HasRequired(t => t.LeftSimulationType)
            //    .WithMany()
            //    .HasForeignKey(t => t.LeftSimulationTypeId)
            //    .WillCascadeOnDelete(false);

            // A Two Income Graph Report has excatly one right Income Graph.
            // An Income graph can be a "right income graph" on many Two Income Graph Reports
            //HasRequired(t => t.RightSimulationType)
            //    .WithMany()
            //    .HasForeignKey(t => t.RightSimulationTypeId)
            //    .WillCascadeOnDelete(false);
        

            // A Two Income Graph Report has excatly one left Income Graph.
            // An Income graph can be a "left income graph" on many Two Income Graph Reports
            //HasRequired(l => l.LeftSimCompareSimulationTypes)
            //    .WithMany()
            //    .HasForeignKey(t =>  t.LeftSimCompareSimulationTypeId )
            //    .WillCascadeOnDelete(false);

            // A Two Income Graph Report has excatly one right Income Graph.
            // An Income graph can be a "right income graph" on many Two Income Graph Reports
            //HasRequired(l => l.RightSimCompareSimulationTypes)
            //    .WithMany()
            //    .HasForeignKey(t => t.RightSimCompareSimulationTypeId)
            //    .WillCascadeOnDelete(false);
        }
    }
}
