﻿ using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Atlas.Institution.Model;

namespace Atlas.Institution.Data.Mapping
{
    class BalanceSheetMixLookupMap : EntityTypeConfiguration<BalanceSheetMixLookup>
    {
        public BalanceSheetMixLookupMap()
        {
            // Attendance has a composite key: SessionId and PersonId
            HasKey(i => new { i.IsAsset, i.Acc_Type, i.RBC_Tier });
        }
    }
}
