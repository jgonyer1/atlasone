﻿using System;

namespace Atlas.Institution.Model.Helpers
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class DecimalPrecisionAttribute : Attribute
    {
        public DecimalPrecisionAttribute(byte precision, byte scale)
        {
            if (precision < 1 || precision > 38) throw new ArgumentOutOfRangeException("precision", "Precision must be in the range 1-38 inclusive");

            if (scale > precision) throw new ArgumentOutOfRangeException("scale", "Scale must be less than or equal to precision");

            Precision = precision;
            Scale = scale;
        }

        public byte Precision { get; private set; }
        public byte Scale { get; private set; }
    }
}
