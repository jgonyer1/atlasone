﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Atlas.Institution.Model
{

    //This class uses an existing table in the DDW database.
    //The Db must be patched to add column BankName to the "dbo.asOfDateInfo" table.

    //Setup code for this table must be removed the from the inital migration,
    //otherwise EF will try to create a table for this class.
    //To create initiatl migration run this command in the package manage: "add-migration initial"
    //then remove all code pretaining to creating the "dbo.asOfDateInfo" table and its constaints
    //replace it with: 
    
    // AddColumn("dbo.asOfDateInfo", "BankName", c => c.String(nullable:false));

    [Table("asOfDateInfo")]
     public class Information
    {
        public int Id { get; set; }
        //public string BankName { get; set; }
        [Column("asOfDate")]
        public DateTime AsOfDate { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public virtual ICollection<Simulation> Simulations { get; set; }

        public int BasisDate { get; set; }
    }
}
