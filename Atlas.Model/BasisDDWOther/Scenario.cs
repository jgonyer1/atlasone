﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Xml.Linq;

namespace Atlas.Institution.Model
{

    [Table("Basis_Scenario")]
    public class Scenario
    {
        public int SimulationId { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual Simulation Simulation { get; set; }

        [StringLength(100)]
        public string Number { get; set; }
        [StringLength(100)]
        public string Switch { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int Months { get; set; }
        public int Quarters { get; set; }
        public int Years { get; set; }
        [StringLength(100)]
        public string GYears { get; set; }
        [StringLength(100)]
        public string GYearsSL { get; set; }
        public int Variance { get; set; }
        public int AdvSimu { get; set; }
        public int MarketValue { get; set; }
        public double ExcessLoan { get; set; }
        [StringLength(100)]
        public string ReportStatus { get; set; }
        [StringLength(100)]
        public string ReportDesc { get; set; }
        [StringLength(100)]
        public string FBudget { get; set; }
        [StringLength(100)]
        public string LBudget { get; set; }
        public int BalanceFlag { get; set; }
        [StringLength(100)]
        public string Fill1 { get; set; }
        public int NeedToUpdate { get; set; }
        [StringLength(100)]
        public string AdvRS { get; set; }
        [StringLength(100)]
        public string AdvNII { get; set; }
        [StringLength(100)]
        public string AdvMVPE { get; set; }
        [StringLength(100)]
        public string AdvExclude { get; set; }
        [StringLength(100)]
        public string AdvScenario { get; set; }
        [StringLength(1000)]
        public string AdvPS { get; set; }
        [StringLength(100)]
        public string Note { get; set; }
        [StringLength(100)]
        public string WebIndexScenSeq { get; set; }
        [StringLength(100)]
        public string IndEVEAdjType { get; set; }
        [StringLength(100)]
        public string IndEVEOffsetPeriod { get; set; }
        [StringLength(100)]
        public string IndEVEScen1 { get; set; }
        [StringLength(100)]
        public string IndEVEScen2 { get; set; }
        [StringLength(100)]
        public string IndEVEShockVal { get; set; }
        [StringLength(100)]
        public string DDWScenCode { get; set; }
        [StringLength(100)]
        public string DDWEVEScenCode { get; set; }
        public int DDWSRamp { get; set; }
        public int DDWERamp { get; set; }

        [StringLength(100)]
        public string LastAtlasPush { get; set; }
        [StringLength(50)]
        public string AtlasPushUserId { get; set; }

        // gets all net income record for a unit of time in basis
        // e.g. nMonths = 1 would return all net income records by month
        // e.g. nMonths = 3 would return all net income records by quarter,
        // for quartery data: there there would be a two year gap in the beguinning where only monthy records exist


        public bool IsBase()
        {
            return Name.ToLower().Contains("base");
        }

        public string StandardName()
        {
            return IsBase() ? "base" : Name.ToLower();
        }
    }
}
