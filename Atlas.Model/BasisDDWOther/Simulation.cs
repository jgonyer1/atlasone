﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Atlas.Institution.Model
{
    public class Simulation
    {
        public int Id { get; set; }
        public int InformationId { get; set; }
        public int SimulationTypeId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int Priority { get; set; }
        public virtual Information Information { get; set; }
        public virtual ICollection<Scenario> Scenarios { get; set; }
    }
}
