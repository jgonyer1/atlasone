﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelinesEVEComparative")]
    public class InterestRatePolicyGuidelinesEVEComparative : InterestRatePolicyGuidelinesRow
    {
        public int InterestRatePolicyGuidelinesId { get; set; }
        public virtual InterestRatePolicyGuidelines InterestRatePolicyGuidelines { get; set; }
        [StringLength(100)]
        public string Type { get; set; }

        public virtual ICollection<InterestRatePolicyGuidelinesEVEScenario> Scenarios { get; set; }
    }
}
