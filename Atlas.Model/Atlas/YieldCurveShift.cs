﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_YieldCurveShifts")]
    public class YieldCurveShift 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string WebRateAsOfDate { get; set; }
        [StringLength(100)]
        public string Index { get; set; }

        public bool OverrideDistrict { get; set; }

        public bool OverrideWebRateDate { get; set; }
        [StringLength(100)]
        public string FHLBAdvanceDistrict { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ICollection<YieldCurveShiftScenarioType> YieldCurveShiftScenarioTypes { get; set; }
        public virtual ICollection<YieldCurveShiftPeriodCompare> YieldCurveShiftPeriodCompares { get; set; }
        public virtual ICollection<YieldCurveShiftHistoricDates> YieldCurveShiftHistoricDates { get; set; }
        
    }
}
