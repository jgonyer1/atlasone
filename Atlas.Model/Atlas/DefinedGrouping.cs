﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_DefinedGrouping")]
    public class DefinedGrouping
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public DateTime LastModified { get; set; }
        [StringLength(100)]
        public string LastModifiedBy { get; set; }
        public virtual ICollection<DefinedGroupingGroup> DefinedGroupingGroups{ get; set; }
    }
}
