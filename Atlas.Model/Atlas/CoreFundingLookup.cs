﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CoreFundingLookup")]
    public class CoreFundingLookup
    {
        public int IsAsset { get; set; }
        public int Acc_Type { get; set; }
        public int RBC_Tier { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
    }
}
