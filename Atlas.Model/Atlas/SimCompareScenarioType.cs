﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{

    [Table("ATLAS_SimCompareScenarioType")]
    public class SimCompareScenarioType
    {
        public int Id { get; set; }
        public int SimCompareId { get; set; }
        public virtual SimCompare SimCompare { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
        public int Priority { get; set; }
        
        
    }
}
