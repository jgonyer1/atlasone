﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LBLookback")]
    public class LBLookback
    {
        public int Id {get; set;}
        [StringLength(100)]
        public string Name { get; set; }
        public int Priority { get; set;}
        public bool OverrideSimulationTypeId { get; set; }
        public int SimulationTypeId { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public bool OverrideScenarioTypeId { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }

        public DateTime AsOfDate { get; set;}

        public virtual ICollection<LBLookbackGroup> LookbackGroups { get; set; }
    }
}
