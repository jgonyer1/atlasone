﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusReport")]
    public class BasicSurplusReport 
    {
        public int Id { get; set; }

        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public int BasicSurplusAdminId { get; set; }

        public bool OverrideBasicSurplus { get; set; }

        public bool IncludePolicies { get; set; }

        public virtual BasicSurplusAdmin BasicSurplusAdmin { get; set; }
    }
}
