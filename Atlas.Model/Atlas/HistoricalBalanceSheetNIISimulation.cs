﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_HistoricalBalanceSheetNIISimulation")]
    public class HistoricalBalanceSheetNIISimulation
    {
        public int Id { get; set; }
        public int HistoricalBalanceSheetNIIId { get; set; }
        public int AsOfDateOffset { get; set; }
        public virtual HistoricalBalanceSheetNII HistoricalBalanceSheetNII { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public bool OverrideSimulation { get; set; }
        public int SimulationTypeId { get; set; }
        public int DateOffset { get; set; }
        public int Priority { get; set; }
 
    }
}
