﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_SimulationSummary")]
    public class SimulationSummary 
    {
        public int Id { get; set; }

        public int AsOfDateOffset { get; set; }
        public int ReportSelection { get; set; }
        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }
        [StringLength(100)]
        public string MonthlyQuarterly { get; set; }
        public bool TaxEquivalent { get; set; }
        [StringLength(100)]
        public string Grouping { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
    }
}
