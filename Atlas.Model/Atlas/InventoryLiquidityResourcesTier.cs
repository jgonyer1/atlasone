﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InventoryLiquidityResourcesTier")]
    public class InventoryLiquidityResourcesTier
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public double Amount { get; set; }

        public string CustomName { get; set; }

        public double CumulativeAmount { get; set; }
        public double BasicSurplusAmount { get; set; }

        public int Priority { get; set; }
        public int InventoryLiquidityResourcesId { get; set; }
        public virtual InventoryLiquidityResources InventoryLiquidityResources { get; set; }

        public virtual ICollection<InventoryLiquidityResourcesTierDetail> InventoryLiquidityResourcesTierDetail { get; set; }

    }
}
