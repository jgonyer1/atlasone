﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminOtherLiquidity")]
    public class BasicSurplusAdminOtherLiquidity : BasicSurplusAdminRow
    {
        public decimal MarketValue { get; set; }
        public decimal Pledged { get; set; }
        public decimal Available { get; set; }
    }
}
