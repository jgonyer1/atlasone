﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_NIIPolicy")]
    public class NIIPolicy
    {
        public int Id { get; set; }
        public int NIIPolicyParentId { get; set; }
        public virtual NIIPolicyParent NIIPolicyParent { get; set; }
        public double PolicyLimit { get; set; }
        public int RiskAssessment { get; set; }
        public int Priority { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
        public double CurrentRatio { get; set; }

    }
}
