﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ModelSetup")]
    public class ModelSetup
    {
        public int Id { get; set; }
        public DateTime AsOfDate { get; set; }

        [StringLength(100)]
        public string PrepaymentType { get; set; }
        [StringLength(100)]
        public string OtherLoanPrepaymentType { get; set; }
        public DateTime LoanRatesDate { get; set; }
        [StringLength(100)]
        public string InvestmentType { get; set; }

        //This is for tax equiv income
        public bool ReportTaxEquivalent { get; set; }

        public bool ReportTaxEquivalentYield { get; set; }

        public bool IncludeOffBalanceSheet { get; set; }
        [StringLength(100)]
        public string AvgLifeAssumption { get; set; }
        public double AvgLifeYears { get; set; }
        [StringLength(500)]
        public string AvgLifeMessage { get; set; }
        public double NetNewLoans { get; set; }
        public double NetNewDeposits { get; set; }
    }
}
