﻿namespace Atlas.Institution.Model.Atlas
{
    public enum DocumentOrientation : byte
    {
        Landscape = 1,
        Portrait
    }
}
