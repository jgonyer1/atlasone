﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{

    [Table("ATLAS_Chart")]
    public class Chart
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public bool NameOverride { get; set; }
        public bool IsQuarterly { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public bool OverrideParentSimulationType { get; set; }
        public bool OverrideSimulationTypeId { get; set; }
        //public int SimulationTypeId { get; set; }
        //public virtual SimulationType SimulationType { get; set; }
        public virtual ICollection<ChartScenarioType> ChartScenarioTypes { get; set; }

        //Whether to show Percent Change, Percent Change with Policies, or show now Percent Change
        //0 = None
        //1 = Percent Change
        //2 = Percent Change With Policies
        public int PercentChange { get; set; }

        //If showing percent change, which type to show
        //Corresponds to Id field o ATLAS_NIIPolicyParent
        //year1Year1 = Yr 1 % Change from Yr 1 Base
        //year2Year1 = Yr 2 % Change from Yr 1 Base, 
        //year2Year2 = Yr 2 % Change from Yr 2 Base, 
        //24Month = 24M % Change from Base
        public string PercChangeComparator { get; set; }

        [StringLength(100)]
        public string ComparativeScenario { get; set; }
        public virtual ICollection<ChartSimulationType> ChartSimulationTypes { get; set; }
        public int NumberOfYears()
        {
            return IsQuarterly ? 5 : 2;
        }

        public int NumberOfPeriods()
        {
            return IsQuarterly ? 20 : 24;
        }
    }
}

