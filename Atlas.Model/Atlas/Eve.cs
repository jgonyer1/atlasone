﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Eves")]
    public class Eve 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public bool OverrideSimulationId { get; set; }
        public int SimulationTypeId { get; set; }
        [StringLength(100)]
        public string ViewOption { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ICollection<EveScenarioType> EveScenarioTypes { get; set; }
        public bool PolicyLimits { get; set; }
        public bool RiskSummaryTable { get; set; }
        public bool ShowNCUA { get; set; }
        public string ReportFormat { get; set; }

    }
}
