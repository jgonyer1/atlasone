﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_HistoricalBalanceSheetNIIBottomFootNotes")]
    public class HistoricalBalanceSheetNIIBottomFootNotes
    {
        public int Id { get; set; }
        public int HistoricalBalanceSheetNIIId { get; set; }
        public virtual HistoricalBalanceSheetNII HistoricalBalanceSheetNII { get; set; }
        public int Priority { get; set; }
        [StringLength(500)]
        public string Text { get; set; }
    }
}
