﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LBCustomLayoutClassifications")]
    public class LBCustomLayoutClassification
    {
        public int Id { get; set; }
        public bool IsAsset { get; set; }
        public int Acc_Type { get; set; }
        public int RbcTier { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int LBCustomLayoutRowItemId { get; set; }
        public virtual LBCustomLayoutRowItem LBCustomLayoutRowItem { get; set; }
    }
}
