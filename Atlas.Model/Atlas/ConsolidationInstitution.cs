﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ConsolidationInstitution")]
    public class ConsolidationInstitution
    {
        public int Id { get; set; }
        public int ConsolidationId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string InstitutionDatabaseName { get; set; }
        public virtual Consolidation Consolidation { get; set; }
    }
}
