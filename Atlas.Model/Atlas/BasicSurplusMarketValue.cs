﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusMarketValue")]
    public class BasicSurplusMarketValue
    {
        public int Id { get; set; }
        public int BasicSurplusAdminId { get; set; }
        public virtual BasicSurplusAdmin BasicSurplusAdmin { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public decimal TreasuryAgencyBonds { get; set; }
        public decimal MBSCMOAgency { get; set; }
        public decimal MBSCMOPvt { get; set; }
        public decimal CorporateSecurities { get; set; }
        public decimal MunicipalSecurities { get; set; }
        public decimal EquitySecurities { get; set; }
        public decimal OtherInvestments { get; set; }
        public decimal Total { get; set; }
    }
}
