﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LiquidityProjectionDateOffset")]
    public class LiquidityProjectionDateOffset
    {
        public int Id { get; set; }
        public int LiquidityProjectionId { get; set; }
        public virtual LiquidityProjection LiquidityProjection { get; set; }
        public int Priority { get; set; }
        public bool Override { get; set; }

        public int CurOffSet { get; set; }
        public int PrevOffSet { get; set; }
        public int CurBasicSurplusAdminId { get; set; }
        public int PrevBasicSurplusAdminId { get; set; }
        public int CurNIISimulationTypeId { get; set; }
        public int PrevNIISimulationTypeId { get; set; }
        public int CurScenarioTypeId { get; set; }
        public int PrevScenarioTypeId { get; set; }
    }
}
