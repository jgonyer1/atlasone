﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BalanceSheetMixes")]
    public class BalanceSheetMix 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }

        public string PriorInstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public int PriorAsOfDateOffset { get; set; }
        public int SimulationTypeId { get; set; }

        public int PriorSimulationTypeId { get; set; }

        [StringLength(100)]
        public string LiabilityOption { get; set; }
        public bool NationalCD { get; set; }

        public virtual SimulationType SimulationType { get; set; }

        public bool OverrideSimulationId { get; set; }


        public bool OverridePriorSimulationId { get; set; }
    }
}
