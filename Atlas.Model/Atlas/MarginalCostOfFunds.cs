﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_MarginalCostOfFunds")]
    public class MarginalCostOfFunds
    {
        public int Id { get; set; }
        public bool IsReduction { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string AccountName { get; set; }
        public double Balance { get; set;}
        public double Rate { get; set;}
        [StringLength(100)]
        public string Runoffs { get; set;}
        [StringLength(100)]
        public string RateChanges { get; set;}
    }
}
