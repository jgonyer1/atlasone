﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InventoryLiquidityResourcesWholesaleFunding")]
    public class InventoryLiquidityResourcesWholesaleFunding
    {
        public int Id { get; set; }

        public bool Overide { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public double Policy { get; set; }

        public string PercentageType { get; set; }

        public double AmountPerPolicy { get; set; }
        public double Outstanding { get; set; }

        public double AvailableFunding { get; set; }
        public double AvailableFundingPerPolicy { get; set; }
        public double AvailableFundingPerTotalWholesalePolicy { get; set; }

        public int Priority { get; set; }
        public int InventoryLiquidityResourcesId { get; set; }
        public virtual InventoryLiquidityResources InventoryLiquidityResources { get; set; }


    }
}
