﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LiquidityPolicySetting")]
    public class LiquidityPolicySetting
    {
        public int Id { get; set; }
        public int LiquidityPolicyId { get; set; }
        public virtual LiquidityPolicy LiquidityPolicy { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ShortName { get; set; }
        public bool Include { get; set; }
        public int Priority { get; set; }

    }
}
