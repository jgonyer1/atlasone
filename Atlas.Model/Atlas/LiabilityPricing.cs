﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LiabilityPricing")]
    public class LiabilityPricing
    {
        public int Id { get; set; }
        public DateTime AsOfDate { get; set; }

        [StringLength(100)]
        public string WebRateAsOfDate { get; set; }
        [StringLength(100)]
        public string FHLBDistrict { get; set; }


        public DateTime DepositRateAsOfDate { get; set; }

        public virtual ICollection<NonMaturityDepositRate> NonMaturityDepositRates { get; set; }
        public virtual ICollection<TimeDepositRate> TimeDepositRates { get; set; }
        public virtual ICollection<TimeDepositSpecialRate> TimeDepositSpecialRates { get; set; }
        [StringLength(100)]
        public string HistoricalWebRateDate { get; set; }
        [StringLength(100)]
        public string HistoricalRateCurve { get; set; }
        public DateTime HistoricalRateEffDate { get; set; }
        public virtual ICollection<HistoricalTimeDepositRate> HistoricalTimeDepositRates { get; set; }
        public virtual ICollection<HistoricalTimeDepositSpecialRate> HistoricalTimeDepositSpecialRates { get; set; }

    }
}
