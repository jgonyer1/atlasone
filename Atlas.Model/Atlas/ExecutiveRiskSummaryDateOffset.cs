﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummaryDateOffset")]
    public class ExecutiveRiskSummaryDateOffsets
    {
        public int Id { get; set; }
        public int ExecutiveRiskSummaryId { get; set; }
        public virtual ExecutiveRiskSummary ExecutiveRiskSummary { get; set; }
        public int OffSet { get; set; }
        public int Priority { get; set; }

        public int BasicSurplusAdminId { get; set; }

        public int NIISimulationTypeId { get; set; }

        public int EveSimulationTypeId { get; set; }

        public bool TaxEquivIncome { get; set; }
        public bool TaxEquivYield { get; set; }

        public bool OverrideBasicSurplus { get; set; }


    }
}
