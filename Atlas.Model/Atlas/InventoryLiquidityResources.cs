﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InventoryLiquidityResources")]
    public class InventoryLiquidityResources
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }

        public int BasicSurplusId { get; set; }

        public bool OverrideBasicSurplus { get; set; }

        public bool IncludePolicy { get; set; }
        public bool IncludeVol { get; set; }
        public bool IncludeBalSheet { get; set; }

        public bool OverrideCalculatedFunding { get; set; }


        public bool OverrideBalanceSheetLabel { get; set; }

        public bool OverrideBalanceSheetValue { get; set; }

        public virtual ICollection<InventoryLiquidityResourcesDateOffsets> InventoryLiquidityResourcesDateOffsets { get; set; }
        public virtual ICollection<InventoryLiquidityResourcesTier> InventoryLiquidityResourcesTier { get; set; }

        public virtual ICollection<InventoryLiquidityResourcesWholesaleFunding> InventoryLiquidityResourcesWholesaleFunding { get; set; }

    }
}
