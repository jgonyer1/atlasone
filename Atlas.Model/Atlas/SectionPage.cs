﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_SectionPage")]
    public class SectionPage
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Title { get; set; }
        public bool Appendix { get; set; }
    }
}
