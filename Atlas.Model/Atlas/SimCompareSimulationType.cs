﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{

    [Table("ATLAS_SimCompareSimulationType")]
    public class SimCompareSimulationType
    {
        //These can be used in SimCompare, OneChart, and TwoChart
        public int Id { get; set; }
        public int Side { get; set; }
        public int SimCompareId { get; set; }
        public virtual SimCompare SimCompare { get; set; }
        public int AsOfDateOffset { get; set; }
        public int SimulationTypeId { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public int Priority { get; set; }
    }
}
