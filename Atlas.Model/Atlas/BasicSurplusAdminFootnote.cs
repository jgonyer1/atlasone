﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminFootnote")]
    public class BasicSurplusAdminFootnote : BasicSurplusAdminRow
    {
        [StringLength(500)]
        public string Text { get; set; }
    }
}
