﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_PeriodSmoother")]
    public class PeriodSmoother
    {
        public int Period { get; set; }
    }
}
