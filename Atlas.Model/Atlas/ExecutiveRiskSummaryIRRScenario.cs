﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveSummaryIRRScenario")]
    public class ExecutiveRiskSummaryIRRScenario
    {
        public int Id { get; set; }
        public int Priority { get; set; }

        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }

        public bool OverrideRiskAssesment { get; set; }
        public int RiskAssesment { get; set; }

        public int ExecutiveRiskSummaryIRRDetId { get; set; }
        public virtual ExecutiveRiskSummaryIRRDet ExecutiveRiskSummaryIRRDet { get; set; }
    }
}
