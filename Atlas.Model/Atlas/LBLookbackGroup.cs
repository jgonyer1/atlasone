﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LBLookbackGroup")]
    public class LBLookbackGroup
    {
        public int Id { get; set;}

        public int LBLookbackId { get; set; }
        public virtual LBLookback LBLookback { get; set; }

        public virtual ICollection<LBLookbackData> LBLookbackData { get; set; }

        public int Priority { get; set;}
        public int LayoutId { get; set; }
        public int LBCustomTypeId { get; set; }
        public virtual LBCustomType LBCustomType { get; set; }
        public int StartDateOffset { get; set;}
        public int EndDateOffset { get; set;}
        public bool RateVol { get; set;}
        public bool OverrideTaxEquivalent { get; set; }
        public bool TaxEquivalent { get; set; }//Tax Equiv INCOME
        public DateTime StartDate { get; set;}
        public DateTime EndDate { get; set;}
        public int StartToEndDays { get; set; }
        public int YearFromStartDays { get; set;}
    }
}
