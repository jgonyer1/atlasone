﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminLoanCollateral")]
    public class BasicSurplusAdminLoanCollateral : BasicSurplusAdminRow
    {
        public decimal AmountPledged { get; set; }
        public decimal CollateralValue { get; set; }
        public decimal NetCollateral { get; set; }
        public decimal L360Collateral { get; set; }
    }
}
