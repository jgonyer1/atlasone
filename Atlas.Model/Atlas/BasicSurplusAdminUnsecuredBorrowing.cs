﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminUnsecuredBorrowing")]
    public class BasicSurplusAdminUnsecuredBorrowing : BasicSurplusAdminRow
    {
        public decimal Line { get; set; }
        public decimal Outstanding { get; set; }
        public decimal Available { get; set; }
    }
}
