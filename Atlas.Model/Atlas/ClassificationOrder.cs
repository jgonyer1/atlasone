﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ClassificationOrder")]
    public class ClassificationOrder
    {
        public int Acc_Type { get; set; }
        public int RBC_Tier { get; set; }
        public int Order { get; set; }
    }
}
