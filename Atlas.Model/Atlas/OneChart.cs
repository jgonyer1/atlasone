﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_OneCharts")]
    public class OneChart 
    {
        public int Id { get; set; }
        public int ChartId { get; set; }
        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEquivalentIncome { get; set; }
        public virtual Chart Chart { get; set; }
        public string NiiOption { get; set; }
    }

}