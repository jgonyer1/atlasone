﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminUnrealizedGL")]
    public class BasicSurplusAdminUnrealizedGL : BasicSurplusAdminRow
    {
        public decimal TotalPortfolio { get; set; }
        public decimal Tier1BasicSurplus { get; set; }
        public decimal PercOfAssets { get; set; }
    }
}
