﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LBLookbackData")]
    public class LBLookbackData
    {
        public int Id {get; set;}

        public int LBLookbackGroupId { get; set; }
        public virtual LBLookbackGroup LBLookbackGroup { get; set; }
        public int LBCustomLayoutRowItemId { get; set; }
        

        public int Priority { get; set;}
        public bool IsAsset {get; set;}
        [StringLength(100)]
        public string Title { get; set;}

        public double Proj_AvgBal { get; set; }
        public double Proj_AvgBal_Adj { get; set; }
        public double Proj_AvgRate { get; set; }
        public double Proj_IncExp { get; set; }
        public double Proj_IncExp_Adj { get; set;}

        public double Actual_AvgBal { get; set; }
        public double Actual_AvgRate { get; set; }
        public double Actual_IncExp{ get; set; }
        
        public double Variance_ValDueToRate { get; set;}
        public double Variance_ValDueToVol { get; set; }
        public double Variance_Val { get; set; }
        public double Variance_Perc { get; set;}

        public bool IsSectionHeader { get; set; }
        public bool IsTotalRow { get;set;}
        public bool HideFromAdmin { get; set;}
        public bool IsCalculatedRow { get; set;}
        public int RowType { get; set;}
    }
}
