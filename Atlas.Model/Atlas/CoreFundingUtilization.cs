﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CoreFundingUtilizations")]
    public class CoreFundingUtilization 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int SimulationTypeId { get; set; }
        public bool OverrideSimulation { get; set;}
        public int AsOfDateOffset { get; set;}
        [StringLength(100)]
        public string AssetDetails { get; set; }
        public bool ShowPolicies { get; set;}
        public virtual SimulationType SimulationType { get; set; }
        public virtual ICollection<CoreFundingUtilizationScenarioType> CoreFundingUtilizationScenarioTypes { get; set; }
        public virtual ICollection<CoreFundingUtilizationDateOffSets> CoreFundingUtilizationDateOffSets { get; set; }
      

    }
}
