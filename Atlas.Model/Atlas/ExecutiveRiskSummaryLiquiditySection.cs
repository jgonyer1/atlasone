﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummaryLiquiditySection")]
    public class ExecutiveRiskSummaryLiquiditySection
    {
        public int Id { get; set; }
        public int Priority { get; set;}

        public int ReportSectionTypeId { get; set; }
        [StringLength(100)]
        public string ReportSectionTypeName { get; set; }

        [StringLength(100)]
        public string ReportSide { get; set; }

        public virtual ICollection<ExecutiveRiskSummaryLiquiditySectionDet> ExecutiveRiskSummaryLiquiditySectionDets { get; set;}

        public int ExecutiveRiskSummaryId { get; set; }
        public virtual ExecutiveRiskSummary ExecutiveRiskSummary { get; set;}

        public bool HideLabel { get; set; }
    }
}
