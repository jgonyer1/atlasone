﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelinesEVEScenario")]
    public class InterestRatePolicyGuidelinesEVEScenario : InterestRatePolicyGuidelinesRow
    {
        public int InterestRatePolicyGuidelinesEVEComparativeId { get; set; }
        public virtual InterestRatePolicyGuidelinesEVEComparative InterestRatePolicyGuidelinesEVEComparative { get; set; }

        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
    }
}
