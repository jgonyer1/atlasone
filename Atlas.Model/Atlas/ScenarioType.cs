﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ScenarioType")]
    public class ScenarioType
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string FileName { get; set; }
        public int Priority { get; set; }
        public int RateMovement { get; set; }
        public bool IsEve { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<ChartScenarioType> ChartScenarioTypes { get; set; }
        public virtual ICollection<SimCompareScenarioType> SimCompareScenarioTypes { get; set; }
        public virtual ICollection<EveScenarioType> EveScenarioTypes { get; set; }
        public virtual ICollection<CoreFundingUtilizationScenarioType> CoreFundingUtilizationScenarioTypes { get; set; }



     
        public bool IsBase()
        {
            return Name.ToLower().Contains("base");
        }
    }
}
