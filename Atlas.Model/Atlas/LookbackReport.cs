﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LookbackReport")]
    public class LookbackReport
    {
        public int Id { get; set;}
        [StringLength(100)]
        public string Name { get; set;}
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set;}
        public int AsOfDateOffset { get; set;}
        public virtual ICollection<LookbackReportSection> Sections { get; set;}
    }
}
