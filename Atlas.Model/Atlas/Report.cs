﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

using Atlas.Institution.Model.Atlas;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Report")]
    public class Report
    {
        public int Id { get; set; }
        public int PackageId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int FirstPageNumber { get; set; }
        public int Priority { get; set; }
        public bool HasSplitFootnotes { get; set; } 

        public DocumentOrientation? Orientation { get; set; }
        public bool HideHeader { get; set; }
        public bool HideFooter { get; set; }
        public bool ContentsFromFile { get; set; }
        public bool ConstrainContentWithinBody { get; set; }

        public virtual Package Package { get; set; }
        public virtual ICollection<SplitFootnote> SplitFootnotes { get; set; }
        public virtual ICollection<Footnote> Footnotes { get; set; }

        public bool StaticDates { get; set; }

        //Stores the sourceReport Id if reports parent(the package) was copeid from template
        public int? SourceReportId { get; set; }

        //used if sourceReportId is not null otherwise it is not displayed to user
        public bool Defaults { get; set; }

        [StringLength(100)]
        public string ShortName { get; set; }
        [StringLength(100)]
        public string DisplayName { get; set; }
    }
}
