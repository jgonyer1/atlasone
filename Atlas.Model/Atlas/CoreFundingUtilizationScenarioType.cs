﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CoreFundingUtilizationScenarioType")]
    public class CoreFundingUtilizationScenarioType
    {
        public int Id { get; set; }
        public int CoreFundingUtilizationId { get; set; }
        public virtual CoreFundingUtilization CoreFundingUtilization { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
        public int Priority { get; set; }
 
    }
}
