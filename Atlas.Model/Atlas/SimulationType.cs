﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{

    [Table("ATLAS_SimulationType")]
    public class SimulationType
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public virtual ICollection<SimCompareScenarioType> SimCompareScenarioTypes { get; set; }
    }
}
