﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CapitalAnalysisHistoricalBufferOffSets")]
    public class CapitalAnalysisHistoricalBufferOffsets
    {
        public int Id { get; set; }
        public int CapitalAnalysisId { get; set; }
        public virtual CapitalAnalysis CapitalAnalysis { get; set; }
        public int OffSet { get; set; }
        public int Priority { get; set; }
    }
}
