﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelinesNIIScenario")]
    public class InterestRatePolicyGuidelinesNIIScenario : InterestRatePolicyGuidelinesRow
    {
        public int InterestRatePolicyGuidelinesScenarioTypeId { get; set; }
        public virtual InterestRatePolicyGuidelinesNIIScenarioType InterestRatePolicyGuidelinesScenarioType { get; set; }

        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
    }
}
