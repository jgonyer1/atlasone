﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_AccountType")]
    public class AccountType
    {
        // Sql("DBCC CHECKIDENT('dbo.AccountTypes', RESEED, 0)");
        public int Id { get; set; }
        [StringLength(100)]
        public string  Name { get; set; }
        public virtual ICollection<Classification> RbcTiers { get; set; }
    }

    
}
