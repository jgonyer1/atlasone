﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_TimeDepositGrouping")]
    public class TimeDepositGrouping
    {
        public int Id { get; set; }
        public int TermCode { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int Priority { get; set; }
    }
}
