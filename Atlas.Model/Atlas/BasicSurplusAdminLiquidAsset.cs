﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminLiquidAsset")]
    public class BasicSurplusAdminLiquidAsset : BasicSurplusAdminRow
    {
        public bool Include { get; set; }
        public decimal Percent { get; set; }
        public decimal Amount { get; set; }
    }
}
