﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InventoryLiquidityResourcesTierDetailCollection")]
    public class InventoryLiquidityResourcesTierDetailCollection
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        public bool AddNumber { get; set; }

        public int InventoryLiquidityResourcesTierDetailId { get; set; }
        public virtual InventoryLiquidityResourcesTier InventoryLiquidityResourcesDetailTier { get; set; }

    }
}
