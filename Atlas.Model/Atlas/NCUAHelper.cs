﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_NCUAHelper")]
    public class NCUAHelper
    {
        public int Id { get; set; }
        public int ScenarioTypeId { get; set; }
        public int AccountTypeId { get; set; }
        public double Percentage { get; set; }
    }
}
