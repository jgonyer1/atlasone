﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InventoryLiquidityResourcesTierDetail")]
    public class InventoryLiquidityResourcesTierDetail
    {
        public int Id { get; set; }
        [StringLength(250)]
        public string Name { get; set; }
        public double Amount { get; set; }

        public virtual ICollection<InventoryLiquidityResourcesTierDetailCollection> InventoryLiquidityResourcesTierDetailCollection { get; set; }

        public int Priority { get; set; }

        public int InventoryLiquidityResourcesTierId { get; set; }
        public virtual InventoryLiquidityResourcesTier InventoryLiquidityResourcesTier { get; set; }

    }
}
