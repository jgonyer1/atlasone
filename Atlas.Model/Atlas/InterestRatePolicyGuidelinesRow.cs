﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelinesRow")]
    public class InterestRatePolicyGuidelinesRow
    {
        public int Id { get; set; }

        public bool Graph { get; set; }

        public int Priority { get; set; }
    }
}
