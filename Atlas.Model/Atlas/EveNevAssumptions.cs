﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_EveNevAssumptions")]
    public class EveNevAssumptions 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public bool EnhancedEve { get; set; }
        public bool Override { get; set; }
        [StringLength(100)]
        public string DepositType { get; set; }
        [StringLength(500)]
        public string DepositNotes { get; set; }
    }
}
