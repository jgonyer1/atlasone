﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_YieldCurveShiftPeriodCompare")]
    public class YieldCurveShiftPeriodCompare
    {
        public int Id { get; set; }
        public int YieldCurveShiftId { get; set; }
        public virtual YieldCurveShift YieldCurveShift { get; set; }
        [StringLength(100)]
        public string IndexA { get; set; }
        [StringLength(100)]
        public string IndexB { get; set; }
        public int Priority { get; set; }
       
    }
}
