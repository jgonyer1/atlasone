﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Cashflow")]
    public class CashflowReport 
    {
        public int Id { get; set; }
        public int ReportType { get; set; }
        public int Compare { get; set; }

        public virtual ICollection<CashflowSimulationScenario> CashflowSimulationScenario { get; set; }

        public int DefinedGrouping { get; set; }
        public bool ExcludePrepayments { get; set; }
    }
}
