﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusSecuredLiabilitiesType")]
    public class BasicSurplusSecuredLiabilitiesType
    {
        public int Id { get; set; }
        public int BasicSurplusAdminId { get; set; }
        public virtual BasicSurplusAdmin BasicSurplusAdmin { get; set; }

        public int SecuredLiabilitiesTypeId { get; set; }
        public virtual SecuredLiabilitiesType SecuredLiabilitiesType { get; set; }

        public int Priority { get; set; }
        public bool Override { get; set; }
        public decimal Outstanding { get; set; }
        public decimal PledgingFactor { get; set; }
        public decimal PledgingRequired { get; set; }
        public virtual ICollection<BasicSurplusCollateralType> BasicSurplusCollateralTypes { get; set; }
    }
}
