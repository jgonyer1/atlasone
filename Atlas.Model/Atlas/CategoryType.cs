﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{

    [Table("ATLAS_CategoryType")]
    public class CategoryType
    {
        // CategoryType Ids needs to start at 0 to be compatable with Cat_Type Column in ALB table (Category Table).
        // By default Ids start at 1.
        // Add this line to the initial migration after the table is created in the up method:
        // Sql("DBCC CHECKIDENT('dbo.CategoryTypes', RESEED, 0)");
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ShortName { get; set; }
    }

}
