﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InvestmentPortfolioValuationScenario")]
    public class InvestmentPortfolioValuationScenario
    {
        public int Id { get; set; }
        public int InvestmentPortfolioValuationId { get; set; }
        public virtual InvestmentPortfolioValuation InvestmentPortfolioValuation { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string DDWName { get; set; }
        public int Priority { get; set; }
            
    }
}
