﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ASC825MarketValueScenarioType")]
    public class ASC825MarketValueScenarioType
    {
        public int Id { get; set; }
        public int ASC825MarketValueId { get; set; }
        public virtual ASC825MarketValue ASC825MarketValue { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
        public int Priority { get; set; }
            
    }
}
