﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummaryCapitalSection")]
    public class ExecutiveRiskSummaryCapitalSection
    {
        public int Id { get; set; }
        public int Priority { get; set;}

        [StringLength(100)]
        public string Name { get; set; }

        public bool OverrideRiskAssesment { get; set; }

        public int RiskAssesment { get; set; }

        [StringLength(100)]
        public string ReportSide { get; set; }

        public int ExecutiveRiskSummaryId { get; set; }
        public virtual ExecutiveRiskSummary ExecutiveRiskSummary { get; set;}
    }
}
