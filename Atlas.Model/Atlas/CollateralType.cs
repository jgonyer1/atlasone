﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CollateralType")]
    public class CollateralType
    {
        public int Id { get; set; }
        public int BasicSurplusAdminId { get; set; }
        public virtual BasicSurplusAdmin BasicSurplusAdmin { get; set; }

        public bool Override { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string FieldName { get; set; }
        public int Priority { get; set; }

        public bool Enabled { get; set; }
        public decimal CollateralValue { get; set; }
        public bool AllowCollateralValueEdit { get; set; }
    }
}
