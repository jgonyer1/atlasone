﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ASC825MarketValue")]
    public class ASC825MarketValue 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public int SimulationTypeId { get; set; }
        public virtual SimulationType SimulationType { get; set; }

        [StringLength(100)]
        public string ReportType { get; set; }
        public virtual ICollection<ASC825MarketValueScenarioType> EveScenarioTypes { get; set; } 

    }
}
