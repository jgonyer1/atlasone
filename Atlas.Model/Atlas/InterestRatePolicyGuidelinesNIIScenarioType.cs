﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelinesNIIScenarioType")]
    public class InterestRatePolicyGuidelinesNIIScenarioType : InterestRatePolicyGuidelinesRow
    {
        public int InterestRatePolicyGuidelinesNIIComparativeId { get; set; }
        public virtual InterestRatePolicyGuidelinesNIIComparative InterestRatePolicyGuidelinesNIIComparative { get; set; }
        [StringLength(100)]
        public string Type { get; set; }
        [StringLength(100)]
        public string CustomName { get; set; }

        public virtual ICollection<InterestRatePolicyGuidelinesNIIScenario> Scenarios { get; set; }
    }
}
