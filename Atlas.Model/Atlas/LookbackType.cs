﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LookbackType")]
    public class LookbackType
    {
        public int Id { get; set; }
        public int Priority { get; set; }
        [StringLength(100)]
        public string Name { get; set;}
        public int AsOfDateOffset { get; set;}
        public int LBLookbackId { get; set;}
        //public virtual LBLookback LBLookback {get;set;}

        public int LookbackReportSectionId { get; set;}
        public virtual LookbackReportSection LookbackReportSection { get; set;}

    }
}
