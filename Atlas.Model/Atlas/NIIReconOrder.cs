﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_NIIReconOrder")]
    public class NIIReconOrder
    {
        public int Id { get; set; }
        public int IsAsset { get; set; }
        public int Acc_Type { get; set; }
        public int RBC_Tier { get; set; }
        public int Priority { get; set; }
    }
}
