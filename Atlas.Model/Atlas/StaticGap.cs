﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_StaticGaps")]
    public class StaticGap
    {
        public int Id { get; set; }

        //Left is Top Graph
        [StringLength(100)]
        public string LeftInstitutionDatabaseName { get; set; }
        public int LeftAsOfDateOffset { get; set; }
        public int LeftSimulationTypeId { get; set; }
        public int LeftScenarioTypeId { get; set; }
        public virtual SimulationType LeftSimulationType { get; set; }
        public virtual ScenarioType LeftScenarioType { get; set; }

        //Right is Bottom Graph
        [StringLength(100)]
        public string RightInstitutionDatabaseName { get; set; }
        public int RightAsOfDateOffset { get; set; }
        public int RightSimulationTypeId { get; set; }
        public int RightScenarioTypeId { get; set; }
        public virtual SimulationType RightSimulationType { get; set; }
        public virtual ScenarioType RightScenarioType { get; set; }

        [StringLength(100)]
        public string ReportFormat { get; set; }
        [StringLength(100)]
        public string ViewOption { get; set; }

        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEqivalentYields { get; set; }
        public bool GapRatios { get; set; }

        public bool LeftOverrideSimulationId { get; set; }
        public bool LeftOverrideScenarioId { get; set; }

        public bool RightOverrideSimulationId { get; set; }
        public bool RightOverrideScenarioId { get; set; }
    }
}
