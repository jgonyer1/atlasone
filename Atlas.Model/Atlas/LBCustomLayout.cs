﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LBCustomLayout")]
    public class LBCustomLayout
    {
        public int Id {get; set;}
        [StringLength(100)]
        public string Name { get; set; }
        public bool UserAdded { get; set;}
        public int Priority { get; set;}
        public virtual ICollection<LBCustomLayoutRowItem> RowItems { get; set; }
    }
}
