﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BalanceSheetGroup")]
    public class BalanceSheetGroup
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int AccountingTypeId { get; set; }
        public virtual AccountingType AccountingType { get; set; }
    }
}
