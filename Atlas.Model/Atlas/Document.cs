﻿using Atlas.Institution.Model.Atlas;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Document")]
    public class Document
    {
        public int Id { get; set; }
        public string HtmlSource { get; set; }

        [NotMapped]
        public DocumentOrientation? Orientation { get; set;}
    }
}
