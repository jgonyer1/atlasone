﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CoverPage")]
    public class CoverPage 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public DateTime AsOfDate { get; set; }
        public int PackageType { get; set; }
        [StringLength(100)]
        public string PackageName { get; set; }
        public DateTime? MeetingDate { get; set; }
        public bool OverrideMeetingDate { get; set;}
        [StringLength(100)]
        public string Consultant { get; set; }
        [StringLength(100)]
        public string Analyst { get; set; }

    }
}
