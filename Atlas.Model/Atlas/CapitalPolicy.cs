﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CapitalPolicy")]
    public class CapitalPolicy
    {
        public int Id { get; set; }
        public int PolicyId { get; set; }
        public virtual Policy Policy { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ShortName { get; set; }
        public double PolicyLimit { get; set; }
        public double WellCap { get; set; }
        public int RiskAssessment { get; set; }
        public bool WellCapAvailable { get; set; }
        public bool RiskAssesmentAvailable { get; set; }
        public bool PolicyLimitAvailable { get; set; }
        public int Priority { get; set; }
        [StringLength(100)]
        public string CurrentRatioFormat {get; set; }
        public double CurrentRatio { get; set; }
        [StringLength(100)]
        public string FormatType { get; set; }
    }
}
