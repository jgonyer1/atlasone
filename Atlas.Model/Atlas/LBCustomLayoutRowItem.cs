﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LBCustomLayoutRowItem")]
    public class LBCustomLayoutRowItem
    {
        public int Id { get; set;}
        public int LBCustomLayoutId { get; set; }
        public LBCustomLayout LBCustomLayout { get; set;}
        public int Priority { get; set; }
        public bool IsAsset { get; set; }
        [StringLength(100)]
        public string RowType { get; set; }
        [StringLength(100)]
        public string Title { get; set; }
        public virtual ICollection<LBCustomLayoutClassification> LBCustomLayoutClassifications { get; set; }
    }
}
