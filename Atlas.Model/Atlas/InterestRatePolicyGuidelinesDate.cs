﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelinesDate")]
    public class InterestRatePolicyGuidelinesDate : InterestRatePolicyGuidelinesRow
    {

        public int InterestRatePolicyGuidelinesId { get; set; }
        public virtual InterestRatePolicyGuidelines InterestRatePolicyGuidelines { get; set; }

        public int AsOfDateOffset { get; set; }

        public int NIISimulationTypeId { get; set; }
        public virtual SimulationType NIISimulationType { get; set; }

        public int EVESimulationTypeId { get; set; }
        public virtual SimulationType EVESimulationType { get; set; }
    }
}
