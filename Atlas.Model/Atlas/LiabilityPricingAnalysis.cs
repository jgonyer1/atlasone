﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LiabilityPricingAnalysis")]
    public class LiabilityPricingAnalysis 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public int ComparativeDateOffset { get; set; }

        public bool MostRecentWebRate { get; set; }

        public int DepHisotryStartDate { get; set; }
        public int DepHistoryEndDate { get; set; }

        public bool IncludeRetailRepos { get; set; }

        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }

        public bool OverrideSimulationTypeId { get; set; }
        public bool OverrideScenarioTypeId { get; set; }

        public bool OverrideCompSimulationTypeId { get; set; }
        public bool OverrideCompScenarioTypeId { get; set; }


        public int CompSimulationTypeId { get; set; }
        public int CompScenarioTypeId { get; set; }
    }
}
