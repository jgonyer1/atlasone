﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Classification")]
    public class Classification
    {
        public bool IsAsset { get; set; }
        public int AccountTypeId { get; set; }
        public int RbcTier { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int BalanceSheetGroupId { get; set; }
        public virtual AccountType AccountType { get; set; }
    }
}
