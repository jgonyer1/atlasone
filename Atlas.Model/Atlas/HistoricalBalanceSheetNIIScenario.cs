﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_HistoricalBalanceSheetNIIScenario")]
    public class HistoricalBalanceSheetNIIScenario
    {
        public int Id { get; set; }
        public int HistoricalBalanceSheetNIIId { get; set; }
        public int ScenarioTypeId { get; set; }
        public bool CompareToThis { get; set; }
        public int Priority { get; set; }
        public virtual HistoricalBalanceSheetNII HistoricalBalanceSheetNII { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
 
    }
}
