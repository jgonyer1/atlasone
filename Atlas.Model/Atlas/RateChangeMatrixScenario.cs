﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_RateChangeMatrixScenario")]
    public class RateChangeMatrixScenario
    {
        public int Id { get; set; }
        public int RateChangeMatrixId { get; set; }
        public virtual RateChangeMatrix RateChangeMatrix { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
        public int Priority { get; set; }
            
    }
}
