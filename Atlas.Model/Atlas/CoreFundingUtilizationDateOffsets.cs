﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CoreFundingUtilizationDateOffSets")]
    public class CoreFundingUtilizationDateOffSets
    {
        public int Id { get; set; }
        public int CoreFundingUtilizationId { get; set; }
        public virtual CoreFundingUtilization CorefundingUtilization { get; set; }
        public bool OverrideSimulationTypeId { get; set; }
        public int SimulationTypeId { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public int OffSet { get; set; }
        public int Priority { get; set; }
        public bool Graph { get; set;}
 
    }
}
