﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_TimeDepositMigrations")]
    public class TimeDepositMigration 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }

        //Left Data Fields
        public int AsOfDateOffset { get; set; }

        //Right data Fields
        public int PriorAsOfDateOffset { get; set; }

        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }

        public bool OverrideSimulationTypeId { get; set; }
        public bool OverrideScenarioTypeId { get; set; }

        public bool OverridePriorSimulationTypeId { get; set; }
        public bool OverridePriorScenarioTypeId { get; set; }

        public int PriorSimulationTypeId { get; set; }
        public int PriorScenarioTypeId { get; set; }

        //Classification Settings
        public bool CD { get; set; }
        public bool JumboCD { get; set; }
        public bool PublicCD { get; set; }
        public bool PublicJumbo { get; set; }
        public bool NationalCD { get; set; }
        public bool OnlineCD { get; set; }
        public bool OnlineJumbo{ get; set; }
        public bool BrokeredCD { get; set; }
        public bool BrokeredOneWayCD { get; set; }
        public bool BrokeredReciprocalCD { get; set; }
        public bool OtherTimeDeposits { get; set; }

        //Status Settings
        public bool Regular { get; set; }
        public bool Special { get; set; }

        public int ViewBy { get; set; }
    }
}
