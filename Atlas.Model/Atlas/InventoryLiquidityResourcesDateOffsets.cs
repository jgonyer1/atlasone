﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InventoryLiquidityResourcesDateOffsets")]
    public class InventoryLiquidityResourcesDateOffsets
    {
        public int Id { get; set; }
        public int BasicSurplusAdminId { get; set; }
        public bool Override { get; set; }
        public int Offset { get; set; }
        public int Priority { get; set; }
        public int InventoryLiquidityResourcesId { get; set; }

        public virtual InventoryLiquidityResources InventoryLiquidityResource { get; set; }
    }
}
