﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Consolidation")]
    public class Consolidation
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public virtual ICollection<ConsolidationInstitution> ConsolidationInstitutions { get; set; }
        public virtual ICollection<Package> Packages { get; set; }
    }
}