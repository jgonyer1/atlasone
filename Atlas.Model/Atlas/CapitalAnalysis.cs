﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CapitalAnalysis")]
    public class CapitalAnalysis 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }

        public int CurrentCapitalDateOffset { get; set; }

        public bool IncludePolicyLimits { get; set; }
        public bool IncludeWellCapLimits { get; set; }

        public virtual ICollection<CapitalAnalysisHistoricalBufferOffsets> HistoricalBufferOffsets { get; set; }

        public bool IncludePolicyLimitsOffSets { get; set; }
        public bool IncludeWellCapLimitsOffSets { get; set; }

        [StringLength(100)]
        public string TopRightRatio { get; set; }
        [StringLength(100)]
        public string BottomRightRatio { get; set; }
        [StringLength(100)]
        public string TopLeftRatio { get; set; }
        [StringLength(100)]
        public string BottomLeftRatio { get; set; }

    }
}
