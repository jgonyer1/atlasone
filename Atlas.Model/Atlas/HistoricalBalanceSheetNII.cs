﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_HistoricalBalanceSheetNIIs")]
    public class HistoricalBalanceSheetNII 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string BalanceSheetAssetViewOption { get; set; }
        [StringLength(100)]
        public string BalanceSheetLiabilityViewOption { get; set; }
        [StringLength(100)]
        public string NIIAssetViewOption { get; set; }
        [StringLength(100)]
        public string NIILiabilityViewOption { get; set; }
        [StringLength(100)]
        public string ChangeAttribution { get; set; }
        [StringLength(100)]
        public string ComparativeScenario { get; set;}

        //tax equiv YIELDS
        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEquivalent { get; set;}

        //tax equiv INCOME
        public bool OverrideChartsTaxEquivalent { get; set; }
        public bool ChartsTaxEquivalent { get; set; }

        public bool IsQuarterly { get; set; }
        [StringLength(10)]
        public string Year2Comparative { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; } 
        public virtual ICollection<HistoricalBalanceSheetNIISimulation> HistoricalBalanceSheetNIISimulations { get; set; }
        public virtual ICollection<HistoricalBalanceSheetNIIScenario> HistoricalBalanceSheetNIIScenarios { get; set; }

        public virtual ICollection<HistoricalBalanceSheetNIIBottomFootNotes> HistoricalBalanceSheetNIITopFootNotes { get; set; }
        public virtual ICollection<HistoricalBalanceSheetNIITopFootNotes> HistoricalBalanceSheetNIIBottomFootNotes { get; set; }

        public int LeftChartId { get; set; }
        public virtual Chart LeftChart { get; set; }
        public int RightChartId { get; set; }
        public virtual Chart RightChart { get; set; }

    }
}
