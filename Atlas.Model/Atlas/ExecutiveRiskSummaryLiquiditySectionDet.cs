﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummaryLiquiditySectionDet")]
    public class ExecutiveRiskSummaryLiquiditySectionDet
    {
        public int Id { get; set; }
        public int Priority { get; set;}
        public bool OverrideRiskAssesment { get; set;}
        public int RiskAssesment { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        public int ExecutiveRiskSummaryLiquiditySectionId { get; set; }
        public virtual ExecutiveRiskSummaryLiquiditySection ExecutiveRiskSummaryLiquiditySection { get; set;}
    }
}
