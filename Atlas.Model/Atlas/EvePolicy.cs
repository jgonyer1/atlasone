﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_EvePolicy")]
    public class EvePolicy
    {
        public int Id { get; set; }
        public int EvePolicyParentId { get; set; }
        public virtual EvePolicyParent EvePolicyParent { get; set; }
        public double PolicyLimit { get; set; }
        public int RiskAssessment { get; set; }
        public int Priority { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }
        public double CurrentRatio { get; set; }

    }
}