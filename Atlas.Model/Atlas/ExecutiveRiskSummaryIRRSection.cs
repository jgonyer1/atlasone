﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummaryIRRSection")]
    public class ExecutiveRiskSummaryIRRSection
    {
        public int Id { get; set; }
        public int Priority { get; set;}

        [StringLength(100)]
        public string Name { get; set; }

        public virtual ICollection<ExecutiveRiskSummaryIRRDet> ExecutiveRiskSummaryIRRSectionDets {get; set;}

        [StringLength(100)]
        public string ReportSide { get; set; }

        public int ExecutiveRiskSummaryId { get; set; }
        public virtual ExecutiveRiskSummary ExecutiveRiskSummary { get; set;}

        public bool HideLabel { get; set; }
    }
}
