﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_InterestRatePolicyGuidelines")]
    public class InterestRatePolicyGuidelines 
    {
        public int Id { get; set; }

        //General
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }

        public virtual ICollection<InterestRatePolicyGuidelinesDate> Dates { get; set; }
        public virtual ICollection<InterestRatePolicyGuidelinesNIIComparative> NIIComparatives { get; set; }
        public virtual ICollection<InterestRatePolicyGuidelinesEVEComparative> EVEComparatives { get; set; }
    }
}
