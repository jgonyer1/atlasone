﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Packages")]
    public class Package
    {
        public int Id { get; set; }
        public DateTime AsOfDate { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ModifiedBy { get; set; }
        public DateTime DateModified { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
        public int? SourcePackageId { get; set; }
    }
}
