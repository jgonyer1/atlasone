﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_SimCompares")]
    public class SimCompare
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string LeftGraphName { get; set; }
        public bool LeftNameOverride { get; set; }
        [StringLength(100)]
        public string LeftInstitutionDatabaseName { get; set; }
        //public int LeftAsOfDateOffset { get; set; }
        //public int LeftSimulationTypeId { get; set; }
        //public virtual SimulationType LeftSimulationType { get; set; }

        [StringLength(100)]
        public string RightGraphName { get; set; }
        public bool RightNameOverride { get; set; }
        [StringLength(100)]
        public string RightInstitutionDatabaseName { get; set; }
        //public int RightAsOfDateOffset { get; set; }
        //public int RightSimulationTypeId { get; set; }
        //public virtual SimulationType RightSimulationType { get; set; }
        public bool IsQuarterly { get; set; }
        public string NiiOption { get; set; }
        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEquivalentYields { get; set; }
        public bool Details { get; set; }

        //Whether to show Percent Change, Percent Change with Policies, or show now Percent Change
        //0 = None
        //1 = Percent Change
        //2 = Percent Change With Policies
        public int PercentChange { get; set; }

        /*
         Should be either 
         * year2Year1
         * year2Year2
         * 24Month
         */
        public string PercChangeComparator { get; set; }

        [StringLength(100)]
        public string ComparativeScenario { get; set; }

        public bool OverrideLeftParentSimulationType { get; set; }
        public bool OverrideRightParentSimulationType { get; set; }
        public virtual ICollection<SimCompareSimulationType> SimCompareSimulationTypes { get; set; }
        public virtual ICollection<SimCompareScenarioType> SimCompareScenarioTypes { get; set; }
    }

}


