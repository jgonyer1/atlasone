﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model.Atlas
{
    [Table("ATLAS_FileSummary")]
    public class FileSummary
    {
        [Key]
        public Guid FileID { get; set; }
        public int OwnerType { get; set; }
        public int OwnerID { get; set; }

        [StringLength(256)]
        public string UploadName { get; set; }

        [StringLength(64)]
        public string ContentType { get; set; }
        public int Size { get; set; }
        public string Tag { get; set; }
        public DateTimeOffset DateUploaded { get; set; }
        public string UploadedBy { get; set; }
    }
}
