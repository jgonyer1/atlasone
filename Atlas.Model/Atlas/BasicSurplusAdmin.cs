﻿using Atlas.Institution.Model.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdmin")]
    public class BasicSurplusAdmin
    {
        public int Id { get; set; }
        public int InformationId { get; set; }

        //General
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }

        public bool OverrideSimulationTypeId { get; set; }
        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }

        [StringLength(100)]
        public string Name { get; set; }
        public int Priority { get; set; }

        public virtual ICollection<BasicSurplusMarketValue> BasicSurplusMarketValues { get; set; }
        public virtual ICollection<CollateralType> CollateralTypes { get; set; }
        public virtual ICollection<BasicSurplusSecuredLiabilitiesType> BasicSurplusSecuredLiabilitiesTypes { get; set; }
        

        //Left
        //Section 1
        public bool LeftS1HeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS1HeaderTitle { get; set; }

        public virtual ICollection<BasicSurplusAdminOvernightFund> LeftS1OvntFunds { get; set; }

        public bool LeftS1SecColOverride { get; set; }
        [StringLength(100)]
        public string LeftS1SecColTitle { get; set; }

        public bool LeftS1SecColTotalSecMktValOverride { get; set; }
        [StringLength(100)]
        public string LeftS1SecColTotalSecMktValTitle { get; set; }
        public decimal LeftS1SecColTotalSecMktValUSTAgency { get; set; }
        public decimal LeftS1SecColTotalSecMktValMBSAgency { get; set; }
        public decimal LeftS1SecColTotalSecMktValMBSPvt { get; set; }
        public bool LeftS1SecColLessSecPledgedOverride { get; set; }
        [StringLength(100)]
        public string LeftS1SecColLessSecPledgedTitle { get; set; }
        public virtual ICollection<BasicSurplusAdminSecurityCollateral> LeftS1SecCols { get; set; }
        public bool LeftS1SecColTotalSecColOverride { get; set; }
        [StringLength(100)]
        public string LeftS1SecColTotalSecColTitle { get; set; }
        public decimal LeftS1SecColTotalSecColUSTAgency { get; set; }
        public decimal LeftS1SecColTotalSecColMBSAgency { get; set; }
        public decimal LeftS1SecColTotalSecColMBSPvt { get; set; }

        public bool LeftS1BlankTotalOverride { get; set; }
        [StringLength(100)]
        public string LeftS1BlankTotalTitle { get; set; }
        public decimal LeftS1BlankTotalAmount { get; set; }
        public virtual ICollection<BasicSurplusAdminLiquidAsset> LeftS1LiqAssets { get; set; }
        public bool LeftS1LiqAssetTotalOverride { get; set; }
        [StringLength(100)]
        public string LeftS1LiqAssetTotalTitle { get; set; }
        public decimal LeftS1LiqAssetTotalAmount { get; set; }
        public decimal LeftS1LiqAssetPercent { get; set; }

        //Section 2
        public bool LeftS2HeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS2HeaderTitle { get; set; }

        public bool LeftS2MatUnsecLiabOverride { get; set; }
        [StringLength(100)]
        public string LeftS2MatUnsecLiabTitle { get; set; }
        public decimal LeftS2MatUnsecLiabAmount { get; set; }
        public bool LeftS2DepCovOverride { get; set; }
        [StringLength(100)]
        public string LeftS2DepCovTitle { get; set; }

        [StringLength(100)]
        public string LeftS2DepCovOption { get; set; }

        public bool LeftS2TotalDepOverride { get; set; }
        public decimal LeftS2TotalDepPct { get; set; }
        [StringLength(100)]
        public string LeftS2TotalDepTitle { get; set; }
        public decimal LeftS2TotalDepAmount { get; set; }
        public bool LeftS2TotalDepExclBrokeredOneWay { get; set; }
        public bool LeftS2TotalDepExclBrokeredRecip { get; set; }
        public bool LeftS2TotalDepExclPublicDeposits { get; set; }
        public bool LeftS2TotalDepExclNationalCDs { get; set; }

        public bool LeftS2RegCDMatOverride { get; set; }
        public decimal LeftS2RegCDMatPct { get; set; }
        [StringLength(100)]
        public string LeftS2RegCDMatTitle { get; set; }
        public decimal LeftS2RegCDMatAmount { get; set; }
        public bool LeftS2JumboCDMatOverride { get; set; }
        public decimal LeftS2JumboCDMatPct { get; set; }
        [StringLength(100)]
        public string LeftS2JumboCDMatTitle { get; set; }
        public decimal LeftS2JumboCDMatAmount { get; set; }
        public bool LeftS2OtherDepOverride { get; set; }
        public decimal LeftS2OtherDepPct { get; set; }
        [StringLength(100)]
        public string LeftS2OtherDepTitle { get; set; }
        public decimal LeftS2OtherDepAmount { get; set; }
        public virtual ICollection<BasicSurplusAdminVolatileLiability> LeftS2VolLiabs { get; set; }
        public bool LeftS2AddLiqResOverride { get; set; }
        [StringLength(100)]
        public string LeftS2AddLiqResTitle { get; set; }
        public decimal LeftS2AddLiqResAmount { get; set; }
        public bool LeftS2VolLiabInclPublicCD { get; set; }
        public bool LeftS2VolLiabInclPublicJumboCD { get; set; }
        public bool LeftS2VolLiabInclPublicDDANOWMMDA { get; set; }
        public bool LeftS2VolLiabTotalOverride { get; set; }
        [StringLength(100)]
        public string LeftS2VolLiabTotalTitle { get; set; }
        public decimal LeftS2VolLiabTotalAmount { get; set; }

        public bool LeftS2SubHeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS2SubHeaderTitle { get; set; }

        //Section 3
        public bool LeftS3HeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS3HeaderTitle { get; set; }

        public bool LeftS3FamResReColOverride { get; set; }
        public decimal LeftS3FamResReColAmountPledged { get; set; }

        [DecimalPrecision(18,7)]
        public decimal LeftS3FamResReColCollateralValue { get; set; }
        public decimal LeftS3FamResReColNetCollateral { get; set; }
        public decimal LeftS3FamResReColL360Collateral { get; set; }
        public bool LeftS3HELOCOtherColOverride { get; set; }
        public decimal LeftS3HELOCOtherColAmountPledged { get; set; }
        public decimal LeftS3HELOCOtherColCollateralValue { get; set; }
        public decimal LeftS3HELOCOtherColNetCollateral { get; set; }
        public decimal LeftS3HELOCOtherColL360Collateral { get; set; }
        public bool LeftS3CREColOverride { get; set; }
        public decimal LeftS3CREColAmountPledged { get; set; }
        public decimal LeftS3CREColCollateralValue { get; set; }
        public decimal LeftS3CREColNetCollateral { get; set; }
        public decimal LeftS3CREColL360Collateral { get; set; }
        public virtual ICollection<BasicSurplusAdminLoanCollateral> LeftS3LoanCols { get; set; }

        public bool LeftS3MaxBorLineFHLBOverride { get; set; }
        [StringLength(100)]
        public string LeftS3MaxBorLineFHLBType { get; set; }
        public decimal LeftS3MaxBorLineFHLBPct { get; set; }
        [StringLength(100)]
        public string LeftS3MaxBorLineFHLBTitle { get; set; }
        public decimal LeftS3MaxBorLineFHLBAmount { get; set; }
        public bool LeftS3LoanColFHLBOverride { get; set; }
        [StringLength(100)]
        public string LeftS3LoanColFHLBTitle { get; set; }
        public decimal LeftS3LoanColFHLBAmount { get; set; }
        public decimal LeftS3LoanColFHLBL360 { get; set; }
        public bool LeftS3ExcessLoanColOverride { get; set; }
        [StringLength(100)]
        public string LeftS3ExcessLoanColTitle { get; set; }
        public decimal LeftS3ExcessLoanColAmount { get; set; }

        public bool LeftS3MaxBorCapOverride { get; set; }
        [StringLength(100)]
        public string LeftS3MaxBorCapTitle { get; set; }
        public decimal LeftS3MaxBorCapAmount { get; set; }
        public bool LeftS3ColEncOverride { get; set; }
        [StringLength(100)]
        public string LeftS3ColEncTitle { get; set; }
        public decimal LeftS3ColEncAmount { get; set; }
        public virtual ICollection<BasicSurplusAdminLoan> LeftS3Loans { get; set; }
        public bool LeftS3LoanTotalOverride { get; set; }
        [StringLength(100)]
        public string LeftS3LoanTotalTitle { get; set; }
        public decimal LeftS3LoanTotalAmount { get; set; }

        public bool LeftS3SubHeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS3SubHeaderTitle { get; set; }

        //Section 4
        public bool LeftS4HeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS4HeaderTitle { get; set; }

        public bool LeftS4BrokDepCapOverride { get; set; }
        [StringLength(100)]
        public string LeftS4BrokDepType { get; set; }
        public bool LeftS4BrokDepPolicyLimit { get; set; }
        public decimal LeftS4BrokDepCapPct { get; set; }
        [StringLength(100)]
        public string LeftS4BrokDepCapTitle { get; set; }
        public decimal LeftS4BrokDepCapAmount { get; set; }
        public bool LeftS4BrokDepBalOverride { get; set; }
        [StringLength(100)]
        public string LeftS4BrokDepBalTitle { get; set; }
        public decimal LeftS4BrokDepBalAmount { get; set; }
        public bool LeftS4BrokDepBalExclBrokeredOneWay { get; set; }
        public bool LeftS4BrokDepBalExclBrokeredRecip { get; set; }
        public bool LeftS4BrokDepBalInclNationalCDs { get; set; }
        public virtual ICollection<BasicSurplusAdminBrokeredDeposit> LeftS4BrokDeps { get; set; }
        public bool LeftS4BrokDepTotalOverride { get; set; }
        [StringLength(100)]
        public string LeftS4BrokDepTotalTitle { get; set; }
        public decimal LeftS4BrokDepTotalAmount { get; set; }

        public bool LeftS4SubHeaderOverride { get; set; }
        [StringLength(100)]
        public string LeftS4SubHeaderTitle { get; set; }

        //Middle
        public virtual ICollection<BasicSurplusAdminMisc> MidS1Miscs { get; set; }
        public bool MidS1TotalDepositOverride { get; set; }
        public decimal MidS1TotalDepositAmount { get; set; }

        //public bool MidS1BasicSurplusOverride { get; set; } //No Override, always automatic
        public decimal MidS1BasicSurplusAmount { get; set; }
        public decimal MidS1BasicSurplusPercent { get; set; }
        //public bool MidS1BasicSurplusFHLBOverride { get; set; } //No Override, always automatic
        public decimal MidS1BasicSurplusFHLBAmount { get; set; }
        public decimal MidS1BasicSurplusFHLBPercent { get; set; }
        //public bool MidS1BasicSurplusBrokOverride { get; set; } //No Override, always automatic
        public decimal MidS1BasicSurplusBrokAmount { get; set; }
        public decimal MidS1BasicSurplusBrokPercent { get; set; }

        //Right
        //Section 1
        public bool RightS1HeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS1HeaderTitle { get; set; }

        public bool RightS1SubHeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS1SubHeaderTitle { get; set; }

        public virtual ICollection<BasicSurplusAdminOtherLiquidity> RightS1OtherLiqs { get; set; }
        public bool RightS1OtherLiqTotalOverride { get; set; }
        [StringLength(100)]
        public string RightS1OtherLiqTotalTitle { get; set; }
        public decimal RightS1OtherLiqTotalMarketValue { get; set; }
        public decimal RightS1OtherLiqTotalPledged { get; set; }
        public decimal RightS1OtherLiqTotalAvailable { get; set; }

        //Section 2
        public bool RightS2HeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS2HeaderTitle { get; set; }

        public bool RightS2SubHeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS2SubHeaderTitle { get; set; }

        public virtual ICollection<BasicSurplusAdminSecuredBorrowing> RightS2SecBors { get; set; }
        public bool RightS2SecBorTotalOverride { get; set; }
        [StringLength(100)]
        public string RightS2SecBorTotalTitle { get; set; }
        public decimal RightS2SecBorTotalLine { get; set; }
        public decimal RightS2SecBorTotalOutstanding { get; set; }
        public decimal RightS2SecBorTotalAvailable { get; set; }

        //Section 3
        public bool RightS3HeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS3HeaderTitle { get; set; }

        public virtual ICollection<BasicSurplusAdminUnsecuredBorrowing> RightS3UnsecBors { get; set; }
        public bool RightS3UnsecBorTotalOverride { get; set; }
        [StringLength(100)]
        public string RightS3UnsecBorTotalTitle { get; set; }
        public decimal RightS3UnsecBorTotalLine { get; set; }
        public decimal RightS3UnsecBorTotalOutstanding { get; set; }
        public decimal RightS3UnsecBorTotalAvailable { get; set; }

        //Section 4
        public bool RightS4HeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS4HeaderTitle { get; set; }

        public bool RightS4Column1Override { get; set; }
        [StringLength(100)]
        public string RightS4Column1Title { get; set; }

        public bool RightS4Column2Override { get; set; }

        //no override
        [StringLength(100)]
        public string RightS4Column2Title { get; set; }

        [StringLength(100)]
        public string RightS4Column3Title { get; set; }

        //public virtual ICollection<BasicSurplusAdminOtherSecured> RightS4OtherSecs { get; set; }
        public virtual ICollection<BasicSurplusAdminUnrealizedGL> RightS4UnrealGL { get; set; }

        //public bool RightS4FootnoteOverride { get; set; }
        //[StringLength(500)]
        //public string RightS4FootnoteText { get; set; }

        //Section 5
        public bool RightS5HeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS5HeaderTitle { get; set; }

        public virtual ICollection<BasicSurplusAdminBorrowingCapacity> RightS5BorCaps { get; set; }
        public bool RightS5BorCapsPolicyLimit { get; set; }
        //public bool RightS5AuthBorCapOverride { get; set; } //No Override, always automatic
        [StringLength(100)]
        public string RightS5AuthBorCapTitle { get; set; }
        public decimal RightS5AuthBorCapAmount { get; set; }

        //Section 6
        public bool RightS6HeaderOverride { get; set; }
        [StringLength(100)]
        public string RightS6HeaderTitle { get; set; }

        public bool RightS6PubDepReqPledgingOverride { get; set; }
        [StringLength(100)]
        public string RightS6PubDepReqPledgingTitle { get; set; }
        public decimal RightS6PubDepReqPledgingCurrentQuarter { get; set; }
        public decimal RightS6PubDepReqPledgingPreviousQuarter { get; set; }
        public virtual ICollection<BasicSurplusAdminFootnote> RightS6Footnotes { get; set; }


        public bool OverrideMarketValues { get; set; }
    }
}
