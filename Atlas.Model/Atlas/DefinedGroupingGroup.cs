﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_DefinedGroupingGroups")]
    public class DefinedGroupingGroup
    {
        public int Id { get; set; }
        public int IsAsset { get; set; }
        public int Priority { get; set; }
        public int DefinedGroupingId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public virtual ICollection<DefinedGroupingClassification> DefinedGroupingClassification { get; set; }
        public virtual DefinedGrouping DefinedGrouping { get; set; }
    }
}
