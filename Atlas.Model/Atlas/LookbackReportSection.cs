﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LookbackReportSection")]
    public class LookbackReportSection
    {
        public int Id { get; set;}
        [StringLength(100)]
        public string Name { get; set;}
        public int Priority { get; set;}
        public virtual ICollection<LookbackType> LookbackTypes { get; set;}
        public int LBLookbackLayoutId { get; set; }
        public virtual LookbackReport LookbackReport { get; set;}
        public int LookbackReportId { get; set;}
    }
}
