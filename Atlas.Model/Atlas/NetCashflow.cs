﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_NetCashflow")]
    public class NetCashflow
    {
        public int Id { get; set; }

        public int AsOfDateOffset { get; set; }

        public virtual ICollection<NetCashflowSimulationScenario> NetCashflowSimulationScenario { get; set; }

        public int DefinedGrouping { get; set; }
        public bool ExcludePrepayments { get; set; }
    }
}
