﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ASC825DiscountRate")]
    public class ASC825DiscountRate 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }


    }
}
