﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_OtherPolicy")]
    public class OtherPolicy
    {
        public int Id { get; set; }
        public int PolicyId { get; set; }
        public virtual Policy Policy { get; set; }
        public double PolicyLimit { get; set; }
        public int RiskAssessment { get; set; }
        public int Priority { get; set; }
        public double CurrentRatio { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string ShortName { get; set; }
        [StringLength(100)]
        public string FormatType { get; set; }

        [StringLength(100)]
        public string CurrentRatioFormat { get; set; }
    }
}
