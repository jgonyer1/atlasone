﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LiquidityProjection")]
    public class LiquidityProjection 
    {
        public int Id { get; set; }

        //Left is Top Graph
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }

        public bool IncludePublicDep { get; set; }
        public bool IncludeNationalDep { get; set; }
        public bool IncludeBrokeredDep { get; set; }
        public bool IncludeBrokeredRecip { get; set; }
        public bool IncludeBrokeredOneWay { get; set; }
        public bool IncludeSecuredRetail { get; set; }

        public virtual ICollection<LiquidityProjectionDateOffset> LiquidityProjectionDateOffsets { get; set; }
        [StringLength(100)]
        public string ProjectionPeriod { get; set; }
        [StringLength(100)]
        public string ViewOption { get; set; }
        [StringLength(100)]
        public string NonPledgeGroupOption { get; set; }
    }
}
