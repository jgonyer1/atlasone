﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummaryIRRDet")]
    public class ExecutiveRiskSummaryIRRDet
    {
        public int Id { get; set; }
        public int Priority { get; set;}

        [StringLength(100)]
        public string Name { get; set;}
        [StringLength(100)]

        public virtual ICollection<ExecutiveRiskSummaryIRRScenario> ExecutiveSummaryIRRScenarios { get; set; }

        public int ExecutiveRiskSummaryIRRSectionId { get; set; }
        public virtual ExecutiveRiskSummaryIRRSection ExecutiveRiskSummaryIRRSection { get; set; }

        public bool HideLabel { get; set; }
    }
}
