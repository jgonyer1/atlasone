﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BalanceSheetCompares")]
    public class BalanceSheetCompare 
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string LeftInstitutionDatabaseName { get; set; }
        public int LeftAsOfDateOffset { get; set; }
        public int LeftSimulationTypeId { get; set; }
        public int LeftScenarioTypeId { get; set; }
        public virtual SimulationType LeftSimulationType { get; set; }
        public virtual ScenarioType LeftScenarioType { get; set; }
        public DateTime LeftStartDate { get; set; }
        public DateTime LeftEndDate { get; set; }

        [StringLength(100)]
        public string RightInstitutionDatabaseName { get; set; }
        public int RightAsOfDateOffset { get; set; }
        public int RightSimulationTypeId { get; set; }
        public int RightScenarioTypeId { get; set; }
        public virtual SimulationType RightSimulationType { get; set; }
        public virtual ScenarioType RightScenarioType { get; set; }
        public DateTime RightStartDate { get; set; }
        public DateTime RightEndDate { get; set; }

        [StringLength(100)]
        public string ViewOption { get; set; }
        [StringLength(100)]
        public string BalanceType { get; set; }

        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEqivalentYields { get; set; }
        public bool Balances { get; set; }
        public bool Rates { get; set; }

        public bool LeftOverrideSimulationId { get; set; }
        public bool LeftOverrideScenarioId { get; set; }

        public bool RightOverrideSimulationId { get; set; }
        public bool RightOverrideScenarioId { get; set; }
    }
}
