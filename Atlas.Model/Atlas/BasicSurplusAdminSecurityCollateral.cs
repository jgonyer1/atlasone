﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminSecurityCollateral")]
    public class BasicSurplusAdminSecurityCollateral : BasicSurplusAdminRow
    {
        public decimal USTAgency { get; set; }
        public decimal MBSAgency { get; set; }
        public decimal MBSPvt { get; set; }
    }
}
