﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminLoan")]
    public class BasicSurplusAdminLoan : BasicSurplusAdminRow
    {
        public decimal Amount { get; set; }
    }
}
