﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Footnote")]
    public class Footnote
    {
        public int Id { get; set; }
        public int ReportId { get; set; }
        public int Priority { get; set; }
        [StringLength(500)]
        public string Text { get; set; }

        public Report Report { get; set; }
    }
}
