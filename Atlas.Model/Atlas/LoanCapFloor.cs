﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_LoanCapFloors")]
    public class LoanCapFloor 
    {
        public int Id { get; set; }

        public int Type { get; set; }
        public int Mode { get; set; }
        public int BPBreakout { get; set; }
        public int TopAsOfDateOffset { get; set; }
        public int BottomAsOfDateOffset { get; set; }
        [StringLength(100)]
        public string TopInstitutionDatabaseName { get; set; }
        [StringLength(100)]
        public string BottomInstitutionDatabaseName { get; set; }
        [StringLength(300)]
        public string BPBreakoutTitles { get; set; }
        /*
        [NotMapped]
        public int[] BpBreakoutEndpoints {
            get {
                string[] eps = this.BpEndpoints.Split(',');
                return new int[] { int.Parse(eps[0]), int.Parse(eps[1]), int.Parse(eps[2]), int.Parse(eps[3]) };
            }
            set {
                this.BpEndpoints = string.Format("{0},{1},{2},{3}", value[0], value[1], value[2], value[3]);
            }
        }
        */
        [StringLength(300)]
        public string BpEndpoints { get; set; }
        public virtual SimulationType SimulationType { get; set; }
    }
}


