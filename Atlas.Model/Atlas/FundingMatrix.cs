﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_FundingMatrices")]
    public class FundingMatrix 
    {
        public int Id { get; set; }
        public virtual ICollection<FundingMatrixOffset> FundingMatrixOffsets { get; set; }

        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEqivalentYields { get; set; }


    }
}
