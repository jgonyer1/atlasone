﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{

    [Table("ATLAS_YieldCurveShiftScenarioType")]
    public class YieldCurveShiftScenarioType
    {
        public int Id { get; set; }
        public int YieldCurveShiftId { get; set; }
        public virtual YieldCurveShift YieldCurveShift { get; set; }
        public int WebRateScenarioTypeId { get; set; }
        public virtual WebRateScenarioType WebRateScenarioType { get; set; }
        public int Priority { get; set; }
 
    }
}
