﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_HistoricalTimeDepositlRate")]
    public class HistoricalTimeDepositRate
    {
        public int Id { get; set; }
        public int LiabilityPricingId { get; set; }
        public virtual LiabilityPricing LiabilityPricing { get; set; }
        [StringLength(100)]
        public string Term { get; set; }
        public double Rate { get; set; }
       
    }
}
