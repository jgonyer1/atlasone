﻿using Atlas.Institution.Model.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusCollateralType")]
    public class BasicSurplusCollateralType
    {
        public int Id { get; set; }
        public int BasicSurplusSecuredLiabilitiesTypeId { get; set; }
        public virtual BasicSurplusSecuredLiabilitiesType BasicSurplusSecuredLiabilitiesType { get; set; }

        public int CollateralTypeId { get; set; }
        public virtual CollateralType CollateralType { get; set; }

        public int Priority { get; set; }
        public decimal PledgingRequired { get; set; }
        public decimal AmountPledgedMktVal { get; set; }

        [DecimalPrecision(18, 7)]
        public decimal CollateralValuePercentage { get; set; }
        public decimal NetCollateralValue { get; set; }
        public decimal MaxCollateralUsed { get; set; }
        public decimal RemainingPledgingRequired { get; set; }
        public decimal ExcessCollateralMktVal { get; set; }
        public decimal OverCollateralizedBasicSurplus { get; set; }
    }
}
