﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Atlas.Institution.Model
{
    [Table("Atlas_TimeDepositMigrationDefaultView")]
    public class TimeDepositMigrationDefaultView
    {
        public int Id { get; set; }
        public int Class { get; set; }
        public int IlpKey4 { get; set; }
        public string Name { get; set; }

        public int Priority { get; set; }
    }
}
