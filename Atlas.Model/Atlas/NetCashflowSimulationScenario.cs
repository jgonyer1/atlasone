﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_NetCashflowSimulationScenario")]
    public class NetCashflowSimulationScenario 
    {
        public int Id { get; set; }
        public int AsOfDateOffset { get; set; }
        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }
        public int NetCashflowId { get; set; }
        public int Priority { get; set; }
        public virtual NetCashflow NetCashflow{ get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }

    }
}
