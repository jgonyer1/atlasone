﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_CashflowSimulationScenario")]
    public class CashflowSimulationScenario 
    {
        public int Id { get; set; }
        
        public string InstitutionDatabaseName { get; set; }
        public int AsOfDateOffset { get; set; }
        public int SimulationTypeId { get; set; }
        public int ScenarioTypeId { get; set; }
        public int CashflowReportId { get; set; }
        public int Priority { get; set; }
        public virtual CashflowReport CashflowReport { get; set; }
        public virtual SimulationType SimulationType { get; set; }
        public virtual ScenarioType ScenarioType { get; set; }

        public bool OverrideSimulationId { get; set; }

    }
}
