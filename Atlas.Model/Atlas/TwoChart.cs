﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_TwoCharts")]
    public class TwoChart 
    {
        public int Id { get; set; }
        public int LeftChartId { get; set; }
        public int RightChartId { get; set; }
        public bool OverrideTaxEquiv { get; set; }
        public bool TaxEquivalentIncome { get; set; }
        public virtual Chart LeftChart { get; set; }
        public virtual Chart RightChart { get; set; }
        public string NiiOption { get; set; }
        public bool IsQuarterly { get; set; }
    }
}
