﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_BasicSurplusAdminBorrowingCapacity")]
    public class BasicSurplusAdminBorrowingCapacity : BasicSurplusAdminRow
    {
        [StringLength(100)]
        public string Type { get; set; }
        public decimal Percent { get; set; }
        public decimal Amount { get; set; }
    }
}
