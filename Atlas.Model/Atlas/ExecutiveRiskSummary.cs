﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_ExecutiveRiskSummary")]
    public class ExecutiveRiskSummary
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string InstitutionDatabaseName { get; set; }

        public bool ExcludePolicies{ get; set; }

        public bool ExcludeRiskAssessments{ get; set; }

       /* public bool TaxEquivYields { get; set; }

        public bool TaxEquivIncome { get; set; }

        public bool OverrideTaxEquivYield { get; set; }
        public bool OverrideTaxEquivIncome { get; set; }*/


        public bool LiquidityRiskAssesmentOverride { get; set; }

        public bool InterestRateRiskAssesmentOverride { get; set; }

        public bool CapitalRiskAssesmentOverride { get; set; }


        public int LiquidityRiskAssesment { get; set; }

        public int InterestRateRiskAssesment { get; set; }

        public int CapitalRiskAssesment { get; set; }

        public int LiqOrder { get; set; }
        public int CapOrder { get; set; }
        public int IRROrder { get; set; }


        public bool HideLiq { get; set; }
        public bool HideCap { get; set; }
        public bool HideIRR { get; set; }

        /*
         *  Right and Left Liquidity
         */
        public virtual ICollection<ExecutiveRiskSummaryLiquiditySection> ExecutiveRiskSummaryLiquiditySections { get; set; }

        public virtual ICollection<ExecutiveRiskSummaryIRRSection> ExecutiveSummaryIRRSections { get; set; }

        public virtual ICollection<ExecutiveRiskSummaryCapitalSection> ExecutiveRiskSummaryCapitalSection { get; set; }

        public virtual ICollection<ExecutiveRiskSummaryDateOffsets> ExecutiveRiskSummaryDateOffsets { get; set; }

    }
}
