﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Atlas.Institution.Model
{
    [Table("ATLAS_Policy")]
    public class Policy
    {
        public int Id { get; set; }
        public DateTime AsOfDate { get; set; }
        public DateTime MeetingDate { get; set; }
        public int NIISimulationTypeId { get; set; }
        public virtual SimulationType NIISimulationType { get; set; }

        public int EveSimulationTypeId { get; set; }
        public virtual SimulationType EveSimulationType { get; set; }

        public int BasicSurplusAdminId { get; set; }

        public virtual ICollection<LiquidityPolicy> LiquidityPolicies { get; set; }
        public virtual ICollection<NIIPolicyParent> NIIPolicyParents { get; set; }
        public virtual ICollection<CoreFundParent> CoreFundParents { get; set; }
        public virtual ICollection<EvePolicyParent> EvePolicyParents { get; set; }
        public virtual ICollection<CapitalPolicy> CapitalPolicies { get; set; }
        public virtual ICollection<OtherPolicy> OtherPolicies { get; set; }

        public int ReportedEveScenarioId { get; set; }
        public bool UseDefault { get; set; }

        public int LiquidityRiskAssessment { get; set; }
        public int NIIRiskAssessment { get; set; }
        public int CapitalRiskAssessment { get; set; }

    }
}
