/*
	Simple Helper Library for Things without ids, etc, etc, etc
*/


	async function clearInput(elementHandle){
		await elementHandle.click();
		await elementHandle.focus();
		// click three times to select all
		await elementHandle.click({clickCount: 3});
		await elementHandle.press('Backspace');
	}

	function escapeXpathString(str) {
		const splitedQuotes = str.replace(/'/g, `', "'", '`);
		return "'" + splitedQuotes + "'";
	}
	
	async function getObjectsByXPath(page, text, elemType){
		const escapedText = escapeXpathString(text);
		var objs = await getByXPath(`//${elemType}[contains(text(), ${escapedText})]`, page);
		
		return objs;
	}
	
	async function getByXPath(lookup, page){
		var objects = await page.$x(lookup);
		return objects;
	}
	
	async function click(page, el, options) {
		var previousBorder = await page.evaluate(e => { var ret = e.style.border; e.style.border = 'solid 5px red'; return ret; }, el);
		await el.click(el, options || {});
		await page.evaluate((e, pb) => { e.style.border = pb; }, el, previousBorder);
	}
	
	async function isButtonEnabled(page, buttonOrButtonText) {
		var objs;
		
		switch(buttonOrButtonText.constructor.name) {
			case 'String':
				const escapedText = escapeXpathString(buttonOrButtonText);
				var lookup = `//button[contains(., ${escapedText})]`;
				objs = await getByXPath(lookup, page);
				break;
				
			case 'ElementHandle':
				objs = [buttonOrButtonText];
				break;
				
			default:
				throw 'Unrecognized argument type ' + buttonOrButtonText.constructor.name;
		}
				
		if(objs.length != 1) {
			throw `Only a single button should be returned for isButtonEnabled but ${objs.length} were returned`;
		}
		
		return await page.evaluate(el => el.getAttribute('disabled') === null, objs[0]);
	}
	
	async function clickByText(page, text, elemType) {
		const escapedText = escapeXpathString(text);
		var lookup = `//${elemType}[contains(., ${escapedText})]`;
		var objs = await getByXPath(lookup, page);

		//if we foudn one and should only be one click first one in array
		if (objs.length > 0) {
			await objs[0].click();
		} else {
			throw new Error(`Object not found: ${text}`);
		}
	}

	async function hoverByText(page, text, elemType) {
		const escapedText = escapeXpathString(text);
		var objs = await getByXPath(`//${elemType}[contains(text(), ${escapedText})]`, page);

		if (objs.length > 0) {
			await objs[0].hover();

		} else {
			throw new Error(`Object not found: ${text}`);
		}
	}
	
	async function clientGoto(page, hashUrl) {
		if(hashUrl.indexOf('#') != 0)
			throw 'Client url must start with a #';
		
		var pageUrl = page.url();
		var hashIndex = pageUrl.indexOf('#');
		
		if(hashIndex > -1 && pageUrl.substr(hashIndex) == hashUrl) return;
		
		await page.evaluate((hurl) => {					
			var anchor = document.createElement('a');
			anchor.setAttribute('href', hurl);
			document.body.appendChild(anchor);
			anchor.click();
		}, hashUrl);
	}
	
	async function delay(timeoutMS) {
		return new Promise(resolve => { setTimeout(resolve, timeoutMS); });
	}
	
	async function poll(fn, timeout) {
		var isComplete = false;
		var pulseRate = 500; // ms
		var result;
		
		return new Promise((resolve, reject) => {
			async function getResult() {
				var result = fn();
				
				if(! result) return false;
				
				if(result.constructor.name == 'Promise')
					return await result;
				
				return result;
			}
			
			function continuation() {
				if(isComplete) return;
							
				if(timeout <= 0) {
					isComplete = true;
					return;
				}
				
				getResult()
					.then(r => {
						if(! r) return;
						
						isComplete = true;
						resolve(r);
					})
					.catch(e => {
						reject(e);
						isComplete = true;
						return;						
					});
				
				setTimeout(continuation, pulseRate);
				timeout -= pulseRate;				
			}
			
			continuation();
		});
	}
	
	// from: { x, y } or selector or elementHandle
	// to: {x, y} or selector or elementHandle
	// options
		// position: above, below, on (default)
		// delay: (optional) milliseconds
	async function drag(page, from, to, options) {
		options = options || {};
		
		var fromXY;
		var toXY;
		var el, bb;
		
		switch(from && from.constructor.name) {
			case 'String':
				el = await page.$(from);
				bb = await el.boundingBox();
				fromXY = { x: bb.width / 2 + bb.x, y: bb.height / 2 + bb.y };
				break;
				
			case 'Object':
				fromXY = { x: from.x || 0, y: from.y || 0 };
				break;
				
			case 'ElementHandle':
				bb = await from.boundingBox();
				fromXY = { x: bb.width / 2 + bb.x, y: bb.height / 2 + bb.y };
				break;
				
			default:
				throw 'Unsupported type passed in @from parameter';
		}
		
		function toFromBB(bb) {
			switch(options.position) {
				case 'above':
					return { x: bb.width / 2 + bb.x, y: bb.y - 1 };
				
				case 'below':
					return { x: bb.width / 2 + bb.x, y: bb.height + bb.y + 1 };
					
				default:
					return { x: bb.width / 2 + bb.x, y: bb.height / 2 + bb.y };
			}
		}
		
		switch(to && to.constructor.name) {
			case 'String':
				el = await to.$(from);
				bb = await el.boundingBox();
				toXY = toFromBB(bb);
				break;
				
			case 'Object':
				toXY = { x: to.x || 0, y: to.y || 0 };
				break;
				
			case 'ElementHandle':
				bb = await to.boundingBox();
				toXY = toFromBB(bb);
				break;
				
			default:
				throw 'Unsupported type passed in @to parameter';
		}
		
		await page.mouse.move(fromXY.x, fromXY.y);
		await page.mouse.down();
		await page.mouse.move(toXY.x, toXY.y, { steps: 20 });
		await page.mouse.up();		
	}

	async function clickAddWaitForModal(page) {
		var addButton = await page.waitForSelector('button[title~=add]');
				
		click(page, addButton);
	
		await page.waitForSelector('.modalBlockout', { visible: true });
	}

	function delay(timeout) {
		return new Promise((resolve) => {
		  setTimeout(resolve, timeout);
		});
	  }

	async function getPrioritiesArray(page) {
		var els = await page.$$('tbody.sortable > tr > td:nth-child(3)');
		
		var results = [];
		
		for(var i=0; i<els.length; i++) {
			results.push(await page.evaluate(el => parseInt(el.innerText, 10), els[i]));
		}
		
		return results;
	}
	  


module.exports = {
	hoverByText: hoverByText,
	clickByText: clickByText,
	getObjectsByXPath: getObjectsByXPath,
	isButtonEnabled: isButtonEnabled,
	click: click,
	drag: drag,
	clientGoto: clientGoto,
	delay: delay,
	poll: poll,
	clickAddWaitForModal: clickAddWaitForModal,
	getPrioritiesArray: getPrioritiesArray,
	clearInput: clearInput
};
