const chalk = require('chalk')
const puppeteer = require('puppeteer')
const h = require('./harness');
const fs = require('fs')
const mkdirp = require('mkdirp')
const os = require('os')
const path = require('path')

const DIR = path.join(os.tmpdir(), 'jest_puppeteer_global_setup')

module.exports = async function() {
	const harness = await h.Harness({
		baseUrl: "http://10.0.0.25/",
		//username: "jkeith",
		//password: "JuKe2018July30??",
		//institutionName: "Mechanics Amber Justin",
		username: "knajem",
		password: "ColtonJolene519",
		institutionName: "DDW_Atlas_DEV_E2E_Test", 
		
		asOfDate: "06/30/2017",
		headed: true		
	});

	// This global is not available inside tests but only in global teardownY
	global.__HARNESS__ = harness;
	
	// Instead, we expose the connection details via file system to be used in tests
	mkdirp.sync(DIR)
  
	fs.writeFileSync(path.join(DIR, 'config'), JSON.stringify(harness.config));
	fs.writeFileSync(path.join(DIR, 'wsEndpoint'), harness.browser.wsEndpoint());
}
