const SELECTOR_TIMEOUT_MS = 10000;

const puppeteer = require('puppeteer');

var headed = process.argv.some(v => v == '--headed');

module.exports = {
	HarnessConnect: async function(config, wsEndpoint) {
		var browser = await puppeteer.connect({browserWSEndpoint: wsEndpoint});
			
		var page = (await browser.pages()).find(p => p.url().startsWith(config.baseUrl));
		
		if(! page) {
			page = await browser.newPage();
			await page.goto(config.baseUrl);
		}
		
		return {
			config: config,
			browser: browser,
			page: page
		};
	},
	
	//  config settings are
	//  {
	// 		baseUrl: (string), required - URL without # to connect to
	//		username: (string), required - username to log through okta with
	//		password: (string), required - password to log through okta with
	//		institutionName: (string), optional - institution to operate within
	//											- if the institution isn't found, an error is thrown
	//		asOfDate: (string), optional		- asOfDate to operate within
	//											- if the asOfDate isn't found, an error is thrown
	//		headed: (bool), optional			- when true, the browser is visible
	//  }
	
	Harness: async function(config) {
		var result = new Promise(async function(resolve, reject) {
			var _page, _browser, _cookies = [], _oktaPassed = false;
		
			puppeteer.launch({headless: ! config.headed}).then(async browser => {
				_browser = browser;
				
				_browser.on('targetchanged', async t => {
					var url = t.url().toLowerCase().trim();
								
					if(_oktaPassed) return;

					if(url.indexOf('dcg.okta') > -1) {	
						console.log(url);
						await _page.waitForSelector('#okta-signin-username'); //#okta-signin-submit');
						await _page.type('#okta-signin-username', username);
						await _page.type('#okta-signin-password', password);
						await _page.click('#okta-signin-submit');
					}
					
					if(url.indexOf(config.baseUrl.toLowerCase().trim()) > -1)
					{					
						_oktaPassed = true;
						
						var loginLogoutButton;
						
						try
						{
							loginLogoutButton = await _page.waitForSelector('#databaseLockButton', {timeout: SELECTOR_TIMEOUT_MS, visible: true});
							
							if(await _page.evaluate(el => el.innerText, loginLogoutButton) != 'Log In') {
								loginLogoutButton.click();
							}
						}
						catch(e) {
							reject('Timeout while waiting for Login button to appear');
							return;
						}
						
						if(config.institutionName) {
							var databaseSelect;

							try{
								databaseSelect = await _page.waitForSelector('#atlasDatabaseSelector', {timeout: SELECTOR_TIMEOUT_MS, visible: true});
								
								var selectID = await _page.evaluate((el, instnm) => {								
									 for(var i=0; i<el.options.length; i++) {
										var option = el.options[i];
										
										if(option.text.toLowerCase() == instnm) {
											return option.value;
										}
										
										if(option.value.toLowerCase() == instnm) {
											return option.value;
										}										
									}
									
									return null;
								}, databaseSelect, config.institutionName.toLowerCase());

								if(! selectID) {
									reject('Could not find Institution with name ' + config.institutionName);
									return;
								}
								
								await _page.select('#atlasDatabaseSelector', selectID);
							}
							catch(e) {
								reject(e);
								return;
							}
						}
						
						if(config.asOfDate) {
							var asOfDateSelect;

							try{
								asOfDateSelect = await _page.waitForSelector('#atlasAsOfDateSelector', {timeout: SELECTOR_TIMEOUT_MS, visible: true});
								
								var selectID = await _page.evaluate((el, aod) => {								
									 for(var i=0; i<el.options.length; i++) {
										var option = el.options[i];
										
										if(option.text.toLowerCase() == aod) {
											return option.value;
										}
										
										if(option.value.toLowerCase() == aod) {
											return option.value;
										}										
									}
									
									return null;
								}, asOfDateSelect, config.asOfDate);

								if(! selectID) {
									reject('Could not find as of date ' + config.asOfDate);
									return;
								}
								
								await _page.select('#atlasAsOfDateSelector', selectID);
							}
							catch(e) {
								reject(e);
								return;
							}
						}
						
						await loginLogoutButton.click();
					
						resolve({
							page: _page,
							browser: _browser,
							config: { baseUrl: config.baseUrl }
						});
					}
				});
				
				_page = await browser.newPage();
				
				await _page.goto(config.baseUrl);
			});
		});
		
		return result;
	}
};

return module.exports;