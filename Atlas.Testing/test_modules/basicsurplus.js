const th = require('../testHelpers');



	async function createBasicSurplus(page, name, toDelete){
		
			await th.clickAddWaitForModal(page);
			
			var okButton = await page.waitForSelector('.modal-footer button.btn-primary', { visible: true });
			var inputBox = await page.waitForSelector('.modal-body input', { visible: true });
            
            var uniqueName = 'Test Basic Surplus ' + new Date();
           
            //If we want to specify name for duplicate checking lets do that
            if (name != null && name != ''){
                uniqueName = name;
            }
            
            th.clearInput(inputBox);

			await inputBox.type(uniqueName);
			await okButton.click();
			
			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurplusadmin/') > -1, 2000)

			var url = page.url();
			
			await page.goBack();

			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurpluses/') > -1, 2000);

			// the binding takes a second
			await th.delay(1500);
			
			var nameTD = await page.waitForSelector('tbody.sortable > tr:last-child > td:nth-child(2)');
			
			// the name should be in the last position
			var compareUniqueName = await page.evaluate(el => el.innerText.trim(), nameTD);

			var editButton = await page.waitForSelector('tbody.sortable > tr:last-child button > i.fa-edit');
			await th.click(page, editButton);
					
			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurplusadmin/') > -1, 2000);
			
            
            //If we want to test delete then 
            if (toDelete){
                var deleteButton = await page.waitForSelector('#deleteButton', { visible: true });
			
                // have to wait for delete button to become enabled
                await th.poll(async () => {
                    var ret = await page.evaluate(el => el.parentElement.getAttribute('disabled'), deleteButton);
                    return (ret === null);
                }, 2000);
                
                await th.click(page, deleteButton);
                
                await page.waitForSelector('.modalBlockout', { visible: true });
                
                var yesButton = await page.waitForSelector('.modal-footer > button:not(.btn-primary)', { visible: true });
                await th.click(page, yesButton);
            }
            else{
                var backButton = await page.waitForSelector('#backButton', { visible: true });
                await th.click(page, backButton);
            }
                    
			
            await th.poll(() => page.url().toLowerCase().indexOf('/basicsurpluses/') > -1, 2000);
    }

    async function deleteBasicSurplus(bsName){

    }
    

    module.exports = {
        createBasicSurplus: createBasicSurplus,

    };
    