const timeout = 5000

const th = require('../testHelpers');

//Break out of unique test for testing purposes we can resue functions (like creating basic surpluses)
const bsTests = require('../test_modules/basicsurplus.js');

describe(
	'/ Basic Surplus',
	() => {
		var page = global.__HARNESS__.page;

		beforeAll(async () => {
			await th.clientGoto(page, '#/basicsurpluses/')
			await page.waitForSelector('table.table');

			//Create Three basic surpluses
			await  bsTests.createBasicSurplus(page, 'bs 1', false);
			await  bsTests.createBasicSurplus(page, 'bs 2', false);
			await  bsTests.createBasicSurplus(page, 'bs 3', false);
			await  bsTests.createBasicSurplus(page, 'bs 2', true);


		});

		test("Legend name is 'Basic Surplus'", async () => {
			var objs = await th.getObjectsByXPath(page, 'Basic Surplus', 'legend' );
			expect(objs.length).toBeTruthy();
		});
		
		test('Initial button enable state', async () => {
			expect(await th.isButtonEnabled(page, 'Back')).toBeTruthy();
			expect(await th.isButtonEnabled(page, 'Cancel')).toBeFalsy();
			expect(await th.isButtonEnabled(page, 'Save')).toBeFalsy();
		});

		test('Create Three Basic Surplues', async () => {
	

			expect(false).toBeFalsy();

		});

		test('Clicking add should bring up modal', async () => {
			await th.clickAddWaitForModal(page);
					
			var cancelButton = await page.waitForSelector('.modal-footer button.btn-cancel');
			var okButton = await page.waitForSelector('.modal-footer button.btn-primary');
			
			expect(await th.isButtonEnabled(page, cancelButton)).toBeTruthy();
			expect(await th.isButtonEnabled(page, okButton)).toBeFalsy();
			
			var inputBox = await page.waitForSelector('.modal-body input', { visible: true });
				
			await inputBox.type('1234', {delay: 10});
			
			expect(await th.isButtonEnabled(page, okButton)).toBeTruthy();
			
			for(var i=0; i<4; i++) {
				await inputBox.press('Backspace', { delay: 10 });
			}
			
			expect(await th.isButtonEnabled(page, okButton)).toBeFalsy();
			
			th.click(page, cancelButton);
						
			// modal is closed
			await page.waitForSelector('.modalBlockout', { hidden: true });
		});


	

		test('Should not be able to create package with existing name', async () => {
			var nameCell = await page.waitForSelector('tbody.sortable > tr > td:nth-child(2)');
			
			expect(nameCell).not.toBeNull();
			
			var existingName = 'BS 1'; //Hraded coded tot he one we created earlier
			
			await th.clickAddWaitForModal(page);
			
			var cancelButton = await page.waitForSelector('.modal-footer button.btn-cancel', { visible: true });
			var okButton = await page.waitForSelector('.modal-footer button.btn-primary', { visible: true });
			var inputBox = await page.waitForSelector('.modal-body input', { visible: true });
			
			async function typeAndCheck(value) {
				await inputBox.type(value, {delay: 10});
				await okButton.click();
				await page.waitForSelector('.modal-content.messageBox', { visible: true });
				var innerOkButton = await page.waitForSelector('.modal-content.messageBox button', { visible: true });
				innerOkButton.click();
				
				for(var i=0; i<value.length; i++) {
					await inputBox.press('Backspace', { delay: 10 });
				}
			}
			
			await typeAndCheck('  ' + existingName.toUpperCase() + '     ');
			await typeAndCheck('  ' + existingName.toLowerCase() + '     ');

			th.click(page, cancelButton);
			
			// modal is closed
			await page.waitForSelector('.modalBlockout', { hidden: true });
			
			// if the test didn't time out waiting for elements, it passed
			expect(true).toBe(true);
		}, 20000);

		test('Basic Surpluses should be listed in priority order', async () => {
			var arr = await th.getPrioritiesArray(page);
								
			for(var i=0; i<arr.length; i++) {
				expect(arr[i]).toBe(i);
			}
		});
		
		test('Item reorder tests', async () => {	
			var firstRow = await page.$('tbody.sortable > tr:nth-child(1) > td:nth-child(2)');
			var secondRow = await page.$('tbody.sortable > tr:nth-child(2) > td:nth-child(2)');
			
			var originalFirstRowName = await page.evaluate(el => el.innerHTML, firstRow);
			var originalSecondRowName = await page.evaluate(el => el.innerHTML, secondRow);
			
			await th.drag(page, firstRow, secondRow, { position: 'below' });
			
			// the priorities should remain in row order
			var arr = await th.getPrioritiesArray(page);
								
			for(var i=0; i<arr.length; i++) {
				expect(arr[i]).toBe(i);
			}
			
			var newFirstRowName = await page.evaluate(el => el.innerHTML, await page.$('tbody.sortable > tr:nth-child(1) > td:nth-child(2)'));
			var newSecondRowName = await page.evaluate(el => el.innerHTML, await page.$('tbody.sortable > tr:nth-child(2) > td:nth-child(2)'));
			
			// items should be swapped
			expect(newFirstRowName).toBe(originalSecondRowName);
			expect(newSecondRowName).toBe(originalFirstRowName);
			
			// save and cancel should be live
			expect(await th.isButtonEnabled(page, 'Cancel')).toBeTruthy();
			expect(await th.isButtonEnabled(page, 'Save')).toBeTruthy();
			
			// all the buttons in the rows should be disabled
			var allRowButtons = page.$('tbody.sortable > tr > td button');
			
			for(var i=0; i<allRowButtons.length; i++) {
				expect(await th.isButtonEnabled(page, allRowButtons[i])).toBeTruthy();
			}
			
			var cancelButton = (await page.$x("//button[contains(., 'Cancel')]"))[0];
			
			await cancelButton.click();
			
			// POST CANCELLATION -------------------------------------------------------------------
			
			// the priorities should remain in row order
			var arr = await th.getPrioritiesArray(page);
								
			for(var i=0; i<arr.length; i++) {
				expect(arr[i]).toBe(i);
			}
			
			// order should be restored
			var newFirstRowName = await page.evaluate(el => el.innerHTML, await page.$('tbody.sortable > tr:nth-child(1) > td:nth-child(2)'));
			var newSecondRowName = await page.evaluate(el => el.innerHTML, await page.$('tbody.sortable > tr:nth-child(2) > td:nth-child(2)'));
			expect(newFirstRowName).toBe(originalFirstRowName);
			expect(newSecondRowName).toBe(originalSecondRowName);
			
			// all the buttons in the rows should be enabled
			var allRowButtons = page.$('tbody.sortable > tr > td button');
			
			for(var i=0; i<allRowButtons.length; i++) {
				expect(await th.isButtonEnabled(page, allRowButtons[i])).toBeFalsy();
			}
			
			// save and cancel should be disabled
			expect(await th.isButtonEnabled(page, 'Cancel')).toBeFalsy();
			expect(await th.isButtonEnabled(page, 'Save')).toBeFalsy();
		});
		

		test('Clicking the edit button should to the package edit page', async () => {
			var editButton = await page.waitForSelector('button > i.fa-edit');
			await editButton.click();
			
			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurplusadmin/') > -1, 2000);
					
			await page.goBack();

			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurpluses/') > -1, 2000);
			
			// if nothing failed by this point, the test passes
			expect(true).toBe(true);
			
		}, 5000);
		
		
		/*test('Adding a new basic surplus should create it, goes to the package edit, and cause it to appear at the end of the list', async () => {
			await clickAddWaitForModal(page);
			
			var okButton = await page.waitForSelector('.modal-footer button.btn-primary', { visible: true });
			var inputBox = await page.waitForSelector('.modal-body input', { visible: true });
			
			var uniqueName = 'Test Basic Surplus ' + new Date();
			
			await inputBox.type(uniqueName, {delay: 10});
			await okButton.click();
			
			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurplusadmin/') > -1, 2000)

			var url = page.url();
			
			await page.goBack();

			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurpluses/') > -1, 2000);

			// the binding takes a second
			await th.delay(1500);
			
			var nameTD = await page.waitForSelector('tbody.sortable > tr:last-child > td:nth-child(2)');
			
			// the name should be in the last position
			var compareUniqueName = await page.evaluate(el => el.innerText.trim(), nameTD);
			expect(compareUniqueName).toBe(uniqueName);
			
			var editButton = await page.waitForSelector('tbody.sortable > tr:last-child button > i.fa-edit');
			await th.click(page, editButton);
					
			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurplusadmin/') > -1, 2000);
			
			var compareUrl = page.url();
			
			expect(compareUrl).toBe(url);
			
			var deleteButton = await page.waitForSelector('button > i.fa-trash-o', { visible: true });
			
			// have to wait for delete button to become enabled
			await th.poll(async () => {
				var ret = await page.evaluate(el => el.parentElement.getAttribute('disabled'), deleteButton);
				return (ret === null);
			}, 2000);
			
			await th.click(page, deleteButton);
			
			await page.waitForSelector('.modalBlockout', { visible: true });
			
			var yesButton = await page.waitForSelector('.modal-footer > button:not(.btn-primary)', { visible: true });
			await th.click(page, yesButton);
			
			await th.poll(() => page.url().toLowerCase().indexOf('/basicsurpluses/') > -1, 2000);
		}, 20000);*/
		
		test('Changing order and clicking save should commit order', async () => {
			var firstRow = await page.$('tbody.sortable > tr:nth-child(1) > td:nth-child(2)');
			var secondRow = await page.$('tbody.sortable > tr:nth-child(2) > td:nth-child(2)');
			
			var originalFirstRowName = await page.evaluate(el => el.innerHTML, firstRow);
			var originalSecondRowName = await page.evaluate(el => el.innerHTML, secondRow);
			
			await th.drag(page, firstRow, secondRow, { position: 'below' });
			
			var saveButton = await page.$('button > i.fa-save');
			
			await th.poll(async() => {
				var ret = await page.evaluate(el => el.getAttribute('disabled'), saveButton);
				return (ret === null);
			}, 2000);
			
			await th.click(page, saveButton);
			
			await th.clickByText(page, 'Back', 'button');
			
			await th.poll(() => /\/#\/$/.test(page.url()), 2000);
			
			await th.clientGoto(page, '#basicsurpluses');
			
			firstRow = await page.$('tbody.sortable > tr:nth-child(1) > td:nth-child(2)');
			secondRow = await page.$('tbody.sortable > tr:nth-child(2) > td:nth-child(2)');
			
			var newFirstRowName = await page.evaluate(el => el.innerHTML, firstRow);
			var newSecondRowName = await page.evaluate(el => el.innerHTML, secondRow);
			
			// items should be swapped
			expect(newFirstRowName).toBe(originalSecondRowName);
			expect(newSecondRowName).toBe(originalFirstRowName);
			
			// RESTORATION --------------------------------------------------------------
			await th.drag(page, secondRow, firstRow, { position: 'above' });
			
			saveButton = await page.$('button > i.fa-save');
			
			await th.poll(async() => {
				var ret = await page.evaluate(el => el.getAttribute('disabled'), saveButton);
				return (ret === null);
			}, 2000);
		}, 5000);
	},
	timeout
);