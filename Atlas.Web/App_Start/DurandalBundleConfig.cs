using System;
using System.Web.Optimization;

namespace Atlas.Web
{
    public class DurandalBundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(
                new ScriptBundle("~/scripts/jsbundle")
                    .Include("~/Scripts/jquery-{version}.js")
                    .Include("~/Scripts/jquery-ui-1.10.3.min.js")
                    .Include("~/Scripts/knockout-{version}.js")
                    .Include("~/Scripts/bootstrap.min.js")
                    .Include("~/Scripts/Q.js")
                    .Include("~/Scripts/toastr.js")
                    .Include("~/Scripts/breeze.min.js")
                    .Include("~/Scripts/moment.js")
                    .Include("~/Scripts/numeral.js")
                    .Include("~/Scripts/highcharts.js")
                    .Include("~/Scripts/highcharts-more.js")
                    .Include("~/Scripts/grouped-categories.js")
                    .Include("~/Scripts/thenBy.js")
                    .Include("~/Scripts/jquery.masked.js")
                );
        }

        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
            {
                throw new ArgumentNullException("ignoreList");
            }

            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            //ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }
    }
}