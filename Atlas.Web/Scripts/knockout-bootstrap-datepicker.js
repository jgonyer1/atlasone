﻿
define(['durandal/system',
        'jquery',
        'durandal/composition',
        'scripts/bootstrap-datepicker'],


        function (system, $, composition) {
            composition.addBindingHandler('datepicker', {
                init: function (element, valueAccessor, allBindingsAccessor) {
                    // First get the latest data that we're bound to
                    var value = valueAccessor();

                    // Next, whether or not the supplied model property is observable, get its current value
                    var valueUnwrapped = ko.unwrap(value);

                    //initialize datepicker with some optional options
                    var options = allBindingsAccessor().datepickerOptions || {};
                    $(element).datepicker(options);

                    //when the datepicker is hidden via a date being picked with the picker or the text box being blured/de-focused)
                    //assign the date entered to the view model
                    $(element).datepicker().on('hide', function (e) {
                        // if bound to an observable
                        if (ko.isObservable(value)) {
                            value(e.date);
                            // else bound to a normal js property
                        } else {
                            value = e.date;
                        }
                    });
                },
                update: function (element, valueAccessor) {
                    var value = ko.utils.unwrapObservable(valueAccessor());
                    $(element).datepicker("setDate", value);
                }
            });
        })