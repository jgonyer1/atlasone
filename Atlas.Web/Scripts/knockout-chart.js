﻿
define(['durandal/system', 'jquery', 'durandal/composition'],
    function (system, $, composition) {
    composition.addBindingHandler('chart', {
        counter: 0,
        prefix: '__hchrt_',
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            
            // First get the latest data that we're bound to
            var value = valueAccessor();

            // Next, whether or not the supplied model property is observable, get its current value
            var valueUnwrapped = ko.unwrap(value);

            $(element).highcharts(valueUnwrapped);
            

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            // First get the latest data that we're bound to
            var value = valueAccessor();

            // Next, whether or not the supplied model property is observable, get its current value
            var valueUnwrapped = ko.unwrap(value);

            $(element).highcharts(valueUnwrapped);
        }
    });
})