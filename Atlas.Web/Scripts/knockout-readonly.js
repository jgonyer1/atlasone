﻿
define(['durandal/system', 'jquery', 'durandal/composition'],
    function (system, $, composition) {
        ko.bindingHandlers.isReadonly = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

                // First get the latest data that we're bound to
                var value = valueAccessor();

                // Next, whether or not the supplied model property is observable, get its current value
                var valueUnwrapped = ko.unwrap(value);

                if (valueUnwrapped) {
                    element.setAttribute("readonly", "");
                } else {
                    element.removeAttribute("readonly");
                }


            },
            update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                // First get the latest data that we're bound to
                var value = valueAccessor();

                // Next, whether or not the supplied model property is observable, get its current value
                var valueUnwrapped = ko.unwrap(value);

                if (valueUnwrapped) {
                    element.setAttribute("readonly", "");
                } else {
                    element.removeAttribute("readonly");
                }
            }
        };

        //Adding here just because
        ko.bindingHandlers.formatBalance = {
            init: function (element, valueAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function () {
                        return numeral(ko.unwrap(value)).format('0,0');
                    },
                    write: function (newValue) {
                        if ($.trim(newValue) == '')
                            value("0");
                        else
                            value(numeral().unformat(newValue));

                        //value.valueHasMutated();
                    }
                }).extend({ notify: 'always' });

                if (element.tagName.toLowerCase() == 'input')
                    ko.applyBindingsToNode(element, {
                        value: interceptor
                    });
                else
                    ko.applyBindingsToNode(element, {
                        text: interceptor
                    });
            }
        }


        ko.bindingHandlers.option = {
            update: function (element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());
                ko.selectExtensions.writeValue(element, value);
            }
        };

        //Adding here just because
        ko.bindingHandlers.formatPolicy = {
            init: function (element, valueAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function () {
                        if (ko.unwrap(value) == '-999') {
                            return '';
                        }
                        else {
                            return numeral(ko.unwrap(value)).format('0.00');
                        }

                    },
                    write: function (newValue) {
                        if ($.trim(newValue) == '')
                            value("-999");
                        else
                            value(numeral().unformat(newValue));
                    }
                }).extend({ notify: 'always' });

                if (element.tagName.toLowerCase() == 'input')
                    ko.applyBindingsToNode(element, {
                        value: interceptor
                    });
                else
                    ko.applyBindingsToNode(element, {
                        text: interceptor
                    });
            }
        }

        ko.bindingHandlers.formatBPChange = {
            init: function (element, valueAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function () {
                        if (ko.unwrap(value) == '-999') {
                            return '';
                        }
                        else {
                            return numeral(ko.unwrap(value)).format('0,0');
                        }

                    },
                    write: function (newValue) {
                        if ($.trim(newValue) == '')
                            value("-999");
                        else
                            value(numeral().unformat(newValue));
                    }
                }).extend({ notify: 'always' });

                if (element.tagName.toLowerCase() == 'input')
                    ko.applyBindingsToNode(element, {
                        value: interceptor
                    });
                else
                    ko.applyBindingsToNode(element, {
                        text: interceptor
                    });
            }
        }

        ko.bindingHandlers.formatCurrentRatio = {
            init: function (element, valueAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function () {
                        if (ko.unwrap(value) == '-999') {
                            return '';
                        }
                        else {
                            return numeral(ko.unwrap(value) * 100).format('0.00');
                        }

                    },
                    write: function (newValue) {
                        if ($.trim(newValue) == '')
                            value("-999");
                        else
                            value(numeral(numeral().unformat(newValue)).value() / 100);
                    }
                }).extend({ notify: 'always' });

                if (element.tagName.toLowerCase() == 'input')
                    ko.applyBindingsToNode(element, {
                        value: interceptor
                    });
                else
                    ko.applyBindingsToNode(element, {
                        text: interceptor
                    });
            }
        }

        //Adding here just because
        ko.bindingHandlers.formatBalanceBlank = {
            init: function (element, valueAccessor) {
                var value = valueAccessor();
                var interceptor = ko.computed({
                    read: function () {
                        if (ko.unwrap(value) == '-999') {
                            return '';
                        }
                        else {
                            return numeral(ko.unwrap(value)).format('0,0');
                        }

                    },
                    write: function (newValue) {
                        if ($.trim(newValue) == '')
                            value("-999");
                        else
                            value(numeral().unformat(newValue));
                    }
                }).extend({ notify: 'always' });

                if (element.tagName.toLowerCase() == 'input')
                    ko.applyBindingsToNode(element, {
                        value: interceptor
                    });
                else
                    ko.applyBindingsToNode(element, {
                        text: interceptor
                    });
            }
        }
        //source http://www.knockmeout.net/2011/03/guard-your-model-accept-or-cancel-edits.html
        //with an Edit by Jake to include the actual dataModel observable because Entity / Knockout doesn't let us do much to actual data model properties
        //wrapper to an observable that requires accept/cancel
        ko.protectedObservable = function (initialValue, dataModelObs) {
            //private variables
            var _actualValue = ko.observable(initialValue),
                _tempValue = initialValue;

            //computed observable that we will return
            var result = ko.computed({
                //always return the actual value
                read: function () {
                    return _actualValue();
                },
                //stored in a temporary spot until commit
                write: function (newValue) {
                    _tempValue = newValue;
                }
            }).extend({ notify: "always" });

            //if different, commit temp value
            result.commit = function () {
                if (_tempValue !== _actualValue()) {
                    _actualValue(_tempValue);
                    if (dataModelObs != null && dataModelObs != undefined) {
                        dataModelObs(_tempValue);
                    }
                }
            };

            //force subscribers to take original
            result.reset = function () {
                _actualValue.valueHasMutated();
                _tempValue = _actualValue();   //reset temp value
            };

            return result;
        };


    })