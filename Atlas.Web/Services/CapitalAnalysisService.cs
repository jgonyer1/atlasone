﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    //Class that stores the chart values
    class CapAnalChart
    {
        public string chartName { get; set; }
        public string[] categories { get; set; }
        public CapAnalChartValue[] chartValues { get; set; }


    }

    class CapAnalChartValue
    {
        public double policyLimit { get; set; }
        public double wellCap { get; set; }
        public double currRatio { get; set; }
    }

    public class CapitalAnalysisService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object CapitalAnalysisViewModel(int reportId)
        {

             InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            
            
            //Look Up Funding Matrix Settings
            var sr = gctx.CapitalAnalysis.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            InstitutionService instService = new InstitutionService(sr.InstitutionDatabaseName);

            //If Not Found Kick Out
            if (sr == null) throw new Exception("Capital Analysis Report Not Found");

            //Build in statment for offsets
            string asOfDateIn = "";
            DateTime[] asOfDates = new DateTime[sr.HistoricalBufferOffsets.Count];
            int aodCounter = 0;

            bool isCreditUnion = false;
            GlobalController gc = new GlobalController();
            DataTable allAODS = gc.AsOfDates();
            string curCapAod = "";
            if(allAODS.Rows.Count >= sr.CurrentCapitalDateOffset)
            {
                curCapAod = DateTime.Parse(allAODS.Rows[sr.CurrentCapitalDateOffset]["asOfDateDescript"].ToString()).ToShortDateString();
            }
            else
            {
                curCapAod = allAODS.Rows[0]["asOfDateDescript"].ToString();
            }
            if (gc.CreditUnionStatus().Rows[0]["InstType"].ToString() == "BK")
            {
                isCreditUnion = false;
            }
            else
            {
                isCreditUnion = true;
            }

            

            foreach (CapitalAnalysisHistoricalBufferOffsets offSet in sr.HistoricalBufferOffsets)
            {
                Information info = instService.GetInformation(rep.Package.AsOfDate, offSet.OffSet);

                if (asOfDateIn == "")
                {
                    asOfDateIn += "('" + info.AsOfDate.ToShortDateString() + "'";
                }
                else
                {
                    asOfDateIn += ",'" + info.AsOfDate.ToShortDateString() + "'";
                }
                asOfDates[aodCounter] = info.AsOfDate;
                aodCounter += 1;
            }
            asOfDateIn += ")";

            //Sorty As Of Date Array
            Array.Sort(asOfDates);



            //Build out in statement for capital policy Names
            string polNames = String.Format("('{0}','{1}','{2}','{3}')", sr.BottomLeftRatio, sr.BottomRightRatio, sr.TopLeftRatio, sr.TopRightRatio);

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	p.asOfDate ");
            sqlQuery.AppendLine(" 	,ac.name ");
            sqlQuery.AppendLine(" 	,ac.PolicyLimit / 100 as policyLimit ");
            sqlQuery.AppendLine(" 	,ac.wellCap / 100 as wellCap ");
            sqlQuery.AppendLine(" 	,ac.currentRatio as currRatio ");
            sqlQuery.AppendLine(" 	FROM Atlas_Policy as p ");
            sqlQuery.AppendLine(" 	INNER JOIN ATLAS_CapitalPolicy AS ac ");
            sqlQuery.AppendLine(" 	ON ac.PolicyId = p.id ");
            sqlQuery.AppendLine(" 	WHERE ");
            sqlQuery.AppendLine(" 	p.asOfDate IN ");
            sqlQuery.AppendLine(" 	" + asOfDateIn  + " ");
            sqlQuery.AppendLine(" 	AND ac.name IN  ");
            sqlQuery.AppendLine(" " + polNames + " ");

            //Loads Results into table
            DataTable results = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());

            //Declare CapAnalChartsArry
            CapAnalChart[] chts = new CapAnalChart[4];

            chts[0] = new CapAnalChart();
            chts[0].chartName = sr.TopLeftRatio;
            chts[0].categories = new string[sr.HistoricalBufferOffsets.Count];
            chts[0].chartValues = new CapAnalChartValue[sr.HistoricalBufferOffsets.Count];

            chts[1] = new CapAnalChart();
            chts[1].chartName = sr.TopRightRatio;
            chts[1].categories = new string[sr.HistoricalBufferOffsets.Count];
            chts[1].chartValues = new CapAnalChartValue[sr.HistoricalBufferOffsets.Count];

            chts[2] = new CapAnalChart();
            chts[2].chartName = sr.BottomLeftRatio;
            chts[2].categories = new string[sr.HistoricalBufferOffsets.Count];
            chts[2].chartValues = new CapAnalChartValue[sr.HistoricalBufferOffsets.Count];

            chts[3] = new CapAnalChart();
            chts[3].chartName = sr.BottomRightRatio;
            chts[3].categories = new string[sr.HistoricalBufferOffsets.Count];
            chts[3].chartValues = new CapAnalChartValue[sr.HistoricalBufferOffsets.Count];

            int cnt = 0;
            foreach (DateTime aod in asOfDates)
            {
                chts[0].categories[cnt] = aod.ToShortDateString();
                chts[1].categories[cnt] = aod.ToShortDateString();
                chts[2].categories[cnt] = aod.ToShortDateString();
                chts[3].categories[cnt] = aod.ToShortDateString();

                chts[0].chartValues[cnt] = new CapAnalChartValue();
                chts[1].chartValues[cnt] = new CapAnalChartValue();
                chts[2].chartValues[cnt] = new CapAnalChartValue();
                chts[3].chartValues[cnt] = new CapAnalChartValue();

                cnt += 1;
            }
            //Loop through results and populate chart objects
            foreach (DataRow dr in results.Rows)
            {

                //Take each row look up asofdate index from as of date array as well as chart object to put the data
                DateTime aod = DateTime.Parse(dr["asOfDate"].ToString());
                int aodIndex = Array.IndexOf(asOfDates, aod);
                int chartIndex = Array.FindIndex(chts, chartName => chartName.chartName == dr["name"].ToString());

                chts[chartIndex].chartValues[aodIndex].policyLimit = double.Parse(dr["PolicyLimit"].ToString());
                chts[chartIndex].chartValues[aodIndex].wellCap = double.Parse(dr["WellCap"].ToString());
                chts[chartIndex].chartValues[aodIndex].currRatio = double.Parse(dr["currRatio"].ToString());

            }

            //Now Build out table for bottom of chart

            //Load as of date from offset settings
            Information tblInfo = instService.GetInformation(rep.Package.AsOfDate, sr.CurrentCapitalDateOffset);
            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("     cp.name ");
            sqlQuery.AppendLine("     ,cp.PolicyLimit as PolicyLimits ");
            sqlQuery.AppendLine("     ,cp.wellCap ");
            sqlQuery.AppendLine("     ,cp.CurrentRatio ");
            sqlQuery.AppendLine(" FROM Atlas_CapitalPolicy as cp ");
            sqlQuery.AppendLine(" INNER JOIN Atlas_Policy as p ");
            sqlQuery.AppendLine(" ON cp.PolicyId = p.id ");
            sqlQuery.AppendLine(" WHERE p.asOfDate = '" + tblInfo.AsOfDate + "' ");
            DataTable tblResults = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());

            //Create out table and columbns for out table
            DataTable outTable = new DataTable();
            outTable.Columns.Add("name");
            outTable.Columns.Add("ratio");
            outTable.Columns.Add("amount");
            outTable.Columns.Add("policylimits");
            outTable.Columns.Add("polCapital");
            outTable.Columns.Add("polAssets");
            outTable.Columns.Add("wellCapLimits");
            outTable.Columns.Add("wcCapital");
            outTable.Columns.Add("wcAssets");



            Dictionary<string, DataRow> values = new Dictionary<string, DataRow>();

            //Loop through resutls and build out dictionary for calculation puproses
            foreach(DataRow dr in tblResults.Rows)
            {
                values.Add(dr["name"].ToString(), dr);
            }

            if (isCreditUnion)
            {
                double A = double.Parse(values["Net Worth Ratio"]["policylimits"].ToString()) / 100;
                double B = double.Parse(values["Net Worth Ratio"]["wellCap"].ToString()) / 100;
                double C = double.Parse(values["Net Worth Ratio"]["CurrentRatio"].ToString());
                double D = double.Parse(values["Net Worth"]["CurrentRatio"].ToString());
                double E = double.Parse(values["Total Assets for Net Worth Ratio"]["CurrentRatio"].ToString());

                DataRow dr = outTable.NewRow();
                dr["name"] = "Net Worth";
                dr["ratio"] = C.ToString();
                dr["amount"] = (D / 1000).ToString();
                dr["policylimits"] = A.ToString();
                dr["polCapital"] = (((A - C) * E) / 1000).ToString();
                dr["polAssets"] = (((D / A) - E) / 1000).ToString();
                dr["wellCapLimits"] = B.ToString();
                dr["wcCapital"] = (((B - C) * E) / 1000).ToString();
                dr["wcAssets"] = (((D / B) - E) / 1000).ToString();
                outTable.Rows.Add(dr);

                dr = outTable.NewRow();
                dr["name"] = "Total Assets For Net Worth Ratio";
                dr["ratio"] = "";
                dr["amount"] = (E / 1000).ToString();
                dr["policylimits"] = "";
                dr["polCapital"] = "";
                dr["polAssets"] = "";
                dr["wellCapLimits"] = "";
                dr["wcCapital"] = "";
                dr["wcAssets"] = "";
                outTable.Rows.Add(dr);
            }
            else
            {
                //Set up variables used in formula
                double A = double.Parse(values["Tier 1 Leverage Ratio"]["policylimits"].ToString()) / 100;
                double B = double.Parse(values["Tier 1 Leverage Ratio"]["wellCap"].ToString()) / 100;
                double C = double.Parse(values["Tier 1 Leverage Ratio"]["CurrentRatio"].ToString());

                double D = double.Parse(values["Tier 1 Common Capital (CET1) Risk Based"]["policylimits"].ToString()) / 100;
                double E = double.Parse(values["Tier 1 Common Capital (CET1) Risk Based"]["wellCap"].ToString()) / 100;
                double F = double.Parse(values["Tier 1 Common Capital (CET1) Risk Based"]["CurrentRatio"].ToString());

                double G = double.Parse(values["Tier 1 Capital Ratio Risk Based"]["policylimits"].ToString()) / 100;
                double H = double.Parse(values["Tier 1 Capital Ratio Risk Based"]["wellCap"].ToString()) / 100;
                double I = double.Parse(values["Tier 1 Capital Ratio Risk Based"]["CurrentRatio"].ToString());

                double J = double.Parse(values["Total Capital Ratio Risk Based"]["policylimits"].ToString()) / 100;
                double K = double.Parse(values["Total Capital Ratio Risk Based"]["wellCap"].ToString()) / 100;
                double L = double.Parse(values["Total Capital Ratio Risk Based"]["CurrentRatio"].ToString());

                double M = double.Parse(values["Common Capital (CET1)"]["CurrentRatio"].ToString());
                double N = double.Parse(values["Tier 1 Capital"]["CurrentRatio"].ToString());
                double O = double.Parse(values["Total Capital"]["CurrentRatio"].ToString());
                double P = double.Parse(values["Total Assets for Leverage Ratio"]["CurrentRatio"].ToString());
                double Q = double.Parse(values["Total Risk Weighted Assets"]["CurrentRatio"].ToString());

                //Now using dictionary add rows for calculating

                DataRow tier1Leverage = outTable.NewRow();
                tier1Leverage["name"] = "TIER 1 LEVERAGE";
                tier1Leverage["ratio"] = C.ToString();
                tier1Leverage["amount"] = (N / 1000).ToString();
                tier1Leverage["policylimits"] = A.ToString();
                tier1Leverage["polCapital"] = (((A - C) * P) / 1000).ToString();
                tier1Leverage["polAssets"] = (((N / A) - P) / 1000).ToString();
                tier1Leverage["wellCapLimits"] = B.ToString();
                tier1Leverage["wcCapital"] = (((B - C) * P) / 1000).ToString();
                tier1Leverage["wcAssets"] = (((N / B) - P) / 1000).ToString();
                outTable.Rows.Add(tier1Leverage);


                DataRow totAssetLeverage = outTable.NewRow();
                totAssetLeverage["name"] = "TOTAL ASSETS FOR LEVERAGE RATIO";
                totAssetLeverage["ratio"] = "";
                totAssetLeverage["amount"] = (P / 1000).ToString();
                totAssetLeverage["policylimits"] = "";
                totAssetLeverage["polCapital"] = "";
                totAssetLeverage["polAssets"] = "";
                totAssetLeverage["wellCapLimits"] = "";
                totAssetLeverage["wcCapital"] = "";
                totAssetLeverage["wcAssets"] = "";
                outTable.Rows.Add(totAssetLeverage);

                DataRow commonEq = outTable.NewRow();
                commonEq["name"] = "COMMON EQUITY TIER 1";
                commonEq["ratio"] = F.ToString();
                commonEq["amount"] = (M / 1000).ToString();
                commonEq["policylimits"] = D.ToString();
                commonEq["polCapital"] = (((D - F) * Q) / 1000).ToString();
                commonEq["polAssets"] = (((M / D) - Q) / 1000).ToString();
                commonEq["wellCapLimits"] = E.ToString();
                commonEq["wcCapital"] = (((E - F) * Q) / 1000).ToString();
                commonEq["wcAssets"] = (((M / E) - Q) / 1000).ToString();
                outTable.Rows.Add(commonEq);


                DataRow tier1Cap = outTable.NewRow();
                tier1Cap["name"] = "TIER 1 CAPITAL";
                tier1Cap["ratio"] = I.ToString();
                tier1Cap["amount"] = (N / 1000).ToString();
                tier1Cap["policylimits"] = G.ToString();
                tier1Cap["polCapital"] = (((G - I) * Q) / 1000).ToString();
                tier1Cap["polAssets"] = (((N / G) - Q) / 1000).ToString();
                tier1Cap["wellCapLimits"] = H.ToString();
                tier1Cap["wcCapital"] = (((H - I) * Q) / 1000).ToString();
                tier1Cap["wcAssets"] = (((N / H) - Q) / 1000).ToString();
                outTable.Rows.Add(tier1Cap);

                DataRow totCap = outTable.NewRow();
                totCap["name"] = "TOTAL CAPITAL";
                totCap["ratio"] = L.ToString();
                totCap["amount"] = (O / 1000).ToString();
                totCap["policylimits"] = J.ToString();
                totCap["polCapital"] = (((J - L) * Q) / 1000).ToString();
                totCap["polAssets"] = (((O / J) - Q) / 1000).ToString();
                totCap["wellCapLimits"] = K.ToString();
                totCap["wcCapital"] = (((K - L) * Q) / 1000).ToString();
                totCap["wcAssets"] = (((O / K) - Q) / 1000).ToString();
                outTable.Rows.Add(totCap);

                DataRow totRisk = outTable.NewRow();
                totRisk["name"] = "TOTAL RISK WEIGHTED ASSETS";
                totRisk["ratio"] = "";
                totRisk["amount"] = (Q / 1000).ToString();
                totRisk["policylimits"] = J.ToString();
                totRisk["polCapital"] = "";
                totRisk["polAssets"] = "";
                totRisk["wellCapLimits"] = "";
                totRisk["wcCapital"] = "";
                totRisk["wcAssets"] = "";
                outTable.Rows.Add(totRisk);
            }

            

            return new
            {
                curCapAod = curCapAod,
                outTable = outTable,
                charts = chts,
                chartIncPol = sr.IncludePolicyLimitsOffSets,
                chartIncWell = sr.IncludeWellCapLimitsOffSets,
                tableIncPol = sr.IncludePolicyLimits,
                tableIncWell = sr.IncludeWellCapLimits
            };

        }
    }
}