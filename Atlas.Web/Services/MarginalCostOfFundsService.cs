﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class MarginalCostOfFundsService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object MarginalCostOfFundsViewModel(int reportId)
        {
            
            string conStr = Utilities.BuildInstConnectionString("");
            InstitutionContext gctx = new InstitutionContext(conStr);
            string instDBName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();

            var instType = Utilities.ExecuteSql(conStr, "select InstType from [DWR_FedRates].[dbo].[DatabaseHighlineMapping] where DatabaseName = '" + instDBName + "'");
            string bankType = "Bank";
            if (instType.Rows[0]["InstType"].ToString().Equals("CU"))
            {
                bankType = "Credit Union";
            }

            //Look Up Funding Matrix Settings
            var mcf = gctx.MarginalCostOfFunds.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (mcf == null) throw new Exception("Marginal Cost Of Funds not found");

            var instService = new InstitutionService("");

            var asOfDate = rep.Package.AsOfDate;
            
            
            string[] runoffs = mcf.Runoffs.Split(',');
            string[] rateChanges = mcf.RateChanges.Split(',');
            double[][] values = new double[runoffs.Length][];

            for (int ro = 0; ro < runoffs.Length; ro++)
            {
                values[ro] = new double[rateChanges.Length];
                for (int rc = 0; rc < rateChanges.Length; rc++)
                {
                    values[ro][rc] = GetValue(mcf.Rate, mcf.Balance, rateChanges[rc], runoffs[ro], mcf.IsReduction);
                }
            }
            

            return new
            {
                mcf = mcf,
                matrixValues = values,
                bankType = bankType,
                errors = errors,
                warnings = warnings
            };
        }

        public double GetValue(double curRate, double balance, string rcN, string roN, bool isReduction) { 
            double _rcN = 0.0;
            double _roN = 0.0;

            if (!Double.TryParse(rcN, out _rcN))
            {
                errors.Add("Rate Change Could Not Be Converted to Decimal");
            }
            if (!Double.TryParse(roN, out _roN))
            {
                errors.Add("Runoff Could Not Be Converted to Decimal");
            }
            //_rcN /= 100;
            _roN /= 100;

            if(isReduction)
                return ((_rcN * (1 - _roN) * balance) / (balance * _roN))+ curRate;
            else
                return ((_rcN * (1 - _roN) * balance) / (balance * _roN)) + (curRate + _rcN);
        }

        public string GetRoundRatioString(double value)
        {
            return String.Format("{0:f2}",Math.Round(value, 2, MidpointRounding.AwayFromZero));
        }
        public string GetRoundBPString(double value)
        {
            return Math.Round(value, 0, MidpointRounding.AwayFromZero).ToString();
        }
    }
}