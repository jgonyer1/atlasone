﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using System.Text;
using System.Data;

namespace Atlas.Web.Services
{
    public class L360PushHelper
    {
        public enum PeriodType
        {
            Monthly,
            Quarterly
        }

        public struct FieldRollup
        {
            public readonly string fieldName;
            public readonly DateTime startDate;
            public readonly int numMonthly, numQuarterly;

            public FieldRollup(string fieldName, DateTime startDate, int numMonthly, int numQuarterly)
            {
                this.fieldName = fieldName;
                this.startDate = startDate;
                this.numMonthly = numMonthly;
                this.numQuarterly = numQuarterly;
            }
        }

        static protected string PeriodName(string fieldName, PeriodType periodType)
        {
            switch (periodType)
            {
                case PeriodType.Quarterly:
                    return fieldName + "-Quarterly";
                default: //PeriodType.Monthly
                    return fieldName + "-Monthly";
            }
        }

        static protected int PeriodInterval(PeriodType periodType)
        {
            switch (periodType)
            {
                case PeriodType.Quarterly:
                    return 3;
                default: //PeriodType.Monthly
                    return 1;
            }
        }

        /// <summary>
        /// Tree of the input data in the form: accType -> rbcTier -> fieldName -> month -> data
        /// </summary>
        protected Dictionary<int, Dictionary<int, Dictionary<string, Dictionary<DateTime, double>>>> DataMap { get; set; }

        public L360PushHelper(DataTable dt,
            string accTypeCol = "Acc_TypeOR", string rbcTierCol = "ClassOR",
            string fieldNameCol = "FieldName", string dataCol = "Data",
            string monthCol = "Month", string nMonthsCol = "NMonths",
            Dictionary<string, string> groupFields = null,
            FieldRollup[] rollFields = null)
        {
            //Someone somewhere sometime will hate me for this and I apologize, I was in a rush :)
            this.DataMap = new Dictionary<int, Dictionary<int, Dictionary<string, Dictionary<DateTime, double>>>>();
            this.SQLQuery = new StringBuilder();
            int accType, rbcTier;
            string fieldName;
            double data;
            DateTime month;
            int nMonths, i, j, numMonthlyQuarterly;
            foreach (DataRow drow in dt.Rows)
            {
                //Figure out the data
                //If these types are not as expected we've done something wrong, so use explicit casts to fail early
                //The one exception is month and nMonths, which may validly be null for an entirely pre-grouped field (like ALAs)
                accType = (int)drow[accTypeCol];
                rbcTier = (int)drow[rbcTierCol];
                fieldName = drow[fieldNameCol].ToString();
                data = (double)drow[dataCol];
                month = DateTime.Parse(((!Convert.IsDBNull(drow[monthCol])) ? drow[monthCol] : default(DateTime)).ToString()); //This must be a non-null value as it's a dict key, so we'll just use the default

                //Group data as needed
                if (groupFields.ContainsKey(fieldName))
                    fieldName = groupFields[fieldName];

                //Map the data - either make a new record or add to existing grouped record
                if (!this.DataMap.ContainsKey(accType))
                    this.DataMap.Add(accType, new Dictionary<int, Dictionary<string, Dictionary<DateTime, double>>>());
                if (!this.DataMap[accType].ContainsKey(rbcTier))
                    this.DataMap[accType].Add(rbcTier, new Dictionary<string, Dictionary<DateTime, double>>());
                if (!this.DataMap[accType][rbcTier].ContainsKey(fieldName))
                    this.DataMap[accType][rbcTier].Add(fieldName, new Dictionary<DateTime, double>());
                if (!this.DataMap[accType][rbcTier][fieldName].ContainsKey(month))
                    this.DataMap[accType][rbcTier][fieldName].Add(month, data);
                else
                    this.DataMap[accType][rbcTier][fieldName][month] += data;

                //Also add monthly breakout here
                //we don't need to explicitly store nMonths as we can deduce it later via math magic
                //(and I don't feel like loading it into a struct)
                foreach (FieldRollup fr in rollFields)
                {
                    if (fr.fieldName == fieldName)
                    {
                        fieldName = PeriodName(fieldName, PeriodType.Monthly);
                        nMonths = (int)drow[nMonthsCol];
                        if (!this.DataMap[accType][rbcTier].ContainsKey(fieldName))
                            this.DataMap[accType][rbcTier].Add(fieldName, new Dictionary<DateTime, double>());
                        if (!this.DataMap[accType][rbcTier][fieldName].ContainsKey(month))
                            this.DataMap[accType][rbcTier][fieldName].Add(month, data / (float)nMonths); //If nMonths is zero, let it blow up as this should never happen
                        else
                            this.DataMap[accType][rbcTier][fieldName][month] += data / (float)nMonths;
                    }
                }
            }

            //Roll fields
            foreach (FieldRollup fr in rollFields)
            {
                numMonthlyQuarterly = Math.Max(fr.numMonthly, fr.numQuarterly * PeriodInterval(PeriodType.Quarterly)); //We build quarterly off monthly, so it must go out at least as far
                foreach (var accTypeDict in this.DataMap.Values)
                {
                    foreach (var rbcTierDict in accTypeDict.Values)
                    {
                        //Monthly Data - fill in the blanks (either with previous month's data if within nMonths or 0)
                        fieldName = PeriodName(fr.fieldName, PeriodType.Monthly);
                        month = fr.startDate;
                        nMonths = 0;
                        data = 0;
                        if (!rbcTierDict.ContainsKey(fieldName))
                            rbcTierDict.Add(fieldName, new Dictionary<DateTime, double>());
                        for (i = 0; i < numMonthlyQuarterly; i++)
                        {
                            //Reset data to 0 if we're no longer within nMonths
                            if (nMonths <= 0)
                                data = 0;

                            //If a record doesn't exist, it's either intenionally blank (0) or we're filling in from previous months if within nMonths
                            if (!rbcTierDict[fieldName].ContainsKey(month))
                                rbcTierDict[fieldName].Add(month, data);
                            else
                            {
                                //This should never happen
                                if (nMonths > 0)
                                    throw new Exception("Hit an existing record while filling in monthly data! Do you have two records within NMonths of eachother?");

                                //Re-deduce nMonths via math magic - zero value is ok here as it just means it was a blank record
                                nMonths = (rbcTierDict[fieldName][month] == 0) ? 0 : Convert.ToInt32(rbcTierDict[fr.fieldName][month] / rbcTierDict[fieldName][month]); //Convert handles ToEven rounding for us
                                data = rbcTierDict[fieldName][month];
                            }

                            month = month.AddMonths(PeriodInterval(PeriodType.Monthly));
                            nMonths--;
                        }

                        //Quarterly Data - just march through monthly records inserting a new quarterly records every 3 months
                        fieldName = PeriodName(fr.fieldName, PeriodType.Quarterly);
                        month = fr.startDate;
                        nMonths = PeriodInterval(PeriodType.Quarterly); //Always filling in 3 months of data
                        if (!rbcTierDict.ContainsKey(fieldName))
                            rbcTierDict.Add(fieldName, new Dictionary<DateTime, double>());
                        for (i = 0; i < fr.numQuarterly; i++)
                        {
                            //Create a new record every 3 months with aggregated monthly data
                            data = 0;
                            for (j = 0; j < nMonths; j++)
                                data += rbcTierDict[PeriodName(fr.fieldName, PeriodType.Monthly)][month.AddMonths(j)];
                            rbcTierDict[fieldName].Add(month, data);

                            month = month.AddMonths(nMonths);
                        }
                    }
                }
            }
        }

        public double GetField(string fieldName, int accType, int rbcTier, DateTime month = default(DateTime))
        {
            if (this.DataMap.ContainsKey(accType)
            && this.DataMap[accType].ContainsKey(rbcTier)
            && this.DataMap[accType][rbcTier].ContainsKey(fieldName)
            && this.DataMap[accType][rbcTier][fieldName].ContainsKey(month))
                return this.DataMap[accType][rbcTier][fieldName][month] / 1000.0; //Treat BASIS data as scaled
            return 0;
        }

        public double GetFields(string fieldName, int accType, int[] rbcTiers = null, DateTime month = default(DateTime))
        {
            double ret = 0;

            //Bail early if no accType
            if (!this.DataMap.ContainsKey(accType))
                return ret;

            //If we've only specified accType, just get all rbcTiers
            if (rbcTiers == null || rbcTiers.Length < 1)
                rbcTiers = this.DataMap[accType].Keys.ToArray();

            foreach (var rbcTier in rbcTiers)
                ret += this.GetField(fieldName, accType, rbcTier, month);

            return ret;
        }

        public double GetFieldsExcl(string fieldName, int accType, int[] rbcTiers = null, DateTime month = default(DateTime))
        {
            double ret = 0;

            //First get all rbcTiers for this accType...
            ret += this.GetFields(fieldName, accType, month: month);

            //... then subtract the ones we don't want (unless the array is empty, in which case this will function like GetFields)
            if (rbcTiers != null && rbcTiers.Length > 0)
                ret -= this.GetFields(fieldName, accType, rbcTiers, month);

            return ret;
        }

        public double[] GetFieldRange(DateTime startDate, PeriodType periodType, int numPeriods, string fieldName, int accType, int rbcTier)
        {
            double[] ret = new double[numPeriods];
            for (int i = 0; i < numPeriods; i++)
            {
                ret[i] = this.GetField(PeriodName(fieldName, periodType), accType, rbcTier, startDate);
                startDate = startDate.AddMonths(PeriodInterval(periodType));
            }

            return ret;
        }

        public double[] GetFieldsRange(DateTime startDate, PeriodType periodType, int numPeriods, string fieldName, int accType, int[] rbcTiers = null)
        {
            double[] ret = new double[numPeriods];
            for (int i = 0; i < numPeriods; i++)
            {
                ret[i] = this.GetFields(PeriodName(fieldName, periodType), accType, rbcTiers, startDate);
                startDate = startDate.AddMonths(PeriodInterval(periodType));
            }

            return ret;
        }

        public double[] GetFieldsRangeExcl(DateTime startDate, PeriodType periodType, int numPeriods, string fieldName, int accType, int[] rbcTiers = null)
        {
            double[] ret = new double[numPeriods];
            for (int i = 0; i < numPeriods; i++)
            {
                ret[i] = this.GetFieldsExcl(PeriodName(fieldName, periodType), accType, rbcTiers, startDate);
                startDate = startDate.AddMonths(PeriodInterval(periodType));
            }

            return ret;
        }

        /// <summary>
        /// Queue for pending SQL commands.
        /// </summary>
        protected StringBuilder SQLQuery { get; set; }

        /// <summary>
        /// Current SQL queue size.
        /// </summary>
        protected int SQLQueueCount { get; set; }

        /// <summary>
        /// Connection string for the SQL queue executor.
        /// </summary>
        public string SQLQueueConStr { get; set; }

        /// <summary>
        /// Threshold over which any queued updates will automaticlly execute, clearing the queue.
        /// </summary>
        public int SQLQueueAutoExecuteThreshold { get; set; }

        protected DataTable SQLCheckQueue()
        {
            if (this.SQLQueueCount++ > this.SQLQueueAutoExecuteThreshold)
            {
                SQLQueueCount = 0;
                return this.SQLExecuteQueue();
            }
            return null;
        }

        public DataTable SQLExecuteQueue()
        {
            var ret = Utilities.ExecuteSql(this.SQLQueueConStr, this.SQLQuery.ToString());
            this.SQLQuery.Clear();
            return ret;
        }

        public DataTable SQLPushLRM(int bankKey, double data, string srcGroup, string srcRow)
        {
            this.SQLQuery.AppendLine(String.Format("UPDATE [DIP_Data].[dbo].[LRmReport] SET [currentTrend] = {1} WHERE asOfDate = '1/1/1910' AND BankKey = {0} AND SrcGroup = '{2}' AND SrcRow = '{3}'",
                bankKey, data, srcGroup, srcRow));
            return this.SQLCheckQueue();
        }

        public DataTable SQLPushSOF(int bankKey, double[][] dataArrays, string srcName, string colName)
        {
            int j;
            for (int i = 0; i < 2; i++)
            {
                this.SQLQuery.AppendLine("UPDATE [DIP_Data].[dbo].[Sourcesoffunds]");
                this.SQLQuery.AppendLine("SET");
                for (j = 0; j < 24; j++)
                {
                    this.SQLQuery.Append(String.Format("    [Q{0}] = {1}", j + 1, dataArrays[i][j]));
                    this.SQLQuery.AppendLine((j < 23) ? "," : "");
                }
                this.SQLQuery.AppendLine(String.Format("WHERE asOfDate = '1/1/1910' AND BankKey = {0} AND PeriodType = {1} AND SrcName = '{2}' AND colName = '{3}'",
                    bankKey, i, srcName, colName));
            }
            return this.SQLCheckQueue();
        }

        public DataTable SQLPushSOF(int bankKey, double data, string srcName, string colName)
        {
            double[][] dataArrays = new double[][] { new double[24], new double[24] };
            for (int i = 0; i < 2; i++)
                dataArrays[i][0] = data;
            return this.SQLPushSOF(bankKey, dataArrays, srcName, colName);
        }

        public DataTable SQLPushAR(int bankKey, double data, string srcGroup, string srcCat)
        {
            this.SQLQuery.AppendLine(String.Format("UPDATE [DIP_Data].[dbo].[AlcoRatios] SET [ALCOValue] = {1} WHERE asOfDate = '1/1/1910' AND BankKey = {0} AND SrcGroup = '{2}' AND SrcCat = '{3}'",
                bankKey, data, srcGroup, srcCat));
            return this.SQLCheckQueue();
        }

        public DataTable SQLPushAEC(int bankKey, double data, string srcKey, string srcCol)
        {
            this.SQLQuery.AppendLine(String.Format("UPDATE [DIP_Data].[dbo].[AllocatedExCol] SET [TotalPortBal] = {1} WHERE AsOfDate = '1/1/1910' AND BankKey = {0} AND srcKey = '{2}' AND SrcCol = '{3}'",
                bankKey, data, srcKey, srcCol));
            return this.SQLCheckQueue();
        }

        public DataTable SQLPushCOL(int bankKey, double[] dataArray, string srcKey, string srcCol)
        {
            this.SQLQuery.AppendLine("UPDATE [DIP_Data].[dbo].[Collateral]");
            this.SQLQuery.AppendLine(String.Format("SET [RetailRepos] = {0},", dataArray[0]));
            this.SQLQuery.AppendLine(String.Format("	[SecuredDeposits] = {0},", dataArray[1]));
            this.SQLQuery.AppendLine(String.Format("	[FHLB] = {0},", dataArray[2]));
            this.SQLQuery.AppendLine(String.Format("	[OtherBorrows] = {0},", dataArray[3]));
            this.SQLQuery.AppendLine(String.Format("	[FRB] = {0}", dataArray[4]));
            this.SQLQuery.AppendLine(String.Format("WHERE AsOfDate = '1/1/1910' AND BankKey = {0} AND SrcKey = '{1}' AND SrcCol = '{2}'",
                bankKey, srcKey, srcCol));
            return this.SQLCheckQueue();
        }
    }
}
