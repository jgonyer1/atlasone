﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class RateChangeMatrixService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object RateChangeMatrixViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.RateChangeMatrices.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("Rate Change Matrix Not Found");


            DataTable rates = new DataTable();

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            string[] scens = new string[sr.RateChangeMatrixScenarios.Count];
            //Find simulation Id
            var sim = instService.GetSimulation(rep.Package.AsOfDate, sr.AsOfDateOffset, sr.SimulationType);

            if (sim == null) {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                int simId = sim.Id;

                //Set Sfirst of month based off the offset
                string firstOfMonth = new DateTime(sim.Information.AsOfDate.Year, sim.Information.AsOfDate.Month, 1).ToShortDateString();

                //Builds Scenario In statement and puts scenario in order in array to pass back to view
                string scenInStatement = "(";
                
                int scenCounter = 0;
                foreach (RateChangeMatrixScenario srs in sr.RateChangeMatrixScenarios.OrderBy(s => s.Priority)) {
                    if (scenInStatement == "(") {
                        scenInStatement += "'" + srs.ScenarioTypeId.ToString() + "'";
                    }
                    else {
                        scenInStatement += ",'" + srs.ScenarioTypeId.ToString() + "'";
                    }
                    scens[scenCounter] = srs.ScenarioType.Name;
                    scenCounter += 1;
                }
                scenInStatement += ")";

                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("	bb.type ");
                sqlQuery.AppendLine(" 	,ats.name as scenName ");
                sqlQuery.AppendLine(" 	,bb.name  ");
                sqlQuery.AppendLine(" 	,(endRate.Rate - minMax.curRate) * 100 as bpMove ");
                sqlQuery.AppendLine("	,bb.eveDiscountFloor   ");
                sqlQuery.AppendLine(" 	FROM  ");
                sqlQuery.AppendLine(" (SELECT  ");
                sqlQuery.AppendLine(" 	code  ");
                sqlQuery.AppendLine(" 	,scenario  ");
                sqlQuery.AppendLine(" 	,MIN(minDate) as firstChange  ");
                sqlQuery.AppendLine(" 	,MAX(minDate) as lastChange  ");
                sqlQuery.AppendLine(" 	,MAX(curRate) as curRate  ");
                sqlQuery.AppendLine(" 	FROM	  ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine(" 		p.simulationid  ");
                sqlQuery.AppendLine(" 		,scenario  ");
                sqlQuery.AppendLine(" 		,p.code  ");
                sqlQuery.AppendLine(" 		,MIN(p.month) as minDAte  ");
                sqlQuery.AppendLine(" 		,p.rate  ");
                sqlQuery.AppendLine(" 		,MAX(a.rate) as currate  ");
                sqlQuery.AppendLine(" 	FROM Basis_IndexP as p  ");
                sqlQuery.AppendLine(" 	INNER JOIN   ");
                sqlQuery.AppendLine(" 	(SELECT * FROM Basis_IndexA where simulationid = {1} and month = '{0}') as a  ");
                sqlQuery.AppendLine(" 	ON a.code = p.code   ");
                sqlQuery.AppendLine(" 	 WHERE p.simulationid = {1} AND p.rate <> a.rate  ");
                sqlQuery.AppendLine(" 	GROUP BY p.simulationid, p.scenario, p.code, p.rate) as rt  ");
                sqlQuery.AppendLine(" 	GROUP BY rt.code, rt.scenario) as minMax  ");
                sqlQuery.AppendLine(" 	LEFT JOIN  ");
                sqlQuery.AppendLine("   ");
                sqlQuery.AppendLine(" 	(SELECT code, scenario, rate FROM Basis_IndexP WHERE DATEADD(m, nmonths, month) = DATEADD(m, 361, '{0}') AND simulationId = {1}) as endRate  ");
                sqlQuery.AppendLine(" 	ON minMax.code = endRate.code AND minMax.scenario = endRate.scenario  ");
                sqlQuery.AppendLine(" 	RIGHT JOIN ");
                sqlQuery.AppendLine(" 	(SELECT code, Name, sequence, Special_Type, EveDiscountFloor, type   FROM Basis_IndexB WHERE simulationId = {1}) as bb  ");
                sqlQuery.AppendLine(" 	ON bb.code = minMax.code  ");
                sqlQuery.AppendLine(" 	LEFT JOIN ");
                sqlQuery.AppendLine(" 	(SELECT number, name, scenarioTypeID FROM Basis_Scenario WHERE simulationId = {1} ) as als  ");
                sqlQuery.AppendLine(" 	ON als.number = minMax.scenario   ");
                sqlQuery.AppendLine("	LEFT JOIN Atlas_ScenarioType as ats ");
                sqlQuery.AppendLine("	ON ats.id = als.scenarioTypeId ");
                sqlQuery.AppendLine("	WHERE bb.Special_Type = 0 AND (ScenarioTypeId IN {2} OR (endRate.rate is null AND bb.type = 1)) ");
                sqlQuery.AppendLine("   ");
                sqlQuery.AppendLine(" 	ORDER BY bb.type, bb.sequence, als.name  ");




                rates = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), String.Format(sqlQuery.ToString(), firstOfMonth, simId.ToString(), scenInStatement));
            }

            
       
            return new
            {
                tableData = rates,
                scenario = scens,
                errors = errors,
                warnings = warnings
            };
        }
    }
}