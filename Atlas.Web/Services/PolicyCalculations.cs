﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atlas.Institution.Data;
using System.Data;
using System.Text;
using Atlas.Institution.Model;
using System.Data.Entity;
using System.Collections;

namespace Atlas.Web.Services
{
    public static class PolicyCalculations
    {
        public static Dictionary<string, double> EvePoliciesValues(SimulationType simType, DateTime asOfDate, string instName, bool ncua, int definedGrouping)
        {
            string conStr = Utilities.BuildInstConnectionString(instName);

            StringBuilder sqlQuery = new StringBuilder();

            var instService = new InstitutionService(instName);
            var sim = instService.GetSimulation(asOfDate, 0, simType);

            if (sim == null)
            {
                return null;
            }

            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	alst.name  ");

              if (ncua)
              {
                  sqlQuery.AppendLine(" ,SUM(CASE WHEN alb.isAsset = 1 THEN als.MVAmount ELSE 0 END) - SUM( ");
                  sqlQuery.AppendLine(" 	CASE  ");
                  sqlQuery.AppendLine(" 		WHEN alb.isAsset = 0 THEN  ");
                  sqlQuery.AppendLine(" 			CASE WHEN alb.Acc_TypeOR = 3 THEN  ");
                  sqlQuery.AppendLine(" 				CASE WHEN nc.percentage is null THEN  ");
                  sqlQuery.AppendLine(" 					ala.End_Bal  ");
                  sqlQuery.AppendLine(" 				ELSE   ");
                  sqlQuery.AppendLine(" 					ala.End_Bal * nc.percentage    ");
                  sqlQuery.AppendLine(" 				END   ");
                  sqlQuery.AppendLine(" 			ELSE ");
                  sqlQuery.AppendLine(" 				als.MVAmount  ");
                  sqlQuery.AppendLine(" 			END  ");
                  sqlQuery.AppendLine(" 		ELSE  ");
                  sqlQuery.AppendLine(" 			0 ");
                  sqlQuery.AppendLine(" 		END) AS econValue   ");
                  sqlQuery.AppendLine("    ,SUM(CASE WHEN alb.isAsset = 1 THEN als.MVAmount ELSE 0 END) as assetValue   ");
              }
              else
              {
                sqlQuery.AppendLine(" 	,SUM(CASE WHEN alb.isAsset = 1 THEN als.MVAmount ELSE 0 END) - SUM(CASE WHEN alb.isAsset = 0 THEN als.MVAmount ELSE 0 END) AS econValue  ");
                sqlQuery.AppendLine("   ,SUM(CASE WHEN alb.isAsset = 1 THEN als.MVAmount ELSE 0 END) as assetValue  ");
              }



            sqlQuery.AppendLine(" 	FROM  ");
            sqlQuery.AppendLine(" (SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + sim.Id + ") as als   ");

            if (ncua)
            {
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT code, end_Bal  FROM BASIS_ALA WHERE simulationId = " + sim.Id + " and month = '" + Utilities.GetFirstOfMonthStr(asOfDate.ToShortDateString()) + "' ) as ala ");
                sqlQuery.AppendLine(" ON ala.code = als.code ");
            }

            sqlQuery.AppendLine(" INNER JOIN BASIS_ALB as alb  ");
            sqlQuery.AppendLine(" ON alb.code = als.code AND als.simulationid = alb.simulationid  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_Scenario AS bs  ");
            sqlQuery.AppendLine(" ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType  as alst  ");
            sqlQuery.AppendLine(" ON alst.id = bs.scenarioTypeid  ");


            if (definedGrouping != 0)
            {
                sqlQuery.AppendLine(" INNER JOIN ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine(" (SELECT   ");
                sqlQuery.AppendLine(" 	dgg.name  ");
                sqlQuery.AppendLine(" 	,dgg.[Priority]  ");
                sqlQuery.AppendLine(" 	,dgc.IsAsset   ");
                sqlQuery.AppendLine(" 	,dgc.Acc_Type   ");
                sqlQuery.AppendLine(" 	,dgc.RbcTier   ");
                sqlQuery.AppendLine("   FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg  ");
                sqlQuery.AppendLine("   INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg  ");
                sqlQuery.AppendLine("   ON dg.id = dgg.DefinedGroupingId   ");
                sqlQuery.AppendLine("   INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc  ");
                sqlQuery.AppendLine("   ON dgg.id = dgc.DefinedGroupingGroupId   ");
                sqlQuery.AppendLine("   WHERE dg.id = " + definedGrouping.ToString() + ") as dgs ON  ");
                sqlQuery.AppendLine("   dgs.acc_Type = alb.acc_Type AND dgs.rbcTier = alb.rbcTier AND dgs.IsAsset = alb.isAsset  ");
            }

            if (ncua)
            {
                sqlQuery.AppendLine("LEFT JOIN ATLAS_NCUAHelper as nc ");
                sqlQuery.AppendLine(" ON alst.Id = nc.scenarioTypeId and alb.Acc_TypeOR = nc.AccountTypeId ");
            }

            sqlQuery.AppendLine(" WHERE alst.name <> 'Base' AND alb.Cat_Type in (0,1,5) AND alb.Acc_TypeOR <> '4'  AND als.exclude =0 and alb.Exclude = 0   ");
            sqlQuery.AppendLine(" GROUP BY alst.name  ");

            DataTable scens = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            Dictionary<string, double[]> eves = new Dictionary<string, double[]>();

            foreach (DataRow dr in scens.Rows)
            {
                double[] dbls = new double[2];
                dbls[0] = double.Parse(dr["econValue"].ToString());
                dbls[1] = double.Parse(dr["assetValue"].ToString());
                eves.Add(dr["name"].ToString(), dbls);
            }


            Dictionary<string, double> vals = new Dictionary<string, double>();

            vals.Add("preShock0 Shock", 0);

            if (eves.ContainsKey("0 Shock"))
            {
                vals["preShock0 Shock"] = eves["0 Shock"][0] / eves["0 Shock"][1];
            }
           

            string[] eveTypes = new string[] { "postShock", "bpChange", "eveChange" };

            double a = 0;
            double b = 0;

            foreach (KeyValuePair<string, double[]> kvp in eves)
            {
                foreach (string s in eveTypes)
                {
                    vals.Add(s + kvp.Key, 0);

                    switch (s)
                    {
                        case "postShock":
                            if (kvp.Value[1] == 0)
                            {
                                vals[s + kvp.Key] = 0;
                            }
                            else
                            {
                                vals[s + kvp.Key] = kvp.Value[0] / kvp.Value[1];
                            }
                            break;
                        case "bpChange":
                            if (eves.ContainsKey("0 Shock"))
                            {
                                if (kvp.Value[1] == 0)
                                {
                                    a = 0;
                                }
                                else
                                {
                                    a = (kvp.Value[0] / kvp.Value[1]);
                                }
                                if(eves["0 Shock"][1] == 0){
                                    b = 0;
                                }
                                else{
                                    b = eves["0 Shock"][0] / eves["0 Shock"][1];
                                }
                                //vals[s + kvp.Key] = (kvp.Value[0] / kvp.Value[1]) - (eves["0 Shock"][0] / eves["0 Shock"][1]);
                                vals[s + kvp.Key] = a - b;
                            }
                            else
                            {
                                vals[s + kvp.Key] = 0;
                            }
                          
                            break;
                        case "eveChange":
                            if (eves.ContainsKey("0 Shock"))
                            {
                                if(Math.Abs(eves["0 Shock"][0]) == 0){
                                    vals[s + kvp.Key] = 0;
                                }
                                else 
                                {
                                    vals[s + kvp.Key] = (kvp.Value[0] - eves["0 Shock"][0]) / Math.Abs(eves["0 Shock"][0]);
                                }
                            }
                            else
                            {
                                vals[s + kvp.Key] = 0;
                            }
                           
                            break;
                    }
                }
            }

            return vals;
        }

        public static Dictionary<string, Dictionary<string, double>> NIIPoliciesValues(SimulationType simType, DateTime asOfDate, string instName, bool useExecValues, bool taxIncome)
        {
            string conStr = Utilities.BuildInstConnectionString(instName);

            StringBuilder sqlQuery = new StringBuilder();

            InstitutionContext gctx = new InstitutionContext(conStr);
            var modelSetup = gctx.ModelSetups.Where(s => s.AsOfDate == asOfDate);

            bool taxEquiv = false;
            if (modelSetup.Count() > 0)
            {
                var ms = modelSetup.FirstOrDefault();
                taxEquiv = ms.ReportTaxEquivalent;
            }

            if (useExecValues)
            {
                taxEquiv = taxIncome;
            }

            var instService = new InstitutionService(instName);
            
            var sim = instService.GetSimulation(asOfDate, 0, simType);

            if (sim == null)
            {
                return null;
            }
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("   baseSim.yearGroup as yearGroup  ");
            sqlQuery.AppendLine(" 	,scenNames.name  ");
            sqlQuery.AppendLine(" 	,SUM(CASE WHEN alb.isAsset = 1 THEN baseSim.IeTotAmount ELSE -baseSim.IETotAmount END) as Amount  ");
            sqlQuery.AppendLine(" 	FROM  ");
            sqlQuery.AppendLine("   ");
            sqlQuery.AppendLine(" (SELECT simulationid  ");
            sqlQuery.AppendLine(" 		,month  ");
            sqlQuery.AppendLine(" 		,scenario  ");
            sqlQuery.AppendLine(" 		,code  ");
            if (taxEquiv)
            {
                sqlQuery.AppendLine(" 		,AveTEAmt as IETotAmount ");
            }
            else
            {
                sqlQuery.AppendLine(" 		,IETotAmount ");
            }

            sqlQuery.AppendLine(" 		,CASE  ");
            sqlQuery.AppendLine(" 			WHEN month <= DATEADD(m, 12, '" + asOfDate.ToShortDateString() + "') THEN  ");
            sqlQuery.AppendLine(" 				'Year1'  ");
            sqlQuery.AppendLine(" 			WHEN month <= DATEADD(m, 24, '" + asOfDate.ToShortDateString() + "') THEN  ");
            sqlQuery.AppendLine(" 				'Year2'  ");
            sqlQuery.AppendLine(" 			WHEN month <= DATEADD(m, 36, '" + asOfDate.ToShortDateString() + "') THEN  ");
            sqlQuery.AppendLine(" 				'Year 3'  ");
            sqlQuery.AppendLine(" 			WHEN month <= DATEADD(m, 48, '" + asOfDate.ToShortDateString() + "') THEN  ");
            sqlQuery.AppendLine(" 				'Year 4'  ");
            sqlQuery.AppendLine(" 			ELSE  ");
            sqlQuery.AppendLine(" 				'Year 5'  ");
            sqlQuery.AppendLine(" 		END as yearGroup              ");
            sqlQuery.AppendLine("  FROM Basis_ALP13 WHERE simulationId = " + sim.Id.ToString() + " ");
            sqlQuery.AppendLine("     AND month <= DATEADD(m, 24, '" + asOfDate.ToShortDateString() + "') ");
            sqlQuery.AppendLine(" ) as baseSim  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_ALB as alb ON   ");
            sqlQuery.AppendLine(" alb.simulationId = baseSim.simulationId and alb.code = baseSim.code  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_ALS as als ON  ");
            sqlQuery.AppendLine(" als.simulationId = baseSim.simulationId and als.code = baseSim.code and als.scenario = baseSim.scenario  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_Scenario  as scen ON  ");
            sqlQuery.AppendLine(" scen.number = als.scenario AND scen.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as scenNames ON  ");
            sqlQuery.AppendLine(" scennames.Id = scen.ScenarioTypeId   ");
            sqlQuery.AppendLine(" WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5)  ");
            sqlQuery.AppendLine(" GROUP BY baseSim.yearGroup,  baseSim.yearGroup, scenNames.name  ");
            sqlQuery.AppendLine(" ORDER BY scenNames.name ");
            DataTable scens = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            //Build Dictionary For Easier Access
            Dictionary<string, Dictionary<string, double>> grps = new Dictionary<string, Dictionary<string, double>>();
            foreach (DataRow dr in scens.Rows)
            {

                if (!grps.ContainsKey(dr["name"].ToString()))
                {
                    grps.Add(dr["name"].ToString(), new Dictionary<string, double>());
                }

                if (!grps[dr["name"].ToString()].ContainsKey(dr["yearGroup"].ToString()))
                {
                    grps[dr["name"].ToString()].Add(dr["yearGroup"].ToString(), 0);
                }


                grps[dr["name"].ToString()][dr["yearGroup"].ToString()] = Double.Parse(dr["amount"].ToString());
            }

            Dictionary<string, Dictionary<string, double>> niis = new Dictionary<string, Dictionary<string, double>>();
            niis.Add("year1Year1", new Dictionary<string, double>());
            niis["year1Year1"].Add("Min", 0);
            niis.Add("year2Year1", new Dictionary<string, double>());
            niis["year2Year1"].Add("Min", 0);
            niis.Add("year2Year2", new Dictionary<string, double>());
            niis["year2Year2"].Add("Min", 0);
            niis.Add("24Month", new Dictionary<string, double>());
            niis["24Month"].Add("Min", 0);

            foreach (KeyValuePair<string, Dictionary<string, double>> kvp in grps)
            {
                //Calcualte Year 1 Change From Year 1 Base
                if (kvp.Value.ContainsKey("Year1") && kvp.Key != "Base")
                {
                    niis["year1Year1"].Add(kvp.Key, 0);
                    niis["year1Year1"][kvp.Key] = (grps[kvp.Key]["Year1"] - grps["Base"]["Year1"]) / Math.Abs(grps["Base"]["Year1"]);
                    if (niis["year1Year1"][kvp.Key] < niis["year1Year1"]["Min"] || niis["year1Year1"]["Min"] == 0)
                    {
                        niis["year1Year1"]["Min"] = niis["year1Year1"][kvp.Key];
                    }
                }
                else if (kvp.Value.ContainsKey("Year1"))
                {
                    niis["year1Year1"]["Base"] = grps["Base"]["Year1"] / 1000;
                }

                //Calculate Year 2 Changes From Year 1 Base
                if (kvp.Value.ContainsKey("Year2"))
                {
                    //Year 2 Year 1
                    niis["year2Year1"].Add(kvp.Key, 0);
                    niis["year2Year1"][kvp.Key] = (grps[kvp.Key]["Year2"] - grps["Base"]["Year1"]) / Math.Abs(grps["Base"]["Year1"]);
                    if (niis["year2Year1"][kvp.Key] < niis["year2Year1"]["Min"] || niis["year2Year1"]["Min"] == 0)
                    {
                        niis["year2Year1"]["Min"] = niis["year2Year1"][kvp.Key];
                    }

                    if (kvp.Key != "Base")
                    {
                        //Year 2 Year 2
                        niis["year2Year2"].Add(kvp.Key, 0);
                        niis["year2Year2"][kvp.Key] = (grps[kvp.Key]["Year2"] - grps["Base"]["Year2"]) / Math.Abs(grps["Base"]["Year2"]);
                        if (niis["year2Year2"][kvp.Key] < niis["year2Year2"]["Min"] || niis["year2Year2"]["Min"] == 0)
                        {
                            niis["year2Year2"]["Min"] = niis["year2Year2"][kvp.Key];
                        }


                        //Month 24
                        niis["24Month"].Add(kvp.Key, 0);
                        niis["24Month"][kvp.Key] = ((grps[kvp.Key]["Year2"] + grps[kvp.Key]["Year1"]) - (grps["Base"]["Year1"] + grps["Base"]["Year2"])) / Math.Abs(grps["Base"]["Year1"] + grps["Base"]["Year2"]);
                        if (niis["24Month"][kvp.Key] < niis["24Month"]["Min"] || niis["24Month"]["Min"] == 0)
                        {
                            niis["24Month"]["Min"] = niis["24Month"][kvp.Key];
                        }
                    }
                    else
                    {
                        niis["year2Year2"]["Base"] = grps["Base"]["Year2"] / 1000;
                        niis["24Month"]["Base"] = (grps["Base"]["Year1"] + grps["Base"]["Year2"]) / 1000;
                    }
                }
            }

            niis.Add("changeInNII", new Dictionary<string, double>());
            niis["changeInNII"].Add("Min", 0);
            double down = -1000;
            double up = -1000;

            if (niis.ContainsKey("year1Year1"))
            {
                if (niis["year1Year1"].ContainsKey("Down 200BP"))
                {
                    down = niis["year1Year1"]["Down 200BP"];
                }
                else if (niis["year1Year1"].ContainsKey("Down 100BP"))
                {
                    down = niis["year1Year1"]["Down 100BP"];
                }
                if (niis["year1Year1"].ContainsKey("Up 200BP"))
                {
                    up = niis["year1Year1"]["Up 200BP"];
                }
                else if (niis["year1Year1"].ContainsKey("Up 100BP"))
                {
                    up = niis["year1Year1"]["Up 100BP"];
                }

                //Find Min
                if (up != -1000 && down != -1000)
                {
                    if (up < down)
                    {
                        niis["changeInNII"]["Min"] = up;
                    }
                    else
                    {
                        niis["changeInNII"]["Min"] = down;
                    }

                }
                else if (up != -1000)
                {
                    niis["changeInNII"]["Min"] = up;
                }

                else if (down != -1000)
                {
                    niis["changeInNII"]["Min"] = down;
                }

            }


            return niis;

        }



        public static Dictionary<string, double> CoreFundPoliciesValues(SimulationType simType, DateTime asOfDate, string instName)
        {
            string conStr = Utilities.BuildInstConnectionString(instName);

            StringBuilder sqlQuery = new StringBuilder();

            var instService = new InstitutionService(instName);
            var sim = instService.GetSimulation(asOfDate, 0, simType);


            if (sim == null)
            {
                return null;
            }

            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" g.scenarioName  ");
            sqlQuery.AppendLine(" ,SUM(CASE WHEN g.isAsset = 1 THEN balance ELSE 0 END ) / NULLIF(SUM(CASE WHEN g.isAsset = 0 THEN balance ELSE 0 END) ,0) as coreFund ");
            sqlQuery.AppendLine(" FROM   ");
            sqlQuery.AppendLine("  (SELECT    ");
            sqlQuery.AppendLine("  aod.asOfDate  ");
            sqlQuery.AppendLine("  ,als.name as scenarioName  ");
            sqlQuery.AppendLine("  ,sum(ALP6.sGapBal / ALP6.NMonths) as balance   ");
            sqlQuery.AppendLine("  ,alb.Acc_TypeOR as acc_type ");
            sqlQuery.AppendLine("  ,alb.IsAsset as isAsset ");
            sqlQuery.AppendLine("  ,alb.ClassOR as rbc ");
            sqlQuery.AppendLine(" FROM   ");
            sqlQuery.AppendLine(" (SELECT   ");
            sqlQuery.AppendLine(" month   ");
            sqlQuery.AppendLine(" ,simulationId   ");
            sqlQuery.AppendLine(" ,scenario   ");
            sqlQuery.AppendLine(" ,code   ");
            sqlQuery.AppendLine(" ,nMonths   ");
            sqlQuery.AppendLine(" ,sGapBal   ");
            sqlQuery.AppendLine(" ,sGapRate   ");
            sqlQuery.AppendLine(" FROM Basis_ALP6  ");
            sqlQuery.AppendLine(" WHERE simulationId = " + sim.Id.ToString() + " ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" ) as ALP6   ");
            sqlQuery.AppendLine("  INNER JOIN Basis_ALB as ALB ON   ");
            sqlQuery.AppendLine("  ALB.simulationId = ALP6.simulationID AND   ");
            sqlQuery.AppendLine("  ALB.code = ALP6.code    ");
            sqlQuery.AppendLine("  INNER JOIN ATLAS_PeriodSmoother AS ps   ");
            sqlQuery.AppendLine("  ON ps.period <= ALp6.nMonths   ");
            sqlQuery.AppendLine("  INNER JOIN Simulations AS sim ON   ");
            sqlQuery.AppendLine("  sim.id = ALP6.SimulationId    ");
            sqlQuery.AppendLine("  INNER JOIN AsOfDateInfo as aod ON   ");
            sqlQuery.AppendLine("  sim.InformationId = aod.id and ALP6.Month >= DATEADD(m, 60, aod.asOfDate)  ");
            sqlQuery.AppendLine("  INNER JOIN Basis_Scenario as bs ON  ");
            sqlQuery.AppendLine("  bs.Number = alp6.Scenario and bs.SimulationId = alp6.SimulationId   ");
            sqlQuery.AppendLine("  INNER JOIN ATLAS_ScenarioType AS als ");
            sqlQuery.AppendLine("  ON als.id = bs.ScenarioTypeId  ");
            sqlQuery.AppendLine("  group by aod.asOfDate, als.name, ALB.isAsset, ALB.Acc_TypeOR, alb.ClassOR ");
            sqlQuery.AppendLine("  ) as G ");
            sqlQuery.AppendLine("  LEFT JOIN ATLAS_AccountType as accTypes ON ");
            sqlQuery.AppendLine("  G.acc_type = accTypes.Id and G.isAsset = 1 ");
            sqlQuery.AppendLine("  LEFT JOIN ATLAS_CoreFundingLookup as cflookups ON   ");
            sqlQuery.AppendLine("  G.IsAsset = cflookups.isAsset and G.Acc_Type = cflookups.Acc_Type and G.Rbc = cflookups.RBC_Tier   ");
            sqlQuery.AppendLine("  where cflookups.name is not null or (cflookups.name is null and G.IsAsset = 1)  ");
            sqlQuery.AppendLine("  GROUP BY g.scenarioName ");

            DataTable scens = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            Dictionary<string, double> cfs = new Dictionary<string, double>();
            cfs.Add("MaxCoreFund", 0);
            double max = 0;
            double coreFundNum = 0;
            foreach (DataRow dr in scens.Rows)
            {
                if (dr["coreFund"].ToString().Length > 0)
                {
                    coreFundNum = double.Parse(dr["coreFund"].ToString());
                }
                cfs.Add(dr["scenarioName"].ToString(), coreFundNum);
                
                
                if (coreFundNum > max)
                {
                    cfs["MaxCoreFund"] = coreFundNum;
                }
            }
            return cfs;
        }

        public static Dictionary<string, double> LiquidyAndOtherRatioValues(int eveId, int simTypeId, DateTime asOfDate, int bsId, string instName, bool useExecValues, bool execTaxYield)
        {
            string conStr = Utilities.BuildInstConnectionString(instName);

            StringBuilder sqlQuery = new StringBuilder();
            InstitutionContext gctx = new InstitutionContext(conStr);
            var eveConfig = gctx.Policies.Where(s => s.Id == eveId).Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings)).Include(s => s.OtherPolicies).FirstOrDefault();

            DataTable aods = Utilities.ExecuteSql(conStr, "SELECT id FROM AsOfDateInfo WHERE asOfDate = '" + asOfDate.ToShortDateString() + "'");
            int infoId = (int)aods.Rows[0][0];

            var modelSetup = gctx.ModelSetups.Where(s => s.AsOfDate == asOfDate);

            bool taxEquivYield = false;
            bool offBalanceSheet = false;
            if (modelSetup.Count() > 0)
            {
                var ms = modelSetup.FirstOrDefault();
                taxEquivYield = ms.ReportTaxEquivalentYield;
                offBalanceSheet = ms.IncludeOffBalanceSheet;
            }


            if (useExecValues)
            {
                taxEquivYield = execTaxYield;
            }

            //Pulls the one value we need from Basic Surplus

            //This Does The pull to get acutals by classification 
            sqlQuery.Clear();
            sqlQuery.AppendLine("	SELECT ");
            sqlQuery.AppendLine("		sims.asOfDate ");
            sqlQuery.AppendLine("		,alb.isAsset ");
            sqlQuery.AppendLine("		,alb.Acc_TypeOR ");
            sqlQuery.AppendLine("		,alb.classOr ");
            sqlQuery.AppendLine("		,SUM(ala.End_Bal) as EndBal ");

            if (taxEquivYield)
            {
                sqlQuery.AppendLine("		,ISNULL(SUM(ala.End_Bal * ala.Total_End_TEYLD) / NULLIF(SUM(ala.End_Bal), 0),0) as EndRate ");
            }
            else
            {
                sqlQuery.AppendLine("		,ISNULL(SUM(ala.End_Bal * ala.End_Rate) / NULLIF(SUM(ala.End_Bal), 0),0) as EndRate ");
            }
  
            sqlQuery.AppendLine("	FROM  ");
            sqlQuery.AppendLine("	Basis_ALA AS ala ");
            sqlQuery.AppendLine("	INNER JOIN ");
            sqlQuery.AppendLine("		(SELECT s.id, aod.asOfDate FROM simulations as s ");
            sqlQuery.AppendLine("		INNER JOIN AsOfDatEInfo as aod ");
            sqlQuery.AppendLine("		ON aod.id = s.informationid	 ");
            sqlQuery.AppendLine("		WHERE informationId = " + infoId.ToString() + " AND simulationTypeid = " + simTypeId + ") as sims ");
            sqlQuery.AppendLine("		ON ala.SimulationId = sims.id  ");
            sqlQuery.AppendLine("		AND ala.month = CAST(CAST(DATEPART(year, sims.asOfDate) AS VARCHAR(10)) + '-' + CAST(DATEPART(month, sims.asOfDate) AS VARCHAR(10)) + '-' + '1' AS DATETIME) ");
            sqlQuery.AppendLine("		INNER JOIN Basis_ALB AS alb ON ");
            sqlQuery.AppendLine("		alb.code = ala.code ");
            sqlQuery.AppendLine("		AND alb.SimulationId = ala.SimulationId  ");
            sqlQuery.AppendLine("		WHERE alb.cat_Type in (0,1,5)  AND alb.exclude = 0 ");
            sqlQuery.AppendLine("		GROUP BY  ");
            sqlQuery.AppendLine("			sims.asOfDate ");
            sqlQuery.AppendLine("			,alb.isAsset ");
            sqlQuery.AppendLine("			,alb.Acc_TypeOR  ");
            sqlQuery.AppendLine("			,alb.classOr ");
            DataTable liqTable = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            //Settings
            bool borrowAssetsIncRetail = false;
            bool borrowDepositsIncRetail = false;
            bool borrowMaxIncRetail = false;

            //Borkered Settings
            bool brokeredAssetsIncRecip = false;
            bool brokeredDepositsIncRecip = false;
            bool brokeredMaxIncRecip = false;
            bool brokeredAssetsIncNat = false;
            bool brokeredDepositsIncNat = false;
            bool brokeredMaxIncNat = false;

            //Wholesale Settings
            bool wholeIncNat = false;
            bool wholeIncRecip = false;
            bool wholeIncRetail = false;


            //Get Settings to Exlude or include certain types of classifications
            foreach (LiquidityPolicy lp in eveConfig.LiquidityPolicies)
            {
                switch (lp.ShortName)
                {
                    case "borrowAsset":
                        borrowAssetsIncRetail = lp.LiquidityPolicySettings.First().Include;
                        break;
                    case "borrowDeposits":
                        borrowDepositsIncRetail = lp.LiquidityPolicySettings.First().Include;
                        break;
                    case "borrowMax":
                        borrowMaxIncRetail = lp.LiquidityPolicySettings.First().Include;
                        break;
                    case "brokeredAsset":
                        foreach (LiquidityPolicySetting lps in lp.LiquidityPolicySettings)
                        {
                            if (lps.ShortName == "incBrokeredRecip")
                            {
                                brokeredAssetsIncRecip = lps.Include;
                            }
                            else
                            {
                                brokeredAssetsIncNat = lps.Include;
                            }
                        }
                        break;
                    case "brokeredDeposits":
                        foreach (LiquidityPolicySetting lps in lp.LiquidityPolicySettings)
                        {
                            if (lps.ShortName == "incBrokeredRecip")
                            {
                                brokeredDepositsIncRecip = lps.Include;
                            }
                            else
                            {
                                brokeredDepositsIncNat = lps.Include;
                            }
                        }
                        break;
                    case "brokeredMax":
                        foreach (LiquidityPolicySetting lps in lp.LiquidityPolicySettings)
                        {
                            if (lps.ShortName == "incBrokeredRecip")
                            {
                                brokeredMaxIncRecip = lps.Include;
                            }
                            else
                            {
                                brokeredMaxIncNat = lps.Include;
                            }
                        }
                        break;
                    case "wholeAsset":
                        foreach (LiquidityPolicySetting lps in lp.LiquidityPolicySettings)
                        {
                            if (lps.ShortName == "incBrokeredRecip")
                            {
                                wholeIncRecip = lps.Include;
                            }
                            else if (lps.ShortName == "incRetailRepos")
                            {
                                wholeIncRetail = lps.Include;
                            }
                            else
                            {
                                wholeIncNat = lps.Include;
                            }
                        }
                        break;
                }
            }

            //Total Values
            double fhlbTotal = 0;
            double totAssets = 0;
            double totAssetsRate = 0;
            double totLiab = 0;
            double totLiabRate = 0;
            double borrowAssets = 0;
            double borrowDeposits = 0;
            double borrowMax = 0;
            double brokeredAssets = 0;
            double brokeredDeposits = 0;
            double brokeredMax = 0;
            double wholeAssets = 0;


            double bsminLiqAssets = 0;

            double fhlbAssetsMax = 0;
            double fhlbDepositsMax = 0;
            double fhlbMax = 0;

            double deposits = 0;

            double netLoans = 0;
            double loanLoss = 0;
            double loans = 0;
            double totInvests = 0;
            double totBorrow = 0;
            double totDeposits = 0;

            //Build Totals And Calculate Ratios

            foreach (DataRow dr in liqTable.Rows)
            {
                if ((bool)dr["isAsset"])
                {
                    totAssets += (double)dr["EndBal"];
                    totAssetsRate += (double)dr["EndBal"] * (double)dr["EndRate"];

                    //Total Loans
                    if (dr["Acc_TypeOr"].ToString() == "2")
                    {
                        netLoans += (double)dr["EndBal"];

                        if (dr["classOr"].ToString() != "12" && dr["classOr"].ToString() != "11")
                        {
                            loans += (double)dr["EndBal"];
                        }

                        //Total Loan Loss
                        if (dr["classOr"].ToString() == "12")
                        {
                            loanLoss += (double)dr["EndBal"];

                        }

                    }



                }
                else
                {
                    totLiab += (double)dr["EndBal"];
                    totLiabRate += (double)dr["EndBal"] * (double)dr["EndRate"];
                }

                //Total Investments
                if ((int)dr["Acc_TypeOr"] == 1)
                {
                    totInvests += (double)dr["EndBal"];
                }


                //Total Investments
                if ((int)dr["Acc_TypeOr"] == 3 || (int)dr["Acc_TypeOr"] == 8)
                {
                    totDeposits += (double)dr["EndBal"];
                }

                //Total Borrowings
                if ((int)dr["Acc_TypeOr"] == 6)
                {
                    totBorrow += (double)dr["EndBal"];
                    //Handle Include settings
                    if ((int)dr["classOr"] == 2 || (int)dr["classOr"] == 3)
                    {
                        if (borrowAssetsIncRetail)
                        {
                            borrowAssets += (double)dr["EndBal"];
                        }
                        if (borrowDepositsIncRetail)
                        {
                            borrowDeposits += (double)dr["EndBal"];
                        }
                        if (borrowMaxIncRetail)
                        {
                            borrowMax += (double)dr["EndBal"];
                        }
                    }

                    else
                    {
                        borrowAssets += (double)dr["EndBal"];
                        borrowDeposits += (double)dr["EndBal"];
                        borrowMax += (double)dr["EndBal"];
                    }


                    //Capture FHLB in here
                    if ((int)dr["classOr"] == 1)
                    {
                        fhlbTotal += (double)dr["EndBal"];
                    }

                }

                //Only Total Time Deps and NMDs
                if ((int)dr["Acc_TypeOr"] == 8 || (int)dr["Acc_TypeOr"] == 3)
                {
                    deposits += (double)dr["EndBal"];
                }

                //Total Brokreed
                switch ((int)dr["classOr"])
                {
                    case 13: //NMD - Brokered Now
                    case 16: //NMD - Brokered One Way
                    case 14: //NMD - Brokered MMDA
                    case 15: //NMD - Brokered Reciprocal MMD
                        if ((int)dr["Acc_TypeOr"] == 3)
                        {
                            if ((int)dr["classOr"] == 15)
                            {
                                if (brokeredAssetsIncRecip)
                                {
                                    brokeredAssets += (double)dr["EndBal"];
                                }
                                if (brokeredDepositsIncRecip)
                                {
                                    brokeredDeposits += (double)dr["EndBal"];
                                }
                                if (brokeredMaxIncRecip)
                                {
                                    brokeredMax += (double)dr["EndBal"];
                                }
                            }
                            else
                            {
                                brokeredMax += (double)dr["EndBal"];
                                brokeredDeposits += (double)dr["EndBal"];
                                brokeredAssets += (double)dr["EndBal"];
                            }

                        }
                        break;
                    case 9: //TD - Brokered Reciprocal CD
                    case 10: //TD - Brokered One Way CD
                    case 5:  //TD - Brokered CD
                    case 6:  //TD - Natial CD
                        if ((int)dr["Acc_TypeOr"] == 8)
                        {
                            if ((int)dr["classOr"] == 9 || (int)dr["classOr"] == 8)
                            {
                                if (brokeredAssetsIncRecip)
                                {
                                    brokeredAssets += (double)dr["EndBal"];
                                }
                                if (brokeredDepositsIncRecip)
                                {
                                    brokeredDeposits += (double)dr["EndBal"];
                                }
                                if (brokeredMaxIncRecip)
                                {
                                    brokeredMax += (double)dr["EndBal"];
                                }
                            }
                            else if ((int)dr["classOr"] == 6)
                            {
                                if (brokeredAssetsIncNat)
                                {
                                    brokeredAssets += (double)dr["EndBal"];
                                }
                                if (brokeredDepositsIncNat)
                                {
                                    brokeredDeposits += (double)dr["EndBal"];
                                }
                                if (brokeredMaxIncNat)
                                {
                                    brokeredMax += (double)dr["EndBal"];
                                }
                            }
                            else
                            {
                                brokeredMax += (double)dr["EndBal"];
                                brokeredDeposits += (double)dr["EndBal"];
                                brokeredAssets += (double)dr["EndBal"];
                            }
                        }

                        break;

                }


                //Total Whole Sale
    
                if ((((int)dr["ClassOr"] == 13 || (int)dr["ClassOr"] == 16 || (int)dr["ClassOr"] == 14) && (int)dr["Acc_TypeOr"] == 3) || (((int)dr["ClassOr"] == 10 || (int)dr["ClassOr"] == 5 ) && (int)dr["Acc_TypeOr"] == 8))
                {
                    wholeAssets += (double)dr["EndBal"];
                }

                if (((int)dr["classOr"] == 15 && (int)dr["Acc_TypeOr"] == 3) || ((int)dr["classOr"] == 9 && (int)dr["Acc_TypeOr"] == 8))
                {
                    if (wholeIncRecip)
                    {
                        wholeAssets += (double)dr["EndBal"];
                    }
                }
                else if ((int)dr["classOr"] == 6 && (int)dr["Acc_TypeOr"] == 8)
                {
                    if (wholeIncNat)
                    {
                        wholeAssets += (double)dr["EndBal"];
                    }
                }

                if (((int)dr["classOr"] == 2 || (int)dr["classOr"] == 3) && (int)dr["Acc_TypeOr"] == 6)
                {
                    if (wholeIncRetail)
                    {
                        wholeAssets += (double)dr["EndBal"];
                    }
                }
                else if ((int)dr["Acc_TypeOr"] == 6)
                {
                    wholeAssets += (double)dr["EndBal"];     
                }


            }
        

            totLiabRate = (totLiabRate / totLiab);

            if (offBalanceSheet)
            {

                sqlQuery.Clear();
                sqlQuery.AppendLine("	SELECT ");
                if (taxEquivYield)
                {
                    sqlQuery.AppendLine("		ISNULL(SUM(ala.End_Bal * ala.Total_End_TEYLD) / NULLIF(SUM(ala.End_Bal), 0),0) as EndRate ");
                }
                else
                {
                    sqlQuery.AppendLine("		ISNULL(SUM(ala.End_Bal * ala.End_Rate) / NULLIF(SUM(ala.End_Bal), 0),0) as EndRate ");
                }

                sqlQuery.AppendLine("	FROM  ");
                sqlQuery.AppendLine("	Basis_ALA AS ala ");
                sqlQuery.AppendLine("	INNER JOIN ");
                sqlQuery.AppendLine("		(SELECT s.id, aod.asOfDate FROM simulations as s ");
                sqlQuery.AppendLine("		INNER JOIN AsOfDatEInfo as aod ");
                sqlQuery.AppendLine("		ON aod.id = s.informationid	 ");
                sqlQuery.AppendLine("		WHERE informationId = " + infoId.ToString() + " AND simulationTypeid = " + simTypeId + ") as sims ");
                sqlQuery.AppendLine("		ON ala.SimulationId = sims.id  ");
                sqlQuery.AppendLine("		AND ala.month = CAST(CAST(DATEPART(year, sims.asOfDate) AS VARCHAR(10)) + '-' + CAST(DATEPART(month, sims.asOfDate) AS VARCHAR(10)) + '-' + '1' AS DATETIME) ");
                sqlQuery.AppendLine("		INNER JOIN Basis_ALB AS alb ON ");
                sqlQuery.AppendLine("		alb.code = ala.code ");
                sqlQuery.AppendLine("		AND alb.SimulationId = ala.SimulationId  ");
                sqlQuery.AppendLine("		WHERE alb.cat_Type in (0,1,5)  AND alb.exclude = 0 and alb.IsAsset = 0 ");

                DataTable offBalanceData = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

                if (offBalanceData.Rows.Count > 0)
                {
                    totLiabRate = double.Parse(offBalanceData.Rows[0][0].ToString());
                }
            }

            double balanceSheetSpread = ((totAssetsRate / totAssets) - (totLiabRate)) / 100;

            //Create dictiony to return so we can set policies as well as pull this numbers into reports if needed
            Dictionary<string, double> vals = new Dictionary<string, double>();
            vals.Add("Borrowings / Assets (Max.)", (borrowAssets / totAssets));
            vals.Add("Borrowings / Deposits", (borrowDeposits / deposits));
            vals.Add("Maximum Borrowing Capacity", (borrowMax));
            vals.Add("Brokered Deposits / Assets (Max.)", (brokeredAssets / totAssets));
            vals.Add("Brokered Deposits / Deposits (Max.)", (brokeredDeposits / deposits));
            vals.Add("Maximum Brokered Capacity", brokeredMax);
            vals.Add("Total Wholesale Funds / Assets (Max.)", (wholeAssets / totAssets));

            vals.Add("Net Loans / Assets", (netLoans / totAssets));
            vals.Add("Net Loans / Deposits", (netLoans / deposits));
            vals.Add("Loan Loss / Loans", (loanLoss / loans) * -1);

            vals.Add("Total Assets", totAssets);
            vals.Add("Total Investments", totInvests);
            vals.Add("Deposits", deposits);
            vals.Add("Borrowings", totBorrow);
            vals.Add("Balance Sheet Spread", balanceSheetSpread);
            vals.Add("Gross Loans", loans);


            //FHLB Calcs
            vals.Add("FHLB/Assets (Max.)", fhlbTotal / totAssets);
            vals.Add("FHLB/Deposits (Max.)", fhlbTotal / totDeposits);
            vals.Add("Maximum FHLB Capacity", fhlbTotal);

            //Now set the basic surplus ones
            sqlQuery.Clear();
            sqlQuery.AppendLine(" select bsa.id,  MidS1BasicSurplusPercent, MidS1BasicSurplusFHLBPercent,  MidS1BasicSurplusBrokPercent, aod.asOfDate, leftS1LiqAssetTotalAmount from ATLAS_BasicSurplusAdmin as bsa ");
            sqlQuery.AppendLine(" INNER JOIN AsOfDateInfo as aod ");
            sqlQuery.AppendLine(" ON aod.id = bsa.InformationId ");
            sqlQuery.AppendLine(" where bsa.id = " + bsId + " ");
            sqlQuery.AppendLine("  order by aod.asOfDate ");

            DataTable bsTable = Utilities.ExecuteSql(conStr, sqlQuery.ToString());


            vals.Add("Basic Surplus (Min.)", 0);
            vals.Add("Basic Surplus w/ FHLB & Brokered (Min.)", 0);
            vals.Add("Basic Surplus w/ FHLB (Min.)", 0);
            if (bsTable.Rows.Count > 0)
            {
                vals["Basic Surplus (Min.)"] = double.Parse(bsTable.Rows[0]["MidS1BasicSurplusPercent"].ToString()) / 100;
                vals["Basic Surplus w/ FHLB & Brokered (Min.)"] = double.Parse(bsTable.Rows[0]["MidS1BasicSurplusBrokPercent"].ToString()) / 100;
                vals["Basic Surplus w/ FHLB (Min.)"] = double.Parse(bsTable.Rows[0]["MidS1BasicSurplusFHLBPercent"].ToString()) / 100;


                vals["Total Liquid Assets"] = double.Parse(bsTable.Rows[0]["leftS1LiqAssetTotalAmount"].ToString()) / (totAssets / 1000);
            }




            //chcek for divide by zeros
            ArrayList RemoveList = new ArrayList();
            foreach (KeyValuePair<string, double> kvp in vals)
            {
                if (Double.IsNaN(kvp.Value))
                {
                    RemoveList.Add(kvp.Key);
                }
            }

            foreach (string x in RemoveList)
            {
                vals[x] = 0;
            }

            return vals;

        }

    }
}