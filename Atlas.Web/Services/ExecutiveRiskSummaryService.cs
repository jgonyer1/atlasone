﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using System.Data;
using System.Text;

namespace Atlas.Web.Services
{
    //Class that stores the chart values
    public class ExecLiqSection
    {
        public string name { get; set; }
        public bool hideLabel { get; set; }

        public DataTable table {get; set;}
    }

    public class ExecIRRSection
    {
        public string name { get; set; }

        public bool hideLabel { get; set; }

        public ExecIRRSubSection[] subSections { get; set; }
    }

    public class ExecIRRSubSection
    {
        public string name { get; set; }

        public bool hideLabel { get; set; }

        public DataTable table { get; set; }

        public bool showPol { get; set; }
    }

    public class ExecutiveRiskSummaryService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();

        public object ExecutiveRiskSummaryViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.ExecutiveRiskSummaries.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            InstitutionService instService = new InstitutionService(sr.InstitutionDatabaseName);

            //If Not Found Kick Out
            if (sr == null) throw new Exception("Executive Risk Summary Report Not Found");

            //Load as of date offsets and build in statement as well as array
            string asOfDateIn = "";
            DateTime[] asOfDates = new DateTime[sr.ExecutiveRiskSummaryDateOffsets.Count];
            string[] bs = new string[sr.ExecutiveRiskSummaryDateOffsets.Count];
            SimulationType[] eve = new SimulationType[sr.ExecutiveRiskSummaryDateOffsets.Count];
            SimulationType[] nii = new SimulationType[sr.ExecutiveRiskSummaryDateOffsets.Count];
            string[] niiId = new string[sr.ExecutiveRiskSummaryDateOffsets.Count];

            bool[] taxIncome = new bool[sr.ExecutiveRiskSummaryDateOffsets.Count];
            bool[] taxYield = new bool[sr.ExecutiveRiskSummaryDateOffsets.Count];

            int aodCounter = 0;

            ExecutiveRiskSummaryDateOffsets curOff = null;

            //Loop through offsets and add columns for past values
            Information curInfo = null;
            foreach (ExecutiveRiskSummaryDateOffsets offSet in sr.ExecutiveRiskSummaryDateOffsets.OrderBy(s => s.OffSet))
            {
                Information info = instService.GetInformation(rep.Package.AsOfDate, offSet.OffSet);

                if (aodCounter == 0)
                {
                    curOff = offSet;
                    curInfo = instService.GetInformation(rep.Package.AsOfDate, offSet.OffSet);
                }

                if (asOfDateIn == "")
                {
                    asOfDateIn += "('" + info.AsOfDate.ToShortDateString() + "'";
                }
                else
                {
                    asOfDateIn += ",'" + info.AsOfDate.ToShortDateString() + "'";
                }

                asOfDates[aodCounter] = info.AsOfDate;

                //Here I will check if the values are nto overridden, if they are not I will update them regardless and save it so i do not have to worry about it later on in the config
                if (!offSet.OverrideBasicSurplus)
                {
                    DataTable pols = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT NIISimulationTypeId,EveSimulationTypeId,BasicSurplusAdminId FROM Atlas_Policy WHERE asOfDate = '"  + info.AsOfDate.ToShortDateString() + "' ");
                    if (pols.Rows.Count > 0)
                    {
                        offSet.NIISimulationTypeId = int.Parse(pols.Rows[0][0].ToString());
                        offSet.EveSimulationTypeId = int.Parse(pols.Rows[0][1].ToString());
                        offSet.BasicSurplusAdminId = int.Parse(pols.Rows[0][2].ToString());
                    }

                    DataTable models = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT [ReportTaxEquivalent] as taxIncome ,[ReportTaxEquivalentYield] as taxYield FROM ATLAS_ModelSetup WHERE asOfDate = '" + info.AsOfDate.ToShortDateString() + "' ");
                    if (models.Rows.Count > 0)
                    {
                        offSet.TaxEquivIncome = Boolean.Parse(models.Rows[0][0].ToString());
                        offSet.TaxEquivYield = Boolean.Parse(models.Rows[0][1].ToString());
                    }
                }

                bs[aodCounter] = offSet.BasicSurplusAdminId.ToString();
                eve[aodCounter] = instService.GetSimulationType(offSet.EveSimulationTypeId);
                nii[aodCounter] = instService.GetSimulationType(offSet.NIISimulationTypeId);
                niiId[aodCounter] = offSet.NIISimulationTypeId.ToString();
                taxIncome[aodCounter] = offSet.TaxEquivIncome;
                taxYield[aodCounter] = offSet.TaxEquivYield;

                aodCounter += 1;
            }

            gctx.SaveChanges();

            //Load as of date from package
            string curDate = curInfo.AsOfDate.ToShortDateString();

            asOfDateIn += ")";

            //Load policy id based off of package as of date
            DataTable pol = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT id FROM Atlas_Policy WHERE asOfDate = '" + curInfo.AsOfDate + "'");
            int polId = 0;
            if (pol.Rows.Count > 0)
            {
                polId = int.Parse(pol.Rows[0][0].ToString());
            }

            StringBuilder sqlQuery = new StringBuilder();

            //Load Simulatios, basicsurplus, and eve nev from policies screen and report config
            bool showIRRPolicies = true;
            bool showLiqPols = true;
            bool showOtherPols = true;

            DataTable sims = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT id, basicSurplusAdminId ,EveSimulationTypeId ,NIISimulationTypeId, LiquidityRiskAssessment, NIIRiskAssessment ,CapitalRiskAssessment FROM [ATLAS_Policy] where asOfDAte = '" + curDate + "'");

            //If now policies found throw error
            if (sims.Rows.Count == 0) throw new Exception("No Policies found for " + curDate);


            if (curOff == null ) throw new Exception("No OffSets");

            //Compare report config and policies config
            if (curOff.NIISimulationTypeId.ToString() != sims.Rows[0][3].ToString() || curOff.EveSimulationTypeId.ToString() != sims.Rows[0][2].ToString())
            {
                showIRRPolicies = false;
            }

            if (curOff.BasicSurplusAdminId.ToString() != sims.Rows[0][1].ToString() || curOff.NIISimulationTypeId.ToString() != sims.Rows[0][3].ToString())
            {
                showLiqPols = false;
            }

            if (curOff.NIISimulationTypeId.ToString() != sims.Rows[0][3].ToString())
            {
                showOtherPols = false;
            }

            //To prevent altering code too much here we are going to just check if show policies is checked off and then rip them in doesnt matter anymore ATO-466
            if (!sr.ExcludePolicies)
            {
                showLiqPols = true;
                showIRRPolicies = true;
            }

            //Load all values for 
            Dictionary<string, Dictionary<string, double>> liqRatios = new Dictionary<string, Dictionary<string, double>>();
            Dictionary<string, Dictionary<string, double>> eveRatios = new Dictionary<string, Dictionary<string, double>>();
            Dictionary<string, Dictionary<string, double>> cfsRatios = new Dictionary<string, Dictionary<string, double>>();
            Dictionary<string, Dictionary<string, Dictionary<string, double>>> niiRatios = new Dictionary<string, Dictionary<string, Dictionary<string, double>>>();

            for (int i = 0; i < asOfDates.Length; i++)
            {
                niiRatios.Add(asOfDates[i].ToShortDateString(), PolicyCalculations.NIIPoliciesValues(nii[i], asOfDates[i], sr.InstitutionDatabaseName, true, taxIncome[i]));
                eveRatios.Add(asOfDates[i].ToShortDateString(), PolicyCalculations.EvePoliciesValues(eve[i], asOfDates[i], sr.InstitutionDatabaseName, false, 0));
                cfsRatios.Add(asOfDates[i].ToShortDateString(), PolicyCalculations.CoreFundPoliciesValues(nii[i], asOfDates[i], sr.InstitutionDatabaseName));

                DataTable histPol = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT id FROM Atlas_Policy WHERE asOfDate = '" + asOfDates[i].ToShortDateString() + "'");

                int histPolId = 0;
                if (histPol.Rows.Count > 0)
                {
                    histPolId = int.Parse(histPol.Rows[0][0].ToString());
                }

                liqRatios.Add(asOfDates[i].ToShortDateString(), PolicyCalculations.LiquidyAndOtherRatioValues(histPolId, int.Parse(niiId[i]), asOfDates[i], int.Parse(bs[i]), sr.InstitutionDatabaseName, true, taxYield[i]));
            }

            //Build Left and Right Liquidity Sections
            ExecLiqSection[] leftLiqSection = generateLiqSection(instService.ConnectionString(), asOfDates, "Left", sr.Id.ToString(), showLiqPols, sims.Rows[0][0].ToString(), curInfo.AsOfDate, liqRatios, asOfDateIn, showOtherPols);
            ExecLiqSection[] rightLiqSection = generateLiqSection(instService.ConnectionString(), asOfDates, "Right", sr.Id.ToString(), showLiqPols, sims.Rows[0][0].ToString(), curInfo.AsOfDate, liqRatios, asOfDateIn, showOtherPols);

            //Build Left and Right Interest Rate Risk Sections

            ExecIRRSection[] leftIRRSection = generateIRRSection(instService.ConnectionString(), asOfDates, "Left", sr.Id.ToString(), showIRRPolicies, showIRRPolicies, sims.Rows[0][0].ToString(), curInfo.AsOfDate, niiRatios, eveRatios, cfsRatios);
            ExecIRRSection[] rightIRRSection = generateIRRSection(instService.ConnectionString(), asOfDates, "Right", sr.Id.ToString(), showIRRPolicies, showIRRPolicies, sims.Rows[0][0].ToString(), curInfo.AsOfDate, niiRatios, eveRatios, cfsRatios);

            //Cap Sections
            DataTable leftCapSection = generateCapitalSection(instService.ConnectionString(), asOfDates, "Left", sr.Id.ToString(), sims.Rows[0][0].ToString(), curInfo.AsOfDate, asOfDateIn, liqRatios, showOtherPols);
            DataTable rightCapSection = generateCapitalSection(instService.ConnectionString(), asOfDates, "Right", sr.Id.ToString(), sims.Rows[0][0].ToString(), curInfo.AsOfDate, asOfDateIn, liqRatios, showOtherPols);
            
            //Get over all risk assements for the three sections and then check overrides if they are there
            int liqRA = 0;
            int irrRA = 0;
            int capRA = int.Parse(sims.Rows[0][6].ToString());

            if (showLiqPols)
            {
                liqRA = int.Parse(sims.Rows[0][4].ToString());
            }

            if (showIRRPolicies)
            {
                irrRA = int.Parse(sims.Rows[0][5].ToString());
            }
           
            if (sr.LiquidityRiskAssesmentOverride)
            {
                liqRA = sr.LiquidityRiskAssesment;
            }

            if (sr.CapitalRiskAssesmentOverride)
            {
                capRA = sr.CapitalRiskAssesment;
            }

            if (sr.InterestRateRiskAssesmentOverride)
            {
                irrRA = sr.InterestRateRiskAssesment;
            }

            bool showCurrent = true;
            if (rep.Package.AsOfDate != asOfDates[0])
            {
                showCurrent = false;
            }

            return new
            {
                leftLiqSection = leftLiqSection,
                rightLiqSection = rightLiqSection,
                leftIRRSection = leftIRRSection,
                rightIRRSection = rightIRRSection,
                leftCapSection = leftCapSection,
                rightCapSection = rightCapSection,
                showBSPolicies = showLiqPols,
                showIRRPolicies = showIRRPolicies,
                showEVEPolicies = showIRRPolicies,
                showCapPolicies = true, // Always true
                liqRA = liqRA,
                irrRA = irrRA,
                capRA = capRA,
                histDates = asOfDates,
                showPolicies = !sr.ExcludePolicies,
                showRA = !sr.ExcludeRiskAssessments,
                liqOrder = sr.LiqOrder,
                capOrder = sr.CapOrder,
                iRROrder = sr.IRROrder,
                hideLiq = sr.HideLiq,
                hideIRR = sr.HideIRR,
                hideCap = sr.HideCap,
                showCurrent = showCurrent
            };
        }

        public ExecLiqSection[] generateLiqSection(string conStr, DateTime[] asOfDates, string side, string erId, bool showPols, string polId, DateTime packageAod, Dictionary<string, Dictionary<string, double>> liqRatios, string asOfDatesInc, bool showOtherPols)
        {
            string baseConStr = Utilities.BuildInstConnectionString("");
            StringBuilder sqlQuery = new StringBuilder();

            DataTable liqSections = Utilities.ExecuteSql(baseConStr, "SELECT [id], [Priority] ,[ReportSectionTypeName], [hideLabel] FROM [ATLAS_ExecutiveRiskSummaryLiquiditySection] WHERE reportSide = '" + side + "' AND ExecutiveRiskSummaryId = " + erId + " ORDER BY Priority");

            if (liqSections.Rows.Count == 0)
            {
                return new ExecLiqSection[0];
            }

            ExecLiqSection[] secs = new ExecLiqSection[liqSections.Rows.Count];
            int rowCounter = 0;

            // pull data for each left section
            foreach (DataRow dr in liqSections.Rows)
            {
                secs[rowCounter] = new ExecLiqSection();
                secs[rowCounter].name = dr["ReportSectionTypeName"].ToString();
                secs[rowCounter].hideLabel = bool.Parse(dr["hideLabel"].ToString());
                secs[rowCounter].table = new DataTable();

                DataTable retTable = new DataTable();
                // left Liq Sections
                secs[rowCounter].table.Columns.Add("Name");
                secs[rowCounter].table.Columns.Add("Policy");
                secs[rowCounter].table.Columns.Add("Risk");
                secs[rowCounter].table.Columns.Add("Current");
                secs[rowCounter].table.Columns.Add("Format");

                for (int i = 0; i < asOfDates.Length; i++)
                {
                    secs[rowCounter].table.Columns.Add("hist" + i);
                }

                // pull details rows under liq section
                DataTable bsDets = Utilities.ExecuteSql(baseConStr, "SELECT [Priority],[OverrideRiskAssesment],[RiskAssesment],[Name] FROM [ATLAS_ExecutiveRiskSummaryLiquiditySectionDet] WHERE ExecutiveRiskSummaryLiquiditySectionId = " + dr["id"].ToString() + " ORDER BY Priority");

                // loop through details and build out table
                foreach (DataRow det in bsDets.Rows)
                {
                    DataRow newRow = secs[rowCounter].table.NewRow();
                    newRow["Name"] = det["Name"].ToString();

                    string pol = "";
                    string risk = "";
                
                    string[] paramArr = new string[] { "@detName" };
                    string[] paramVals = new string[] { det["Name"].ToString() };
                
                    DataTable bsPols = Utilities.ExecuteParameterizedSql(conStr, "SELECT Name, NULLIF(policyLimit, -999) as  policylimit, RiskAssessment, shortName, formatType, otherPol, currentRatioFormat  FROM (SELECT DISTINCT [Name], policyId, policyLimit, RiskAssessment, shortName, formatType, 0 as OtherPol, 'perc' as currentRatioFormat  FROM [ATLAS_LiquidityPolicy] as lp UNION ALL SELECT DISTINCT [Name], policyId, policyLimit, RiskAssessment, shortName, formatType, 1 as OtherPol, currentRatioFormat   FROM Atlas_OtherPolicy) as pols WHERE name = @detName AND policyid = " + polId + "", paramArr, paramVals);
                
                    // = Utilities.ExecuteSql(conStr, "SELECT Name, NULLIF(policyLimit, -999) as  policylimit, RiskAssessment, shortName, formatType  FROM (SELECT DISTINCT [Name], policyId, policyLimit, RiskAssessment, shortName, formatType  FROM [ATLAS_LiquidityPolicy] as lp UNION ALL SELECT DISTINCT [Name], policyId, policyLimit, RiskAssessment, shortName, formatType   FROM Atlas_OtherPolicy) as pols WHERE name = '" + det["Name"].ToString() + "' AND policyid = " + polId + "");
                    bool divideBy1000 = false;
                    newRow["Format"] = "0.0%";
                    newRow["Policy"] = "";
                
                    bool customPol = false;
                    bool customPolFormatSet = false;

                    if (bsPols.Rows.Count > 0)
                    {
                        // certain non-custom policies that take dollar amounts
                        switch (bsPols.Rows[0]["shortName"].ToString())
                        {
                            case "borrowMax":
                            case "brokeredMax":
                            case "fhlbMax":
                                newRow["Format"] = "$0,0";
                                divideBy1000 = true;
                                break;

                            default:
                                if (!string.IsNullOrEmpty(bsPols.Rows[0]["formatType"].ToString())) // if formatType is not null, there is a custom format
                                {
                                    switch (bsPols.Rows[0]["formatType"].ToString())
                                    {
                                        case "%":
                                            newRow["Format"] = "0.0%";
                                            break;
                                        case "$":
                                            newRow["Format"] = "$0,0";
                                            break;
                                        case "#":
                                            newRow["Format"] = "0,0";
                                            break;
                                    }

                                    customPolFormatSet = true;
                                    customPol = true;
                                }

                                break;
                        }
                    }
                    else
                    {
                        customPol = true;
                    }

                    if (bsPols.Rows.Count > 0)
                    {
                        pol = bsPols.Rows[0]["PolicyLimit"].ToString();
                        risk = bsPols.Rows[0]["riskAssessment"].ToString();
                
                        if (bsPols.Rows[0]["OtherPol"].ToString() == "1")
                        {
                            if (bsPols.Rows[0]["currentRatioFormat"].ToString() == "perc")
                            {
                                newRow["Format"] = "0.0%";
                            }
                            else
                            {
                                newRow["Format"] = "$0,0";
                                divideBy1000 = true;
                            }

                            if (showOtherPols)
                            {
                                newRow["Policy"] = pol;
                                newRow["risk"] = risk;
                            }
                            else
                            {
                                newRow["Policy"] = "";
                                newRow["risk"] = "";
                            }
                        }
                        else if (showPols)
                        {
                            newRow["Policy"] = pol;
                            newRow["risk"] = risk;
                        }
                        else
                        {     
                            newRow["Policy"] = "";
                            newRow["risk"] = "";
                        }
                    }
                
                    if (((bool)det["OverrideRiskAssesment"]))
                    {
                        newRow["risk"] = det["RiskAssesment"].ToString();
                    }
                
                    if (!customPol)
                    {
                        if (liqRatios[packageAod.ToShortDateString()].ContainsKey(det["name"].ToString()))
                        {
                            newRow["Current"] = (liqRatios[packageAod.ToShortDateString()][det["name"].ToString()] / (divideBy1000 ? 1000 : 1)).ToString();
                        }
                        else
                        {
                            newRow["Current"] = "";
                        }
                
                        //Loop through as Ofdates and set historuc ones
                        for (int a = 1; a < asOfDates.Length; a++)
                        {
                            if (liqRatios[asOfDates[a].ToShortDateString()].ContainsKey(det["name"].ToString()))
                            {
                                newRow["hist" + a.ToString()] = (liqRatios[asOfDates[a].ToShortDateString()][det["name"].ToString()] / (divideBy1000 ? 1000 : 1)).ToString();
                            }
                            else
                            {
                                newRow["hist" + a.ToString()] = "";
                            }
                        }
                    }
                    else
                    {
                        sqlQuery.Clear();
                        sqlQuery.AppendLine(" SELECT currentRatio, asOfDate, name, formatType FROM");
                        sqlQuery.AppendLine(" (SELECT currentRatio, asOfDate, name, formatType FROM ATLAS_LiquidityPolicy AS lp INNER JOIN ATLAS_Policy AS p ON lp.policyId = p.id ");
                        sqlQuery.AppendLine(" UNION ALL ");
                        sqlQuery.AppendLine(" SELECT currentRatio, asOfDate, name, formatType FROM ATLAS_OtherPolicy AS lp INNER JOIN ATLAS_Policy AS p ON lp.policyId = p.id) as lps ");
                        sqlQuery.AppendLine(" WHERE name = @detName and asOfDate in " + asOfDatesInc + " ORDER BY asOfDate DESC ");
                
                        DataTable liqPols = Utilities.ExecuteParameterizedSql(conStr, sqlQuery.ToString(), new string[] { "@detName" }, new string[] { det["Name"].ToString() });
                
                        foreach (DataRow liq in liqPols.Rows)
                        {
                            if (!customPolFormatSet)
                            {
                                switch (liq["formatType"].ToString())
                                {
                                    case "%":
                                        newRow["Format"] = "0.0%";
                                        break;
                                    case "$":
                                        newRow["Format"] = "$0,0";
                                        break;
                                    case "#":
                                        newRow["Format"] = "0,0";
                                        break;
                                }
                                newRow["Current"] = "";
                                customPolFormatSet = true;
                            }
                
                            DateTime curDate = DateTime.Parse(liq["asOfDate"].ToString());
                
                            if (curDate == packageAod)
                            {
                                //Set current, policy and ratio 
                                newRow["Current"] = liq["CurrentRatio"].ToString();
                            }
                            else//find as of date in array and set value
                            {
                                int aodIdx = Array.IndexOf(asOfDates, curDate);
                
                                if (aodIdx >= 0)
                                {
                                    newRow["hist" + aodIdx] = liq["CurrentRatio"].ToString();
                                }
                            }
                        }
                    }
          
                    secs[rowCounter].table.Rows.Add(newRow);
                }
                
                rowCounter += 1;
            }

            return secs;
        }

        public ExecIRRSection[] generateIRRSection(string conStr, DateTime[] asOfDates, string side, string erId, bool showIRRPolicies, bool showEVEPolicies, string polId, DateTime packageAod, Dictionary<string, Dictionary<string, Dictionary<string, double>>> niiRatios, Dictionary<string, Dictionary<string, double>> eveRatios , Dictionary<string, Dictionary<string, double>> cfsRatios)
        {
            string baseConStr = Utilities.BuildInstConnectionString("");
            StringBuilder sqlQuery = new StringBuilder();

            DataTable irrSections = Utilities.ExecuteSql(baseConStr, "SELECT [id], [Priority] ,[Name], [hideLabel] FROM [ATLAS_ExecutiveRiskSummaryIRRSection] WHERE reportSide = '" + side + "' AND ExecutiveRiskSummaryId = " + erId + " ORDER BY Priority");

            if (irrSections.Rows.Count == 0)
            {
                return new ExecIRRSection[0];
            }

            ExecIRRSection[] secs = new ExecIRRSection[irrSections.Rows.Count];
            int rowCounter = 0;
            //Pull data for each left section
            foreach (DataRow dr in irrSections.Rows)
            {
                secs[rowCounter] = new ExecIRRSection();

                //Now that i am in sections pull up all irr dets
                secs[rowCounter].name = dr["name"].ToString();
                secs[rowCounter].hideLabel = bool.Parse(dr["hideLabel"].ToString());

                DataTable irrDets = Utilities.ExecuteSql(baseConStr, "SELECT [id], [Priority] ,[Name], [hideLabel] FROM [ATLAS_ExecutiveRiskSummaryIRRDet] WHERE  [ExecutiveRiskSummaryIRRSectionId] = " + dr["id"].ToString() + " ORDER BY Priority");
                secs[rowCounter].subSections = new ExecIRRSubSection[irrDets.Rows.Count];

                int detCounter = 0;
                foreach (DataRow det in irrDets.Rows)
                {
                    secs[rowCounter].subSections[detCounter] = new ExecIRRSubSection();
                    secs[rowCounter].subSections[detCounter].name = det["Name"].ToString();
                    secs[rowCounter].subSections[detCounter].hideLabel = bool.Parse(det["hideLabel"].ToString());
                    secs[rowCounter].subSections[detCounter].table = new DataTable();

                    DataTable retTable = new DataTable();
                    //Left Liq Sections
                    secs[rowCounter].subSections[detCounter].table.Columns.Add("Name");
                    secs[rowCounter].subSections[detCounter].table.Columns.Add("Policy");
                    secs[rowCounter].subSections[detCounter].table.Columns.Add("Risk");
                    secs[rowCounter].subSections[detCounter].table.Columns.Add("Current");
                    secs[rowCounter].subSections[detCounter].table.Columns.Add("Format");
                    for (int i = 0; i < asOfDates.Length; i++)
                    {
                        secs[rowCounter].subSections[detCounter].table.Columns.Add("hist" + i);
                    }

                    sqlQuery.Clear();
                    sqlQuery.AppendLine(" SELECT st.id ,st.name ,[OverrideRiskAssesment] ,[RiskAssesment] ");
                    sqlQuery.AppendLine("   FROM [ATLAS_ExecutiveSummaryIRRScenario] as ess ");
                    sqlQuery.AppendLine("   INNER JOIN [ATLAS_ScenarioType] AS st ON st.id = ess.scenarioTypeID WHERE ess.[ExecutiveRiskSummaryIRRDetId] = " + det["id"].ToString() + "  ");
                    sqlQuery.AppendLine("   ORDER BY ess.[Priority] ");

                    //Now loop through the scenarios and fill out table with policy info
                    DataTable scens = Utilities.ExecuteSql(baseConStr, sqlQuery.ToString());

                    string lookUpVal = "";
                    string type = "CoreFunding";
                    bool polFlag = showIRRPolicies;
                    switch (det["Name"].ToString())
                    {
                        case "Year 1 NII % ∆ from Year 1 Base":
                            lookUpVal = "year1Year1";
                            type = "IRR";
                            polFlag = showIRRPolicies;
                            break;
                        case "Year 2 NII % ∆ from Year 1 Base":
                            lookUpVal = "year2Year1";
                            type = "IRR";
                            polFlag = showIRRPolicies;
                            break;
                        case "Year 2 NII % ∆ from Year 2 Base":
                            lookUpVal = "year2Year2";
                            type = "IRR";
                            break;
                        case "24 Month % ∆ from Base":
                            lookUpVal = "24Month";
                            type = "IRR";
                            polFlag = showIRRPolicies;
                            break;
                        case "Post Shock EVE Ratio":
                        case "Post Shock NEV Ratio":
                            lookUpVal = "postShock";
                            type = "EVE";
                            polFlag = showEVEPolicies;
                            break;
                        case "EVE Ratio BP ∆ from 0 Shock":
                        case "NEV Ratio BP ∆ from 0 Shock":
                            lookUpVal = "bpChange";
                            type = "EVE";
                            polFlag = showEVEPolicies;
                            break;
                        case "EVE % ∆ from 0 Shock":
                        case "NEV % ∆ from 0 Shock":
                            lookUpVal = "eveChange";
                            type = "EVE";
                            polFlag = showEVEPolicies;
                            break;
                    }

                    secs[rowCounter].subSections[detCounter].showPol = polFlag;

                    foreach (DataRow scen in scens.Rows)
                    {
                        DataRow newRow = secs[rowCounter].subSections[detCounter].table.NewRow();

                        newRow["Name"] = scen["name"].ToString();
      
                        if (type == "EVE")
                        {
                            //if (det["name"].ToString() == "EVE Ratio BP Change from 0 Shock" || det["name"].ToString() == "NEV Ratio BP Change from 0 Shock")
                            if(det["name"].ToString().Contains(" BP "))
                            {
                                newRow["format"] = "BP";
                            }
                            else
                            {
                                newRow["Format"] = "0.0%";
                            }

                            newRow["Policy"] = "";
                            newRow["risk"] = "";
                            newRow["Current"] = "";

                            //Load Policies 
                            DataTable pols = Utilities.ExecuteSql(conStr, "SELECT NULLIF(nip.policylimit, -999) AS policylimit, nip.RiskAssessment, ni.shortNAme FROM [ATLAS_EvePolicy] as nip INNER JOIN ATLAS_EvePolicyParent as ni ON ni.id = nip.EvePolicyParentId  WHERE ni.PolicyId = " + polId + " AND nip.ScenarioTypeId = " + scen["id"].ToString() + " AND ni.shortName = '" + lookUpVal + "'");

                            if (eveRatios.ContainsKey(packageAod.ToShortDateString()))
                            {
                                if (eveRatios[packageAod.ToShortDateString()].ContainsKey(lookUpVal + scen["name"])){
                                    newRow["Current"] = eveRatios[packageAod.ToShortDateString()][lookUpVal + scen["name"].ToString()];
                                }
                            }

                            if (showEVEPolicies)  
                            {
                                if (pols.Rows.Count > 0)
                                {
                                    newRow["Policy"] = pols.Rows[0]["policylimit"].ToString();
                                    newRow["risk"] = pols.Rows[0]["RiskAssessment"].ToString();
                                }
                            }

                            //Always override it even if no policies
                            if ((bool)scen["OverrideRiskAssesment"]){
                                newRow["risk"] = scen["RiskAssesment"].ToString();
                            }
                        }
                        else if (type == "IRR")
                        {
                            if (lookUpVal != "year2Year1" && scen["Name"].ToString().ToUpper() == "BASE")
                            {
                                newRow["Format"] = "$0,0";
                                newRow["Policy"] = "";
                                newRow["Risk"] = "";
                            }
                            else
                            {
                                newRow["Format"] = "0.0%";
                            }

                            //Load Policies 
                            DataTable pols = Utilities.ExecuteSql(conStr, "SELECT nip.policylimit, nip.RiskAssessment FROM [ATLAS_NIIPolicy] as nip INNER JOIN ATLAS_NIIPolicyParent as ni ON ni.id = nip.NIIPolicyParentId  WHERE ni.PolicyId = " + polId + " AND nip.ScenarioTypeId = " + scen["id"].ToString() + " AND ni.shortName = '" + lookUpVal + "'");

                            if (niiRatios.ContainsKey(packageAod.ToShortDateString()))
                            {
                                if (niiRatios[packageAod.ToShortDateString()] != null) {
                                    if (niiRatios[packageAod.ToShortDateString()].ContainsKey(lookUpVal))
                                    {
                                        if (niiRatios[packageAod.ToShortDateString()][lookUpVal].ContainsKey(scen["name"].ToString()))
                                        {
                                            newRow["Current"] = niiRatios[packageAod.ToShortDateString()][lookUpVal][scen["name"].ToString()];
                                        }

                                    }
                                }
                            }
      
                            if (showIRRPolicies)
                            {
                                if (pols.Rows.Count > 0)
                                {
                                    newRow["Policy"] = pols.Rows[0]["policylimit"];
                                    newRow["risk"] = pols.Rows[0]["RiskAssessment"];
                                }
                            }

                            //Always override it even if no policies
                            if ((bool)scen["OverrideRiskAssesment"])
                            {
                                newRow["risk"] = scen["RiskAssesment"].ToString();
                            }
                        }
                        else
                        {
                            newRow["Format"] = "0.0%";

                            DataTable pols = Utilities.ExecuteSql(conStr, "SELECT cfp.[PolicyLimit], cfp.[RiskAssessment] FROM [ATLAS_CoreFundPolicy] AS cfp INNER JOIN ATLAS_CoreFundParent AS cfpp ON cfp.CoreFundParentId = cfpp.id WHERE cfpp.PolicyId = " + polId + " AND cfp.scenarioTypeId = " + scen["id"].ToString() + "");
                            //corefund super easy just by scenario
                            if (cfsRatios.ContainsKey(packageAod.ToShortDateString()))
                            {
                                if (cfsRatios[packageAod.ToShortDateString()] != null)
                                {
                                    if (cfsRatios[packageAod.ToShortDateString()].ContainsKey(scen["name"].ToString()))
                                    {
                                        newRow["Current"] = cfsRatios[packageAod.ToShortDateString()][scen["name"].ToString()];
                                    }
                                }
                            }

                            if (showIRRPolicies)
                            {
                                if (pols.Rows.Count > 0)
                                {
                                    newRow["Policy"] = pols.Rows[0]["policylimit"];
                                    newRow["risk"] = pols.Rows[0]["RiskAssessment"];
                                }
                            }

                            //Always override it even if no policies
                            if ((bool)scen["OverrideRiskAssesment"])
                            {
                                newRow["risk"] = scen["RiskAssesment"].ToString();
                            }
                        }

                        //now loop through as ofdates and set historic onces
                        for (int a = 1; a < asOfDates.Length; a++)
                        {
                            if (type == "EVE")
                            {
                                if (eveRatios.ContainsKey(asOfDates[a].ToShortDateString()))
                                {
                                    if (eveRatios[asOfDates[a].ToShortDateString()].ContainsKey(lookUpVal + scen["name"]))
                                    {
                                        newRow["hist" + a.ToString()] = eveRatios[asOfDates[a].ToShortDateString()][lookUpVal + scen["name"].ToString()];
                                    }
                                }
                            }
                            else if (type == "IRR")
                            {
                                if (niiRatios.ContainsKey(asOfDates[a].ToShortDateString()))
                                {
                                    if (niiRatios[asOfDates[a].ToShortDateString()] != null && niiRatios[asOfDates[a].ToShortDateString()].ContainsKey(lookUpVal))
                                    {
                                        if (niiRatios[asOfDates[a].ToShortDateString()][lookUpVal].ContainsKey(scen["name"].ToString()))
                                        {
                                            newRow["hist" + a.ToString()] = niiRatios[asOfDates[a].ToShortDateString()][lookUpVal][scen["name"].ToString()];
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //corefund super easy just by scenario
                                if (cfsRatios.ContainsKey(asOfDates[a].ToShortDateString()))
                                {
                                    if (cfsRatios[asOfDates[a].ToShortDateString()] != null && cfsRatios[asOfDates[a].ToShortDateString()].ContainsKey(scen["name"].ToString()))
                                    {
                                        newRow["hist" + a.ToString()] = cfsRatios[asOfDates[a].ToShortDateString()][scen["name"].ToString()];
                                    }
                                }
                            }

                        }

                        //Load Policies based off of type
                        secs[rowCounter].subSections[detCounter].table.Rows.Add(newRow);
                    } //Ends Scenario loop

                    detCounter += 1;
                }//Ends details loop
                rowCounter += 1;
            }//End Sections loop
            return secs;
        }

        public DataTable generateCapitalSection(string conStr, DateTime[] asOfDates, string side, string erId, string polId, DateTime packageAod, string asOfDateInIncCurrent, Dictionary<string, Dictionary<string, double>> liqRatios, bool showOtherPols)
        {
            string baseConStr = Utilities.BuildInstConnectionString("");
            //Create output table
            DataTable capTable = new DataTable();
            //Left Liq Sections
            capTable.Columns.Add("Name");
            capTable.Columns.Add("Policy");
            capTable.Columns.Add("Risk");
            capTable.Columns.Add("Current");
            capTable.Columns.Add("Format");

            for (int i = 0; i < asOfDates.Length; i++)
            {
                capTable.Columns.Add("hist" + i);
            }

            DataTable execCaps = Utilities.ExecuteSql(baseConStr, "SELECT [Priority],[Name],[OverrideRiskAssesment],[RiskAssesment] FROM [ATLAS_ExecutiveRiskSummaryCapitalSection] WHERE ExecutiveRiskSummaryId = " + erId + " AND ReportSide = '" + side + "' ORDER BY Priority");

            foreach(DataRow dr in execCaps.Rows)
            {
                DataRow newRow = capTable.NewRow();
                newRow["Name"] = dr["Name"].ToString();
                newRow["Current"] = "";
                newRow["Policy"] = "";
                StringBuilder sqlQuery = new StringBuilder();

                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT currentRatio, asOfDate, name, formatType, policyLimit, RiskAssessment, OtherPol, currentRatioFormat FROM");
                sqlQuery.AppendLine(" (SELECT currentRatio, asOfDate, name, formatType, RiskAssessment, policyLimit, 0 as OtherPol, currentRatioFormat FROM ATLAS_CapitalPolicy AS lp INNER JOIN ATLAS_Policy AS p ON lp.policyId = p.id ");
                sqlQuery.AppendLine(" UNION ALL ");
                sqlQuery.AppendLine(" SELECT currentRatio, asOfDate, name, formatType, RiskAssessment, policyLimit, 1 as OtherPol, currentRatioFormat FROM ATLAS_OtherPolicy AS lp INNER JOIN ATLAS_Policy AS p ON lp.policyId = p.id) as lps ");
                sqlQuery.AppendLine(" WHERE name = @detName and asOfDate in " + asOfDateInIncCurrent + " ORDER BY asOfDate DESC");
                
                DataTable capPols = Utilities.ExecuteParameterizedSql(conStr, sqlQuery.ToString(), new string[] { "@detName" }, new string[] { dr["Name"].ToString() });
                
                bool customPolFormatSet = false;
                bool otherPol = false;
                
                if (capPols.Rows.Count > 0)
                {
                    if (capPols.Rows[0]["OtherPol"].ToString() == "1")
                    {
                        otherPol = true;
                    }
                }
                bool divideBy1000 = false;

                foreach (DataRow cp in capPols.Rows)
                {
                    if (cp["formatType"].ToString() != "" && !customPolFormatSet) //if formattyope is not null means ucstoms
                    {
                        switch (cp["formatType"].ToString())
                        {
                            case "%":
                                newRow["Format"] = "0.00%";
                                break;
                            case "$":
                                newRow["Format"] = "$0,0";
                                break;
                            case "#":
                                newRow["Format"] = "0,0";
                                break;
                        }
                        customPolFormatSet = true;
                    }
                    else //everything else is percentage format
                    {
                        newRow["Format"] = "0.00%";
                    }
      
                    if (newRow["Format"].ToString() == "0.00%" && otherPol)
                    {
                        if (cp["currentRatioFormat"].ToString() == "perc") 
                        {
                            newRow["Format"] = "0.0%";
                        }
                        else{
                            newRow["Format"] = "$0,0";
                            divideBy1000 = true;
                        }
                    }
                
                    DateTime curDate = DateTime.Parse(cp["asOfDate"].ToString());
                
                    if (curDate == packageAod)
                    {
                        newRow["Current"] = cp["CurrentRatio"].ToString();
                
                        //Set current, policy and ratio           
                         newRow["Policy"] = cp["PolicyLimit"].ToString();
                         newRow["Risk"] = cp["RiskAssessment"].ToString();
                    }
                    else if (!otherPol)//find as of date in array and set value
                    {
                        int aodIdx = Array.IndexOf(asOfDates, curDate);
                
                        if (aodIdx >= 0)
                        {
                            newRow["hist" + aodIdx] = cp["CurrentRatio"].ToString();
                        }
                    }
                }
                
                if (otherPol)
                {
                    if (liqRatios[packageAod.ToShortDateString()].ContainsKey(dr["name"].ToString()))
                    {
                        newRow["Current"] = double.Parse((liqRatios[packageAod.ToShortDateString()][dr["name"].ToString()]).ToString()) / (divideBy1000 ? 1000 : 1);
                    }
                    else
                    {
                        newRow["Current"] = "";
                    }
                
                    if (!showOtherPols)
                    {
                        newRow["Policy"] = "";
                        newRow["Risk"] = "";
                    }

                    for (int a = 1; a < asOfDates.Length; a++)
                    {
                        if (liqRatios[asOfDates[a].ToShortDateString()].ContainsKey(dr["name"].ToString()))
                        {
                            newRow["hist" + a.ToString()] = double.Parse((liqRatios[asOfDates[a].ToShortDateString()][dr["name"].ToString()]).ToString()) / (divideBy1000 ? 1000 : 1);
                        }
                        else
                        {
                            newRow["hist" + a.ToString()] = "";
                        }
                    }
                }
                
                if (((bool)dr["OverrideRiskAssesment"]))
                {
                    newRow["risk"] = dr["RiskAssesment"].ToString();
                }

                capTable.Rows.Add(newRow);
            }

            return capTable;
        }
    }
}