﻿using Atlas.Institution.Data;
using Atlas.Institution.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;

namespace Atlas.Web.Services
{
    public class InterestRatePolicyGuidelinesService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object InterestRatePolicyGuidelinesViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            var interestRatePolicyGuidelines = gctx.InterestRatePolicyGuidelines.Where(s => s.Id == reportId).FirstOrDefault();
            if (interestRatePolicyGuidelines == null)
                throw new Exception("InterestRatePolicyGuidelines Not Found");

            string conStr = Utilities.BuildInstConnectionString(interestRatePolicyGuidelines.InstitutionDatabaseName);
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            var asOfDates = Utilities.ExecuteSql(conStr, "SELECT Id, asOfDate FROM asOfDateInfo WHERE asOfDate <= '" + date + "' ORDER BY asOfDate DESC");

            //IQueryable<Policy>[] policies = new IQueryable<Policy>[interestRatePolicyGuidelines.Dates.Count];
            var dates = new List<DateTime>();
            var policies = new List<Policy>();
            foreach (var d in interestRatePolicyGuidelines.Dates.OrderBy(s => s.Priority))
            {
                if (d.AsOfDateOffset < asOfDates.Rows.Count)
                {
                    date = DateTime.Parse(asOfDates.Rows[d.AsOfDateOffset]["asOfDate"].ToString());
                    dates.Add(date);
                    policies.Add(Policy(new InstitutionContext(Utilities.BuildInstConnectionString(interestRatePolicyGuidelines.InstitutionDatabaseName)), date, interestRatePolicyGuidelines.InstitutionDatabaseName));
                }
            }
            return new
            {
                interestRatePolicyGuidelines = interestRatePolicyGuidelines,
                dates = dates,
                policies = policies,
                errors = errors,
                warnings = warnings
            };
        }

        public Policy Policy(InstitutionContext gctx, DateTime date, string instDBName)
        {
            DataTable ret = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(instDBName), "SELECT Id FROM [dbo].[ATLAS_Policy] WHERE asOfDate = '" + date.ToShortDateString() + "'");
            if (ret.Rows.Count <= 0) {
                    warnings.Add(String.Format(Utilities.GetErrorTemplate(4), date.ToShortDateString()));
            }
            int eveId = (ret.Rows.Count > 0) ? (int)ret.Rows[0][0] : -1;

            return gctx.Policies
                .Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings))
                .Include(s => s.NIIPolicyParents.Select(z => z.NIIPolicies.Select(y => y.ScenarioType)))
                //.Include(s => s.CoreFundParents.Select(z => z.CoreFundPolicies.Select(y => y.ScenarioType)))
                .Include(s => s.EvePolicyParents.Select(z => z.EvePolicies.Select(y => y.ScenarioType)))
                //.Include(s => s.CapitalPolicies)
                //.Include(s => s.OtherPolicies)
                .Where(s => s.Id == eveId)
                .FirstOrDefault();
        }
    }
}
