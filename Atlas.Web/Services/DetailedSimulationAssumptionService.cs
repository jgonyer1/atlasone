﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class DetailedSimulationAssumptionService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object DetailedSimulationAssumptionViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            GlobalController gc = new GlobalController();
            
            //Look Up Funding Matrix Settings
            var c = gctx.DetailedSimulationAssumptions.Where(z => z.Id == reportId).FirstOrDefault();
            Dictionary<string, DataTable> policyOffsets = gc.PolicyOffsets(c.InstitutionDatabaseName);
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (c == null) throw new Exception("Detailed Simulation Assumption not found");



            string conStr = Utilities.BuildInstConnectionString(c.InstitutionDatabaseName);


            var instService = new InstitutionService(c.InstitutionDatabaseName);
            string aodStr = instService.GetInformation(rep.Package.AsOfDate, c.AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            
            if (!c.OverrideSimulationType)
            {
                if (policyOffsets.ContainsKey(aodStr))
                {
                    c.SimulationTypeId = Int32.Parse(policyOffsets[aodStr].Rows[0]["niiId"].ToString());
                }
            }
            if (!c.OverrideScenarioType)
            {
                if (policyOffsets.ContainsKey(aodStr))
                {
                    c.ScenarioTypeId = Int32.Parse(policyOffsets[aodStr].Rows[0]["bsScenario"].ToString());
                }
            }
            gctx.SaveChanges();
            var obj = instService.GetSimulationScenario(rep.Package.AsOfDate, c.AsOfDateOffset, c.SimulationTypeId, c.ScenarioTypeId);
            string simulationid = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();
            
            

            DataTable albRecs = new DataTable();
            DataTable relatTable = new DataTable();
            if (simulationid == "" || scenario == "" || asOfDate == "") { 
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), c.ScenarioType.Name, c.SimulationType.Name, c.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                DateTime curDate = DateTime.Parse(asOfDate);
                curDate = new DateTime(curDate.Year, curDate.Month, 1);
                DateTime firstProj = curDate.AddMonths(1);

                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	alb.code	 ");
                sqlQuery.AppendLine(" 	,alb.name ");
                sqlQuery.AppendLine(" 	,alp1.stBal as startBal ");
                sqlQuery.AppendLine(" 	,alp1.stRate  ");
                sqlQuery.AppendLine(" 	,CASE alb.Int_Freq ");
                sqlQuery.AppendLine(" 		WHEN 0 THEN '30/360' ");
                sqlQuery.AppendLine(" 		WHEN 1 THEN 'Actual/360' ");
                sqlQuery.AppendLine(" 		WHEN 2 THEN 'Actual/Actual' ");
                sqlQuery.AppendLine(" 		ELSE 'Unkown' ");
                sqlQuery.AppendLine(" 	END as accrual ");
                sqlQuery.AppendLine(" 	,idxR.Name as repriceIndex ");
                sqlQuery.AppendLine("     ,als.RepEIndexO as repriceOffset ");
                sqlQuery.AppendLine(" 	,CASE ");
                sqlQuery.AppendLine(" 		WHEN als.RepEIndexT = 0 THEN ");
                sqlQuery.AppendLine(" 			RepERate.rate + als.RepEIndexO ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			RepERate.rate + (RepERate.rate * (als.RepEIndexO / 100)) ");
                sqlQuery.AppendLine("     END as RepERate ");
                sqlQuery.AppendLine(" 	,CASE ");
                sqlQuery.AppendLine(" 		WHEN als.TiedBalance <> 0 THEN ");
                sqlQuery.AppendLine(" 			1 ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			als.RepEFReq ");
                sqlQuery.AppendLine(" 	END as repEFreq ");
                sqlQuery.AppendLine(" 	,als.repEChngCap  ");
                sqlQuery.AppendLine(" 	,als.repEChngFlr  ");
                sqlQuery.AppendLine(" 	,alp6.lifeTimeRateCap  as RepELifeCap ");
                sqlQuery.AppendLine(" 	,alp6.lifeTimeRatefloor  as RepEFloor ");
                sqlQuery.AppendLine(" 	,idxN.name as newIndex ");
                sqlQuery.AppendLine(" 	,als.newIndexO as newOffset ");
                sqlQuery.AppendLine(" 	,CASE ");
                sqlQuery.AppendLine(" 		WHEN als.NewIndexT = 0 THEN ");
                sqlQuery.AppendLine(" 			NewRate.rate + als.NewIndexO ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			NewRate.rate + (NewRate.rate * (als.NewIndexO / 100)) ");
                sqlQuery.AppendLine("     END as NewRate ");
                sqlQuery.AppendLine(" 	,idxrn.Name as newRepriceIndex ");
                sqlQuery.AppendLine(" 	,als.RepNIndexO as newRepriceOffset ");
                sqlQuery.AppendLine(" 	,CASE ");
                sqlQuery.AppendLine(" 		WHEN als.RepNIndexT  = 0 THEN ");
                sqlQuery.AppendLine(" 			repNRate.rate + als.RepNIndexO ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			repNRate.rate + (repNRate.rate * (als.RepNIndexO / 100)) ");
                sqlQuery.AppendLine("     END as newRepriceRate ");
                sqlQuery.AppendLine(" 	,CASE  ");
                sqlQuery.AppendLine(" 		WHEN als.TiedBalance <> 0 THEN ");
                sqlQuery.AppendLine(" 			1 ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			als.RepNFreqLag ");
                sqlQuery.AppendLine(" 	END as newRepriceFixedPer ");
                sqlQuery.AppendLine(" 	,als.repNFreq as newRepriceFreq ");
                sqlQuery.AppendLine(" 	,als.repNChngCap as newRepriceCap ");
                sqlQuery.AppendLine(" 	,als.repNChngFlr as newRepriceFlr ");
                sqlQuery.AppendLine(" 	,als.newCap as newRepriceLifetimeCap ");
                sqlQuery.AppendLine(" 	,als.newFlr as newRepriceLifetimeFloor ");
                sqlQuery.AppendLine(" 	,CASE als.PayEPrePay  ");
                sqlQuery.AppendLine(" 		WHEN 0 THEN ");
                sqlQuery.AppendLine(" 			'--' ");
                sqlQuery.AppendLine(" 		WHEN 1 THEN ");
                sqlQuery.AppendLine(" 			'User Def.' ");
                sqlQuery.AppendLine(" 		WHEN 2 THEN ");
                sqlQuery.AppendLine(" 			CASE  ");
                sqlQuery.AppendLine(" 				WHEN idxp.code IS NULL THEN ");
                sqlQuery.AppendLine(" 					'Index' ");
                sqlQuery.AppendLine(" 				ELSE ");
                sqlQuery.AppendLine(" 					idxp.Name  ");
                sqlQuery.AppendLine(" 			END ");
                sqlQuery.AppendLine(" 		WHEN 3 THEN	 ");
                sqlQuery.AppendLine(" 			'BK' ");
                sqlQuery.AppendLine(" 		WHEN 4 THEN ");
                sqlQuery.AppendLine(" 			'Decay' ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			'--' ");
                sqlQuery.AppendLine(" 	END as preEIndex ");
                sqlQuery.AppendLine(" 	,CASE als.PayNPrePay  ");
                sqlQuery.AppendLine(" 		WHEN 0 THEN ");
                sqlQuery.AppendLine(" 			'--' ");
                sqlQuery.AppendLine(" 		WHEN 1 THEN ");
                sqlQuery.AppendLine(" 			'User Def.' ");
                sqlQuery.AppendLine(" 		WHEN 2 THEN ");
                sqlQuery.AppendLine(" 			CASE  ");
                sqlQuery.AppendLine(" 				WHEN idxpn.code IS NULL THEN ");
                sqlQuery.AppendLine(" 					'Index' ");
                sqlQuery.AppendLine(" 				ELSE ");
                sqlQuery.AppendLine(" 					idxpn.Name  ");
                sqlQuery.AppendLine(" 			END ");
                sqlQuery.AppendLine(" 		WHEN 3 THEN	 ");
                sqlQuery.AppendLine(" 			'BK' ");
                sqlQuery.AppendLine(" 		WHEN 4 THEN ");
                sqlQuery.AppendLine(" 			'Decay' ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			'--' ");
                sqlQuery.AppendLine(" 	END as preNIndex ");
                sqlQuery.AppendLine(" 	,alp6.repEBal  ");
                sqlQuery.AppendLine(" 	,alp1.endNewBal as newCashflowBal ");
                sqlQuery.AppendLine("    ,CASE WHEN als.AmtNAmort <> 0 THEN als.newWAM ELSE null END as newCashflowWAM");
                sqlQuery.AppendLine("    ,als.matNSensi ");
                sqlQuery.AppendLine("    ,alb.isAsset ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_ALB WHERE simulationid = {0} AND cat_type in (0,1,5) AND exclude = 0) AS alb INNER JOIN ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_ALS WHERE simulationid = {0} AND scenario = '{1}' AND exclude = 0) AS als ON ");
                sqlQuery.AppendLine(" als.code = alb.code  ");
                sqlQuery.AppendLine(" LEFT JOIN  ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_ALA WHERE simulationid = {0} AND month = '{2}') AS ala ");
                sqlQuery.AppendLine(" ON ala.code = alb.code ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_IndexB WHERE simulationid = {0} ) as idxR ON ");
                sqlQuery.AppendLine(" als.RepEIndexC = idxr.code  ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_IndexB WHERE simulationid = {0} ) as idxN ON ");
                sqlQuery.AppendLine(" als.NewIndexC = idxn.code  ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_IndexB WHERE simulationid = {0} ) as idxP ON ");
                sqlQuery.AppendLine(" als.PreIndEIndexC = idxp.code  ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_IndexB WHERE simulationid = {0} ) as idxrn ON ");
                sqlQuery.AppendLine(" als.repNIndexC = idxrn.code  ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT * FROM Basis_IndexB WHERE simulationid = {0} ) as idxpn ON ");
                sqlQuery.AppendLine(" als.preIndNIndexC = idxpn.code  ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" ( ");
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		alp6.code ");
                sqlQuery.AppendLine(" 		,SUM(CASE WHEN als.tiedBalance <> 0 then 100 else alp6.repEBal END) as repEBal  ");
                sqlQuery.AppendLine(" 		,CASE WHEN als.RepEPerFlag <> 0 THEN ");
                sqlQuery.AppendLine(" 			SUM(alp6.RepEFlrs * alp6.repEPer * alp1.stBal) ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			SUM(alp6.RepEFlrs * alp6.RepEFreqBal) ");
                sqlQuery.AppendLine(" 		END / NULLIF(MAX(alp1.stBal), 0) as lifeTimeRatefloor ");
                sqlQuery.AppendLine(" 		,CASE WHEN als.RepEPerFlag <> 0 THEN ");
                sqlQuery.AppendLine(" 			SUM(alp6.RepECaps * alp6.repEPer * alp1.stBal) ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			SUM(alp6.RepECaps * alp6.RepEFreqBal) ");
                sqlQuery.AppendLine(" 		END / NULLIF(MAX(alp1.stBal), 0) as lifeTimeRateCap  ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 	(SELECT code, RepEBal, repecaps, RepEFlrs, repEPer, repEFreqBal FROM Basis_ALP6 WHERE simulationid = {0} AND scenario = '{1}' ) as alp6 ");
                sqlQuery.AppendLine(" 	INNER JOIN (SELECT code, repEPerFlag, tiedBalance FROM Basis_ALS WHERE simulationid = {0} and scenario = '{1}') as als ");
                sqlQuery.AppendLine("     ON als.code = alp6.code ");
                sqlQuery.AppendLine(" 	INNER JOIN (SELECT code, SUM(CASE WHEN month = '{3}' THEN  stBal ELSE 0 END) as stBal FROM Basis_ALP1 WHERE simulationid = {0} and scenario = '{1}' GROUP BY code) as alp1 ");
                sqlQuery.AppendLine(" 	ON alp1.code = als.code ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" 	GROUP BY alp6.code, als.RepEPerFlag  ");
                sqlQuery.AppendLine(" 	) as alp6 ON ");
                sqlQuery.AppendLine(" alb.code = alp6.code ");
                sqlQuery.AppendLine(" Left JOIN ");
                sqlQuery.AppendLine(" (SELECT  ");
                sqlQuery.AppendLine(" 	code ");
                sqlQuery.AppendLine(" 	,SUM(CASE WHEN month = '{3}' THEN stBal ELSE 0 END) as stBal ");
                sqlQuery.AppendLine(" 	,SUM(CASE WHEN month = '{3}' THEN stRate ELSE 0 END) as stRate ");
                sqlQuery.AppendLine(" 	,SUM(EndNBal) as endNewBal ");
                sqlQuery.AppendLine(" 	FROM Basis_ALP1 WHERE simulationid = {0} AND scenario = '{1}'  GROUP BY Code) as alp1 ON ");
                sqlQuery.AppendLine(" 	alp1.code = als.code  ");
                sqlQuery.AppendLine(" 	LEFT JOIN ");
                sqlQuery.AppendLine(" 	(SELECT code, rate FROM Basis_IndexP WHERE month = '{3}' AND simulationid = {0} and scenario = '{1}') as repERate ");
                sqlQuery.AppendLine(" 	ON repERate.code = als.RepEIndexC ");
                sqlQuery.AppendLine(" 	LEFT JOIN ");
                sqlQuery.AppendLine(" 	(SELECT code, rate FROM Basis_IndexP WHERE month = '{3}' AND simulationid = {0} and scenario = '{1}') as NewRate ");
                sqlQuery.AppendLine(" 	ON NewRate.code = als.NewIndexC ");
                sqlQuery.AppendLine(" 	LEFT JOIN ");
                sqlQuery.AppendLine(" 	(SELECT code, rate FROM Basis_IndexP WHERE month = '{3}' AND simulationid = {0} and scenario = '{1}') as repNRate ");
                sqlQuery.AppendLine(" 	ON repNRate.code = als.RepNIndexC  ");
                sqlQuery.AppendLine("    ");
                sqlQuery.AppendLine(" 	ORDER BY alb.IsAsset DESC, alb.Sequence  ");
                albRecs = Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), simulationid, scenario, curDate.ToShortDateString(), firstProj.ToShortDateString()));

                Dictionary<string, string> catsReported = new Dictionary<string, string>();

                DataTable projLabels = Utilities.ProjectionLabels(conStr, "BASIS_ALP1", simulationid, scenario, "", asOfDate);

                albRecs.PrimaryKey = new DataColumn[] { albRecs.Columns["code"] };

                
                relatTable.Columns.Add("name", typeof(string));
                relatTable.Columns.Add("rowType", typeof(int));

                //Starting COlumns
                relatTable.Columns.Add("balance", typeof(double));
                relatTable.Columns.Add("rate", typeof(double));
                relatTable.Columns.Add("accrual", typeof(string));

                //Existing Reprice Columns
                relatTable.Columns.Add("repriceIndex", typeof(string));
                relatTable.Columns.Add("repriceOffset", typeof(double));
                relatTable.Columns.Add("repriceRate", typeof(double));
                relatTable.Columns.Add("repriceFreq", typeof(double));
                relatTable.Columns.Add("repriceChgCap", typeof(double));
                relatTable.Columns.Add("repriceChgFloor", typeof(double));
                relatTable.Columns.Add("repriceLifeCap", typeof(double));
                relatTable.Columns.Add("repriceLifeFloor", typeof(double));

                //New Volume Columns
                relatTable.Columns.Add("newIndex", typeof(string));
                relatTable.Columns.Add("newOffset", typeof(double));
                relatTable.Columns.Add("newRate", typeof(double));

                //New Repricing 
                relatTable.Columns.Add("newRepricingIndex", typeof(string));
                relatTable.Columns.Add("newRepricingOffset", typeof(double));
                relatTable.Columns.Add("newRepricingRate", typeof(double));
                relatTable.Columns.Add("newRepricingFixedPer", typeof(string));
                relatTable.Columns.Add("newRepriceFreq", typeof(double));
                relatTable.Columns.Add("newRepriceChgCap", typeof(double));
                relatTable.Columns.Add("newRepriceChgFloor", typeof(double));
                relatTable.Columns.Add("newRepriceLifeCap", typeof(double));
                relatTable.Columns.Add("newRepriceLifeFloor", typeof(double));

                //New Cashflow
                relatTable.Columns.Add("newCashflowTerm", typeof(string));
                relatTable.Columns.Add("newCashflowAmount", typeof(double));

                //Preapay Type
                relatTable.Columns.Add("preapyExisting", typeof(string));
                relatTable.Columns.Add("prepayNew", typeof(string));

                //Balance Fields to Hide and Show Values
                relatTable.Columns.Add("newCashflowBalance", typeof(double));
                relatTable.Columns.Add("existingRepriceBalance", typeof(double));
                relatTable.Columns.Add("newRepricingBalance", typeof(double));
                relatTable.Columns.Add("balanceFound", typeof(int));
                relatTable.Columns.Add("isAsset", typeof(int));
                foreach (DataRow alb in albRecs.Rows) {

                    //If Already processed this code in another realtionship ignore
                    if (catsReported.ContainsKey(alb["code"].ToString())) {
                        continue;
                    }

                    DataRow relatRow = FindSourceRelationship(alb["code"].ToString(), simulationid, scenario, conStr);
                    if (alb["name"].ToString().ToLower().Contains("nmd migration")) {
                        for (int x = 1; x < 3; x++) {
                            Console.WriteLine(x);
                        }
                    }
                    if (relatRow == null) {

                        relatRow = FindTargetRelationship(alb["code"].ToString(), simulationid, scenario, conStr);

                        //If not in a source or target spit out category
                        if (relatRow == null) {
                            catsReported.Add(alb["code"].ToString(), "");
                            relatTable.Rows.Add(CategoryDetail(alb, relatTable.NewRow(), 0, simulationid, scenario, conStr, curDate.ToShortDateString()));
                        }
                    }
                    else {
                        //Add Relation Ship Record
                        string[] codes = relatRow["sourceCode"].ToString().Trim().Split(';');

                        bool reportIt = true;
                        foreach (string cd in codes) {
                            if (catsReported.ContainsKey(cd)) {
                                reportIt = false;
                            }
                        }

                        if (reportIt) {
                            //Add Relation Ship Heaser
                            DataRow outRow = relatTable.NewRow();
                            outRow["Name"] = relatRow["Description"];
                            outRow["rowType"] = 3;


                             if (relatRow["Description"].ToString().ToLower().Contains("nmd migration"))
                            {
                                for (int x = 1; x < 3; x++)
                                {
                                    Console.WriteLine(x);
                                }
                            }

                            relatTable.Rows.Add(outRow);
  
                            //Loop Through Each Code
                            foreach (string cd in codes) {
                                if (!catsReported.ContainsKey(cd.Trim())) {
                                    DataRow foundRow = albRecs.Rows.Find(cd.Trim());
                                    if (foundRow != null) {
                                        relatTable.Rows.Add(CategoryDetail(foundRow, relatTable.NewRow(), 1, simulationid, scenario, conStr, curDate.ToShortDateString()));
                                        catsReported.Add(cd.Trim(), "");
                                    }
                                }
                                else {

                                }

                            }


                            if (relatRow["Exempt"].ToString() == "0") {

                                //Get First What If Record
                                DataTable relat2Tbl = Utilities.ExecuteSql(conStr, "SELECT * FROM Basis_Relat2 WHERE simulationid = " + simulationid + " AND  code = '" + relatRow["relat2code"].ToString() + "' ORDER BY Sequence ");

                                foreach (DataRow relat2Row in relat2Tbl.Rows) {
                                    if (relat2Row["exemptSeq"].ToString() == "1") {
                                        continue;
                                    }

                                    DataRow relRow = alb;
                                    if (relat2Row["TargetType"].ToString() != "5") {
                                        relRow = albRecs.Rows.Find(relat2Row["targetCode"].ToString());
                                        //relRow["newCashflowTerm"] = GetNewCashflowFields(relRow["code"].ToString(), simulationid, scenario, conStr, relRow["matNSensi"].ToString());

                                    }


                                    DataRow foundRow = relRow;

                                    if (foundRow != null) {
                                        DataRow outRelatRow = relatTable.NewRow();


                                        string headerText = "";
                                        string tempText = "";
                                        switch (relat2Row["TransferOPtionType"].ToString()) {
                                            case "0":
                                                tempText = "Rem";
                                                break;
                                            case "1":
                                                tempText = relat2Row["TransferOptionValue"].ToString() + "%";
                                                break;
                                            case "2":
                                                tempText = "Period Cap " + Math.Round(double.Parse(relat2Row["TransferOptionValue"].ToString()) / 1000, 0).ToString();
                                                break;
                                            case "3":
                                                tempText = "Total Cap " + Math.Round(double.Parse(relat2Row["TransferOptionValue"].ToString()) / 1000, 0).ToString();
                                                break;
                                        }


                                        switch (relat2Row["PeriodStatus"].ToString()) {
                                            case "0":
                                                break;
                                            case "1":
                                                if (DateTime.Parse(relat2Row["SDate"].ToString()) < firstProj) {
                                                    headerText = "(" + firstProj.ToShortDateString();
                                                }
                                                else {
                                                    headerText = "(" + DateTime.Parse(relat2Row["SDate"].ToString()).ToString("MMM yy");
                                                }
                                                headerText = headerText + " - " + DateTime.Parse(relat2Row["EDate"].ToString()).ToString("MMM yy") + " Fixed)";
                                                break;
                                            case "2":
                                                headerText = "(" + curDate.AddMonths(int.Parse(relat2Row["SPeriod"].ToString())).ToString("MMM yy") + " - ";
                                                string endPeriod = "Y30";

                                                if (projLabels.Rows.Count > int.Parse(relat2Row["EPeriod"].ToString())) {
                                                    endPeriod = projLabels.Rows[int.Parse(relat2Row["EPeriod"].ToString())][1].ToString();
                                                }



                                                headerText += endPeriod + " Rolling)";
                                                break;

                                        }
                                        if (relat2Row["TargetType"].ToString() == "5") {
                                            headerText = headerText + " to Itself";
                                        }
                                        else {
                                            headerText = headerText + " to " + foundRow["Name"].ToString();
                                        }

                                        outRelatRow["Name"] = tempText + " " + headerText;
                                        string temp = "!@$!@$";
                                        if (outRelatRow["Name"].ToString() == "LIBOR Floor - Receive Variable (1M)")
                                        {
                                            string text = "123";
                                            string text23 = text.Substring(1);
                                            temp = text23;
                                        }
                                        outRelatRow["rowType"] = 2;
                                        outRelatRow["balance"] = foundRow["StartBal"];
                                        outRelatRow["rate"] = foundRow["stRate"];
                                        outRelatRow["accrual"] = foundRow["accrual"];

                                        //Existing Reprice Columns
                                        outRelatRow["repriceIndex"] = foundRow["repriceIndex"];
                                        outRelatRow["repriceOffset"] = foundRow["repriceOffset"];
                                        outRelatRow["repriceRate"] = foundRow["RepERate"];
                                        outRelatRow["repriceFreq"] = foundRow["RepEFreq"];
                                        outRelatRow["repriceChgCap"] = foundRow["repEChngCap"];
                                        outRelatRow["repriceChgFloor"] = foundRow["repEChngFlr"];
                                        outRelatRow["repriceLifeCap"] = foundRow["RepELifeCap"];
                                        outRelatRow["repriceLifeFloor"] = foundRow["RepEFloor"];

                                        //New Volume Columns
                                        outRelatRow["newIndex"] = foundRow["newIndex"];
                                        outRelatRow["newOffset"] = foundRow["newOffset"];
                                        outRelatRow["newRate"] = foundRow["newRate"];

                                        //New Repricing 
                                        outRelatRow["newRepricingIndex"] = foundRow["newRepriceIndex"];
                                        outRelatRow["newRepricingOffset"] = foundRow["newRepriceOffset"];
                                        outRelatRow["newRepricingRate"] = foundRow["newRepriceRate"];
                                        outRelatRow["newRepricingFixedPer"] = foundRow["newRepriceFixedPer"];
                                        outRelatRow["newRepriceFreq"] = foundRow["newRepriceFreq"];
                                        outRelatRow["newRepriceChgCap"] = foundRow["newRepriceCap"];
                                        outRelatRow["newRepriceChgFloor"] = foundRow["newRepriceFlr"];
                                        outRelatRow["newRepriceLifeCap"] = foundRow["newRepriceLifetimeCap"];
                                        outRelatRow["newRepriceLifeFloor"] = foundRow["newRepriceLifetimeFloor"];


                                        if (foundRow["newCashflowBal"].ToString() != "" && double.Parse(foundRow["newCashflowBal"].ToString()) != 0) {
                                            outRelatRow["newCashflowTerm"] = GetNewCashflowFields(foundRow["code"].ToString(), simulationid, scenario, conStr, foundRow["matNSensi"].ToString(), curDate.ToShortDateString());
                                            outRelatRow["newCashflowAmount"] = foundRow["newCashflowWAM"];
                                        }
                                        else {
                                            outRelatRow["newCashflowTerm"] = 0;
                                            outRelatRow["newCashflowAmount"] = 0;
                                        }
                                        //New Cashflow


                                        //Preapay Type
                                        outRelatRow["preapyExisting"] = foundRow["preEIndex"];
                                        outRelatRow["prepayNew"] = foundRow["preNIndex"];

                                        //Balance Fields
                                        outRelatRow["newCashflowBalance"] = foundRow["newCashflowBal"];
                                        outRelatRow["existingRepriceBalance"] = foundRow["repEBal"];
                                        outRelatRow["isAsset"] = alb["IsAsset"];

                                        //if we could not find it in alb just dont even add it
                                        if (alb["isAsset"].ToString() != "")
                                        {
                                            relatTable.Rows.Add(outRelatRow);
                                        }
                                            

                                        


                                    }





                                }
                            }

                        }




                    }

                }
            }          
            
                return new
                {
                    tbl = relatTable,
                    warnings = warnings,
                    errors = errors
                };
        }


        public DataRow CategoryDetail(DataRow sourceRow, DataRow outRow, int rowType, string simulaitonid, string scenario, string constr, string curDate)
        {
            outRow["name"] = sourceRow["Name"];
            outRow["rowType"] = rowType;
            //if (rowType != 1)
            //{
                //Starting COlumns

            if (sourceRow["startBal"].ToString() == "")
            {
                outRow["balanceFound"] = 0;
            }
            else
            {
                outRow["balanceFound"] = 1;
            }
                outRow["balance"] = sourceRow["StartBal"];
                outRow["rate"] = sourceRow["stRate"];
                outRow["accrual"] = sourceRow["accrual"];

                //Existing Reprice Columns
                outRow["repriceIndex"] = sourceRow["repriceIndex"];
                outRow["repriceOffset"] = sourceRow["repriceOffset"];
                outRow["repriceRate"] = sourceRow["RepERate"];
                outRow["repriceFreq"] = sourceRow["RepEFreq"];
                outRow["repriceChgCap"] = sourceRow["repEChngCap"];
                outRow["repriceChgFloor"] = sourceRow["repEChngFlr"];
                outRow["repriceLifeCap"] = sourceRow["RepELifeCap"];
                outRow["repriceLifeFloor"] = sourceRow["RepEFloor"];

                //New Volume Columns
                outRow["newIndex"] = sourceRow["newIndex"];
                outRow["newOffset"] = sourceRow["newOffset"];
                outRow["newRate"] = sourceRow["newRate"];

                //New Repricing 
                outRow["newRepricingIndex"] = sourceRow["newRepriceIndex"];
                outRow["newRepricingOffset"] = sourceRow["newRepriceOffset"];
                outRow["newRepricingRate"] = sourceRow["newRepriceRate"];
                outRow["newRepricingFixedPer"] = sourceRow["newRepriceFixedPer"];
                outRow["newRepriceFreq"] = sourceRow["newRepriceFreq"];
                outRow["newRepriceChgCap"] = sourceRow["newRepriceCap"];
                outRow["newRepriceChgFloor"] = sourceRow["newRepriceFlr"];
                outRow["newRepriceLifeCap"] = sourceRow["newRepriceLifetimeCap"];
                outRow["newRepriceLifeFloor"] = sourceRow["newRepriceLifetimeFloor"];


                if (sourceRow["newCashflowBal"].ToString() != "" && double.Parse(sourceRow["newCashflowBal"].ToString()) != 0)
                {
                    outRow["newCashflowTerm"] = GetNewCashflowFields(sourceRow["code"].ToString(), simulaitonid, scenario, constr, sourceRow["matNSensi"].ToString(), curDate);
                    outRow["newCashflowAmount"] = sourceRow["newCashflowWAM"];
                }
                else
                {
                    outRow["newCashflowTerm"] = 0;
                    outRow["newCashflowAmount"] = 0;
                }
                //New Cashflow


                //Preapay Type
                outRow["preapyExisting"] = sourceRow["preEIndex"];
                outRow["prepayNew"] = sourceRow["preNIndex"];


                //Balance Fields
                outRow["newCashflowBalance"] = sourceRow["newCashflowBal"];
                outRow["existingRepriceBalance"] = sourceRow["repEBal"];
                outRow["isAsset"] = sourceRow["isAsset"];

           // }
     
            return outRow;

        }

        public DataRow FindSourceRelationship(string code, string simulationid, string scenario, string conStr)
        {

            DataTable retTable = Utilities.ExecuteSql(conStr, string.Format("SELECT * FROM Basis_Relat WHERE (SourceCode like '{0}%' OR sourceCode like '% {0};%') AND simulationId = {1} AND scenario = '{2}' AND exempt = 0  AND Type  = '0' AND sourceType in (0,1) AND targetType in (0,1,5) AND sourceColumn in (0,1,2,3,8)", code, simulationid, scenario));
            if (retTable.Rows.Count > 0){
                return retTable.Rows[0];
            }
            else
            {
                return null;
            }

        }
       
        public DataRow FindTargetRelationship(string code, string simulationid, string scenario, string conStr)
        {

            DataTable retTable = Utilities.ExecuteSql(conStr, string.Format("SELECT * FROM Basis_Relat WHERE (SourceCode like '{0}%' OR sourceCode like '% {0}') AND simulationId = {1} AND scenario = '{2}' AND exempt = 0 AND sourceType in (0,1) AND sourceColumn in (0,1,2,3,8) AND targetType in (0,1,5) ", code, simulationid, scenario));
            if (retTable.Rows.Count > 0){
               DataRow row = retTable.Rows[0];
               if (row["type"].ToString() == "0" && row["TargetType"].ToString() == "5")
               {
                   if (row["SourceCode"].ToString().Contains(" " + code) || row["SourceCode"].ToString().StartsWith(code + ";"))
                   {
                       return row;
                   }
                   else
                   {
                       return null;
                   }
               }
               else
               {
                   DataTable ret2Table = Utilities.ExecuteSql(conStr, "SELECT * FROM Basis_Relat2 WHERE targetCode = '" + code + "'");
                   if (ret2Table.Rows.Count > 0)
                   {
                       return ret2Table.NewRow();                     
                   }
                   else
                   {
                       return null;
                   }
               }
            }
            else
            {
                DataTable ret2Table = Utilities.ExecuteSql(conStr, "SELECT * FROM Basis_Relat2 WHERE targetCode = '" + code + "' and simulationid = " + simulationid + "");
                if (ret2Table.Rows.Count > 0)
                {
                    return ret2Table.NewRow();                 
                }
                else
                {
                    return null;
                }
            }

        }

        public string GetNewCashflowFields(string code, string simulationid, string scenario, string constr, string matNSensi, string asOfDate)
        {

            if (code == "18072")
            {
                for (int i = 0; i < 2; i++)
                {
                    Console.WriteLine("asfasf");
                }
            }

            DataTable newTbl = Utilities.ExecuteSql(constr, "SELECT matNPer * 100 as matNPer, nmonths, DATEDIFF(m,'"  + asOfDate + "', month) - 1 + nMonths as numMonths FROM Basis_ALP9 WHERE SimulationId = " + simulationid + " AND scenario = '" + scenario + "' AND code = '" + code + "' ORDER BY CAST(month as Datetime)");



            double startMonth = 0;
            double numMonths = 0;
            int numOfPer = 0;
            double previousValue = 0;
            bool firstFlag = true;
            string termString = "";
            string firstMonth = "";
        
            for (int i = 0; i < newTbl.Rows.Count; i++)
            {
                bool process = false;
                bool secondProcess = false;
                DataRow dr = newTbl.Rows[i];
                int monthsInBucket = int.Parse(dr["nMonths"].ToString());
                int nMonths = int.Parse(dr["numMonths"].ToString());
                double value = Math.Round(double.Parse(dr["matNPer"].ToString()), 3);
                startMonth = nMonths;


              //  startMonth = numMonths + monthsInBucket;
                if (monthsInBucket > 1)
                {
                  
                    if (value != 0)
                    {
                        process = true;
                        numOfPer += 1;
                    }


                }
                else
                {
                    if (value != 0)
                    {
                        double nextValue = 0;
                        if (i + 1 < newTbl.Rows.Count)
                        {
                            nextValue  = Math.Round(double.Parse(newTbl.Rows[i+1]["matNPer"].ToString()), 3);
                        }

                        if (value == previousValue && value == nextValue)
                        {
                            continue;
                        }
                        else if (value == previousValue ){
                            numOfPer += 1;
                            secondProcess = true;
                            firstMonth = "";
                            previousValue = value;
                        }
                        else if (value == nextValue)
                        {
                            previousValue = value;
                            firstMonth = int.Parse(startMonth.ToString()).ToString();
                            previousValue = value;
                        }
                        else
                        {
                            process = true;
                            numOfPer += 1;
                        }

                    }
                    else
                    {
                        previousValue = 0;
                    }
                }


                if (process)
                {
                    if (firstFlag){
                        firstFlag  = false;
                          termString = (value).ToString() + "% " + (startMonth).ToString();
                    }
                    else{
                        termString = ";" + (value).ToString() + "% " + (startMonth).ToString();
                    }
                }
                if (secondProcess)
                {
                    if (firstFlag)
                    {
                        firstFlag = false;
                        termString = (value).ToString() + "% " + firstMonth + "-" + startMonth.ToString();
                    }
                    else
                    {
                        termString = ";" + (value).ToString() + "% " + firstMonth + "-" + startMonth.ToString();
                    }
                }
                numMonths = numMonths + monthsInBucket;
            }


            if (numOfPer > 4)
            {
                numMonths = 0;
                double totalPer = 0;
                double wghtTerm = 0;
                
                foreach (DataRow dr in newTbl.Rows)
                {

                    double monthsInBucket = double.Parse(dr["nMonths"].ToString());
                    switch (matNSensi)
                    {
                        case "0":
                            startMonth = numMonths;

                            break;
                        case "1":
                            startMonth = numMonths + (monthsInBucket / 2);
                            break;
                        case "2":
                            startMonth = numMonths + monthsInBucket;
                            break;
                    }
                    double value = Math.Round(double.Parse(dr["matNPer"].ToString()), 3);
                    if (value != 0)
                    {
                     


                        totalPer = totalPer + value;
                        wghtTerm = wghtTerm + (startMonth * value);
                    }
                    numMonths = numMonths + monthsInBucket;
                }


                double weightTerm = 0;
                if (totalPer == 0)
                {
                    weightTerm = 0;
                }
                else
                {
                    weightTerm = wghtTerm / totalPer;

                }
                return Math.Round(wghtTerm, 1).ToString() + "m";
            }
            else
            {

                if (termString == "")
                {
                    return "--";
                }
                else
                {
                    return termString;
                }

            }

        }

    }


}