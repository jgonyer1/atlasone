﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using Atlas.Institution.Data;
using System.Text;
using System.Data;

namespace Atlas.Web.Services
{
    public class LoanCapFloorService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object GetConsolidatedLoanCapFloorData(int lcfId)
        {
            InstitutionContext _gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            var lcf = _gctx.LoanCapFloors.Where(s => s.Id == lcfId).FirstOrDefault();
            var rep = _gctx.Reports.Where(s => s.Id == lcfId).FirstOrDefault();

            string fieldToUse = "lifetimeratecap";
            if (lcf.Type == 2)
            {
                fieldToUse = "lifetimerateflr";
            }

            var simType = new SimulationType();
            simType.Id = 1;

            DataTable topDT = new DataTable();
            DataTable bottomDT = new DataTable();

            var topInstSerice = new InstitutionService(lcf.TopInstitutionDatabaseName);
            var bottomInstSerice = new InstitutionService(lcf.BottomInstitutionDatabaseName);
            var topSim = topInstSerice.GetSimulation(rep.Package.AsOfDate, lcf.TopAsOfDateOffset, simType);
            var bottomSim = bottomInstSerice.GetSimulation(rep.Package.AsOfDate, lcf.BottomAsOfDateOffset, simType);
            string topTitle = "";
            string bottomTitle = "";
            string diffTitle = "";

            if (topSim == null)
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), "Base", lcf.TopAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else if (bottomSim == null)
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), "Base", lcf.BottomAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else
            {
                string[] endPoints = lcf.BpEndpoints.Split(new Char[] { ',' });
                topDT = LoanCapFloorTable(lcf.Mode, topSim.Id, lcf.TopInstitutionDatabaseName, fieldToUse, endPoints);
                bottomDT = LoanCapFloorTable(lcf.Mode, bottomSim.Id, lcf.BottomInstitutionDatabaseName, fieldToUse, endPoints);

                string topInstName = "";
                string bottomInstName = "";


                if (lcf.TopInstitutionDatabaseName != lcf.BottomInstitutionDatabaseName)
                {
                    topInstName = Utilities.InstName(lcf.TopInstitutionDatabaseName);
                    bottomInstName = Utilities.InstName(lcf.BottomInstitutionDatabaseName);
                }


                topTitle = topInstName + " " + topSim.Information.AsOfDate.ToShortDateString();
                bottomTitle = bottomInstName + " " + bottomSim.Information.AsOfDate.ToShortDateString();


                DateTime oldDate = new DateTime(2002, 7, 15);
                DateTime newDate = DateTime.Now;

                // Difference in days, hours, and minutes.
                int monthDiff = ((topSim.Information.AsOfDate.Year - bottomSim.Information.AsOfDate.Year) * 12) + topSim.Information.AsOfDate.Month - bottomSim.Information.AsOfDate.Month;

                diffTitle = monthDiff.ToString() + "M Variance";
            }


            return new
            {
                topTable = topDT,
                bottomTable = bottomDT,
                topTitle = topTitle,
                bottomTitle = bottomTitle,
                diffTitle = diffTitle,
                errors = errors,
                warnings = warnings
            };
        }

        private DataTable LoanCapFloorTable(int mode, int simulation, string database, string fieldToUse, string[] breakOuts)
        {

            string effect = "capEffect";
            if (fieldToUse == "lifetimerateflr")
            {
                effect = "flrEffect";
            }
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT  ");
            sqlQuery.AppendLine(" 		name as indexName  ");
            sqlQuery.AppendLine(" 		,colNumber  ");
            sqlQuery.AppendLine(" 		,floatAdjust  ");
            sqlQuery.AppendLine(" 		,SUM(balanceOwned) as balance  ");
            sqlQuery.AppendLine("		,AVG(RepIndexRate) as rate ");
            sqlQuery.AppendLine(" 		,sequence  ");
            sqlQuery.AppendLine(" 	  ");
            sqlQuery.AppendLine(" 	FROM  ");
            sqlQuery.AppendLine(" 	(SELECT   ");
            sqlQuery.AppendLine(" 		{0}   ");
            sqlQuery.AppendLine(" 		,balanceOwned  ");
            sqlQuery.AppendLine(" 		,blb.name  ");
            sqlQuery.AppendLine("		,bla.RepIndexRate  ");
            sqlQuery.AppendLine("		,blb.Sequence  ");
            sqlQuery.AppendLine(" 	   ---Changes based off of if report or reconcillation ");

            //Reconcoialtion
            if (mode == 2)
            {
                sqlQuery.AppendLine(" 	   ,CASE   ");
                sqlQuery.AppendLine(" 			WHEN IntRateChngfrq <= 6  THEN  ");
                sqlQuery.AppendLine(" 				'Floating'  ");
                sqlQuery.AppendLine(" 			WHEN IntRateChngfrq > 6 THEN  ");
                sqlQuery.AppendLine(" 				'Adjustable'  ");
                sqlQuery.AppendLine(" 		END as floatAdjust  ");
            }
            else
            {
                sqlQuery.AppendLine(" 	   ,CASE   ");
                sqlQuery.AppendLine(" 			WHEN (IntRateChngfrq = 1 OR IntRateChngfrq = 3 OR IntRateChngfrq = 6) AND DATEDIFF(month, asOfDate, nxtRepriceDate) <= 6 THEN  ");
                sqlQuery.AppendLine(" 				'Floating'  ");
                sqlQuery.AppendLine(" 			WHEN IntRateChngfrq > 6 AND DATEDIFF(month, asOfDate, nxtRepriceDate) <= 12 THEN  ");
                sqlQuery.AppendLine(" 				'Adjustable'  ");
                sqlQuery.AppendLine(" 		END as floatAdjust  ");
            }


            sqlQuery.AppendLine(" 		,CASE   ");

            if (fieldToUse == "lifetimeratecap")
            {
                sqlQuery.AppendLine(" 			WHEN lifeTimeRateCap > 25 THEN  ");
                sqlQuery.AppendLine(" 				14  ");
            }

            sqlQuery.AppendLine(" 			WHEN {0} * 100 = 0 THEN  ");
            sqlQuery.AppendLine(String.Format("                   CASE WHEN  {0} * 100 <> 0 THEN", fieldToUse));
            sqlQuery.AppendLine("                                                 7");
            sqlQuery.AppendLine("                                      ELSE 14 END");
            sqlQuery.AppendLine(" 			WHEN {0}  * 100 > 0 THEN  ");
            sqlQuery.AppendLine(" 				CASE  ");
            for (int i = breakOuts.Length - 1; i >= 0; i--)
            {
                sqlQuery.AppendLine(String.Format(" WHEN ABS({0} * 100) > {1} THEN  ", effect, breakOuts[i]));
                sqlQuery.AppendLine((breakOuts.Length - i).ToString());
            }
            sqlQuery.AppendLine(" WHEN ABS({0}  * 100) > 0 THEN ");
            sqlQuery.AppendLine(" 						5 ");
            sqlQuery.AppendLine(" 				END  ");
            sqlQuery.AppendLine(" 			ELSE  ");
            sqlQuery.AppendLine(" 				CASE  ");
            for (int i = breakOuts.Length - 1; i >= 0; i--)
            {
                sqlQuery.AppendLine(String.Format(" WHEN ABS({0} * 100) > {1}  THEN  ", effect, breakOuts[i]));
                sqlQuery.AppendLine((1 + i + 8).ToString());
            }
            sqlQuery.AppendLine(" WHEN ABS({0} * 100) > 0 THEN ");
            sqlQuery.AppendLine(" 						8 ");
            sqlQuery.AppendLine(" 				END  ");
            sqlQuery.AppendLine(" 			END as colNumber  ");
            sqlQuery.AppendLine("   ");
            sqlQuery.AppendLine("   FROM [Basis_LoanA]  as bla  ");
            sqlQuery.AppendLine("   INNER JOIN Basis_IndexB as blb  ");
            sqlQuery.AppendLine("   ON bla.RepIndexCode = blb.Code AND bla.SimulationId = blb.SimulationId   ");
            sqlQuery.AppendLine("   WHERE bla.simulationid = " + simulation.ToString() + " and bla.NonAccrual <> -1 ");
            //if (mode == 2)
            //{
            //    sqlQuery.AppendLine(" AND bla.IntRateChngfrq <= 12 ");
            //}
            //else
            //{
            //    sqlQuery.AppendLine(" AND bla.IntRateChngfrq <= 12 AND bla.nxtRepriceDate <= DATEADD(m, 12, asOfDate)");
            //}

            sqlQuery.AppendLine("    ) as s   ");
            sqlQuery.AppendLine("   WHERE floatAdjust = 'Adjustable' or floatAdjust  = 'Floating'  ");
            sqlQuery.AppendLine("   GROUP by name, Sequence, colNumber, floatAdjust  ");
            sqlQuery.AppendLine("   ORDER BY Sequence");

            return Utilities.ExecuteSql(Utilities.BuildInstConnectionString(database), String.Format(sqlQuery.ToString(), effect));
        }
    }
}