﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Atlas.Web.Services
{
    public class YieldCurveShiftService
    {
       

        public object YieldCurveShiftViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var ycs = gctx.YieldCurveShifts.Where(s => s.Id == reportId).FirstOrDefault();

            //If Not Found Kick Out
            if (ycs == null) throw new Exception("Yield Curve Shift Not Found");
            //Get Web Rates Connection String
            string conStr = System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString;
            string fedConStr = System.Configuration.ConfigurationManager.ConnectionStrings["DDwConnection"].ConnectionString;

            //Need to build in statement as well as order case statement becuase we cann not join to cofig table becuase on another server and we are not linking them
            string scenarioInStatement = "(";
            string caseStatementOrder = " CASE ";
            foreach (var s in ycs.YieldCurveShiftScenarioTypes.OrderBy(z=> z.Priority))
            {
                if (scenarioInStatement == "(")
                {
                    scenarioInStatement += "'" + s.WebRateScenarioType.ScenNumber + "'";             
                }
                else
                {
                    scenarioInStatement += ",'" + s.WebRateScenarioType.ScenNumber + "'";
                }

                //Build out case statement
                caseStatementOrder += " WHEN si.name = '" + s.WebRateScenarioType.Name + "' THEN " + s.Priority;                     
            }
            scenarioInStatement += ")";

            caseStatementOrder += " END ";


            if (scenarioInStatement == "()")
            {
                return null;
            }

            DateTime[] historicDates = new DateTime[ycs.YieldCurveShiftHistoricDates.Count];


            //if not overriding liabprice settings go get thema nd update just in case
            if (!ycs.OverrideDistrict || !ycs.OverrideWebRateDate)
            {


                DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
                LiabilityPricing lb = gctx.LiabilityPricings.Where(s => s.AsOfDate == date).FirstOrDefault();

                if (!ycs.OverrideDistrict)
                {
                    ycs.FHLBAdvanceDistrict = lb.FHLBDistrict;
                }
                if (!ycs.OverrideWebRateDate)
                {
                    ycs.WebRateAsOfDate = lb.WebRateAsOfDate;
                }
               
               
                gctx.SaveChanges();
            }

            string dateClause = "('" + ycs.WebRateAsOfDate + "'";
            int dateCounter = 0;
            foreach (var s in ycs.YieldCurveShiftHistoricDates.OrderBy(s=>s.Priority))
            {
                //If current set to the most recent web rate date
                if (s.WebRateAsOfDate == "Current")
                {
                    DataTable maxDate = Utilities.ExecuteSql(conStr, "SELECT CONVERT(VARCHAR(10), MAX(asOfDate), 101) as asOfDate  FROM [Webrates].[dbo].[RateBaseData] ");
                    if (maxDate.Rows.Count > 0)
                    {
                        s.WebRateAsOfDate = maxDate.Rows[0][0].ToString();
                    }
                    else
                    {
                        s.WebRateAsOfDate = DateTime.Now.ToShortDateString();
                    }
                }



                historicDates[dateCounter] = DateTime.Parse(s.WebRateAsOfDate.ToString());
                dateClause += ",'" + s.WebRateAsOfDate + "'";
   
                //Build out case statement
                dateCounter += 1;
            }

            dateClause += ")";

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	rbd.asOfDate ");
            sqlQuery.AppendLine(" 	,si.Name as scenName ");
            sqlQuery.AppendLine(" 	,rf.Name as rateName ");
            sqlQuery.AppendLine(" 	,rbd.iRAte ");
            sqlQuery.AppendLine(" 	,rbd.iRAte + ISNULL(sm.AdjustAmount, 0) as rate ");
            sqlQuery.AppendLine(" 	FROM ");
            sqlQuery.AppendLine(" 	(SELECT ");
            sqlQuery.AppendLine(" 		asOfDate ");
            sqlQuery.AppendLine(" 		,scenNumber ");
            sqlQuery.AppendLine(" 		,rateCode ");
            sqlQuery.AppendLine(" 		,SUM(AdjAmount) as AdjustAmount ");
            sqlQuery.AppendLine(" 		FROM [RateScenData] ");
            sqlQuery.AppendLine(" 		WHERE asOfDate in " + dateClause + " ");
            sqlQuery.AppendLine(" 		GROUP BY asOfDate, ScenNumber, rateCode) as sm ");
            sqlQuery.AppendLine(" RIGHT JOIN ");
            sqlQuery.AppendLine(" 		( ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			rbd.irate, ");
            sqlQuery.AppendLine(" 			rbd.AsOfDate, ");
            sqlQuery.AppendLine(" 			s.ScenNumber, ");
            sqlQuery.AppendLine(" 			rbd.RateCode		 ");
            sqlQuery.AppendLine(" 			 FROM RateBaseData as rbd  ");
            sqlQuery.AppendLine(" 			INNER JOIN ScenInfo as s ON ");
            sqlQuery.AppendLine(" 			1 = 1 ");
            sqlQuery.AppendLine(" 			WHERE  asOfDate in  " + dateClause + "  ");
            sqlQuery.AppendLine(" ");
            sqlQuery.AppendLine(" ) as rbd ");
            sqlQuery.AppendLine(" ON rbd.rateCode = sm.rateCode  AND sm.asOfDate = rbd.asOfDate and rbd.ScenNumber = sm.scenNumber ");

            sqlQuery.AppendLine(" 		INNER JOIN ScenInfo as si ON ");
            sqlQuery.AppendLine(" 		si.scenNumber = rbd.scenNumber ");
            sqlQuery.AppendLine(" 		INNER JOIN RateInfo as rf ON ");
            sqlQuery.AppendLine(" 		rf.code = rbd.rateCode  ");
            sqlQuery.AppendLine(" 		WHERE  rf.name in  (");
            switch (ycs.Index){
                case "LIBOR":
                        sqlQuery.AppendLine("   '(006) LIBOR 1 Month', ");
                        sqlQuery.AppendLine("   '(007) LIBOR 3 Month', ");
                        sqlQuery.AppendLine("   '(008) LIBOR 6 Month', ");
                        sqlQuery.AppendLine("   '(253) Libor 1 Year', ");
                        sqlQuery.AppendLine("   '(171) Libor 2 Year',  ");
                        sqlQuery.AppendLine("   '(172) Libor 3 Year' ,  ");
                        sqlQuery.AppendLine("   '(174) Libor 5 Year',  ");
                        sqlQuery.AppendLine("   '(177) Libor 7 Year',  ");
                        sqlQuery.AppendLine("   '(178) Libor 10 Year',  ");
                        sqlQuery.AppendLine("   '(207) Libor 30 Year' ");

                    break;
                case "FHLB":
                    switch (ycs.FHLBAdvanceDistrict){
                        case "Atlanta":
                            sqlQuery.AppendLine(" '(049) FHLB Atlanta 1 Month', ");
                            sqlQuery.AppendLine(" '(051) FHLB Atlanta 3 Month', ");
                            sqlQuery.AppendLine(" '(052) FHLB Atlanta 6 Month', ");
                            sqlQuery.AppendLine(" '(053) FHLB Atlanta 1 Year', ");
                            sqlQuery.AppendLine(" '(054) FHLB Atlanta 2 Year', ");
                            sqlQuery.AppendLine(" '(055) FHLB Atlanta 3 Year', ");
                            sqlQuery.AppendLine(" '(057) FHLB Atlanta 5 Year', ");
                            sqlQuery.AppendLine(" '(183) FHLB Atlanta 7 Year', ");
                            sqlQuery.AppendLine(" '(058) FHLB Atlanta 10 Year' ");
                            break;
                        case "Boston":

                            
                            sqlQuery.AppendLine(" '(025) FHLB Boston 1 Month', ");
                            sqlQuery.AppendLine(" '(027) FHLB Boston 3 Month', ");
                            sqlQuery.AppendLine(" '(028) FHLB Boston 6 Month', ");
                            sqlQuery.AppendLine(" '(029) FHLB Boston 1 Year', ");
                            sqlQuery.AppendLine(" '(030) FHLB Boston 2 Year', ");
                            sqlQuery.AppendLine(" '(031) FHLB Boston 3 Year', ");
                            sqlQuery.AppendLine(" '(033) FHLB Boston 5 Year', ");
                            sqlQuery.AppendLine(" '(179) FHLB Boston 7 Year', ");
                            sqlQuery.AppendLine(" '(047) FHLB Boston 10 Year' ");
                            break;
                        case "Chicago":      
                            sqlQuery.AppendLine(" '(060) FHLB Chicago 1 Month', ");
                            sqlQuery.AppendLine(" '(062) FHLB Chicago 3 Month', ");
                            sqlQuery.AppendLine(" '(063) FHLB Chicago 6 Month', ");
                            sqlQuery.AppendLine(" '(064) FHLB Chicago 1 Year', ");
                            sqlQuery.AppendLine(" '(065) FHLB Chicago 2 Year', ");
                            sqlQuery.AppendLine(" '(066) FHLB Chicago 3 Year', ");
                            sqlQuery.AppendLine(" '(068) FHLB Chicago 5 Year', ");
                            sqlQuery.AppendLine(" '(184) FHLB Chicago 7 Year', ");
                            sqlQuery.AppendLine(" '(069) FHLB Chicago 10 Year', ");
                            sqlQuery.AppendLine(" '(249) FHLB Chicago 30 Year' ");
                            break;
                        case "Cincinnati":
                            sqlQuery.AppendLine(" '(071) FHLB Cincinnati 1 Month', ");
                            sqlQuery.AppendLine(" '(073) FHLB Cincinnati 3 Month', ");
                            sqlQuery.AppendLine(" '(074) FHLB Cincinnati 6 Month', ");
                            sqlQuery.AppendLine(" '(075) FHLB Cincinnati 1 Year', ");
                            sqlQuery.AppendLine(" '(076) FHLB Cincinnati 2 Year', ");
                            sqlQuery.AppendLine(" '(077) FHLB Cincinnati 3 Year', ");
                            sqlQuery.AppendLine(" '(079) FHLB Cincinnati 5 Year', ");
                            sqlQuery.AppendLine(" '(185) FHLB Cincinnati 7 Year', ");
                            sqlQuery.AppendLine(" '(080) FHLB Cincinnati 10 Year' ");
                            break;
                        case "Dallas":
                            sqlQuery.AppendLine(" '(082) FHLB Dallas 1 Month', ");
                            sqlQuery.AppendLine(" '(084) FHLB Dallas 3 Month', ");
                            sqlQuery.AppendLine(" '(085) FHLB Dallas 6 Month', ");
                            sqlQuery.AppendLine(" '(086) FHLB Dallas 1 Year', ");
                            sqlQuery.AppendLine(" '(087) FHLB Dallas 2 Year', ");
                            sqlQuery.AppendLine(" '(088) FHLB Dallas 3 Year', ");
                            sqlQuery.AppendLine(" '(090) FHLB Dallas 5 Year', ");
                            sqlQuery.AppendLine(" '(188) FHLB Dallas 7 Year', ");
                            sqlQuery.AppendLine(" '(091) FHLB Dallas 10 Year' ");
                            break;
                        case "Des Moines":
                            sqlQuery.AppendLine(" '(093) FHLB Des Moines 1 Month', ");
                            sqlQuery.AppendLine(" '(095) FHLB Des Moines 3 Month', ");
                            sqlQuery.AppendLine(" '(096) FHLB Des Moines 6 Month', ");
                            sqlQuery.AppendLine(" '(097) FHLB Des Moines 1 Year', ");
                            sqlQuery.AppendLine(" '(098) FHLB Des Moines 2 Year', ");
                            sqlQuery.AppendLine(" '(099) FHLB Des Moines 3 Year', ");
                            sqlQuery.AppendLine(" '(100) FHLB Des Moines 5 Year', ");
                            sqlQuery.AppendLine(" '(191) FHLB Des Moines 7 Year', ");
                            sqlQuery.AppendLine(" '(101) FHLB Des Moines 10 Year' ");
                            break;
                        case "Indianapolis":
                            sqlQuery.AppendLine(" '(103) FHLB Indianapolis 1 Month', ");
                            sqlQuery.AppendLine(" '(105) FHLB Indianapolis 3 Month', ");
                            sqlQuery.AppendLine(" '(106) FHLB Indianapolis 6 Month', ");
                            sqlQuery.AppendLine(" '(107) FHLB Indianapolis 1 Year', ");
                            sqlQuery.AppendLine(" '(108) FHLB Indianapolis 2 Year', ");
                            sqlQuery.AppendLine(" '(109) FHLB Indianapolis 3 Year', ");
                            sqlQuery.AppendLine(" '(111) FHLB Indianapolis 5 Year', ");
                            sqlQuery.AppendLine(" '(192) FHLB Indianapolis 7 Year', ");
                            sqlQuery.AppendLine(" '(112) FHLB Indianapolis 10 Year' ");
                            break;
                        case "New York":   
                            sqlQuery.AppendLine(" '(035) FHLB NY 1 Month', ");
                            sqlQuery.AppendLine(" '(037) FHLB NY 3 Month', ");
                            sqlQuery.AppendLine(" '(038) FHLB NY 6 Month', ");
                            sqlQuery.AppendLine(" '(039) FHLB NY 1 Year', ");
                            sqlQuery.AppendLine(" '(040) FHLB NY 2 Year', ");
                            sqlQuery.AppendLine(" '(041) FHLB NY 3 Year', ");
                            sqlQuery.AppendLine(" '(043) FHLB NY 5 Year', ");
                            sqlQuery.AppendLine(" '(182) FHLB NY 7 Year', ");
                            sqlQuery.AppendLine(" '(046) FHLB NY 10 Year' ");
                            break;
                        case "Pittsburgh":
                            sqlQuery.AppendLine(" '(114) FHLB Pittsburgh 1 Month', ");
                            sqlQuery.AppendLine(" '(116) FHLB Pittsburgh 3 Month', ");
                            sqlQuery.AppendLine(" '(117) FHLB Pittsburgh 6 Month', ");
                            sqlQuery.AppendLine(" '(118) FHLB Pittsburgh 1 Year', ");
                            sqlQuery.AppendLine(" '(119) FHLB Pittsburgh 2 Year', ");
                            sqlQuery.AppendLine(" '(120) FHLB Pittsburgh 3 Year', ");
                            sqlQuery.AppendLine(" '(122) FHLB Pittsburgh 5 Year', ");
                            sqlQuery.AppendLine(" '(193) FHLB Pittsburgh 7 Year', ");
                            sqlQuery.AppendLine(" '(123) FHLB Pittsburgh 10 Year' ");
                            break;
                        case "San Francisco":
                            sqlQuery.AppendLine(" '(125) FHLB San Francisco 1 Month', ");
                            sqlQuery.AppendLine(" '(127) FHLB San Francisco 3 Month', ");
                            sqlQuery.AppendLine(" '(128) FHLB San Francisco 6 Month', ");
                            sqlQuery.AppendLine(" '(129) FHLB San Francisco 1 Year', ");
                            sqlQuery.AppendLine(" '(130) FHLB San Francisco 2 Year', ");
                            sqlQuery.AppendLine(" '(131) FHLB San Francisco 3 Year', ");
                            sqlQuery.AppendLine(" '(132) FHLB San Francisco 5 Year', ");
                            sqlQuery.AppendLine(" '(196) FHLB San Francisco 7 Year', ");
                            sqlQuery.AppendLine(" '(133) FHLB San Francisco 10 Year', ");
                            sqlQuery.AppendLine(" '(199) FHLB San Francisco 30 Year' ");
                            break;
                        case "Seattle":    
                            sqlQuery.AppendLine(" '(135) FHLB Seattle 1 Month', ");
                            sqlQuery.AppendLine(" '(137) FHLB Seattle 3 Month', ");
                            sqlQuery.AppendLine(" '(138) FHLB Seattle 6 Month', ");
                            sqlQuery.AppendLine(" '(139) FHLB Seattle 1 Year', ");
                            sqlQuery.AppendLine(" '(140) FHLB Seattle 2 Year', ");
                            sqlQuery.AppendLine(" '(141) FHLB Seattle 3 Year', ");
                            sqlQuery.AppendLine(" '(143) FHLB Seattle 5 Year', ");
                            sqlQuery.AppendLine(" '(200) FHLB Seattle 7 Year', ");
                            sqlQuery.AppendLine(" '(144) FHLB Seattle 10 Year', ");
                            sqlQuery.AppendLine(" '(203) FHLB Seattle 30 Year' ");
                            break;
                        case "Topeka":
                            sqlQuery.AppendLine(" '(146) FHLB Topeka 1 Month', ");
                            sqlQuery.AppendLine(" '(148) FHLB Topeka 3 Month', ");
                            sqlQuery.AppendLine(" '(149) FHLB Topeka 6 Month', ");
                            sqlQuery.AppendLine(" '(150) FHLB Topeka 1 Year', ");
                            sqlQuery.AppendLine(" '(151) FHLB Topeka 2 Year', ");
                            sqlQuery.AppendLine(" '(152) FHLB Topeka 3 Year', ");
                            sqlQuery.AppendLine(" '(154) FHLB Topeka 5 Year', ");
                            sqlQuery.AppendLine(" '(204) FHLB Topeka 7 Year', ");
                            sqlQuery.AppendLine(" '(155) FHLB Topeka 10 Year' ");
                            break;
                    }
                    break;
                case "Treasury":
                        sqlQuery.AppendLine("   '(010) CMT 1 Month', ");
                        sqlQuery.AppendLine("   '(011) CMT 3 Month', ");
                        sqlQuery.AppendLine("   '(012) CMT 6 Month', ");
                        sqlQuery.AppendLine("   '(013) CMT 1 Year', ");
                        sqlQuery.AppendLine("   '(014) CMT 2 Year',  ");
                        sqlQuery.AppendLine("   '(015) CMT 3 Year' ,  ");
                        sqlQuery.AppendLine("   '(016) CMT 5 Year',  ");
                        sqlQuery.AppendLine("   '(017) CMT 7 Year',  ");
                        sqlQuery.AppendLine("   '(018) CMT 10 Year',  ");
                        sqlQuery.AppendLine("   '(208) CMT 30 Year' ");
                    break;
            }

            sqlQuery.AppendLine(" )");

            sqlQuery.AppendLine(" 	 AND rbd.scenNumber in " + scenarioInStatement  + " ");
            sqlQuery.AppendLine(" 	  ");
            sqlQuery.AppendLine(" 	  ");
            sqlQuery.AppendLine(" 	ORDER BY rbd.asOfDate, " + caseStatementOrder + ", rf.name ");



            //Get Web Rates Connection String
            DataTable retTable = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(conStr))
            {
                SqlCommand sqlCmd = new SqlCommand(sqlQuery.ToString(), sqlCon);
                try
                {
                    sqlCon.Open();
                    retTable.Load(sqlCmd.ExecuteReader());
                    sqlCon.Close();
                }
                catch (Exception ex)
                {
                    sqlCon.Close();
                    throw ex;
                }
            }



            //var scens = ycs.YieldCurveShiftScenarioTypes.OrderBy(s => s.Priority).ToArray();

            string[] scens = new string[ycs.YieldCurveShiftScenarioTypes.Count];

            int counter =0 ;
            foreach (YieldCurveShiftScenarioType s in  ycs.YieldCurveShiftScenarioTypes.OrderBy(s => s.Priority)){
                scens[counter] = s.WebRateScenarioType.Name;
                counter += 1;
            }

            //sqlQuery.Clear();
            //sqlQuery.AppendLine("select IndexB.RateDate, ISNULL(IndexB.Rate, 0) - ISNULL(IndexA.Rate, 0) as Rate from DWR_FedRates.dbo.FedRates as IndexB");
            //sqlQuery.AppendLine("full outer join DWR_FedRates.dbo.FedRates as IndexA on IndexA.RateDate = IndexB.RateDate");
            //sqlQuery.AppendLine("where IndexB.RateDate >= '" + ycs.CompareStartDate.ToShortDateString() + "' and IndexB.RateDate <= '" + ycs.CompareEndDate.ToShortDateString() + "'");
            //sqlQuery.AppendLine("and IndexB.IndexCode = '{0}' and IndexA.IndexCode = '{1}'");
            //sqlQuery.AppendLine("order by IndexB.RateDate");
            ////get the compare series
            //DataTable[] compares = new DataTable[ycs.YieldCurveShiftPeriodCompares.Count];

            //int cCount = 0;
            //foreach (YieldCurveShiftPeriodCompare s in ycs.YieldCurveShiftPeriodCompares.OrderBy(s => s.Priority))
            //{
            //    string qry = String.Format(sqlQuery.ToString(), s.IndexB, s.IndexA);
            //    compares[cCount] = Utilities.ExecuteSql(fedConStr, String.Format(qry, s.IndexB));
            //    cCount++;
            //}
            //object[] comps = new object[ycs.YieldCurveShiftPeriodCompares.Count];
            //counter = 0;
            //foreach (YieldCurveShiftPeriodCompare s in ycs.YieldCurveShiftPeriodCompares.OrderBy(s => s.Priority))
            //{
            //    comps[counter] = new { 
            //        Period1 = s.Period1,
            //        Period2 = s.Period2
            //    };
            //    counter += 1;
            //}

            return new {
                rateData = retTable,
                rateDate = ycs.WebRateAsOfDate,
                historicDates = historicDates,
                scens = scens,
               // comps = compares,
                index = ycs.Index,
                fhlb = ycs.FHLBAdvanceDistrict,
                ycs = ycs
            };
        }

    }
}