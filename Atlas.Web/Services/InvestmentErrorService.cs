﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class InvestmentErrorService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object InvestmentErrorViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Investment Errors
            var sr = gctx.InvestmentErrors.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("Investment Error Not Found");



            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            var info = instService.GetInformation(rep.Package.AsOfDate, sr.AsOfDateOffset);
            string asOfDate = info.AsOfDate.ToShortDateString();
         
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	cusip  ");
            sqlQuery.AppendLine(" 	,ParValue  ");
            sqlQuery.AppendLine(" 	,BookValue  ");
            sqlQuery.AppendLine(" 	,BookPrice  ");
            sqlQuery.AppendLine(" 	,MarketPrice  ");
            sqlQuery.AppendLine(" 	,ISNULL(LEFT(BasisIntent,3), '') as basisIntent ");
            sqlQuery.AppendLine(" 	,yield  ");
            sqlQuery.AppendLine(" 	,CurrentCoupon  ");
            sqlQuery.AppendLine(" 	,MaturityDate   ");
            sqlQuery.AppendLine(" 	,CASE	  ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType in ('Adjustable', 'A') THEN  ");
            sqlQuery.AppendLine(" 			'Adjustable'  ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType in ('COMP', 'Complex') THEN  ");
            sqlQuery.AppendLine(" 			'Complex'  ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('Fixed', 'F') THEN  ");
            sqlQuery.AppendLine(" 			'Fixed'  ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('None', 'NONE') THEN  ");
            sqlQuery.AppendLine(" 			'None'  ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('Step Up', 'SU') THEN   ");
            sqlQuery.AppendLine(" 			'Step Up'  ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('Tax Credit', 'TC') THEN  ");
            sqlQuery.AppendLine(" 			'Tax Credit'  ");
            sqlQuery.AppendLine(" 		ELSE  ");
            sqlQuery.AppendLine(" 			'Zero Coupon'  ");
            sqlQuery.AppendLine(" 	END as couponType  ");
            sqlQuery.AppendLine(" 	,ISNULL(masterIndex,'') as masterIndex  ");
            sqlQuery.AppendLine(" 	,mastermargin  ");
            sqlQuery.AppendLine(" 	,masterDate  ");
            sqlQuery.AppendLine(" 	,MasterResetFreq   ");
            sqlQuery.AppendLine(" 	,MasterPerCap  ");
            sqlQuery.AppendLine(" 	,MasterLifeCap  ");
            sqlQuery.AppendLine(" 	,MasterLifeFloor  ");
            sqlQuery.AppendLine("   ");
            sqlQuery.AppendLine(" FROM investment WHERE asOfDate = '" + asOfDate + "'  and custom <> '' ");

            DataTable tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());
            if (tblData.Rows.Count == 0) {
                errors.Add(String.Format("Investment data not found for As Of Date" + asOfDate));
            }
            return new
            {
                tblData = tblData,
            };
        }
    }
}