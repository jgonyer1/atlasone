﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class SummaryOfResultsService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object SummaryOfResultsViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.SummaryOfResults.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("Summary rate Not Found");

            var instService = new InstitutionService(sr.InstitutionDatabaseName);

            StringBuilder sqlQuery = new StringBuilder();
            DataTable scenarios = new DataTable();
            DataTable actualTotals = new DataTable();
            DataTable policyLimits = new DataTable();


            Dictionary<string, double[]> eveScens = new Dictionary<string, double[]>();
            Dictionary<string, double[]> scensBelowEveChange = new Dictionary<string, double[]>();
            Dictionary<string, double[]> scensBelowPostShock = new Dictionary<string, double[]>();
            Dictionary<string, double[]> scensBelowBpChange = new Dictionary<string, double[]>();
            var asOfDate = instService.GetInformation(rep.Package.AsOfDate, sr.AsOfDateOffset).AsOfDate;
            string conStr = Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName);
            string title = "An Economic Value of Equity";
            string titleType = "EVE";
            string xxm2m = "";
            string xxcaprat = "";
            string xxprem = "";
            string xxworstRatio = "";
            string bankName = "BANK NAME";
            string minEveScen = "";
            double minEve = double.MaxValue;
            string minEveRatioScen = "";
            double minEveRatio = double.MaxValue;
            string maxEveRatioScen = "";
            double maxEveRatio = double.MinValue;
            string xxpolicyEve = "";
            string xxpolicyRatio = "";
            string xxpolicyDiffRatio = "";
            string xxshockEve = "";

            bool eveChangeHasPolicy = false;
            bool postShockHasPolicy = false;
            bool bpChangeHasPolicy = false;

            string[] xxPolicyEveScenarios = new string[]{};
            string[] xxPolicyRatioScenarios = new string[]{};
            string[] xxPolicyDiffRatioScenarios = new string[]{};

            string[] xxPolicyEveValues = new string[scensBelowEveChange.Keys.Count];
            string[] xxPolicyRatioValues = new string[scensBelowPostShock.Keys.Count];
            string[] xxPolicyDiffRatioValues = new string[scensBelowBpChange.Keys.Count];

            string[] xxPolicyEvePolicyVals = new string[scensBelowEveChange.Keys.Count];
            string[] xxPolicyRatioPolicyVals = new string[scensBelowPostShock.Keys.Count];
            string[] xxPolicyDiffRatioPolicyVals = new string[scensBelowBpChange.Keys.Count];

            var sim = instService.GetSimulation(rep.Package.AsOfDate, sr.AsOfDateOffset, sr.SimulationType);
            if (sim == null) {
                errors.Add(String.Format("Simulation {0} not found for offset {1} from package As Of Date {2}", sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                var instType = Utilities.ExecuteSql(conStr, "select InstType from [DWR_FedRates].[dbo].[DatabaseHighlineMapping] where DatabaseName = '" + sr.InstitutionDatabaseName + "'");

                bankName = Utilities.ExecuteSql(conStr, "select Name from Basis_Bank where simulationId = " + sim.Id).Rows[0]["Name"].ToString();

                if (instType.Rows[0]["InstType"].ToString().Equals("CU")) {
                    title = "A Net Economic Value";
                    titleType = "NEV";
                }

                //economic values

                sqlQuery.AppendLine("SELECT ");
                sqlQuery.AppendLine("st.name ");
                sqlQuery.AppendLine(",st.Id");
                sqlQuery.AppendLine(",SUM(CASE WHEN alb.isAsset = 1 THEN alp17.MVamount ELSE 0 END) - SUM(CASE WHEN alb.isAsset = 0 THEN alp17.MVamount ELSE 0 END) AS equityValue ");
                sqlQuery.AppendLine(",SUM(CASE WHEN alb.isAsset = 1 THEN alp17.MVamount ELSE 0 END) as assetValue ");
                sqlQuery.AppendLine(",SUM(CASE WHEN alb.Acc_TypeOR = 3 THEN alp17.MVamount ELSE 0 end) AS nmdValue"); sqlQuery.AppendLine("FROM ");
                sqlQuery.AppendLine("(SELECT * FROM BASIS_ALS WHERE simulationId = " + sim.Id + ") as alp17");
                sqlQuery.AppendLine("INNER JOIN BASIS_ALB as alb ");
                sqlQuery.AppendLine("ON alb.code = alp17.code AND alb.simulationid = alp17.simulationid and alb.cat_type in(0,1,5)");
                sqlQuery.AppendLine("INNER JOIN Basis_Scenario AS bs ");
                sqlQuery.AppendLine("ON bs.number = alp17.scenario AND bs.SimulationId = alp17.SimulationId  ");
                sqlQuery.AppendLine("INNER JOIN ATLAS_ScenarioType  as st");
                sqlQuery.AppendLine("ON st.id = bs.scenarioTypeid");
                sqlQuery.AppendLine("WHERE st.Id != 1");
                sqlQuery.AppendLine("GROUP BY st.name, st.Id");

                scenarios = Utilities.ExecuteSql(conStr, sqlQuery.ToString());
                sqlQuery.Clear();

                //total actuals
                sqlQuery.AppendLine(" select ");
                sqlQuery.AppendLine("sum(case when alb.Acc_TypeOR = 4 then end_Bal else 0 end) as totalEquity");
                sqlQuery.AppendLine(",sum(CASE WHEN alb.Acc_TypeOR = 3 THEN end_Bal else 0 end) as totalNMD");
                sqlQuery.AppendLine(",sum(case when alb.isAsset = 1 then end_Bal else 0 end) as totalAssets");
                sqlQuery.AppendLine("from (select end_Bal, code, simulationId from basis_ala where simulationId = " + sim.Id + " and month = '" + asOfDate.Month + "/1/" + asOfDate.Year + "' )as ala");
                sqlQuery.AppendLine("inner join basis_alb as alb");
                sqlQuery.AppendLine("on alb.code = ala.code and (alb.Acc_TypeOR in (3,4) or alb.IsAsset = 1) and alb.simulationId = ala.SimulationId and alb.cat_type in (0,1,5)");

                actualTotals = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

                sqlQuery.Clear();

                sqlQuery.AppendLine("select ");
                sqlQuery.AppendLine("Name");
                sqlQuery.AppendLine(",ScenarioTypeId");
                sqlQuery.AppendLine(",shortName");
                //sqlQuery.AppendLine(",epo.CurrentRatio");
                sqlQuery.AppendLine(",epo.PolicyLimit");
                sqlQuery.AppendLine("FROM");
                sqlQuery.AppendLine("(SELECT id ");
                sqlQuery.AppendLine("FROM ATLAS_Policy WHERE asOfDate = '" + asOfDate.ToString() + "')  as p");
                sqlQuery.AppendLine("INNER JOIN ATLAS_EvePolicyParent as ep ON");
                sqlQuery.AppendLine("p.id = ep.policyid");
                sqlQuery.AppendLine("INNER JOIN");
                sqlQuery.AppendLine("ATLAS_EvePolicy AS epo ON");
                sqlQuery.AppendLine("epo.EvePolicyParentId = ep.id");
                sqlQuery.AppendLine("WHERE  epo.CurrentRatio <> 0");

                policyLimits = Utilities.ExecuteSql(conStr, sqlQuery.ToString());


                xxcaprat = GetRoundRatioString((double.Parse(actualTotals.Rows[0]["totalEquity"].ToString()) / double.Parse(actualTotals.Rows[0]["totalAssets"].ToString())) * 100);
                foreach (DataRow dr in scenarios.Rows) {
                    double[] dbls = new double[4];
                    dbls[0] = double.Parse(dr["equityValue"].ToString());
                    dbls[1] = double.Parse(dr["assetValue"].ToString());
                    dbls[2] = double.Parse(dr["nmdValue"].ToString());
                    dbls[3] = double.Parse(dr["Id"].ToString());
                    eveScens.Add(dr["name"].ToString(), dbls);

                    //I can get these done here, might as well I guess
                    if (((string)dr["name"]).Equals("0 Shock")) {
                        xxm2m = GetRoundRatioString((double.Parse(dr["equityValue"].ToString()) / double.Parse(dr["assetValue"].ToString())) * 100);
                        xxprem = GetRoundRatioString((1 - (double.Parse(dr["nmdValue"].ToString()) / double.Parse(actualTotals.Rows[0]["totalNMD"].ToString()))) * 100);
                    }

                }

                if (!eveScens.ContainsKey("0 Shock")) {
                    errors.Add("0 Shock Scenario cannot be found in " + sim.Name + " Simulation");
                }
                else {
                    List<string> eveSceneList = new List<string>(eveScens.Keys.ToArray());
                    eveSceneList.Remove("0 Shock");
                    xxshockEve = String.Join(", ", eveSceneList);

                    foreach (string key in eveScens.Keys) {
                        double eveVal = ((eveScens[key][0] - eveScens["0 Shock"][0]) / eveScens["0 Shock"][0]);
                        double eveRatioVal = (eveScens[key][0] / eveScens[key][1]);
                        double bpChange = (eveRatioVal - (eveScens["0 Shock"][0] / eveScens["0 Shock"][1]));

                        if (!key.Equals("0 Shock")) {

                            //policies
                            double eveChangePolicy = 0.0;
                            double postShockPolicy = 0.0;
                            double bpChangePolicy = 0.0;

                            foreach (DataRow r in policyLimits.Rows) {
                                if (double.Parse(r["ScenarioTypeId"].ToString()) == double.Parse(eveScens[key][3].ToString()) && (string)r["shortName"] == "eveChange")
                                    eveChangePolicy = double.Parse(r["PolicyLimit"].ToString());
                                if (double.Parse(r["ScenarioTypeId"].ToString()) == double.Parse(eveScens[key][3].ToString()) && (string)r["shortName"] == "postShock")
                                    postShockPolicy = double.Parse(r["PolicyLimit"].ToString());
                                if (double.Parse(r["ScenarioTypeId"].ToString()) == double.Parse(eveScens[key][3].ToString()) && (string)r["shortName"] == "bpChange")
                                    bpChangePolicy = double.Parse(r["PolicyLimit"].ToString());

                            }
                            //check if we have a policy that's not -999
                            if (eveChangePolicy != -999.0)
                                eveChangeHasPolicy = true;
                            if (postShockPolicy != -999.0)
                                postShockHasPolicy = true;
                            if (bpChangePolicy != -999.0)
                                bpChangeHasPolicy = true;

                            if (eveVal * 100 < eveChangePolicy)
                                scensBelowEveChange.Add(key, new double[] { eveVal / 100, eveChangePolicy });
                            if (eveRatioVal * 100 < postShockPolicy)
                                scensBelowPostShock.Add(key, new double[] { eveRatioVal / 100, postShockPolicy });
                            if (bpChange * 10000 < bpChangePolicy)
                                scensBelowBpChange.Add(key, new double[] { bpChange, bpChangePolicy });

                            if (minEve == double.MaxValue || (eveVal < minEve)) {
                                minEve = eveVal;
                                minEveScen = key;
                            }
                            //else if (eveVal < minEve)
                            //{
                            //    minEve = eveVal;
                            //    minEveScen = key;
                            //}
                            if (minEveRatio == double.MaxValue || eveRatioVal < minEveRatio) {
                                minEveRatio = eveRatioVal;
                                minEveRatioScen = key;
                            }
                            if (maxEveRatio == double.MinValue || eveRatioVal > maxEveRatio) {
                                maxEveRatio = eveRatioVal;
                                maxEveRatioScen = key;
                            }
                        }
                    }
                    //go through and make collections of scen names, scen vals, and policy vals for the xxpolicy____  variables

                    xxPolicyEveScenarios = scensBelowEveChange.Keys.ToArray();
                    xxPolicyRatioScenarios = scensBelowPostShock.Keys.ToArray();
                    xxPolicyDiffRatioScenarios = scensBelowBpChange.Keys.ToArray();

                    if (xxPolicyEveScenarios.Length == 0)
                        xxpolicyEve = "All measures of % Change in " + titleType + " from 0 Shock are above policy guidelines.";
                    if (xxPolicyRatioScenarios.Length == 0)
                        xxpolicyRatio = "All measures of " + titleType + " Ratio (" + titleType + "/EVA) are above policy guidelines.";
                    if (xxPolicyDiffRatioScenarios.Length == 0)
                        xxpolicyDiffRatio = "All measures of BP Change in " + titleType + " Ratio (" + titleType + "/EVA) from 0 Shock are above policy guidelines.";

                    xxPolicyEveValues = new string[scensBelowEveChange.Keys.Count];
                    xxPolicyRatioValues = new string[scensBelowPostShock.Keys.Count];
                    xxPolicyDiffRatioValues = new string[scensBelowBpChange.Keys.Count];

                    xxPolicyEvePolicyVals = new string[scensBelowEveChange.Keys.Count];
                    xxPolicyRatioPolicyVals = new string[scensBelowPostShock.Keys.Count];
                    xxPolicyDiffRatioPolicyVals = new string[scensBelowBpChange.Keys.Count];


                    int c = 0;
                    foreach (string key in scensBelowEveChange.Keys) {
                        xxPolicyEveValues[c] = GetRoundRatioString(scensBelowEveChange[key][0] * 10000) + "%";
                        xxPolicyEvePolicyVals[c] = String.Format(GetRoundRatioString(scensBelowEveChange[key][1]), "#.00") + "%";
                        c++;
                    }
                    c = 0;
                    foreach (string key in scensBelowPostShock.Keys) {
                        xxPolicyRatioValues[c] = String.Format("{0:.##}", GetRoundRatioString(scensBelowPostShock[key][0] * 10000)) + "%";
                        xxPolicyRatioPolicyVals[c] = String.Format(GetRoundRatioString(scensBelowPostShock[key][1]), "#.00") + "%";
                        c++;
                    }
                    c = 0;
                    foreach (string key in scensBelowBpChange.Keys) {
                        xxPolicyDiffRatioValues[c] = GetRoundBPString(scensBelowBpChange[key][0] * 10000) + "BP";
                        xxPolicyDiffRatioPolicyVals[c] = GetRoundBPString(scensBelowBpChange[key][1]) + "BP";
                        c++;
                    }

                    xxworstRatio = GetRoundBPString((Math.Abs(double.Parse(xxm2m) - (minEveRatio * 100)) * 100));
                }
            }

            

            

            return new
            {
                title = title,
                titleType = titleType,
                xbankname = bankName,
                xaod = asOfDate.ToString("d"),
                xxm2m = xxm2m,
                xxcaprat = xxcaprat,
                xxprem = xxprem,
                xxresult = sr.ExtraText,
                xxworstScenEve = minEveScen,
                xxworstEve = GetRoundRatioString(minEve * 100),
                xxpolicyEve = xxpolicyEve,
                xxpolicyEveScenarios = String.Join(", ", xxPolicyEveScenarios),
                xxpolicyEveValues = String.Join(", ", xxPolicyEveValues),
                xxpolicyEvePolicyValues = String.Join(", ", xxPolicyEvePolicyVals),
                xxworstScenRatio = minEveRatioScen,
                xxworstDiffRatio = xxworstRatio,
                xxworstRatio = GetRoundRatioString(minEveRatio * 100),
                xxpolicyRatio = xxpolicyRatio,
                xxpolicyRatioScenarios = String.Join(", ", xxPolicyRatioScenarios),
                xxpolicyRatioValues = String.Join(", ", xxPolicyRatioValues),
                xxpolicyRatioPolicyValues = String.Join(", ", xxPolicyRatioPolicyVals),
                xxpolicyDiffRatio = xxpolicyDiffRatio,
                xxpolicyDiffRatioScenarios = String.Join(", ", xxPolicyDiffRatioScenarios),
                xxpolicyDiffRatioValues = String.Join(", ", xxPolicyDiffRatioValues),
                xxpolicyDiffRatioPolicyValues = String.Join(", ", xxPolicyDiffRatioPolicyVals),
                xxshockEve = xxshockEve,
                xxbestRatio = GetRoundRatioString(maxEveRatio * 100),
                eveChangeHasPolicy = eveChangeHasPolicy,
                postShockHasPolicy = postShockHasPolicy,
                bpChangeHasPolicy = bpChangeHasPolicy,
                errors = errors,
                warnings = warnings
            };
        }

        public string GetRoundRatioString(double value)
        {
            return String.Format("{0:f2}",Math.Round(value, 2, MidpointRounding.AwayFromZero));
        }
        public string GetRoundBPString(double value)
        {
            return Math.Round(value, 0, MidpointRounding.AwayFromZero).ToString();
        }
    }
}