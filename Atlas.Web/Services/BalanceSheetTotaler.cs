﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using System.Text;
using System.Data;

namespace Atlas.Web.Services
{
    public class BalanceSheetTotaler
    {
        /// <summary>
        /// View option for determining how to handle totals.
        /// </summary>
        public enum ViewByType
        {
            /// <summary>
            /// View by None (-1); total data and take no additional steps.
            /// </summary>
            None = -1,
            /// <summary>
            /// View by Sub Accounts (0); total data and zero out all Masters.
            /// </summary>
            SubAccounts,
            /// <summary>
            /// View by Masters (1); total data and zero out all Sub Accounts.
            /// </summary>
            Masters,
            /// <summary>
            /// View by Classifications (2); total data and automatically add intermediate totaling rows whenever Account Type changes, as well as Total Assets and Total Liabilities grand totals.
            /// </summary>
            Classifications,
            /// <summary>
            /// View by Account Types (3); total data and automatically add Total Assets and Total Liabilities grand totals. (same as Defined Groupings)
            /// </summary>
            AccountTypes,
            /// <summary>
            /// View by Defined Groupings (4); total data and automatically add Total Assets and Total Liabilities grand totals. (same as Account Types)
            /// </summary>
            DefinedGroupings
        }

        /// <summary>
        /// Type of totaling to do for a TotalingItem.
        /// </summary>
        public enum TotalingType
        {
            /// <summary>
            /// Sum individual values.
            /// </summary>
            Sum,
            /// <summary>
            /// Average individual values. If TotalingItem.WeightCol is specified, a weighted average is used; otherwise, a regular average is used.
            /// </summary>
            Average,
            /// <summary>
            /// Use maximum value.
            /// </summary>
            Max,
            /// <summary>
            /// Use minimum value.
            /// </summary>
            Min
        }

        /// <summary>
        /// Determines how to total an individual column.
        /// </summary>
        public class TotalingItem
        {
            public string DataCol { get; set; }
            public string WeightCol { get; set; }
            public TotalingType Type { get; set; }

            /// <summary>
            /// Declares a new TotalingItem.
            /// </summary>
            /// <param name="dataCol">ColumnName of column to total.</param>
            /// <param name="type">Type of totaling.</param>
            /// <param name="weightCol">If TotalingType.Average, ColumnName of column to weigh against for a weighted average. If blank, a regular average is used.</param>
            public TotalingItem(string dataCol, TotalingType type = TotalingType.Sum, string weightCol = "")
            {
                this.DataCol = dataCol;
                this.WeightCol = weightCol;
                this.Type = type;
            }
        }

        /// <summary>
        /// Helper method to safely parse a double from a string.
        /// </summary>
        /// <param name="input">String to parse.</param>
        /// <returns>input as double if successfully parsed; 0 otherwise.</returns>
        static private double DoubleSafeParse(string input)
        {
            double ret;
            return (double.TryParse(input, out ret)) ? ret : 0;
        }

        /// <summary>
        /// Helper method to safely parse an int from a string.
        /// </summary>
        /// <param name="input">String to parse.</param>
        /// <returns>input as int if successfully parsed; 0 otherwise.</returns>
        static private int IntSafeParse(string input)
        {
            int ret;
            return (int.TryParse(input, out ret)) ? ret : 0;
        }

        /// <summary>
        /// Clear all items from a totaling row.
        /// </summary>
        /// <param name="items">Items to clear.</param>
        /// <param name="totalRow">Totaling row to clear from.</param>
        static private void ClearTotal(IEnumerable<TotalingItem> items, DataRow totalRow)
        {
            string tempWeightCol;

            foreach (TotalingItem item in items)
            {
                totalRow[item.DataCol] = 0;

                tempWeightCol = "#TempWeight" + item.WeightCol;
                if (totalRow.Table.Columns.Contains(tempWeightCol))
                    totalRow[tempWeightCol] = 0;

                if (totalRow.Table.Columns.Contains("#TempCount"))
                    totalRow["#TempCount"] = 0;
            }
        }

        /// <summary>
        /// Adds all items to a totaling row.
        /// </summary>
        /// <param name="items">Items to add.</param>
        /// <param name="drow">Row to add items from.</param>
        /// <param name="totalRow">Totaling row to add items to.</param>
        static private void AddToTotal(IEnumerable<TotalingItem> items, DataRow drow, DataRow totalRow)
        {
            double totData, drData, drWeight;
            string tempWeightCol;

            foreach (TotalingItem item in items)
            {
                totData = DoubleSafeParse(totalRow[item.DataCol].ToString());
                drData = DoubleSafeParse(drow[item.DataCol].ToString());
                drWeight = (item.WeightCol == "") ? 0 : DoubleSafeParse(drow[item.WeightCol].ToString());

                switch (item.Type)
                {
                    case TotalingType.Sum:
                        totData += drData;
                        break;
                    case TotalingType.Average:
                        //If we're doing a weighted average, do so appropriately; otherwise just do a normal average, keeping track of count
                        if (item.WeightCol != "")
                        {
                            totData += drData * drWeight;

                            //Add a temporary column for weight so we can get an unaltered sum of it later
                            tempWeightCol = "#TempWeight" + item.WeightCol;
                            if (!totalRow.Table.Columns.Contains(tempWeightCol))
                                totalRow.Table.Columns.Add(tempWeightCol, typeof(double));

                            //Add the weight so we can get a sum of it later, but only once (e.g. multiple columns weighted against rates, don't want to add rates twice!)
                            if (!totalRow.Table.Columns[tempWeightCol].ReadOnly)
                            {
                                totalRow[tempWeightCol] = DoubleSafeParse(totalRow[tempWeightCol].ToString()) + drWeight;
                                totalRow.Table.Columns[tempWeightCol].ReadOnly = true;
                            }
                        }
                        else
                        {
                            totData += drData;

                            //Add our #TempCount col if it doesn't exist
                            if (!totalRow.Table.Columns.Contains("#TempCount"))
                                totalRow.Table.Columns.Add("#TempCount", typeof(int));
                            totalRow["#TempCount"] = IntSafeParse(totalRow["#TempCount"].ToString()) + 1;
                        }
                        break;
                    case TotalingType.Max:
                        if (drData > totData)
                            totData = drData;
                        break;
                    case TotalingType.Min:
                        if (drData < totData)
                            totData = drData;
                        break;
                }

                //Update total row DataCol's data (which might not have changed)
                totalRow[item.DataCol] = totData;
            }

            //Unlock the WeightCol
            foreach (TotalingItem item in items)
                if (item.WeightCol != "")
                    totalRow.Table.Columns["#TempWeight" + item.WeightCol].ReadOnly = false;
        }

        /// <summary>
        /// Pulls totals from a totaling row.
        /// </summary>
        /// <param name="items">Items to pull.</param>
        /// <param name="drow">Row to pull items to.</param>
        /// <param name="totalRow">Totaling row to pull items from.</param>
        static private void PullTotal(IEnumerable<TotalingItem> items, DataRow drow, DataRow totalRow)
        {
            double totData, totWeight;

            foreach (TotalingItem item in items)
            {
                try
                {
                    totData = DoubleSafeParse(totalRow[item.DataCol].ToString());
                    totWeight = (item.WeightCol == "") ? 0 : DoubleSafeParse(totalRow["#TempWeight" + item.WeightCol].ToString());
                }
                catch (Exception ex)
                {
                    totData = 0;
                    totWeight = 0;
                }


                switch (item.Type)
                {
                    case TotalingType.Sum:
                    case TotalingType.Max:
                    case TotalingType.Min:
                        drow[item.DataCol] = totData;
                        break;
                    case TotalingType.Average:
                        drow[item.DataCol] = (item.WeightCol != "")
                            ? ((totWeight == 0) ? 0 : totData / totWeight)
                            : totData / IntSafeParse(totalRow["#TempCount"].ToString());
                        break;
                }
            }
        }

        /// <summary>
        /// Adds a new row to the totaling table.
        /// </summary>
        /// <param name="retTable">Table to add to.</param>
        /// <param name="newRow">New row to add.</param>
        /// <param name="groupings">List of groupings.</param>
        /// <param name="groupByCol">ColumnName for column to optionally group on. If blank, no groupings are done.</param>
        static private void AddTotalRow(DataTable retTable, DataRow newRow, LinkedList<object> groupings, string groupByCol)
        {
            object grouping;

            if (groupByCol == "")
                retTable.Rows.Add(newRow.ItemArray);
            else
            {
                grouping = newRow[groupByCol];
                if (grouping.ToString() == "")
                    foreach (var g in groupings)
                        retTable.Rows.Add(newRow.ItemArray)[groupByCol] = g;
                else
                {
                    if (!groupings.Contains(grouping))
                        groupings.AddLast(grouping);
                    retTable.Rows.Add(newRow.ItemArray);
                }
            }
        }

        /// <summary>
        /// Returns a new DataTable with calculated totals, adding totaling rows as necessary per specified options.
        /// </summary>
        /// <param name="dt">DataTable to modify.</param>
        /// <param name="items">List of items to determine which columns should be totaled (and how).</param>
        /// <param name="viewOption">View option for table; if Classification or AccountTypes, automatically add totaling rows not normally present in ALB records.</param>
        /// <param name="groupByCol">ColumnName for column to optionally group on. If blank, no groupings are done.</param>
        /// <param name="nameCol">ColumnName for category names.</param>
        /// <param name="catTypeCol">For ViewByType.None, ViewByType.SubAccounts or ViewByType.Masters (not needed otherwise); ColumnName for cateory types.</param>
        /// <param name="acctNameCol">For ViewByType.Classifications (not needed otherwise); ColumnName for category's associated account names.</param>
        /// <param name="isAssetCol">For ViewByType.Classifications, ViewByType.AccountTypes or ViewByType.DefinedGroupings (not needed otherwise); ColumnName for if category is an asset.</param>
        /// <returns>New DataTable with proper totaling rows.</returns>
        static public DataTable CalcTotalsFor(DataTable dt, IEnumerable<TotalingItem> items,
            ViewByType viewOption = ViewByType.None,
            string groupByCol = "",
            string nameCol = "Name", string catTypeCol = "Cat_Type",
            string acctNameCol = "AccountName", string isAssetCol = "IsAsset")
        {
            //Sanity check
            if (dt.Rows.Count < 1)
                return dt;

            //Setup our return table with same schema as our incoming dt
            //Note that we don't copy it as we will populate the rows with e.g. new total rows as we go)
            var retTable = dt.Clone();

            //If we don't have a catType, it's probably because we grouped on classifications or acct type (or we screwed up!)
            if (!retTable.Columns.Contains(catTypeCol))
                retTable.Columns.Add(catTypeCol, typeof(int));

            //Allow nulls and editing because that's what we're doing!
            foreach (DataColumn dcol in retTable.Columns)
            {
                dcol.AllowDBNull = true;
                dcol.ReadOnly = false;
            }

            //Build the new retTable off dt, inserting any new rows (additional totals for classificaitons / account types, column groupings, etc.)
            DataRow newRow;
            var groupings = new LinkedList<object>();
            object grouping = null;
            bool needGrandTotals = viewOption > ViewByType.Masters; //ViewByType.Classifications, ViewByType.AccountTypes, ViewByType.DefinedGroupings;
            bool needTotalAsset = needGrandTotals;
            string acctName = "";
            foreach (DataRow drow in dt.Rows)
            {
                //Handle classification totaling based on where acctType changes (just do it by acctName)
                if (viewOption == ViewByType.Classifications)
                {
                    if (acctName == "")
                        acctName = drow[acctNameCol].ToString();

                    //Insert a new total for this acctName
                    if (acctName != drow[acctNameCol].ToString())
                    {
                        newRow = retTable.NewRow();
                        newRow[nameCol] = "Total " + acctName;
                        newRow[catTypeCol] = 4;
                        newRow[isAssetCol] = needTotalAsset; //Should be accurate if we needGrandTotals (which ViewByType.Classifications infers)
                        AddTotalRow(retTable, newRow, groupings, groupByCol);

                        //Get current account name so we can catch single-row acctName changes (e.g. Other Liability)
                        acctName = drow[acctNameCol].ToString();
                    }
                }

                //Insert a total for all assets where it switches to Liabilites
                if (needTotalAsset && !bool.Parse(drow[isAssetCol].ToString()))
                {
                    needTotalAsset = false;
                    newRow = retTable.NewRow();
                    newRow[nameCol] = "Total Assets";
                    newRow[catTypeCol] = 7;
                    newRow[isAssetCol] = true;
                    AddTotalRow(retTable, newRow, groupings, groupByCol);
                }

                //Handle stuff tomorrow morning! SNAKES
                AddTotalRow(retTable, drow, groupings, groupByCol);
            }

            //Iteration's over, add our final two totaling rows if needed
            if (needGrandTotals)
            {
                //Final total for classifications (if needed)
                if (acctName != "") //infers viewOption == ViewByType.Classifications
                {
                    newRow = retTable.NewRow();
                    newRow[nameCol] = "Total " + acctName;
                    newRow[catTypeCol] = 4;
                    newRow[isAssetCol] = needTotalAsset; //Should be accurate if we needGrandTotals
                    AddTotalRow(retTable, newRow, groupings, groupByCol);
                }

                //Final total for all liabilities (or just plain old total if we only had one type!)
                newRow = retTable.NewRow();
                newRow[nameCol] = (needTotalAsset) ? "Total" : "Total Liabilities";
                newRow[catTypeCol] = 7;
                newRow[isAssetCol] = needTotalAsset; //Should be accurate if we needGrandTotals
                AddTotalRow(retTable, newRow, groupings, groupByCol);
            }

            //Finally, totaling logic! Build a new totalsTable (mainly to hold schema for convenience) and totaling dictionary (what we actually reference), by grouping if required
            var totalsTable = retTable.Clone();
            var totalsGroupings = new Dictionary<object, Dictionary<int, DataRow>>();
            grouping = "";

            int catType = 0;
            DataRow totalRow;
            Dictionary<int, DataRow> totals;
            foreach (DataRow drow in retTable.Rows)
            {
                //Just assume 0 for catType if not present
                catType = IntSafeParse(drow[catTypeCol].ToString());

                //Handle groupings
                if (groupByCol != "")
                    grouping = drow[groupByCol];

                //Create new dictionary for grouping if appropriate (grouping may just be null for no groupings)
                if (totalsGroupings.ContainsKey(grouping))
                    totals = totalsGroupings[grouping];
                else
                {
                    totals = new Dictionary<int, DataRow>();
                    totalsGroupings.Add(grouping, totals);
                    totals.Add(2, totalsTable.Rows.Add()); //I1 Intermediate Total
                    totals.Add(3, totalsTable.Rows.Add()); //I2 Intermediate Total
                    totals.Add(4, totalsTable.Rows.Add()); //I3 Intermediate Total
                    totals.Add(6, totalsTable.Rows.Add()); //Master Account
                    totals.Add(7, totalsTable.Rows.Add()); //Grand Total
                }


                if (drow[nameCol].ToString() == "Total Municipals")
                {
                    for (int i = 0; i <1; i++)
                    {
                        Console.WriteLine(i);
                    }
                }
                //See if we're a total - if so, cash it in, otherwise add us to totals
                if (totals.ContainsKey(catType))
                {
                    totalRow = totals[catType];
                    PullTotal(items, drow, totalRow);

                    switch (catType)
                    {
                        case 7: //Grand Total, clear self only if switching from Assets to Liabilities
                            if (drow[nameCol].ToString() == "Total Assets")
                            {
                                ClearTotal(items, totalRow);
                                //If doing a grand total think we should clear all lower level totals since this only happens at total assets anyqays
                                ClearTotal(items, totals[6]);
                                ClearTotal(items, totals[4]);
                                ClearTotal(items, totals[3]);
                                ClearTotal(items, totals[2]);
                            }
                              

                         

                            break;
                        case 4: //I3, clear self + I2 + I1
                            ClearTotal(items, totalRow);
                            ClearTotal(items, totals[3]);
                            ClearTotal(items, totals[2]);
                            break;
                        case 3: //I2, clear self + I1
                            ClearTotal(items, totalRow);
                            ClearTotal(items, totals[2]);
                            break;
                        default: //Others, clear self
                            ClearTotal(items, totalRow);
                            break;
                    }
                }
                else
                {
                    foreach (var kvp in totals)
                    {
                        //Only add Subs to Master totals
                        if (kvp.Key == 6 && catType != 5)
                            continue;
                        AddToTotal(items, drow, kvp.Value);
                    }
                }

                //Sub/Master logic - clear out one if using the other (safe since we've already added it to (or reported as) total)
                if ((viewOption == ViewByType.SubAccounts && catType == 6)
                || (viewOption == ViewByType.Masters && catType == 5))
                    ClearTotal(items, drow);
            }

            return retTable;
        }
    }
}
