﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class TimeDepositMigrationService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object TimeDepositMigrationViewModel(int reportId)
        {

            //Get inst context that holds the report
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Load report settings
            var c = gctx.TimeDepositMigrations.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            if (c == null) throw new Exception("Time Deposit Migration Not Found");


            //Load inst selected in config
            var instService = new InstitutionService(c.InstitutionDatabaseName);
            var curInfo = instService.GetInformation(rep.Package.AsOfDate, c.AsOfDateOffset);
            var priorInfo = instService.GetInformation(rep.Package.AsOfDate, c.PriorAsOfDateOffset);


            /*
             * Checks overdies and sets them accordinly
             */
            string dateLookUp = curInfo.AsOfDate.Month.ToString().PadLeft(2, '0') + "/" + curInfo.AsOfDate.Day.ToString().PadLeft(2, '0') + "/" + curInfo.AsOfDate.Year.ToString();

            string priorDateLookUp = priorInfo.AsOfDate.Month.ToString().PadLeft(2, '0') + "/" + priorInfo.AsOfDate.Day.ToString().PadLeft(2, '0') + "/" + priorInfo.AsOfDate.Year.ToString();

            GlobalController gc = new GlobalController();
            Dictionary<string, DataTable> pols = gc.PolicyOffsets(c.InstitutionDatabaseName);

            if (!c.OverrideSimulationTypeId)
            {
                if (pols.ContainsKey(dateLookUp))
                {
                    c.SimulationTypeId = int.Parse(pols[dateLookUp].Rows[0]["bsSimulation"].ToString());
                }

            }

            if (!c.OverrideScenarioTypeId)
            {
                if (pols.ContainsKey(dateLookUp))
                {
                    c.ScenarioTypeId = int.Parse(pols[dateLookUp].Rows[0]["bsScenario"].ToString());
                }
            }


            if (!c.OverridePriorSimulationTypeId)
            {
                if (pols.ContainsKey(priorDateLookUp))
                {
                    c.PriorSimulationTypeId = int.Parse(pols[priorDateLookUp].Rows[0]["bsSimulation"].ToString());
                }

            }

            if (!c.OverridePriorScenarioTypeId)
            {
                if (pols.ContainsKey(priorDateLookUp))
                {
                    c.PriorScenarioTypeId = int.Parse(pols[priorDateLookUp].Rows[0]["bsScenario"].ToString());
                }
            }


            //Load Up Simulation Information
            object curObj = instService.GetSimulationScenario(curInfo.AsOfDate, 0, c.SimulationTypeId, c.ScenarioTypeId);
            string simulationId = curObj.GetType().GetProperty("simulation").GetValue(curObj).ToString();
            string scenario = curObj.GetType().GetProperty("scenario").GetValue(curObj).ToString();

            object priorObj = instService.GetSimulationScenario(priorInfo.AsOfDate, 0, c.PriorSimulationTypeId, c.PriorScenarioTypeId);
            string priorSimulationId = priorObj.GetType().GetProperty("simulation").GetValue(priorObj).ToString();
            string priorScenario = priorObj.GetType().GetProperty("scenario").GetValue(priorObj).ToString();

            //Build out in statement for regular or special
            string regSpecExclude = "";

            if (!c.Regular)
            {
                regSpecExclude = ",0,1,3";
            }

            if (!c.Special)
            {
                regSpecExclude += ",2,4";
            }

            //trim exclude leading comma
            if (regSpecExclude != "")
            {
                regSpecExclude = regSpecExclude.Substring(1);
                regSpecExclude = " NOT IN (" + regSpecExclude + ") OR b.classOr IN (2,4,5,6,9,10)";
            }
            else
            {
                regSpecExclude = "";
            }


            //Build out not in statement for classifications what is not included
            string exclude = "";
            if (!c.CD)
            {
                exclude += ",1";
            }

            if (!c.JumboCD)
            {
                exclude += ",3";
            }

            if (!c.PublicCD)
            {
                exclude += ",2";
            }

            if (!c.PublicJumbo)
            {
                exclude += ",4";
            }

            if (!c.NationalCD)
            {
                exclude += ",6";
            }

            if (!c.OnlineCD)
            {
                exclude += ",7";
            }

            if (!c.OnlineJumbo)
            {
                exclude += ",8";
            }

            if (!c.BrokeredCD)
            {
                exclude += ",5";
            }

            if (!c.BrokeredOneWayCD)
            {
                exclude += ",10";
            }

            if (!c.BrokeredReciprocalCD)
            {
                exclude += ",9";
            }

            if (!c.OtherTimeDeposits)
            {
                exclude += ",0";
            }

            //trim exclude leading comma
            if (exclude != "")
            {
                exclude = exclude.Substring(1);
                exclude = " NOT IN (" + exclude + ")";
            }
            else
            {
                exclude = "";
            }


            //string cattype
            string catTypeIn = "(0,1,5)";
            if (c.ViewBy == 1)
            {
                catTypeIn = "(0,1,5)";
            }
            

            //Build SQL PUll
            StringBuilder sqlQuery = new StringBuilder();

            //Break outs report by term buuckets

            sqlQuery.AppendLine(" SELECT ");
           

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	allgroupings.groupingName ");
                sqlQuery.AppendLine(" 	,allgroupings.termBucketName ");
            }
            else
            {
                sqlQuery.AppendLine(" 	allgroupings.groupingName as Name ");
                sqlQuery.AppendLine(" 	,allgroupings.Cat_Type ");
                sqlQuery.AppendLine(" 	,allgroupings.IsAsset ");
            }
            
            sqlQuery.AppendLine(" 	,ISNULL(p.startingBal, 0)  as priorBal ");
            sqlQuery.AppendLine(" 	,ISNULL(p.startingRate, 0)  as priorRate ");
            sqlQuery.AppendLine(" 	,ISNULL(pm.maturting, 0) as priorMatBal ");
            sqlQuery.AppendLine(" 	,ISNULL(pm.maturingRate, 0) as priorMatRate ");
            sqlQuery.AppendLine(" 	,ISNULL(c.startingBal, 0) as currentBal ");
            sqlQuery.AppendLine(" 	,ISNULL(c.startingRate, 0) as currentRate ");
            sqlQuery.AppendLine(" 	,ISNULL(cm.maturting, 0) as currentMatBal ");
            sqlQuery.AppendLine(" 	,ISNULL(cm.maturingRate, 0) as currentMatRate ");
            sqlQuery.AppendLine(" 	FROM  ");
            sqlQuery.AppendLine("  ( ");
            sqlQuery.AppendLine("  SELECT   ");
            
            if (c.ViewBy == 1)
            {
                sqlQuery.AppendLine(" 	b.name as groupingName ");
            }
            else
            {
                sqlQuery.AppendLine(" 	dv.name as groupingName ");
                sqlQuery.AppendLine(" 	,tb.name as termBucketName ");
            }
           
            sqlQuery.AppendLine("  	,SUM(a.end_Bal) as startingBal   ");
            sqlQuery.AppendLine("  	,SUM(a.End_Bal * a.Total_End_Rate) / NULLIF(SUM(a.End_Bal),  0) as startingRate   ");
            sqlQuery.AppendLine("  	FROM Basis_ALA as a   ");
            sqlQuery.AppendLine("  	INNER JOIN Basis_ALB as b ON a.code = b.code and a.SimulationId = b.SimulationId AND b.Acc_TypeOR = 8 AND a.simulationId = " + priorSimulationId + " AND a.month = '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "' ");
            
            if (c.ViewBy == 0)
            {
                //DEfault View
                sqlQuery.AppendLine(" 		INNER JOIN [Atlas_TimeDepositMigrationDefaultView] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.class = ISNULL(b.classOr,0) AND dv.IlpKey4 = ISNULL(b.IlpKey4, 0) AND b.acc_TypeOr = 8  ");
            }
            else if (c.ViewBy == 2)
            {
                //Classification
                sqlQuery.AppendLine("  INNER JOIN [Atlas_Classification] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.AccountTypeId = ISNULL(b.Acc_TypeOr,0) AND dv.rbcTier = ISNULL(b.classOr, 0) AND dv.isAsset = b.isAsset AND b.acc_TypeOr = 8  ");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	INNER JOIN [Atlas_TimeDepositMigrationTermBuckets] as tb ");
                sqlQuery.AppendLine(" 	ON tb.IlpKey5 = ISNULL(b.IlpKey5, 0)  ");
            }

            sqlQuery.AppendLine(" WHERE b.exclude = 0   AND b.cat_type IN " + catTypeIn + " ");
            if (exclude != "")
            {
                sqlQuery.AppendLine(" AND b.classOr " + exclude);
            }

            if (regSpecExclude != "")
            {
                sqlQuery.AppendLine(" AND (b.IlpKey4 " + regSpecExclude + ")" );
            }

            if (c.ViewBy != 1) { 
                sqlQuery.AppendLine(" 	GROUP BY dv.name,tb.name ");
            }
            else
            {
                sqlQuery.AppendLine(" 	GROUP BY b.name ");
            }

            sqlQuery.AppendLine(" 	) as p ");
            sqlQuery.AppendLine(" 	FULL OUTER JOIN	  ");
            sqlQuery.AppendLine(" 	(SELECT   ");

            if (c.ViewBy == 1)
            {
                sqlQuery.AppendLine(" 	b.name as groupingName ");
            }
            else
            {
                sqlQuery.AppendLine(" 	dv.name as groupingName ");
                sqlQuery.AppendLine(" 	,tb.name as termBucketName ");
            }


            sqlQuery.AppendLine(" 		,SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)) AS  maturting   ");
            sqlQuery.AppendLine(" 		,SUM((ISNULL(alp3.MatEBal, 0) * ISNULL(alp3.MatERate, 0)) + (ISNULL(alp4.AmtEBal, 0) * ISNULL(alp4.AmtERate, 0)) + (ISNULL(alp5.PayEBal, 0) * ISNULL(alp5.PayERate, 0))) / NULLIF(SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)), 0) AS  maturingRate   ");
            sqlQuery.AppendLine("  	FROM  ");
            sqlQuery.AppendLine(" 	Basis_ALB as b   ");
            
            if (c.ViewBy == 0)
            {
                //DEfault View
                sqlQuery.AppendLine("   INNER JOIN [Atlas_TimeDepositMigrationDefaultView] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.class = ISNULL(b.classOr,0) AND dv.IlpKey4 = ISNULL(b.IlpKey4, 0) AND b.acc_TypeOr = 8  ");
            }
            else if (c.ViewBy == 2)
            {
                //Classification
                sqlQuery.AppendLine("  INNER JOIN [Atlas_Classification] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.AccountTypeId = ISNULL(b.Acc_TypeOr,0) AND dv.rbcTier = ISNULL(b.classOr, 0) AND dv.isAsset = b.isAsset AND b.acc_TypeOr = 8  ");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	INNER JOIN [Atlas_TimeDepositMigrationTermBuckets] as tb ");
                sqlQuery.AppendLine(" 	ON tb.IlpKey5 = ISNULL(b.IlpKey5, 0)  ");
            }

            sqlQuery.AppendLine(" 	LEFT JOIN Basis_ALP3 AS alp3 ON alp3.code = b.code AND alp3.SimulationId = b.SimulationId AND alp3.scenario = '" + priorScenario + "' AND alp3.month > '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "' AND alp3.Month <= '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "'   ");
            sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP4 AS alp4 ON alp4.code = b.code AND alp4.SimulationId = b.SimulationId AND alp4.scenario = '" + priorScenario + "' AND alp4.month > '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "' AND alp4.Month <= '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "'   ");
            sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP5 AS alp5 ON alp5.code = b.code AND alp5.SimulationId = b.SimulationId AND alp5.scenario = '" + priorScenario + "' AND alp5.month > '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "' AND alp5.Month <= '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "'   ");
            sqlQuery.AppendLine(" 	WHERE b.acc_TypeOr = 8 AND b.SimulationId = " + priorSimulationId + " AND  b.exclude = 0   AND b.cat_type IN " + catTypeIn + "   ");

            if (exclude != "")
            {
                sqlQuery.AppendLine(" AND b.classOr " + exclude);
            }

            if (regSpecExclude != "")
            {
                sqlQuery.AppendLine(" AND (b.IlpKey4 " + regSpecExclude + ")");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	GROUP BY dv.name, tb.name ");
            }
            else
            {
                //Classification
                sqlQuery.AppendLine(" 	GROUP BY b.name ");
            }

           
            sqlQuery.AppendLine(" 	) as pm ");
            sqlQuery.AppendLine(" 	ON pm.groupingName = p.groupingName ");

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	AND pm.termBucketName = p.termBucketName ");
            }

           
            sqlQuery.AppendLine(" 	FULL OUTER JOIN ");
            sqlQuery.AppendLine(" 	  ( ");
            sqlQuery.AppendLine(" 	  SELECT   ");

            if (c.ViewBy == 1)
            {
                sqlQuery.AppendLine(" 	b.name as groupingName ");
            }
            else
            {
                sqlQuery.AppendLine(" 	dv.name as groupingName ");
                sqlQuery.AppendLine(" 	,tb.name as termBucketName ");
            }

            sqlQuery.AppendLine("  		,SUM(a.end_Bal) as startingBal   ");
            sqlQuery.AppendLine("  		,SUM(a.End_Bal * a.Total_End_Rate) / NULLIF(SUM(a.End_Bal),  0) as startingRate   ");
            sqlQuery.AppendLine("  		FROM Basis_ALA as a   ");
            sqlQuery.AppendLine("  		INNER JOIN Basis_ALB as b ON a.code = b.code and a.SimulationId = b.SimulationId AND b.Acc_TypeOR = 8 AND a.simulationId = " + simulationId + " AND a.month = '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ");
            
            if (c.ViewBy == 0)
            {
                //DEfault View
                sqlQuery.AppendLine("   INNER JOIN [Atlas_TimeDepositMigrationDefaultView] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.class = ISNULL(b.classOr,0) AND dv.IlpKey4 = ISNULL(b.IlpKey4, 0) AND b.acc_TypeOr = 8  ");
            }
            else if (c.ViewBy == 2)
            {
                //Classification
                sqlQuery.AppendLine("  INNER JOIN [Atlas_Classification] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.AccountTypeId = ISNULL(b.Acc_TypeOr,0) AND dv.rbcTier = ISNULL(b.classOr, 0) AND dv.isAsset = b.isAsset AND b.acc_TypeOr = 8  ");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	INNER JOIN [Atlas_TimeDepositMigrationTermBuckets] as tb ");
                sqlQuery.AppendLine(" 	ON tb.IlpKey5 = ISNULL(b.IlpKey5, 0)  ");
                
            }

            sqlQuery.AppendLine(" WHERE b.exclude = 0   AND b.cat_type IN " + catTypeIn + "  ");
            if (exclude != "")
            {
                sqlQuery.AppendLine(" AND b.classOr " + exclude);
            }

            if (regSpecExclude != "")
            {
                sqlQuery.AppendLine(" AND (b.IlpKey4 " + regSpecExclude + ")");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	GROUP BY dv.name,tb.name ");
            }
            else
            {
                sqlQuery.AppendLine(" 	GROUP BY b.name ");
            }


            sqlQuery.AppendLine(" 	) as c ");
            sqlQuery.AppendLine(" 	ON c.groupingName = ISNULL(pm.groupingName, p.groupingName) ");

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" AND c.termBucketName = ISNULL(pm.termBucketName, p.termBucketName) ");
            }
            
            sqlQuery.AppendLine(" 	FULL OUTER JOIN	  ");
            sqlQuery.AppendLine(" 	(SELECT   ");


            if (c.ViewBy == 1)
            {
                sqlQuery.AppendLine(" 	b.name as groupingName ");
            }
            else
            {
                sqlQuery.AppendLine(" 	dv.name as groupingName ");
                sqlQuery.AppendLine(" 	,tb.name as termBucketName ");
            }

            sqlQuery.AppendLine(" 		,SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)) AS  maturting   ");
            sqlQuery.AppendLine(" 		,SUM((ISNULL(alp3.MatEBal, 0) * ISNULL(alp3.MatERate, 0)) + (ISNULL(alp4.AmtEBal, 0) * ISNULL(alp4.AmtERate, 0)) + (ISNULL(alp5.PayEBal, 0) * ISNULL(alp5.PayERate, 0))) / NULLIF(SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)), 0) AS  maturingRate   ");
            sqlQuery.AppendLine("  	FROM  ");
            sqlQuery.AppendLine(" 	Basis_ALB as b   ");
            
            if (c.ViewBy == 0)
            {
                //DEfault View
                sqlQuery.AppendLine("   INNER JOIN [Atlas_TimeDepositMigrationDefaultView] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.class = ISNULL(b.classOr,0) AND dv.IlpKey4 = ISNULL(b.IlpKey4, 0) AND b.acc_TypeOr = 8  ");
            }
            else if (c.ViewBy == 2)
            {
                //Classification
                sqlQuery.AppendLine("  INNER JOIN [Atlas_Classification] as dv  ");
                sqlQuery.AppendLine(" 	ON dv.AccountTypeId = ISNULL(b.Acc_TypeOr,0) AND dv.rbcTier = ISNULL(b.classOr, 0) AND dv.isAsset = b.isAsset AND b.acc_TypeOr = 8  ");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	INNER JOIN [Atlas_TimeDepositMigrationTermBuckets] as tb ");
                sqlQuery.AppendLine(" 	ON tb.IlpKey5 = ISNULL(b.IlpKey5, 0)  ");
            }

            sqlQuery.AppendLine(" 	LEFT JOIN Basis_ALP3 AS alp3 ON alp3.code = b.code AND alp3.SimulationId = b.SimulationId AND alp3.scenario = '" + scenario + "' AND alp3.month > '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' AND alp3.Month <=  DATEADD(m, DATEDIFF(m, '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "', '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ), '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "')   ");
            sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP4 AS alp4 ON alp4.code = b.code AND alp4.SimulationId = b.SimulationId AND alp4.scenario = '" + scenario + "' AND alp4.month > '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' AND alp3.Month <=  DATEADD(m, DATEDIFF(m, '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "', '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ), '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "')   ");
            sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP5 AS alp5 ON alp5.code = b.code AND alp5.SimulationId = b.SimulationId AND alp5.scenario = '" + scenario + "' AND alp5.month > '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' AND alp3.Month <=  DATEADD(m, DATEDIFF(m, '" + Utilities.GetFirstOfMonthStr(priorInfo.AsOfDate.ToShortDateString()) + "', '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ), '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "')   ");
            sqlQuery.AppendLine(" 	WHERE b.acc_TypeOr = 8 AND b.SimulationId = " + simulationId + " AND b.exclude = 0   AND b.cat_type IN " + catTypeIn + "  ");

            if (exclude != "")
            {
                sqlQuery.AppendLine(" AND b.classOr " + exclude);
            }

            if (regSpecExclude != "")
            {
                sqlQuery.AppendLine(" AND (b.IlpKey4 " + regSpecExclude + ")");
            }

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	GROUP BY dv.name, tb.name ");
            }
            else
            {
                //Classification
                sqlQuery.AppendLine(" 	GROUP BY b.name ");
            }

            sqlQuery.AppendLine(" 	) as cm ");
            sqlQuery.AppendLine(" 	ON cm.groupingName =  ISNULL(ISNULL(pm.groupingName, p.groupingName), c.groupingName) ");

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" AND cm.termBucketName = ISNULL(ISNULL(pm.termBucketName, p.termBucketName), c.termBucketName) ");
            }
            
            sqlQuery.AppendLine("  ");

            if (c.ViewBy != 1)
            {
                sqlQuery.AppendLine(" 	FULL OUTER JOIN ");
                sqlQuery.AppendLine(" 	(SELECT ");
                sqlQuery.AppendLine(" 		alldvs.name as groupingName ");
                sqlQuery.AppendLine(" 		,allTerms.name as termBucketName ");
                sqlQuery.AppendLine(" 		,allDvs.priority as groupPriority ");
                sqlQuery.AppendLine(" 		,allterms.priority as termPriority ");
                sqlQuery.AppendLine(" 		FROM ");


                if (c.ViewBy == 0)
                {
                    //DEfault View
                    sqlQuery.AppendLine(" 	(SELECT DISTINCT name, priority FROM [Atlas_TimeDepositMigrationDefaultView]) as alldvs ");
                }
                else if (c.ViewBy == 2)
                {
                    //Classification
                    sqlQuery.AppendLine(" 	(SELECT DISTINCT name, aco.[Order] as priority FROM [Atlas_Classification] as ac INNER JOIN ATLAS_ClassificationOrder as aco ON ac.AccountTypeId = aco.Acc_Type AND ac.rbctier = aco.rbc_tier WHERE AccountTypeId = 8) as alldvs ");
                }


                sqlQuery.AppendLine(" 	INNER JOIN ");
                sqlQuery.AppendLine(" 	(SELECT DISTINCT name, priority FROM [Atlas_TimeDepositMigrationTermBuckets]) as allTerms ");
                sqlQuery.AppendLine(" 	ON 1 = 1) as allgroupings ");
                sqlQuery.AppendLine(" 	ON allgroupings.groupingName =  ISNULL(ISNULL(ISNULL(p.groupingName, pm.groupingName),c.groupingName), cm.groupingName) ");

                sqlQuery.AppendLine(" 	AND allGroupings.termBucketNAme = ISNULL(ISNULL(ISNULL(p.termBucketName, pm.termBucketName), c.termBucketName), cm.termBucketName)  ");
               
                sqlQuery.AppendLine("  ");

     
                sqlQuery.AppendLine(" 	ORDER BY allgroupings.groupPriority, allgroupings.termPriority ");
  
                
            }
            else
            {
                sqlQuery.AppendLine(" FULL OUTER JOIN ");
                sqlQuery.AppendLine(" (SELECT MAX(name) as groupingName, MAX(sequence) as sequence, MAX(Cat_Type) as Cat_Type, isAsset as isAsset FROM Basis_ALB as b WHERE simulationId in (" + simulationId + ", " + priorSimulationId + ") AND acc_typeOr = 8 AND b.exclude = 0   AND (b.cat_type IN (2, 3, 4, 6, 7)  ");

                if (exclude != "" || regSpecExclude != "")
                {
                    sqlQuery.AppendLine(" OR (b.cat_type in (0,1,5) ");
                    if (exclude != "")
                    {
                        sqlQuery.AppendLine(" AND b.classOr " + exclude);
                    }

                    if (regSpecExclude != "")
                    {
                        sqlQuery.AppendLine(" AND (b.IlpKey4 " + regSpecExclude + ")");
                    }
                    sqlQuery.AppendLine(" )");
                }
                else
                {
                    sqlQuery.AppendLine(" OR b.cat_type in (0,1,5)");
                }
                sqlQuery.AppendLine(")");


                sqlQuery.AppendLine(" GROUP BY b.code, b.isAsset ");
                sqlQuery.AppendLine(" ) as allgroupings ");
                sqlQuery.AppendLine(" 	ON allgroupings.groupingName =  ISNULL(ISNULL(ISNULL(p.groupingName, pm.groupingName),c.groupingName), cm.groupingName) ");
                sqlQuery.AppendLine(" ORDER BY allgroupings.sequence ");
            }

            DataTable data = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), sqlQuery.ToString());

            if (c.ViewBy == 1)
            {
                var totalingItems = new LinkedList<BalanceSheetTotaler.TotalingItem>();
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("priorBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("currentBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("priorRate", BalanceSheetTotaler.TotalingType.Average, "priorBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("currentRate", BalanceSheetTotaler.TotalingType.Average, "currentBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("priorMatBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("currentMatBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("priorMatRate", BalanceSheetTotaler.TotalingType.Average, "priorMatBal"));
                totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("currentMatRate", BalanceSheetTotaler.TotalingType.Average, "currentMatBal"));

                data = BalanceSheetTotaler.CalcTotalsFor(data, totalingItems, BalanceSheetTotaler.ViewByType.SubAccounts);

            }
            int monthDiff = ((curInfo.AsOfDate.Year - priorInfo.AsOfDate.Year) * 12) + curInfo.AsOfDate.Month - priorInfo.AsOfDate.Month;
            DateTime newDate = curInfo.AsOfDate.AddMonths(monthDiff);

            return new
            {
                table = data,
                viewBy = c.ViewBy,
                asOfDate = curInfo.AsOfDate.ToShortDateString(),
                priorDate = priorInfo.AsOfDate.ToShortDateString(),
                cdMatStartDate = curInfo.AsOfDate.AddDays(1).ToShortDateString(),
                cdMatEndDate = new DateTime(newDate.Year, newDate.Month, DateTime.DaysInMonth(newDate.Year, newDate.Month)).ToShortDateString()

            };
        }
    }
}