﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atlas.Institution.Data;

namespace Atlas.Web.Services
{
    public class ActualService
    {
        private readonly InstitutionContext _gctx = new InstitutionContext();

        public object BalanceSheetMixViewModel(int reportId)
        {
            var balanceSheetMix = _gctx.BalanceSheetMixes.Find(reportId);
            if (balanceSheetMix == null) throw new Exception("balanceSheetMix Not Found");
            return new { ViewModel = "placeholder"};
        }

    }
}