﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class StaticGapService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object StaticGapViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            GlobalController gc = new GlobalController();
            //Look Up Funding Matrix Settings
            var staticGap = gctx.StaticGaps.Where(s => s.Id == reportId).FirstOrDefault();
            var report = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();

            //If Not Found Kick Out
            if (report == null) throw new Exception("Report Not Found");

            //If Not Found Kick Out
            if (staticGap == null) throw new Exception("Static Gap Not Found");

            //Left Sutff(Top)
            var leftInstService = new InstitutionService(staticGap.LeftInstitutionDatabaseName);
            var leftInfo = leftInstService.GetInformation(report.Package.AsOfDate, staticGap.LeftAsOfDateOffset);
            List<Simulation> leftSims = leftInfo.Simulations.ToList();
            List<Scenario> curScens;
            string leftAod = leftInfo.AsOfDate.ToString("MM/dd/yyyy");
            bool foundBaseScen = false;
            Simulation curSim;
            Scenario curScen;
            Dictionary<string, DataTable> leftPolicyOffsets = gc.PolicyOffsets(staticGap.LeftInstitutionDatabaseName);
            int simTypeToUse = -1;
            SimulationType leftSimulationTypeToUse;
            int scenTypeToUse = -1;
            ScenarioType leftScenarioTypeToUse;
            if (staticGap.LeftOverrideSimulationId)
            {
                simTypeToUse = staticGap.LeftSimulationTypeId;
            }
            else
            {
                simTypeToUse = Convert.ToInt32(leftPolicyOffsets[leftAod].Rows[0]["niiId"].ToString());
            }
            leftSimulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeToUse).FirstOrDefault();
            if (staticGap.LeftOverrideScenarioId)
            {
                scenTypeToUse = staticGap.LeftScenarioTypeId;
            }
            else
            {
                if (leftPolicyOffsets[leftAod].Rows[0]["niiId"].ToString() == leftPolicyOffsets[leftAod].Rows[0]["bsSimulation"].ToString())
                {
                    scenTypeToUse = Convert.ToInt32(leftPolicyOffsets[leftAod].Rows[0]["bsScenario"].ToString());
                }
                else
                {
                    //look for base scenario
                    for (int i = 0; i < leftSims.Count && !foundBaseScen; i++)
                    {
                        curSim = leftSims[i];
                        if (curSim.SimulationTypeId == simTypeToUse)
                        {
                            curScens = curSim.Scenarios.ToList();
                            for (int j = 0; j < curScens.Count && !foundBaseScen; j++)
                            {
                                curScen = curScens[j];
                                if (j == 0)
                                {

                                    //set the scenario to use to the first scenario we find in the simulation. If Base gets found, it will be set to that
                                    scenTypeToUse = curScen.ScenarioTypeId;
                                }
                                if (curScen.ScenarioTypeId == 1)
                                {
                                    scenTypeToUse = 1;
                                    foundBaseScen = true;
                                }
                            }
                        }
                    }
                }
            }
            leftScenarioTypeToUse = gctx.ScenarioTypes.Where(s => s.Id == scenTypeToUse).FirstOrDefault();
            var leftObj = leftInstService.GetSimulationScenario(report.Package.AsOfDate, staticGap.LeftAsOfDateOffset, simTypeToUse, scenTypeToUse);
            DataTable leftDt = new DataTable();
            if (
                leftObj.GetType().GetProperty("simulation").GetValue(leftObj).ToString() == "" ||
                leftObj.GetType().GetProperty("scenario").GetValue(leftObj).ToString() == "" ||
                leftObj.GetType().GetProperty("asOfDate").GetValue(leftObj).ToString() == ""
            )
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), leftScenarioTypeToUse.Name, leftSimulationTypeToUse.Name, staticGap.LeftAsOfDateOffset, report.Package.AsOfDate.ToShortDateString()));
            }
            else
            {
                leftDt = StaticGapDataTable(leftInstService, simTypeToUse, scenTypeToUse, report.Package.AsOfDate, staticGap.LeftAsOfDateOffset, staticGap.ReportFormat, staticGap.ViewOption, staticGap.TaxEqivalentYields, leftSimulationTypeToUse.Name, leftScenarioTypeToUse.Name);
            }
            //

            //Right Stuff(Bottom)
            var rightInstService = new InstitutionService(staticGap.RightInstitutionDatabaseName);
            DataTable rightDt = new DataTable();
            var rightInfo = rightInstService.GetInformation(report.Package.AsOfDate, staticGap.RightAsOfDateOffset);
            List<Simulation> rightSims = rightInfo.Simulations.ToList();
            //List<Scenario> curScens;
            string rightAod = rightInfo.AsOfDate.ToString("MM/dd/yyyy");
            foundBaseScen = false;
            //Simulation curSim;
            //Scenario curScen;
            Dictionary<string, DataTable> rightPolicyOffsets = gc.PolicyOffsets(staticGap.LeftInstitutionDatabaseName);
            simTypeToUse = -1;
            SimulationType rightSimulationTypeToUse;
            scenTypeToUse = -1;
            ScenarioType rightScenarioTypeToUse;
            if (staticGap.RightOverrideSimulationId)
            {
                simTypeToUse = staticGap.RightSimulationTypeId;
            }
            else
            {
                simTypeToUse = Convert.ToInt32(rightPolicyOffsets[rightAod].Rows[0]["niiId"].ToString());
            }
            rightSimulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeToUse).FirstOrDefault();
            if (staticGap.RightOverrideScenarioId)
            {
                scenTypeToUse = staticGap.RightScenarioTypeId;
            }
            else
            {
                if (rightPolicyOffsets[rightAod].Rows[0]["niiId"].ToString() == rightPolicyOffsets[leftAod].Rows[0]["bsSimulation"].ToString())
                {
                    scenTypeToUse = Convert.ToInt32(rightPolicyOffsets[rightAod].Rows[0]["bsScenario"].ToString());
                }
                else
                {
                    //look for base scenario
                    for (int i = 0; i < rightSims.Count && !foundBaseScen; i++)
                    {
                        curSim = rightSims[i];
                        if (curSim.SimulationTypeId == simTypeToUse)
                        {
                            curScens = curSim.Scenarios.ToList();
                            for (int j = 0; j < curScens.Count && !foundBaseScen; j++)
                            {
                                curScen = curScens[j];
                                if (j == 0)
                                {

                                    //set the scenario to use to the first scenario we find in the simulation. If Base gets found, it will be set to that
                                    scenTypeToUse = curScen.ScenarioTypeId;
                                }
                                if (curScen.ScenarioTypeId == 1)
                                {
                                    scenTypeToUse = 1;
                                    foundBaseScen = true;
                                }
                            }
                        }
                    }
                }
            }
            rightScenarioTypeToUse = gctx.ScenarioTypes.Where(s => s.Id == scenTypeToUse).FirstOrDefault();
            var rightObj = rightInstService.GetSimulationScenario(report.Package.AsOfDate, staticGap.RightAsOfDateOffset, simTypeToUse, scenTypeToUse);

            if (
                rightObj.GetType().GetProperty("simulation").GetValue(rightObj).ToString() == "" ||
                rightObj.GetType().GetProperty("scenario").GetValue(rightObj).ToString() == "" ||
                rightObj.GetType().GetProperty("asOfDate").GetValue(rightObj).ToString() == ""
            )
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), rightScenarioTypeToUse.Name, rightSimulationTypeToUse.Name, staticGap.RightAsOfDateOffset, report.Package.AsOfDate.ToShortDateString()));
            }
            else
            {

                rightDt = StaticGapDataTable(rightInstService, simTypeToUse, scenTypeToUse, report.Package.AsOfDate, staticGap.RightAsOfDateOffset, staticGap.ReportFormat, staticGap.ViewOption, staticGap.TaxEqivalentYields, rightSimulationTypeToUse.Name, rightScenarioTypeToUse.Name);
            }

            return new
            {
                schedule = staticGap.ReportFormat,
                viewOption = staticGap.ViewOption,
                gapRatios = staticGap.GapRatios,
                leftTable = leftDt,
                leftDate = leftInfo.AsOfDate.ToShortDateString(),
                leftSimulation = leftSimulationTypeToUse.Name,
                leftScenario = leftScenarioTypeToUse.Name,
                leftInstName = Utilities.InstName(staticGap.LeftInstitutionDatabaseName),
                leftInstDBName = staticGap.LeftInstitutionDatabaseName,
                rightTable = rightDt,
                rightDate = rightInfo.AsOfDate.ToShortDateString(),
                rightSimulation = rightSimulationTypeToUse.Name,
                rightScenario = rightScenarioTypeToUse.Name,
                rightInstName = Utilities.InstName(staticGap.RightInstitutionDatabaseName),
                rightInstDBName = staticGap.RightInstitutionDatabaseName,
                warnings = warnings,
                errors = errors
            };
        }

        private DataTable StaticGapDataTable(InstitutionService instService, int simulationTypeId, int scenarioTypeId, DateTime packageAsOfDate, int dateOffset, string schedule, string grouping, bool taxEquiv, string simulationName, string scenarioName)
        {


            var obj = instService.GetSimulationScenario(packageAsOfDate, dateOffset, simulationTypeId, scenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" g.monthGrouping ");
            sqlQuery.AppendLine(" ,g.isAsset ");

            switch (grouping)
            {
                case "0":
                    sqlQuery.AppendLine(" ,g.name as categoryName ");
                    break;
                case "1":
                    sqlQuery.AppendLine(" ,at.name as accountName ");
                    sqlQuery.AppendLine(" ,cl.name as classificationName ");
                    break;
                case "2":
                    sqlQuery.AppendLine(" ,at.name as accountName ");
                    break;
                default:
                    if (grouping.IndexOf("dg") != -1)
                    {
                        sqlQuery.AppendLine(" ,g.name as name ");
                    }
                    else
                    {
                        return new DataTable();
                    }

                    break;
            }

            sqlQuery.AppendLine(" ,g.balance ");

            if (taxEquiv)
            {
                sqlQuery.AppendLine(" ,g.rate + g.taxCreditYield as rate");
            }
            else
            {
                sqlQuery.AppendLine(" ,g.rate ");
            }


            sqlQuery.AppendLine(" ,g.oneDayBal ");
            sqlQuery.AppendLine(" ,g.oneDayRate ");
            sqlQuery.AppendLine(" FROM ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" (SELECT  ");
            sqlQuery.AppendLine(" g.monthGrouping   ");
            sqlQuery.AppendLine(" ,g.isAsset   ");

            switch (grouping)
            {
                case "0":
                    sqlQuery.AppendLine(" ,g.name ");
                    sqlQuery.AppendLine(" ,MAX(g.Sequence) as sequence ");
                    break;
                case "1":
                    sqlQuery.AppendLine(" ,g.Acc_Type  ");
                    sqlQuery.AppendLine(" ,g.rbcTier ");
                    break;
                case "2":
                    sqlQuery.AppendLine(" ,g.Acc_Type  ");
                    break;
                default:
                    sqlQuery.AppendLine(" ,dgs.name ");
                    sqlQuery.AppendLine(" ,MAX(dgs.[Priority]) as [priority] ");
                    break;
            }

            sqlQuery.AppendLine(" ,SUM(balance) as balance   ");
            sqlQuery.AppendLine(" ,CASE WHEN SUM(balance) <> 0 THEN SUM(weightedRate) / SUM(balance) ELSE 0 END as rate   ");
            sqlQuery.AppendLine(" ,CASE WHEN SUM(balance) <> 0 THEN SUM(weightedTaxCreditYield) / SUM(balance) ELSE 0 END as taxCreditYield     ");
            sqlQuery.AppendLine("  ,SUM(g.OneDaySGapBal ) as oneDayBal ");
            sqlQuery.AppendLine(" ,CASE WHEN SUM(g.oneDaySGapBal) <> 0 THEN SUM(g.OneDaySGapBal * g.oneDaySGapRate) / SUM(g.oneDaySGapBal) ELSE 0 END as oneDayRate ");
            sqlQuery.AppendLine(" FROM  	 ");
            sqlQuery.AppendLine(" (SELECT   ");
            sqlQuery.AppendLine(" 	sm.month   ");
            sqlQuery.AppendLine(" 	,CASE    ");
            sqlQuery.AppendLine(" 		WHEN sm.month = DATEADD(d, 1, sm.asOfDate) THEN   ");
            sqlQuery.AppendLine(" 			sm.balance - sm.OneDaySGapBal   ");
            sqlQuery.AppendLine(" 		ELSE   ");
            sqlQuery.AppendLine(" 			sm.balance   ");
            sqlQuery.AppendLine(" 	END as balance   ");
            sqlQuery.AppendLine(" 	,CASE    ");
            sqlQuery.AppendLine(" 		WHEN sm.month = DATEADD(d, 1, sm.asOfDate) THEN   ");
            sqlQuery.AppendLine(" 			sm.weightedRate - (sm.OneDaySGapBal * sm.OneDaySGapRate)   ");
            sqlQuery.AppendLine(" 		ELSE   ");
            sqlQuery.AppendLine(" 			sm.weightedRate   ");
            sqlQuery.AppendLine(" 	END as weightedRate ");
            sqlQuery.AppendLine(" 	,sm.weightedTaxCreditYield   ");
            sqlQuery.AppendLine(" 	,sm.nMonths   ");
            sqlQuery.AppendLine(" 	,sm.OneDaySGapBal   ");
            sqlQuery.AppendLine(" 	,sm.OneDaySGapRate   ");
            sqlQuery.AppendLine(" 	,sm.asOfDate   ");


            switch (grouping)
            {
                case "0":
                    sqlQuery.AppendLine(" 	,sm.Sequence ");
                    sqlQuery.AppendLine("   ,sm.Name ");
                    break;
                case "1":
                    sqlQuery.AppendLine(" 	,sm.Acc_Type ");
                    sqlQuery.AppendLine(" 	,sm.rbcTier ");
                    break;
                case "2":
                    sqlQuery.AppendLine(" 	,sm.Acc_Type ");
                    break;
                default:
                    sqlQuery.AppendLine(" 		,sm.rbcTier ");
                    sqlQuery.AppendLine(" 		,sm.Acc_Type  ");
                    break;
            }


            //sqlQuery.AppendLine(" 	,CASE   ");
            //sqlQuery.AppendLine(" 		WHEN sm.month <= DATEADD(m, 3,  sm.asOfDate) THEN   ");
            //sqlQuery.AppendLine(" 			REPLACE(RIGHT(CONVERT(VARCHAR(9), sm.month, 6), 6), ' ', '-')   ");
            //sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 6, sm.asOfDate) THEN   ");
            //sqlQuery.AppendLine(" 			'4-6 Months'   ");
            //sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 12, sm.asOfDate) THEN   ");
            //sqlQuery.AppendLine(" 			'7-12 Months'   ");
            //sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 24, sm.asOfDate) THEN   ");
            //sqlQuery.AppendLine(" 			'13-24 Months'   ");


            //if (schedule.Equals("3"))
            //{
            //summary schedule has diff buckets starting here
            //Months 25-60
            //sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN   ");
            //sqlQuery.AppendLine(" 			'25-60 Months'   ");
            //Over 60 Months
            //sqlQuery.AppendLine(" 		ELSE   ");
            //sqlQuery.AppendLine(" 			'Over 60 Months'   ");
            //}
            //else
            //{

            //    sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN   ");
            //    sqlQuery.AppendLine(" 			'37-60 Months'   ");

            //    switch (schedule)
            //    {
            //        case "0":
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 24, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'13-24 Months'   ");
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'25-36 Months'   ");
            //            sqlQuery.AppendLine(" 		ELSE   ");
            //            sqlQuery.AppendLine(" 			'Over 60 Months'   ");
            //            break;
            //        case "1":
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'13-36 Months'   ");
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 120, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'61-120 Months'   ");
            //            sqlQuery.AppendLine(" 		ELSE   ");
            //            sqlQuery.AppendLine(" 			'Over 120 Months'   ");
            //            break;
            //        case "2":
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'13-36 Months'   ");
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 120, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'61-120 Months'   ");
            //            sqlQuery.AppendLine(" 		WHEN sm.month < DATEADD(m, 180, sm.asOfDate) THEN   ");
            //            sqlQuery.AppendLine(" 			'121-180 Months'   ");
            //            sqlQuery.AppendLine(" 		ELSE   ");
            //            sqlQuery.AppendLine(" 			'Over 180 Months'   ");
            //            break;
            //    }
            //}
            sqlQuery.AppendLine(GetStaticGapMonthBreakout(schedule));


            sqlQuery.AppendLine(" 		END as monthGrouping   ");
            sqlQuery.AppendLine(" 		,IsAsset as IsAsset   ");
            sqlQuery.AppendLine(" 	FROM   ");
            sqlQuery.AppendLine(" 	(	 ");
            sqlQuery.AppendLine(" 	 ");
            sqlQuery.AppendLine(" 	 ");
            sqlQuery.AppendLine(" 	SELECT   ");
            sqlQuery.AppendLine(" 		CASE WHEN ps.period = 1 THEN CAST(ep.month as datetime) ELSE DATEADD(m, ps.period - 1, CAST(ep.month as datetime)) END as month   ");
            sqlQuery.AppendLine(" 		,ep.SGapBal / ep.nMonths as Balance   ");

            if (taxEquiv)
            {
                sqlQuery.AppendLine(" 		,DWR_FedRates.dbo.[TaxEquivilentYield](bb.LocDivExclu, bb.StaDivExclu, bb.FedDivExclu, bb.LocTaxExempt, bb.StaTaxExempt, bb.FedTaxExempt, teyld.localAmount, teyld.stateAmount, teyld.fedAmount,  ep.SGapRate) * (ep.sGapBal / ep.NMonths) as  weightedRate   ");
            }
            else
            {
                sqlQuery.AppendLine(" 		,ep.SGapRate * (ep.sGapBal / ep.NMonths) as  weightedRate   ");
            }
            //ep.SGapTaxCreditYield  * (ep.sGapBal / ep.NMonths) as weightedTaxCreditYield
            sqlQuery.AppendLine(" 		,ep.SGapTaxCreditYield  * (ep.sGapBal / ep.NMonths) as weightedTaxCreditYield   ");

            sqlQuery.AppendLine(" 		,ep.NMonths   ");
            sqlQuery.AppendLine(" 		,ep.code   ");
            sqlQuery.AppendLine(" 		,bb.Name ");

            switch (grouping)
            {
                case "0":
                    sqlQuery.AppendLine(" 		,bb.Sequence ");
                    break;
                case "1":
                    sqlQuery.AppendLine(" 		,bb.classOr as rbcTier ");
                    sqlQuery.AppendLine(" 		,bb.acc_TypeOr as Acc_Type  ");
                    break;
                case "2":
                    sqlQuery.AppendLine(" 		,bb.acc_TypeOr as Acc_Type   ");
                    break;
                default:
                    sqlQuery.AppendLine(" 		,bb.classOr as rbcTier ");
                    sqlQuery.AppendLine(" 		,bb.acc_TypeOr as Acc_Type  ");
                    break;

            }

            sqlQuery.AppendLine(" 		,sc.OneDaySGapBal   ");
            sqlQuery.AppendLine(" 		,sc.OneDaySGapRate    ");
            sqlQuery.AppendLine(" 		,CAST('" + asOfDate + "' as datetime) as asOfDate   ");
            sqlQuery.AppendLine(" 		,bb.IsAsset   ");
            sqlQuery.AppendLine(" 		FROM   ");
            sqlQuery.AppendLine(" 		(SELECT   ");
            sqlQuery.AppendLine(" 			simulationId   ");
            sqlQuery.AppendLine(" 			,scenario   ");
            sqlQuery.AppendLine(" 			,code ");
            sqlQuery.AppendLine(" 			,Month   ");
            sqlQuery.AppendLine(" 			,SGapBal   ");
            sqlQuery.AppendLine(" 			,SGapRate   ");
            sqlQuery.AppendLine(" 			,SGapTaxCreditYield   ");
            sqlQuery.AppendLine(" 			,NMonths   ");
            sqlQuery.AppendLine(" 			FROM Basis_ALP6 WHERE simulationId = " + simulationId + " AND scenario = '" + scenario + "') as ep   ");
            sqlQuery.AppendLine(" 			INNER JOIN   ");
            sqlQuery.AppendLine(" 			ATLAS_PeriodSmoother as ps   ");
            sqlQuery.AppendLine(" 			ON ep.NMonths >= ps.period   ");
            sqlQuery.AppendLine(" 			INNER JOIN Basis_ALS as sc   ");
            sqlQuery.AppendLine(" 			ON sc.simulationId = ep.SimulationId AND   ");
            sqlQuery.AppendLine(" 			sc.scenario = ep.scenario AND   ");
            sqlQuery.AppendLine(" 			sc.code = ep.code   ");
            sqlQuery.AppendLine(" 			INNER JOIN Basis_ALB as bb   ");
            sqlQuery.AppendLine(" 			ON ep.SimulationId = bb.SimulationId    ");
            sqlQuery.AppendLine(" 			AND ep.code = bb.code	  ");
            sqlQuery.AppendLine(" 			INNER JOIN   ");
            sqlQuery.AppendLine(" 			(SELECT  ");
            sqlQuery.AppendLine(" 				g.month  ");
            sqlQuery.AppendLine(" 				,SUM(localAmount) as localAmount  ");
            sqlQuery.AppendLine(" 				,SUM(stateAmount) as stateAmount  ");
            sqlQuery.AppendLine(" 				,SUM(fedAmount) as fedAmount  ");
            sqlQuery.AppendLine(" 				FROM   ");
            sqlQuery.AppendLine(" 					(SELECT month,amount as localAmount, 0.0 as stateAmount, 0.0 as fedAmount FROM Basis_OtherP WHERE code =   ");
            sqlQuery.AppendLine(" 					(SELECT TOP 1 [Code] FROM [Basis_OTHERB] where sequence = 137 and simulationId = " + simulationId + ") AND simulationId = " + simulationId + " and scenario = '" + scenario + "'  ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" 					UNION ALL  ");
            sqlQuery.AppendLine(" 					SELECT month, 0.0 as localAmount, amount as stateAmount, 0.0 as fedAmount FROM Basis_OtherP WHERE code =    ");
            sqlQuery.AppendLine(" 					(SELECT TOP 1 [Code] FROM [Basis_OTHERB] where sequence = 138 and simulationId = " + simulationId + ") AND simulationId = " + simulationId + " and scenario = '" + scenario + "'  ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" 					UNION ALL   ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" 					SELECT month, 0.0 as localAmount, 0.0 as stateAmount, amount as fedAmount FROM Basis_OtherP WHERE code =    ");
            sqlQuery.AppendLine(" 					(SELECT TOP 1 [Code] FROM [Basis_OTHERB] where sequence = 139 and simulationId = " + simulationId + ")  AND simulationId = " + simulationId + " and scenario = '" + scenario + "'  ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" 					) as g  ");
            sqlQuery.AppendLine(" 					GROUP BY month) as teyld ON  ");
            sqlQuery.AppendLine(" 			teyld.month = ep.month 			   ");
            sqlQuery.AppendLine(" 			WHERE sc.exclude = 0 AND bb.exclude = 0 AND bb.Cat_Type in (0,1,5)  ) as sm) as g ");


            switch (grouping)
            {
                case "0":
                    sqlQuery.AppendLine(" 			GROUP BY g.monthGrouping, g.isAsset,  g.name, g.sequence) as g ");
                    sqlQuery.AppendLine(" 			ORDER BY g.IsAsset DESC, g.sequence ");
                    break;
                case "1":
                    sqlQuery.AppendLine(" 			GROUP BY g.monthGrouping, g.isAsset,  g.Acc_Type, g.RbcTier) as g ");
                    sqlQuery.AppendLine(" 			INNER JOIN Atlas_AccountType AS at ");
                    sqlQuery.AppendLine(" 			ON  at.id = g.acc_Type ");
                    sqlQuery.AppendLine(" 			INNER JOIN Atlas_Classification AS cl ");
                    sqlQuery.AppendLine(" 			ON cl.IsAsset = g.isAsset AND cl.AccountTypeId = at.id AND cl.RbcTier = g.RbcTier ");
                    sqlQuery.AppendLine(" 			INNER JOIN Atlas_AccountTypeOrder AS ato ON ");
                    sqlQuery.AppendLine(" 			ato.Acc_Type = g.Acc_Type AND ato.isAsset = g.isAsset ");
                    sqlQuery.AppendLine(" 			INNER JOIN Atlas_ClassificationOrder as clo ON ");
                    sqlQuery.AppendLine(" 			clo.Acc_Type = g.acc_Type AND clo.RBC_Tier = g.RbcTier  ");
                    sqlQuery.AppendLine(" 			ORDER BY g.IsAsset DESC, ato.[Order], clo.[Order] ");
                    break;
                case "2":
                    sqlQuery.AppendLine(" 			GROUP BY g.monthGrouping, g.isAsset,  g.Acc_Type) as g ");
                    sqlQuery.AppendLine(" 			INNER JOIN Atlas_AccountType AS at ");
                    sqlQuery.AppendLine(" 			ON  at.id = g.acc_Type ");
                    sqlQuery.AppendLine(" 			INNER JOIN Atlas_AccountTypeOrder AS ato ON ");
                    sqlQuery.AppendLine(" 			ato.Acc_Type = g.Acc_Type AND ato.isAsset = g.isAsset ");
                    sqlQuery.AppendLine(" 			ORDER BY g.IsAsset DESC, ato.[Order] ");
                    break;
                default:

                    if (grouping.IndexOf("dg") != -1)
                    {
                        int id = int.Parse(grouping.Substring(3));
                        sqlQuery.AppendLine("			INNER JOIN ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine("			(SELECT  ");
                        sqlQuery.AppendLine("				dgg.name ");
                        sqlQuery.AppendLine("				,dgg.[Priority] ");
                        sqlQuery.AppendLine("				,dgc.IsAsset  ");
                        sqlQuery.AppendLine("				,dgc.Acc_Type  ");
                        sqlQuery.AppendLine("				,dgc.RbcTier  ");
                        sqlQuery.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                        sqlQuery.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                        sqlQuery.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                        sqlQuery.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                        sqlQuery.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                        sqlQuery.AppendLine("			  WHERE dg.id = " + id.ToString() + ") as dgs ON ");
                        sqlQuery.AppendLine("			  dgs.acc_Type = g.acc_Type AND dgs.rbcTier = g.rbcTier AND dgs.IsAsset = g.isAsset ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine("			GROUP BY g.monthGrouping, g.isAsset,  dgs.name, dgs.[priority]) as g ");
                        sqlQuery.AppendLine("           ORDER BY g.isAsset DESC, g.name, g.[priority]  ");


                    }
                    else
                    {
                        return new DataTable();
                    }





                    break;
            }
            DataTable temp = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
            return Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
        }
        string GetStaticGapMonthBreakout(string schedule)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" 	,CASE   ");
            sb.AppendLine(" 		WHEN sm.month <= DATEADD(m, 3,  sm.asOfDate) THEN   ");
            sb.AppendLine(" 			REPLACE(RIGHT(CONVERT(VARCHAR(9), sm.month, 6), 6), ' ', '-')   ");
            sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 6, sm.asOfDate) THEN   ");
            sb.AppendLine(" 			'4-6 Months'   ");
            sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 12, sm.asOfDate) THEN   ");
            sb.AppendLine(" 			'7-12 Months'   ");

            switch (schedule)
            {
                //Schedule J
                case "0":
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 24, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'13-24 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'25-36 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'37-60 Months'   ");
                    sb.AppendLine(" 		ELSE   ");
                    sb.AppendLine(" 			'Over 60 Months'   ");
                    break;
                //Scehdule H
                case "1":
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'13-36 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'37-60 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 120, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'61-120 Months'   ");
                    sb.AppendLine(" 		ELSE   ");
                    sb.AppendLine(" 			'Over 120 Months'   ");
                    break;

                //extended Schedule H
                case "2":
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'13-36 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'37-60 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 120, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'61-120 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 180, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'121-180 Months'   ");
                    sb.AppendLine(" 		ELSE   ");
                    sb.AppendLine(" 			'Over 180 Months'   ");
                    break;
                //Summary
                case "3":
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 24, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'13-24 Months'   ");
                    sb.AppendLine(" 		WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN   ");
                    sb.AppendLine(" 			'25-60 Months'   ");
                    sb.AppendLine(" 		ELSE   ");
                    sb.AppendLine(" 			'Over 60 Months'   ");
                    break;
            }
            return sb.ToString();
        }
    }
}