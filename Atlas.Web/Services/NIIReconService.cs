﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace Atlas.Web.Services
{
    public class NIIReconService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object CheckNIISimulation(string instDBName, DateTime aod, int aodOffset, SimulationType simType )
        {
            var instService = new InstitutionService(instDBName);
            var sim = instService.GetSimulation(aod, aodOffset, simType);
            if (sim == null) {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), simType.Name, aodOffset, aod.ToShortDateString()));
            }
            return new{
                errors = errors,
                warnings = warnings
            };
        }
       /* public C1XLBook NIIReconSheet(int reportId, NIIRecon nr, string timePeriod, Report rep)
        {

            var instService = new InstitutionService(nr.InstitutionDatabaseName);
            var sim = instService.GetSimulation(rep.Package.AsOfDate, nr.AsOfDateOffset, nr.SimulationType);
            string conStr = Utilities.BuildInstConnectionString(nr.InstitutionDatabaseName);

            

            //Open copy in memory
            C1XLBook xl = new C1XLBook();
            string path = HttpContext.Current.Server.MapPath("../../NIIReconciliation.xlsx");
            xl.Load(path, FileFormat.OpenXml);

            if (sim == null) {
                //errors.Add(String.Format("Simulation {0} not found at offset {1} from package As Of Date {2}", nr.SimulationType.Name, nr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                throw new Exception(String.Format(Utilities.GetErrorTemplate(2), nr.SimulationType.Name, nr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                xl.Sheets[1][2, 1].Value = Utilities.InstName(nr.InstitutionDatabaseName);
                xl.Sheets[1][3, 1].Value = sim.Information.AsOfDate.ToShortDateString();


                if (nr.TimePeriod == "90 Day") {
                    xl.Sheets[1][4, 1].Value = "quarter";
                }
                else {
                    xl.Sheets[1][4, 1].Value = "year";
                }

                //Get NII Values
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("		aac.Name ");
                sqlQuery.AppendLine("	    ,acl.name as className ");
                sqlQuery.AppendLine("		,scenNAmes.Name as scenName   ");
                sqlQuery.AppendLine("		,MAX(ni.priority) as CellPosition ");
                sqlQuery.AppendLine("		,MAX(nrs.priority) as ColPosition ");
                sqlQuery.AppendLine(" 	,SUM(nii.IeTotAmount) as Amount  ");
                sqlQuery.AppendLine(" 	FROM         ");
                sqlQuery.AppendLine("		(SELECT simulationid  ");
                sqlQuery.AppendLine("				,month  ");
                sqlQuery.AppendLine("				,scenario  ");
                sqlQuery.AppendLine("				,code  ");
                if (nr.TaxEquiv) {
                    sqlQuery.AppendLine("		       ,AveTEAmt as IETotAmount");
                }
                else {
                    sqlQuery.AppendLine("		        ,IETotAmount  ");
                }

                sqlQuery.AppendLine("		FROM Basis_ALP13 WHERE simulationId = " + sim.Id.ToString() + " ");
                if (nr.TimePeriod == "90 Day") {
                    sqlQuery.AppendLine("		AND month <= DATEADD(m, 3, '" + sim.Information.AsOfDate.ToShortDateString() + "') ");
                }
                else {
                    sqlQuery.AppendLine("		AND month <= DATEADD(m, 12, '" + sim.Information.AsOfDate.ToShortDateString() + "') ");
                }

                sqlQuery.AppendLine("		) as nii ");
                sqlQuery.AppendLine("		INNER JOIN Basis_ALB as alb ON   ");
                sqlQuery.AppendLine("		alb.simulationId = nii.simulationId and alb.code = nii.code  ");
                sqlQuery.AppendLine("		INNER JOIN Basis_ALS as als ON  ");
                sqlQuery.AppendLine("		als.simulationId = nii.simulationId and als.code = nii.code and als.scenario = nii.scenario  ");
                sqlQuery.AppendLine("		INNER JOIN Basis_Scenario  as scen ON  ");
                sqlQuery.AppendLine("		scen.number = als.scenario AND scen.SimulationId = als.SimulationId   ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_ScenarioType as scenNames ON  ");
                sqlQuery.AppendLine("		scennames.Id = scen.ScenarioTypeId   ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_AccountType AS aac ");
                sqlQuery.AppendLine("		ON aac.Id = alb.Acc_TypeOR  ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_Classification AS acl ");
                sqlQuery.AppendLine("		ON acl.RbcTier = alb.classOr AND acl.AccountTypeId = alb.Acc_TypeOR  ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_NIIReconOrder  AS Ni ");
                sqlQuery.AppendLine("		ON ni.isAsset = alb.isASset and ni.Acc_Type = alb.Acc_TypeOR AND ni.RBC_Tier = alb.ClassOR and acl.IsAsset = alb.isAsset     ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_NIIReconScenarioType as nrs ON ");
                sqlQuery.AppendLine("		nrs.NIIReconId = " + nr.Id.ToString() + " and nrs.ScenarioTypeId = scenNames.id ");
                sqlQuery.AppendLine("		WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5)  ");
                sqlQuery.AppendLine("		GROUP BY aac.Name ");
                sqlQuery.AppendLine("				,acl.name ");
                sqlQuery.AppendLine("				,scenNAmes.Name  	 ");
                sqlQuery.AppendLine("		ORDER BY scenNAmes.name ");
                DataTable nii = Utilities.ExecuteSql(conStr, sqlQuery.ToString());


                foreach (DataRow dr in nii.Rows) {
                    if (dr["className"].ToString() == "C & I") {
                        for (int x = 0; x < 2; x++) {
                            Console.WriteLine(x);
                        }
                    }

                    int xPos = int.Parse(dr["cellPosition"].ToString());
                    int yPos = int.Parse(dr["colPosition"].ToString()) + 1;
                    yPos = yPos * 2;
                    //xl.Sheets[1][26 + xPos, yPos + 2].Value = "NII FOR " + dr["scenName"].ToString();
                    xl.Sheets[1][26 + xPos, yPos + 2].Value = double.Parse(dr["Amount"].ToString());
                }


                //Get Average Balance Values
                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("		aac.Name ");
                sqlQuery.AppendLine("	    ,acl.name as className ");
                sqlQuery.AppendLine("		,scenNAmes.Name  as scenName  ");
                sqlQuery.AppendLine("		,MAX(ni.priority) as CellPosition ");
                sqlQuery.AppendLine("		,MAX(nrs.priority) as ColPosition ");

                if (nr.TimePeriod == "90 Day") {
                    int days = 0;
                    for (int i = 1; i <= 3; i++) {
                        DateTime dt = sim.Information.AsOfDate.AddMonths(i);
                        days += DateTime.DaysInMonth(dt.Year, dt.Month);
                    }

                    sqlQuery.AppendLine(" 	  ,SUM(av.daysInMonth * av.AveBal) / " + days.ToString() + " as Amount  ");
                }
                else {
                    sqlQuery.AppendLine(" 	  ,SUM(av.daysInMonth * av.AveBal) / 365 as Amount  ");
                }

                sqlQuery.AppendLine(" 	FROM         ");
                sqlQuery.AppendLine("		(SELECT simulationid  ");
                sqlQuery.AppendLine("				,month  ");
                sqlQuery.AppendLine("				,scenario  ");
                sqlQuery.AppendLine("				,code  ");
                sqlQuery.AppendLine("		       --,AveTEAmt as IETotAmount  --Tax Rquic ");
                sqlQuery.AppendLine("		        ,AveBal ");
                sqlQuery.AppendLine("				,DAY(EOMONTH(month)) as daysInMonth ");
                sqlQuery.AppendLine("		FROM Basis_ALP1 WHERE simulationId = " + sim.Id.ToString() + " ");

                if (nr.TimePeriod == "90 Day") {
                    sqlQuery.AppendLine("		AND month <= DATEADD(m, 3, '" + sim.Information.AsOfDate.ToShortDateString() + "') ");
                }
                else {
                    sqlQuery.AppendLine("		AND month <= DATEADD(m, 12, '" + sim.Information.AsOfDate.ToShortDateString() + "') ");
                }
                sqlQuery.AppendLine("		) as av ");
                sqlQuery.AppendLine("		INNER JOIN Basis_ALB as alb ON   ");
                sqlQuery.AppendLine("		alb.simulationId = av.simulationId and alb.code = av.code  ");
                sqlQuery.AppendLine("		INNER JOIN Basis_ALS as als ON  ");
                sqlQuery.AppendLine("		als.simulationId = av.simulationId and als.code = av.code and als.scenario = av.scenario  ");
                sqlQuery.AppendLine("		INNER JOIN Basis_Scenario  as scen ON  ");
                sqlQuery.AppendLine("		scen.number = als.scenario AND scen.SimulationId = als.SimulationId   ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_ScenarioType as scenNames ON  ");
                sqlQuery.AppendLine("		scennames.Id = scen.ScenarioTypeId   ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_AccountType AS aac ");
                sqlQuery.AppendLine("		ON aac.Id = alb.Acc_TypeOR  ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_Classification AS acl ");
                sqlQuery.AppendLine("		ON acl.RbcTier = alb.classOr AND acl.AccountTypeId = alb.Acc_TypeOR  and acl.IsAsset = alb.isAsset   ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_NIIReconOrder  AS Ni ");
                sqlQuery.AppendLine("		ON ni.isAsset = alb.isASset and ni.Acc_Type = alb.Acc_TypeOR AND ni.RBC_Tier = alb.ClassOR  ");
                sqlQuery.AppendLine("		INNER JOIN ATLAS_NIIReconScenarioType as nrs ON ");
                sqlQuery.AppendLine("		nrs.NIIReconId = " + nr.Id.ToString() + " and nrs.ScenarioTypeId = scenNames.id ");
                sqlQuery.AppendLine("		WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5)  ");
                sqlQuery.AppendLine("		GROUP BY aac.Name ");
                sqlQuery.AppendLine("				,acl.name ");
                sqlQuery.AppendLine("				,scenNAmes.Name  	 ");
                sqlQuery.AppendLine("		ORDER BY scenNAmes.name ");
                DataTable avg = Utilities.ExecuteSql(conStr, sqlQuery.ToString());
                foreach (DataRow dr in avg.Rows) {
                    if (dr["className"].ToString() == "Overnight") {
                        for (int x = 0; x < 2; x++) {
                            Console.WriteLine(x);
                        }
                    }
                    int xPos = int.Parse(dr["cellPosition"].ToString());
                    int yPos = int.Parse(dr["colPosition"].ToString()) + 1;

                    yPos = (yPos * 2) - 1;

                    xl.Sheets[1][26 + xPos, yPos + 2].Value = double.Parse(dr["Amount"].ToString());
                    //xl.Sheets[1][26 + xPos, yPos + 2].Value = "AVG FOR " + dr["scenName"].ToString();

                    xl.Sheets[1][25, yPos + 2].Value = dr["scenName"].ToString();
                }
            }

            

            //xl.Sheets(2)(i + 2, 0).Value = FormatAccountType(dr("depClass").ToString, dr("timeIndex").ToString) 'Name
            //xl.Sheets(2)(i + 2, 1).Value = ParseDouble(dr("balance").ToString()) 'Balance
            //xl.Sheets(2)(i + 2, 2).Value = ParseDouble(dr("rate").ToString()) / 100 'Rate
            //xl.Sheets(2)(i + 2, 3).Value = ParseDouble(dr("avgLife12M").ToString()) 'Base 12
            //xl.Sheets(2)(i + 2, 4).Value = ParseDouble(dr("avgLife36M").ToString()) 'Base 36
            //xl.Sheets(2)(i + 2, 5).Value = ParseDouble(dr("avgLife12MStress").ToString()) 'Stress 12
            //xl.Sheets(2)(i + 2, 6).Value = ParseDouble(dr("avgLife36MStress").ToString()) 'Stress 36

            return xl;
        }*/
    }
}