﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class ASC825DiscountRateService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object ASC825DiscountRateViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.ASC825DiscountRates.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("ASC825 Discount Rate Report Not Found");


            DataTable tblData = new DataTable();
            DataTable monthLabels = new DataTable();

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            object obj = instService.GetSimulationScenario(rep.Package.AsOfDate, sr.AsOfDateOffset, sr.SimulationTypeId, sr.ScenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();

            if (simulationId == "" || scenario == "" || asOfDate == "") {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), sr.ScenarioType.Name, sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }else {
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	idxp.month ");
                sqlQuery.AppendLine(" 	,idxb.name ");
                sqlQuery.AppendLine(" 	,idxp.rate ");
                sqlQuery.AppendLine(" 	,idxp.offset ");
                sqlQuery.AppendLine(" 	,idxb.type ");
                sqlQuery.AppendLine(" 	,idxb.specialType ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" (SELECT code, name, Sequence, type, Special_Type as specialType FROM Basis_IndexB WHERE simulationid = " + simulationId + " ) AS idxB ");
                sqlQuery.AppendLine(" INNER JOIN ");
                sqlQuery.AppendLine(" (SELECT code, rate, month, offset FROM Basis_IndexP WHERE simulationId = " + simulationId + " and scenario = '" + scenario + "') AS idxp ");
                sqlQuery.AppendLine(" ON idxb.code = idxp.code  ");
                sqlQuery.AppendLine(" INNER JOIN ");
                sqlQuery.AppendLine(" (SELECT code, SUM(Rate) as totOff FROM Basis_IndexP WHERE simulationId = " + simulationId + " and scenario = '" + scenario + "' GROUP BY Code)  AS nonz ");
                sqlQuery.AppendLine(" ON nonz.code = idxp.code  ");
                sqlQuery.AppendLine(" WHERE nonz.totOff > 0 ");
                sqlQuery.AppendLine(" ORDER BY idxb.type, idxb.Sequence, idxp.month ");

                tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());
                monthLabels = Utilities.ProjectionLabels(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), "Basis_IndexP", simulationId, scenario, "", asOfDate);
            }


            
          

            return new
            {
                tableData = tblData,
                monthLabels = monthLabels,
                errors = errors,
                warnings = warnings
            };
        }
    }
}