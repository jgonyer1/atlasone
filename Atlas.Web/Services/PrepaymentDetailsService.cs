﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class PrepaymentDetailsService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        private readonly InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

        public object PrepaymentDetailsViewModel(int reportId)
        {

            
            string conStr = "";
            var conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
            {
                InitialCatalog = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString()
            };
            conStr = conenctionStringBuilder.ConnectionString;
            InstitutionContext gctx = new InstitutionContext(conStr);
            var pd = gctx.PrepaymentDetails.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();

            //make a new connection string for the institution selected
            conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
            {
                InitialCatalog = pd.InstitutionDatabaseName
            };
            conStr = conenctionStringBuilder.ConnectionString;

            //If Not Found Kick Out
            if (pd == null) throw new Exception("Simulation Summary Not Found");
            
            DataTable[] scens = new DataTable[pd.PrePaymentDetailScenarioTypes.Count];
            DataTable[] lblTables = new DataTable[pd.PrePaymentDetailScenarioTypes.Count];
            string[] scenarioNames = new string[pd.PrePaymentDetailScenarioTypes.Count];
            string AOD = rep.Package.AsOfDate.ToString();
            var instService = new InstitutionService(pd.InstitutionDatabaseName);
            AOD = instService.GetInformation(rep.Package.AsOfDate, pd.AsOfDateOffset).AsOfDate.ToString();
            //string simulationId = instService.GetSimulation(DateTime.Parse(AOD), pd.AsOfDateOffset, pd.SimulationType).Id.ToString();

            int counter = 0;
            foreach (var scen in pd.PrePaymentDetailScenarioTypes.OrderBy(s => s.Priority))
            {
                var obj = instService.GetSimulationScenario(DateTime.Parse(AOD), pd.AsOfDateOffset, pd.SimulationType.Id, scen.ScenarioType.Id);
                string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
                string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();

                if (simulationId.Length > 0 && scenario.Length > 0)
                {
                    string query = GetQuery(AOD, simulationId, scenario);

                    scens[counter] = Utilities.ExecuteSql(instService.ConnectionString(), query);
                    lblTables[counter] = Utilities.ProjectionLabels(conStr, "Basis_ALP1", simulationId, scenario, "", AOD);
                    scenarioNames[counter] = scen.ScenarioType.Name;
                    counter++;
                }
                else 
                {
                    errors.Add(String.Format(Utilities.GetErrorTemplate(1), scen.ScenarioType.Name, pd.SimulationType.Name, pd.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                }
            }

            return new
            {
                scenarioDataTables = scens,
                lblTables = lblTables,
                scenarios = scenarioNames,
                warnings = warnings,
                errors = errors
                
            };
        }

        private string GetQuery(string aod, string simulationId, string scenario)
        {
            StringBuilder q = new StringBuilder();

            q.AppendLine("SELECT ");
            q.AppendLine("	Sequence");
            q.AppendLine("	,Name");
            q.AppendLine("	,albs.code");
            q.AppendLine("	,cast(alps.month as datetime) as month");
            q.AppendLine("	,CASE WHEN eSpeed is null then 'New' ELSE 'Exist' END as NEString");
            q.AppendLine("	,'('+CASE WHEN eSpeed is null THEN");
            q.AppendLine("		CASE ");
            q.AppendLine("		   WHEN PayNPrepay = 0 THEN 'None'");
            q.AppendLine("		   WHEN PayNPrepay = 1 THEN 'User Entered'");
            q.AppendLine("		   WHEN PayNPrepay = 2 THEN 'Prepay Index'");
            q.AppendLine("		   WHEN PayNPrepay = 3 THEN 'Prepay Matrix'");
            q.AppendLine("		   ELSE 'None'");
            q.AppendLine("		END");
            q.AppendLine("	ELSE");
            q.AppendLine("		CASE ");
            q.AppendLine("		   WHEN PayEPrepay = 0 THEN 'None'");
            q.AppendLine("		   WHEN PayEPrepay = 1 THEN 'User Entered'");
            q.AppendLine("		   WHEN PayEPrepay = 2 THEN 'Prepay Index'");
            q.AppendLine("		   WHEN PayEPrepay = 3 THEN 'BK'");
            q.AppendLine("		   WHEN PayEPrepay = 4 THEN 'Decay'");
            q.AppendLine("		   WHEN PayEPrepay = 5 THEN 'Prepay Matrix'");
            q.AppendLine("		   WHEN PayEPrepay = 6 THEN 'ILP'");
            q.AppendLine("		   ELSE 'None'");
            q.AppendLine("		END");
            q.AppendLine("	END + ')'");
            q.AppendLine("	AS Calc");
            q.AppendLine("	,CASE WHEN eSpeed is null THEN");
            q.AppendLine("		CASE ");
            q.AppendLine("		   WHEN NewPCType = 0 then 'PSA'");
            q.AppendLine("		   WHEN NewPCType = 1 then 'CPR'");
            q.AppendLine("		   ELSE 'None'");
            q.AppendLine("		END");
            q.AppendLine("	ELSE");
            q.AppendLine("		CASE ");
            q.AppendLine("		   WHEN ExistPCType = 0 then 'PSA'");
            q.AppendLine("		   WHEN ExistPCType = 1 then 'CPR'");
            q.AppendLine("		   ELSE 'None'");
            q.AppendLine("		END ");
            q.AppendLine("	END AS PCType");
            q.AppendLine("	,sum(eSpeed) as eSpeed");
            q.AppendLine("	,sum(eBal) as ebal");
            q.AppendLine("	,sum(nSpeed) as nSpeed");
            q.AppendLine("	,sum(nBal) as nBal");
            q.AppendLine("  ,isnull(eSpeed, nSpeed) as endBal");
            q.AppendLine(" FROM");
            q.AppendLine("(select");
            q.AppendLine("	sequence");
            q.AppendLine("	,alb.name");
            q.AppendLine("	,alb.code");
            q.AppendLine("	,alb.simulationId");
            q.AppendLine("	,als.Scenario");
            q.AppendLine("	,PayEPrePay");
            q.AppendLine("	,PayNPrepay");
            q.AppendLine("	,ExistPCType");
            q.AppendLine("	,NewPCType");
            q.AppendLine("	from ");
            q.AppendLine("	(select sequence,name,simulationId, code, exclude from basis_alb where SimulationId = "+ simulationId +" and exclude = 0) as alb");
            q.AppendLine("	inner join");
            q.AppendLine("	(select scenario, code, PayNPrepay, PayEPrePay, SimulationId, ExistPCType, NewPCType from basis_als where scenario ='"+ scenario +"' and (PayEPrePay <> 0 or PayNPrePay <> 0) and exclude = 0) as als");
            q.AppendLine("	on alb.code = als.code and alb.SimulationId = als.SimulationId");
            q.AppendLine(") albs");
            q.AppendLine("inner join");
            q.AppendLine("(SELECT ");
            q.AppendLine("  code");
            q.AppendLine("  ,simulationId");
            q.AppendLine("  ,month");
            q.AppendLine(",scenario");
            q.AppendLine(",eSpeed");
            q.AppendLine(",eBal");
            q.AppendLine(",nSpeed");
            q.AppendLine(",nBal");
            q.AppendLine("FROM");
            q.AppendLine("(");
            q.AppendLine("		SELECT");
            q.AppendLine("	       ISNULL(alp11.code, alp5.code) as code");
            q.AppendLine("	       ,ISNULL(alp11.simulationId, alp5.simulationid) as simulationId");
            q.AppendLine("	       ,ISNULL(alp11.month, alp5.month) as month");
            q.AppendLine("		   ,ISNULL(alp11.scenario, alp5.scenario) as scenario");
            q.AppendLine("	       ,alp5.PAyEPSACPR as eSpeed");
            q.AppendLine("	       ,alp5.PayEBalX as eBal");
            q.AppendLine("	       ,alp11.PAyNPSACPR as nSpeed");
            q.AppendLine("	       ,alp11.PayNBalX as nBal");
            q.AppendLine("	       FROM");
            q.AppendLine("	(select code, scenario, simulationID, month, PAyEPSACPR, PayEBalX  from ");
            q.AppendLine("		(select");
            q.AppendLine("			alp5.code, scenario, simulationId, sum(PAyEPSACPR) as PAyEPSACPR, sum(PayEBalX) as PayEBalX, month ");
            q.AppendLine("			from Basis_ALP5 as alp5 ");
            q.AppendLine("          inner join (select code from");
            q.AppendLine("              (select code,sum(PAyEPSACPR) as summedspeeed, sum(PayEBalX) as summedBal from basis_alp5 where simulationId = "+ simulationId +" and scenario = '"+ scenario +"' group by code) as filter");
            q.AppendLine("              where summedBal <> 0 and summedspeeed <> 0) as filterTbl on alp5.code = filterTbl.Code");
            q.AppendLine("          WHERE  simulationId = " + simulationId + " AND scenario = '" + scenario + "' group by simulationId, scenario, alp5.code, month) as a");
          // q.AppendLine("		WHERE PayEBalX <> 0 and PAyEPSACPR <> 0");
            q.AppendLine("	) as alp5");
            q.AppendLine("	FULL OUTER JOIN ");
            q.AppendLine("	(select code, scenario, simulationID, month, PAyNPSACPR, PayNBalX  from ");
            q.AppendLine("		(select");
            q.AppendLine("			alp11.code, scenario, simulationId, sum(PAyNPSACPR) as PAyNPSACPR, sum(PayNBalX) as PayNBalX, month ");
            q.AppendLine("			from Basis_ALP11 as alp11 ");
            q.AppendLine("          inner join (select code from");
            q.AppendLine("              (select code,sum(PAyNPSACPR) as summedspeeed, sum(PayNBalX) as summedBal from basis_alp11 where simulationId = " + simulationId + " and scenario = '" + scenario + "' group by code) as filter");
            q.AppendLine("              where summedBal <> 0 and summedspeeed <> 0) as filterTbl on alp11.code = filterTbl.Code");
            q.AppendLine("WHERE  simulationId = " + simulationId + " AND scenario = '" + scenario + "' group by simulationId, scenario, alp11.code, month) as a");
           //q.AppendLine("		WHERE PayNBalX <> 0 and PAyNPSACPR <> 0");
            q.AppendLine("	) as alp11");
            q.AppendLine("	ON alp5.simulationId = alp11.simulationId AND alp5.scenario = alp11.scenario AND alp5.month = alp11.month AND alp5.code = alp11.code");
            q.AppendLine(") as tbl");
            q.AppendLine(") as alps");
            q.AppendLine("on albs.code = alps.code and albs.SimulationId = alps.simulationId and albs.scenario = alps.scenario ");
            q.AppendLine("group by month, sequence, Name, albs.code, albs.SimulationId, albs.Scenario,PayEPrePay, PayNPrepay, albs.NewPCType, albs.ExistPCType");
            q.AppendLine(", eSpeed, nSpeed order by  Sequence,cast(month as datetime)");


            return q.ToString();
        }

    }
}