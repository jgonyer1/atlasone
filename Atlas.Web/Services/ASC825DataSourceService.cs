﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class ASC825DataSourceService
    {
        public object ASC825DataSourceViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.ASC825DataSources.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("ASC825 Data Source Not Found");

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            var info = instService.GetInformation(rep.Package.AsOfDate, sr.AsOfDateOffset);

            return new
            {
                asOfDate = info.AsOfDate.ToShortDateString(),
                instName = Utilities.InstName(sr.InstitutionDatabaseName)
                
            };
        }
    }
}