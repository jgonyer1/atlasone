﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Dynamic;
using System.ComponentModel;
using System.Data;

namespace Atlas.Web.Services
{
    public class IncomeService
    {
        private readonly InstitutionContext _gctx = new InstitutionContext();



        public object SimCompareViewModel(int simCompareId, DataTable asOfDates)
        {
            SimCompareService sv = new SimCompareService();
            return sv.SimCompareViewModel(simCompareId, asOfDates);


            //var simCompare = _gctx.SimCompares.Find(simCompareId);
            //if (simCompare == null) throw new ArgumentOutOfRangeException("simCompareId", "SimCompare not found");


            //var twoChart = new SimCompareTwoChart(simCompare);
            //var leftSeriesResult = CalculateSeriesData(twoChart.LeftChart, twoChart.Package, false);
            //var rightSeriesResult = CalculateSeriesData(twoChart.RightChart, twoChart.Package, false);

            //var max = Math.Max(leftSeriesResult.Max, rightSeriesResult.Max);
            //var min = Math.Min(leftSeriesResult.Min, rightSeriesResult.Min);

            //var leftTableRows = leftSeriesResult.TableRows.ToList();
            //var rightTableRows = rightSeriesResult.TableRows.ToList();
            //var compareTableRows = new List<List<double>>();
            //for (var rowNum = 0; rowNum < leftTableRows.Count(); rowNum++)
            //{
            //    var leftRow = leftTableRows[rowNum].ToList();
            //    var rightRow = rightTableRows[rowNum].ToList();
            //    var compareRow = new List<double>();
            //    for (var colNum = 0; colNum < leftRow.Count(); colNum++)
            //    {
            //        compareRow.Add(leftRow[colNum] - rightRow[colNum]);
            //    }
            //    compareTableRows.Add(compareRow);
            //}

            //var vm = new
            //{
            //    LeftChartVm = new
            //    {
            //        leftSeriesResult.SeriesJsonObjects,
            //        min,
            //        max,
            //        TableRows = CreateTableRows(leftSeriesResult.TableRows)
            //    },
            //    rightChartVm = new
            //    {
            //        rightSeriesResult.SeriesJsonObjects,    
            //        min,
            //        max,
            //        TableRows = CreateTableRows(rightSeriesResult.TableRows)
            //    },

            //    TableHeaders = CreateTableheaders(twoChart  .LeftChart.ChartScenarioTypes),
            //    compareTableRows = CreateTableRows(compareTableRows)
            //};
            //return vm;
            //return null;
        }

        

        public static int GetQuarter(DateTime date)
        {
            if (date.Month < 4) return 1;
            if (date.Month < 7) return 2;
            if (date.Month < 10) return 3;
            return 4;
        }


        public static int GetYearRelativeToAsOfDate(DateTime projectionDate, DateTime asOfDate)
        {
            if (projectionDate < asOfDate.AddYears(1)) return 1;
            if (projectionDate < asOfDate.AddYears(2)) return 2;
            if (projectionDate < asOfDate.AddYears(3)) return 3;
            if (projectionDate < asOfDate.AddYears(4)) return 4;
            return 5;
        }

        private int CalculateYAxisMinValue(double minIncomeValue, double maxIncomeValue)
        {
            var calcMin = (int)(Math.Floor(minIncomeValue / 25) * 25) - GetYAxisPaddingAmount(minIncomeValue, maxIncomeValue);
            if (minIncomeValue >= 0 && calcMin < 0) calcMin = 0;
            return calcMin;
        }

        private int CalculateYAxisMaxValue(double minIncomeValue, double maxIncomeValue)
        {
            return Convert.ToInt32(Math.Ceiling(maxIncomeValue / 25) * 25) + GetYAxisPaddingAmount(minIncomeValue, maxIncomeValue);
        }

        private static int GetYAxisPaddingAmount(double minIncomeValue, double maxIncomeValue)
        {
            var paddingAmount = Convert.ToInt32(((maxIncomeValue - minIncomeValue) / 4) / 25) * 25;
            return paddingAmount <= 25 ? 25 : paddingAmount;
        }

        public List<Row> CreateTableRows(List<List<double>> tableRowsData)
        {
            var tableRows = new List<Row>();
            for (var i = 0; i < tableRowsData.Count(); i++)
            {
                var row = tableRowsData[i];
                tableRows.Add(new Row
                {
                    Name = "Year-" + (i + 1),
                    Values = row.Select(niiVal => (niiVal / 1000).ToString("N0")).ToList()
                });
            }
            return tableRows;
        }

        public List<string> CreateTableheaders(ICollection<ChartScenarioType> chartScenarioTypes)
        {
            var tableHeaders = chartScenarioTypes.OrderBy(s => s.Priority).Select(s => s.ScenarioType.Name).ToList();
            tableHeaders.Insert(0, "");
            return tableHeaders;
        }
        //overlaod for SimCompare
        public List<string> CreateTableheaders(List<SimCompareScenarioType> chartScenarioTypes)
        {
            var tableHeaders = chartScenarioTypes.OrderBy(s => s.Priority).Select(s => s.ScenarioType.Name).ToList();
            tableHeaders.Insert(0, "");
            return tableHeaders;
        }

        public object CreateChartJson(Chart chart, List<object> listOfSeriesJsonObjects, double min, double max)
        {
            var quarterlyXaxis = new object[]
            {
                new
                {
                    Name = "Year 1",
                    categories = new string[] {"Q1", "Q2", "Q3", "Q4"}
                },
                new
                {
                    Name = "Year 2",
                    categories = new string[] {"Q1", "Q2", "Q3", "Q4"}
                },
                new
                {
                    Name = "Year 3",
                    categories = new string[] {"Q1", "Q2", "Q3", "Q4"}
                },
                new
                {
                    Name = "Year 4",
                    categories = new string[] {"Q1", "Q2", "Q3", "Q4"}
                },
                new
        {
                    Name = "Year 5",
                    categories = new string[] {"Q1", "Q2", "Q3", "Q4"}
                },
            };

            var monthlyXaxis = new object[]
            {
                new
                {
                    Name = "Year 1",
                    categories = new string[] {"M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12"}
                },
                new
                {
                    Name = "Year 2",
                    categories = new string[] {"M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12"}
                }
            };


            var chartJson = new
           {
                Title = new { Text = chart.Name },
                Credits = new { enabled = false },
               PlotOptions = new { Series = new { Animation = false } },
               Series = listOfSeriesJsonObjects,
               yAxis = new
               {
                    title = new { text = "Net Interest Income ($000)" },
                   min = CalculateYAxisMinValue(min, max),
                   max = CalculateYAxisMaxValue(min, max)
               },
               xAxis = new
               {
                    min = 0,
                    labels = new { style = new { fontSize = "7px" } },
                    categories = chart.IsQuarterly ? quarterlyXaxis : monthlyXaxis
               }
                //xAxis = new
                //{
                //    //min = 1,
                //    TickInterval = 1,
                //    labels = new { format = chart.IsQuarterly ? "Q{value}" : "M{value}" },
                //}
           };
            return chartJson;
        }

        private string SeriesColor(int scenario)
        {
            string color = "";
            switch (scenario)
            {
                case 2:
                case 3:
                case 4:
                case 38:
                    color = "#B71213";
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                    color = "#81BC00";
                    break;
                case 9: 
                case 39: 
                case 40: 
                case 41:
                    color = "#282634";
                    break;
                case 10:
                case 11:
                case 12:
                case 13:
                case 42: 
                case 43: 
                    color = "#611427";
                    break;
                case 14:
                case 15:
                case 44: 
                case 45: 
                    color = "#EC2E48";
                    break;
                case 16:
                case 46: 
                case 47: 
                case 48: 
                    color = "#8A704C";
                    break;
                case 17:
                case 18:
                case 19:
                case 49: 
                    color = "#fa5b0f";
                    break;
                case 20:
                case 21:
                case 22:
                case 23:
                    color = "#464646";
                    break;
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                    color = "#262626";
                    break;
                case 37:
                    color = "#553285";
                    break;
                default:
                     color = "#0069AA";
                    break;
            }

            return color;
        }
    
    
        
    }

    public class SimCompareTableDataPoint
    {
        public SimCompareScenarioType ChartScenarioType { get; set; }
        public int Year { get; set; }
        public double Value { get; set; }
    }

    public class SimCompareNetIncome
    {
        public SimCompareScenarioType ChartScenarioType { get; set; }
        public Simulation Simulation { get; set; }
        public Scenario Scenario { get; set; }
        public DateTime Month { get; set; }
        public double Value { get; set; }

    }

    public class SimCompareChart
    {
        public string Name;
        public bool IsQuarterly;
        public int? ConsolidationId;
        public Consolidation Consolidation;
        public int AsOfDateOffset;
        public List<SimCompareScenarioType> ChartScenarioTypes;
        public List<SimCompareSimulationType> Simulations;

        public SimCompareChart(SimCompare simCompare, bool isLeft){
            //this.IsQuarterly = simCompare.IsQuarterly;
            //this.ChartScenarioTypes = simCompare.SimCompareScenarioTypes.ToList();
            //this.Simulations = new List<SimCompareSimulationType>();
            //if(isLeft){
            //    this.Name = simCompare.LeftGraphName;
            //    this.Consolidation = simCompare.LeftConsolidation;
            //    this.ConsolidationId = simCompare.LeftConsolidationId;
            //    this.AsOfDateOffset = simCompare.LeftAsOfDateOffset;
                
            //    foreach(SimCompareSimulationType scsim in simCompare.SimCompareSimulationTypes){
            //        if(scsim.Side == 0){
            //            this.Simulations.Add(scsim);
            //        }
            //    }
            //}else{
            //    this.Name = simCompare.RightGraphName;
            //    this.Consolidation = simCompare.RightConsolidation;
            //    this.ConsolidationId = simCompare.RightConsolidationId;
            //    this.AsOfDateOffset = simCompare.RightAsOfDateOffset;
            //    foreach(SimCompareSimulationType scsim in simCompare.SimCompareSimulationTypes){
            //        if(scsim.Side == 1){
            //            this.Simulations.Add(scsim);
            //        }
            //    }
            //}
        }

    }
}