﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using System.Text;
using System.Data;

namespace Atlas.Web.Services
{
    public class BasicSurplusReportService
    {
        public object BasicSurplusReportViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            var basicSurplusReport = gctx.BasicSurplusReports.Where(s => s.Id == reportId).FirstOrDefault();
            if (basicSurplusReport == null)
                throw new Exception("BasicSurplusReport Not Found");

            return new { basicSurplusAdmin = basicSurplusReport.BasicSurplusAdmin };
        }
    }
}
