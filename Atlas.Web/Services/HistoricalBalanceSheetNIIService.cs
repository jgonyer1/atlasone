﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{

    //series has a name and a list of values
    public class HistBalSheetChartSeries
    {
        public string Name;
        public string Type;
        public double[] Data;
    }

    public class HistBalSheetNiiChart
    {
        public string ChartName;
        public string[] Categories;
        //list of series
        //there will be one series for each scenario / simulation combo 
        public HistBalSheetChartSeries[] Series;
        public bool IsQuarterly;
    }

    public class HistoricalBalanceSheetNIIService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        private readonly InstitutionContext _gctx = new InstitutionContext();



        public object HistoricalBalanceSheetNIIViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            GlobalController gc = new GlobalController();
            var histNii = gctx.HistoricalBalanceSheetNIIs.Where(s => s.Id == reportId).FirstOrDefault();
            List<ChartSimulationType> leftSimTypes = histNii.LeftChart.ChartSimulationTypes.ToList();
            List<ChartSimulationType> rightSimTypes = histNii.RightChart.ChartSimulationTypes.ToList();
            Dictionary<string, DataTable> policyOffsets = gc.PolicyOffsets(histNii.InstitutionDatabaseName);
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            if (histNii == null) throw new ArgumentOutOfRangeException("reportId", "Historical Balance Sheet Nii not found");

            InstitutionService instService = new InstitutionService(histNii.InstitutionDatabaseName);
            string leftAodStr = instService.GetInformation(rep.Package.AsOfDate, leftSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            string rightAodStr = instService.GetInformation(rep.Package.AsOfDate, rightSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            string instDBName = histNii.InstitutionDatabaseName;

            DataTable balSheetCompareTable = new DataTable();
            balSheetCompareTable.Columns.Add("name", typeof(string));
            for (int i = 0; i < histNii.HistoricalBalanceSheetNIISimulations.Count; i++)
            {
                balSheetCompareTable.Columns.Add("bal_" + i, typeof(double));
                balSheetCompareTable.Columns.Add("rate_" + i, typeof(double));
                balSheetCompareTable.Columns.Add("perc_assets_" + i, typeof(double));
                balSheetCompareTable.Columns.Add("bal_delta_" + i, typeof(double));
                balSheetCompareTable.Columns.Add("rate_delta_" + i, typeof(double));
                balSheetCompareTable.Columns.Add("perc_assets_delta_" + i, typeof(double));
            }

            object[] balanceSheetObjs = new object[histNii.HistoricalBalanceSheetNIISimulations.Count];
            object[] niiObjs = new object[histNii.HistoricalBalanceSheetNIISimulations.Count * histNii.HistoricalBalanceSheetNIIScenarios.Count];
            object[] totalAssets = new object[histNii.HistoricalBalanceSheetNIISimulations.Count];
            object[] totalLiabs = new object[histNii.HistoricalBalanceSheetNIISimulations.Count];
            string simTypeIdToUse = "";
            int simTypeIdInt = 0;
            SimulationType simulationTypeToUse;

            DataTable modelSetupVals = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT ReportTaxEquivalent, ReportTaxEquivalentYield FROM Atlas_ModelSetup WHERE AsOfDate = '" + rep.Package.AsOfDate.ToShortDateString() + "'");
            if (modelSetupVals.Rows.Count > 0 && !histNii.OverrideTaxEquiv)
            {
                histNii.TaxEquivalent = (bool)modelSetupVals.Rows[0]["ReportTaxEquivalentYield"];
            }

            if (modelSetupVals.Rows.Count > 0 && !histNii.OverrideChartsTaxEquivalent)
            {
                histNii.ChartsTaxEquivalent = (bool)modelSetupVals.Rows[0]["ReportTaxEquivalent"];
            }

            if (policyOffsets.ContainsKey(leftAodStr) && !histNii.LeftChart.OverrideParentSimulationType)
            {
                simTypeIdToUse = policyOffsets[leftAodStr].Rows[0]["niiId"].ToString();
                simTypeIdInt = Convert.ToInt32(simTypeIdToUse);
                simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeIdInt).FirstOrDefault();
                leftSimTypes[0].SimulationTypeId = simulationTypeToUse.Id;
                leftSimTypes[0].SimulationType = simulationTypeToUse;
            }

            if (policyOffsets.ContainsKey(rightAodStr) && !histNii.RightChart.OverrideParentSimulationType)
            {
                simTypeIdToUse = policyOffsets[rightAodStr].Rows[0]["niiId"].ToString();
                simTypeIdInt = Convert.ToInt32(simTypeIdToUse);
                simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeIdInt).FirstOrDefault();
                rightSimTypes[0].SimulationTypeId = simulationTypeToUse.Id;
                rightSimTypes[0].SimulationType = simulationTypeToUse;
            }


            //stuff for the chart
            //aodStr = instService.GetInformation(rep.Package.AsOfDate, leftSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            if (!histNii.LeftChart.NameOverride)
            {
                histNii.LeftChart.Name = leftSimTypes[0].SimulationType.Name + " Simulation as of " + leftAodStr;
            }
            List<List<DataTable>> chartData = new List<List<DataTable>>();
            string scenIn = "";
            for (int i = 0; i < histNii.LeftChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList().Count; i++)
            {
                if (i > 0)
                {
                    scenIn += ",";
                }
                scenIn += histNii.LeftChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList()[i].ScenarioTypeId;
            }
            scenIn = "(" + scenIn + ")";

            string dgId = "-1";
            if (histNii.NIIAssetViewOption.IndexOf("dg_") > -1)
            {
                dgId = histNii.NIIAssetViewOption.Substring(3);
            }
            DataTable leftChartData = Utilities.SimulationScenarioTable(histNii.LeftChart.IsQuarterly, rep.Package.AsOfDate, leftSimTypes[0].AsOfDateOffset, leftSimTypes[0].SimulationType, histNii.LeftChart.InstitutionDatabaseName, scenIn, histNii.ChartsTaxEquivalent, dgId);
            scenIn = "";
            for (int i = 0; i < histNii.RightChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList().Count; i++)
            {
                if (i > 0)
                {
                    scenIn += ",";
                }
                scenIn += histNii.RightChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList()[i].ScenarioTypeId;
            }
            scenIn = "(" + scenIn + ")";

            //aodStr = instService.GetInformation(rep.Package.AsOfDate, rightSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            if (!histNii.RightChart.NameOverride)
            {
                histNii.RightChart.Name = rightSimTypes[0].SimulationType.Name + " Simulation as of " + rightAodStr;
            }
            gctx.SaveChanges();

            DataTable rightChartData = Utilities.SimulationScenarioTable(histNii.RightChart.IsQuarterly, rep.Package.AsOfDate, rightSimTypes[0].AsOfDateOffset, rightSimTypes[0].SimulationType, histNii.RightChart.InstitutionDatabaseName, scenIn, histNii.ChartsTaxEquivalent, dgId);

            DataRow[] dataSubset;
            HistBalSheetNiiChart LeftChart = new HistBalSheetNiiChart();
            LeftChart.IsQuarterly = histNii.LeftChart.IsQuarterly;
            LeftChart.ChartName = histNii.LeftChart.Name;
            LeftChart.Series = new HistBalSheetChartSeries[histNii.LeftChart.ChartScenarioTypes.Count];
            if (histNii.LeftChart.IsQuarterly)
            {
                LeftChart.Categories = new string[20];
            }
            else
            {
                LeftChart.Categories = new string[24];
            }

            HistBalSheetNiiChart RightChart = new HistBalSheetNiiChart();
            RightChart.IsQuarterly = histNii.RightChart.IsQuarterly;
            RightChart.ChartName = histNii.RightChart.Name;
            RightChart.Series = new HistBalSheetChartSeries[histNii.RightChart.ChartScenarioTypes.Count];
            if (histNii.RightChart.IsQuarterly)
            {
                RightChart.Categories = new string[20];
            }
            else
            {
                RightChart.Categories = new string[24];
            }

            int drCount = 0;

            for (int sc = 0; sc < histNii.LeftChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList().Count; sc++)
            {
                LeftChart.Series[sc] = new HistBalSheetChartSeries();
                LeftChart.Series[sc].Name = histNii.LeftChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList()[sc].ScenarioType.Name.ToString();
                if (histNii.LeftChart.IsQuarterly)
                {
                    LeftChart.Series[sc].Data = new double[20];
                }
                else
                {
                    LeftChart.Series[sc].Data = new double[24];
                }
                if (histNii.LeftChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList()[sc].ScenarioType.Name.IndexOf("Base") > -1)
                {
                    LeftChart.Series[sc].Type = "column";
                }
                else
                {
                    LeftChart.Series[sc].Type = "line";
                }
                dataSubset = leftChartData.Select("name = '" + LeftChart.Series[sc].Name + "'");
                drCount = 0;
                foreach (DataRow dr in dataSubset)
                {
                    if (sc == 0)
                    {
                        //LeftChart.Categories[drCount] = DateTime.Parse(dr["month"].ToString()).ToString("M/yy");
                        LeftChart.Categories[drCount] = DateTime.Parse(dr["month"].ToString()).ToShortDateString();
                    }
                    LeftChart.Series[sc].Data[drCount] = Double.Parse(dr["Amount"].ToString());
                    drCount++;
                }
            }

            for (int sc = 0; sc < histNii.RightChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList().Count; sc++)
            {
                RightChart.Series[sc] = new HistBalSheetChartSeries();
                RightChart.Series[sc].Name = histNii.RightChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList()[sc].ScenarioType.Name.ToString();
                if (histNii.RightChart.IsQuarterly)
                {
                    RightChart.Series[sc].Data = new double[20];
                }
                else
                {
                    RightChart.Series[sc].Data = new double[24];
                }
                if (histNii.RightChart.ChartScenarioTypes.OrderBy(s => s.Priority).ToList()[sc].ScenarioType.Name.IndexOf("Base") > -1)
                {
                    RightChart.Series[sc].Type = "column";
                }
                else
                {
                    RightChart.Series[sc].Type = "line";
                }
                dataSubset = rightChartData.Select("name = '" + RightChart.Series[sc].Name + "'");
                drCount = 0;
                foreach (DataRow dr in dataSubset)
                {
                    if (sc == 0)
                    {
                        //RightChart.Categories[drCount] = DateTime.Parse(dr["month"].ToString()).ToString("M/yy");
                        RightChart.Categories[drCount] = DateTime.Parse(dr["month"].ToString()).ToShortDateString();
                    }
                    RightChart.Series[sc].Data[drCount] = Double.Parse(dr["Amount"].ToString());
                    drCount++;
                }
            }



            string[] asOfDates = new string[histNii.HistoricalBalanceSheetNIISimulations.Count];
            string[] scenarioNames = new string[histNii.HistoricalBalanceSheetNIIScenarios.Count];
            string compareScenarioName = "";

            int simCounter = 0;
            string filter = "";
            DataRow curRow;
            DataRow tempRow1;
            DataRow tempRow2;
            DataRow[] rows;
            DataTable templateTable = new DataTable();

            //SimulationType simulationTypeToUse;

            string simulationIdToUse = "";
            int simulationIdIntToUse = 0;
            foreach (var simulationType in histNii.HistoricalBalanceSheetNIISimulations.OrderBy(s => s.Priority))
            {
                //this block gets the data use for the upper balanace sheet
                Simulation sim = instService.GetSimulation(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationType.SimulationType);
                simulationTypeToUse = simulationType.SimulationType;

                string aod = instService.GetInformation(rep.Package.AsOfDate, simulationType.AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
                if (!simulationType.OverrideSimulation)
                {
                    if (policyOffsets.Keys.Contains(aod))
                    {
                        simulationIdToUse = policyOffsets[aod].Rows[0]["niiId"].ToString();
                        simulationIdIntToUse = Convert.ToInt32(simulationIdToUse);
                        simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simulationIdIntToUse).FirstOrDefault();
                        sim = instService.GetSimulation(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationTypeToUse);
                    }
                }


                if (sim == null)
                {
                    errors.Add(String.Format(Utilities.GetErrorTemplate(2), simulationType.SimulationType.Name, simulationType.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                }
                else
                {

                    DataTable[] assetsAndLiabs = new DataTable[2];
                    int scenCounter = 0;
                    string simDate = sim.Name + "_" + aod;



                    assetsAndLiabs[0] = BalanceSheetTable(instService, sim.Id.ToString(), aod, "1", histNii.BalanceSheetAssetViewOption, histNii.TaxEquivalent);
                    assetsAndLiabs[1] = BalanceSheetTable(instService, sim.Id.ToString(), aod, "0", histNii.BalanceSheetAssetViewOption, histNii.TaxEquivalent);
                    balanceSheetObjs[simCounter] = new { simDate = simDate, assets = assetsAndLiabs[0], liabilities = assetsAndLiabs[1] };

                    //0: total balance, 1: total weighted rate
                    double[] totalsAss = GetTotals(assetsAndLiabs[0]);
                    double[] totalsLiab = GetTotals(assetsAndLiabs[1]);
                    totalAssets[simCounter] = new { simDate = simDate, total = totalsAss[0], rate = totalsAss[1] };
                    totalLiabs[simCounter] = new { simDate = simDate, total = totalsLiab[0], rate = totalsLiab[1] };

                    for (int k = 0; k < 2; k++)
                    {
                        for (int r = 0; r < assetsAndLiabs[k].Rows.Count; r++)
                        {

                            filter = "name = '" + assetsAndLiabs[k].Rows[r][0].ToString() + "'";
                            rows = balSheetCompareTable.Select(filter);
                            if (rows.Length == 0)
                            {
                                curRow = balSheetCompareTable.NewRow();
                                curRow["name"] = assetsAndLiabs[k].Rows[r][0].ToString();
                            }
                            else
                            {
                                curRow = rows[0];
                            }

                            curRow["bal_" + simCounter] = TryConvertDouble(assetsAndLiabs[k].Rows[r]["endBal"].ToString());
                            curRow["rate_" + simCounter] = TryConvertDouble(assetsAndLiabs[k].Rows[r]["endRate"].ToString());
                            curRow["perc_assets_" + simCounter] = totalsAss[0] == 0 ? 0 : TryConvertDouble(curRow["bal_" + simCounter].ToString()) / totalsAss[0];
                            curRow["bal_delta_" + simCounter] = TryConvertDouble(curRow["bal_0"].ToString()) - TryConvertDouble(curRow["bal_" + simCounter].ToString());
                            curRow["rate_delta_" + simCounter] = TryConvertDouble(curRow["rate_0"].ToString()) - TryConvertDouble(curRow["rate_" + simCounter].ToString());
                            curRow["perc_assets_delta_" + simCounter] = TryConvertDouble(curRow["perc_assets_0"].ToString()) - TryConvertDouble(curRow["perc_assets_" + simCounter].ToString());
                            //curRow["perc_assets_" + simCounter] = TryConvertDouble(assetsAndLiabs[0].Rows[r][3].ToString());

                            if (rows.Length == 0)
                            {
                                balSheetCompareTable.Rows.Add(curRow);
                            }
                        }
                        //Total Rows
                        if (k == 0)
                        {
                            filter = "name = 'Total Assets'";
                            rows = balSheetCompareTable.Select(filter);
                            if (rows.Length == 0)
                            {
                                curRow = balSheetCompareTable.NewRow();
                                curRow["name"] = "Total Assets";
                            }
                            else
                            {
                                curRow = rows[0];
                            }
                            curRow["bal_" + simCounter] = totalsAss[0];
                            curRow["rate_" + simCounter] = totalsAss[1] / 100;
                            curRow["perc_assets_" + simCounter] = 1;
                            curRow["bal_delta_" + simCounter] = TryConvertDouble(curRow["bal_0"].ToString()) - TryConvertDouble(curRow["bal_" + simCounter].ToString());
                            curRow["rate_delta_" + simCounter] = TryConvertDouble(curRow["rate_0"].ToString()) - TryConvertDouble(curRow["rate_" + simCounter].ToString());
                            curRow["perc_assets_delta_" + simCounter] = TryConvertDouble(curRow["perc_assets_0"].ToString()) - TryConvertDouble(curRow["perc_assets_" + simCounter].ToString());

                            if (rows.Length == 0)
                            {
                                balSheetCompareTable.Rows.Add(curRow);
                            }
                        }
                        else
                        {
                            filter = "name = 'Total Liabilities'";
                            rows = balSheetCompareTable.Select(filter);
                            if (rows.Length == 0)
                            {
                                curRow = balSheetCompareTable.NewRow();
                                curRow["name"] = "Total Liabilities";
                            }
                            else
                            {
                                curRow = rows[0];
                            }
                            curRow["bal_" + simCounter] = totalsLiab[0];
                            curRow["rate_" + simCounter] = totalsLiab[1] / 100;
                            curRow["perc_assets_" + simCounter] = 1;
                            curRow["bal_delta_" + simCounter] = TryConvertDouble(curRow["bal_0"].ToString()) - TryConvertDouble(curRow["bal_" + simCounter].ToString());
                            curRow["rate_delta_" + simCounter] = TryConvertDouble(curRow["rate_0"].ToString()) - TryConvertDouble(curRow["rate_" + simCounter].ToString());
                            curRow["perc_assets_delta_" + simCounter] = TryConvertDouble(curRow["perc_assets_0"].ToString()) - TryConvertDouble(curRow["perc_assets_" + simCounter].ToString());

                            if (rows.Length == 0)
                            {
                                balSheetCompareTable.Rows.Add(curRow);
                            }
                        }
                    }

                    //Balance Sheet Spread row

                    rows = balSheetCompareTable.Select("name = 'Balance Sheet Spread'");
                    if (rows.Length == 0)
                    {
                        curRow = balSheetCompareTable.NewRow();
                        curRow["name"] = "Balance Sheet Spread";
                    }
                    else
                    {
                        curRow = rows[0];
                    }

                    tempRow1 = balSheetCompareTable.Select("name = 'Total Assets'")[0];
                    tempRow2 = balSheetCompareTable.Select("name = 'Total Liabilities'")[0];

                    curRow["rate_" + simCounter] = TryConvertDouble(tempRow1["rate_" + simCounter].ToString()) - TryConvertDouble(tempRow2["rate_" + simCounter].ToString());
                    curRow["rate_delta_" + simCounter] = TryConvertDouble(tempRow1["rate_delta_" + simCounter].ToString()) - TryConvertDouble(tempRow2["rate_delta_" + simCounter].ToString());


                    if (rows.Length == 0)
                    {
                        balSheetCompareTable.Rows.Add(curRow);
                    }

                    //loop through the scenarios and get the nii data
                    //this first loop is just to make sure we actually have one set of usable data
                    foreach (var scenarioType in histNii.HistoricalBalanceSheetNIIScenarios.OrderBy(s => s.Priority))
                    {
                        Scenario scen = instService.GetScenario(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationTypeToUse, scenarioType.ScenarioType);
                        if (scen != null)
                        {
                            templateTable.Clear();
                            templateTable = NiiTable(instService, sim.Id.ToString(), scen.Number.ToString(), aod, "0", "0", histNii.ChartsTaxEquivalent);
                            break;
                        }
                    }
                    //loop through the scenarios and get the nii data
                    foreach (var scenarioType in histNii.HistoricalBalanceSheetNIIScenarios.OrderBy(s => s.Priority))
                    {

                        Scenario scen = instService.GetScenario(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationTypeToUse, scenarioType.ScenarioType);
                        if (scen == null)
                        {
                            //errors.Add(String.Format(Utilities.GetErrorTemplate(1), scenarioType.ScenarioType.Name, sim.Name, simulationType.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                            if (templateTable.Rows.Count > 0)
                            {
                                foreach (DataColumn dc in templateTable.Columns)
                                {
                                    dc.ReadOnly = false;
                                }
                                foreach (DataRow dr in templateTable.Rows)
                                {
                                    dr["name"] = scenarioType.ScenarioType.Name;
                                    dr["endBal"] = 0;
                                    int index = (histNii.HistoricalBalanceSheetNIIScenarios.Count * simCounter) + scenCounter;
                                    niiObjs[index] = new { simulation = sim.Name, scenario = scenarioType.ScenarioType.Name, aod = aod, data = templateTable };
                                }

                            }
                        }
                        else
                        {
                            if (scen.ScenarioTypeId.ToString() == histNii.ComparativeScenario)
                            {
                                compareScenarioName = scen.Name;
                            }
                            int index = (histNii.HistoricalBalanceSheetNIIScenarios.Count * simCounter) + scenCounter;
                            niiObjs[index] = new { simulation = sim.Name, scenario = scen.Name, aod = aod, data = NiiTable(instService, sim.Id.ToString(), scen.Number.ToString(), aod, "0", "0", histNii.ChartsTaxEquivalent) };
                            scenCounter++;
                        }

                    }
                }

                simCounter++;
            }



            simCounter = 0;

            foreach (var simulationType in histNii.HistoricalBalanceSheetNIISimulations.OrderBy(s => s.Priority))
            {
                string simulationId = "";
                string scenario = "";
                string asOfDate = "";
                string aod = instService.GetInformation(rep.Package.AsOfDate, simulationType.AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
                foreach (var scenarioType in histNii.HistoricalBalanceSheetNIIScenarios.OrderBy(s => s.Priority))
                {
                    var obj = instService.GetSimulationScenario(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationType.SimulationTypeId, scenarioType.ScenarioTypeId);
                    simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
                    scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
                    asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();
                    if(simulationId.Length + scenario.Length + asOfDate.Length > 0)
                    {
                        templateTable.Clear();
                        templateTable = ChangeAttrTable(instService, simulationId, scenario, asOfDate, histNii.ChangeAttribution, histNii.ChartsTaxEquivalent, "0", "0");
                        break;
                    }
                }
            }

                foreach (var simulationType in histNii.HistoricalBalanceSheetNIISimulations.OrderBy(s => s.Priority))
            {
                string simulationId = "";
                string asOfDate = "";
                string aod = instService.GetInformation(rep.Package.AsOfDate, simulationType.AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
                simulationTypeToUse = simulationType.SimulationType;
                chartData.Add(new List<DataTable>());
                foreach (var scenarioType in histNii.HistoricalBalanceSheetNIIScenarios.OrderBy(s => s.Priority))
                {
                    DataTable cd = new DataTable();
                    var obj = instService.GetSimulationScenario(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationType.SimulationTypeId, scenarioType.ScenarioTypeId);
                    if (!simulationType.OverrideSimulation && policyOffsets.Keys.Contains(aod))
                    {
                        simulationIdIntToUse = Convert.ToInt32(policyOffsets[aod].Rows[0]["niiId"].ToString());
                        obj = instService.GetSimulationScenario(rep.Package.AsOfDate, simulationType.AsOfDateOffset, simulationIdIntToUse, scenarioType.ScenarioTypeId);
                        simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simulationIdIntToUse).FirstOrDefault();
                    }
                    string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
                    if (simulationId.Length == 0)
                    {
                        simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
                    }
                    if (asOfDate.Length == 0)
                    {
                        asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();
                    }
                    if (simulationId == "" || scenario == "" || asOfDate == "")
                    {
                        //errors.Add(String.Format(Utilities.GetErrorTemplate(1), scenarioType.ScenarioType.Name, simulationTypeToUse.Name, simulationType.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                        if (templateTable.Rows.Count > 0)
                        {
                            foreach (DataColumn dc in templateTable.Columns)
                            {
                                dc.ReadOnly = false;
                            }
                            foreach (DataRow dr in templateTable.Rows)
                            {
                                dr["endBal"] = 0;
                            }
                        }
                    }
                    else
                    {
                        cd = ChangeAttrTable(instService, simulationId, scenario, asOfDate, histNii.ChangeAttribution, histNii.ChartsTaxEquivalent, "0", "0");
                    }

                    chartData[simulationType.Priority].Add(cd);

                    if (simCounter == 0)
                    {
                        scenarioNames[scenarioType.Priority] = scenarioType.ScenarioType.Name;
                    }
                }

                asOfDates[simulationType.Priority] = asOfDate;
                simCounter++;
            }



            return new
            {
                balSheetCompareTable = balSheetCompareTable,
                balanceSheetData = balanceSheetObjs,
                totalAssets = totalAssets,
                totalLiabs = totalLiabs,
                nii = niiObjs,
                chartData = chartData,
                leftChart = LeftChart,
                rightChart = RightChart,
                scenarioNames = scenarioNames,
                asOfDates = asOfDates,
                scenarioCount = histNii.HistoricalBalanceSheetNIIScenarios.Count,
                simulationCount = histNii.HistoricalBalanceSheetNIISimulations.Count,
                year2Comp = histNii.Year2Comparative,
                changeAttr = histNii.ChangeAttribution,
                compareScenarioName = compareScenarioName,
                historicalBalanceSheetData = histNii,
                warnings = warnings,
                errors = errors
            };
        }

        public double TryConvertDouble(string input)
        {
            double outVal;
            if (double.TryParse(input, out outVal))
            {
                return outVal;
            }
            else
            {
                return 0;
            }
        }

        public double[] GetTotals(DataTable tbl)
        {
            double total = 0;
            double rate = 0;
            foreach (DataRow dr in tbl.Rows)
            {
                string val = dr["endBal"].ToString();
                total += (double)dr["endBal"];
                rate += (double)dr["endBal"] * (double)dr["endRate"];
            }
            rate = rate / total;
            return new double[] { total, rate };

        }


        public DataTable BalanceSheetTable(InstitutionService instServ, string simulationId, string aod, string al, string grouping, bool taxEquiv)
        {
            StringBuilder groupingTable = new StringBuilder("ATLAS_AccountType as ac on alb.Acc_TypeOR = ac.id");
            StringBuilder sb = new StringBuilder();
            DateTime d = DateTime.Parse(aod);
            string m = d.Month + "/1/" + d.Year;
            string rateField = "end_rate";
            if (taxEquiv)
            {
                rateField = "Total_End_TEYLD";
            }

            if (grouping.IndexOf("dg_") > -1)
            {
                groupingTable.Clear();
                groupingTable.AppendLine("(SELECT");
                groupingTable.AppendLine("	dgg.name ");
                groupingTable.AppendLine("	,dgg.[Priority] ");
                groupingTable.AppendLine("	,dgc.IsAsset  ");
                groupingTable.AppendLine("	,dgc.Acc_Type  ");
                groupingTable.AppendLine("	,dgc.RbcTier  ");
                groupingTable.AppendLine("FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                groupingTable.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                groupingTable.AppendLine("ON dg.id = dgg.DefinedGroupingId  ");
                groupingTable.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                groupingTable.AppendLine("ON dgg.id = dgc.DefinedGroupingGroupId  ");
                groupingTable.AppendLine("WHERE dg.id = " + grouping.Replace("dg_", "") + ") as ac ON ");
                groupingTable.AppendLine("ac.acc_Type = alb.Acc_TypeOR AND ac.rbcTier = alb.ClassOR AND ac.IsAsset = alb.isAsset");
            }

            sb.AppendLine("SELECT");

            if (grouping.IndexOf("dg_") == -1)
            {
                sb.AppendLine("CASE WHEN acc_typeOr = 0 then");
                sb.AppendLine(" CASE WHEN alb.isAsset = 1 then ac.name + ' Asset'");
                sb.AppendLine(" ELSE ac.name + ' Liability'");
                sb.AppendLine("	END");
                sb.AppendLine("ELSE ac.Name");
                sb.AppendLine("END as accountName");
            }
            else
            {
                sb.AppendLine("ac.Name as accountName");
            }

            sb.AppendLine(",ISNULL(month, '"+ m +"') as month");
            sb.AppendLine("       ,alb.isAsset");
            sb.AppendLine("       ,SUM(ISNULL(End_Bal, 0)) as endBal");
            sb.AppendLine("       ,ISNULL(SUM(End_Bal * " + rateField + ") / NULLIF(SUM(End_Bal), 0),0) as endRate");
            //sb.AppendLine("FROM (SELECT code, end_Bal, " + rateField + ", month FROM Basis_ALA WHERE SimulationId = {0} and month <= '" + aod + "' and month > dateadd(m, -1, '" + aod + "')) as ala");
            sb.AppendLine("FROM (SELECT code, end_Bal, " + rateField + ", month FROM Basis_ALA WHERE SimulationId = {0} and month = '" + m + "') as ala");
            sb.AppendLine("       RIGHT JOIN Basis_ALB as alb");
            sb.AppendLine("       ON alb.code = ala.code and alb.simulationId = {0}");
            sb.AppendLine("	   inner join {1}");
            sb.AppendLine("    inner join ATLAS_AccountTypeOrder as ao on alb.isAsset = ao.IsAsset and alb.Acc_TypeOR = ao.Acc_Type");
            sb.AppendLine(" WHERE alb.Cat_Type in (0,1,5) and alb.exclude = 0 and alb.isasset = {2}");
            sb.AppendLine(" GROUP BY ");
            sb.AppendLine("ISNULL(month, '" + m + "')");
            sb.AppendLine(",alb.isAsset       ");

            if (grouping.IndexOf("dg_") == -1)
            {
                sb.AppendLine(",Acc_TypeOr");
            }

            sb.AppendLine(", ac.name");

            if (grouping.IndexOf("dg_") == -1)
            {
                sb.AppendLine(",ao.[order]");
                sb.AppendLine("Order By ao.[order]");
            }
            else
            {
                sb.AppendLine(",ac.Priority");
                sb.AppendLine("Order By ac.Priority");
            }

            string query = String.Format(sb.ToString(), simulationId, groupingTable.ToString(), al);

            return Utilities.ExecuteSql(instServ.ConnectionString(), String.Format(sb.ToString(), simulationId, groupingTable.ToString(), al));
        }

        public string NIIAssetLiabBlock(string isAsset, string grouping, bool te)
        {
            string field = te ? "AveTEAmt" : "IETotAmount";

            StringBuilder s = new StringBuilder();
            s.AppendLine("(");
            s.AppendLine("	SELECT ");
            s.AppendLine("	  baseSim.yearGroup as yearGroup");
            s.AppendLine("	 ,scenNames.name");
            s.AppendLine("	 ,SUM(baseSim." + field + ") as endBal");
            s.AppendLine("	 FROM");
            s.AppendLine("	(SELECT simulationid");
            s.AppendLine("			,month");
            s.AppendLine("			,scenario");
            s.AppendLine("			,code");
            s.AppendLine("			," + field);
            s.AppendLine("			,CASE");
            s.AppendLine("				  WHEN month <= DATEADD(m, 12, '{0}') THEN");
            s.AppendLine("						 'Year1'  ");
            s.AppendLine("				  WHEN month <= DATEADD(m, 24, '{0}') THEN");
            s.AppendLine("						 'Year2'");
            s.AppendLine("			END as yearGroup              ");
            s.AppendLine("	 FROM Basis_ALP13 WHERE simulationId = {1} ");
            s.AppendLine("		AND month <= DATEADD(m, 24, '{0}')");
            s.AppendLine("	) as baseSim");
            s.AppendLine("	INNER JOIN Basis_ALB as alb ON");
            s.AppendLine("	alb.simulationId = baseSim.simulationId and alb.code = baseSim.code");

            if (grouping.IndexOf("dg_") > -1)
            {
                s.AppendLine("	INNER JOIN");
                s.AppendLine("	(SELECT");
                s.AppendLine("		dgg.name");
                s.AppendLine("		,dgg.[Priority]");
                s.AppendLine("		,dgc.IsAsset");
                s.AppendLine("		,dgc.Acc_Type");
                s.AppendLine("		,dgc.RbcTier");
                s.AppendLine("	FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg");
                s.AppendLine("	INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg");
                s.AppendLine("	ON dg.id = dgg.DefinedGroupingId");
                s.AppendLine("	INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc");
                s.AppendLine("	ON dgg.id = dgc.DefinedGroupingGroupId");
                s.AppendLine("	WHERE dg.id = " + grouping.Replace("dg_", "") + ") as ac ON");
                s.AppendLine("	ac.acc_Type = alb.Acc_TypeOR AND ac.rbcTier = alb.ClassOR AND ac.IsAsset = alb.isAsset");
            }

            s.AppendLine("	INNER JOIN Basis_ALS as als ON");
            s.AppendLine("	als.simulationId = baseSim.simulationId and als.code = baseSim.code and als.scenario = baseSim.scenario and als.scenario = '{2}'");
            s.AppendLine("	INNER JOIN Basis_Scenario  as scen ON");
            s.AppendLine("	scen.number = als.scenario AND scen.SimulationId = als.SimulationId");
            s.AppendLine("	INNER JOIN ATLAS_ScenarioType as scenNames ON");
            s.AppendLine("	scennames.Id = scen.ScenarioTypeId");
            s.AppendLine("	WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5) and alb.IsAsset = " + isAsset);
            s.AppendLine("	GROUP BY baseSim.yearGroup,  baseSim.yearGroup, scenNames.name");
            s.AppendLine(") as ");

            if (isAsset.Equals("1"))
            {
                s.Append("assets");
            }
            else
            {
                s.Append("liabs");
            }

            return s.ToString();

        }

        public DataTable NiiTable(InstitutionService instServ, string simulationId, string scenario, string aod, string assetGrouping, string liabGrouping, bool taxEquiv)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select isnull(assets.yearGroup, liabs.yearGroup) as yearGroup,");
            sb.AppendLine("	isnull(assets.name, liabs.name) as name,");
            sb.AppendLine("	isnull(assets.endBal, 0) - isnull(liabs.endBal,0) as endBal");
            sb.AppendLine("	from");
            sb.AppendLine(NIIAssetLiabBlock("1", assetGrouping, taxEquiv));
            sb.AppendLine("FULL OUTER JOIN");
            sb.AppendLine(NIIAssetLiabBlock("0", liabGrouping, taxEquiv));
            sb.AppendLine("ON assets.yearGroup = liabs.yearGroup");
            //sb.AppendLine("SELECT ");
            //sb.AppendLine("  baseSim.yearGroup as yearGroup  ");
            //sb.AppendLine(" ,scenNames.name  ");
            //sb.AppendLine(" ,SUM(CASE WHEN alb.isAsset = 1 THEN baseSim.IeTotAmount ELSE -baseSim.IETotAmount END) as endBal  ");
            //sb.AppendLine(" FROM  ");
            //sb.AppendLine("  ");
            //sb.AppendLine("(SELECT simulationid  ");
            //sb.AppendLine("        ,month  ");
            //sb.AppendLine("        ,scenario  ");
            //sb.AppendLine("        ,code  ");
            //sb.AppendLine("        ,IETotAmount ");
            //sb.AppendLine("        ,CASE  ");
            //sb.AppendLine("              WHEN month <= DATEADD(m, 12, '{0}') THEN  ");
            //sb.AppendLine("                     'Year1'  ");
            //sb.AppendLine("              WHEN month <= DATEADD(m, 24, '{0}') THEN  ");
            //sb.AppendLine("                     'Year2'  ");
            //sb.AppendLine("        END as yearGroup              ");
            //sb.AppendLine(" FROM Basis_ALP13 WHERE simulationId = {1}");
            //sb.AppendLine("    AND month <= DATEADD(m, 24, '{0}')");
            //sb.AppendLine(") as baseSim");
            //sb.AppendLine("INNER JOIN Basis_ALB as alb ON");
            //sb.AppendLine("alb.simulationId = baseSim.simulationId and alb.code = baseSim.code");
            //sb.AppendLine("INNER JOIN Basis_ALS as als ON");
            //sb.AppendLine("als.simulationId = baseSim.simulationId and als.code = baseSim.code and als.scenario = baseSim.scenario and als.scenario = '{2}'");
            //sb.AppendLine("INNER JOIN Basis_Scenario  as scen ON");
            //sb.AppendLine("scen.number = als.scenario AND scen.SimulationId = als.SimulationId");
            //sb.AppendLine("INNER JOIN ATLAS_ScenarioType as scenNames ON");
            //sb.AppendLine("scennames.Id = scen.ScenarioTypeId");
            //sb.AppendLine("WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5)");
            //sb.AppendLine("GROUP BY baseSim.yearGroup,  baseSim.yearGroup, scenNames.name");

            string query = String.Format(sb.ToString(), aod, simulationId, scenario);

            return Utilities.ExecuteSql(instServ.ConnectionString(), String.Format(sb.ToString(), aod, simulationId, scenario));
        }

        public DataTable ChangeAttrTable(InstitutionService instServ, string simulationId, string scenario, string aod, string changeAttr, bool taxEquiv, string assetGrouping, string liabGrouping)
        {
            string fieldStr = taxEquiv ? "AveTEAmt" : "IETotAmount";
            StringBuilder assetGroupingTable = new StringBuilder("ATLAS_AccountType as aa on alb.Acc_TypeOR = aa.id");
            StringBuilder liabGroupingTable = new StringBuilder("ATLAS_AccountType as aa on alb.Acc_TypeOR = aa.id");

            if (assetGrouping.IndexOf("dg_") > -1)
            {
                assetGroupingTable.Clear();
                assetGroupingTable.AppendLine("(SELECT");
                assetGroupingTable.AppendLine("	dgg.name ");
                assetGroupingTable.AppendLine("	,dgg.[Priority] ");
                assetGroupingTable.AppendLine("	,dgc.IsAsset  ");
                assetGroupingTable.AppendLine("	,dgc.Acc_Type  ");
                assetGroupingTable.AppendLine("	,dgc.RbcTier  ");
                assetGroupingTable.AppendLine("FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                assetGroupingTable.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                assetGroupingTable.AppendLine("ON dg.id = dgg.DefinedGroupingId  ");
                assetGroupingTable.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                assetGroupingTable.AppendLine("ON dgg.id = dgc.DefinedGroupingGroupId  ");
                assetGroupingTable.AppendLine("WHERE dg.id = " + assetGrouping.Replace("dg_", "") + ") as aa ON ");
                assetGroupingTable.AppendLine("aa.acc_Type = alb.Acc_TypeOR AND aa.rbcTier = alb.ClassOR AND aa.IsAsset = alb.isAsset");
            }
            if (liabGrouping.IndexOf("dg_") > -1)
            {
                liabGroupingTable.Clear();
                liabGroupingTable.AppendLine("(SELECT");
                liabGroupingTable.AppendLine("	dgg.name ");
                liabGroupingTable.AppendLine("	,dgg.[Priority] ");
                liabGroupingTable.AppendLine("	,dgc.IsAsset  ");
                liabGroupingTable.AppendLine("	,dgc.Acc_Type  ");
                liabGroupingTable.AppendLine("	,dgc.RbcTier  ");
                liabGroupingTable.AppendLine("FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                liabGroupingTable.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                liabGroupingTable.AppendLine("ON dg.id = dgg.DefinedGroupingId  ");
                liabGroupingTable.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                liabGroupingTable.AppendLine("ON dgg.id = dgc.DefinedGroupingGroupId  ");
                liabGroupingTable.AppendLine("WHERE dg.id = " + liabGrouping.Replace("dg_", "") + ") as aa ON ");
                liabGroupingTable.AppendLine("aa.acc_Type = alb.Acc_TypeOR AND aa.rbcTier = alb.ClassOR AND aa.IsAsset = alb.isAsset");
            }

            StringBuilder sb = new StringBuilder();

            if (true)
            {
                sb.AppendLine("select * from (");
                sb.AppendLine("SELECT ");
                sb.AppendLine("	aa.Name as accountName");
                sb.AppendLine("	, sum(" + fieldStr + ") as endBal");
                sb.AppendLine("	,alb.IsAsset");

                if (assetGrouping.IndexOf("dg_") == -1)
                {
                    sb.AppendLine(",ao.[order]");
                }
                else
                {
                    sb.AppendLine(",aa.Priority");
                }

                sb.AppendLine("	FROM Basis_ALP13 as alp");
                sb.AppendLine("	INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                sb.AppendLine("	INNER JOIN {0}");

                sb.AppendLine("	INNER JOIN ATLAS_AccountTypeOrder as ao on");
                sb.AppendLine("	ao.acc_type = alb.acc_typeOR and ao.IsAsset = alb.isAsset");
                sb.AppendLine("	WHERE alp.simulationid = " + simulationId + " AND Scenario = '" + scenario + "' and alb.isAsset = 1 ");
                sb.AppendLine("	AND month <= DATEADD(m, 12, '" + aod + "')");
                sb.AppendLine("	GROUP BY ");
                sb.AppendLine("	aa.Name, alb.isAsset");
                if (assetGrouping.IndexOf("dg_") == -1)
                {
                    sb.AppendLine(",ao.[order]");
                }
                else
                {
                    sb.AppendLine(",aa.Priority");
                }

                sb.AppendLine("UNION ALL");

                sb.AppendLine("SELECT ");
                sb.AppendLine("	aa.Name as accountName");
                sb.AppendLine("	, sum(" + fieldStr + ") as endBal");
                sb.AppendLine("	,alb.IsAsset");

                if (assetGrouping.IndexOf("dg_") == -1)
                {
                    sb.AppendLine(",ao.[order]");
                }
                else
                {
                    sb.AppendLine(",aa.Priority");
                }

                sb.AppendLine("	FROM Basis_ALP13 as alp");
                sb.AppendLine("	INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                sb.AppendLine("	INNER JOIN {1}");

                sb.AppendLine("	INNER JOIN ATLAS_AccountTypeOrder as ao on");
                sb.AppendLine("	ao.acc_type = alb.acc_typeOR and ao.IsAsset = alb.isAsset");
                sb.AppendLine("	WHERE alp.simulationid = " + simulationId + " AND Scenario = '" + scenario + "' and alb.isAsset = 0 ");
                sb.AppendLine("	AND month <= DATEADD(m, 12, '" + aod + "')");
                sb.AppendLine("	GROUP BY ");
                sb.AppendLine("	aa.Name, alb.isAsset");
                if (assetGrouping.IndexOf("dg_") == -1)
                {
                    sb.AppendLine(",ao.[order]");
                }
                else
                {
                    sb.AppendLine(",aa.Priority");
                }

                sb.AppendLine(") as t");
                sb.AppendLine("order by t.isAsset desc");

                if (assetGrouping.IndexOf("dg_") == -1)
                {
                    sb.AppendLine(",t.[order]");
                }
                else
                {
                    sb.AppendLine(",t.Priority");
                }

            }
            else if (changeAttr.Equals("2"))
            {
                sb.AppendLine("SELECT ");
                sb.AppendLine("CASE WHEN alb.IsAsset = 1 THEN 'Assets' ELSE 'Liabilities' END as name");
                sb.AppendLine(",sum(" + fieldStr + ") as endBal");
                sb.AppendLine(",alb.IsAsset");
                sb.AppendLine("FROM Basis_ALP13 as alp");
                sb.AppendLine("INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                sb.AppendLine("INNER JOIN {0}");
                //sb.AppendLine("INNER JOIN ATLAS_AccountType as aa on");
                //sb.AppendLine("aa.Id = alb.acc_typeOR");
                sb.AppendLine("INNER JOIN ATLAS_AccountTypeOrder as ao on");
                sb.AppendLine("ao.acc_type = alb.acc_typeOR and ao.IsAsset = alb.isAsset");
                sb.AppendLine("WHERE alp.simulationid = " + simulationId + " AND Scenario = '" + scenario + "' ");
                sb.AppendLine("AND month <= DATEADD(m, 12, '" + aod + "')");
                sb.AppendLine("GROUP BY ");
                sb.AppendLine("alb.isAsset");
                sb.AppendLine("order by isasset desc");

            }

            string query = String.Format(sb.ToString(), assetGroupingTable.ToString(), liabGroupingTable.ToString());

            return Utilities.ExecuteSql(instServ.ConnectionString(), query);
        }

    }
}