﻿#undef RENDER_SOURCE_PLACEMENT_GUIDE
#undef RENDER_TARGET_PLACEMENT_GUIDE

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

using Atlas.Institution.Model;
using Atlas.Institution.Model.Atlas;
using Atlas.Web.Models;
using Newtonsoft.Json;
using Spire.Pdf;
using Spire.Pdf.Actions;
using Spire.Pdf.Bookmarks;
using Spire.Pdf.General;
using Spire.Pdf.Graphics;

namespace Atlas.Web.Services
{
    public class PdfService
    {
        const string PUPPET_PATH = @"C:\DCG Source\AtlasOne\node_modules\puppeteer\";

        private GlobalContextProvider _contextProvider = new GlobalContextProvider("");

        static PdfService()
        {
            SpireLicenseLoader.LoadLicense();
        }

        private PDFExportStatus buildScreenshotPDFs(ICollection<Report> reports, bool singleReportMode, string folderPath, string dbName)
        {
            string puppetFile = HttpContext.Current.Server.MapPath("../../puppetExporter.js");

            // If cannot load script file kick out
            if (!System.IO.File.Exists(puppetFile))
            {
                return new PDFExportStatus(false, null, "Could not locate puppetExporter.js file") { IsExportFatal = true };
            }

            //Read Text from file now that we know it exists
            string puppetCode = System.IO.File.ReadAllText(puppetFile);

            //set first report url
            string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

            string url = singleReportMode
                ? baseUrl + "#/reportview/" + reports.First().Id
                : baseUrl + "#/reportview/" + reports.OrderBy(r => r.Priority).First().Id;

            // set puppeteer parameters
            puppetCode = puppetCode.Replace("{{firstURL}}", url);
            puppetCode = puppetCode.Replace("{{onlyOne}}", singleReportMode.ToString().ToLowerInvariant());
            puppetCode = puppetCode.Replace("{{totalReportCount}}", reports.Count.ToString());
            puppetCode = puppetCode.Replace("{{folderPath}}", folderPath.Replace("\\", "\\\\")); // JS needs an extra escape char

            string jsFileName = dbName + "_" + reports.First().PackageId + "_exporter.js";

            //Save new exporter js code with updated variables
            using (StreamWriter file = new StreamWriter(PUPPET_PATH + jsFileName))
            {
                file.Write(puppetCode);
            }

            List<HttpCookie> actualCookies = new List<HttpCookie>();
            foreach (string key in HttpContext.Current.Request.Cookies.AllKeys)
            {
                //ASPNETFORMSAUTH cookie has null domain. not sure what other ones do, but we want that to come through, at least for development.
                if (HttpContext.Current.Request.Cookies[key].Domain == null)
                {
                    HttpContext.Current.Request.Cookies[key].Domain = HttpContext.Current.Request.Url.Host;
                }

                //phantom will only use the cookies that have the same domain anyway
                if (HttpContext.Current.Request.Cookies[key].Domain == HttpContext.Current.Request.Url.Host)
                {
                    actualCookies.Add(HttpContext.Current.Request.Cookies[key]);
                }
            }

            string jsonCookies = new JavaScriptSerializer().Serialize(actualCookies);
            string cookieFileName = "cookies_.txt";
            string cookieFilePath = folderPath + cookieFileName;
            StreamWriter cookFile = new StreamWriter(cookieFilePath);
            cookFile.Write(jsonCookies);
            cookFile.Close();

            //Kick off the process of exporting now
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
            {
                FileName = "cmd.exe",
                WorkingDirectory = PUPPET_PATH,
                Arguments = "/c node " + jsFileName,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal,
                CreateNoWindow = true
            };

            process.StartInfo = startInfo;
            process.EnableRaisingEvents = true;
            process.Start();
            process.WaitForExit();

            try
            {
                return JsonConvert.DeserializeObject<PDFExportStatus>(System.IO.File.ReadAllText(folderPath + "status.json"));
            }
            catch
            {
                return new PDFExportStatus(false, null, "status.json not found, could not determine status of package export") { IsExportFatal = true };
            }
        }

        public PDFExportStatus GetPDFStatus(int packageID)
        {
            string dbName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string packageId = packageID.ToString();
            string folderPath = FileProcessingService.GetBasePath() + dbName + "\\\\" + packageId + "\\\\";

            try
            {
                if (System.IO.File.Exists(folderPath + "status.json"))
                {
                    return JsonConvert.DeserializeObject<PDFExportStatus>(System.IO.File.ReadAllText(folderPath + "status.json"));
                }
            }
            catch(FileNotFoundException)
            {
            }
            catch(InvalidOperationException)
            {
            }
            catch(IOException)
            {
                // don't update the status in this case
                return null;
            }
            catch(Exception ex)
            {
                return new PDFExportStatus(false, null, ex.Message);
            }

            return new PDFExportStatus(true, null, "Waiting for status");
        }

        public PDFExportStatus ExportPackage(int packageID)
        {
            Report[] reports = _contextProvider.Context.Reports.Include("Package").Where(r => r.PackageId == packageID).OrderBy(r => r.Priority).ToArray();

            if (!reports.Any())
                return null;

            return exportPackageReports(reports, false);
        }

        public PDFExportStatus ExportReport(int reportID)
        {
            Report report = _contextProvider.Context.Reports.Include("Package").FirstOrDefault(r => r.Id == reportID);

            if (report == null)
                return null;

            return exportPackageReports(new[] { report }, true);
        }

        // all reports should be in the same package, and there should be at least one
        private PDFExportStatus exportPackageReports(ICollection<Report> reports, bool singleReportMode)
        {
            const string TEMPLATE_DB_NAME = "DDW_Atlas_Templates";

            string currentDatabaseName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();

            // -------- folder setup
            bool justCoverPage = false;
            string dbName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string folderPath = FileProcessingService.GetBasePath() + dbName + "\\" + reports.First().PackageId + "\\";

            //This will create all sub directories
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            // Now empty directory so that we can create stuff from scratch
            DirectoryInfo di = new DirectoryInfo(folderPath);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            bool screenshotsAreRequired = reports.Any(r => !r.ContentsFromFile);

            PDFExportStatus lastStatus;

            if (screenshotsAreRequired)
            {
                lastStatus = buildScreenshotPDFs(reports, singleReportMode, folderPath, dbName);
                
                // todo jdk
                if(! lastStatus.IsSuccess || lastStatus.IsExportFatal)
                {
                    return lastStatus;
                }
            }

            //Spire.License.LicenseProvider.SetLicenseFileName(HttpContext.Current.Server.MapPath("../../license.elic.xml"));
            //PdfUnitConvertor unitCvtr = new PdfUnitConvertor();
            //PdfMargins margin = new PdfMargins();
            //margin.Top = unitCvtr.ConvertUnits(2.54f, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point);
            //margin.Bottom = margin.Top;
            //margin.Left = unitCvtr.ConvertUnits(4.17f, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point);
            //margin.Right = margin.Left;

            // -------- create an empty document which will become the final document after merge

            PdfDocument packagePDF = new PdfDocument();
            PdfDocument reportsPDF = new PdfDocument();
            Dictionary<string, string> tblContents = new Dictionary<string, string>();
            List<TableOfContentItem> tableOfContents = new List<TableOfContentItem>();

            //hack to hold onito old package for adding header to table of contents 
            Package pckg = new Package();

            int startingPageNumber = 1;
            var sectionCounter = 1000;
            int appendixCounter = 1;
            int appendixAscii = 64;
            Boolean appendix = false;

            string instName = Utilities.RegulatoryName(dbName);
            string footerText = instName + " - Page";

            PdfService pdfService = new PdfService();

            try
            {
                System.IO.File.WriteAllText(
                    folderPath + "status.json",
                    JsonConvert.SerializeObject(
                        new PDFExportStatus(true, null, "Adding Headers and Footers", 0) { TotalReportCount = reports.Count }
                    )
                );
            }
            catch (Exception)
            {
                // ignore
            }

            int counter = 0;

            // If in package mode add one to it because of table of contents that will get inserted
            if (!singleReportMode)
            {
                startingPageNumber += 1;
            }

            foreach (Report report in reports)
            {
                //  if (report.ShortName != "NIIRecon")
                // {
                // report.FirstPageNumber = startingPageNumber + 1;
                pckg = report.Package;
                PdfDocument reportPDF;

                bool isOverridden = !string.Equals(TEMPLATE_DB_NAME, currentDatabaseName, StringComparison.CurrentCultureIgnoreCase) && report.Defaults;

                Report localReport;
                
                if(isOverridden)
                {
                    int? templateReportID = PackageService.GetTemplateReportId(currentDatabaseName, report.Id, report.PackageId, report.SourceReportId ?? report.Id);

                    GlobalContextProvider templateGTX = new GlobalContextProvider(TEMPLATE_DB_NAME);
                    localReport = templateGTX.Context.Reports.Include("Package").FirstOrDefault(r => r.Id == templateReportID);
                }
                else
                {
                    localReport = report;
                }

                if (localReport.ContentsFromFile)
                {
                    FileProcessingService fps = new FileProcessingService();

                    IReadOnlyCollection<FileSummary> reportFiles = fps.GetByOwner(FileOwnerType.Report, localReport.Id, databaseName: isOverridden ? TEMPLATE_DB_NAME : null);
                    
                    FileSummary reportFile = reportFiles.FirstOrDefault(fsi => fsi.Tag == (localReport.Orientation.HasValue ? localReport.Orientation.Value.ToString() : null));

                    if(ReferenceEquals(null, reportFile))
                    {
                        if(! reportFiles.Any())
                        {
                            return new PDFExportStatus(false, localReport.Name, "Backing file for Report " + localReport.Id + " is not found") { IsExportFatal = true };
                        }

                        reportFile = reportFiles.First();
                    }

                    byte[] fileBlob;
                    fps.GetWithData(reportFile.FileID, out fileBlob);

                    reportPDF = new PdfDocument(fileBlob);
                }
                else
                {
                    if (!System.IO.File.Exists(folderPath + counter.ToString() + ".pdf"))
                    {
                        return new PDFExportStatus(false, localReport.Name, "Could not locate generated file \"" + folderPath + counter.ToString() + ".pdf\"") { IsExportFatal = true };
                    }

                    reportPDF = new PdfDocument(folderPath + counter.ToString() + ".pdf");
                }

                 if (localReport.ShortName != "CoverPage" && localReport.ShortName != "SectionPage")
                 {
                    //reportPDF = pdfService.FormatPDF(localReport, reportPDF, startingPageNumber, localReport.Name, instName, localReport.Package.AsOfDate.ToShortDateString());
                    //In the case of an Atlas Templates linked report, we do  not want to show the Atlas Templates package asofdate. Take the actual report asofdate
                    reportPDF = pdfService.FormatPDF(localReport, reportPDF, startingPageNumber, localReport.Name, instName, report.Package.AsOfDate.ToShortDateString(), false);
                }

                //hack here becuase spire sucks if its the title page put it ont he packagepdf not the collection of reports pdf
                //this is because the insert page method does not work thereofre I need to only append in the right order

                if (localReport.ShortName == "CoverPage")
                {
                    packagePDF.AppendPage(reportPDF);
                    if (singleReportMode)
                    {
                        justCoverPage = true;
                    }
                }
                else
                {
                    reportsPDF.AppendPage(reportPDF);
                }

                // }
                counter += 1;

                if (!singleReportMode)
                {
                    //Now that we know page numbes we can build table of contents
                    //Only Increment counter if an "actual" report
                    if (localReport.ShortName != "SectionPage" && localReport.ShortName != "CoverPage")
                    {
                        if (appendix)
                        {
                            tblContents.Add((char)appendixAscii + appendixCounter.ToString(), localReport.Name);
                            appendixCounter += reportPDF.Pages.Count;
                        }
                        else
                        {
                            tblContents.Add(startingPageNumber.ToString(), localReport.Name);
                        }

                        tableOfContents.Add(new TableOfContentItem(startingPageNumber, localReport.Name, "REPORT"));
                    }
                    else if (localReport.ShortName == "SectionPage")
                    {
                        var section = _contextProvider.Context.SectionPages.Where(s => s.Id == localReport.Id).FirstOrDefault();
                        if (section != null)
                        {
                            if (section.Appendix)
                            {
                                appendixCounter = 1;
                                appendixAscii += 1;
                                appendix = true;
                            }
                            else
                            {
                                appendix = false;
                                appendixCounter = 1;
                            }
                        }
                        else
                        {
                            appendix = false;
                            appendixCounter = 1;
                        }

                        tblContents.Add("SECTION" + sectionCounter.ToString(), localReport.Name);

                        tableOfContents.Add(new TableOfContentItem(startingPageNumber, localReport.Name, "SECTION"));
                        sectionCounter += 1;
                    }
                    else
                    {
                        tableOfContents.Add(new TableOfContentItem(startingPageNumber, localReport.Name, "COVER"));
                    }
                }

                startingPageNumber += reportPDF.Pages.Count;
            }

            //Now that I haave table of contents build I can build it
            if (!singleReportMode)
            {
                CreateTableOfContents(tblContents, folderPath);
                PdfDocument tocPDF = new PdfDocument(folderPath + "tableOfContents.pdf");
                tocPDF = pdfService.FormatPDF(null, tocPDF, startingPageNumber, "", instName, pckg.AsOfDate.ToShortDateString(), true);
                //I am appending here becuase the page.inser method does not work as they claim
                packagePDF.AppendPage(tocPDF);

                //now append the rest of the reports
                packagePDF.AppendPage(reportsPDF);

                //SPires fucked up way to insert pages
                //  for (int i = 0; i < tocPDF.Pages.Count; i++)
                //  {
                //  PdfPageBase page = tocPDF.Pages[i];
                //  SizeF size = page.Size;
                //  PdfPageBase newPage = packagePdf.Pages.Insert(1 + i, size, margin, );
                ///newPage.Canvas.DrawTemplate(page.CreateTemplate(), new PointF(0, 0));
                //

                //Remove the pages at the end for some reason it likes to double insert them
                //packagePdf.Pages.RemoveAt(packagePdf.Pages.Count - 1 - i);
                //  }

                //now that i appended pdf

            }
            else
            {
                if(!justCoverPage)
                {
                    packagePDF.AppendPage(reportsPDF);
                }
            }

            // Lets create some bookmarks

            PdfBookmark currentSectionBookmark = null;
            foreach (TableOfContentItem tbc in tableOfContents)
            {
                if (tbc.type == "COVER")
                {
                    //add vendor bookmark
                    PdfDestination coverDest = new PdfDestination(packagePDF.Pages[0]);
                    PdfBookmark coverMark = packagePDF.Bookmarks.Add(tbc.title);
                    coverMark.Color = Color.SeaGreen;
                    coverMark.DisplayStyle = PdfTextStyle.Bold;
                    coverMark.Action = new PdfGoToAction(coverDest);

                    //While here add table of contents book mark
                    PdfDestination tbcDest = new PdfDestination(packagePDF.Pages[1]);
                    PdfBookmark tbcMark = packagePDF.Bookmarks.Add("Table of Contents");
                    tbcMark.Color = Color.SeaGreen;
                    tbcMark.DisplayStyle = PdfTextStyle.Bold;
                    tbcMark.Action = new PdfGoToAction(tbcDest);
                }
                else if (tbc.type == "SECTION")
                {
                    PdfDestination sectionDest = new PdfDestination(packagePDF.Pages[tbc.pageNum - 1]);
                    currentSectionBookmark = packagePDF.Bookmarks.Add(tbc.title);
                    currentSectionBookmark.Color = Color.SeaGreen;
                    currentSectionBookmark.DisplayStyle = PdfTextStyle.Bold;
                    currentSectionBookmark.Action = new PdfGoToAction(sectionDest);
                }
                else
                {
                    if (currentSectionBookmark != null)
                    {
                        PdfDestination partBookmarkDest = new PdfDestination(packagePDF.Pages[tbc.pageNum - 1]);
                        PdfBookmark partBookmark = currentSectionBookmark.Add(tbc.title);

                        partBookmark.Color = Color.Firebrick;

                        partBookmark.DisplayStyle = PdfTextStyle.Regular;

                        partBookmark.Action = new PdfGoToAction(partBookmarkDest);
                    }
                    else
                    {
                        PdfDestination sectionDest = new PdfDestination(packagePDF.Pages[tbc.pageNum - 1]);
                        PdfBookmark soloMark = packagePDF.Bookmarks.Add(tbc.title);
                        soloMark.Color = Color.SeaGreen;
                        soloMark.DisplayStyle = PdfTextStyle.Bold;
                        soloMark.Action = new PdfGoToAction(sectionDest);
                    }
                }
            }

            //Response.AppendHeader("Content-Disposition", "inline; filename=" + package.Name + ".pdf");
            //Byte[] btyes = null;
            //MemoryStream ms = new MemoryStream();

            if (!singleReportMode)
            {
                packagePDF.SaveToFile(folderPath + "packageExport.pdf");
            }
            else
            {
                packagePDF.SaveToFile(folderPath + "singleExport.pdf");
            }

            return new PDFExportStatus(true, null, "Export complete") { IsExportComplete = true };
        }

        public void CreateTableOfContents(Dictionary<string, string> contents, string folderPath)
        {
            StringBuilder htmlBldr = new StringBuilder();
            StringBuilder leftTable = new StringBuilder();
            StringBuilder rightTable = new StringBuilder();
            htmlBldr.AppendLine("<html><body style='text-align: center; font-family: sans-serif; font-size: 10px; min-height: 9.1in;'>");
            htmlBldr.AppendLine("");

            int sectionCounter = 0;

            int repCounter = 0;
            bool appendLeft = true;
            string lastSectionName = "";
            string curTables = TableOfContentsTable(true);
            int lineItemsPerSide = 33;

            foreach (string key in contents.Keys)
            {
                //If new sectoin and close to end of page go to new page
                if (key.Contains("SECTION") && repCounter >= lineItemsPerSide - 4)
                {
                    //If on  Right side and about to switch start a new page
                    if (!appendLeft)
                    {
                        htmlBldr.AppendLine(string.Format(curTables, leftTable.ToString(), rightTable.ToString(), (appendLeft == true ? "none" : "inherit")));
                        leftTable.Clear();
                        rightTable.Clear();
                        curTables = TableOfContentsTable(false);
                    }

                    appendLeft = !appendLeft;
                    repCounter = 0;
                }

                if (appendLeft)
                {
                    if (key.Contains("SECTION"))
                    {
                        leftTable.AppendLine("<tr><td>&nbsp;</td><td style='width: 10%; text-align: right'></td>");
                        leftTable.AppendLine("<tr style=' font-weight: bold;'><td style='width: 90%; text-align: left;'>" + contents[key] + "</td><td style='width: 10%; text-align: right'></td>");
                        sectionCounter += 1;
                        repCounter += 2;
                        lastSectionName = contents[key];
                    }
                    else
                    {
                        leftTable.AppendLine("<tr><td style='width: 90%; text-align: left; padding-left: 40px;'>" + contents[key] + "</td><td style='width: 10%; text-align: right'>" + key + "</td>");
                        repCounter += 1;
                    }
                }
                else
                {
                    if (key.Contains("SECTION"))
                    {
                        rightTable.AppendLine("<tr><td>&nbsp;</td><td style='width: 10%; text-align: right'></td>");
                        rightTable.AppendLine("<tr style=' font-weight: bold;'><td style='width: 90%; text-align: left;'>" + contents[key] + "</td><td style='width: 10%; text-align: right'></td>");
                        sectionCounter += 1;
                        repCounter += 2;
                        lastSectionName = contents[key];
                    }
                    else
                    {
                        rightTable.AppendLine("<tr><td style='width: 90%; text-align: left; padding-left: 40px;'>" + contents[key] + "</td><td style='width: 10%; text-align: right'>" + key + "</td>");
                        repCounter += 1;
                    }
                }

                bool justSwitched = false;
                if (repCounter == lineItemsPerSide + 1)
                {
                    //If on Right side and about to switch start a new page
                    if (!appendLeft)
                    {
                        htmlBldr.AppendLine(string.Format(curTables, leftTable.ToString(), rightTable.ToString(), (appendLeft == true ? "none" : "inherit")));
                        leftTable.Clear();
                        rightTable.Clear();
                        curTables = TableOfContentsTable(false);
                    }

                    appendLeft = !appendLeft;
                    repCounter = 0;
                    justSwitched = true;
                }

                if (justSwitched)
                {
                    if (appendLeft)
                    {
                        leftTable.AppendLine("<tr><td>&nbsp;</td><td style='width: 10%; text-align: right'></td>");
                        leftTable.AppendLine("<tr style=' font-weight: bold;'><td style='width: 90%; text-align: left;'>" + lastSectionName + "(cont)</td><td style='width: 10%; text-align: right'></td>");
                        sectionCounter += 1;
                        repCounter += 2;
                        lastSectionName = contents[key];
                    }
                    else
                    {
                        rightTable.AppendLine("<tr><td>&nbsp;</td><td style='width: 10%; text-align: right'></td>");
                        rightTable.AppendLine("<tr style=' font-weight: bold;'><td style='width: 90%; text-align: left;'>" + lastSectionName + "(cont)</td><td style='width: 10%; text-align: right'></td>");
                        sectionCounter += 1;
                        repCounter += 2;
                        lastSectionName = contents[key];
                    }
                }
            }

            //if append left is fase

            //Append Table
            htmlBldr.AppendLine(string.Format(curTables, leftTable.ToString(), rightTable.ToString(), (appendLeft == true ? "none" : "inherit")));

            htmlBldr.AppendLine("<div style='border: 1px solid #000000; position: absolute; right: 40px; bottom: 5px; margin-right: 30px; width: 45%;padding: 5px'>");
            htmlBldr.AppendLine("<div style='font-weight: bold; clear: both;'>Disclaimer</div>");
            htmlBldr.AppendLine("The information contained herein consists of data supplied by various third parties, and");
            htmlBldr.AppendLine("by the institution itself. While Darling Consulting Group (DCG) obtains the data from");
            htmlBldr.AppendLine("sources we believe to be reliable, we do not guarantee its accuracy. It is the");
            htmlBldr.AppendLine("responsibility of the user to independently verify all information contained in the report.");
            htmlBldr.AppendLine("</div>");
            htmlBldr.AppendLine("</body></html>");

            var htmlToPdfConverter = new HiQPdf.HtmlToPdf { SerialNumber = "6KCBubiM-jqSBipqJ-mpHextjI-2cjbyNHR-0cjb2cbZ-2sbR0dHR" };
            // set PDF page size and orientation
            htmlToPdfConverter.Document.PageSize = HiQPdf.PdfPageSize.Letter;
            htmlToPdfConverter.Document.PageOrientation = HiQPdf.PdfPageOrientation.Landscape;
            // set PDF page margins
            htmlToPdfConverter.Document.Margins = new HiQPdf.PdfMargins(30, 30, 30, 0);


            HiQPdf.PdfDocument doc = htmlToPdfConverter.ConvertHtmlToPdfDocument(string.Format(htmlBldr.ToString(), leftTable.ToString(), rightTable.ToString()), null);
            //write it out and read it back in beucase spire is weird
            doc.WriteToFile(folderPath + "tableOfContents.pdf");
        }

        private string TableOfContentsTable(bool firstOne)
        {
            StringBuilder htmlBldr = new StringBuilder();
            if (firstOne)
            {
                htmlBldr.AppendLine("<div style=''>");
            }
            else
            {
                htmlBldr.AppendLine("<div style='page-break-before: always; clear: both;'>");
            }

            htmlBldr.AppendLine("<table style='width: 45%; float: left'>");
            htmlBldr.AppendLine("<thead><tr><th></th><th style='text-decoration: underline;'>Page</th>");
            htmlBldr.AppendLine("   <tbody>");
            htmlBldr.AppendLine(" {0} ");
            htmlBldr.AppendLine("   </tbody>");
            htmlBldr.AppendLine("<tbody>");
            htmlBldr.AppendLine("</table>");
            htmlBldr.AppendLine("</body>");
            htmlBldr.AppendLine("<table style='width: 45%; float: right;display: {2};'>");
            htmlBldr.AppendLine("<thead><tr><th></th><th style='text-decoration: underline;'>Page</th>");
            htmlBldr.AppendLine("   <tbody>");
            htmlBldr.AppendLine(" {1} ");
            htmlBldr.AppendLine("   </tbody>");
            htmlBldr.AppendLine("<tbody>");
            htmlBldr.AppendLine("</table>");
            htmlBldr.AppendLine("</div>");
            return htmlBldr.ToString();
        }

        public PdfDocument FormatPDF(Report report, PdfDocument sourceDocument, int startingPageNumber, string reportName, string bankName, string asOfDate, bool onlyHeader)
        {
            const int X_MARGIN = 10;
            const int Y_HEADER_TEXT_MARGIN = 15;
            const int Y_HEADER_LINE_MARGIN = 26;
            const int Y_FOOTER_TEXT_MARGIN = 12;
            const int Y_FOOTER_LINE_MARGIN = 22;

            // nothing to do
            if (report != null)
            {
                if (report.HideHeader && report.HideFooter) return sourceDocument;
            }
         

            PdfDocument targetDocument = new PdfDocument();

            float canvasWidth, canvasHeight;

            // black font pen and right alignemnt for drawing text
            PdfPen borderPen = new PdfPen(Color.Black, 1f);
            PdfSolidBrush fontPen = new PdfSolidBrush(Color.Black);
            PdfStringFormat rightAlignment = new PdfStringFormat(PdfTextAlignment.Right, PdfVerticalAlignment.Middle);
            PdfFont font = new PdfFont(PdfFontFamily.Helvetica, 12f);

            int headerMargin = 0;
            int footerMargin = 0;
            if (report != null)
            {
                headerMargin = !report.ConstrainContentWithinBody || report.HideHeader ? 0 : Y_HEADER_LINE_MARGIN + 2;
                footerMargin = !report.ConstrainContentWithinBody || report.HideFooter ? 0 : Y_FOOTER_LINE_MARGIN + 2;
            }
           
            
            foreach (PdfPageBase sourcePage in sourceDocument.Pages)
            {
                PdfPageBase targetPage = addTargetPageTranslateCanvas(targetDocument, sourcePage, headerMargin, footerMargin, out canvasWidth, out canvasHeight);


                if (report == null)
                {
                    string headerText = bankName + " as of " + asOfDate;
                    targetPage.Canvas.DrawString(headerText, font, fontPen, canvasWidth - X_MARGIN, Y_HEADER_TEXT_MARGIN, rightAlignment);
                    targetPage.Canvas.DrawRectangle(borderPen, new Rectangle(new Point(X_MARGIN, Y_HEADER_LINE_MARGIN), new Size((int)(canvasWidth - X_MARGIN * 2), 1)));
                }
                else if (!report.HideHeader)
                {
                    string headerText = reportName + " - " + asOfDate;
                    targetPage.Canvas.DrawString(headerText, font, fontPen, canvasWidth - X_MARGIN, Y_HEADER_TEXT_MARGIN, rightAlignment);
                    targetPage.Canvas.DrawRectangle(borderPen, new Rectangle(new Point(X_MARGIN, Y_HEADER_LINE_MARGIN), new Size((int)(canvasWidth - X_MARGIN * 2), 1)));
                }

                if (report != null)
                {
                    if (!report.HideFooter && !onlyHeader)
                    {
                        string footerText = bankName + " - Page " + startingPageNumber;
                        targetPage.Canvas.DrawRectangle(borderPen, new Rectangle(new Point(X_MARGIN, (int)(canvasHeight - Y_FOOTER_LINE_MARGIN)), new Size((int)(canvasWidth - X_MARGIN * 2), 1)));
                        targetPage.Canvas.DrawString(footerText, font, fontPen, canvasWidth - X_MARGIN, canvasHeight - Y_FOOTER_TEXT_MARGIN, rightAlignment);
                    }
                }


                startingPageNumber++;
            }

            return targetDocument;
        }

        private void SetDocumentTemplate(PdfDocument doc, SizeF pageSize, PdfMargins margin)
        {
            PdfPageTemplateElement leftSpace = new PdfPageTemplateElement(margin.Left, pageSize.Height);
            doc.Template.Left = leftSpace;

            PdfPageTemplateElement topSpace = new PdfPageTemplateElement(pageSize.Width, margin.Top)
            {
                Foreground = true
            };
            doc.Template.Top = topSpace;

            PdfPageTemplateElement rightSpace = new PdfPageTemplateElement(margin.Right, pageSize.Height);
            doc.Template.Right = rightSpace;

            PdfPageTemplateElement bottomSpace = new PdfPageTemplateElement(pageSize.Width, margin.Bottom)
            {
                Foreground = true
            };
            doc.Template.Bottom = bottomSpace;
        }

        private static PdfPageBase addTargetPageTranslateCanvas(PdfDocument targetDocument, PdfPageBase sourcePage, int topMargin, int bottomMargin, out float canvasWidth, out float canvasHeight)
        {
            switch (sourcePage.Rotation)
            {
                case PdfPageRotateAngle.RotateAngle90:
                case PdfPageRotateAngle.RotateAngle270:
                    canvasWidth = sourcePage.ActualSize.Height;
                    canvasHeight = sourcePage.ActualSize.Width;
                    break;

                case PdfPageRotateAngle.RotateAngle0:
                case PdfPageRotateAngle.RotateAngle180:
                    canvasWidth = sourcePage.ActualSize.Width;
                    canvasHeight = sourcePage.ActualSize.Height;
                    break;

                default:
                    throw new NotSupportedException("Undefined rotation");
            }

#if RENDER_SOURCE_PLACEMENT_GUIDE
            renderSourcePlacementGuide(sourcePage);
#endif

            PdfPageBase targetPage = targetDocument.Pages.Add(new SizeF(canvasWidth, canvasHeight), new PdfMargins(0));

            // these will also both be zero if constrainWithinBody is false
            if (topMargin == 0 && bottomMargin == 0)
            {
                targetPage.Canvas.DrawTemplate(sourcePage.CreateTemplate(), new PointF(0, 0));
                return targetPage;
            }

            float scaleH = (canvasHeight - topMargin - bottomMargin) / canvasHeight;

            targetPage.Canvas.ScaleTransform(scaleH, scaleH);

            float insertAtX;
            float insertAtY;
            PointF renderAt;

            switch (sourcePage.Rotation)
            {
                case PdfPageRotateAngle.RotateAngle90:
                case PdfPageRotateAngle.RotateAngle270:
                    insertAtX = topMargin / scaleH;
                    insertAtY = (canvasWidth / canvasHeight) * (topMargin + bottomMargin) / -2 / scaleH;
                    renderAt = new PointF(insertAtX, insertAtY);
                    break;

                default:
                    insertAtX = ((1 - scaleH) * canvasWidth) / 2 / scaleH;
                    insertAtY = topMargin / scaleH;
                    renderAt = new PointF(insertAtX, insertAtY);
                    break;
            }
            
            targetPage.Canvas.DrawTemplate(sourcePage.CreateTemplate(), renderAt);

#if RENDER_TARGET_PLACEMENT_GUIDE
            insertAtX = ((1 - scaleH) * canvasWidth) / 2 / scaleH;
            insertAtY = topMargin / scaleH;
            renderTargetPlacementGuide(targetPage, canvasWidth, canvasHeight, insertAtX, insertAtY);
#endif

            targetPage.Canvas.ScaleTransform(1/scaleH, 1/scaleH);

            return targetPage;
        }

#if RENDER_SOURCE_PLACEMENT_GUIDE
        private static void renderSourcePlacementGuide(PdfPageBase sourcePage)
        {
            float canvasWidth = sourcePage.Canvas.Size.Width;
            float canvasHeight = sourcePage.Canvas.Size.Height;

            if (sourcePage.Rotation == PdfPageRotateAngle.RotateAngle90)
            {
                sourcePage.Canvas.TranslateTransform(canvasWidth / 2, canvasHeight / 2);
                sourcePage.Canvas.RotateTransform(270);
                sourcePage.Canvas.TranslateTransform(-canvasHeight / 2, -canvasWidth / 2);

                canvasWidth = sourcePage.Canvas.Size.Height;
                canvasHeight = sourcePage.Canvas.Size.Width;
            }

            PdfBrush gradientBrush = new PdfLinearGradientBrush(new PointF(0, 0), new PointF(canvasWidth, canvasHeight), new PdfRGBColor(Color.Red), new PdfRGBColor(Color.Blue));
            PdfPen blackSingleLinePen = new PdfPen(new PdfRGBColor(Color.Black), 1);

            sourcePage.Canvas.SetTransparency(0.5f);
            sourcePage.Canvas.DrawRectangle(blackSingleLinePen, gradientBrush, 0, 0, canvasWidth, canvasHeight);
            sourcePage.Canvas.DrawLine(blackSingleLinePen, new PointF(0, 0), new PointF(canvasWidth, canvasHeight));
            sourcePage.Canvas.DrawLine(blackSingleLinePen, new PointF(0, canvasHeight), new PointF(canvasWidth, 0));
            sourcePage.Canvas.SetTransparency(1);
        }
#endif

#if RENDER_TARGET_PLACEMENT_GUIDE
        private static void renderTargetPlacementGuide(PdfPageBase targetPage, float canvasWidth, float canvasHeight, float insertAtX, float insertAtY)
        {
            targetPage.Canvas.SetTransparency(0.5f);
            PdfPen blackSingleLinePen = new PdfPen(new PdfRGBColor(Color.Black), 1);
            PdfBrush redBrush = new PdfSolidBrush(new PdfRGBColor(Color.Red));
            targetPage.Canvas.DrawRectangle(blackSingleLinePen, redBrush, insertAtX, insertAtY, canvasWidth, canvasHeight);

            PdfBrush greenBrush = new PdfSolidBrush(new PdfRGBColor(Color.Green));
            targetPage.Canvas.DrawRectangle(greenBrush, insertAtX, insertAtY, 50, 50);

            PdfBrush purpleBrush = new PdfSolidBrush(new PdfRGBColor(Color.Purple));
            targetPage.Canvas.DrawRectangle(purpleBrush, insertAtX + canvasWidth - 50, insertAtY + canvasHeight - 50, 50, 50);

            targetPage.Canvas.DrawLine(blackSingleLinePen, new PointF(insertAtX, insertAtY), new PointF(canvasWidth+insertAtX, canvasHeight+insertAtY));
            targetPage.Canvas.DrawLine(blackSingleLinePen, new PointF(insertAtX, canvasHeight+insertAtY), new PointF(canvasWidth+insertAtX, insertAtY));

            targetPage.Canvas.SetTransparency(1);
        }
#endif
    }
}