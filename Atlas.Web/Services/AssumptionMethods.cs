﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class AssumptionMethodsService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();

        public object AssumptionMethodsViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var c = gctx.AssumptionMethods.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (c == null)
                throw new Exception("Assumption Methods Not Found");

            var instService = new InstitutionService(c.InstitutionDatabaseName);
            var info = instService.GetInformation(rep.Package.AsOfDate, c.AsOfDateOffset);

            var pol = gctx.ModelSetups.Where(s => s.AsOfDate == info.AsOfDate);
            var pric = gctx.LiabilityPricings.Where(s => s.AsOfDate == info.AsOfDate);

            string loanType = "N/A";
            string otherLoanType = "N/A";
            string loanDate = "N/A";
            string depositDate = "N/A";
            string marketDate = "N/A";

            if (pol.Count() > 0)
            {
                loanType = pol.FirstOrDefault().PrepaymentType;
                if (loanType == "BK")
                {
                    loanType = "Black Knight Financial Services (BKFS).";
                }
                else
                {
                    loanType = "The Client.";
                }


                otherLoanType = pol.FirstOrDefault().OtherLoanPrepaymentType;
                loanDate = pol.FirstOrDefault().LoanRatesDate.ToShortDateString();
            }
            else
                warnings.Add("Model Setup not found; please run Model Setup before setting up the package");

            if (pric.Count() > 0)
            {
                depositDate = pric.FirstOrDefault().DepositRateAsOfDate.ToShortDateString();
                marketDate = pric.FirstOrDefault().WebRateAsOfDate.ToString();
            }
            else
                warnings.Add("Liability Pricing not found; please run Liability Pricing before setting up the package");

            return new
            {
                asOfDate = info.AsOfDate.ToShortDateString(),
                instName = Utilities.InstName(c.InstitutionDatabaseName),
                loanType = loanType,
                otherLoanType = otherLoanType,
                loanDate = loanDate,
                depositDate = depositDate,
                marketDate = marketDate,
                errors = errors,
                warnings = warnings
            };
        }
    }
}