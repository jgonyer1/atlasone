﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class CashflowService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object CashflowViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var cashflow = gctx.CashflowReports.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (cashflow == null) throw new Exception("Cashflow Report Not Found");

            DataTable[] cfs = new DataTable[cashflow.CashflowSimulationScenario.Count];
            string[] scens = new string[cashflow.CashflowSimulationScenario.Count];
            string[] sims = new string[cashflow.CashflowSimulationScenario.Count];
            string[] dates = new string[cashflow.CashflowSimulationScenario.Count];
            int counter = 0;






            GlobalController gc = new GlobalController();
            foreach (CashflowSimulationScenario simScen in cashflow.CashflowSimulationScenario.OrderBy(s => s.Priority))
            {
                var instService = new InstitutionService(simScen.InstitutionDatabaseName);
                var info = instService.GetInformation(rep.Package.AsOfDate, simScen.AsOfDateOffset);

                Dictionary<string, DataTable> policyOffsets = new Dictionary<string, DataTable>();
                policyOffsets = gc.PolicyOffsets(simScen.InstitutionDatabaseName);

                if (!simScen.OverrideSimulationId)
                {
                    if (policyOffsets.Keys.Contains(info.AsOfDate.ToString("MM/dd/yyyy")))
                    {
                        simScen.SimulationTypeId = Convert.ToInt32(policyOffsets[info.AsOfDate.ToString("MM/dd/yyyy")].Rows[0]["niiId"].ToString());
                    }

                }


                var obj = instService.GetSimulationScenario(rep.Package.AsOfDate, simScen.AsOfDateOffset, simScen.SimulationTypeId, simScen.ScenarioTypeId);
                string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
                string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
                string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();



                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine("	SELECT ");
                sqlQuery.AppendLine("		CASE ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 3, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q1Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 6, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q2Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 9, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q3Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 12, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q4Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 15, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q1Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 18, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q2Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 21, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q3Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 24, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q4Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 36, '{2}') THEN ");
                sqlQuery.AppendLine("				'Y3' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 48, '{2}') THEN ");
                sqlQuery.AppendLine("				'Y4' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 60, '{2}') THEN ");
                sqlQuery.AppendLine("				'Y5' ");
                sqlQuery.AppendLine("			ELSE ");
                sqlQuery.AppendLine("				'>Y5' ");
                sqlQuery.AppendLine("			END as qtrs ");
                sqlQuery.AppendLine("		,dgs.name ");
                
                if (!cashflow.ExcludePrepayments)
                {
                    sqlQuery.AppendLine("		,SUM(alpdata.principalPayDown + PayEBal ) ");
                    //sqlQuery.AppendLine(" + ISNULL(SUM( ");
                    //sqlQuery.AppendLine("			CASE ");
                    //sqlQuery.AppendLine("				WHEN endEBal >= 0 AND ABS(negNew) > endEBal  THEN ");
                    //sqlQuery.AppendLine("					PayEBal + ABS(EndEBal) ");
                    //sqlQuery.AppendLine("				WHEN endEBal <= 0 AND ABS(negNew) < endEBal THEN ");
                    //sqlQuery.AppendLine("					PayEBal + ABS(EndEBal) ");
                    //sqlQuery.AppendLine("				ELSE ");
                    //sqlQuery.AppendLine("					PayEBal + ABS(negNew) ");
                    //sqlQuery.AppendLine("				END ");
                    //sqlQuery.AppendLine("		), 0) ");
                }
                else
                {
                    sqlQuery.AppendLine("		,SUM(alpdata.principalPayDown) ");
                }
                sqlQuery.AppendLine(" as cashFlow ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("		FROM ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("		(SELECT code, classOr, acc_typeOr, isAsset FROM Basis_ALb WHERE SimulationId = {0} AND exclude <> 1) AS alb ");
                sqlQuery.AppendLine("		INNER JOIN ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("		(SELECT  ");
                sqlQuery.AppendLine("			ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ALP1.code, ALP3.code), ALP4.code), ALP8.code), ALP7.code), ALP5.code), ALP6.code) as code ");
                sqlQuery.AppendLine("			,ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ALP1.month, ALP3.month), ALP4.month), ALP8.month), ALP7.month), ALP5.month), ALP6.month) as month ");
                sqlQuery.AppendLine("			,ISNULL(alp3.matebal, 0) + ISNULL(alp4.AmtEBal, 0) AS principalPayDown ");
                sqlQuery.AppendLine("			,alp1.stBal AS startBal ");
                sqlQuery.AppendLine("			,ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) AS relNew ");
                sqlQuery.AppendLine("			,ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) as selfNew ");
                sqlQuery.AppendLine("			,CASE  ");
                sqlQuery.AppendLine("				WHEN ALP1.stBal > 0 THEN ");
                sqlQuery.AppendLine("					CASE ");
                sqlQuery.AppendLine("						WHEN ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) > 0 THEN ");
                sqlQuery.AppendLine("							0 ");
                sqlQuery.AppendLine("						ELSE ");
                sqlQuery.AppendLine("							ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) ");
                sqlQuery.AppendLine("					END ");
                sqlQuery.AppendLine("				WHEN ALP1.stBal < 0 THEN ");
                sqlQuery.AppendLine("					CASE ");
                sqlQuery.AppendLine("					WHEN ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) < 0 THEN ");
                sqlQuery.AppendLine("						0 ");
                sqlQuery.AppendLine("					ELSE ");
                sqlQuery.AppendLine("						 ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) ");
                sqlQuery.AppendLine("					END ");
                sqlQuery.AppendLine("				ELSE ");
                sqlQuery.AppendLine("					 ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) ");
                sqlQuery.AppendLine("				END as negNew ");
                sqlQuery.AppendLine("			,ALP7.newBal as newBal ");
                sqlQuery.AppendLine("			,ALP6.repEFreqBal as repEFreqBal ");
                sqlQuery.AppendLine("			,ALP6.repEFreqBal as repEBal ");
                sqlQuery.AppendLine("			,ALP1.EndEbal as endEBal ");
                sqlQuery.AppendLine("			,ISNULL(alp5.PayEBal, 0) as PayEBal ");
                sqlQuery.AppendLine("		 FROM ");
                sqlQuery.AppendLine("	 ");
                sqlQuery.AppendLine("		(SELECT code, month, AmtEBal FROM Basis_ALP4 WHERE SimulationId = {0} AND scenario = '{1}') AS alp4 ");
                sqlQuery.AppendLine("		FULL OUTER JOIN ");
                sqlQuery.AppendLine("		(SELECT code, month, mateBal  FROM Basis_ALP3 WHERE SimulationId = {0} AND scenario = '{1}') AS alp3 ");
                sqlQuery.AppendLine("		ON alp4.code = alp3.code AND alp4.month = alp3.month ");
                sqlQuery.AppendLine("		FULL OUTER JOIN ");
                sqlQuery.AppendLine("		(SELECT code, month,  stBal, endEBal FROM Basis_ALP1 WHERE SimulationId = {0} AND scenario = '{1}') AS alp1 ");
                sqlQuery.AppendLine("		ON alp3.code = alp1.code AND alp3.month = alp1.month ");
                sqlQuery.AppendLine("		FULL OUTER JOIN ");
                sqlQuery.AppendLine("		(SELECT code ");
                sqlQuery.AppendLine("			,month  ");
                sqlQuery.AppendLine("			,RelNewFBal ");
                sqlQuery.AppendLine("			,relNewMBal ");
                sqlQuery.AppendLine("			,relNewLBal ");
                sqlQuery.AppendLine("			,selfFBal ");
                sqlQuery.AppendLine("			,selfMBal ");
                sqlQuery.AppendLine("			,selfLBal FROM basis_ALP8 WHERE SimulationId = {0} AND scenario = '{1}') AS alp8 ");
                sqlQuery.AppendLine("		ON alp1.code = alp8.code AND alp1.Month = alp8.month ");
                sqlQuery.AppendLine("		FULL OUTER JOIN ");
                sqlQuery.AppendLine("		(SELECT code, month,  newBal FROM basis_ALP7 WHERE SimulationId = {0} AND scenario = '{1}') AS alp7  ");
                sqlQuery.AppendLine("		ON alp8.code = alp7.code AND alp8.month = alp7.month ");
                sqlQuery.AppendLine("		FULL OUTER JOIN ");
                sqlQuery.AppendLine("		(SELECT code, month,  PayEBal FROM basis_ALP5 WHERE SimulationId = {0} AND scenario = '{1}') AS alp5 ");
                sqlQuery.AppendLine("		ON alp7.code = alp5.code AND alp7.month = alp5.month ");
                sqlQuery.AppendLine("		FULL OUTER JOIN ");
                sqlQuery.AppendLine("		(SELECT code, month,  repEFreqBal, RepEBal FROM Basis_ALP6 WHERE SimulationId = {0} AND scenario = '{1}') AS alp6 ");
                sqlQuery.AppendLine("		ON alp5.code = alp6.code AND alp5.month = alp6.month) AS alpData ");
                sqlQuery.AppendLine("		ON alpData.code = alb.code ");
                sqlQuery.AppendLine("		INNER JOIN ");
                sqlQuery.AppendLine("		(SELECT code, exclude FROM Basis_ALS WHERE exclude <> 1 AND scenario = '{1}' and SimulationId = {0}) as als ");
                sqlQuery.AppendLine("		ON alb.code = als.Code ");
                sqlQuery.AppendLine("		INNER JOIN  ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("		(SELECT   ");
                sqlQuery.AppendLine("			dgg.name  ");
                sqlQuery.AppendLine("			,dgg.[Priority]  ");
                sqlQuery.AppendLine("			,dgc.IsAsset   ");
                sqlQuery.AppendLine("			,dgc.Acc_Type   ");
                sqlQuery.AppendLine("			,dgc.RbcTier   ");
                sqlQuery.AppendLine("		  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg  ");
                sqlQuery.AppendLine("		  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg  ");
                sqlQuery.AppendLine("		  ON dg.id = dgg.DefinedGroupingId   ");
                sqlQuery.AppendLine("		  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc  ");
                sqlQuery.AppendLine("		  ON dgg.id = dgc.DefinedGroupingGroupId   ");
                sqlQuery.AppendLine("		  WHERE dg.id = " + cashflow.DefinedGrouping.ToString() + ") as dgs ON  ");
                sqlQuery.AppendLine("		  dgs.acc_Type = alb.acc_TypeOr AND dgs.rbcTier =alb.classOr AND dgs.IsAsset = alb.isAsset  ");
                sqlQuery.AppendLine("			GROUP BY dgs.name, dgs.[Priority],  CASE ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 3, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q1Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 6, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q2Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 9, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q3Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 12, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q4Y1' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 15, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q1Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 18, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q2Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 21, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q3Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 24, '{2}') THEN ");
                sqlQuery.AppendLine("				'Q4Y2' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 36, '{2}') THEN ");
                sqlQuery.AppendLine("				'Y3' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 48, '{2}') THEN ");
                sqlQuery.AppendLine("				'Y4' ");
                sqlQuery.AppendLine("			WHEN alpData.month < DATEADD(m, 60, '{2}') THEN ");
                sqlQuery.AppendLine("				'Y5' ");
                sqlQuery.AppendLine("			ELSE ");
                sqlQuery.AppendLine("				'>Y5' ");
                sqlQuery.AppendLine("			END  ");
                sqlQuery.AppendLine("    ORDER BY dgs.[Priority] ");


                string q = String.Format(sqlQuery.ToString(), simulationId, scenario, asOfDate);
                if (simulationId == "" || scenario == "" || asOfDate == "") {
                    errors.Add(String.Format(Utilities.GetErrorTemplate(1), simScen.ScenarioType.Name, simScen.SimulationType.Name, simScen.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                    cfs[counter] = new DataTable();
                }
                else {
                    cfs[counter] = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(simScen.InstitutionDatabaseName), String.Format(sqlQuery.ToString(), simulationId, scenario, asOfDate));
                }
                
                scens[counter] =  simScen.ScenarioType.Name;
                sims[counter] = simScen.SimulationType.Name;
                dates[counter] = asOfDate;
                counter += 1;
            }

           


       
            return new
            {
                tableData = cfs,
                reportType = cashflow.ReportType,
                scens = scens,
                compareScen= cashflow.Compare,
                sims = sims,
                dates = dates,
                errors = errors,
                warnings = warnings
            };
        }
    }
}