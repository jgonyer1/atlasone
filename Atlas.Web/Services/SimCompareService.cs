﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class SimCompareService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object SimCompareViewModel(int simCompareId, DataTable asOfDates)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            SimCompare simCompare = gctx.SimCompares.Where(s => s.Id == simCompareId).FirstOrDefault();
            InstitutionService leftInstService = new InstitutionService(simCompare.LeftInstitutionDatabaseName);
            InstitutionService rightInstService = new InstitutionService(simCompare.RightInstitutionDatabaseName);
            GlobalController gc = new GlobalController();
            Dictionary<string, DataTable> leftPolicyOffsets = gc.PolicyOffsets(simCompare.LeftInstitutionDatabaseName);
            Dictionary<string, DataTable> rightPolicyOffsets = gc.PolicyOffsets(simCompare.RightInstitutionDatabaseName);
            string leftConStr = leftInstService.ConnectionString();

            string rightConStr = rightInstService.ConnectionString();
            simCompare.SimCompareScenarioTypes = simCompare.SimCompareScenarioTypes.OrderBy(s => s.Priority).ToList();

            List<SimCompareSimulationType> LeftSimCompSimTypes = simCompare.SimCompareSimulationTypes.Where(s => s.Side == 0).OrderBy(s => s.Priority).ToList();
            List<SimCompareSimulationType> RightSimCompSimTypes = simCompare.SimCompareSimulationTypes.Where(s => s.Side == 1).OrderBy(s => s.Priority).ToList();
            SimCompareSimulationType curSimCompSimType;
            var rep = gctx.Reports.Where(s => s.Id == simCompareId).FirstOrDefault();
            string leftAodStr = leftInstService.GetInformation(rep.Package.AsOfDate, LeftSimCompSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            string rightAodStr = rightInstService.GetInformation(rep.Package.AsOfDate, RightSimCompSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            DataTable modelSetupVals = Utilities.ExecuteSql(leftConStr, "SELECT ReportTaxEquivalent FROM Atlas_ModelSetup WHERE AsOfDate = '" + rep.Package.AsOfDate.ToShortDateString() + "'");

            DataTable leftVarianceTbl = new DataTable();
            DataTable leftParentSimTbl = new DataTable();
            DataTable leftChartData = new DataTable();
            DataTable rightVarianceTbl = new DataTable();
            DataTable rightParentSimTbl = new DataTable();
            DataTable rightChartData = new DataTable();
            DataTable tempDataSet = new DataTable();

            string compareScenarioName = "";
            string scenarioTypeIn = "";

            if (simCompare == null) throw new ArgumentOutOfRangeException("simCompareId", "SimCompare not found");

            //hard push the config values for overridable fields
            //tax equivalent

            //parent simulations
            if (!simCompare.OverrideLeftParentSimulationType)
            {
                if (leftPolicyOffsets.ContainsKey(leftAodStr))
                {
                    int simTypeId = Convert.ToInt32(leftPolicyOffsets[leftAodStr].Rows[0]["niiId"].ToString()); ;
                    SimulationType simType = gctx.SimulationTypes.Where(s => s.Id == simTypeId).First();
                    simCompare.SimCompareSimulationTypes.Where(s => s.Side == 0 && s.Priority == 0).First().SimulationType = simType;
                    simCompare.SimCompareSimulationTypes.Where(s => s.Side == 0 && s.Priority == 0).First().SimulationTypeId = simType.Id;
                    LeftSimCompSimTypes = simCompare.SimCompareSimulationTypes.Where(s => s.Side == 0).OrderBy(s => s.Priority).ToList();
                }
            }
            if (!simCompare.OverrideRightParentSimulationType)
            {
                if (rightPolicyOffsets.ContainsKey(rightAodStr))
                {
                    int simTypeId = Convert.ToInt32(rightPolicyOffsets[rightAodStr].Rows[0]["niiId"].ToString()); ;
                    SimulationType simType = gctx.SimulationTypes.Where(s => s.Id == simTypeId).First();
                    simCompare.SimCompareSimulationTypes.Where(s => s.Side == 1 && s.Priority == 0).First().SimulationType = simType;
                    simCompare.SimCompareSimulationTypes.Where(s => s.Side == 1 && s.Priority == 0).First().SimulationTypeId = simType.Id;
                    RightSimCompSimTypes = simCompare.SimCompareSimulationTypes.Where(s => s.Side == 1).OrderBy(s => s.Priority).ToList();
                }
            }


            //graph names
            if (!simCompare.LeftNameOverride)
            {
                simCompare.LeftGraphName = LeftSimCompSimTypes[0].SimulationType.Name + " Simulation as of " + leftAodStr;
            }
            if (!simCompare.RightNameOverride)
            {
                simCompare.RightGraphName = RightSimCompSimTypes[0].SimulationType.Name + " Simulation as of " + rightAodStr;
            }
            gctx.SaveChanges();

            //Stores In Statement for scenarioTypeid

            //Sets Up Table That We Can Keep Cloning 
            foreach (var scen in simCompare.SimCompareScenarioTypes)
            {
                if (scen.ScenarioTypeId.ToString() == simCompare.ComparativeScenario)
                {
                    compareScenarioName = scen.ScenarioType.Name;
                }
                if (scenarioTypeIn == "")
                {
                    scenarioTypeIn = "(" + scen.ScenarioTypeId.ToString();
                }
                else
                {
                    scenarioTypeIn += "," + scen.ScenarioTypeId.ToString();
                }
            }
            scenarioTypeIn += ")";

            //Here starts the new Age of SimCompare...behold

            curSimCompSimType = LeftSimCompSimTypes[0];
            leftParentSimTbl = SimulationScenarioTable(simCompare.IsQuarterly, rep.Package.AsOfDate, curSimCompSimType.AsOfDateOffset, curSimCompSimType.SimulationType, simCompare.LeftInstitutionDatabaseName, scenarioTypeIn, simCompare.TaxEquivalentYields, simCompare.NiiOption);
            leftVarianceTbl = leftParentSimTbl.Copy();

            for (int c = 0; c < leftParentSimTbl.Columns.Count; c++)
            {
                leftParentSimTbl.Columns[c].ReadOnly = false;
                leftVarianceTbl.Columns[c].ReadOnly = false;
            }
            foreach (DataRow dr in leftVarianceTbl.Rows)
            {
                dr["Amount"] = 0;
            }

            leftVarianceTbl = getUpdatedVarianceTable(LeftSimCompSimTypes, leftParentSimTbl, leftVarianceTbl, simCompare, scenarioTypeIn, rep.Package.AsOfDate);
            leftChartData = getChartData(leftParentSimTbl, leftVarianceTbl);
            //Now do the right
            curSimCompSimType = RightSimCompSimTypes[0];
            rightParentSimTbl = SimulationScenarioTable(simCompare.IsQuarterly, rep.Package.AsOfDate, curSimCompSimType.AsOfDateOffset, curSimCompSimType.SimulationType, simCompare.RightInstitutionDatabaseName, scenarioTypeIn, simCompare.TaxEquivalentYields, simCompare.NiiOption);
            rightVarianceTbl = rightParentSimTbl.Copy();
            for (int c = 0; c < rightParentSimTbl.Columns.Count; c++)
            {
                rightParentSimTbl.Columns[c].ReadOnly = false;
                rightVarianceTbl.Columns[c].ReadOnly = false;
            }
            foreach (DataRow dr in rightVarianceTbl.Rows)
            {
                dr["Amount"] = 0;
            }

            rightVarianceTbl = getUpdatedVarianceTable(RightSimCompSimTypes, rightParentSimTbl, rightVarianceTbl, simCompare, scenarioTypeIn, rep.Package.AsOfDate);
            rightChartData = getChartData(rightParentSimTbl, rightVarianceTbl);
            SimulationType LeftSimulationType = LeftSimCompSimTypes[0].SimulationType;//simCompare.SimCompareSimulationTypes.Where(s => s.Side == 0).Select(sm => sm.SimulationType).First();
            SimulationType RightSimulationType = RightSimCompSimTypes[0].SimulationType;//simCompare.SimCompareSimulationTypes.Where(s => s.Side == 1).Select(sm => sm.SimulationType).First();


            bool leftTaxEquivToUse = false;
            if (modelSetupVals.Rows.Count > 0 && !simCompare.OverrideTaxEquiv)
            {
                leftTaxEquivToUse = (bool)modelSetupVals.Rows[0][0];
            }
            else
            {
                leftTaxEquivToUse = simCompare.TaxEquivalentYields;
            }

            DataTable compareScens = SimulationScenarioTable(simCompare.IsQuarterly, rep.Package.AsOfDate, LeftSimCompSimTypes[0].AsOfDateOffset, LeftSimCompSimTypes[0].SimulationType, simCompare.LeftInstitutionDatabaseName, "(" + simCompare.ComparativeScenario + ")", simCompare.TaxEquivalentYields, simCompare.NiiOption);
            string compareScenName = compareScens.Rows[0]["name"].ToString();

            string leftSimulationId = "-999";
            leftSimulationId = LeftSimCompSimTypes[0].SimulationTypeId.ToString();//simCompare.SimCompareSimulationTypes.Where(s => s.Side == 0).FirstOrDefault().SimulationTypeId.ToString();
            string rightSimulationId = "-999";
            rightSimulationId = LeftSimCompSimTypes[0].SimulationTypeId.ToString();//simCompare.SimCompareSimulationTypes.Where(s => s.Side == 1).FirstOrDefault().SimulationTypeId.ToString();

            DataSet leftComparePolicies = GetComparePolicies(DateTime.Parse(asOfDates.Rows[LeftSimCompSimTypes[0].AsOfDateOffset][0].ToString()), simCompare.PercChangeComparator, scenarioTypeIn, simCompare.LeftInstitutionDatabaseName, leftSimulationId);
            DataSet rightComparePolicies = GetComparePolicies(DateTime.Parse(asOfDates.Rows[RightSimCompSimTypes[0].AsOfDateOffset][0].ToString()), simCompare.PercChangeComparator, scenarioTypeIn, simCompare.RightInstitutionDatabaseName, rightSimulationId);
            DataTable retCompareTableRight = new DataTable();
            DataTable retCompareTableLeft = new DataTable();

            Dictionary<int, object> simulations = new Dictionary<int, object>();
            Dictionary<string, Dictionary<string, GraphValues[]>> chartData = new Dictionary<string, Dictionary<string, GraphValues[]>>();
            chartData.Add("left", new Dictionary<string, GraphValues[]>());
            chartData.Add("right", new Dictionary<string, GraphValues[]>());

            Dictionary<string, Dictionary<string, GraphValues[]>> diffData = new Dictionary<string, Dictionary<string, GraphValues[]>>();
            diffData.Add("left", new Dictionary<string, GraphValues[]>());
            diffData.Add("right", new Dictionary<string, GraphValues[]>());

            string filter = "";

            //Lets try this again
            DataTable fullDiffTable = GetFullDiffTable(simCompare.IsQuarterly);
            DataTable compareScenTable = GetFullDiffTable(simCompare.IsQuarterly);
            foreach (DataRow dr in compareScens.Rows)
            {
                string scenario = dr["name"].ToString();
                filter = "scenario = '" + scenario + "'";
                DataRow[] drs = compareScenTable.Select(filter);
                string colName = "";
                if (drs.Length == 0)
                {
                    DataRow newRow = compareScenTable.NewRow();
                    newRow["scenario"] = scenario;
                    compareScenTable.Rows.Add(newRow);
                    drs = compareScenTable.Select(filter);
                }
                if (drs.Length == 1)
                {

                    switch (dr["yearGroup"].ToString())
                    {
                        case "Year 1":
                            colName = "left_Y1_nii";
                            break;
                        case "Year 2":
                            colName = "left_Y2_nii";
                            break;
                        case "Year 3":
                            colName = "left_Y3_nii";
                            break;
                        case "Year 4":
                            colName = "left_Y4_nii";
                            break;
                        case "Year 5":
                            colName = "left_Y5_nii";
                            break;
                    }
                }
                if (String.IsNullOrEmpty(drs[0][colName].ToString()))
                {
                    drs[0][colName] = (double)dr["Amount"];
                }
                else
                {
                    drs[0][colName] = (double)drs[0][colName] + (double)dr["Amount"];
                }
            }
            if (compareScenTable.Rows.Count == 1 && !simCompare.IsQuarterly)
            {
                compareScenTable.Rows[0]["left_Y2_cum_nii"] = Convert.ToDouble(compareScenTable.Rows[0]["left_Y1_nii"].ToString()) + Convert.ToDouble(compareScenTable.Rows[0]["left_Y2_nii"].ToString());
            }

            //now do it for Right simulation
            compareScens = SimulationScenarioTable(simCompare.IsQuarterly, rep.Package.AsOfDate, RightSimCompSimTypes[0].AsOfDateOffset, RightSimCompSimTypes[0].SimulationType, simCompare.RightInstitutionDatabaseName, "(" + simCompare.ComparativeScenario + ")", simCompare.TaxEquivalentYields, simCompare.NiiOption);
            foreach (DataRow dr in compareScens.Rows)
            {
                string scenario = dr["name"].ToString();
                filter = "scenario = '" + scenario + "'";
                DataRow[] drs = compareScenTable.Select(filter);
                string colName = "";
                if (drs.Length == 0)
                {
                    DataRow newRow = compareScenTable.NewRow();
                    newRow["scenario"] = scenario;
                    compareScenTable.Rows.Add(newRow);
                    drs = compareScenTable.Select(filter);
                }
                if (drs.Length == 1)
                {

                    switch (dr["yearGroup"].ToString())
                    {
                        case "Year 1":
                            colName = "right_Y1_nii";
                            break;
                        case "Year 2":
                            colName = "right_Y2_nii";
                            break;
                        case "Year 3":
                            colName = "right_Y3_nii";
                            break;
                        case "Year 4":
                            colName = "right_Y4_nii";
                            break;
                        case "Year 5":
                            colName = "right_Y5_nii";
                            break;
                    }
                }
                if (String.IsNullOrEmpty(drs[0][colName].ToString()))
                {
                    drs[0][colName] = (double)dr["Amount"];
                }
                else
                {
                    drs[0][colName] = (double)drs[0][colName] + (double)dr["Amount"];
                }
            }

            //now do cumulatives for compare scenario
            if (compareScenTable.Rows.Count == 1 && !simCompare.IsQuarterly)
            {
                compareScenTable.Rows[0]["right_Y2_cum_nii"] = Convert.ToDouble(compareScenTable.Rows[0]["right_Y1_nii"].ToString()) + Convert.ToDouble(compareScenTable.Rows[0]["right_Y2_nii"].ToString());
            }


            string side = "left";

            bool parent = true;
            //SimulationType sim = simCompare.LeftSimulationType;
            //DataTable tmpTable;
            //tmpTable = SimulationScenarioTable(simCompare.IsQuarterly, rep.Package.AsOfDate, simCompare.LeftAsOfDateOffset, sim, simCompare.LeftInstitutionDatabaseName, scenarioTypeIn, simCompare.TaxEquivalentYields);

            //if (tmpTable.Rows.Count == 0)
            //{
            //    errors.Add(String.Format(Utilities.GetErrorTemplate(2), sim.Name, simCompare.LeftAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            //}
            int scenCount = 0;
            string prevScen = "";

            //start assembling chartData
            foreach (DataRow dr in leftChartData.Rows)
            {
                string scenario = dr["name"].ToString();


                //Set Prev Scenario
                if (prevScen == "")
                {
                    prevScen = scenario;
                }

                //Reset Scen Count if it was not last scenario
                if (scenario != prevScen)
                {
                    scenCount = 0;
                }

                if (parent)
                {
                    if (!chartData[side].ContainsKey(scenario))
                    {
                        if (simCompare.IsQuarterly)
                        {
                            chartData[side].Add(scenario, new GraphValues[20]);
                        }
                        else
                        {
                            chartData[side].Add(scenario, new GraphValues[24]);
                        }

                        for (var z = 0; z < chartData[side][scenario].Length; z++)
                        {
                            chartData[side][scenario][z] = new GraphValues();
                        }

                    }

                    chartData[side][scenario][scenCount].value = Double.Parse(dr["Amount"].ToString());
                    chartData[side][scenario][scenCount].month = dr["month"].ToString();
                }
                else
                {
                    if (!diffData[side].ContainsKey(scenario))
                    {
                        if (simCompare.IsQuarterly)
                        {
                            diffData[side].Add(scenario, new GraphValues[20]);
                        }
                        else
                        {
                            diffData[side].Add(scenario, new GraphValues[24]);
                        }
                        for (var z = 0; z < diffData[side][scenario].Length; z++)
                        {
                            diffData[side][scenario][z] = new GraphValues();
                        }
                    }

                    //diffData[side][scenario][scenCount] += (chartData[side][scenario][scenCount] - Double.Parse(dr["Amount"].ToString()));
                    if (chartData[side].ContainsKey(scenario))
                    {
                        diffData[side][scenario][scenCount].value += (Double.Parse(dr["Amount"].ToString()) - chartData[side][scenario][scenCount].value);
                        diffData[side][scenario][scenCount].month += dr["month"].ToString();
                    }

                }
                scenCount += 1;
                prevScen = scenario;
            }
            //populate full difference table
            int mQ = simCompare.IsQuarterly ? 4 : 12;
            foreach (KeyValuePair<string, GraphValues[]> kvp in chartData[side])
            {
                filter = "scenario = '" + kvp.Key + "'";
                DataRow[] drs = fullDiffTable.Select(filter);

                if (drs.Length == 0)
                {
                    DataRow newRow = fullDiffTable.NewRow();
                    newRow["scenario"] = kvp.Key;
                    fullDiffTable.Rows.Add(newRow);
                    drs = fullDiffTable.Select(filter);
                }

                if (drs.Length == 1)//there can only be one highlander
                {
                    for (var z = 0; z < chartData[side][kvp.Key].Length; z++)
                    {
                        int yearNum = (int)Math.Floor((double)((z + mQ) / mQ));
                        string colName = side + "_Y" + yearNum + "_nii";
                        if (string.IsNullOrEmpty(drs[0][colName].ToString()))
                        {
                            drs[0][colName] = chartData[side][kvp.Key][z].value;
                        }
                        else
                        {
                            drs[0][colName] = (double)drs[0][colName] + chartData[side][kvp.Key][z].value;
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, GraphValues[]> kvp in chartData[side])
            {
                if (diffData[side].ContainsKey(kvp.Key))
                {
                    for (var z = 0; z < diffData[side][kvp.Key].Length; z++)
                    {
                        chartData[side][kvp.Key][z].value += diffData[side][kvp.Key][z].value;
                    }
                }
            }
            side = "right";

            //bool parent = true;
            //sim = simCompare.RightSimulationType;
            ////DataTable tmpTable;
            //tmpTable = SimulationScenarioTable(simCompare.IsQuarterly, rep.Package.AsOfDate, simCompare.RightAsOfDateOffset, sim, simCompare.RightInstitutionDatabaseName, scenarioTypeIn, simCompare.TaxEquivalentYields);

            //if (tmpTable.Rows.Count == 0)
            //{
            //    errors.Add(String.Format(Utilities.GetErrorTemplate(2), sim.Name, simCompare.RightAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            //}

            scenCount = 0;
            prevScen = "";


            foreach (DataRow dr in rightChartData.Rows)
            {
                string scenario = dr["name"].ToString();

                //Set Prev Scenario
                if (prevScen == "")
                {
                    prevScen = scenario;
                }

                //Reset Scen Count if it was not last scenario
                if (scenario != prevScen)
                {
                    scenCount = 0;
                }

                if (parent)
                {
                    if (!chartData[side].ContainsKey(scenario))
                    {
                        if (simCompare.IsQuarterly)
                        {
                            chartData[side].Add(scenario, new GraphValues[20]);
                        }
                        else
                        {
                            chartData[side].Add(scenario, new GraphValues[24]);
                        }

                        for (var z = 0; z < chartData[side][scenario].Length; z++)
                        {
                            chartData[side][scenario][z] = new GraphValues();
                        }

                    }
                    chartData[side][scenario][scenCount].month = dr["month"].ToString();
                    chartData[side][scenario][scenCount].value = Double.Parse(dr["Amount"].ToString());
                }
                else
                {
                    if (!diffData[side].ContainsKey(scenario))
                    {
                        if (simCompare.IsQuarterly)
                        {
                            diffData[side].Add(scenario, new GraphValues[20]);
                        }
                        else
                        {
                            diffData[side].Add(scenario, new GraphValues[24]);
                        }
                        for (var z = 0; z < diffData[side][scenario].Length; z++)
                        {
                            diffData[side][scenario][z] = new GraphValues();
                        }
                    }

                    //diffData[side][scenario][scenCount] += (chartData[side][scenario][scenCount] - Double.Parse(dr["Amount"].ToString()));
                    if (chartData[side].ContainsKey(scenario))
                    {
                        diffData[side][scenario][scenCount].value += (Double.Parse(dr["Amount"].ToString()) - chartData[side][scenario][scenCount].value);
                        diffData[side][scenario][scenCount].month += dr["month"].ToString();
                    }

                }
                scenCount += 1;
                prevScen = scenario;
            }
            //populate full difference table
            mQ = simCompare.IsQuarterly ? 4 : 12;
            foreach (KeyValuePair<string, GraphValues[]> kvp in chartData[side])
            {
                filter = "scenario = '" + kvp.Key + "'";
                DataRow[] drs = fullDiffTable.Select(filter);

                if (drs.Length == 0)
                {
                    DataRow newRow = fullDiffTable.NewRow();
                    newRow["scenario"] = kvp.Key;
                    fullDiffTable.Rows.Add(newRow);
                    drs = fullDiffTable.Select(filter);
                }

                if (drs.Length == 1)//there can only be one highlander
                {
                    for (var z = 0; z < chartData[side][kvp.Key].Length; z++)
                    {
                        int yearNum = (int)Math.Floor((double)((z + mQ) / mQ));
                        string colName = side + "_Y" + yearNum + "_nii";
                        if (string.IsNullOrEmpty(drs[0][colName].ToString()))
                        {
                            drs[0][colName] = chartData[side][kvp.Key][z].value;
                        }
                        else
                        {
                            drs[0][colName] = (double)drs[0][colName] + chartData[side][kvp.Key][z].value;
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, GraphValues[]> kvp in chartData[side])
            {
                if (diffData[side].ContainsKey(kvp.Key))
                {
                    for (var z = 0; z < diffData[side][kvp.Key].Length; z++)
                    {
                        chartData[side][kvp.Key][z].value += diffData[side][kvp.Key][z].value;
                    }
                }
            }

            //populate the difference part of the difference table
            //at this point left and right are populated
            //need to do cumulativess and differences
            double compareScenNumLeft = Convert.ToDouble(compareScenTable.Rows[0]["left_Y1_nii"].ToString());
            double compareScenNumRight = Convert.ToDouble(compareScenTable.Rows[0]["right_Y1_nii"].ToString());
            foreach (DataRow dr in fullDiffTable.Rows)
            {
                double dollarDelta = 0;
                if (simCompare.IsQuarterly)
                {
                    //dr["diff_Y1_nii"] = Convert.ToDouble(dr["right_Y1_nii"].ToString()) - Convert.ToDouble(dr["left_Y1_nii"].ToString());
                    //dr["diff_Y2_nii"] = Convert.ToDouble(dr["right_Y2_nii"].ToString()) - Convert.ToDouble(dr["left_Y2_nii"].ToString());
                    //dr["diff_Y3_nii"] = Convert.ToDouble(dr["right_Y3_nii"].ToString()) - Convert.ToDouble(dr["left_Y3_nii"].ToString());
                    //dr["diff_Y4_nii"] = Convert.ToDouble(dr["right_Y4_nii"].ToString()) - Convert.ToDouble(dr["left_Y4_nii"].ToString());
                    //dr["diff_Y5_nii"] = Convert.ToDouble(dr["right_Y5_nii"].ToString()) - Convert.ToDouble(dr["left_Y5_nii"].ToString());
                    dr["diff_Y1_nii"] = Utilities.SafeDoubleConvert(dr["right_Y1_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y1_nii"].ToString());
                    dr["diff_Y2_nii"] = Utilities.SafeDoubleConvert(dr["right_Y2_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y2_nii"].ToString());
                    dr["diff_Y3_nii"] = Utilities.SafeDoubleConvert(dr["right_Y3_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y3_nii"].ToString());
                    dr["diff_Y4_nii"] = Utilities.SafeDoubleConvert(dr["right_Y4_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y4_nii"].ToString());
                    dr["diff_Y5_nii"] = Utilities.SafeDoubleConvert(dr["right_Y5_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y5_nii"].ToString());
                }
                else
                {
                    dollarDelta = Utilities.SafeDoubleConvert(dr["left_Y2_nii"].ToString()) - compareScenNumLeft;
                    dr["left_Y2_dollar_delta"] = dollarDelta;
                    dr["left_Y2_perc_delta"] = dollarDelta / compareScenNumLeft;
                    dr["left_Y2_cum_nii"] = Utilities.SafeDoubleConvert(dr["left_Y2_nii"].ToString()) + Utilities.SafeDoubleConvert(dr["left_Y1_nii"].ToString());

                    dollarDelta = Utilities.SafeDoubleConvert(dr["right_Y2_nii"].ToString()) - compareScenNumRight;
                    dr["right_Y2_dollar_delta"] = dollarDelta;
                    dr["right_Y2_perc_delta"] = dollarDelta / compareScenNumRight;
                    dr["right_Y2_cum_nii"] = Utilities.SafeDoubleConvert(dr["right_Y2_nii"].ToString()) + Utilities.SafeDoubleConvert(dr["right_Y1_nii"].ToString());

                    if (dr["scenario"].ToString().ToUpper() == compareScenName.ToUpper())
                    {
                        dr["left_Y1_dollar_delta"] = DBNull.Value;
                        dr["left_Y1_perc_delta"] = DBNull.Value;
                        dr["right_Y1_dollar_delta"] = DBNull.Value;
                        dr["right_Y1_perc_delta"] = DBNull.Value;
                        dr["left_Y2_cum_dollar_delta"] = DBNull.Value;
                        dr["left_Y2_cum_perc_delta"] = DBNull.Value;
                        dr["right_Y2_cum_dollar_delta"] = DBNull.Value;
                        dr["right_Y2_cum_perc_delta"] = DBNull.Value;
                    }
                    else
                    {
                        dollarDelta = Utilities.SafeDoubleConvert(dr["left_Y1_nii"].ToString()) - compareScenNumLeft;
                        dr["left_Y1_dollar_delta"] = dollarDelta;
                        dr["left_Y1_perc_delta"] = dollarDelta / compareScenNumLeft;

                        dollarDelta = Utilities.SafeDoubleConvert(dr["left_Y2_cum_nii"].ToString()) - Utilities.SafeDoubleConvert(compareScenTable.Rows[0]["left_Y2_cum_nii"].ToString());
                        dr["left_Y2_cum_dollar_delta"] = dollarDelta;
                        dr["left_Y2_cum_perc_delta"] = dollarDelta / Utilities.SafeDoubleConvert(compareScenTable.Rows[0]["left_Y2_cum_nii"].ToString());

                        dollarDelta = Utilities.SafeDoubleConvert(dr["right_Y1_nii"].ToString()) - compareScenNumRight;
                        dr["right_Y1_dollar_delta"] = dollarDelta;
                        dr["right_Y1_perc_delta"] = dollarDelta / compareScenNumRight;

                        dollarDelta = Utilities.SafeDoubleConvert(dr["right_Y2_cum_nii"].ToString()) - Utilities.SafeDoubleConvert(compareScenTable.Rows[0]["right_Y2_cum_nii"].ToString());
                        dr["right_Y2_cum_dollar_delta"] = dollarDelta;
                        dr["right_Y2_cum_perc_delta"] = dollarDelta / Utilities.SafeDoubleConvert(compareScenTable.Rows[0]["right_Y2_cum_nii"].ToString());
                    }

                    //now do differences
                    dr["diff_Y1_nii"] = Utilities.SafeDoubleConvert(dr["right_Y1_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y1_nii"].ToString());
                    dr["diff_Y2_nii"] = Utilities.SafeDoubleConvert(dr["right_Y2_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y2_nii"].ToString());
                    dr["diff_Y2_cum_nii"] = Utilities.SafeDoubleConvert(dr["right_Y2_cum_nii"].ToString()) - Utilities.SafeDoubleConvert(dr["left_Y2_cum_nii"].ToString());

                }
            }

            DataTable fullPolicyCompareTable = new DataTable();
            fullPolicyCompareTable.Columns.Add("compare_label", typeof(string));
            fullPolicyCompareTable.Columns.Add("scenario", typeof(string));
            fullPolicyCompareTable.Columns.Add("left_policy1", typeof(double));
            fullPolicyCompareTable.Columns.Add("left_Y1", typeof(double));
            fullPolicyCompareTable.Columns.Add("left_policy2", typeof(double));
            fullPolicyCompareTable.Columns.Add("left_Y2", typeof(double));
            fullPolicyCompareTable.Columns.Add("diff_Y1", typeof(double));
            fullPolicyCompareTable.Columns.Add("diff_Y2", typeof(double));
            fullPolicyCompareTable.Columns.Add("right_policy1", typeof(double));
            fullPolicyCompareTable.Columns.Add("right_Y1", typeof(double));
            fullPolicyCompareTable.Columns.Add("right_policy2", typeof(double));
            fullPolicyCompareTable.Columns.Add("right_Y2", typeof(double));
            //policy / compare table
            filter = "";
            double a = 0;
            double b = 0;
            if (compareScenTable.Rows.Count > 0)
            {
                foreach (DataRow dr in fullDiffTable.Rows)
                {
                    filter = "Name = '" + dr["scenario"] + "'";
                    DataRow[] drs = fullDiffTable.Select("scenario = '" + dr["scenario"] + "'");
                    DataRow[] leftPolDrs = leftComparePolicies.Tables["Table1"].Select(filter);
                    DataRow[] rightPolDrs = rightComparePolicies.Tables["Table1"].Select(filter);
                    DataRow newRow = fullPolicyCompareTable.NewRow();
                    newRow["scenario"] = dr["scenario"];
                    if (leftPolDrs.Length > 0)
                    {
                        newRow["left_policy1"] = Convert.ToDouble(leftPolDrs[0]["PolicyLimit"]);
                    }
                    else
                    {
                        newRow["left_policy1"] = DBNull.Value;
                    }
                    if (rightPolDrs.Length > 0)
                    {
                        newRow["right_policy1"] = Convert.ToDouble(rightPolDrs[0]["PolicyLimit"]);
                    }
                    else
                    {
                        newRow["right_policy1"] = DBNull.Value;
                    }
                    leftPolDrs = leftComparePolicies.Tables["Table2"].Select(filter);
                    rightPolDrs = rightComparePolicies.Tables["Table2"].Select(filter);
                    if (leftPolDrs.Length > 0)
                    {
                        newRow["left_policy2"] = Convert.ToDouble(leftPolDrs[0]["PolicyLimit"]);
                    }
                    else
                    {
                        newRow["left_policy2"] = DBNull.Value;
                    }
                    if (rightPolDrs.Length > 0)
                    {
                        newRow["right_policy2"] = Convert.ToDouble(rightPolDrs[0]["PolicyLimit"]);
                    }
                    else
                    {
                        newRow["right_policy2"] = DBNull.Value;
                    }
                    if (drs.Length > 0)
                    {
                        if (drs[0]["scenario"].ToString() == compareScenTable.Rows[0]["scenario"].ToString())
                        {
                            newRow["left_Y1"] = DBNull.Value;
                            newRow["right_Y1"] = DBNull.Value;
                        }
                        else
                        {
                            newRow["left_Y1"] = (Convert.ToDouble(drs[0]["left_Y1_nii"]) - Convert.ToDouble(compareScenTable.Rows[0]["left_Y1_nii"])) / Convert.ToDouble(compareScenTable.Rows[0]["left_Y1_nii"]);
                            newRow["right_Y1"] = (Convert.ToDouble(drs[0]["right_Y1_nii"]) - Convert.ToDouble(compareScenTable.Rows[0]["right_Y1_nii"])) / Convert.ToDouble(compareScenTable.Rows[0]["right_Y1_nii"]);
                        }



                        if (simCompare.PercChangeComparator == "year2Year1")//Year 2 to Year1 Base
                        {
                            a = (Convert.ToDouble(drs[0]["left_Y2_nii"]) / Convert.ToDouble(compareScenTable.Rows[0]["left_Y1_nii"])) - 1;
                            b = (Convert.ToDouble(drs[0]["right_Y2_nii"]) / Convert.ToDouble(compareScenTable.Rows[0]["right_Y1_nii"])) - 1;
                        }
                        else if (simCompare.PercChangeComparator == "year2Year2")//Year2 to Year2 Base
                        {
                            a = (Convert.ToDouble(drs[0]["left_Y2_nii"]) / Convert.ToDouble(compareScenTable.Rows[0]["left_Y2_nii"])) - 1;
                            b = (Convert.ToDouble(drs[0]["right_Y2_nii"]) / Convert.ToDouble(compareScenTable.Rows[0]["right_Y2_nii"])) - 1;
                        }
                        else if (simCompare.PercChangeComparator == "24Month")//2 Cumulative
                        {
                            if (simCompare.IsQuarterly)
                            {
                                a = ((Convert.ToDouble(drs[0]["left_Y1_nii"]) + Convert.ToDouble(drs[0]["left_Y2_nii"])) / (Convert.ToDouble(compareScenTable.Rows[0]["left_Y1_nii"]) + Convert.ToDouble(compareScenTable.Rows[0]["left_Y2_nii"]))) - 1;
                                b = ((Convert.ToDouble(drs[0]["right_Y1_nii"]) + Convert.ToDouble(drs[0]["right_Y2_nii"])) / (Convert.ToDouble(compareScenTable.Rows[0]["right_Y1_nii"]) + Convert.ToDouble(compareScenTable.Rows[0]["right_Y2_nii"]))) - 1;
                            }
                            else
                            {
                                a = ((Convert.ToDouble(drs[0]["left_Y2_cum_nii"]) / Convert.ToDouble(compareScenTable.Rows[0]["left_Y2_cum_nii"]))) - 1;
                                b = (Convert.ToDouble(drs[0]["right_Y2_cum_nii"]) / Convert.ToDouble(compareScenTable.Rows[0]["right_Y2_cum_nii"])) - 1;
                            }
                        }
                        newRow["left_Y2"] = a;
                        newRow["right_Y2"] = b;
                        if (newRow["left_Y1"] != DBNull.Value)
                        {
                            newRow["diff_Y1"] = Convert.ToDouble(newRow["left_Y1"]) - Convert.ToDouble(newRow["right_Y1"]);
                        }
                        else
                        {
                            newRow["diff_Y1"] = DBNull.Value;
                        }

                        newRow["diff_Y2"] = Convert.ToDouble(newRow["left_Y2"]) - Convert.ToDouble(newRow["right_Y2"]);

                        if (simCompare.PercChangeComparator == "year2Year1" || dr["scenario"].ToString() != compareScenarioName)
                        {
                            fullPolicyCompareTable.Rows.Add(newRow);
                        }

                    }
                }
            }



            DataTable differenceDifferenceTable = new DataTable();
            differenceDifferenceTable.Columns.Add("Year 1", typeof(String));
            differenceDifferenceTable.Columns.Add("Year 2", typeof(String));
            a = 0;
            b = 0;

            for (var r = 0; r < retCompareTableLeft.Rows.Count; r++)
            {
                double result = 0;
                DataRow newRow = differenceDifferenceTable.NewRow();
                if (String.IsNullOrEmpty(retCompareTableLeft.Rows[r]["Year 1"].ToString()) || String.IsNullOrEmpty(retCompareTableRight.Rows[r]["Year 1"].ToString()))
                {
                    newRow["Year 1"] = "";
                }
                else
                {
                    a = Convert.ToDouble(retCompareTableLeft.Rows[r]["Year 1"].ToString().Replace("%", ""));
                    b = Convert.ToDouble(retCompareTableRight.Rows[r]["Year 1"].ToString().Replace("%", ""));
                    newRow["Year 1"] = a - b;
                    //newRow["Year 1"] = ((double)retCompareTableLeft.Rows[r]["Year 1"] - (double)retCompareTableRight.Rows[r]["Year 1"]).ToString();
                }
                if (String.IsNullOrEmpty(retCompareTableLeft.Rows[r]["Year 2"].ToString()) || String.IsNullOrEmpty(retCompareTableRight.Rows[r]["Year 2"].ToString()))
                {
                    newRow["Year 2"] = "";
                }
                else
                {
                    a = Convert.ToDouble(retCompareTableLeft.Rows[r]["Year 2"].ToString().Replace("%", ""));
                    b = Convert.ToDouble(retCompareTableRight.Rows[r]["Year 2"].ToString().Replace("%", ""));
                    newRow["Year 2"] = a - b;
                    //newRow["Year 2"] = ((double)retCompareTableLeft.Rows[r]["Year 2"] - (double)retCompareTableRight.Rows[r]["Year 2"]).ToString();
                }
            }
            string leftPolicyNiiSimulationId = "";
            string rightPolicyNiiSimulationId = "";
            string qry = "select niisimulationtypeid from ATLAS_Policy where asofdate = '" + rep.Package.AsOfDate.ToShortDateString() + "'";
            InstitutionService instService = new InstitutionService(simCompare.LeftInstitutionDatabaseName);
            string conString = instService.ConnectionString();
            DataTable polSimId = Utilities.ExecuteSql(conString, qry);
            if (polSimId.Rows.Count == 0)
            {
                leftPolicyNiiSimulationId = "-999";
            }
            else
            {
                leftPolicyNiiSimulationId = polSimId.Rows[0][0].ToString();
            }

            instService = new InstitutionService(simCompare.RightInstitutionDatabaseName);
            conString = instService.ConnectionString();
            polSimId = Utilities.ExecuteSql(conString, qry);
            if (polSimId.Rows.Count == 0)
            {
                rightPolicyNiiSimulationId = "-999";
            }
            else
            {
                rightPolicyNiiSimulationId = polSimId.Rows[0][0].ToString();
            }



            return new
            {
                leftSimulationId = leftSimulationId,
                rightSimulationId = rightSimulationId,
                leftPolicyNiiSimulationId = leftPolicyNiiSimulationId,
                rightPolicyNiiSimulationId = leftPolicyNiiSimulationId,
                left = chartData["left"],
                right = chartData["right"],
                compareScenarioName = compareScenName,
                //leftPercentChangeData = retCompareTableLeft,
                //rightPercentChangeData = retCompareTableRight,
                percentChangeDifference = differenceDifferenceTable,
                fullDiffTable = fullDiffTable,
                fullPolicyCompareTable = fullPolicyCompareTable,
                warnings = warnings,
                errors = errors
            };

        }

        public DataTable getUpdatedVarianceTable(List<SimCompareSimulationType> SimCompSimTypes, DataTable parentSimTbl, DataTable varianceTbl, SimCompare simCompare, string scenarioTypeIn, DateTime AsOfDate)
        {
            SimCompareSimulationType curSimCompSimType;
            DataTable tempDataTable;
            DataRow[] parentSet;
            DataRow parentRow;
            DataRow varianceRow;
            DataRow childRow;
            double tempVal;
            int rowCounter = 0;
            int simCounter = 1;
            for (simCounter = 1; simCounter < SimCompSimTypes.Count; simCounter++)
            {
                curSimCompSimType = SimCompSimTypes[simCounter];
                tempDataTable = SimulationScenarioTable(simCompare.IsQuarterly, AsOfDate, curSimCompSimType.AsOfDateOffset, curSimCompSimType.SimulationType, simCompare.LeftInstitutionDatabaseName, scenarioTypeIn, simCompare.TaxEquivalentYields, simCompare.NiiOption);
                for (rowCounter = 0; rowCounter < tempDataTable.Rows.Count; rowCounter++)
                {
                    childRow = tempDataTable.Rows[rowCounter];
                    if (parentSimTbl.Rows.Count > rowCounter)
                    {
                        parentRow = parentSimTbl.Rows[rowCounter];
                        tempVal = GetDoubleFromDataTable(childRow["Amount"].ToString());
                        tempVal -= GetDoubleFromDataTable(parentRow["Amount"].ToString());
                        if (varianceTbl.Rows.Count > rowCounter)
                        {

                            varianceRow = varianceTbl.Rows[rowCounter];
                            varianceRow["Amount"] = GetDoubleFromDataTable(varianceRow["Amount"].ToString()) + tempVal;

                        }
                    }
                }
            }
            return varianceTbl;
        }

        public DataTable getUpdatedChartVarianceTable(List<ChartSimulationType> ChartSimTypes, DataTable parentSimTbl, DataTable varianceTbl, Chart Chart, string scenarioTypeIn, DateTime AsOfDate, bool taxEquiv, string niiOption)
        {
            ChartSimulationType curSimCompSimType;
            DataTable tempDataTable;
            DataRow[] parentSet;
            DataRow parentRow;
            DataRow varianceRow;
            DataRow childRow;
            double tempVal;
            int rowCounter = 0;
            int simCounter = 1;
            for (simCounter = 1; simCounter < ChartSimTypes.Count; simCounter++)
            {
                curSimCompSimType = ChartSimTypes[simCounter];
                tempDataTable = SimulationScenarioTable(Chart.IsQuarterly, AsOfDate, curSimCompSimType.AsOfDateOffset, curSimCompSimType.SimulationType, Chart.InstitutionDatabaseName, scenarioTypeIn, taxEquiv, niiOption);
                for (rowCounter = 0; rowCounter < tempDataTable.Rows.Count; rowCounter++)
                {
                    childRow = tempDataTable.Rows[rowCounter];
                    if (parentSimTbl.Rows.Count > rowCounter)
                    {
                        parentRow = parentSimTbl.Rows[rowCounter];
                        tempVal = GetDoubleFromDataTable(childRow["Amount"].ToString());
                        tempVal -= GetDoubleFromDataTable(parentRow["Amount"].ToString());
                        if (varianceTbl.Rows.Count > rowCounter)
                        {

                            varianceRow = varianceTbl.Rows[rowCounter];
                            varianceRow["Amount"] = GetDoubleFromDataTable(varianceRow["Amount"].ToString()) + tempVal;

                        }
                    }
                }
            }
            return varianceTbl;
        }

        public DataTable getChartData(DataTable origParentTable, DataTable varianceTable)
        {
            DataTable retTable = new DataTable();
            DataRow[] parentSet;
            DataRow[] varSet;
            DataRow parentRow;
            DataRow varRow;
            DataRow retRow;
            int rowCounter = 0;
            string qry = "";
            retTable = origParentTable.Copy();
            foreach (DataColumn dc in retTable.Columns)
            {
                dc.ReadOnly = false;
            }
            //foreach(DataRow dr in retTable.Rows)
            for (rowCounter = 0; rowCounter < retTable.Rows.Count; rowCounter++)
            {
                retRow = retTable.Rows[rowCounter];
                //qry = "month = '" + dr["month"] + "' AND yearGroup = '" + dr["yearGroup"] + "' AND name = '" + dr["name"] + "'";
                //parentSet = origParentTable.Select(qry);
                //varSet = varianceTable.Select(qry);
                //if(parentSet.Length > 0 && varSet.Length > 0)
                //{
                //    parentRow = parentSet[0];
                //    varRow = varSet[0];
                //    dr["Amount"] = GetDoubleFromDataTable(parentRow["Amount"].ToString()) + GetDoubleFromDataTable(varRow["Amount"].ToString());
                //}
                if (origParentTable.Rows.Count > rowCounter)
                {
                    parentRow = origParentTable.Rows[rowCounter];
                    if (varianceTable.Rows.Count > rowCounter)
                    {
                        varRow = varianceTable.Rows[rowCounter];
                        retRow["Amount"] = GetDoubleFromDataTable(parentRow["Amount"].ToString()) + GetDoubleFromDataTable(varRow["Amount"].ToString());
                    }
                }
            }
            return retTable;
        }

        public double GetDoubleFromDataTable(string dtVal)
        {
            double a = 0;
            bool success = false;
            success = Double.TryParse(dtVal, out a);
            if (success)
            {
                return a;

            }
            else
            {
                return 0;
            }
        }

        public DataTable GetFullDiffTable(bool isQuarterly)
        {
            DataTable ret = new DataTable();
            ret.Columns.Add("scenario", typeof(string));
            //we're just going to put all columns in the data set and hide things on the front end
            if (isQuarterly)
            {
                ret.Columns.Add("left_Y1_nii", typeof(double));
                ret.Columns.Add("left_Y2_nii", typeof(double));
                ret.Columns.Add("left_Y3_nii", typeof(double));
                ret.Columns.Add("left_Y4_nii", typeof(double));
                ret.Columns.Add("left_Y5_nii", typeof(double));
                ret.Columns.Add("diff_Y1_nii", typeof(double));
                ret.Columns.Add("diff_Y2_nii", typeof(double));
                ret.Columns.Add("diff_Y3_nii", typeof(double));
                ret.Columns.Add("diff_Y4_nii", typeof(double));
                ret.Columns.Add("diff_Y5_nii", typeof(double));
                ret.Columns.Add("right_Y1_nii", typeof(double));
                ret.Columns.Add("right_Y2_nii", typeof(double));
                ret.Columns.Add("right_Y3_nii", typeof(double));
                ret.Columns.Add("right_Y4_nii", typeof(double));
                ret.Columns.Add("right_Y5_nii", typeof(double));
            }
            else
            {
                ret.Columns.Add("left_Y1_nii", typeof(double));
                ret.Columns.Add("left_Y1_dollar_delta", typeof(double));
                ret.Columns.Add("left_Y1_perc_delta", typeof(double));
                ret.Columns.Add("left_Y2_nii", typeof(double));
                ret.Columns.Add("left_Y2_dollar_delta", typeof(double));
                ret.Columns.Add("left_Y2_perc_delta", typeof(double));
                ret.Columns.Add("left_Y2_cum_nii", typeof(double));
                ret.Columns.Add("left_Y2_cum_dollar_delta", typeof(double));
                ret.Columns.Add("left_Y2_cum_perc_delta", typeof(double));
                ret.Columns.Add("diff_Y1_nii");
                ret.Columns.Add("diff_Y2_nii");
                ret.Columns.Add("diff_Y2_cum_nii");
                ret.Columns.Add("right_Y1_nii", typeof(double));
                ret.Columns.Add("right_Y1_dollar_delta", typeof(double));
                ret.Columns.Add("right_Y1_perc_delta", typeof(double));
                ret.Columns.Add("right_Y2_nii", typeof(double));
                ret.Columns.Add("right_Y2_dollar_delta", typeof(double));
                ret.Columns.Add("right_Y2_perc_delta", typeof(double));
                ret.Columns.Add("right_Y2_cum_nii", typeof(double));
                ret.Columns.Add("right_Y2_cum_dollar_delta", typeof(double));
                ret.Columns.Add("right_Y2_cum_perc_delta", typeof(double));
            }
            return ret;
        }

        public object ChartViewModel(Chart chart, DataTable asOfDates, bool taxEquiv, int id, string niiOption)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            Report rep = gctx.Reports.Where(r => r.Id == id).FirstOrDefault();
            List<ChartSimulationType> chartSimTypes = chart.ChartSimulationTypes.OrderBy(s => s.Priority).ToList();
            //Stores In Statement for scenarioTypeid
            string scenarioTypeIn = "";
            //Sets Up Table That We Can Keep Cloning 
            foreach (var scen in chart.ChartScenarioTypes.OrderBy(s => s.Priority))
            {
                if (scenarioTypeIn == "")
                {
                    scenarioTypeIn = "(" + scen.ScenarioTypeId.ToString();
                }
                else
                {
                    scenarioTypeIn += "," + scen.ScenarioTypeId.ToString();
                }
            }
            scenarioTypeIn += ")";
            DataTable parentTable = SimulationScenarioTable(chart.IsQuarterly, rep.Package.AsOfDate, chartSimTypes[0].AsOfDateOffset, chartSimTypes[0].SimulationType, chart.InstitutionDatabaseName, scenarioTypeIn, taxEquiv, niiOption);
            DataTable varianceTable = parentTable.Copy();
            DataTable chartDataTable;
            for (int c = 0; c < parentTable.Columns.Count; c++)
            {
                parentTable.Columns[c].ReadOnly = false;
                varianceTable.Columns[c].ReadOnly = false;
            }
            foreach (DataRow dr in varianceTable.Rows)
            {
                dr["Amount"] = 0; ;
            }
            varianceTable = getUpdatedChartVarianceTable(chartSimTypes, parentTable, varianceTable, chart, scenarioTypeIn, rep.Package.AsOfDate, taxEquiv, niiOption);
            chartDataTable = getChartData(parentTable, varianceTable);
            DataTable tmpTable;
            //tmpTable = SimulationScenarioTable(chart.IsQuarterly, rep.Package.AsOfDate, chart.AsOfDateOffset, chart.SimulationType, chart.InstitutionDatabaseName, scenarioTypeIn, taxEquiv);
            Dictionary<string, GraphValues[]> chartData = BuildChartDictionary(chartDataTable, chart.IsQuarterly);
            DataTable chartNiiTbl = new DataTable();
            chartNiiTbl.Columns.Add("scenario", typeof(string));
            if (chart.IsQuarterly)
            {
                chartNiiTbl.Columns.Add("Y1_nii", typeof(double));
                chartNiiTbl.Columns.Add("Y2_nii", typeof(double));
                chartNiiTbl.Columns.Add("Y3_nii", typeof(double));
                chartNiiTbl.Columns.Add("Y4_nii", typeof(double));
                chartNiiTbl.Columns.Add("Y5_nii", typeof(double));
            }
            else
            {
                chartNiiTbl.Columns.Add("Y1_nii", typeof(double));
                chartNiiTbl.Columns.Add("Y2_nii", typeof(double));
            }
            string filter = "";
            int mQ = chart.IsQuarterly ? 4 : 12;
            foreach (KeyValuePair<string, GraphValues[]> kvp in chartData)
            {
                filter = "scenario = '" + kvp.Key + "'";
                DataRow[] drs = chartNiiTbl.Select(filter);

                if (drs.Length == 0)
                {
                    DataRow newRow = chartNiiTbl.NewRow();
                    newRow["scenario"] = kvp.Key;
                    chartNiiTbl.Rows.Add(newRow);
                    drs = chartNiiTbl.Select(filter);
                }

                if (drs.Length == 1)//there can only be one highlander
                {
                    for (var z = 0; z < chartData[kvp.Key].Length; z++)
                    {
                        int yearNum = (int)Math.Floor((double)((z + mQ) / mQ));
                        string colName = "Y" + yearNum + "_nii";
                        if (string.IsNullOrEmpty(drs[0][colName].ToString()))
                        {
                            drs[0][colName] = chartData[kvp.Key][z].value;
                        }
                        else
                        {
                            drs[0][colName] = (double)drs[0][colName] + chartData[kvp.Key][z].value;
                        }
                    }
                }
            }

            //check to see if we got data for every scenario selected
            List<string> foundScens = new List<string>();
            foreach (DataRow dr in parentTable.Rows)
            {
                if (!foundScens.Contains(dr["name"]))
                {
                    foundScens.Add(dr["name"].ToString());
                }
            }
            if (foundScens.Count < chart.ChartScenarioTypes.Count)
            {
                foreach (ChartScenarioType cSc in chart.ChartScenarioTypes)
                {
                    if (!foundScens.Contains(cSc.ScenarioType.Name))
                    {
                        warnings.Add(String.Format(Utilities.GetErrorTemplate(1), cSc.ScenarioType.Name, chartSimTypes[0].SimulationType.Name, chart.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                    }
                }
            }

            if (parentTable.Rows.Count == 0)
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), chartSimTypes[0].SimulationType.Name, chart.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }

            DataTable compareScens = new DataTable();
            DataSet comparePolicies = new DataSet();
            DataTable retCompareTableLeft = new DataTable();
            string compareScenName = "";

            if (chart.ComparativeScenario != null)
            {
                compareScens = SimulationScenarioTable(chart.IsQuarterly, rep.Package.AsOfDate, chartSimTypes[0].AsOfDateOffset, chartSimTypes[0].SimulationType, chart.InstitutionDatabaseName, "(" + chart.ComparativeScenario + ")", taxEquiv, niiOption);
                compareScenName = compareScens.Rows[0]["name"].ToString();
                comparePolicies = GetComparePolicies(DateTime.Parse(asOfDates.Rows[chart.AsOfDateOffset][0].ToString()), chart.PercChangeComparator, scenarioTypeIn, chart.InstitutionDatabaseName, chartSimTypes[0].SimulationTypeId.ToString());
                retCompareTableLeft = GetPercentageCompareTable(compareScens, comparePolicies, chartData, chart.IsQuarterly, chart.PercentChange, chart.PercChangeComparator, compareScenName);
            }
            else
            {
                errors.Add("No Compare Scenario Selected");
            }


            return new
            {
                chartData = chartData,
                niiTable = chartNiiTbl,
                compareTable = retCompareTableLeft,
                compareScenName = compareScenName,
                errors = errors,
                warnings = warnings
            };
        }


        public object OneChartViewModel(int oneChartId, DataTable asOfDates)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            var oneChart = gctx.OneCharts.Find(oneChartId);
            var rep = gctx.Reports.Where(s => s.Id == oneChartId).FirstOrDefault();
            InstitutionService instService = new InstitutionService(oneChart.Chart.InstitutionDatabaseName);
            string aodStr = instService.GetInformation(rep.Package.AsOfDate, oneChart.Chart.AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            DateTime aod = DateTime.Parse(aodStr);
            ModelSetup modelSetup = gctx.ModelSetups.Where(m => m.AsOfDate.Year == rep.Package.AsOfDate.Year && m.AsOfDate.Month == rep.Package.AsOfDate.Month && m.AsOfDate.Day == rep.Package.AsOfDate.Day).FirstOrDefault();
            List<ChartSimulationType> simTypes = oneChart.Chart.ChartSimulationTypes.OrderBy(s => s.Priority).ToList();
            GlobalController gc = new GlobalController();
            Dictionary<string, DataTable> policyOffsets = gc.PolicyOffsets(oneChart.Chart.InstitutionDatabaseName);
            string simTypeIdToUse = "";
            int simTypeIdInt = -1;
            SimulationType simulationTypeToUse;


            //check and default the overridable fields
            if (!oneChart.OverrideTaxEquiv)
            {
                oneChart.TaxEquivalentIncome = modelSetup.ReportTaxEquivalent;
            }

            if (!oneChart.Chart.OverrideParentSimulationType)
            {
                if (policyOffsets.ContainsKey(aodStr))
                {
                    simTypeIdToUse = policyOffsets[aodStr].Rows[0]["niiId"].ToString();
                    simTypeIdInt = Convert.ToInt32(simTypeIdToUse);
                    simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeIdInt).FirstOrDefault();
                    simTypes[0].SimulationTypeId = simulationTypeToUse.Id;
                    simTypes[0].SimulationType = simulationTypeToUse;
                }
            }

            if (!oneChart.Chart.NameOverride)
            {
                oneChart.Chart.Name = simTypes[0].SimulationType.Name + " Simulation as of " + aodStr;
            }
            gctx.SaveChanges();

            return ChartViewModel(oneChart.Chart, asOfDates, oneChart.TaxEquivalentIncome, oneChart.Id, oneChart.NiiOption);
        }



        public object TwoChartViewModel(int twoChartId, DataTable asOfDates)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            var twoChart = gctx.TwoCharts.Find(twoChartId);
            
            List<ChartSimulationType> leftSimTypes = twoChart.LeftChart.ChartSimulationTypes.OrderBy(s => s.Priority).ToList();
            List<ChartSimulationType> rightSimTypes = twoChart.RightChart.ChartSimulationTypes.OrderBy(s => s.Priority).ToList();
            var rep = gctx.Reports.Where(s => s.Id == twoChartId).FirstOrDefault();

            InstitutionService instService = new InstitutionService(twoChart.LeftChart.InstitutionDatabaseName);
            string aodStr = instService.GetInformation(rep.Package.AsOfDate, leftSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            DateTime aod = DateTime.Parse(aodStr);
            ModelSetup modelSetup = gctx.ModelSetups.Where(m => m.AsOfDate.Year == rep.Package.AsOfDate.Year && m.AsOfDate.Month == rep.Package.AsOfDate.Month && m.AsOfDate.Day == rep.Package.AsOfDate.Day).FirstOrDefault();

            GlobalController gc = new GlobalController();
            Dictionary<string, DataTable> policyOffsets = gc.PolicyOffsets(twoChart.LeftChart.InstitutionDatabaseName);
            string simTypeIdToUse = "";
            int simTypeIdInt = -1;
            SimulationType simulationTypeToUse;

            //check and default the overridable fields
            if (!twoChart.OverrideTaxEquiv)
            {
                twoChart.TaxEquivalentIncome = modelSetup.ReportTaxEquivalent;
            }

            if (!twoChart.LeftChart.OverrideParentSimulationType)
            {
                if (policyOffsets.ContainsKey(aodStr))
                {
                    simTypeIdToUse = policyOffsets[aodStr].Rows[0]["niiId"].ToString();
                    simTypeIdInt = Convert.ToInt32(simTypeIdToUse);
                    simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeIdInt).FirstOrDefault();
                    leftSimTypes[0].SimulationTypeId = simulationTypeToUse.Id;
                    leftSimTypes[0].SimulationType = simulationTypeToUse;
                }
            }

            if (!twoChart.LeftChart.NameOverride)
            {
                twoChart.LeftChart.Name = leftSimTypes[0].SimulationType.Name + " Simulation as of " + aodStr;
            }

            //now do it for Right Chart
            //check and default the overridable fields
            instService = new InstitutionService(twoChart.RightChart.InstitutionDatabaseName);
            aodStr = instService.GetInformation(rep.Package.AsOfDate, rightSimTypes[0].AsOfDateOffset).AsOfDate.ToString("MM/dd/yyyy");
            aod = DateTime.Parse(aodStr);
            policyOffsets = gc.PolicyOffsets(twoChart.RightChart.InstitutionDatabaseName);

            if (!twoChart.RightChart.OverrideParentSimulationType)
            {
                if (policyOffsets.ContainsKey(aodStr))
                {
                    simTypeIdToUse = policyOffsets[aodStr].Rows[0]["niiId"].ToString();
                    simTypeIdInt = Convert.ToInt32(simTypeIdToUse);
                    simulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeIdInt).FirstOrDefault();
                    rightSimTypes[0].SimulationTypeId = simulationTypeToUse.Id;
                    rightSimTypes[0].SimulationType = simulationTypeToUse;
                }
            }

            if (!twoChart.RightChart.NameOverride)
            {
                twoChart.RightChart.Name = rightSimTypes[0].SimulationType.Name + " Simulation as of " + aodStr;
            }


            //Everytime we view just set the indiviual charts graph types to be the global one
            twoChart.LeftChart.IsQuarterly = twoChart.IsQuarterly;
            twoChart.RightChart.IsQuarterly = twoChart.IsQuarterly;

            //Save it off so that we are good
            gctx.SaveChanges();

            var leftChartViewModel = ChartViewModel(twoChart.LeftChart, asOfDates, twoChart.TaxEquivalentIncome, twoChart.Id, twoChart.NiiOption);
            var rightChartViewModel = ChartViewModel(twoChart.RightChart, asOfDates, twoChart.TaxEquivalentIncome, twoChart.Id, twoChart.NiiOption);
            return new
            {
                leftChartViewModel = leftChartViewModel,
                rightChartViewModel = rightChartViewModel
            };
        }

        public DataTable GetPercentageCompareTable(DataTable compareBaseScenario, DataSet comparePolicies, Dictionary<string, GraphValues[]> chartData, bool isQuarterly, int percentChange, string percChangeComparator, string compScenName)
        {
            DataTable ret = BuildCompareTable(percentChange == 2, isQuarterly);
            double[] baseScenCompareNumbers;
            double[] compareNumbers;
            int moduloTracker = isQuarterly ? 4 : 12;
            int r = 0;
            double curScenVal = 0, compScenVal = 0;
            if (isQuarterly)
            {
                baseScenCompareNumbers = new double[5];
                compareNumbers = new double[5];
            }
            else
            {
                baseScenCompareNumbers = new double[2];
                compareNumbers = new double[5];
            }
            for (int i = 0; i < baseScenCompareNumbers.Length; i++)
            {
                baseScenCompareNumbers[i] = 0;
            }

            for (r = 0; r < compareBaseScenario.Rows.Count; r++)
            {
                baseScenCompareNumbers[(int)Math.Floor((double)(r / moduloTracker))] += (double)compareBaseScenario.Rows[r]["Amount"];
            }
            r = 0;
            foreach (string key in chartData.Keys)
            {
                for (r = 0; r < compareNumbers.Length; r++)
                {
                    compareNumbers[r] = 0;
                }
                for (r = 0; r < chartData[key].Length; r++)
                {
                    compareNumbers[(int)Math.Floor((double)(r / moduloTracker))] += chartData[key][r].value;
                }
                DataRow newDR = ret.NewRow();

                switch (percChangeComparator)
                {
                    case "year2Year1":
                        curScenVal = compareNumbers[1];
                        compScenVal = baseScenCompareNumbers[0];
                        break;
                    case "year2Year2":
                        curScenVal = compareNumbers[1];
                        compScenVal = baseScenCompareNumbers[1];
                        break;
                    case "24Month":
                        curScenVal = compareNumbers[0] + compareNumbers[1];
                        compScenVal = baseScenCompareNumbers[0] + baseScenCompareNumbers[1];
                        break;
                }

                if (percentChange == 2) //we are showing policies
                {
                    newDR[0] = key;
                    if (key.ToUpper() == "BASE")
                    {
                        newDR[1] = DBNull.Value;
                    }
                    else
                    {
                        if (GetFormattedPolicyNumber(key, comparePolicies.Tables[0]) != -999)
                        {
                            newDR[1] = GetFormattedPolicyNumber(key, comparePolicies.Tables[0]);
                        }
                        else
                        {
                            newDR[1] = DBNull.Value;
                        }

                    }

                    //Year1 Compare
                    if (key.ToUpper() == compScenName.ToUpper())
                    {
                        newDR[2] = DBNull.Value;
                    }
                    else
                    {
                        newDR[2] = Math.Round((compareNumbers[0] - baseScenCompareNumbers[0]) / baseScenCompareNumbers[0], 4, MidpointRounding.AwayFromZero);
                    }

                    //Policy 2
                    if (GetFormattedPolicyNumber(key, comparePolicies.Tables[1]) != -999)
                    {
                        newDR[3] = GetFormattedPolicyNumber(key, comparePolicies.Tables[1]);
                    }
                    else
                    {
                        newDR[3] = DBNull.Value;
                    }


                    //2 Year Compare
                    if ((percChangeComparator == "24Month" || percChangeComparator == "year2Year2") && key.ToUpper() == compScenName.ToUpper())
                    {
                        newDR[4] = DBNull.Value;
                    }
                    else
                    {
                        newDR[4] = (curScenVal - compScenVal) / compScenVal;
                    }

                    //newDR[4] = String.Format("{0:P2}", (compareNumbers[1] - baseScenCompareNumbers[1]) / baseScenCompareNumbers[1]);

                }
                else
                {
                    newDR[0] = key;
                    //Year1 Compare
                    if (key.ToUpper() == compScenName.ToUpper())
                    {
                        newDR[1] = DBNull.Value;
                    }
                    else
                    {
                        newDR[1] = Math.Round((compareNumbers[0] - baseScenCompareNumbers[0]) / baseScenCompareNumbers[0], 4, MidpointRounding.AwayFromZero);
                    }


                    //Year2 Compare
                    if ((percChangeComparator == "24Month" || percChangeComparator == "year2Year2") && key.ToUpper() == compScenName.ToUpper())
                    {
                        newDR[2] = DBNull.Value;
                    }
                    else
                    {
                        newDR[2] = (curScenVal - compScenVal) / compScenVal;
                    }

                    //newDR[2] = String.Format("{0:P2}", (compareNumbers[1] - baseScenCompareNumbers[1]) / baseScenCompareNumbers[1]);
                    if (isQuarterly)
                    {
                        //newDR[3] = String.Format("{0:P2}", (compareNumbers[2] - baseScenCompareNumbers[2]) / baseScenCompareNumbers[2]);
                        //newDR[4] = String.Format("{0:P2}", (compareNumbers[3] - baseScenCompareNumbers[3]) / baseScenCompareNumbers[3]);
                        //newDR[5] = String.Format("{0:P2}", (compareNumbers[4] - baseScenCompareNumbers[4]) / baseScenCompareNumbers[4]);
                    }
                }
                if (percChangeComparator == "year2Year1" || key != compScenName)
                {
                    //fullPolicyCompareTable.Rows.Add(newRow);
                    ret.Rows.Add(newDR);
                }

            }

            return ret;

        }

        double GetFormattedPolicyNumber(string scenarioName, DataTable policyLimits)
        {
            double ret = -999;
            foreach (DataRow dr in policyLimits.Rows)
            {
                if (dr["Name"].ToString().ToUpper() == scenarioName.ToUpper())
                {
                    ret = Convert.ToDouble(dr["PolicyLimit"].ToString());
                }
            }
            return ret;
        }

        public DataTable BuildCompareTable(bool includePolicies, bool isQuarterly)
        {
            DataTable ret = new DataTable();
            ret.Columns.Add("Scenario", typeof(string));
            if (includePolicies)
                ret.Columns.Add("Policy1", typeof(double));

            ret.Columns.Add("Year1", typeof(double));

            if (includePolicies)
                ret.Columns.Add("Policy2", typeof(double));

            ret.Columns.Add("Year2", typeof(double));
            return ret;
        }

        public Dictionary<string, GraphValues[]> BuildChartDictionary(DataTable tmpTable, bool isQuarterly)
        {

            int scenCount = 0;
            string prevScen = "";
            Dictionary<string, GraphValues[]> chartData = new Dictionary<string, GraphValues[]>();
            foreach (DataRow dr in tmpTable.Rows)
            {
                string scenario = dr["name"].ToString();

                //Set Prev Scenario
                if (prevScen == "")
                {
                    prevScen = scenario;
                }

                //Reset Scen Count if it was not last scenario
                if (scenario != prevScen)
                {
                    scenCount = 0;
                }

                if (!chartData.ContainsKey(scenario))
                {
                    if (isQuarterly)
                    {
                        chartData.Add(scenario, new GraphValues[20]);
                    }
                    else
                    {
                        chartData.Add(scenario, new GraphValues[24]);
                    }

                    for (var z = 0; z < chartData[scenario].Length; z++)
                    {
                        chartData[scenario][z] = new GraphValues();
                    }

                }

                chartData[scenario][scenCount].value = Double.Parse(dr["Amount"].ToString());
                chartData[scenario][scenCount].month = dr["month"].ToString();

                scenCount += 1;
                prevScen = scenario;
            }


            return chartData;
        }

        public DataSet GetComparePolicies(DateTime asOfDate, string policyParentId, string scenarioInClause, string instDatabase, string simulationId)
        {

            InstitutionService instService = new InstitutionService(instDatabase);
            DataSet ret = new DataSet();
            DataTable yr1 = new DataTable();
            DataTable yr2 = new DataTable();
            yr1.TableName = "Year1";
            yr2.TableName = "Year2";
            string qry = "";
            qry += "SELECT \n";
            qry += "niiPolicy.PolicyLimit, \n";
            //qry += "CASE WHEN niiPolicy.PolicyLimit = -999 THEN NULL \n";
            //qry += "ELSE niiPolicy.PolicyLimit END as PolicyLimit, \n";
            qry += "niiPolicy.ScenarioTypeId, \n";
            qry += "scen.[Name], \n";
            qry += "policyParent.Name as policyParentName \n";
            qry += "FROM ATLAS_Policy policy \n";
            qry += "INNER JOIN ATLAS_NIIPolicyParent policyParent \n";
            qry += "ON policy.Id = policyParent.PolicyId \n";
            qry += "INNER JOIN ATLAS_NIIPolicy niiPolicy \n";
            qry += "ON policyParent.Id = niiPolicy.NIIPolicyParentId \n";
            qry += "INNER JOIN ATLAS_ScenarioType scen ON niiPolicy.ScenarioTypeId = scen.Id \n";
            qry += "WHERE policy.asofdate = @AOD \n";
            qry += "and policyParent.shortName = @POL_PARENT_ID \n";
            qry += "and niiPolicy.ScenarioTypeId in " + scenarioInClause + " \n";
            qry += "and niiPolicy.PolicyLimit <> -999 \n";
            //qry += "and policy.NIISimulationTypeId = " + simulationId;



            string[] paramArr = new string[] { "@AOD", "@POL_PARENT_ID" };
            string[] paramVals = new string[] { asOfDate.ToShortDateString(), "year1Year1" };

            yr1 = Utilities.ExecuteParameterizedSql(instService.ConnectionString(), qry, paramArr, paramVals);

            //paramVals[0] = asOfDate.AddYears(1).ToShortDateString();
            paramVals[1] = policyParentId.ToString();

            yr2 = Utilities.ExecuteParameterizedSql(instService.ConnectionString(), qry, paramArr, paramVals);

            ret.Tables.Add(yr1);
            ret.Tables.Add(yr2);
            return ret;
        }

        public DataTable SimulationScenarioTable(bool isQuarterly, DateTime asOfDate, int asOfDateOffSet, SimulationType simulationType, string instDatabase, string scenarioTypeIn, bool taxEquiv, string niiOption)
        {
            StringBuilder sqlQuery = new StringBuilder();
            InstitutionService instService = new InstitutionService(instDatabase);
            Simulation bankSim = instService.GetSimulation(asOfDate, asOfDateOffSet, simulationType);
            DataTable retTable = new DataTable();

            StringBuilder scenOrderCase = new StringBuilder();
            scenOrderCase.AppendLine("CASE ");
            string tempScenIn = scenarioTypeIn.Replace("(", "");
            tempScenIn = tempScenIn.Replace(")", "");
            string[] scenArr = tempScenIn.Split(',');

            for (int i = 0; i < scenArr.Length; i++)
            {
                scenOrderCase.AppendLine("WHEN scenNames.Id = '" + scenArr[i] + "' THEN " + i);
            }
            scenOrderCase.AppendLine("END");

            if (bankSim == null || (niiOption != "nii" && niiOption != "ni"))
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), simulationType.Name, asOfDateOffSet, asOfDate.ToShortDateString()));
            }
            else if(niiOption == "nii")
            {
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	MAX(DATEADD(d, -1, DATEADD(m, baseSim.nMonths, baseSim.month))) as month ");
                sqlQuery.AppendLine("   ,baseSim.yearGroup as yearGroup ");
                sqlQuery.AppendLine(" 	,scenNames.name ");
                sqlQuery.AppendLine(" 	,SUM(CASE WHEN alb.isAsset = 1 THEN baseSim.IeTotAmount ELSE -baseSim.IETotAmount END) as Amount ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" (SELECT simulationid ");
                sqlQuery.AppendLine(" 		,month ");
                sqlQuery.AppendLine(" 		,nMonths ");
                sqlQuery.AppendLine(" 		,scenario ");
                sqlQuery.AppendLine(" 		,code ");
                if (taxEquiv)
                {
                    sqlQuery.AppendLine(" 		,AveTEAmt as IETotAmount ");
                }
                else
                {
                    sqlQuery.AppendLine(" 		,IETotAmount ");
                }

                if (isQuarterly)
                {
                    sqlQuery.AppendLine(Utilities.GroupQtrsCaseStatement(bankSim.Information.AsOfDate));
                }

                sqlQuery.AppendLine("		,CASE ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 12, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 1' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 24, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 2' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 36, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 3' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 48, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 4' ");
                sqlQuery.AppendLine("			ELSE ");
                sqlQuery.AppendLine("				'Year 5' ");
                sqlQuery.AppendLine("		END as yearGroup ");

                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("  FROM Basis_ALP13 WHERE simulationId = " + bankSim.Id + "");
                if (isQuarterly)
                {
                    sqlQuery.AppendLine(" AND month <= DATEADD(m, 60, '" + bankSim.Information.AsOfDate.ToShortDateString() + "')");
                }
                else
                {
                    sqlQuery.AppendLine(" AND month <= DATEADD(m, 24, '" + bankSim.Information.AsOfDate.ToShortDateString() + "')");
                }

                sqlQuery.AppendLine(" ) as baseSim ");
                sqlQuery.AppendLine(" INNER JOIN Basis_ALB as alb ON  ");
                sqlQuery.AppendLine(" alb.simulationId = baseSim.simulationId and alb.code = baseSim.code ");
                sqlQuery.AppendLine(" INNER JOIN Basis_ALS as als ON ");
                sqlQuery.AppendLine(" als.simulationId = baseSim.simulationId and als.code = baseSim.code and als.scenario = baseSim.scenario ");
                sqlQuery.AppendLine(" INNER JOIN Basis_Scenario  as scen ON ");
                sqlQuery.AppendLine(" scen.number = als.scenario AND scen.SimulationId = als.SimulationId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as scenNames ON ");
                sqlQuery.AppendLine(" scennames.Id = scen.ScenarioTypeId  ");
                sqlQuery.AppendLine(" WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5) AND scen.scenarioTypeId IN " + scenarioTypeIn + " ");
                if (isQuarterly)
                {
                    sqlQuery.AppendLine(" GROUP BY baseSim.qtr,  baseSim.yearGroup, scenNames.name, ");
                }
                else
                {
                    sqlQuery.AppendLine(" GROUP BY baseSim.month, baseSim.yearGroup, scenNames.name, ");
                }
                sqlQuery.AppendLine(scenOrderCase.ToString());
                sqlQuery.AppendLine(" ORDER BY ");
                sqlQuery.AppendLine(scenOrderCase.ToString() + ",");
                sqlQuery.AppendLine("scenNames.name, MAX(baseSim.month)");
                retTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
            }
            else if(niiOption == "ni")
            {
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	MAX(DATEADD(d, -1, DATEADD(m, baseSim.nMonths, baseSim.month))) as month ");
                sqlQuery.AppendLine("   ,baseSim.yearGroup as yearGroup ");
                sqlQuery.AppendLine(" 	,scenNames.name ");
                sqlQuery.AppendLine(" 	,SUM(Amount) as Amount ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" (SELECT simulationid ");
                sqlQuery.AppendLine(" 		,month ");
                sqlQuery.AppendLine(" 		,nMonths ");
                sqlQuery.AppendLine(" 		,scenario ");
                sqlQuery.AppendLine(" 		,code ");
                sqlQuery.AppendLine(" 		,Amount - RelAmount as Amount ");
                //if (taxEquiv)
                //{
                //    sqlQuery.AppendLine(" 		,AveTEAmt as IETotAmount ");
                //}
                //else
                //{
                //    sqlQuery.AppendLine(" 		,IETotAmount ");
                //}

                if (isQuarterly)
                {
                    sqlQuery.AppendLine(Utilities.GroupQtrsCaseStatement(bankSim.Information.AsOfDate));
                }

                sqlQuery.AppendLine("		,CASE ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 12, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 1' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 24, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 2' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 36, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 3' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 48, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 4' ");
                sqlQuery.AppendLine("			ELSE ");
                sqlQuery.AppendLine("				'Year 5' ");
                sqlQuery.AppendLine("		END as yearGroup ");

                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("  FROM Basis_OTHERP WHERE simulationId = " + bankSim.Id + "");
                if (isQuarterly)
                {
                    sqlQuery.AppendLine(" AND month <= DATEADD(m, 60, '" + bankSim.Information.AsOfDate.ToShortDateString() + "')");
                }
                else
                {
                    sqlQuery.AppendLine(" AND month <= DATEADD(m, 24, '" + bankSim.Information.AsOfDate.ToShortDateString() + "')");
                }

                sqlQuery.AppendLine(" ) as baseSim ");
                sqlQuery.AppendLine(" INNER JOIN Basis_OTHERB as alb ON  ");
                sqlQuery.AppendLine(" alb.simulationId = baseSim.simulationId and alb.code = baseSim.code ");
                //sqlQuery.AppendLine(" INNER JOIN Basis_ALS as als ON ");
                //sqlQuery.AppendLine(" als.simulationId = baseSim.simulationId and als.code = baseSim.code and als.scenario = baseSim.scenario ");
                sqlQuery.AppendLine(" INNER JOIN Basis_Scenario  as scen ON ");
                sqlQuery.AppendLine(" scen.number = baseSim.scenario AND scen.SimulationId = alb.SimulationId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as scenNames ON ");
                sqlQuery.AppendLine(" scennames.Id = scen.ScenarioTypeId  ");
                sqlQuery.AppendLine(" WHERE alb.Sequence = '113' AND scen.scenarioTypeId IN " + scenarioTypeIn + " ");
                if (isQuarterly)
                {
                    sqlQuery.AppendLine(" GROUP BY baseSim.qtr,  baseSim.yearGroup, scenNames.name, ");
                }
                else
                {
                    sqlQuery.AppendLine(" GROUP BY baseSim.month, baseSim.yearGroup, scenNames.name, ");
                }
                sqlQuery.AppendLine(scenOrderCase.ToString());
                sqlQuery.AppendLine(" ORDER BY ");
                sqlQuery.AppendLine(scenOrderCase.ToString() + ",");
                sqlQuery.AppendLine("scenNames.name, MAX(baseSim.month)");
                retTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
            }

            return retTable;
        }


        public class GraphValues
        {
            public string month { get; set; }
            public double value { get; set; }

        }
    }


}