﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class CoreFundingUtilizationService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public List<string> orderedScenNames = new List<string>();

        public object CoreFundingUtilizationViewModel(int reportId)
        {
            InstitutionContext _gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            //look up corefunding settings
            var cfu = _gctx.CoreFundingUtilizations.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = _gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            var instService = new InstitutionService(cfu.InstitutionDatabaseName);
            GlobalController gc = new GlobalController();
            Dictionary<string, DataTable> policyOffsets = gc.PolicyOffsets(cfu.InstitutionDatabaseName);

            

            DataTable topTable = new DataTable();
            DataTable historicalTable = new DataTable();
            DataTable orderTable = new DataTable();
            int policyId;
            int cfParentId;
            DataTable polTbl;

            //get package as of date for report
            var mainInfo = instService.GetInformation(rep.Package.AsOfDate, cfu.AsOfDateOffset);
            string aod = mainInfo.AsOfDate.ToString("MM/dd/yyyy");
            int simulationTypeIdToUse = Convert.ToInt32(policyOffsets[aod].Rows[0]["niiId"].ToString());
            if (!cfu.OverrideSimulation)
            {
                //simulationTypeIdToUse = cfu.SimulationTypeId;
                cfu.SimulationTypeId = simulationTypeIdToUse;
            }
            foreach(CoreFundingUtilizationDateOffSets o in cfu.CoreFundingUtilizationDateOffSets)
            {
                var info = instService.GetInformation(rep.Package.AsOfDate, o.OffSet);
                string a = info.AsOfDate.ToString("MM/dd/yyyy");
                if (!o.OverrideSimulationTypeId)
                {
                    if (policyOffsets.ContainsKey(a))
                    {
                        o.SimulationTypeId = Convert.ToInt32(policyOffsets[a].Rows[0]["niiId"].ToString());
                        o.SimulationType = _gctx.SimulationTypes.Where(st => st.Id == o.SimulationTypeId).FirstOrDefault();
                    }
                }
            }
            _gctx.SaveChanges();
            SimulationType simulationTypeToUse = _gctx.SimulationTypes.Where(s => s.Id == cfu.SimulationTypeId).FirstOrDefault();


            List<int> scenTypeIds = new List<int>();
            List<int> dateOffsets = new List<int>();
            DateTime[] orderedHistoricalDates = new DateTime[cfu.CoreFundingUtilizationDateOffSets.Count];
            int histDN = 0;
            
            Dictionary<string, Dictionary<string, DataTable>> scenarioPolicies = new Dictionary<string, Dictionary<string, DataTable>>();
            Dictionary<DateTime, bool> historicalGraphing = new Dictionary<DateTime,bool>();


            foreach (CoreFundingUtilizationScenarioType scenType in cfu.CoreFundingUtilizationScenarioTypes.OrderBy(a => a.Priority))
            {
                scenTypeIds.Add(scenType.ScenarioTypeId);
            }
            foreach (CoreFundingUtilizationDateOffSets dOff in cfu.CoreFundingUtilizationDateOffSets.OrderBy(b => b.Priority))
            {
                dateOffsets.Add(dOff.OffSet);
                Information info = instService.GetInformation(rep.Package.AsOfDate, dOff.OffSet);
                orderedHistoricalDates[histDN] = info.AsOfDate;
                if (!historicalGraphing.ContainsKey(info.AsOfDate))
                {
                    historicalGraphing.Add(info.AsOfDate, dOff.Graph);
                }
                histDN++;
            }

            
            foreach (int scenTypeId in scenTypeIds)
            {
                
                var obj = instService.GetSimulationScenario(rep.Package.AsOfDate, cfu.AsOfDateOffset, simulationTypeIdToUse, scenTypeId);
                string scenName = cfu.CoreFundingUtilizationScenarioTypes.Where(a => a.ScenarioTypeId == scenTypeId).Select(a => a.ScenarioType).FirstOrDefault().Name;
                bool dateOffsetFlag = true;
                for(int d = 0; d < dateOffsets.Count; d++)
                {
                    int dOff = dateOffsets[d];
                    string asOfDate = instService.GetInformation(rep.Package.AsOfDate, dOff).AsOfDate.ToString();
                    polTbl = new DataTable();

                    polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT Id FROM ATLAS_Policy WHERE AsOfDate = '" + asOfDate + "'");
                    if (polTbl.Rows.Count > 0)
                    {
                        if (int.TryParse(polTbl.Rows[0][0].ToString(), out policyId))
                        {
                            polTbl.Clear();
                            polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "Select Id from ATLAS_CoreFundParent where PolicyId = " + policyId);
                            if (polTbl.Rows.Count > 0)
                            {
                                if (int.TryParse(polTbl.Rows[0][0].ToString(), out cfParentId))
                                {
                                    polTbl.Clear();
                                    polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "Select PolicyLimit, RiskAssessment from ATLAS_CoreFundPolicy where CoreFundParentId = " + cfParentId + " and ScenarioTypeId = " + scenTypeId);
                                    if (polTbl.Rows.Count > 0)
                                    {
                                        if (!scenarioPolicies.ContainsKey(scenName))
                                        {
                                            scenarioPolicies.Add(scenName, new Dictionary<string, DataTable>());
                                        }
                                        if (!scenarioPolicies[scenName].ContainsKey(asOfDate))
                                        {
                                            scenarioPolicies[scenName].Add(asOfDate, polTbl);
                                        }

                                    }
                                }
                            }
                        }
                    }
                    if(d == 0 && dateOffsetFlag)
                    {
                        d--;
                        dateOffsetFlag = false;
                    }
                }
               if(!scenarioPolicies[scenName].ContainsKey(mainInfo.AsOfDate.ToString())) 
               {
                   polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT Id FROM ATLAS_Policy WHERE AsOfDate = '" + mainInfo.AsOfDate.ToString() + "'");
                   if (polTbl.Rows.Count > 0)
                   {
                       if (int.TryParse(polTbl.Rows[0][0].ToString(), out policyId))
                       {
                           polTbl.Clear();
                           polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "Select Id from ATLAS_CoreFundParent where PolicyId = " + policyId);
                           if (polTbl.Rows.Count > 0)
                           {
                               if (int.TryParse(polTbl.Rows[0][0].ToString(), out cfParentId))
                               {
                                   polTbl.Clear();
                                   polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "Select PolicyLimit, RiskAssessment from ATLAS_CoreFundPolicy where CoreFundParentId = " + cfParentId + " and ScenarioTypeId = " + scenTypeId);
                                   if (polTbl.Rows.Count > 0)
                                   {
                                       if (!scenarioPolicies.ContainsKey(scenName))
                                       {
                                           scenarioPolicies.Add(scenName, new Dictionary<string, DataTable>());
                                       }
                                       if (!scenarioPolicies[scenName].ContainsKey(mainInfo.AsOfDate.ToString()))
                                       {
                                           scenarioPolicies[scenName].Add(mainInfo.AsOfDate.ToString(), polTbl);
                                       }

                                   }
                               }
                           }
                       }
                   }
               }

            }
            

            if (cfu.AssetDetails.IndexOf("dg") == -1)
            {
                topTable = CoreFundingTable(instService, cfu, rep.Package.AsOfDate, false, true);

                historicalTable = CoreFundingTable(instService, cfu, rep.Package.AsOfDate, true, false);
            }
            else
            {
                topTable = CoreFundingTableDefinedGroupings(instService, cfu, rep.Package.AsOfDate, false, cfu.AssetDetails, true);
                historicalTable = CoreFundingTableDefinedGroupings(instService, cfu, rep.Package.AsOfDate, true, cfu.AssetDetails, false);
                orderTable = OrderTable(instService, cfu.AssetDetails);
            }



            if (historicalTable.Rows.Count <= 0)
            {
                warnings.Add("No Historical Data Found.");
            }

            return new
            {
                mainAsOfDate = mainInfo.AsOfDate,
                orderedDates = orderedHistoricalDates,
                packageAsOfDate = mainInfo.AsOfDate,
                instName = cfu.InstitutionDatabaseName,
                topTable = topTable,
                historicalTable = historicalTable,
                orderTable = orderTable,
                numScenarios = scenTypeIds.Count + 1,
                scenarios = orderedScenNames,
                assetDetails = cfu.AssetDetails,
                showPolicies = cfu.ShowPolicies,
                policyLimits = scenarioPolicies,
                historicalGraphing = historicalGraphing,
                errors = errors,
                warnings = warnings
            };
        }

        //private DataTable GetPolicyTable(InstitutionService instService, string asOfDate)
        //{
        //    //double policyLimit;
        //    int policyId;
        //    int cfParentId;
        //    DataTable polTbl = new DataTable();

        //    polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT Id FROM ATLAS_Policy WHERE AsOfDate = '" + asOfDate + "'");
        //    if (polTbl.Rows.Count > 0)
        //    {
        //        if (int.TryParse(polTbl.Rows[0][0].ToString(), out policyId))
        //        {
        //            polTbl.Clear();
        //            polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "Select Id from ATLAS_CoreFundParent where PolicyId = " + policyId);
        //            if (polTbl.Rows.Count > 0)
        //            {
        //                if (int.TryParse(polTbl.Rows[0][0].ToString(), out cfParentId))
        //                {
        //                    polTbl.Clear();
        //                    polTbl = Utilities.ExecuteSql(instService.ConnectionString(), "Select PolicyLimit, RiskAssessment from ATLAS_CoreFundPolicy where CoreFundParentId = " + cfParentId + " and ScenarioTypeId = " + scenTypeId);
        //                    if (polTbl.Rows.Count > 0)
        //                    {
        //                        if (!scenarioPolicies.ContainsKey(scenName))
        //                        {
        //                            scenarioPolicies.Add(scenName, new Dictionary<string, DataTable>());
        //                        }
        //                        if (!scenarioPolicies[scenName].ContainsKey(asOfDate))
        //                        {
        //                            scenarioPolicies[scenName].Add(asOfDate, polTbl);
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return new DataTable();
        //}

        private DataTable OrderTable(InstitutionService instService, string grouping)
        {
            string qry = "SELECT name, priority from [DDW_Atlas_Templates].[dbo].[ATLAS_DefinedGroupingGroups] WHERE IsAsset = 1 AND DefinedGroupingId = " + grouping.Replace("dg_", "");
            return Utilities.ExecuteSql(instService.ConnectionString(), qry);
        }


        private DataTable CoreFundingTable(InstitutionService instService, CoreFundingUtilization cfu, DateTime packageAsOfDate, bool isHistoricTable, bool giveError)
        {
            List<object> objs = new List<object>();
            if(!isHistoricTable)
            {
                foreach (CoreFundingUtilizationScenarioType scenType in cfu.CoreFundingUtilizationScenarioTypes.OrderBy(s => s.Priority))
                {
                    var o = instService.GetSimulationScenario(packageAsOfDate, 0, cfu.SimulationTypeId, scenType.ScenarioType.Id);
                    if (o.GetType().GetProperty("simulation").GetValue(o).ToString().Length > 0 && o.GetType().GetProperty("scenario").GetValue(o).ToString().Length > 0)
                    {
                        objs.Add(instService.GetSimulationScenario(packageAsOfDate, cfu.AsOfDateOffset, cfu.SimulationTypeId, scenType.ScenarioType.Id));
                        if (!orderedScenNames.Contains(scenType.ScenarioType.Name))
                        {
                            orderedScenNames.Add(scenType.ScenarioType.Name);
                        }
                    }
                    else
                    {
                        if (giveError)
                        {
                            //errors.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, cfu.SimulationTypeId, cfu.AsOfDateOffset, packageAsOfDate.ToShortDateString()));
                        }
                        else
                        {
                            //warnings.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, cfu.SimulationTypeId, cfu.AsOfDateOffset, packageAsOfDate.ToShortDateString()));
                        }
                    }

                }
            }
            else
            {
                foreach (CoreFundingUtilizationDateOffSets offset in cfu.CoreFundingUtilizationDateOffSets)
                {

                    foreach (CoreFundingUtilizationScenarioType scenType in cfu.CoreFundingUtilizationScenarioTypes.OrderBy(s => s.Priority))
                    {
                        var o = instService.GetSimulationScenario(packageAsOfDate, offset.OffSet, offset.SimulationType.Id, scenType.ScenarioType.Id);
                        if (o.GetType().GetProperty("simulation").GetValue(o).ToString().Length > 0 && o.GetType().GetProperty("scenario").GetValue(o).ToString().Length > 0)
                        {
                            objs.Add(instService.GetSimulationScenario(packageAsOfDate, offset.OffSet, offset.SimulationType.Id, scenType.ScenarioType.Id));
                            if (!orderedScenNames.Contains(scenType.ScenarioType.Name))
                            {
                                orderedScenNames.Add(scenType.ScenarioType.Name);
                            }
                        }
                        else
                        {
                            if (giveError)
                            {
                                //errors.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, offset.SimulationType.Name, offset.OffSet, packageAsOfDate.ToShortDateString()));
                            }
                            else
                            {
                                //warnings.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, offset.SimulationType.Name, offset.OffSet, packageAsOfDate.ToShortDateString()));
                            }
                        }

                    }
                }
            }
            
            int objsCount = objs.Count;
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("G.asOfDate");
            sqlQuery.AppendLine(",G.scenarioName");
            sqlQuery.AppendLine(",G.balance");
            sqlQuery.AppendLine(",G.isAsset");
            sqlQuery.AppendLine(",isnull(accTypes.Name, cflookups.name) as name");
            sqlQuery.AppendLine(", case");
            for (int i = 0; i < objsCount; i++)
            {
                string closer = i < objsCount - 1 ? " OR" : "";
                object obj = objs[i];
                if (obj.GetType().GetProperty("simulation").GetValue(obj).ToString().Length > 0 && obj.GetType().GetProperty("scenario").GetValue(obj).ToString().Length > 0)
                    sqlQuery.AppendLine(" when G.Scenario = '" + obj.GetType().GetProperty("scenario").GetValue(obj).ToString() + "' then " + i);
            }
            sqlQuery.AppendLine("end as scenarioPriority");
            sqlQuery.AppendLine("FROM  ");
            sqlQuery.AppendLine(" (SELECT   ");
            sqlQuery.AppendLine(" aod.asOfDate ");
            sqlQuery.AppendLine(" ,bs.name as scenarioName ");
            sqlQuery.AppendLine(" ,ALP6.Scenario");
            sqlQuery.AppendLine(" ,sum(ALP6.sGapBal / ALP6.NMonths) as balance  ");
            sqlQuery.AppendLine(" ,alb.Acc_TypeOR as acc_type");
            sqlQuery.AppendLine(" ,alb.IsAsset as isAsset");
            sqlQuery.AppendLine(" ,alb.ClassOR as rbc");
            sqlQuery.AppendLine("FROM  ");
            sqlQuery.AppendLine("(SELECT  ");
            sqlQuery.AppendLine("month  ");
            sqlQuery.AppendLine(",simulationId  ");
            sqlQuery.AppendLine(",scenario  ");
            sqlQuery.AppendLine(",code  ");
            sqlQuery.AppendLine(",nMonths  ");
            sqlQuery.AppendLine(",sGapBal  ");
            sqlQuery.AppendLine(",sGapRate  ");
            sqlQuery.AppendLine("FROM Basis_ALP6 WHERE   ");

            for (int i = 0; i < objsCount; i++)
            {
                string closer = i < objsCount - 1 ? " OR" : "";
                object obj = objs[i];
                if (obj.GetType().GetProperty("simulation").GetValue(obj).ToString().Length > 0 && obj.GetType().GetProperty("scenario").GetValue(obj).ToString().Length > 0)
                    sqlQuery.AppendLine(" (simulationID = " + obj.GetType().GetProperty("simulation").GetValue(obj).ToString() + " and scenario = '" + obj.GetType().GetProperty("scenario").GetValue(obj).ToString() + "') " + closer);
            }
            sqlQuery.AppendLine(") as ALP6  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_ALB as ALB ON  ");
            sqlQuery.AppendLine(" ALB.simulationId = ALP6.simulationID AND  ");
            sqlQuery.AppendLine(" ALB.code = ALP6.code   ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_PeriodSmoother AS ps  ");
            sqlQuery.AppendLine(" ON ps.period <= ALp6.nMonths  ");
            sqlQuery.AppendLine(" INNER JOIN Simulations AS sim ON  ");
            sqlQuery.AppendLine(" sim.id = ALP6.SimulationId   ");
            sqlQuery.AppendLine(" INNER JOIN AsOfDateInfo as aod ON  ");
            //sqlQuery.AppendLine(" sim.InformationId = aod.id and ALP6.Month >= DATEADD(m, 61, aod.asOfDate) ");
            sqlQuery.AppendLine(" sim.InformationId = aod.id and CASE WHEN ps.period = 1 THEN CAST(alp6.month as datetime) ELSE DATEADD(m, ps.period - 1, CAST(alp6.month as datetime)) END  >= DATEADD(m, 60, aod.asOfDate) ");
            sqlQuery.AppendLine(" INNER JOIN Basis_Scenario as bs ON ");
            sqlQuery.AppendLine(" bs.Number = alp6.Scenario and bs.SimulationId = alp6.SimulationId  ");
            sqlQuery.AppendLine(" group by aod.asOfDate, bs.Name, ALB.isAsset, ALB.Acc_TypeOR, alb.ClassOR, scenario");
            sqlQuery.AppendLine(" ) as G");
            sqlQuery.AppendLine(" LEFT JOIN ATLAS_AccountType as accTypes ON");
            sqlQuery.AppendLine(" G.acc_type = accTypes.Id and G.isAsset = 1");
            sqlQuery.AppendLine(" LEFT JOIN ATLAS_CoreFundingLookup as cflookups ON  ");
            sqlQuery.AppendLine(" G.IsAsset = cflookups.isAsset and G.Acc_Type = cflookups.Acc_Type and G.Rbc = cflookups.RBC_Tier  ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_AccountTypeOrder as accTypeOrder ON  ");
            sqlQuery.AppendLine(" accTypeOrder.Acc_Type = G.acc_type AND accTypeOrder.IsAsset = G.isAsset ");
            sqlQuery.AppendLine(" where (cflookups.name is not null or (cflookups.name is null and G.IsAsset = 1 AND accTypes.Id <> 7 AND accTypes.Id <> 8)) AND (isnull(accTypes.Name, cflookups.name) <> 'Borrowings' OR (isnull(accTypes.Name, cflookups.name) = 'Borrowings' AND G.balance <> 0))"); //if we're not using defined groupings we don't want off balance sheet stuff in there
            sqlQuery.AppendLine(" order by scenarioPriority, isAsset desc, ");

            //assets should be order by the table. But liabs need special order
            sqlQuery.AppendLine(" CASE WHEN G.isAsset = 1 THEN accTypeOrder.[Order] ");
            sqlQuery.AppendLine("  ELSE ");
            sqlQuery.AppendLine(" 	CASE WHEN isnull(accTypes.Name, cflookups.name) = 'Equity' THEN 100 ");
            sqlQuery.AppendLine(" 	WHEN isnull(accTypes.Name, cflookups.name) = 'Other Liability' THEN 101 ");
            sqlQuery.AppendLine(" 	WHEN isnull(accTypes.Name, cflookups.name) = 'DDA' THEN 102 ");
            sqlQuery.AppendLine(" 	WHEN isnull(accTypes.Name, cflookups.name) = 'NOW' THEN 103 ");
            sqlQuery.AppendLine(" 	WHEN isnull(accTypes.Name, cflookups.name) = 'Savings' THEN 104 ");
            sqlQuery.AppendLine(" 	WHEN isnull(accTypes.Name, cflookups.name) = 'Other Deposits' THEN 105 ");
            sqlQuery.AppendLine(" 	WHEN isnull(accTypes.Name, cflookups.name) = 'Borrowings' THEN 106 ");
            sqlQuery.AppendLine(" 	END ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine("  END ");


            return Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
        }

        private DataTable CoreFundingTableDefinedGroupings(InstitutionService instService, CoreFundingUtilization cfu, DateTime packageAsOfDate, bool isHistoricTable, string grouping, bool giveError)
        {
            List<object> objs = new List<object>();

            if (!isHistoricTable)
            {
                foreach (CoreFundingUtilizationScenarioType scenType in cfu.CoreFundingUtilizationScenarioTypes.OrderBy(s => s.Priority))
                {
                    var o = instService.GetSimulationScenario(packageAsOfDate, cfu.AsOfDateOffset, cfu.SimulationTypeId, scenType.ScenarioType.Id);
                    if (o.GetType().GetProperty("simulation").GetValue(o).ToString().Length > 0 && o.GetType().GetProperty("scenario").GetValue(o).ToString().Length > 0)
                    {
                        objs.Add(instService.GetSimulationScenario(packageAsOfDate, cfu.AsOfDateOffset, cfu.SimulationTypeId, scenType.ScenarioType.Id));
                        if (!orderedScenNames.Contains(scenType.ScenarioType.Name))
                        {
                            orderedScenNames.Add(scenType.ScenarioType.Name);
                        }
                    }
                    else
                    {
                        if (giveError)
                        {
                            //errors.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, cfu.SimulationType.Name, cfu.AsOfDateOffset, packageAsOfDate.ToShortDateString()));
                        }
                        else
                        {
                            //warnings.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, cfu.SimulationType.Name, cfu.AsOfDateOffset, packageAsOfDate.ToShortDateString()));
                        }
                    }
                }
            }
            else
            {

            }
            foreach (CoreFundingUtilizationDateOffSets offset in cfu.CoreFundingUtilizationDateOffSets)
            {
                foreach (CoreFundingUtilizationScenarioType scenType in cfu.CoreFundingUtilizationScenarioTypes.OrderBy(s => s.Priority))
                {
                    var o = instService.GetSimulationScenario(packageAsOfDate, offset.OffSet, offset.SimulationTypeId, scenType.ScenarioType.Id);
                    if (o.GetType().GetProperty("simulation").GetValue(o).ToString().Length > 0 && o.GetType().GetProperty("scenario").GetValue(o).ToString().Length > 0)
                    {
                        objs.Add(instService.GetSimulationScenario(packageAsOfDate, offset.OffSet, offset.SimulationTypeId, scenType.ScenarioType.Id));
                        if (!orderedScenNames.Contains(scenType.ScenarioType.Name))
                        {
                            orderedScenNames.Add(scenType.ScenarioType.Name);
                        }
                    }
                    else
                    {
                        if (giveError) {
                            //errors.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, offset.SimulationType.Name, offset.OffSet, packageAsOfDate.ToShortDateString()));
                        }
                        else {
                            //warnings.Add(String.Format(Utilities.GetErrorTemplate(1), scenType.ScenarioType.Name, offset.SimulationType.Name, offset.OffSet, packageAsOfDate.ToShortDateString()));
                        }
                    }
                }
            }
            int objsCount = objs.Count;
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("G.asOfDate");
            sqlQuery.AppendLine(",G.scenarioName");
            sqlQuery.AppendLine(",g.name");
            sqlQuery.AppendLine(",G.balance");
            sqlQuery.AppendLine(",G.isAsset");

            sqlQuery.AppendLine(", case");
            for (int i = 0; i < objsCount; i++)
            {
                string closer = i < objsCount - 1 ? " OR" : "";
                object obj = objs[i];
                if (obj.GetType().GetProperty("simulation").GetValue(obj).ToString().Length > 0 && obj.GetType().GetProperty("scenario").GetValue(obj).ToString().Length > 0)
                    sqlQuery.AppendLine(" when G.Scenario = '" + obj.GetType().GetProperty("scenario").GetValue(obj).ToString() + "' then " + i);
            }
            sqlQuery.AppendLine("end as scenarioPriority");

            sqlQuery.AppendLine("FROM  ");
            sqlQuery.AppendLine(" (SELECT   ");
            sqlQuery.AppendLine(" aod.asOfDate ");
            sqlQuery.AppendLine(" ,bs.name as scenarioName ");
            sqlQuery.AppendLine(" ,alb.IsAsset as isAsset");
            sqlQuery.AppendLine(",isnull(dgs.Name, cflookups.name) as name ");
            sqlQuery.AppendLine(",isnull(dgs.Priority, accTypeOrder.[Order]) as priority ");
            sqlQuery.AppendLine(" ,sum(ALP6.sGapBal / ALP6.NMonths) as balance  ");
            sqlQuery.AppendLine(" ,Scenario ");
            sqlQuery.AppendLine(" ,alb.Acc_Type  ");
            sqlQuery.AppendLine("FROM  ");
            sqlQuery.AppendLine("(SELECT  ");
            sqlQuery.AppendLine("month  ");
            sqlQuery.AppendLine(",simulationId  ");
            sqlQuery.AppendLine(",scenario  ");
            sqlQuery.AppendLine(",code  ");
            sqlQuery.AppendLine(",nMonths  ");
            sqlQuery.AppendLine(",sGapBal  ");
            sqlQuery.AppendLine(",sGapRate  ");
            sqlQuery.AppendLine("FROM Basis_ALP6 WHERE   ");

            for (int i = 0; i < objsCount; i++)
            {
                string closer = i < objsCount - 1 ? " OR" : "";
                object obj = objs[i];
                if (obj.GetType().GetProperty("simulation").GetValue(obj).ToString().Length > 0 && obj.GetType().GetProperty("scenario").GetValue(obj).ToString().Length > 0)
                    sqlQuery.AppendLine(" (simulationID = " + obj.GetType().GetProperty("simulation").GetValue(obj).ToString() + " and scenario = '" + obj.GetType().GetProperty("scenario").GetValue(obj).ToString() + "') " + closer);
            }
            sqlQuery.AppendLine(") as ALP6  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_ALB as ALB ON  ");
            sqlQuery.AppendLine(" ALB.simulationId = ALP6.simulationID AND  ");
            sqlQuery.AppendLine(" ALB.code = ALP6.code   ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_PeriodSmoother AS ps  ");
            sqlQuery.AppendLine(" ON ps.period <= ALp6.nMonths  ");
            sqlQuery.AppendLine(" INNER JOIN Simulations AS sim ON  ");
            sqlQuery.AppendLine(" sim.id = ALP6.SimulationId   ");
            sqlQuery.AppendLine(" INNER JOIN AsOfDateInfo as aod ON  ");
            sqlQuery.AppendLine(" sim.InformationId = aod.id and ALP6.Month >= DATEADD(m, 60, aod.asOfDate) ");
            sqlQuery.AppendLine(" INNER JOIN Basis_Scenario as bs ON ");
            sqlQuery.AppendLine(" bs.Number = alp6.Scenario and bs.SimulationId = alp6.SimulationId  ");
            sqlQuery.AppendLine(" LEFT JOIN");
            sqlQuery.AppendLine(" (SELECT");
            sqlQuery.AppendLine(" dgg.name");
            sqlQuery.AppendLine(" ,dgg.[Priority]");
            sqlQuery.AppendLine(" ,dgc.IsAsset");
            sqlQuery.AppendLine(" ,dgc.Acc_Type");
            sqlQuery.AppendLine(" ,dgc.RbcTier");
            sqlQuery.AppendLine(" FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg");
            sqlQuery.AppendLine(" INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
            sqlQuery.AppendLine(" ON dg.id = dgg.DefinedGroupingId");
            sqlQuery.AppendLine(" INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc");
            sqlQuery.AppendLine(" ON dgg.id = dgc.DefinedGroupingGroupId");
            sqlQuery.AppendLine(" WHERE dg.id = " + grouping.Replace("dg_", "") + " and dgg.IsAsset = 1");
            sqlQuery.AppendLine(" ) AS dgs ON ");
            sqlQuery.AppendLine(" dgs.acc_Type = alb.acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
            sqlQuery.AppendLine(" LEFT JOIN ATLAS_CoreFundingLookup as cflookups ON");
            sqlQuery.AppendLine(" alb.IsAsset = cflookups.isAsset");
            sqlQuery.AppendLine(" AND alb.Acc_TypeOR = cflookups.Acc_Type ");
            sqlQuery.AppendLine(" AND alb.ClassOR = cflookups.RBC_Tier");

            sqlQuery.AppendLine(" LEFT JOIN ATLAS_AccountTypeOrder as accTypeOrder ON ");
            sqlQuery.AppendLine(" alb.Acc_Type = accTypeOrder.Acc_Type");
            sqlQuery.AppendLine(" AND alb.IsAsset = accTypeOrder.isAsset");

            sqlQuery.AppendLine(" where cflookups.name is not null or (cflookups.name is null and dgs.Name is not null)");
            sqlQuery.AppendLine(" group by aod.asOfDate, bs.Name, ALB.isAsset, isnull(dgs.Name, cflookups.name),isnull(dgs.Priority, accTypeOrder.[Order]), scenario, alb.Acc_Type");
            sqlQuery.AppendLine(" ) as g");
            sqlQuery.AppendLine(" order by scenarioPriority, isAsset desc, priority ");

            //sqlQuery.AppendLine(" group by aod.asOfDate, bs.Name, ALB.isAsset, ALB.Acc_Type, alb.RbcTier");
            //sqlQuery.AppendLine(" ) as G");
            //sqlQuery.AppendLine(" LEFT JOIN ATLAS_AccountType as accTypes ON");
            //sqlQuery.AppendLine(" G.acc_type = accTypes.Id and G.isAsset = 1");
            //sqlQuery.AppendLine(" LEFT JOIN DWR_FedRates_JAKE2.dbo.CoreFundingLookups as cflookups ON  ");
            //sqlQuery.AppendLine(" G.IsAsset = cflookups.isAsset and G.Acc_Type = cflookups.Acc_Type and G.Rbc = cflookups.RBC_Tier  ");
            //sqlQuery.AppendLine(" where cflookups.name is not null or (cflookups.name is null and G.IsAsset = 1) ");

            return Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
        }

    }
}