﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class SimulationSummaryService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object SimulationSummaryViewModel(int reportId)
        {
            string conStr = "";
            var conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
            {
                InitialCatalog = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString()
            };
            conStr = conenctionStringBuilder.ConnectionString;

            InstitutionContext gctx = new InstitutionContext(conStr);

            //if prepayments report select, always have the sub accounts grouping
            var simSum = gctx.SimulationSummaries.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            if (simSum.ReportSelection == 2)
            {
                simSum.Grouping = "0";
            }

            var instService = new InstitutionService(simSum.InstitutionDatabaseName);
            var info = instService.GetInformation(rep.Package.AsOfDate, simSum.AsOfDateOffset);

            string simSumAOD = info.AsOfDate.ToString();
            bool monthly = simSum.MonthlyQuarterly.IndexOf("0") > -1;
            int nPeriods = monthly ? 24 : 20;

            DataTable dt = new DataTable();

            //If Not Found Kick Out
            if (simSum == null) throw new Exception("Simulation Summary Not Found");


            var obj = instService.GetSimulationScenario(rep.Package.AsOfDate, simSum.AsOfDateOffset, simSum.SimulationTypeId, simSum.ScenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();

            if (simulationId == "" || scenario == "" || asOfDate == "") {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), simSum.ScenarioType.Name, simSum.SimulationType.Name, simSum.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                dt = SimulationSummaryTable(instService, simSum.SimulationTypeId, simSum.ScenarioTypeId, rep.Package.AsOfDate, simSum.AsOfDateOffset, simSum.ReportSelection, simSum.Grouping, simSum.TaxEquivalent, simSum.MonthlyQuarterly);
                var totalingItems = new List<BalanceSheetTotaler.TotalingItem>();

                if (simSum.ReportSelection == 1) {

                    totalingItems.Add(new BalanceSheetTotaler.TotalingItem("endBal", BalanceSheetTotaler.TotalingType.Average, "summedBal"));
                    //if (simSum.Grouping == "0")
                    //{

                    //}
                    //else
                    //{
                    //    totalingItems.Add(new BalanceSheetTotaler.TotalingItem("endBal", BalanceSheetTotaler.TotalingType.Average));
                    //}

                }
                else {
                    totalingItems.Add(new BalanceSheetTotaler.TotalingItem("endBal"));
                }

                if (simSum.Grouping.IndexOf("dg") > -1) {
                    //defined grouping stuff
                    dt = BalanceSheetTotaler.CalcTotalsFor(dt, totalingItems, BalanceSheetTotaler.ViewByType.AccountTypes, "month", "accountName");
                    dt = Utilities.PivotTable(dt, "accountName", new string[] { "isAsset" }, simSumAOD, monthly, nPeriods, true);
                }
                else {
                    switch (Int32.Parse(simSum.Grouping)) {
                        case 0://sub accounts
                            if (simSum.ReportSelection < 2) {
                                dt = BalanceSheetTotaler.CalcTotalsFor(dt, totalingItems, BalanceSheetTotaler.ViewByType.SubAccounts, "month");
                                dt = Utilities.PivotTable(dt, "name", new string[] { "Cat_Type" }, simSumAOD, monthly, nPeriods, true);
                            }
                            else if (simSum.ReportSelection == 3) {
                                dt = BalanceSheetTotaler.CalcTotalsFor(dt, totalingItems, BalanceSheetTotaler.ViewByType.SubAccounts, "month");
                                dt = Utilities.PivotTable(dt, "name", new string[] { "Cat_Type" }, simSumAOD, monthly, nPeriods, true);
                            }
                            else if (simSum.ReportSelection == 2) {
                                dt = Utilities.PivotTable(dt, "name", new string[] { "NEString", "Calc", "PCType" }, simSumAOD, monthly, nPeriods, false);
                            }

                            break;
                        case 3://account type
                            dt = BalanceSheetTotaler.CalcTotalsFor(dt, totalingItems, BalanceSheetTotaler.ViewByType.AccountTypes, "month", "accountName");
                            dt = Utilities.PivotTable(dt, "accountName", new string[] { "isAsset", "Cat_Type" }, simSumAOD, monthly, nPeriods, true);
                            break;
                        case 2://classifications
                            dt = BalanceSheetTotaler.CalcTotalsFor(dt, totalingItems, BalanceSheetTotaler.ViewByType.Classifications, "month", "classificationName", acctNameCol: "accountName", isAssetCol: "isAsset");
                            dt = Utilities.PivotTable(dt, "classificationName", new string[] { "isAsset", "accountName", "Cat_Type" }, simSumAOD, monthly, nPeriods, true);
                            break;

                    }
                }
            }

            
            //dt = BalanceSheetTotaler.CalcTotalsFor(dt, totalingItems, BalanceSheetTotaler.ViewByType.None, "month");
            //dt = Utilities.PivotTable(dt, "accountName", new string[]{"isAsset"});

            return new
        {
            data = dt,
            reportSelection = simSum.ReportSelection,
            monthly = simSum.MonthlyQuarterly,
            scenarioTypeName = simSum.ScenarioType.Name,
            warnings = warnings,
            errors = errors
        };
        }

        private string GetQuarterlyMonths(int month)
        {
            List<int> monthList = new List<int>();
            int tempMonth = month + 3;
            monthList.Add(month);

            while ((tempMonth - 12) != month)
            {
                if (tempMonth > 12)
                {
                    monthList.Add(tempMonth - 12);
                }
                else
                {
                    monthList.Add(tempMonth);
                }

                tempMonth += 3;
            }

            return String.Join(",", monthList);
        }

        private string CreateQuartleryBuckets(int aodMonth, string monthQualifier = "")
        {
            StringBuilder block = new StringBuilder();
            List<List<int>> blockGroups = new List<List<int>>();
            List<int> qEndMonths = new List<int>();
            string[] quarterEndMonths = GetQuarterlyMonths(aodMonth).Split(',');
            string monthStr = monthQualifier + "Month";

            block.AppendLine("CASE");

            for (int i = 0; i < quarterEndMonths.Length; i++)
            {
                int mo = Int32.Parse(quarterEndMonths[i]);
                int prevMo = ((12 + (mo - 1)) % 12);
                int prevMo2 = ((12 + (mo - 2)) % 12);

                if (prevMo == 0)
                {
                    prevMo = 12;
                }
                if (prevMo2 == 0)
                {
                    prevMo2 = 12;
                }
                string months = prevMo2 + "," + prevMo + "," + mo;

                switch (mo)
                {
                    case 1:
                        block.AppendLine("WHEN DATEPART(m, " + monthStr + ") in (" + months + ") THEN");
                        block.AppendLine(" CASE WHEN DATEPART(m, " + monthStr + ") in(11,12) THEN CAST('" + mo + "/1/' + CAST(DATEPART(yyyy, " + monthStr + ") + 1 as varchar(5)) AS DATETIME) ELSE CAST('" + mo + "/1/' + CAST(DATEPART(yyyy, " + monthStr + ") as varchar(5)) AS DATETIME) END");
                        break;
                    case 2:
                        block.AppendLine("WHEN DATEPART(m, Month) in (" + months + ") THEN");
                        block.AppendLine(" CASE WHEN DATEPART(m, " + monthStr + ") = 12 THEN CAST('" + mo + "/1/' + CAST(DATEPART(yyyy, " + monthStr + ") + 1 as varchar(5)) as datetime) ELSE CAST('" + mo + "/1/' + CAST(DATEPART(yyyy, " + monthStr + ") as varchar(5)) AS DATETIME)END");
                        break;
                    default:
                        block.AppendLine("WHEN DATEPART(m, " + monthStr + ") in (" + months + ") THEN CAST('" + mo + "/1/' + CAST(DATEPART(yyyy, " + monthStr + ") as varchar(5)) AS datetime)");
                        break;
                }
            }

            block.AppendLine(" END");

            string query = block.ToString();
            return query;
        }

        private string GetQuery(int rprtSelection, string grouping, bool monthly, string aod, bool taxEquiv, string simulationId, string scenario)
        {

            string alpTable = "";
            string alpVolumeField = "";
            string alaVoumeField = "";
            string monthFilter = monthly ? "	AND month <= DATEADD(m, 24, '" + aod + "')" : "	AND month <= DATEADD(m, 60, '" + aod + "')";

            switch (rprtSelection)
            {
                case 0:
                    alpTable = "Basis_ALP1";
                    alpVolumeField = "EndBal";
                    alaVoumeField = "End_Bal";
                    break;
                case 1:
                    alpTable = "Basis_ALP13";
                    if (taxEquiv)
                    {
                        alpVolumeField = "TAveTEYLD";
                        alaVoumeField = "Total_Avg_TEYLD";
                    }
                    else
                    {
                        alpVolumeField = "TotalYieldEnd";
                        alaVoumeField = "Total_End_Rate";
                    }
                    break;
                case 2:
                    break;
                case 3:
                    alpTable = "Basis_ALP13";
                    alpVolumeField = taxEquiv ? "AveTEAmt" : "IEAmount";
                    alaVoumeField = taxEquiv ? "Total_Avg_TEAmt" : "IE";
                    break;

            }
            StringBuilder q = new StringBuilder();


            switch (rprtSelection)
            {
                //balance sheet
                case 0:
                    //defined grouping
                    if (grouping.IndexOf("dg") > -1)
                    {
                        q.AppendLine("SELECT");
                        q.AppendLine("	month");
                        q.AppendLine("	,isAsset");
                        q.AppendLine("	, accountName");
                        q.AppendLine("	,SUM(endBal) as endBal");
                        q.AppendLine("	,accountOrder");
                        q.AppendLine("FROM");
                        q.AppendLine("	( ");
                        q.AppendLine("	  (SELECT");
                        q.AppendLine("		alp.code");
                        q.AppendLine("		,alp.endBal");
                        if (monthly)
                        {
                            q.AppendLine(",alp.month");
                        }
                        else
                        {
                            q.AppendLine("	,qrtGroup as month");
                        }
                        q.AppendLine("		,dgs.name as accountName");
                        q.AppendLine("		,alb.isAsset");
                        q.AppendLine("		,dgs.[Priority] as accountOrder");
                        q.AppendLine("		,alb.Acc_TypeOR as acc_type");
                        q.AppendLine("		,alb.ClassOR as rbcTier");
                        q.AppendLine("		FROM");
                        q.AppendLine("	  (SELECT code");
                        q.AppendLine("	, EndBal / nmonths as endBal");
                        q.AppendLine("		, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                        q.AppendLine("	, SimulationId ");

                        if (!monthly)
                        {
                            q.AppendLine("	," + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                        }
                        q.AppendLine("	FROM Basis_ALP1 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                        q.AppendLine(monthFilter);
                        if (!monthly)
                        {
                            q.AppendLine(" AND CASE WHEN nmonths = 1 THEN DATEPART(m, month) ELSE DATEPART(m, DATEADD(m, nMonths -1, month)) END in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                        }
                       
                        q.AppendLine("	)as alp");
                        q.AppendLine("	  INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                        q.AppendLine("		INNER JOIN");
                        q.AppendLine("(SELECT  ");
                        q.AppendLine("				dgg.name ");
                        q.AppendLine("				,dgg.[Priority] ");
                        q.AppendLine("				,dgc.IsAsset  ");
                        q.AppendLine("				,dgc.Acc_Type  ");
                        q.AppendLine("				,dgc.RbcTier  ");
                        q.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                        q.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                        q.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                        q.AppendLine("			  WHERE dg.id = " + grouping.Replace("dg_", "") + ") as dgs ON ");
                        q.AppendLine("			  dgs.acc_Type = alb.Acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
                        q.AppendLine("	  WHERE alb.cat_Type in (0,1,5))");
                        q.AppendLine("	  UNION ALL");
                        q.AppendLine("	  (SELECT");
                        q.AppendLine("		ala.code");
                        q.AppendLine("		,ala.endBal");
                        q.AppendLine("		,ala.month");
                        q.AppendLine("		,dgs.name as accountName");
                        q.AppendLine("		,alb.isAsset");
                        q.AppendLine("		,dgs.[Priority] as accountOrder");
                        q.AppendLine("		,alb.Acc_TypeOR as acc_type");
                        q.AppendLine("		,alb.ClassOR as rbcTier");
                        q.AppendLine("	  FROM");
                        q.AppendLine("	  (SELECT code, End_Bal as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId + "");
                        q.AppendLine(" AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                        q.AppendLine("	  INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                        q.AppendLine("		INNER JOIN");
                        q.AppendLine("(SELECT  ");
                        q.AppendLine("				dgg.name ");
                        q.AppendLine("				,dgg.[Priority] ");
                        q.AppendLine("				,dgc.IsAsset  ");
                        q.AppendLine("				,dgc.Acc_Type  ");
                        q.AppendLine("				,dgc.RbcTier  ");
                        q.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                        q.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                        q.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                        q.AppendLine("			  WHERE dg.id = " + grouping.Replace("dg_", "") + ") as dgs ON ");
                        q.AppendLine("			  dgs.acc_Type = alb.Acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
                        q.AppendLine("	  WHERE alb.cat_Type in (0,1,5))");
                        q.AppendLine("	) as alp");
                        q.AppendLine("Group by month, IsAsset, accountName, accountOrder");
                        q.AppendLine("Order by isAsset desc, accountOrder");

                    }
                    else
                    {
                        switch (Int32.Parse(grouping))
                        {
                            case 0://sub Accounts
                                q.AppendLine("SELECT");
                                q.AppendLine("  alb.name");
                                q.AppendLine("  ,alb.sequence");
                                q.AppendLine("  ,alb.Cat_Type");
                                q.AppendLine("  ,alp.month");
                                q.AppendLine("  ,alp.endBal");
                                q.AppendLine("FROM");
                                q.AppendLine("(SELECT name, code, sequence, cat_Type FROM Basis_ALB WHERE SimulationId = " + simulationId + " AND Cat_Type <> 6) as alb");
                                q.AppendLine(" LEFT JOIN ");
                                q.AppendLine("( ");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("	alp.code");
                                q.AppendLine("	,alp.endBal");
                                q.AppendLine(",cast(datepart(m, month) as varchar(3)) + '/1/' + cast(datepart(yyyy,month) as varchar(4)) as month");
                                //if (monthly)
                                //{

                                //}
                                //else
                                //{
                                //    q.AppendLine("	,qrtGroup as month");
                                //}
                                q.AppendLine("	FROM");
                                q.AppendLine("  (SELECT code");
                                q.AppendLine(", EndBal as endBal");
                                q.AppendLine("	, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                                q.AppendLine(", SimulationId ");
                                if (!monthly)
                                {
                                    //q.AppendLine("	," + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                }
                                q.AppendLine("FROM Basis_ALP1 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                                q.AppendLine(monthFilter);
                                if (!monthly)
                                {
                                    q.AppendLine("and datepart(m, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END) in (8, 11, 2, 5)");
                                }
                                q.AppendLine(")as alp");
                                q.AppendLine("  RIGHT JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.Cat_Type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine("  ");
                                q.AppendLine("  UNION ALL");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("    ala.code");
                                q.AppendLine("    ,ala.endBal");
                                q.AppendLine("    ,ala.month");
                                q.AppendLine("  FROM");
                                q.AppendLine("  (SELECT code, " + alaVoumeField + " as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId);
                                q.AppendLine(" AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                                q.AppendLine("  RIGHT JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.Cat_Type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine(")as alp");
                                q.AppendLine("ON alp.code = alb.code ");
                                q.AppendLine("ORDER BY alb.Sequence, alp.Month");
                                break;
                            case 3://account type
                                q.AppendLine("SELECT");
                                q.AppendLine("	cast(cast(datepart(m, month) as varchar(3)) + '/1/' + cast(datepart(yyyy,month) as varchar(4)) as datetime) as month");
                                q.AppendLine("	,isAsset");
                                q.AppendLine(",CASE WHEN accountName = 'Other' then");
                                q.AppendLine("		CASE WHEN isAsset = 1 then 'Other Asset'");
                                q.AppendLine("		else 'Other Liability'");
                                q.AppendLine("		END");
                                q.AppendLine("	ELSE accountName");
                                q.AppendLine("	END as accountName");
                                q.AppendLine("	,SUM(endBal) as endBal");
                                q.AppendLine("	,accountOrder");
                                q.AppendLine("FROM");
                                q.AppendLine("	( ");
                                q.AppendLine("	  (SELECT");
                                q.AppendLine("		alp.code");
                                q.AppendLine("		,alp.endBal");
                                q.AppendLine(",alp.month");
                                q.AppendLine("		,aa.name as accountName");
                                q.AppendLine("		,alb.isAsset");
                                q.AppendLine("		,ao.[order] as accountOrder");
                                q.AppendLine("		FROM");
                                q.AppendLine("	  (SELECT code");
                                q.AppendLine("	, EndBal as endBal");
                                if (monthly)
                                {
                                    q.AppendLine(",month");
                                }
                                else
                                {
                                    q.AppendLine("		, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                                }
                                q.AppendLine("	, SimulationId ");
                                q.AppendLine("	FROM Basis_ALP1 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                                q.AppendLine(monthFilter);
                                if (!monthly)
                                {
                                    q.AppendLine(" AND CASE WHEN nmonths = 1 THEN DATEPART(m, month) ELSE DATEPART(m, DATEADD(m, nMonths -1, month)) END in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine("	)as alp");
                                q.AppendLine("	  INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                                q.AppendLine("	  	INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("		aa.Id = alb.acc_typeOR");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("		alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("	  WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("	  UNION ALL");
                                q.AppendLine("	  (SELECT");
                                q.AppendLine("		ala.code");
                                q.AppendLine("		,ala.endBal");
                                q.AppendLine("		,ala.month");
                                q.AppendLine("		,aa.name as accountName");
                                q.AppendLine("		,alb.isAsset");
                                q.AppendLine("		,ao.[order] as accountOrder");
                                q.AppendLine("	  FROM");
                                q.AppendLine("	  (SELECT code, End_Bal as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId);
                                q.AppendLine(" AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                                q.AppendLine("	  INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("	  INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("		aa.Id = alb.acc_typeOR");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("		alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("	  WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("	) as alp");
                                q.AppendLine("Group by month, IsAsset, accountName, accountOrder");
                                q.AppendLine("Order by isAsset desc,max(accountOrder)");

                                break;
                            case 2://classificaitons

                                q.AppendLine("SELECT");
                                q.AppendLine("  cast(cast(datepart(m, month) as varchar(3)) + '/1/' + cast(datepart(yyyy,month) as varchar(4)) as datetime) as month");
                                q.AppendLine("	,isAsset");
                                q.AppendLine(",CASE WHEN accountName = 'Other' THEN ");
                                q.AppendLine("		CASE WHEN isAsset = 1 THEN 'Other Assets'");
                                q.AppendLine("			ELSE 'Other Liabilities'");
                                q.AppendLine("		END");
                                q.AppendLine("		ELSE accountName");
                                q.AppendLine("	END as accountname");
                                q.AppendLine("	,classificationName");
                                q.AppendLine("	,SUM(endBal) as endBal");
                                q.AppendLine("	,classificationOrder");
                                q.AppendLine("FROM");
                                q.AppendLine("	( ");
                                q.AppendLine("	  (SELECT");
                                q.AppendLine("		alp.code");
                                q.AppendLine("		,alp.endBal");
                                q.AppendLine("      ,alp.month");
                                q.AppendLine("		,aa.name as accountName");
                                q.AppendLine("		,ac.name as classificationName");
                                q.AppendLine("		,alb.isAsset");
                                q.AppendLine("		,ao.[order] as accountOrder");
                                q.AppendLine("		,co.[order] as classificationOrder");
                                q.AppendLine("		FROM");
                                q.AppendLine("	  (SELECT code");
                                q.AppendLine("	, EndBal as endBal");
                                if (monthly)
                                {
                                    q.AppendLine(",month");
                                }
                                else
                                {
                                    q.AppendLine("		, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                                }
                                q.AppendLine("	, SimulationId ");
                                q.AppendLine("	FROM Basis_ALP1 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                                q.AppendLine(monthFilter);
                                if (!monthly)
                                {
                                    q.AppendLine(" AND CASE WHEN nmonths = 1 THEN DATEPART(m, month) ELSE DATEPART(m, DATEADD(m, nMonths -1, month)) END in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine("	)as alp");
                                q.AppendLine("	  INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                                q.AppendLine("	  	INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("		aa.Id = alb.acc_typeOR");
                                q.AppendLine("	  	INNER JOIN");
                                q.AppendLine("		ATLAS_Classification as ac on ");
                                q.AppendLine("		alb.IsAsset = ac.IsAsset and alb.ClassOR = ac.RbcTier and alb.acc_typeOR = ac.AccountTypeId");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("		alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_ClassificationOrder as co on");
                                q.AppendLine("		alb.Acc_TypeOR = co.Acc_Type and alb.ClassOR = co.RBC_Tier");
                                q.AppendLine("	  WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("	  UNION ALL");
                                q.AppendLine("	  (SELECT");
                                q.AppendLine("		ala.code");
                                q.AppendLine("		,ala.endBal");
                                q.AppendLine("		,ala.month");
                                q.AppendLine("		,aa.name as accountName");
                                q.AppendLine("		,ac.name as classificationName");
                                q.AppendLine("		,alb.isAsset");
                                q.AppendLine("		,ao.[order] as accountOrder");
                                q.AppendLine("		,co.[order] as classificationOrder");
                                q.AppendLine("	  FROM");
                                q.AppendLine("	  (SELECT code, End_Bal as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId);
                                q.AppendLine(" AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                                q.AppendLine("	  INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("	  INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("		aa.Id = alb.acc_typeOR");
                                q.AppendLine("	  INNER JOIN");
                                q.AppendLine("		ATLAS_Classification as ac on ");
                                q.AppendLine("		alb.IsAsset = ac.IsAsset and alb.ClassOR = ac.RbcTier and alb.acc_typeOR = ac.AccountTypeId		");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("		alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_ClassificationOrder as co on");
                                q.AppendLine("		alb.Acc_TypeOR = co.Acc_Type and alb.ClassOR = co.RBC_Tier");
                                q.AppendLine("	  WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("	) as alp");
                                q.AppendLine("Group by month, IsAsset, accountName, accountOrder, classificationName, classificationOrder");
                                q.AppendLine("Order by isAsset desc, accountOrder, classificationOrder");
                                break;
                        }
                    }

                    break;
                case 1://rate yield
                    if (grouping.IndexOf("dg") > -1)
                    {
                        //defined grouping stuff
                        q.AppendLine("SELECT");
                        q.AppendLine("       month");
                        q.AppendLine("       ,isAsset");
                        q.AppendLine("       ,accountName");
                        q.AppendLine("       ,SUM(endBal*yld)/ nullif (sum(endBal), 0) as endBal");
                        q.AppendLine("       ,sum(EndBal) as summedBal");
                        q.AppendLine("FROM");
                        q.AppendLine("       ( ");
                        q.AppendLine("         (SELECT");
                        q.AppendLine("              alp13.code");
                        q.AppendLine("              ,isnull(alp13.yld, 0) as yld");
                        q.AppendLine("              ,alp1.endBal");
                        if (monthly)
                        {
                            q.AppendLine(",alp1.month");
                        }
                        else
                        {
                            q.AppendLine(",qrtGroup as month");
                        }
                        q.AppendLine("              ,dgs.name as accountName");
                        q.AppendLine("              ,alb.isAsset");
                        q.AppendLine("              ,dgs.[priority] as accountOrder");
                        q.AppendLine("              FROM");
                        q.AppendLine("         (SELECT code");
                        if (taxEquiv)
                        {
                            q.AppendLine(", TEndTEYLD as yld");
                        }
                        else
                        {
                            q.AppendLine(", TotalYieldEnd as yld");
                        }
                        q.AppendLine("              , CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                        q.AppendLine("       ,SimulationId");
                        q.AppendLine("       ,scenario");
                        if (!monthly)
                        {
                            q.AppendLine(", " + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                        }
                        q.AppendLine("       FROM Basis_ALP13");
                        q.AppendLine("       WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                        q.AppendLine(monthFilter);
                        q.AppendLine("       )as alp13");
                        q.AppendLine("       FULL OUTER JOIN");
                        q.AppendLine("       (SELECT ");
                        q.AppendLine("EndBal");
                        q.AppendLine(", simulationId, Scenario, code, month from Basis_ALP1 as alp1 where simulationid = " + simulationId + " AND Scenario = '" + scenario + "' " + monthFilter + ") as alp1");
                        q.AppendLine("       on alp1.SimulationId = alp13.simulationId and alp1.scenario = alp13.scenario and alp1.code = alp13.Code and alp1.month = alp13.month");
                        q.AppendLine("         INNER JOIN Basis_ALB AS alb on ISNULL(alp13.code, alp1.code) = alb.code and ISNULL(alp13.simulationid,alp1.SimulationId) = alb.simulationid");
                        q.AppendLine("		INNER JOIN");
                        q.AppendLine("(SELECT  ");
                        q.AppendLine("				dgg.name ");
                        q.AppendLine("				,dgg.[Priority] ");
                        q.AppendLine("				,dgc.IsAsset  ");
                        q.AppendLine("				,dgc.Acc_Type  ");
                        q.AppendLine("				,dgc.RbcTier  ");
                        q.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                        q.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                        q.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                        q.AppendLine("			  WHERE dg.id = " + grouping.Replace("dg_", "") + ") as dgs ON ");
                        q.AppendLine("			  dgs.acc_Type = alb.Acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
                        q.AppendLine("         WHERE alb.cat_Type in (0,1,5))");
                        q.AppendLine("         UNION ALL");
                        q.AppendLine("         (SELECT");
                        q.AppendLine("              ala.code");
                        q.AppendLine("              ,ala.yld");
                        q.AppendLine("              ,ala.endBal as endBal");
                        q.AppendLine("              ,ala.month");
                        q.AppendLine("              ,dgs.name as accountName");
                        q.AppendLine("              ,alb.isAsset");
                        q.AppendLine("              ,dgs.[priority] as accountOrder");
                        q.AppendLine("         FROM");
                        q.AppendLine("         (SELECT code, ");
                        if (taxEquiv)
                        {
                            q.AppendLine("Total_End_TEYLD as yld, ");
                        }
                        else
                        {
                            q.AppendLine("Total_End_Rate as yld, ");
                        }
                        q.AppendLine("End_Bal as endBal , ");
                        q.AppendLine("month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId);
                        q.AppendLine("AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                        q.AppendLine("         INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                        q.AppendLine("		INNER JOIN");
                        q.AppendLine("(SELECT  ");
                        q.AppendLine("				dgg.name ");
                        q.AppendLine("				,dgg.[Priority] ");
                        q.AppendLine("				,dgc.IsAsset  ");
                        q.AppendLine("				,dgc.Acc_Type  ");
                        q.AppendLine("				,dgc.RbcTier  ");
                        q.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                        q.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                        q.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                        q.AppendLine("			  WHERE dg.id = " + grouping.Replace("dg_", "") + ") as dgs ON ");
                        q.AppendLine("			  dgs.acc_Type = alb.Acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
                        q.AppendLine("         WHERE alb.cat_Type in (0,1,5))");
                        q.AppendLine("       ) as alp");
                        q.AppendLine("Group by month, IsAsset, accountName, accountOrder");
                        q.AppendLine("Order by isAsset desc, accountOrder");
                    }
                    else
                    {
                        switch (Int32.Parse(grouping))
                        {
                            case 0://sub accounts
                                q.AppendLine("SELECT");
                                q.AppendLine("name ");
                                q.AppendLine(",alb.Sequence");
                                q.AppendLine(",Cat_Type");
                                q.AppendLine(",month");
                                q.AppendLine(",isAsset");
                                //q.AppendLine(",yld as endBal");
                                //q.AppendLine(",endbal as summedBal");
                                q.AppendLine("       ,SUM(endBal*yld)/ nullif (sum(endBal), 0) as endBal");
                                q.AppendLine("       ,sum(EndBal) as summedBal");
                                q.AppendLine("FROM");
                                q.AppendLine("(SELECT");
                                q.AppendLine("	name");
                                q.AppendLine("	,code");
                                q.AppendLine("	,sequence");
                                q.AppendLine("	,Cat_Type");
                                q.AppendLine("	,isAsset");
                                q.AppendLine("FROM Basis_ALB WHERE simulationID = " + simulationId + " and Cat_Type <> 6) as alb");
                                q.AppendLine("LEFT JOIN");
                                q.AppendLine("(");
                                q.AppendLine("	(SELECT");
                                q.AppendLine("		isnull(alp13.Code, alp1.code) as code");
                                q.AppendLine("		,isnull(yld, 0) as yld");
                                q.AppendLine("		,endBal");
                                if (monthly)
                                {
                                    q.AppendLine("		,isnull(alp13.month, alp1.month) as month");
                                }
                                else
                                {
                                    q.AppendLine("		,isnull(alp13.month, alp1.month) as month");
                                    //q.AppendLine("		,qrtGroup as month");
                                }
                                q.AppendLine("		from");
                                q.AppendLine("		(select ");
                                q.AppendLine("		alp13.code");
                                if (taxEquiv)
                                {
                                    q.AppendLine("		,TEndTEYLD as yld");
                                }
                                else
                                {
                                    q.AppendLine("		,TotalYieldEnd as yld");
                                }
                                q.AppendLine("		,CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END as month");
                                q.AppendLine("		,alp13.SimulationId");
                                q.AppendLine("		,Scenario");
                                if (!monthly)
                                {

                                    //q.AppendLine(", " + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                }
                                q.AppendLine("		FROM Basis_Alp13 as alp13 WHERE simulationId = " + simulationId + " and scenario = '" + scenario + "'");
                                if (monthly)
                                {
                                    q.AppendLine(monthFilter);
                                }
                                else
                                {
                                    q.AppendLine("AND CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END <= DATEADD(m, 60, '" + aod + "')");
                                    q.AppendLine("and datepart(m, CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END) in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine("");
                                q.AppendLine("		) as alp13");
                                q.AppendLine("		right join");
                                q.AppendLine("		(");
                                q.AppendLine("		select alp1.code");
                                q.AppendLine("		,endbal");
                                //if (taxEquiv)
                                //{
                                //    q.AppendLine("		,endbal");
                                //}
                                //else
                                //{
                                //    q.AppendLine("		,AveBal as endBal");
                                //}
                                q.AppendLine("		,CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END as month");
                                q.AppendLine("		,alp1.Simulationid");
                                q.AppendLine("		,Scenario");
                                q.AppendLine("		from Basis_ALP1 as alp1 WHERE simulationId = " + simulationId + " and scenario = '" + scenario + "'");
                                if (monthly)
                                {
                                    q.AppendLine(monthFilter + " ) as alp1");
                                }
                                else
                                {
                                    q.AppendLine("AND CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END <= DATEADD(m, 60, '" + aod + "') and datepart(m,CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END) in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ") ) as alp1");
                                }
                                q.AppendLine("		on alp1.SimulationId = alp13.SimulationId and alp1.scenario = alp13.scenario and alp1.code = alp13.code and alp1.month = alp13.month");
                                q.AppendLine("		inner join Basis_ALB as alb on isnull(alp13.code, alp1.code) = alb.code and isnull(alp13.SimulationId, alp1.SimulationId) = alb.SimulationId");
                                q.AppendLine("		)");
                                q.AppendLine("		UNION ALL");
                                q.AppendLine("		(SELECT");
                                q.AppendLine("		ala.code");
                                q.AppendLine("		,yld");
                                q.AppendLine("		,endBal");
                                q.AppendLine("		,month");
                                q.AppendLine("		from ");
                                q.AppendLine("		(select ");
                                q.AppendLine("		code ");
                                if (taxEquiv)
                                {
                                    q.AppendLine("		,Total_End_TEYLD as yld");
                                }
                                else
                                {
                                    q.AppendLine("		,Total_End_Rate as yld");
                                }
                                q.AppendLine("		,End_Bal as endBal");
                                q.AppendLine("		,month");
                                q.AppendLine("		,SimulationId");
                                q.AppendLine("		from Basis_ALA where simulationId = " + simulationId + "");
                                q.AppendLine("      AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                                q.AppendLine("		");
                                q.AppendLine("		INNER JOIN Basis_ALB as alb on ala.code = alb.code and ala.SimulationId = alb.SimulationId");
                                q.AppendLine("		where alb.Cat_Type in (0,1,5)");
                                q.AppendLine("		)");
                                q.AppendLine("		) as ala");
                                q.AppendLine("ON ala.code = alb.code");
                                q.AppendLine("where (ala.month is null and cat_type not in(0,1,5)) or ala.month is not null");
                                q.AppendLine("group by name,alb.Sequence,Cat_Type,month,isAsset");
                                q.AppendLine("Order By Sequence, isAsset desc, month");

                                break;
                            case 3://account type
                                q.AppendLine("SELECT");
                                q.AppendLine("       month");
                                q.AppendLine("       ,isAsset");
                                q.AppendLine(",CASE WHEN accountName = 'Other' then");
                                q.AppendLine("			CASE WHEN isAsset = 1 then 'Other Asset'");
                                q.AppendLine("			ELSE 'Other Liability'");
                                q.AppendLine("			END");
                                q.AppendLine("	    else accountName end as accountName");
                                q.AppendLine("       ,SUM(endBal*yld)/ nullif (sum(endBal), 0) as endBal");
                                q.AppendLine("       ,sum(EndBal) as summedBal");
                                q.AppendLine("FROM");
                                q.AppendLine("       ( ");
                                q.AppendLine("         (SELECT");
                                q.AppendLine("              alp13.code");
                                q.AppendLine("              ,isnull(alp13.yld, 0) as yld");
                                q.AppendLine("              ,alp1.endBal");
                                if (monthly)
                                {
                                    q.AppendLine(",alp1.month");
                                }
                                else
                                {
                                    q.AppendLine("		,isnull(alp13.month, alp1.month) as month");
                                    //q.AppendLine(",qrtGroup as month");
                                }
                                q.AppendLine("              ,aa.name as accountName");
                                q.AppendLine("              ,alb.isAsset");
                                q.AppendLine("              ,ao.[order] as accountOrder");
                                q.AppendLine("              FROM");
                                q.AppendLine("         (SELECT code");
                                if (taxEquiv)
                                {
                                    q.AppendLine("	,TEndTEYLD as yld");
                                }
                                else
                                {
                                    q.AppendLine("	,TotalYieldEnd as yld");
                                }
                                q.AppendLine("              , CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END as month");
                                q.AppendLine("       ,SimulationId");
                                q.AppendLine("       ,scenario");
                                if (!monthly)
                                {
                                    //q.AppendLine(", " + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                }
                                q.AppendLine("       FROM Basis_ALP13");
                                q.AppendLine("       WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                                q.AppendLine(monthFilter);
                                if (!monthly)
                                {
                                    q.AppendLine(" and datepart(m, CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END) in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine("       )as alp13");
                                q.AppendLine("       FULL OUTER JOIN");
                                q.AppendLine("       (SELECT ");
                                q.AppendLine("EndBal as endBal");
                                q.AppendLine(", simulationId, Scenario, code, CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END as month from Basis_ALP1 as alp1 where simulationid = " + simulationId + " AND Scenario = '" + scenario + "' " + monthFilter);
                                if (!monthly)
                                {
                                    q.AppendLine(" and datepart(m, CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END) in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine(") as alp1");
                                q.AppendLine("       on alp1.SimulationId = alp13.simulationId and alp1.scenario = alp13.scenario and alp1.code = alp13.Code and alp1.month = alp13.month");
                                q.AppendLine("         INNER JOIN Basis_ALB AS alb on ISNULL(alp13.code, alp1.code) = alb.code and ISNULL(alp13.simulationid,alp1.SimulationId) = alb.simulationid");
                                q.AppendLine("              INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("              aa.Id = alb.acc_typeOR");
                                q.AppendLine("              INNER JOIN ");
                                q.AppendLine("              ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("              alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("         WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("         UNION ALL");
                                q.AppendLine("         (SELECT");
                                q.AppendLine("              ala.code");
                                q.AppendLine("              ,ala.yld");
                                q.AppendLine("              ,ala.endBal as endBal");
                                q.AppendLine("              ,ala.month");
                                q.AppendLine("              ,aa.name as accountName");
                                q.AppendLine("              ,alb.isAsset");
                                q.AppendLine("              ,ao.[order] as accountOrder");
                                q.AppendLine("         FROM");
                                q.AppendLine("         (SELECT code, ");
                                if (taxEquiv)
                                {
                                    q.AppendLine("Total_End_TEYLD as yld, ");
                                }
                                else
                                {
                                    q.AppendLine("Total_End_Rate as yld, ");
                                }
                                q.AppendLine("End_Bal as endBal , ");
                                q.AppendLine("month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId);
                                q.AppendLine("AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                                q.AppendLine("         INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("         INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("              aa.Id = alb.acc_typeOR");
                                q.AppendLine("              INNER JOIN ");
                                q.AppendLine("              ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("              alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("         WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("       ) as alp");
                                q.AppendLine("Group by month, IsAsset, accountName, accountOrder");
                                q.AppendLine("Order by isAsset desc, accountOrder");
                                break;
                            case 2://classifications
                                q.AppendLine("SELECT");
                                q.AppendLine("       month");
                                q.AppendLine("       ,isAsset");
                                q.AppendLine(",CASE WHEN accountName = 'Other' then");
                                q.AppendLine("			CASE WHEN isAsset = 1 then 'Other Asset'");
                                q.AppendLine("			ELSE 'Other Liability'");
                                q.AppendLine("			END");
                                q.AppendLine("	    else accountName end as accountName");
                                q.AppendLine("       ,classificationName");
                                q.AppendLine("       ,SUM(endBal*yld)/ nullif (sum(endBal), 0) as endBal");
                                q.AppendLine("       ,sum(EndBal) as summedBal");
                                q.AppendLine("       ,classificationOrder");
                                q.AppendLine("FROM");
                                q.AppendLine("       ( ");
                                q.AppendLine("         (SELECT");
                                q.AppendLine("              alp13.code");
                                q.AppendLine("              ,isnull(alp13.yld, 0) as yld");
                                q.AppendLine("              ,alp1.endBal");
                                q.AppendLine("          , isnull(alp13.month, alp1.month) as month");
                                //if (monthly)
                                //{
                                //    q.AppendLine(",alp1.month");
                                //}
                                //else
                                //{
                                //    q.AppendLine(",qrtGroup as month");
                                //}

                                q.AppendLine("              ,aa.name as accountName");
                                q.AppendLine("              ,ac.name as classificationName");
                                q.AppendLine("              ,alb.isAsset");
                                q.AppendLine("              ,ao.[order] as accountOrder");
                                q.AppendLine("              ,co.[order] as classificationOrder");
                                q.AppendLine("              ");
                                q.AppendLine("              FROM");
                                q.AppendLine("         (SELECT code");
                                if (taxEquiv)
                                {
                                    q.AppendLine("	,TEndTEYLD as yld");
                                }
                                else
                                {
                                    q.AppendLine("	,TotalYieldEnd as yld");
                                }
                                q.AppendLine("              , CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END as month");
                                q.AppendLine("       ,SimulationId");
                                q.AppendLine("       ,scenario");
                                //if (!monthly)
                                //{
                                //    q.AppendLine(", " + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                //}
                                q.AppendLine("       FROM Basis_ALP13");
                                q.AppendLine("       ");
                                q.AppendLine("       WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");
                                q.AppendLine(monthFilter);
                                if (!monthly)
                                {
                                    q.AppendLine("and datepart(m, CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END) in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine("       )as alp13");
                                q.AppendLine("");
                                q.AppendLine("       FULL OUTER JOIN");
                                q.AppendLine("       (SELECT ");
                                q.AppendLine("EndBal as endBal");
                                q.AppendLine(", simulationId, Scenario, code");
                                q.AppendLine(", CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END as month");
                                q.AppendLine("from Basis_ALP1 as alp1 where simulationid = " + simulationId + " AND Scenario = '" + scenario + "'        AND CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END <= DATEADD(m, " + (monthly ? "24" : "60") + ", '" + aod + "')");
                                if (!monthly)
                                {
                                    q.AppendLine("and datepart(m,CASE WHEN nmonths = 1 then month ELSE DATEADD(m, nmonths-1, month) END) in (" + GetQuarterlyMonths(DateTime.Parse(aod).Month) + ")");
                                }
                                q.AppendLine(") as alp1");
                                q.AppendLine("       on alp1.SimulationId = alp13.simulationId and alp1.scenario = alp13.scenario and alp1.code = alp13.Code and alp1.month = alp13.month");
                                q.AppendLine("         INNER JOIN Basis_ALB AS alb on ISNULL(alp13.code, alp1.code) = alb.code and ISNULL(alp13.simulationid,alp1.SimulationId) = alb.simulationid");
                                q.AppendLine("              INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("              aa.Id = alb.acc_typeOR");
                                q.AppendLine("              INNER JOIN");
                                q.AppendLine("              ATLAS_Classification as ac on ");
                                q.AppendLine("              alb.IsAsset = ac.IsAsset and alb.ClassOR = ac.RbcTier and alb.acc_typeOR = ac.AccountTypeId");
                                q.AppendLine("              INNER JOIN ");
                                q.AppendLine("              ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("              alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("              INNER JOIN ");
                                q.AppendLine("              ATLAS_ClassificationOrder as co on");
                                q.AppendLine("              alb.Acc_TypeOR = co.Acc_Type and alb.ClassOR = co.RBC_Tier");
                                q.AppendLine("         WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("         UNION ALL");
                                q.AppendLine("         (SELECT");
                                q.AppendLine("              ala.code");
                                q.AppendLine("              ,ala.yld");
                                q.AppendLine("              ,ala.endBal as endBal");
                                q.AppendLine("              ,ala.month");
                                q.AppendLine("              ,aa.name as accountName");
                                q.AppendLine("              ,ac.name as classificationName");
                                q.AppendLine("              ,alb.isAsset");
                                q.AppendLine("              ,ao.[order] as accountOrder");
                                q.AppendLine("              ,co.[order] as classificationOrder");
                                q.AppendLine("         FROM");
                                q.AppendLine("         (SELECT code, ");
                                if (taxEquiv)
                                {
                                    q.AppendLine("Total_End_TEYLD as yld, ");
                                }
                                else
                                {
                                    q.AppendLine("Total_End_Rate as yld, ");
                                }
                                q.AppendLine("End_Bal as endBal , ");
                                q.AppendLine("month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId);
                                q.AppendLine("AND month <='" + aod + "' AND month > DATEADD(m, -1, '" + aod + "') ) as ala");
                                q.AppendLine("         INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("         INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("              aa.Id = alb.acc_typeOR");
                                q.AppendLine("         INNER JOIN");
                                q.AppendLine("              ATLAS_Classification as ac on ");
                                q.AppendLine("              alb.IsAsset = ac.IsAsset and alb.ClassOR = ac.RbcTier and alb.acc_typeOR = ac.AccountTypeId              ");
                                q.AppendLine("              INNER JOIN ");
                                q.AppendLine("              ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("              alb.Acc_TypeOR = ao.Acc_Type and alb.IsAsset = ao.IsAsset");
                                q.AppendLine("              INNER JOIN ");
                                q.AppendLine("              ATLAS_ClassificationOrder as co on");
                                q.AppendLine("              alb.Acc_TypeOR = co.Acc_Type and alb.ClassOR = co.RBC_Tier");
                                q.AppendLine("         WHERE alb.cat_Type in (0,1,5))");
                                q.AppendLine("       ) as alp");
                                q.AppendLine("Group by month, IsAsset, accountName, accountOrder, classificationName, classificationOrder");
                                q.AppendLine("Order by isAsset desc, accountOrder, classificationOrder");

                                break;
                        }
                    }
                    break;
                case 2: //prepayments
                    q.AppendLine("SELECT ");
                    q.AppendLine("	Sequence");
                    q.AppendLine("	,Name");
                    q.AppendLine("	,albs.code");
                    q.AppendLine("	--,albs.simulationId");
                    q.AppendLine("	--,albs.Scenario");
                    if (monthly)
                    {
                        q.AppendLine("	,cast(alps.month as datetime) as month");
                    }
                    else
                    {
                        q.AppendLine("	,qrtGroup as month");
                    }
                    q.AppendLine("	--,PayEPrePay");
                    q.AppendLine("	--,PayNPrepay");
                    q.AppendLine("	,CASE WHEN " + (monthly ? "eSpeed" : "sum(eSpeed)") + " is null then 'New' ELSE 'Exist' END as NEString");
                    q.AppendLine("	,'('+CASE WHEN " + (monthly ? "eSpeed" : "sum(eSpeed)") + " is null THEN");
                    q.AppendLine("		CASE ");
                    q.AppendLine("		   WHEN PayNPrepay = 0 THEN 'None'");
                    q.AppendLine("		   WHEN PayNPrepay = 1 THEN 'User Entered'");
                    q.AppendLine("		   WHEN PayNPrepay = 2 THEN 'Prepay Index'");
                    q.AppendLine("		   WHEN PayNPrepay = 3 THEN 'Prepay Matrix'");
                    q.AppendLine("		   ELSE 'None'");
                    q.AppendLine("		END");
                    q.AppendLine("	ELSE");
                    q.AppendLine("		CASE ");
                    q.AppendLine("		   WHEN PayEPrepay = 0 THEN 'None'");
                    q.AppendLine("		   WHEN PayEPrepay = 1 THEN 'User Entered'");
                    q.AppendLine("		   WHEN PayEPrepay = 2 THEN 'Prepay Index'");
                    q.AppendLine("		   WHEN PayEPrepay = 3 THEN 'BK'");
                    q.AppendLine("		   WHEN PayEPrepay = 4 THEN 'Decay'");
                    q.AppendLine("		   WHEN PayEPrepay = 5 THEN 'Prepay Matrix'");
                    q.AppendLine("		   WHEN PayEPrepay = 6 THEN 'ILP'");
                    q.AppendLine("		   ELSE 'None'");
                    q.AppendLine("		END");
                    q.AppendLine("	END + ')'");
                    q.AppendLine("	AS Calc");
                    q.AppendLine("	,CASE WHEN " + (monthly ? "eSpeed" : "sum(eSpeed)") + " is null THEN");
                    q.AppendLine("		CASE ");
                    q.AppendLine("		   WHEN NewPCType = 0 then 'PSA'");
                    q.AppendLine("		   WHEN NewPCType = 1 then 'CPR'");
                    q.AppendLine("		   ELSE 'None'");
                    q.AppendLine("		END");
                    q.AppendLine("	ELSE");
                    q.AppendLine("		CASE ");
                    q.AppendLine("		   WHEN ExistPCType = 0 then 'PSA'");
                    q.AppendLine("		   WHEN ExistPCType = 1 then 'CPR'");
                    q.AppendLine("		   ELSE 'None'");
                    q.AppendLine("		END ");
                    q.AppendLine("	END AS PCType");
                    q.AppendLine("	--,sum(eSpeed) as eSpeed");
                    q.AppendLine("	--,sum(eBal)");
                    q.AppendLine("	--,sum(nSpeed) as nSpeed");
                    q.AppendLine("	--,sum(nBal)");
                    q.AppendLine("  ,isnull(eSpeed, nSpeed) as endBal");
                    q.AppendLine(" FROM");
                    q.AppendLine("(select");
                    q.AppendLine("	sequence");
                    q.AppendLine("	,alb.name");
                    q.AppendLine("	,alb.code");
                    q.AppendLine("	,alb.simulationId");
                    q.AppendLine("	,als.Scenario");
                    q.AppendLine("	,PayEPrePay");
                    q.AppendLine("	,PayNPrepay");
                    q.AppendLine("	,ExistPCType");
                    q.AppendLine("	,NewPCType");
                    q.AppendLine("	from ");
                    q.AppendLine("	(select sequence,name,simulationId, code, exclude from basis_alb where SimulationId = " + simulationId + ") as alb");
                    q.AppendLine("	inner join");
                    q.AppendLine("	(select scenario, code, PayNPrepay, PayEPrePay, SimulationId, ExistPCType, NewPCType from basis_als where scenario = '" + scenario + "' and (PayEPrePay <> 0 or PayNPrePay <> 0)) as als");
                    q.AppendLine("	on alb.code = als.code and alb.SimulationId = als.SimulationId");
                    q.AppendLine(") albs");
                    q.AppendLine("inner join");
                    q.AppendLine("(SELECT ");
                    q.AppendLine("  code");
                    q.AppendLine("  ,simulationId");
                    if (monthly)
                    {
                        q.AppendLine("  ,month");
                    }
                    else
                    {
                        q.AppendLine(", " + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                    }
                    q.AppendLine(",scenario");
                    q.AppendLine(",eSpeed");
                    q.AppendLine(",eBal");
                    q.AppendLine(",nSpeed");
                    q.AppendLine(",nBal");
                    q.AppendLine("FROM");
                    q.AppendLine("(");
                    q.AppendLine("		SELECT");
                    q.AppendLine("	       ISNULL(alp11.code, alp5.code) as code");
                    q.AppendLine("	       ,ISNULL(alp11.simulationId, alp5.simulationid) as simulationId");
                    q.AppendLine("	       ,ISNULL(alp11.month, alp5.month) as month");
                    q.AppendLine("		   ,ISNULL(alp11.scenario, alp5.scenario) as scenario");
                    q.AppendLine("	       ,alp5.PAyEPSACPR as eSpeed");
                    q.AppendLine("	       ,alp5.PayEBalX as eBal");
                    q.AppendLine("	       ,alp11.PAyNPSACPR as nSpeed");
                    q.AppendLine("	       ,alp11.PayNBalX as nBal");
                    q.AppendLine("	       FROM");
                    q.AppendLine("	(select code, scenario, simulationID, month, PAyEPSACPR, PayEBalX  from ");
                    q.AppendLine("		(select");
                    q.AppendLine("			alp5.code, scenario, simulationId, sum(PAyEPSACPR) as PAyEPSACPR, sum(PayEBalX) as PayEBalX, month ");
                    q.AppendLine("			from Basis_ALP5 as alp5 ");
                    q.AppendLine("          inner join (select code from");
                    q.AppendLine("              (select code,sum(PAyEPSACPR) as summedspeeed, sum(PayEBalX) as summedBal from basis_alp5 where simulationId = " + simulationId + " and scenario = '" + scenario + "' group by code) as filter");
                    q.AppendLine("              where summedBal <> 0 and summedspeeed <> 0) as filterTbl on alp5.code = filterTbl.Code");
                    q.AppendLine("          WHERE  simulationId = " + simulationId + " AND scenario = '" + scenario + "' " + monthFilter + " group by simulationId, scenario, alp5.code, month) as a");
                    q.AppendLine("	) as alp5");
                    q.AppendLine("	FULL OUTER JOIN ");
                    q.AppendLine("	(select code, scenario, simulationID, month, PAyNPSACPR, PayNBalX  from ");
                    q.AppendLine("		(select");
                    q.AppendLine("			alp11.code, scenario, simulationId, sum(PAyNPSACPR) as PAyNPSACPR, sum(PayNBalX) as PayNBalX, month ");
                    q.AppendLine("			from Basis_ALP11 as alp11 ");
                    q.AppendLine("          inner join (select code from");
                    q.AppendLine("              (select code,sum(PAyNPSACPR) as summedspeeed, sum(PayNBalX) as summedBal from basis_alp11 where simulationId = " + simulationId + " and scenario = '" + scenario + "' group by code) as filter");
                    q.AppendLine("              where summedBal <> 0 and summedspeeed <> 0) as filterTbl on alp11.code = filterTbl.Code");
                    q.AppendLine("WHERE  simulationId = " + simulationId + " AND scenario = '" + scenario + "' " + monthFilter + " group by simulationId, scenario, alp11.code, month) as a");
                    q.AppendLine("	) as alp11");
                    q.AppendLine("	ON alp5.simulationId = alp11.simulationId AND alp5.scenario = alp11.scenario AND alp5.month = alp11.month AND alp5.code = alp11.code");
                    q.AppendLine(") as tbl");
                    q.AppendLine(") as alps");
                    q.AppendLine("on albs.code = alps.code and albs.SimulationId = alps.simulationId and albs.scenario = alps.scenario ");
                    q.AppendLine("group by " + (monthly ? "month" : "qrtGroup") + ", sequence, Name, albs.code, albs.SimulationId, albs.Scenario,PayEPrePay, PayNPrepay, albs.NewPCType, albs.ExistPCType");
                    if (monthly)
                    {
                        q.Append(", eSpeed, nSpeed");
                    }
                    q.AppendLine(", eSpeed, nSpeed order by  Sequence,cast(" + (monthly ? "month" : "qrtGroup") + " as datetime)");

                    break;
                case 3://interest income expense
                    if (grouping.IndexOf("dg") > -1)
                    {
                        q.AppendLine("SELECT");
                        q.AppendLine("	month");
                        q.AppendLine("	,isAsset");
                        q.AppendLine("	,accountName");
                        q.AppendLine("	,sum(endBal) as endBal");
                        q.AppendLine("	,accountOrder");
                        q.AppendLine("FROM");
                        q.AppendLine("(");
                        q.AppendLine("  (SELECT");
                        q.AppendLine("	alp.code");
                        q.AppendLine("	,alp.endBal");
                        q.AppendLine("	,alp.month");
                        q.AppendLine("	,dgs.name as accountName");
                        q.AppendLine("	,alb.isAsset");
                        q.AppendLine("	,dgs.[Priority] as accountOrder");
                        q.AppendLine("	FROM");
                        q.AppendLine("  (SELECT");
                        q.AppendLine("	alp.code	");
                        q.AppendLine("	,sum(alp.endBal) as endBal");
                        if (monthly)
                        {
                            q.AppendLine(",max(month) as month");
                        }
                        else
                        {
                            q.AppendLine("	,qrtGroup as month");
                        }
                        q.AppendLine("	,simulationid");
                        q.AppendLine("	FROM");
                        q.AppendLine("  (SELECT code");
                        q.AppendLine("	, " + alpVolumeField + " as endBal");
                        q.AppendLine("	, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                        q.AppendLine("	, SimulationId ");
                        if (!monthly)
                        {
                            q.AppendLine("	," + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                        }
                        q.AppendLine("	FROM Basis_ALP13 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "' ");
                        q.AppendLine(monthFilter + ") as alp");
                        q.AppendLine("GROUP BY code");
                        if (monthly)
                        {
                            q.AppendLine(",month");
                        }
                        else
                        {
                            q.AppendLine(",qrtGroup");
                        }
                        q.AppendLine(", simulationid");
                        q.AppendLine(")as alp");
                        q.AppendLine("  INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                        q.AppendLine(" INNER JOIN ");
                        q.AppendLine("			(SELECT  ");
                        q.AppendLine("				dgg.name ");
                        q.AppendLine("				,dgg.[Priority] ");
                        q.AppendLine("				,dgc.IsAsset  ");
                        q.AppendLine("				,dgc.Acc_Type  ");
                        q.AppendLine("				,dgc.RbcTier  ");
                        q.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                        q.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                        q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                        q.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                        q.AppendLine("			  WHERE dg.id =" + grouping.Replace("dg_", "") + ") as dgs ON ");
                        q.AppendLine("			  dgs.acc_Type = alb.acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
                        q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.Cat_Type = 1 OR alb.cat_Type = 5)");
                       q.AppendLine("  UNION ALL");
                       q.AppendLine("  (SELECT");
                       q.AppendLine("    ala.code");
                       q.AppendLine("    ,ala.endBal");
                       q.AppendLine("    ,ala.month");
                       q.AppendLine("	,dgs.name as accountName");
                       q.AppendLine("	,alb.isAsset");
                       q.AppendLine("  ,dgs.[Priority] as accountOrder");
                       q.AppendLine("  FROM");
                       q.AppendLine("  (SELECT code, " + alaVoumeField + " as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId + " AND month > DATEADD(m, -1, '" + aod + "'))as ala");
                       q.AppendLine("  INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                       q.AppendLine("   INNER JOIN ");
                       q.AppendLine("			(SELECT  ");
                       q.AppendLine("				dgg.name ");
                       q.AppendLine("				,dgg.[Priority] ");
                       q.AppendLine("				,dgc.IsAsset  ");
                       q.AppendLine("				,dgc.Acc_Type  ");
                       q.AppendLine("				,dgc.RbcTier  ");
                       q.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                       q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                       q.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                       q.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                       q.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                       q.AppendLine("			  WHERE dg.id = " + grouping.Replace("dg_", "") + ") as dgs ON ");
                       q.AppendLine("			  dgs.acc_Type = alb.acc_TypeOR AND dgs.rbcTier = alb.ClassOR AND dgs.IsAsset = alb.isAsset");
                       q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.Cat_Type = 1 OR alb.cat_Type = 5)");
                        q.AppendLine("  )");
                        q.AppendLine("as alp");
                        q.AppendLine("Group by ");
                        q.AppendLine("month");
                        q.AppendLine(", IsAsset");
                        q.AppendLine(", accountName");
                        q.AppendLine(" ,accountName");
                        q.AppendLine(", accountOrder");
                        q.AppendLine("Order by ");
                        q.AppendLine("isAsset desc");
                        q.AppendLine(" ,max(accountOrder)");

                    }
                    else
                    {
                        switch (Int32.Parse(grouping))
                        {
                            case 0://sub accounts
                                q.AppendLine("SELECT");
                                q.AppendLine("  alb.name");
                                q.AppendLine("  ,alb.sequence");
                                q.AppendLine("  ,alb.Cat_Type");
                                q.AppendLine("  ,alp.month");
                                q.AppendLine("  ,alp.endBal");
                                q.AppendLine("FROM");
                                q.AppendLine("(SELECT name, code, sequence, cat_Type FROM Basis_ALB WHERE SimulationId = " + simulationId + " AND cat_type <> 6) as alb");
                                q.AppendLine(" LEFT JOIN ");
                                q.AppendLine("( ");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("	alp.code");
                                q.AppendLine("	,alp.endBal");
                                q.AppendLine("	,alp.month");
                                q.AppendLine("	FROM");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("	alp.code	");
                                q.AppendLine("	,sum(alp.endBal) as endBal");
                                if (monthly)
                                {
                                    q.AppendLine(",max(month) as month");
                                }
                                else
                                {
                                    q.AppendLine("	,qrtGroup as month");
                                }
                                q.AppendLine("	,simulationid");
                                q.AppendLine("	FROM");
                                q.AppendLine("  (SELECT code");
                                q.AppendLine("	, " + alpVolumeField + " as endBal");
                                q.AppendLine("	, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                                q.AppendLine("	, SimulationId ");

                                if (!monthly)
                                {
                                    q.AppendLine("	," + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                }

                                q.AppendLine("	FROM Basis_ALP13 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "'");

                                q.AppendLine(monthFilter + ") as alp");

                                q.AppendLine("GROUP BY code");

                                if (monthly)
                                {
                                    q.AppendLine(",month");
                                }
                                else
                                {
                                    q.AppendLine(",qrtGroup");
                                }

                                q.AppendLine(", simulationid");
                                q.AppendLine(")as alp");
                                q.AppendLine("  RIGHT JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.cat_type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine("  ");
                                q.AppendLine("  UNION ALL");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("    ala.code");
                                q.AppendLine("    ,ala.endBal");
                                q.AppendLine("    ,ala.month");
                                q.AppendLine("  FROM");
                                q.AppendLine("  (SELECT code, " + alaVoumeField + " as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId + " AND month <= '" + aod + "' AND  month > DATEADD(m, -1, '" + aod + "'))as ala");
                                q.AppendLine("  RIGHT JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("  WHERE alb.cat_Type IN (0,1,5))");
                                q.AppendLine(")as alp");
                                q.AppendLine("ON alp.code = alb.code ");
                                q.AppendLine(" WHERE alp.month IS NOT NULL ");
                                q.AppendLine("ORDER BY alb.Sequence, alp.Month");
                                break;
                            case 3://account types

                                q.AppendLine("SELECT");
                                q.AppendLine("	month");
                                q.AppendLine("	,isAsset");
                                q.AppendLine("	,accountName");
                                q.AppendLine("	,sum(endBal) as endBal");
                                q.AppendLine("	,accountOrder");
                                q.AppendLine("FROM");
                                q.AppendLine("(");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("	alp.code");
                                q.AppendLine("	,alp.endBal");
                                q.AppendLine("	,alp.month");
                                q.AppendLine("	,aa.name as accountName");
                                q.AppendLine("	,alb.isAsset");
                                q.AppendLine("	,ao.[order] as accountOrder");
                                q.AppendLine("	FROM");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("	alp.code	");
                                q.AppendLine("	,sum(alp.endBal) as endBal");
                                if (monthly)
                                {
                                    q.AppendLine(",max(month) as month");
                                }
                                else
                                {
                                    q.AppendLine("	,qrtGroup as month");
                                }

                                q.AppendLine("	,simulationid");
                                q.AppendLine("	FROM");
                                q.AppendLine("  (SELECT code");
                                q.AppendLine("	, " + alpVolumeField + " as endBal");
                                q.AppendLine("	, CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                                q.AppendLine("	, SimulationId ");
                                if (!monthly)
                                {
                                    q.AppendLine("	," + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                }
                                q.AppendLine("	FROM Basis_ALP13 WHERE simulationid = " + simulationId + " AND Scenario = '" + scenario + "' ");
                                q.AppendLine(monthFilter + ") as alp");
                                q.AppendLine("GROUP BY code");
                                if (monthly)
                                {
                                    q.AppendLine(",month");
                                }
                                else
                                {
                                    q.AppendLine(",qrtGroup");
                                }
                                q.AppendLine(", simulationid");
                                q.AppendLine(")as alp");
                                q.AppendLine("  INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                                q.AppendLine("  INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("	aa.Id = alb.acc_typeOR");
                                q.AppendLine("  INNER JOIN ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("  ao.acc_type = alb.acc_typeOR");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.cat_type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine("  UNION ALL");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("    ala.code");
                                q.AppendLine("    ,ala.endBal");
                                q.AppendLine("    ,ala.month");
                                q.AppendLine("	,aa.name as accountName");
                                q.AppendLine("	,alb.isAsset");
                                q.AppendLine("  ,ao.[order] as accountOrder");
                                q.AppendLine("  FROM");
                                q.AppendLine("  (SELECT code, " + alaVoumeField + " as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId + " AND month > DATEADD(m, -1, '" + aod + "'))as ala");
                                q.AppendLine("  INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("  INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("		aa.Id = alb.acc_typeOR");
                                q.AppendLine("		INNER JOIN ");
                                q.AppendLine("		ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("		alb.Acc_TypeOR = ao.Acc_Type");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.cat_type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine("  )");
                                q.AppendLine("as alp");
                                q.AppendLine("Group by ");
                                q.AppendLine("month");
                                q.AppendLine(", IsAsset");
                                q.AppendLine(", accountName");
                                q.AppendLine(" ,accountName");
                                q.AppendLine(", accountOrder");
                                q.AppendLine("Order by ");
                                q.AppendLine("isAsset desc");
                                q.AppendLine(" ,max(accountOrder)");

                                break;
                            case 2://classifications
                                q.AppendLine("SELECT");
                                q.AppendLine("month");
                                q.AppendLine(",isAsset");
                                q.AppendLine(",accountName");
                                q.AppendLine(",accountOrder");
                                q.AppendLine(",classificationName");
                                q.AppendLine(",sum(endBal) as endBal");
                                q.AppendLine(",classificationOrder");
                                q.AppendLine("FROM");
                                q.AppendLine("(");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("alp.code");
                                q.AppendLine(",alp.endBal");
                                q.AppendLine(",alp.month");
                                q.AppendLine(",ac.name as classificationName");
                                q.AppendLine(",alb.isAsset");
                                q.AppendLine(",co.[order] as classificationOrder");
                                q.AppendLine(",aa.name as accountName");
                                q.AppendLine(",ao.[order] as accountOrder");
                                q.AppendLine("FROM");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("alp.code");
                                q.AppendLine(",sum(alp.endBal) as endBal");
                                if (monthly)
                                {
                                    q.AppendLine(",max(month) as month");
                                }
                                else
                                {
                                    q.AppendLine("	,qrtGroup as month");
                                }
                                q.AppendLine(",simulationid");
                                q.AppendLine("FROM");
                                q.AppendLine("  (SELECT code");
                                q.AppendLine(", " + alpVolumeField + " as endBal");
                                q.AppendLine(", CASE WHEN nmonths = 1 then month ELSE DATEADD(d, -1, DATEADD(m, nmonths, month)) END as month");
                                q.AppendLine(", SimulationId ");
                                if (!monthly)
                                {
                                    q.AppendLine("	," + CreateQuartleryBuckets(DateTime.Parse(aod).Month) + " as qrtGroup");
                                }
                                q.AppendLine("FROM Basis_ALP13 WHERE simulationid = " + simulationId + " AND Scenario = '000' ");
                                q.AppendLine(monthFilter + ") as alp");
                                q.AppendLine("GROUP BY code");
                                if (monthly)
                                {
                                    q.AppendLine(",month");
                                }
                                else
                                {
                                    q.AppendLine(",qrtGroup");
                                }
                                q.AppendLine(", simulationid");
                                q.AppendLine(")as alp");
                                q.AppendLine("  INNER JOIN Basis_ALB AS alb on alp.code = alb.code and alp.simulationid = alb.simulationid");
                                q.AppendLine("  INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("aa.Id = alb.acc_typeOR");
                                q.AppendLine("INNER JOIN ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("ao.Acc_Type = alb.Acc_TypeOR and ao.IsAsset = alb.IsAsset");
                                q.AppendLine("INNER JOIN");
                                q.AppendLine("ATLAS_Classification as ac on ");
                                q.AppendLine("alb.IsAsset = ac.IsAsset and alb.ClassOR = ac.RbcTier and alb.acc_typeOR = ac.AccountTypeId");
                                q.AppendLine("  INNER JOIN ");
                                q.AppendLine("ATLAS_ClassificationOrder as co on");
                                q.AppendLine("alb.Acc_TypeOR = co.Acc_Type and alb.ClassOR = co.RBC_Tier");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.cat_type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine("  UNION ALL");
                                q.AppendLine("  (SELECT");
                                q.AppendLine("    ala.code");
                                q.AppendLine("    ,ala.endBal");
                                q.AppendLine("    ,ala.month");
                                q.AppendLine(",ac.name as classificationName");
                                q.AppendLine(",alb.isAsset");
                                q.AppendLine(",co.[order] as classificationOrder");
                                q.AppendLine(",aa.Name as accountName");
                                q.AppendLine(",ao.[order] as accountOrder");
                                q.AppendLine("  FROM");
                                q.AppendLine("  (SELECT code, " + alaVoumeField + " as endBal, month, SimulationId FROM Basis_ALA WHERE simulationid = " + simulationId + " AND month > DATEADD(m, -1, '" + aod + "'))as ala");
                                q.AppendLine("  INNER JOIN Basis_ALB AS alb on ala.code = alb.code and ala.simulationid = alb.simulationid");
                                q.AppendLine("  INNER JOIN ATLAS_AccountType as aa on");
                                q.AppendLine("aa.Id = alb.acc_typeOR");
                                q.AppendLine("  INNER JOIN ATLAS_AccountTypeOrder as ao on");
                                q.AppendLine("ao.Acc_Type = alb.acc_type and ao.IsAsset = alb.IsAsset");
                                q.AppendLine("  INNER JOIN");
                                q.AppendLine("ATLAS_Classification as ac on ");
                                q.AppendLine("alb.IsAsset = ac.IsAsset and alb.ClassOR = ac.RbcTier and alb.acc_typeOR = ac.AccountTypeId");
                                q.AppendLine("INNER JOIN ");
                                q.AppendLine("ATLAS_ClassificationOrder as co on");
                                q.AppendLine("alb.Acc_TypeOR = co.Acc_Type and alb.ClassOR = co.RBC_Tier");
                                q.AppendLine("  WHERE alb.cat_Type = 0 OR alb.cat_type = 1 OR alb.cat_Type = 5)");
                                q.AppendLine("  )");
                                q.AppendLine("as alp");
                                q.AppendLine("Group by ");
                                q.AppendLine("month");
                                q.AppendLine(", IsAsset");
                                q.AppendLine(",accountOrder");
                                q.AppendLine(",accountName");
                                q.AppendLine(", classificationOrder");
                                q.AppendLine(" ,classificationName");
                                q.AppendLine("Order by ");
                                //q.AppendLine("month");
                                q.AppendLine("isAsset desc");
                                q.AppendLine(",accountOrder");
                                q.AppendLine(",classificationOrder");

                                break;
                        }
                    }
                    break;
            }
            return q.ToString();
        }

        private DataTable SimulationSummaryTable(InstitutionService instService, int simulationTypeId, int scenarioTypeId, DateTime packageAsOfDate, int dateOffset, int reportSelection, string grouping, bool taxEquiv, string monthlyQuarterly)
        {
            //report order is Balance Sheet, Rate/Yield Analysis, Asset/Liability Prepayments, Interest Income/Expense
            string alpTable = "";
            string alpVolumeField = "";
            string alaVoumeField = "";
            DataTable retTable = new DataTable();

            switch (reportSelection)
            {
                case 0:
                    alpTable = "Basis_ALP1";
                    alpVolumeField = "EndBal";
                    alaVoumeField = "End_Bal";
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    alpTable = "Basis_ALP13";
                    alpVolumeField = taxEquiv ? "AveTEAmt" : "IEAmount";
                    alaVoumeField = taxEquiv ? "Total_Avg_TEAmt" : "IE";
                    break;

            }


            var obj = instService.GetSimulationScenario(packageAsOfDate, dateOffset, simulationTypeId, scenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();
            bool monthly = monthlyQuarterly.IndexOf("0") > -1;

            string query = GetQuery(reportSelection, grouping, monthly, asOfDate, taxEquiv, simulationId, scenario);
            retTable = Utilities.ExecuteSql(instService.ConnectionString(), query);

            return retTable;
        }
    }
}