﻿using System.Configuration;
using System.Web;

using Spire.License;

namespace Atlas.Web.Services
{
    public static class SpireLicenseLoader
    {
        private static bool __areLicensesLoaded;

        public static bool LoadLicense()
        {
            if (__areLicensesLoaded) return true;

            foreach(string key in new[] { "Spire.License.PDF", "Spire.License.DOC" })
            {
                string license = ConfigurationManager.AppSettings[key];

                if (string.IsNullOrWhiteSpace(license)) return false;

                LicenseProvider.SetLicenseKey(license);
                LicenseProvider.LoadLicense();
            }

            __areLicensesLoaded = true;

            return true;
        }
    }
}