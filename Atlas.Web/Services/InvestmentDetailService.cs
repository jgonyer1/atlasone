﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class InvestmentDetailService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object InvestmentDetailViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.InvestmentDetails.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("Investment Detail Not Found");



            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            var info = instService.GetInformation(rep.Package.AsOfDate, sr.AsOfDateOffset);
            string asOfDate = info.AsOfDate.ToShortDateString();
         
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	cusip ");
            sqlQuery.AppendLine(" 	,ParValue ");
            sqlQuery.AppendLine(" 	,BookValue ");
            sqlQuery.AppendLine(" 	,BookPrice ");
            sqlQuery.AppendLine(" 	,MarketPrice ");
            sqlQuery.AppendLine(" 	,basisintent ");
            sqlQuery.AppendLine(" 	,yield ");
            sqlQuery.AppendLine(" 	,CurrentCoupon ");
            sqlQuery.AppendLine(" 	,AssetClassSubCode  ");
            sqlQuery.AppendLine(" 	,BasisCallFlag ");
            sqlQuery.AppendLine(" 	,CASE ");
            sqlQuery.AppendLine(" 		WHEN BasisTaxFlag in ('None', 'NONE','Yes','Y','Yes-Fed', 'YFED', 'Yes-State', 'YST') THEN ");
            sqlQuery.AppendLine(" 			'Y' ");
            sqlQuery.AppendLine(" 		ELSE ");
            sqlQuery.AppendLine(" 			'N' ");
            sqlQuery.AppendLine(" 	END as taxable ");
            sqlQuery.AppendLine(" 	,CollateralType  ");
            sqlQuery.AppendLine(" 	,LoanAge ");
            sqlQuery.AppendLine(" 	,MaturityDate  ");
            sqlQuery.AppendLine(" 	,OriginalTerm ");
            sqlQuery.AppendLine(" 	,OriginationDate  ");
            sqlQuery.AppendLine(" 	,PaymentFreq  ");
            sqlQuery.AppendLine(" 	,PPHistCPR1Mo  ");
            sqlQuery.AppendLine(" 	,PPHistCPR3Mo  ");
            sqlQuery.AppendLine(" 	,PPHistCPR12Mo  ");
            sqlQuery.AppendLine(" 	,PPHistCPRLife  ");
            sqlQuery.AppendLine(" 	,SecuritySubType  ");
            sqlQuery.AppendLine(" 	,WAC ");
            sqlQuery.AppendLine(" 	,WAM ");
            sqlQuery.AppendLine(" 	,CASE	 ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType in ('Adjustable', 'A') THEN ");
            sqlQuery.AppendLine(" 			'Adjustable' ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType in ('COMP', 'Complex') THEN ");
            sqlQuery.AppendLine(" 			'Complex' ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('Fixed', 'F') THEN ");
            sqlQuery.AppendLine(" 			'Fixed' ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('None', 'NONE') THEN ");
            sqlQuery.AppendLine(" 			'None' ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('Step Up', 'SU') THEN  ");
            sqlQuery.AppendLine(" 			'Step Up' ");
            sqlQuery.AppendLine(" 		WHEN BasisCouponType IN ('Tax Credit', 'TC') THEN ");
            sqlQuery.AppendLine(" 			'Tax Credit' ");
            sqlQuery.AppendLine(" 		ELSE ");
            sqlQuery.AppendLine(" 			'Zero Coupon' ");
            sqlQuery.AppendLine(" 	END as couponType ");
            sqlQuery.AppendLine(" 	,masterIndex ");
            sqlQuery.AppendLine(" 	,mastermargin ");
            sqlQuery.AppendLine(" 	,masterDate ");
            sqlQuery.AppendLine(" 	,MasterResetFreq  ");
            sqlQuery.AppendLine(" 	,MasterPerCap ");
            sqlQuery.AppendLine(" 	,MasterLifeCap ");
            sqlQuery.AppendLine(" 	,MasterLifeFloor ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" FROM investment WHERE asOfDate = '" + asOfDate + "' AND (custom is null or custom = '') ORDER BY Cusip, parValue");

            DataTable tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());
            if (tblData.Rows.Count == 0) {
                errors.Add(String.Format("Investment data not found for As Of Date" + asOfDate));
            }
            return new
            {
                tblData = tblData,
                errors = errors,
                warnings = warnings
            };
        }
    }
}