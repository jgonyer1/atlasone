﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class LiabilityPricingAnalysisService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public object LiabilityPricingAnalysisViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var c = gctx.LiabilityPricingAnalysis.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (c == null) throw new Exception("Liability Pricing Analysis Not Found");

            var instService = new InstitutionService(c.InstitutionDatabaseName);

            InstitutionContext configContext = new InstitutionContext(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName));

            var curInfo = instService.GetInformation(rep.Package.AsOfDate, c.AsOfDateOffset);
            var compInfo = instService.GetInformation(rep.Package.AsOfDate, c.ComparativeDateOffset);

          
            var startDateHist = instService.GetInformation(rep.Package.AsOfDate, c.DepHisotryStartDate);
            var endDateHist = instService.GetInformation(rep.Package.AsOfDate, c.DepHistoryEndDate);

            DataTable marketRates = new DataTable();
            DataTable nonMatRates = new DataTable();
            DataTable timeDeps = new DataTable();
            DataTable treasury = new DataTable();
            DataTable brokered = new DataTable();
            DataTable fhlb = new DataTable();
            DataTable libor = new DataTable();
            DataTable histDeposit = new DataTable();
            DataTable existing = new DataTable();
            DataTable future = new DataTable();
            DataTable waterfall = new DataTable();
            var curPricing = configContext.LiabilityPricings.Where(s => s.AsOfDate == curInfo.AsOfDate ).FirstOrDefault();
            var compPricing = configContext.LiabilityPricings.Where(s => s.AsOfDate == compInfo.AsOfDate).FirstOrDefault();

            string curPricingDate = curPricing.WebRateAsOfDate;
            //if msot recent is checked off then go get the lastest date in web rates

            string dateLookUp = curInfo.AsOfDate.Month.ToString().PadLeft(2, '0') + "/" + curInfo.AsOfDate.Day.ToString().PadLeft(2, '0') + "/" + curInfo.AsOfDate.Year.ToString();

            string compDateLookUp = compInfo.AsOfDate.Month.ToString().PadLeft(2, '0') + "/" + compInfo.AsOfDate.Day.ToString().PadLeft(2, '0') + "/" + compInfo.AsOfDate.Year.ToString();

            if (c.MostRecentWebRate)
            {
                DataTable maxDate = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, "SELECT CONVERT(VARCHAR(10), MAX(asOfDate), 101) as asOfDate  FROM [Webrates].[dbo].[RateBaseData] ");
                if (maxDate.Rows.Count > 0)
                {
                    curPricingDate = maxDate.Rows[0][0].ToString();
                }
            }

            GlobalController gc = new GlobalController();

            Dictionary<string, DataTable> pols = gc.PolicyOffsets(c.InstitutionDatabaseName);

            if (!c.OverrideSimulationTypeId)
            {
                if (pols.ContainsKey(dateLookUp))
                {
                    c.SimulationTypeId = int.Parse(pols[dateLookUp].Rows[0]["bsSimulation"].ToString());
                }

            }

            if (!c.OverrideScenarioTypeId)
            {
                if (pols.ContainsKey(dateLookUp))
                {
                    c.ScenarioTypeId = int.Parse(pols[dateLookUp].Rows[0]["bsScenario"].ToString());
                }
            }


            if (!c.OverrideCompSimulationTypeId)
            {
                if (pols.ContainsKey(compDateLookUp))
                {
                    c.CompSimulationTypeId = int.Parse(pols[compDateLookUp].Rows[0]["bsSimulation"].ToString());
                }
            }

            if (!c.OverrideCompScenarioTypeId)
            {
                if (pols.ContainsKey(compDateLookUp))
                {
                    c.CompScenarioTypeId = int.Parse(pols[compDateLookUp].Rows[0]["bsScenario"].ToString());
                }
            }

            DataTable pol = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), "SELECT [NIISimulationTypeId] FROM Atlas_Policy WHERE asOfDate = '" + curInfo.AsOfDate.ToShortDateString() + "' ");

            if (curPricing == null) {
                errors.Add(String.Format("Pricing for As Of Date {0} could not be found", curInfo.AsOfDate.ToShortDateString()));
            }
            else if (compPricing == null) {
                errors.Add(String.Format("Pricing for As Of Date {0} could not be found", compInfo.AsOfDate.ToShortDateString()));
            }
            else if (pol.Rows.Count == 0)
            {
                errors.Add(String.Format("No Policy Found for As Of Date {0}", curInfo.AsOfDate.ToShortDateString()));
            }
            else {
                //Market Rates Data Pull
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	rt.name ");
                sqlQuery.AppendLine(" 	,os.iRate as curRate ");
                sqlQuery.AppendLine(" 	,comp.iRate as prRate ");
                sqlQuery.AppendLine(" 	,os.iRate - comp.iRate As rateDiff ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine("       [Code] ");
                sqlQuery.AppendLine("       ,[IndexType] ");
                sqlQuery.AppendLine(" 	  ,[Name] ");
                sqlQuery.AppendLine(" 		  FROM [RateInfo] where  ");
                sqlQuery.AppendLine(" 		  name in ('(006) LIBOR 1 Month' ");
                sqlQuery.AppendLine(" 						,'(010) CMT 1 Month' ");


                switch (curPricing.FHLBDistrict) {
                    case "Atlanta":
                        sqlQuery.AppendLine(" ,'(049) FHLB Atlanta 1 Month' ");
                        sqlQuery.AppendLine(" ,'(051) FHLB Atlanta 3 Month' ");

                        break;
                    case "Boston":
                        sqlQuery.AppendLine(" ,'(025) FHLB Boston 1 Month' ");
                        sqlQuery.AppendLine(" ,'(027) FHLB Boston 3 Month' ");
                        break;
                    case "Chicago":
                        sqlQuery.AppendLine(" ,'(062) FHLB Chicago 3 Month' ");
                        sqlQuery.AppendLine(" ,'(060) FHLB Chicago 1 Month' ");
                        break;
                    case "Cincinnati":

                        sqlQuery.AppendLine(" ,'(071) FHLB Cincinnati 1 Month'  ");
                        sqlQuery.AppendLine(" ,'(073) FHLB Cincinnati 3 Month' ");
                        break;
                    case "Dallas":
                        sqlQuery.AppendLine(" ,'(084) FHLB Dallas 3 Month' ");
                        sqlQuery.AppendLine(" ,'(082) FHLB Dallas 1 Month' ");
                        break;
                    case "Des Moines":
                        sqlQuery.AppendLine(" ,'(095) FHLB Des Moines 3 Month' ");
                        sqlQuery.AppendLine(" ,'(093) FHLB Des Moines 1 Month' ");
                        break;
                    case "Indianpolis":
                        sqlQuery.AppendLine(" ,'(105) FHLB Indianapolis 3 Month' ");
                        sqlQuery.AppendLine(" ,'(103) FHLB Indianapolis 1 Month' ");
                        break;
                    case "New York":
                        sqlQuery.AppendLine(" ,'(037) FHLB NY 3 Month' ");
                        sqlQuery.AppendLine(" ,'(035) FHLB NY 1 Month' ");
                        break;
                    case "Pittsburgh":
                        sqlQuery.AppendLine(" ,'(116) FHLB Pittsburgh 3 Month' ");
                        sqlQuery.AppendLine(" ,'(114) FHLB Pittsburgh 1 Month' ");
                        break;
                    case "San Francisco":
                        sqlQuery.AppendLine(" ,'(127) FHLB San Francisco 3 Month' ");
                        sqlQuery.AppendLine(" ,'(125) FHLB San Francisco 1 Month' ");
                        break;
                    case "Seattle":
                        sqlQuery.AppendLine(" ,'(137) FHLB Seattle 3 Month' ");
                        sqlQuery.AppendLine(" ,'(135) FHLB Seattle 1 Month' ");
                        break;
                    case "Topeka":
                        sqlQuery.AppendLine(" ,'(148) FHLB Topeka 3 Month' ");
                        sqlQuery.AppendLine(" ,'(146) FHLB Topeka 1 Month' ");
                        break;
                }

                sqlQuery.AppendLine(" 						,'(007) LIBOR 3 Month' ");
                sqlQuery.AppendLine(" 						,'(011) CMT 3 Month' ");
                sqlQuery.AppendLine(" 						,'(156) Brokered CD''s (FNC) 3 Month' ");
                sqlQuery.AppendLine(" 						,'(175) IBC Data Money Fund'");
                sqlQuery.AppendLine(" 						,'(002) FedFunds' ");
                sqlQuery.AppendLine(" 						)) as rt ");
                sqlQuery.AppendLine(" 			INNER JOIN  ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + compPricing.WebRateAsOfDate + "') as comp ");
                sqlQuery.AppendLine(" 			ON comp.rateCode = rt.Code ");
                sqlQuery.AppendLine(" 			INNER JOIN ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + curPricingDate + "') as os ");
                sqlQuery.AppendLine(" 			ON os.ratecode = rt.code ");

                marketRates = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, sqlQuery.ToString());

                //Non Maturity Deposits Data Pull

                sqlQuery.Clear();
                sqlQuery.AppendLine("	SELECT ");
                sqlQuery.AppendLine("		cur.type ");
                sqlQuery.AppendLine("		,cur.name ");
                sqlQuery.AppendLine("		,cur.rate  as curRate");
                sqlQuery.AppendLine("		,pr.rate as prRate ");
                sqlQuery.AppendLine("		,cur.rate - pr.rate as diff ");
                sqlQuery.AppendLine("		FROM ");
                sqlQuery.AppendLine("	( ");
                sqlQuery.AppendLine("	SELECT ");
                sqlQuery.AppendLine("		nmr.type ");
                sqlQuery.AppendLine("		,nmr.name ");
                sqlQuery.AppendLine("		,nmr.rate ");
                sqlQuery.AppendLine("		FROM ");
                sqlQuery.AppendLine("		(SELECT id From ATLAS_LiabilityPricing WHERE asOfDate = '" + curInfo.AsOfDate.ToShortDateString() + "') as cur ");
                sqlQuery.AppendLine("		INNER JOIN ");
                sqlQuery.AppendLine("		ATLAS_NonMaturityDepositRate as nmr ");
                sqlQuery.AppendLine("		ON nmr.LiabilityPricingId = cur.id ");
                sqlQuery.AppendLine("	) AS cur ");
                sqlQuery.AppendLine("	LEFT JOIN ");
                sqlQuery.AppendLine("	( ");
                sqlQuery.AppendLine("	SELECT ");
                sqlQuery.AppendLine("		nmr.type ");
                sqlQuery.AppendLine("		,nmr.name ");
                sqlQuery.AppendLine("		,nmr.rate ");
                sqlQuery.AppendLine("		FROM ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("		(SELECT id From ATLAS_LiabilityPricing WHERE asOfDate = '" + compInfo.AsOfDate.ToShortDateString() + "') as pr ");
                sqlQuery.AppendLine("		INNER JOIN ");
                sqlQuery.AppendLine("		ATLAS_NonMaturityDepositRate as nmr ");
                sqlQuery.AppendLine("		ON nmr.LiabilityPricingId = pr.id ");
                sqlQuery.AppendLine("	) AS pr ON ");
                sqlQuery.AppendLine("	pr.type = cur.type ");
                sqlQuery.AppendLine("	AND pr.name = cur.name ");
                nonMatRates = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), sqlQuery.ToString());


                //Time Deposit Stuff
                sqlQuery.Clear();
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		cur.term ");
                sqlQuery.AppendLine(" 		,cur.rate as curRate ");
                sqlQuery.AppendLine(" 		,pr.rate as prRate ");
                sqlQuery.AppendLine(" 		,cur.rate - pr.rate as diff ");
                sqlQuery.AppendLine("       ,0 as special ");
                sqlQuery.AppendLine(" 				 ,CASE  ");
                sqlQuery.AppendLine(" 				WHEN CHARINDEX('day', cur.term) > 0 THEN ");
                sqlQuery.AppendLine(" 					CAST(SUBSTRING(cur.term, 0, CHARINDEX(' ', cur.term)) as numeric(18,2)) / 60 ");
                sqlQuery.AppendLine(" 				ELSE ");
                sqlQuery.AppendLine(" 					CAST(SUBSTRING(cur.term, 0, CHARINDEX(' ', cur.term)) as numeric(18,2)) ");
                sqlQuery.AppendLine(" 				END as orderNum ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 	( ");
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		nmr.term ");
                sqlQuery.AppendLine(" 		,nmr.rate ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 		ATLAS_TimeDepositRate as nmr ");
                sqlQuery.AppendLine(" 		WHERE LiabilityPricingId = " + curPricing.Id.ToString() + " ");
                sqlQuery.AppendLine(" 	) AS cur ");
                sqlQuery.AppendLine(" 	LEFT JOIN ");
                sqlQuery.AppendLine(" 	( ");
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		nmr.term ");
                sqlQuery.AppendLine(" 		,nmr.rate ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 		ATLAS_TimeDepositRate as nmr ");
                sqlQuery.AppendLine(" 		WHERE LiabilityPricingId = " + compPricing.Id.ToString() + " ");
                sqlQuery.AppendLine(" 	) AS pr ON ");
                sqlQuery.AppendLine(" 	pr.term = cur.term ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" UNION ALL ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		cur.name as term ");
                sqlQuery.AppendLine(" 		,cur.rate as curRate ");
                sqlQuery.AppendLine(" 		,pr.rate as priorRate ");
                sqlQuery.AppendLine(" 		,cur.rate - pr.rate as diff ");
                sqlQuery.AppendLine("       ,1 as special ");
                sqlQuery.AppendLine(" 		 ,CASE  ");
                sqlQuery.AppendLine(" 				WHEN CHARINDEX('Day', cur.term) >0 THEN ");
                sqlQuery.AppendLine(" 					CAST(SUBSTRING(cur.term, 0, CHARINDEX(' ', cur.term)) as numeric(18,2)) / 60 ");
                sqlQuery.AppendLine(" 				ELSE ");
                sqlQuery.AppendLine(" 					CAST(SUBSTRING(cur.term, 0, CHARINDEX(' ', cur.term)) as numeric(18,2)) ");
                sqlQuery.AppendLine(" 				END as orderNum ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 	( ");
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		nmr.term ");
                sqlQuery.AppendLine(" 		,nmr.rate ");
                sqlQuery.AppendLine(" 		,nmr.name ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 		ATLAS_TimeDepositSpecialRate as nmr ");
                sqlQuery.AppendLine(" 		WHERE LiabilityPricingId = " + curPricing.Id.ToString() + " ");
                sqlQuery.AppendLine(" 	) AS cur ");
                sqlQuery.AppendLine(" 	LEFT JOIN ");
                sqlQuery.AppendLine(" 	( ");
                sqlQuery.AppendLine(" 	SELECT ");
                sqlQuery.AppendLine(" 		nmr.term ");
                sqlQuery.AppendLine(" 		,nmr.rate ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 		ATLAS_TimeDepositSpecialRate as nmr ");
                sqlQuery.AppendLine(" 		WHERE LiabilityPricingId = " + compPricing.Id.ToString() + " ");
                sqlQuery.AppendLine(" 	) AS pr ON ");
                sqlQuery.AppendLine(" 	pr.term = cur.term ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" 	ORDER BY ordernum ");
                timeDeps = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), sqlQuery.ToString());

                //Treasury Data
                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	rt.name ");
                sqlQuery.AppendLine(" 	,os.iRate as curRate ");
                sqlQuery.AppendLine(" 	,comp.iRate as prRate ");
                sqlQuery.AppendLine(" 	,os.iRate - comp.iRate As rateDiff ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine("       [Code] ");
                sqlQuery.AppendLine("       ,[IndexType] ");
                sqlQuery.AppendLine(" 	  ,[Name] ");
                sqlQuery.AppendLine(" 		  FROM [RateInfo] where  ");
                sqlQuery.AppendLine(" 		  name in ('(011) CMT 3 Month' ");
                sqlQuery.AppendLine(" 						,'(012) CMT 6 Month' ");
                sqlQuery.AppendLine(" 						,'(013) CMT 1 Year' ");
                sqlQuery.AppendLine(" 						,'(014) CMT 2 Year' ");
                sqlQuery.AppendLine(" 						,'(015) CMT 3 Year' ");
                sqlQuery.AppendLine(" 						,'(016) CMT 5 Year' ");
                sqlQuery.AppendLine(" 						)) as rt ");
                sqlQuery.AppendLine(" 			INNER JOIN  ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + compPricing.WebRateAsOfDate + "') as comp ");
                sqlQuery.AppendLine(" 			ON comp.rateCode = rt.Code ");
                sqlQuery.AppendLine(" 			INNER JOIN ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + curPricingDate + "') as os ");
                sqlQuery.AppendLine(" 			ON os.ratecode = rt.code ");
                treasury = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, sqlQuery.ToString());

                //Brokered
                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	rt.name ");
                sqlQuery.AppendLine(" 	,os.iRate as curRate ");
                sqlQuery.AppendLine(" 	,comp.iRate as prRate ");
                sqlQuery.AppendLine(" 	,os.iRate - comp.iRate As rateDiff ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine("       [Code] ");
                sqlQuery.AppendLine("       ,[IndexType] ");
                sqlQuery.AppendLine(" 	  ,[Name] ");
                sqlQuery.AppendLine(" 		  FROM [RateInfo] where  ");
                sqlQuery.AppendLine(" 		  name in ('(156) Brokered CD''s (FNC) 3 Month' ");
                sqlQuery.AppendLine(" 						,'(157) Brokered CD''s (FNC) 6 Month' ");
                sqlQuery.AppendLine(" 						,'(158) Brokered CD''s (FNC) 9 Month' ");
                sqlQuery.AppendLine(" 						,'(159) Brokered CD''s (FNC) 1 Year' ");
                sqlQuery.AppendLine(" 						,'(160) Brokered CD''s (FNC) 2 Year' ");
                sqlQuery.AppendLine(" 						,'(161) Brokered CD''s (FNC) 3 Year' ");
                sqlQuery.AppendLine(" 						,'(162) Brokered CD''s (FNC) 4 Year' ");
                sqlQuery.AppendLine(" 						,'(163) Brokered CD''s (FNC) 5 Year' ");
                sqlQuery.AppendLine(" 						)) as rt ");
                sqlQuery.AppendLine(" 			INNER JOIN  ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + compPricing.WebRateAsOfDate + "') as comp ");
                sqlQuery.AppendLine(" 			ON comp.rateCode = rt.Code ");
                sqlQuery.AppendLine(" 			INNER JOIN ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + curPricingDate + "') as os ");
                sqlQuery.AppendLine(" 			ON os.ratecode = rt.code ");
                brokered = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, sqlQuery.ToString());


                //FHLB Data

                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	rt.name ");
                sqlQuery.AppendLine(" 	,os.iRate as curRate ");
                sqlQuery.AppendLine(" 	,comp.iRate as prRate ");
                sqlQuery.AppendLine(" 	,os.iRate - comp.iRate As rateDiff ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine("       [Code] ");
                sqlQuery.AppendLine("       ,[IndexType] ");
                sqlQuery.AppendLine(" 	  ,[Name] ");
                sqlQuery.AppendLine(" 		  FROM [RateInfo] where  ");
                sqlQuery.AppendLine(" 		  name in ( ");
                switch (curPricing.FHLBDistrict) {
                    case "Atlanta":
                        sqlQuery.AppendLine(" '(051) FHLB Atlanta 3 Month', ");
                        sqlQuery.AppendLine(" '(052) FHLB Atlanta 6 Month', ");
                        sqlQuery.AppendLine(" '(176) FHLB Atlanta 9 Month', ");
                        sqlQuery.AppendLine(" '(053) FHLB Atlanta 1 Year', ");
                        sqlQuery.AppendLine(" '(054) FHLB Atlanta 2 Year', ");
                        sqlQuery.AppendLine(" '(055) FHLB Atlanta 3 Year', ");
                        sqlQuery.AppendLine(" '(056) FHLB Atlanta 4 Year', ");
                        sqlQuery.AppendLine(" '(057) FHLB Atlanta 5 Year' ");
                        break;
                    case "Boston":
                        sqlQuery.AppendLine(" '(027) FHLB Boston 3 Month', ");
                        sqlQuery.AppendLine(" '(028) FHLB Boston 6 Month', ");
                        sqlQuery.AppendLine(" '(165) FHLB Boston 9 Month', ");
                        sqlQuery.AppendLine(" '(029) FHLB Boston 1 Year', ");
                        sqlQuery.AppendLine(" '(030) FHLB Boston 2 Year', ");
                        sqlQuery.AppendLine(" '(031) FHLB Boston 3 Year', ");
                        sqlQuery.AppendLine(" '(032) FHLB Boston 4 Year', ");
                        sqlQuery.AppendLine(" '(033) FHLB Boston 5 Year' ");
                        break;
                    case "Chicago":
                        sqlQuery.AppendLine(" '(062) FHLB Chicago 3 Month', ");
                        sqlQuery.AppendLine(" '(063) FHLB Chicago 6 Month', ");
                        sqlQuery.AppendLine(" '(064) FHLB Chicago 1 Year', ");
                        sqlQuery.AppendLine(" '(065) FHLB Chicago 2 Year', ");
                        sqlQuery.AppendLine(" '(066) FHLB Chicago 3 Year', ");
                        sqlQuery.AppendLine(" '(067) FHLB Chicago 4 Year', ");
                        sqlQuery.AppendLine(" '(068) FHLB Chicago 5 Year' ");
                        break;
                    case "Cincinnati":
                        sqlQuery.AppendLine(" '(073) FHLB Cincinnati 3 Month', ");
                        sqlQuery.AppendLine(" '(074) FHLB Cincinnati 6 Month', ");
                        sqlQuery.AppendLine(" '(166) FHLB Cincinnati 9 Month', ");
                        sqlQuery.AppendLine(" '(075) FHLB Cincinnati 1 Year', ");
                        sqlQuery.AppendLine(" '(076) FHLB Cincinnati 2 Year', ");
                        sqlQuery.AppendLine(" '(077) FHLB Cincinnati 3 Year', ");
                        sqlQuery.AppendLine(" '(078) FHLB Cincinnati 4 Year', ");
                        sqlQuery.AppendLine(" '(079) FHLB Cincinnati 5 Year' ");
                        break;
                    case "Dallas":
                        sqlQuery.AppendLine(" '(084) FHLB Dallas 3 Month', ");
                        sqlQuery.AppendLine(" '(085) FHLB Dallas 6 Month', ");
                        sqlQuery.AppendLine(" '(246) FHLB Dallas 9 Month', ");
                        sqlQuery.AppendLine(" '(086) FHLB Dallas 1 Year', ");
                        sqlQuery.AppendLine(" '(087) FHLB Dallas 2 Year', ");
                        sqlQuery.AppendLine(" '(088) FHLB Dallas 3 Year', ");
                        sqlQuery.AppendLine(" '(089) FHLB Dallas 4 Year', ");
                        sqlQuery.AppendLine(" '(090) FHLB Dallas 5 Year' ");
                        break;
                    case "Des Moines":
                        sqlQuery.AppendLine(" '(095) FHLB Des Moines 3 Month', ");
                        sqlQuery.AppendLine(" '(096) FHLB Des Moines 6 Month', ");
                        sqlQuery.AppendLine(" '(097) FHLB Des Moines 1 Year', ");
                        sqlQuery.AppendLine(" '(098) FHLB Des Moines 2 Year', ");
                        sqlQuery.AppendLine(" '(099) FHLB Des Moines 3 Year', ");
                        sqlQuery.AppendLine(" '(250) FHLB Des Moines 4yr', ");
                        sqlQuery.AppendLine(" '(100) FHLB Des Moines 5 Year' ");
                        break;
                    case "Indianpolis":
                        sqlQuery.AppendLine(" '(105) FHLB Indianapolis 3 Month', ");
                        sqlQuery.AppendLine(" '(106) FHLB Indianapolis 6 Month', ");
                        sqlQuery.AppendLine(" '(167) FHLB Indianapolis 9 Month', ");
                        sqlQuery.AppendLine(" '(107) FHLB Indianapolis 1 Year', ");
                        sqlQuery.AppendLine(" '(108) FHLB Indianapolis 2 Year', ");
                        sqlQuery.AppendLine(" '(109) FHLB Indianapolis 3 Year', ");
                        sqlQuery.AppendLine(" '(110) FHLB Indianapolis 4 Year', ");
                        sqlQuery.AppendLine(" '(111) FHLB Indianapolis 5 Year' ");
                        break;
                    case "New York":
                        sqlQuery.AppendLine(" '(037) FHLB NY 3 Month', ");
                        sqlQuery.AppendLine(" '(038) FHLB NY 6 Month', ");
                        sqlQuery.AppendLine(" '(039) FHLB NY 1 Year', ");
                        sqlQuery.AppendLine(" '(040) FHLB NY 2 Year', ");
                        sqlQuery.AppendLine(" '(041) FHLB NY 3 Year', ");
                        sqlQuery.AppendLine(" '(042) FHLB NY 4 Year', ");
                        sqlQuery.AppendLine(" '(043) FHLB NY 5 Year' ");
                        break;
                    case "Pittsburgh":
                        sqlQuery.AppendLine(" '(116) FHLB Pittsburgh 3 Month', ");
                        sqlQuery.AppendLine(" '(117) FHLB Pittsburgh 6 Month', ");
                        sqlQuery.AppendLine(" '(168) FHLB Pittsburgh 9 Month', ");
                        sqlQuery.AppendLine(" '(118) FHLB Pittsburgh 1 Year', ");
                        sqlQuery.AppendLine(" '(119) FHLB Pittsburgh 2 Year', ");
                        sqlQuery.AppendLine(" '(120) FHLB Pittsburgh 3 Year', ");
                        sqlQuery.AppendLine(" '(121) FHLB Pittsburgh 4 Year', ");
                        sqlQuery.AppendLine(" '(122) FHLB Pittsburgh 5 Year' ");
                        break;
                    case "San Francisco":
                        sqlQuery.AppendLine(" '(127) FHLB San Francisco 3 Month', ");
                        sqlQuery.AppendLine(" '(128) FHLB San Francisco 6 Month', ");
                        sqlQuery.AppendLine(" '(129) FHLB San Francisco 1 Year', ");
                        sqlQuery.AppendLine(" '(130) FHLB San Francisco 2 Year', ");
                        sqlQuery.AppendLine(" '(131) FHLB San Francisco 3 Year', ");
                        sqlQuery.AppendLine(" '(132) FHLB San Francisco 5 Year' ");
                        break;
                    case "Seattle":
                        sqlQuery.AppendLine(" '(137) FHLB Seattle 3 Month', ");
                        sqlQuery.AppendLine(" '(138) FHLB Seattle 6 Month', ");
                        sqlQuery.AppendLine(" '(139) FHLB Seattle 1 Year', ");
                        sqlQuery.AppendLine(" '(140) FHLB Seattle 2 Year', ");
                        sqlQuery.AppendLine(" '(141) FHLB Seattle 3 Year', ");
                        sqlQuery.AppendLine(" '(142) FHLB Seattle 4 Year', ");
                        sqlQuery.AppendLine(" '(143) FHLB Seattle 5 Year' ");
                        break;
                    case "Topeka":
                        sqlQuery.AppendLine(" '(148) FHLB Topeka 3 Month', ");
                        sqlQuery.AppendLine(" '(149) FHLB Topeka 6 Month', ");
                        sqlQuery.AppendLine(" '(169) FHLB Topeka 9 Month', ");
                        sqlQuery.AppendLine(" '(150) FHLB Topeka 1 Year', ");
                        sqlQuery.AppendLine(" '(151) FHLB Topeka 2 Year', ");
                        sqlQuery.AppendLine(" '(152) FHLB Topeka 3 Year', ");
                        sqlQuery.AppendLine(" '(153) FHLB Topeka 4 Year', ");
                        sqlQuery.AppendLine(" '(154) FHLB Topeka 5 Year' ");
                        break;
                }
                sqlQuery.AppendLine(" 						)) as rt ");
                sqlQuery.AppendLine(" 			INNER JOIN  ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + compPricing.WebRateAsOfDate + "') as comp ");
                sqlQuery.AppendLine(" 			ON comp.rateCode = rt.Code ");
                sqlQuery.AppendLine(" 			INNER JOIN ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + curPricingDate + "') as os ");
                sqlQuery.AppendLine(" 			ON os.ratecode = rt.code ");
                fhlb = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, sqlQuery.ToString());


                //Libor Rates
                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	rt.name ");
                sqlQuery.AppendLine(" 	,os.iRate as curRate ");
                sqlQuery.AppendLine(" 	,comp.iRate as prRate ");
                sqlQuery.AppendLine(" 	,os.iRate - comp.iRate As rateDiff ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine("       [Code] ");
                sqlQuery.AppendLine("       ,[IndexType] ");
                sqlQuery.AppendLine(" 	  ,[Name] ");
                sqlQuery.AppendLine(" 		  FROM [RateInfo] where  ");
                sqlQuery.AppendLine(" 		  name in (");
                sqlQuery.AppendLine(" 						'(007) LIBOR 3 Month' ");
                sqlQuery.AppendLine(" 						,'(008) LIBOR 6 Month' ");
                sqlQuery.AppendLine(" 						,'(009) LIBOR 12 Month' ");
                sqlQuery.AppendLine(" 						,'(171) Libor 2 Year' ");
                sqlQuery.AppendLine(" 						,'(172) Libor 3 Year' ");
                sqlQuery.AppendLine(" 						,'(173) Libor 4 Year' ");
                sqlQuery.AppendLine(" 						,'(174) Libor 5 Year' ");
                sqlQuery.AppendLine(" 						)) as rt ");
                sqlQuery.AppendLine(" 			INNER JOIN  ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + compPricing.WebRateAsOfDate + "') as comp ");
                sqlQuery.AppendLine(" 			ON comp.rateCode = rt.Code ");
                sqlQuery.AppendLine(" 			INNER JOIN ");
                sqlQuery.AppendLine(" 			(SELECT iRate, rateCode FROM RateBaseData WHERE asOfDate = '" + curPricingDate + "') as os ");
                sqlQuery.AppendLine(" 			ON os.ratecode = rt.code ");
                libor = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, sqlQuery.ToString());

                //Load simulation type id from policies to use in report
               

                sqlQuery.Clear();
                sqlQuery.AppendLine(" select ");
                sqlQuery.AppendLine("  a.month ");
                sqlQuery.AppendLine("  ,CASE WHEN Acc_TypeOr = 6 THEN 3 ELSE Acc_TypeOr END as Acc_TypeOR ");
                sqlQuery.AppendLine("  ,SUM(End_Bal) / 1000 as totBalance ");
                sqlQuery.AppendLine("  ,SUM(End_Bal * End_Rate) / NULLIF(SUM(End_Bal),0) as weightRate ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("  from basis_ala as a ");
                sqlQuery.AppendLine(" INNER JOIN Simulations as s ");
                sqlQuery.AppendLine(" on a.SimulationId = s.id  ");
                sqlQuery.AppendLine(" INNER JOIN asOfDateInfo as aod ON ");
                sqlQuery.AppendLine(" aod.Id = s.InformationId ");
                sqlQuery.AppendLine(" INNER JOIN Basis_alb as b ");
                sqlQuery.AppendLine(" ON a.code = b.code  and a.SimulationId = b.SimulationId ");
                sqlQuery.AppendLine(" WHERE s.SimulationTypeId = 1 AND b.exclude = 0   AND b.cat_type IN(0,1,5) AND a.month = DATEADD(m, -1, DATEADD(d,1, aod.asOfDate)) AND (b.Acc_TypeOR IN (3,8) OR (b.acc_TypeOR = 6 and b.classOr in (2,3))) AND a.month >= DATEADD(m, -1, DATEADD(d,1,'" + startDateHist.AsOfDate.ToShortDateString() + "')) AND a.month <= DATEADD(m, -1, DATEADD(d,1,'" + endDateHist.AsOfDate.ToShortDateString() + "'))  ");
                sqlQuery.AppendLine(" GROUP BY month, CASE WHEN Acc_TypeOr = 6 THEN 3 ELSE Acc_TypeOr END ");
                sqlQuery.AppendLine(" ORDER BY month, CASE WHEN Acc_TypeOr = 6 THEN 3 ELSE Acc_TypeOr END");
                histDeposit = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), sqlQuery.ToString());

                //Load Up Simulation Information
                object curObj = instService.GetSimulationScenario(curInfo.AsOfDate, 0, c.SimulationTypeId, c.ScenarioTypeId);
                string simulationId = curObj.GetType().GetProperty("simulation").GetValue(curObj).ToString();
                string scenario = curObj.GetType().GetProperty("scenario").GetValue(curObj).ToString();

                object compObj = instService.GetSimulationScenario(compInfo.AsOfDate, 0, c.CompSimulationTypeId, c.CompScenarioTypeId);
                string compSimulationId = compObj.GetType().GetProperty("simulation").GetValue(compObj).ToString();
                string compScenario = compObj.GetType().GetProperty("scenario").GetValue(compObj).ToString();

                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	ISNULL(a.startingBal, 0) as startingBal ");
                sqlQuery.AppendLine(" 	,ISNULL(a.startingRate, 0) as startingrate ");
                sqlQuery.AppendLine(" 	,ISNULL(b.maturting, 0) as maturting ");
                sqlQuery.AppendLine(" 	,ISNULL(b.maturingRate, 0) as maturingRate ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine("  (SELECT  ");
                sqlQuery.AppendLine("  	SUM(a.end_Bal) as startingBal  ");
                sqlQuery.AppendLine("  	,SUM(a.End_Bal * a.Total_End_Rate) / NULLIF(SUM(a.End_Bal),  0) as startingRate  ");
                sqlQuery.AppendLine("  	FROM Basis_ALA as a  ");
                sqlQuery.AppendLine("  	INNER JOIN Basis_ALB as b ON a.code = b.code and a.SimulationId = b.SimulationId AND b.Acc_TypeOR = 8 AND a.simulationId = " + compSimulationId + " AND a.month = '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "') as a ");
                sqlQuery.AppendLine(" 	INNER JOIN	 ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine(" 		SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)) AS  maturting  ");
                sqlQuery.AppendLine(" 		,SUM((ISNULL(alp3.MatEBal, 0) * ISNULL(alp3.MatERate, 0)) + (ISNULL(alp4.AmtEBal, 0) * ISNULL(alp4.AmtERate, 0)) + (ISNULL(alp5.PayEBal, 0) * ISNULL(alp5.PayERate, 0))) / NULLIF(SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)), 0) AS  maturingRate  ");
                sqlQuery.AppendLine("  	FROM ");
                sqlQuery.AppendLine(" 	Basis_ALB as b  ");
                sqlQuery.AppendLine(" 	LEFT JOIN Basis_ALP3 AS alp3 ON alp3.code = b.code AND alp3.SimulationId = b.SimulationId AND alp3.scenario = '" + compScenario + "' AND alp3.month > '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "' AND alp3.Month <= '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "'  ");
                sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP4 AS alp4 ON alp4.code = b.code AND alp4.SimulationId = b.SimulationId AND alp4.scenario = '" + compScenario + "' AND alp4.month > '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "' AND alp4.Month <= '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "'  ");
                sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP5 AS alp5 ON alp5.code = b.code AND alp5.SimulationId = b.SimulationId AND alp5.scenario = '" + compScenario + "' AND alp5.month > '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "' AND alp5.Month <= '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "'  ");
                sqlQuery.AppendLine(" 	WHERE b.acc_TypeOr = 8 AND b.SimulationId = " + compSimulationId + " AND b.exclude = 0   AND b.cat_type IN(0,1,5)) as b ");
                sqlQuery.AppendLine(" 	ON 1 = 1 ");
                existing = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), sqlQuery.ToString());

                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	ISNULL(a.startingBal, 0) as startingBal ");
                sqlQuery.AppendLine(" 	,ISNULL(a.startingRate, 0) as startingrate ");
                sqlQuery.AppendLine(" 	,ISNULL(b.maturingFuture, 0) as maturingFuture ");
                sqlQuery.AppendLine(" 	,ISNULL(b.maturingFutureRate, 0) as maturingFutureRate ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine("  (SELECT  ");
                sqlQuery.AppendLine("  	SUM(a.end_Bal) as startingBal  ");
                sqlQuery.AppendLine("  	,SUM(a.End_Bal * a.Total_End_Rate) / NULLIF(SUM(a.End_Bal),  0) as startingRate  ");
                sqlQuery.AppendLine("  	FROM Basis_ALA as a  ");
                sqlQuery.AppendLine("  	INNER JOIN Basis_ALB as b ON a.code = b.code and a.SimulationId = b.SimulationId AND b.Acc_TypeOR = 8 AND a.simulationId = " + simulationId + " AND a.month = '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "') as a ");
                sqlQuery.AppendLine(" 	INNER JOIN	 ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine(" 		SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)) AS  maturingFuture  ");
                sqlQuery.AppendLine(" 		,SUM((ISNULL(alp3.MatEBal, 0) * ISNULL(alp3.MatERate, 0)) + (ISNULL(alp4.AmtEBal, 0) * ISNULL(alp4.AmtERate, 0)) + (ISNULL(alp5.PayEBal, 0) * ISNULL(alp5.PayERate, 0))) / NULLIF(SUM(ISNULL(alp3.MatEBal, 0) + ISNULL(alp4.AmtEBal, 0) + ISNULL(alp5.PayEBal, 0)), 0) AS  maturingFutureRate  ");
                sqlQuery.AppendLine("  	FROM ");
                sqlQuery.AppendLine(" 	Basis_ALB as b  ");
                sqlQuery.AppendLine(" 	LEFT JOIN Basis_ALP3 AS alp3 ON alp3.code = b.code AND alp3.SimulationId = b.SimulationId AND alp3.scenario = '" + scenario + "' AND alp3.month > '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' AND alp3.Month <=  DATEADD(m, DATEDIFF(m, '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "', '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ), '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "')");
                sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP4 AS alp4 ON alp4.code = b.code AND alp4.SimulationId = b.SimulationId AND alp4.scenario = '" + scenario + "' AND alp4.month > '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' AND alp4.Month <= DATEADD(m, DATEDIFF(m, '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "', '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ), '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "')");
                sqlQuery.AppendLine("  	LEFT JOIN Basis_ALP5 AS alp5 ON alp5.code = b.code AND alp5.SimulationId = b.SimulationId AND alp5.scenario = '" + scenario + "' AND alp5.month > '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' AND alp5.Month <= DATEADD(m, DATEDIFF(m, '" + Utilities.GetFirstOfMonthStr(compInfo.AsOfDate.ToShortDateString()) + "', '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "' ), '" + Utilities.GetFirstOfMonthStr(curInfo.AsOfDate.ToShortDateString()) + "')");
                sqlQuery.AppendLine(" 	WHERE b.acc_TypeOr = 8 AND b.exclude = 0   AND b.cat_type IN(0,1,5) AND b.SimulationId = " + simulationId + ") as b ");
                sqlQuery.AppendLine(" 	ON 1 = 1 ");
                future = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(c.InstitutionDatabaseName), sqlQuery.ToString());


                //Take future Table and existing table and create water fall datatable
                waterfall.Columns.Add("label", typeof(string));
                waterfall.Columns.Add("balance", typeof(double));
                waterfall.Columns.Add("rate", typeof(double));
                waterfall.Columns.Add("isIntermediateSum", typeof(int));


                if (existing.Rows.Count > 0 && future.Rows.Count > 0)
                {
                    //Add starting Balance
                    DataRow newRow = waterfall.NewRow();

                    newRow["label"] = compInfo.AsOfDate.ToShortDateString();
                    newRow["balance"] = double.Parse(existing.Rows[0]["startingBal"].ToString());
                    newRow["rate"] = double.Parse(existing.Rows[0]["startingRate"].ToString());
                    newRow["isIntermediateSum"] = 0;

                    waterfall.Rows.Add(newRow);

                    //Add existing maturties
                    newRow = waterfall.NewRow();

                    newRow["label"] = "MATURITIES";
                    newRow["balance"] = double.Parse(existing.Rows[0]["maturting"].ToString()) * -1;
                    newRow["rate"] = double.Parse(existing.Rows[0]["maturingRate"].ToString());
                    newRow["isIntermediateSum"] = 0;
                    waterfall.Rows.Add(newRow);


                    //Add New Originations -- This is a calculation based off of both future and existing
                    newRow = waterfall.NewRow();
                    double newBalance = double.Parse(future.Rows[0]["startingBal"].ToString()) - (double.Parse(existing.Rows[0]["startingBal"].ToString()) - double.Parse(existing.Rows[0]["maturting"].ToString())); ;
                    newRow["label"] = "NEW";
                    newRow["balance"] = newBalance;

                    //Calcualte a weighted average rate
                    double wavg = ((double.Parse(future.Rows[0]["startingBal"].ToString()) * (double.Parse(future.Rows[0]["startingRate"].ToString()))) - ((double.Parse(existing.Rows[0]["startingRate"].ToString()) * double.Parse(existing.Rows[0]["startingBal"].ToString())) - (double.Parse(existing.Rows[0]["maturingRate"].ToString()) * double.Parse(existing.Rows[0]["maturting"].ToString())))) / newBalance;
                    newRow["rate"] = wavg;
                    newRow["isIntermediateSum"] = 0;
                    waterfall.Rows.Add(newRow);

                    //NExt is just an intermediat sum of the previous parts
                    newRow = waterfall.NewRow();

                    newRow["label"] = curInfo.AsOfDate.ToShortDateString();
                    newRow["balance"] = 0;
                    newRow["rate"] = double.Parse(future.Rows[0]["startingRate"].ToString());
                    newRow["isIntermediateSum"] = 1;
                    waterfall.Rows.Add(newRow);
                    
                    //NExt is the maturities 
                    newRow = waterfall.NewRow();
                    int monthDiff = ((curInfo.AsOfDate.Year - compInfo.AsOfDate.Year) * 12) + curInfo.AsOfDate.Month - compInfo.AsOfDate.Month;
                    DateTime newDate = curInfo.AsOfDate.AddMonths(monthDiff);
                    newRow["label"] = "MATURITIES " + curInfo.AsOfDate.AddDays(1).ToShortDateString() + " TO " + new DateTime(newDate.Year, newDate.Month, DateTime.DaysInMonth(newDate.Year,newDate.Month)).ToShortDateString();
                    newRow["balance"] = double.Parse(future.Rows[0]["maturingFuture"].ToString()) * -1;
                    newRow["rate"] = double.Parse(future.Rows[0]["maturingFutureRate"].ToString());
                    newRow["isIntermediateSum"] = 0;
                    waterfall.Rows.Add(newRow);



                }




            }
            return new
            {
                marketRates = marketRates,
                nonMatRates = nonMatRates,
                timeDepRates = timeDeps,
                treasuryRates = treasury,
                brokeredRates = brokered,
                fhlbRates = fhlb,
                liborRates = libor,
                rateCurve = curPricing.HistoricalRateCurve,
                district = curPricing.FHLBDistrict,
                webDate = curPricingDate,
                depositDate = curPricing.DepositRateAsOfDate.ToShortDateString(),
                histDeposit = histDeposit,
                waterfall = waterfall,
                incRetail = c.IncludeRetailRepos,

                warnings = warnings,
                startDate = compInfo.AsOfDate.ToShortDateString(),
                endDate = curPricing.AsOfDate.ToShortDateString(),
                errors = errors
            };
        }
    }
}