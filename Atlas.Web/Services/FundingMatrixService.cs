﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class FundingMatrixService
    {
        private readonly InstitutionContext _gctx = new InstitutionContext();
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();   

        public object FundingMatrixViewModel(int reportId)
        {

            string conStr = "";
            var conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
            {
                InitialCatalog = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString()
            };
            conStr = conenctionStringBuilder.ConnectionString;

            InstitutionContext gctx = new InstitutionContext(conStr);
            GlobalController gc = new GlobalController();
            DataTable[] dataTables = new DataTable[]{};
            DateTime[] dates = new DateTime[]{};
            string[] scenNames = new string[]{};
            string[] simNames = new string[] { };

            

            //Look Up Funding Matrix Settings
            var fundMatrix = gctx.FundingMatrices.Where(s => s.Id == reportId).FirstOrDefault();

            Dictionary<string, DataTable> policyOffsets = new Dictionary<string,DataTable>();

            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (fundMatrix == null) {
                errors.Add("Funding Matrix Not Found");
            }
            else{//actually do the thing
                dataTables = new DataTable[fundMatrix.FundingMatrixOffsets.Count];
                dates = new DateTime[fundMatrix.FundingMatrixOffsets.Count];
                scenNames = new string[fundMatrix.FundingMatrixOffsets.Count];
                simNames = new string[fundMatrix.FundingMatrixOffsets.Count];
                int dCount = 0;
                foreach(FundingMatrixOffset offset in fundMatrix.FundingMatrixOffsets.OrderBy(f => f.Priority)){
                    policyOffsets.Clear();
                    policyOffsets = gc.PolicyOffsets(offset.InstitutionDatabaseName);

                    int simulationTypeIdToUse = offset.SimulationTypeId;

                    var instServ = new InstitutionService(offset.InstitutionDatabaseName);
                    var info = instServ.GetInformation(rep.Package.AsOfDate, offset.AsOfDateOffset);

                    if (!offset.OverrideSimulation)
                    {
                        if (policyOffsets.Keys.Contains(info.AsOfDate.ToString("MM/dd/yyyy")))
                        {
                            simulationTypeIdToUse = Convert.ToInt32(policyOffsets[info.AsOfDate.ToString("MM/dd/yyyy")].Rows[0]["niiId"].ToString());
                        }
                        
                    }

                    DataTable dt = MatrixDataTable(instServ, simulationTypeIdToUse, offset.ScenarioTypeId, rep.Package.AsOfDate, offset.AsOfDateOffset, fundMatrix.TaxEqivalentYields);
                    dataTables[dCount] = dt;
                    dates[dCount] = info.AsOfDate;
                    scenNames[dCount] = offset.ScenarioType.Name;
                    simNames[dCount] = offset.SimulationType.Name;
                    dCount++;

                    if (dt.Rows.Count == 0) {
                        errors.Add(String.Format(Utilities.GetErrorTemplate(1), offset.ScenarioType.Name, offset.SimulationType.Name, offset.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                    }
                }
            }

            return new
            {
                dataTables = dataTables,
                dates = dates,
                scenNames = scenNames,
                simNames = simNames,
                packageAOD = rep.Package.AsOfDate,
                warnings = warnings,
                errors = errors
            };
        }


        private DataTable MatrixDataTable(InstitutionService instService, int simulationTypeId, int scenarioTypeId, DateTime packageAsOfDate, int dateOffset, bool taxEquiv)
        {

            var obj = instService.GetSimulationScenario(packageAsOfDate, dateOffset, simulationTypeId, scenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();
            DataTable retTable = new DataTable();

            if (simulationId == "" || scenario == "" || asOfDate == "") {
                
            }
            else {
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine("SELECT monthGrouping, isAsset, sum(balance) as balance, sum(balance * rate) / sum(balance) as rate from(");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	g.monthGrouping  ");
                sqlQuery.AppendLine(" 	,g.isAsset  ");
                sqlQuery.AppendLine(" 	,SUM(balance) as balance  ");
                sqlQuery.AppendLine(" 	,SUM(weightedRate) / NULLIF(SUM(balance), 0) as rate  ");
                sqlQuery.AppendLine(" 	FROM  ");
                sqlQuery.AppendLine(" 	(SELECT  ");
                sqlQuery.AppendLine(" 		sm.month  ");
                sqlQuery.AppendLine(" 		,CASE   ");
                sqlQuery.AppendLine(" 			WHEN sm.month = DATEADD(d, 1, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				sm.balance - sm.OneDaySGapBal  ");
                sqlQuery.AppendLine(" 			ELSE  ");
                sqlQuery.AppendLine(" 				sm.balance  ");
                sqlQuery.AppendLine(" 		END as balance  ");
                sqlQuery.AppendLine(" 		,CASE   ");
                sqlQuery.AppendLine(" 			WHEN sm.month = DATEADD(d, 1, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				sm.weightedRate - (sm.OneDaySGapBal * sm.OneDaySGapRate)  ");
                sqlQuery.AppendLine(" 			ELSE  ");
                sqlQuery.AppendLine(" 				sm.weightedRate  ");
                sqlQuery.AppendLine(" 		END as weightedRate  ");
                sqlQuery.AppendLine(" 		,sm.nMonths  ");
                sqlQuery.AppendLine(" 		,sm.OneDaySGapBal  ");
                sqlQuery.AppendLine(" 		,sm.OneDaySGapRate  ");
                sqlQuery.AppendLine(" 		,sm.asOfDate  ");
                sqlQuery.AppendLine(" 		,CASE  ");
                sqlQuery.AppendLine(" 			WHEN sm.month <= DATEADD(m, 3,  sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				'0-3<br>Months'  ");
                sqlQuery.AppendLine(" 			WHEN sm.month < DATEADD(m, 6, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				'4-6<br>Months'  ");
                sqlQuery.AppendLine(" 			WHEN sm.month < DATEADD(m, 12, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				'7-12<br>Months'  ");
                sqlQuery.AppendLine(" 			WHEN sm.month < DATEADD(m, 24, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				'13-24<br>Months'  ");
                sqlQuery.AppendLine(" 			WHEN sm.month < DATEADD(m, 36, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				'25-36<br>Months'  ");
                sqlQuery.AppendLine(" 			WHEN sm.month < DATEADD(m, 60, sm.asOfDate) THEN  ");
                sqlQuery.AppendLine(" 				'37-60<br>Months'  ");
                sqlQuery.AppendLine(" 			ELSE  ");
                sqlQuery.AppendLine(" 				'Over 60<br>Months'  ");
                sqlQuery.AppendLine(" 			END as monthGrouping  ");
                sqlQuery.AppendLine(" 			,IsAsset as IsAsset  ");
                sqlQuery.AppendLine(" 		FROM  ");
                sqlQuery.AppendLine("		(SELECT  ");
                sqlQuery.AppendLine("			CASE WHEN ps.period = 1 THEN CAST(ep.month as datetime) ELSE DATEADD(m, ps.period - 1, CAST(ep.month as datetime)) END as month  ");
                sqlQuery.AppendLine("			,ep.SGapBal / ep.nMonths as Balance  ");
                if (taxEquiv) {
                    sqlQuery.AppendLine("			,DWR_FedRates.dbo.[TaxEquivilentYield](bb.LocDivExclu, bb.StaDivExclu, bb.FedDivExclu, bb.LocTaxExempt, bb.StaTaxExempt, bb.FedTaxExempt, teyld.localAmount, teyld.stateAmount, teyld.fedAmount,  ep.SGapRate) * (ep.sGapBal / ep.NMonths) as  weightedRate  ");
                }
                else {
                    sqlQuery.AppendLine("			,ep.SGapRate * (ep.sGapBal / ep.NMonths) as  weightedRate  ");
                }

                sqlQuery.AppendLine("			,ep.NMonths  ");
                sqlQuery.AppendLine("			,ep.code  ");
                sqlQuery.AppendLine("			,sc.OneDaySGapBal  ");
                sqlQuery.AppendLine("			,sc.OneDaySGapRate   ");
                sqlQuery.AppendLine("			,CAST('" + asOfDate + "' as datetime) as asOfDate  ");
                sqlQuery.AppendLine("			,bb.IsAsset  ");
                sqlQuery.AppendLine("			FROM  ");
                sqlQuery.AppendLine("			(SELECT  ");
                sqlQuery.AppendLine("				simulationId  ");
                sqlQuery.AppendLine("				,scenario  ");
                sqlQuery.AppendLine("				,code  ");
                sqlQuery.AppendLine("				,Month  ");
                sqlQuery.AppendLine("				,SGapBal  ");
                sqlQuery.AppendLine("				,SGapRate  ");
                sqlQuery.AppendLine("				,NMonths  ");
                sqlQuery.AppendLine("				FROM Basis_ALP6 WHERE simulationId = " + simulationId + " AND scenario = '" + scenario + "') as ep  ");
                sqlQuery.AppendLine("				INNER JOIN  ");
                sqlQuery.AppendLine("				ATLAS_PeriodSmoother as ps  ");
                sqlQuery.AppendLine("				ON ep.NMonths >= ps.period  ");
                sqlQuery.AppendLine("				INNER JOIN Basis_ALS as sc  ");
                sqlQuery.AppendLine("				ON sc.simulationId = ep.SimulationId AND  ");
                sqlQuery.AppendLine("				sc.scenario = ep.scenario AND  ");
                sqlQuery.AppendLine("				sc.code = ep.code  ");
                sqlQuery.AppendLine("				INNER JOIN Basis_ALB as bb  ");
                sqlQuery.AppendLine("				ON ep.SimulationId = bb.SimulationId   ");
                sqlQuery.AppendLine("				AND ep.code = bb.code	 ");
                sqlQuery.AppendLine("				INNER JOIN  ");
                sqlQuery.AppendLine("				(SELECT ");
                sqlQuery.AppendLine("					g.month ");
                sqlQuery.AppendLine("					,SUM(localAmount) as localAmount ");
                sqlQuery.AppendLine("					,SUM(stateAmount) as stateAmount ");
                sqlQuery.AppendLine("					,SUM(fedAmount) as fedAmount ");
                sqlQuery.AppendLine("					FROM  ");
                sqlQuery.AppendLine("						(SELECT month,amount as localAmount, 0.0 as stateAmount, 0.0 as fedAmount FROM Basis_OtherP WHERE code =  ");
                sqlQuery.AppendLine("						(SELECT TOP 1 [Code] FROM [Basis_OTHERB] where sequence = 137 and simulationId = " + simulationId + ") AND simulationId = " + simulationId + " and scenario = '" + scenario + "' ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("						UNION ALL ");
                sqlQuery.AppendLine("						SELECT month, 0.0 as localAmount, amount as stateAmount, 0.0 as fedAmount FROM Basis_OtherP WHERE code =   ");
                sqlQuery.AppendLine("						(SELECT TOP 1 [Code] FROM [Basis_OTHERB] where sequence = 138 and simulationId = " + simulationId + ") AND simulationId = " + simulationId + " and scenario = '" + scenario + "' ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("						UNION ALL  ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("						SELECT month, 0.0 as localAmount, 0.0 as stateAmount, amount as fedAmount FROM Basis_OtherP WHERE code =   ");
                sqlQuery.AppendLine("						(SELECT TOP 1 [Code] FROM [Basis_OTHERB] where sequence = 139 and simulationId = " + simulationId + ")  AND simulationId = " + simulationId + " and scenario = '" + scenario + "' ");
                sqlQuery.AppendLine(" ");
                sqlQuery.AppendLine("						) as g ");
                sqlQuery.AppendLine("						GROUP BY month) as teyld ON ");
                sqlQuery.AppendLine("				teyld.month = ep.month ");
                sqlQuery.AppendLine("				 ");
                sqlQuery.AppendLine("					  ");
                sqlQuery.AppendLine("				WHERE sc.exclude = 0 AND bb.exclude = 0 AND bb.Cat_Type in (0,1,5)  ");
                sqlQuery.AppendLine("				) as sm) as g  ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("				GROUP BY g.monthGrouping, g.isAsset  ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("				UNION ALL  ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("				SELECT   ");
                sqlQuery.AppendLine("						'0-3<br>Months' as monthGrouping  ");
                sqlQuery.AppendLine("						,bb.IsAsset  ");
                sqlQuery.AppendLine("						,SUM(sc.OneDaySGapBal) as balance  ");
                sqlQuery.AppendLine("						,SUM(sc.OneDaySGapRate * sc.OneDaySGapBal) / NULLIF(SUM(sc.OneDaySGapBal), 0) as rate  ");
                sqlQuery.AppendLine("				FROM (SELECT simulationId, code, oneDaySGapRate, oneDaySGapBal, exclude FROM Basis_ALS WHERE simulationId = " + simulationId + " AND scenario = '" + scenario + "') as sc   ");
                sqlQuery.AppendLine("				INNER JOIN Basis_ALB as bb ON  ");
                sqlQuery.AppendLine("				sc.SimulationId = bb.SimulationId   ");
                sqlQuery.AppendLine("				AND sc.code = bb.code  ");
                sqlQuery.AppendLine("				WHERE sc.exclude = 0 AND bb.exclude = 0  ");
                sqlQuery.AppendLine("				GROUP BY isAsset  ");
                sqlQuery.AppendLine(") as b group by b.monthGrouping, b.IsAsset");
                string q = sqlQuery.ToString();
                retTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
            }

            return retTable;
        }
    }



}