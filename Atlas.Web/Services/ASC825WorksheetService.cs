﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class ASC825WorksheetService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object ASC825WorksheetViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.ASC825Worksheets.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("ASC825 Worksheet Report Not Found");


            DataTable tblData = new DataTable();
            DataTable monthLabels = new DataTable();

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            object obj = instService.GetSimulationScenario(rep.Package.AsOfDate, sr.AsOfDateOffset, sr.SimulationTypeId, sr.ScenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();

            if (simulationId == "" || scenario == "" || asOfDate == "") {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), sr.ScenarioType.Name, sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	alb.name ");
                sqlQuery.AppendLine(" 	,alb.Sequence  ");
                sqlQuery.AppendLine(" 	,alp.month ");
                sqlQuery.AppendLine(" 	,alp.MVCashflow  ");
                sqlQuery.AppendLine(" 	,alp.MVFuncCost ");
                sqlQuery.AppendLine(" 	,alp.MVDiscount ");
                sqlQuery.AppendLine(" 	,alp.MVPeriodOut ");
                sqlQuery.AppendLine(" 	,alp.MVPValue ");
                sqlQuery.AppendLine(" 	,alp.MVMValue ");
                sqlQuery.AppendLine(" 	,alp.MVDur ");
                sqlQuery.AppendLine(" 	,alp.MVModDur  ");
                sqlQuery.AppendLine(" 	,alp.stBal ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" 	(SELECT ");
                sqlQuery.AppendLine(" 		alp17.code ");
                sqlQuery.AppendLine(" 		,alp17.SimulationId ");
                sqlQuery.AppendLine(" 		,alp17.scenario ");
                sqlQuery.AppendLine(" 		,alp17.month ");
                sqlQuery.AppendLine(" 		,alp17.MVCashflow  ");
                sqlQuery.AppendLine(" 		,alp17.MVFuncCost  ");
                sqlQuery.AppendLine(" 		,alp17.MVDiscount ");
                sqlQuery.AppendLine(" 		,alp17.MVPeriodOut ");
                sqlQuery.AppendLine(" 		,alp17.MVPValue ");
                sqlQuery.AppendLine(" 		,alp17.MVMValue ");
                sqlQuery.AppendLine(" 		,alp17.MVDur ");
                sqlQuery.AppendLine(" 		,alp17.MVModDur  ");
                sqlQuery.AppendLine(" 		,CASE WHEN alp1.month < '" + rep.Package.AsOfDate.AddMonths(1).ToShortDateString() + "' THEN alp1.stBal ELSE 0 END as stBal ");
                sqlQuery.AppendLine(" 		FROM ");
                sqlQuery.AppendLine(" 		(SELECT ");
                sqlQuery.AppendLine(" 			code ");
                sqlQuery.AppendLine(" 			,SimulationId ");
                sqlQuery.AppendLine(" 			,scenario ");
                sqlQuery.AppendLine(" 			,month ");
                sqlQuery.AppendLine(" 			,MVCashflow  ");
                sqlQuery.AppendLine(" 			,MVFuncCost  ");
                sqlQuery.AppendLine(" 			,MVDiscount ");
                sqlQuery.AppendLine(" 			,MVPeriodOut ");
                sqlQuery.AppendLine(" 			,MVPValue ");
                sqlQuery.AppendLine(" 			,MVMValue ");
                sqlQuery.AppendLine(" 			,MVDur ");
                sqlQuery.AppendLine(" 			,MVModDur  ");
                sqlQuery.AppendLine(" 			FROM Basis_ALP17 WHERE SImulationId = " + simulationId + " AND scenario = '" + scenario + "') AS alp17 ");
                sqlQuery.AppendLine(" 			INNER JOIN ");
                sqlQuery.AppendLine(" 			Basis_ALP1 AS alp1 ");
                sqlQuery.AppendLine(" 			ON alp1.code = alp17.code  ");
                sqlQuery.AppendLine(" 			AND alp1.simulationId = alp17.simulationId ");
                sqlQuery.AppendLine(" 			AND alp1.Scenario = alp17.Scenario  ");
                sqlQuery.AppendLine(" 			AND alp1.month = alp17.month) AS alp ");
                sqlQuery.AppendLine(" 		INNER JOIN Basis_ALB AS alb ON ");
                sqlQuery.AppendLine(" 		alp.code = alb.code ");
                sqlQuery.AppendLine(" 		AND alp.SimulationId = alb.SimulationId ");
                sqlQuery.AppendLine(" 		INNER JOIN Basis_ALS as als ON ");
                sqlQuery.AppendLine(" 		als.code = alp.code ");
                sqlQuery.AppendLine(" 		AND als.SimulationId = alp.SimulationId  ");
                sqlQuery.AppendLine(" 		AND als.Scenario = alp.scenario  ");
                sqlQuery.AppendLine(" 		WHERE als.exclude = 0  ");
                sqlQuery.AppendLine(" 		AND alb.exclude = 0 ");
                sqlQuery.AppendLine("       AND alb.cat_type IN(0,1,5) ");
                sqlQuery.AppendLine(" 		AND ((alb.Acc_TypeOR = 2 ");
                sqlQuery.AppendLine(" 		AND alb.ClassOR NOT IN (0,11,12)) ");
                sqlQuery.AppendLine(" 		OR (alb.Acc_TypeOR in (8,6) AND alb.ClassOr <> 0)) ");
                sqlQuery.AppendLine(" 		ORDER BY alb.Sequence  ");

                tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());
                monthLabels = Utilities.ProjectionLabels(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), "Basis_ALP17", simulationId, scenario, "", asOfDate);
            }
                    

            return new
            {
                tableData = tblData,
                monthLabels = monthLabels,
                warnings = warnings,
                errors = errors
            };
        }
    }
}