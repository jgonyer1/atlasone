﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class InvestmentPortfolioValuationService
    {
        public object InvestmentPortfolioValuationViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.InvestmentPortfolioValuations.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("Investment Portfolio Valuation Not Found");

            string scenInClause = "(";
            object[] scens = new object[sr.Scenarios.Count];

            int counter = 0;
            foreach (InvestmentPortfolioValuationScenario scen in sr.Scenarios.OrderBy(s=> s.Priority))
            {
                if (scenInClause == "(")
                {
                    scenInClause += "'" + scen.DDWName + "'" ;
                }
                else
                {
                    scenInClause += ",'" + scen.DDWName + "'";
                }
                scens[counter] = new { ddwName = scen.DDWName, dispName = scen.Name };
                counter += 1;
            }
            scenInClause += ")";

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            var info = instService.GetInformation(rep.Package.AsOfDate, sr.AsOfDateOffset);
            string asOfDate = info.AsOfDate.ToShortDateString();
         
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	im.scenario ");
            sqlQuery.AppendLine(" 	,SUM(i.parValue) as ParValue ");
            sqlQuery.AppendLine(" 	,SUM(i.BookValue) as BookValue ");
            sqlQuery.AppendLine(" 	,SUM((i.ParValue * im.Price) / 100) as marketValue ");
            sqlQuery.AppendLine(" 	,SUM((i.ParValue * im.Price) / 100) - SUM(i.BookValue) as GainLoss ");
            sqlQuery.AppendLine(" 	FROM (SELECT ");
            sqlQuery.AppendLine(" 			cusip ");
            sqlQuery.AppendLine(" 			,ParValue ");
            sqlQuery.AppendLine(" 			,BookValue ");
            sqlQuery.AppendLine(" 			FROM Investment WHERE asOfDate = '" + asOfDate + "') as i ");
            sqlQuery.AppendLine(" 			INNER JOIN  ");
            sqlQuery.AppendLine(" 			(SELECT ");
            sqlQuery.AppendLine(" 				cusip ");
            sqlQuery.AppendLine(" 				,parValue ");
            sqlQuery.AppendLine(" 				,scenario as scenario ");
            sqlQuery.AppendLine(" 				,price ");
            sqlQuery.AppendLine(" 				FROM InvestmentMarketValue WHERE asOfdate = '" + asOfDate + "' AND ScenarioName not like '%FWD%' and scenario in " + scenInClause + ") as im ");
            sqlQuery.AppendLine(" 				ON im.cusip = i.cusip AND im.parValue = i.parValue ");
            sqlQuery.AppendLine(" 				GROUP BY im.scenario ");
            sqlQuery.AppendLine(" 				ORDER BY im.scenario ");

            DataTable tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());
            return new
            {
                tblData = tblData,
                scens = scens
                
            };
        }
    }
}