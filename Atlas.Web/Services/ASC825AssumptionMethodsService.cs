﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class ASC825AssumptionMethodsService
    {
        public object ASC825AssumptionMethodsViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var c = gctx.ASC825AssumptionMethods.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (c == null) throw new Exception("ASC825 Data Source Not Found");

            var instService = new InstitutionService(c.InstitutionDatabaseName);
            var info = instService.GetInformation(rep.Package.AsOfDate, c.AsOfDateOffset);

            var pol = gctx.ModelSetups.Where(s => s.AsOfDate == info.AsOfDate);

            string loanType = "N/A";

            if (pol.Count() > 0){
                loanType = pol.FirstOrDefault().PrepaymentType;
                if (loanType == "BK")
                {
                    loanType = "Black Knight Financial Services (BKFS).";
                }
                else{
                    loanType = "The Client.";
                }
            } 


            return new
            {
                asOfDate = info.AsOfDate.ToShortDateString(),
                instName = Utilities.InstName(c.InstitutionDatabaseName),
                loanType = loanType,
                enhanced = c.Enhanced            
            };
        }
    }
}