﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class ASC825MarketValueService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object ASC825MarketValueViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.ASC825MarketValues.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("ASC825 Market Value Not Found");

            string eveInClause = "(";
            string[] eveScens = new string[sr.EveScenarioTypes.Count];

            int counter = 0;
            foreach (ASC825MarketValueScenarioType scen in sr.EveScenarioTypes.OrderBy(s => s.Priority)) {
                if (eveInClause == "(") {
                    eveInClause += scen.ScenarioTypeId;
                }
                else {
                    eveInClause += "," + scen.ScenarioTypeId;
                }
                eveScens[counter] = scen.ScenarioType.Name;
                counter += 1;
            }
            eveInClause += ")";


            DataTable tblData = new DataTable();

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            var sim = instService.GetSimulation(rep.Package.AsOfDate, sr.AsOfDateOffset, sr.SimulationType);
            if (sim == null) {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                string simulationId = sim.Id.ToString();
                string asOfDate = sim.Information.AsOfDate.ToShortDateString();
                if (simulationId == "" || asOfDate == "") {
                    errors.Add(String.Format(Utilities.GetErrorTemplate(2), sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                }
                else {
                    asOfDate = new DateTime(DateTime.Parse(asOfDate).AddMonths(1).Year, DateTime.Parse(asOfDate).AddMonths(1).Month, 1).ToShortDateString();

                    StringBuilder sqlQuery = new StringBuilder();
                    sqlQuery.AppendLine(" SELECT ");
                    sqlQuery.AppendLine(" 	alb.name ");
                    sqlQuery.AppendLine(" 	,alb.IsAsset ");
                    sqlQuery.AppendLine(" 	,alb.cat_type ");
                    sqlQuery.AppendLine(" 	,ast.name as scenario");
                    sqlQuery.AppendLine(" 	,alpData.discount ");
                    sqlQuery.AppendLine("   ,alpData.stBal  as parValue ");
                    sqlQuery.AppendLine(" 	,CASE  ");
                    sqlQuery.AppendLine(" 		WHEN ALS.parBook <> 0 THEN  ");
                    sqlQuery.AppendLine(" 			alpData.stBal  ");
                    sqlQuery.AppendLine(" 		ELSE ");
                    sqlQuery.AppendLine(" 			CASE  ");
                    sqlQuery.AppendLine(" 				WHEN ALS.Start_Par = 0 AND alpData.steBal <> 0 THEN ");
                    sqlQuery.AppendLine(" 					alpData.steBal ");
                    sqlQuery.AppendLine(" 				ELSE ");
                    sqlQuery.AppendLine(" 					als.start_Par ");
                    sqlQuery.AppendLine(" 			END ");
                    sqlQuery.AppendLine(" 	END as bookValue  ");
                    sqlQuery.AppendLine(" 	,AlS.mvAmount ");
                    sqlQuery.AppendLine(" 	,AlS.MvRatio ");
                    sqlQuery.AppendLine(" 	,ALS.mvMethod ");
                    sqlQuery.AppendLine(" 	,ALS.MVRatioOverride  ");
                    sqlQuery.AppendLine("   ,alpData.MVCashflow ");
                    sqlQuery.AppendLine("   ,CASE   ");
                    sqlQuery.AppendLine("       WHEN ALS.Start_Par = 0 AND alpData.steBal <> 0 THEN  ");
                    sqlQuery.AppendLine("           alpData.steBal  ");
                    sqlQuery.AppendLine("       ELSE  ");
                    sqlQuery.AppendLine("           als.start_Par  ");
                    sqlQuery.AppendLine("   END as bookVal ");
                    sqlQuery.AppendLine(" 	FROM	 ");
                    sqlQuery.AppendLine(" 	(SELECT ");
                    sqlQuery.AppendLine(" 		ISNULL(alp1.Code, alp17.code) as code ");
                    sqlQuery.AppendLine(" 		,ISNULL(alp1.simulationId, alp17.simulationId) as simulationId ");
                    sqlQuery.AppendLine(" 		,ISNULL(alp1.scenario, alp17.scenario) as scenario ");
                    sqlQuery.AppendLine(" 		,alp1.stBal ");
                    sqlQuery.AppendLine(" 		,alp1.steBal ");
                    sqlQuery.AppendLine(" 		,alp17.MvCashflow ");
                    sqlQuery.AppendLine(" 		,alp17.discount ");
                    sqlQuery.AppendLine(" 		FROM ");
                    sqlQuery.AppendLine(" 		(SELECT ");
                    sqlQuery.AppendLine(" 			code ");
                    sqlQuery.AppendLine(" 			,simulationId ");
                    sqlQuery.AppendLine(" 			,scenario ");
                    sqlQuery.AppendLine(" 			,stBal ");
                    sqlQuery.AppendLine(" 			,steBal ");
                    sqlQuery.AppendLine(" 		FROM Basis_ALP1 ");
                    sqlQuery.AppendLine(" 		WHERE simulationId = " + simulationId + " AND month = '" + asOfDate + "') AS ALP1 ");
                    sqlQuery.AppendLine(" 		INNER JOIN ");
                    sqlQuery.AppendLine(" 		(SELECT  ");
                    sqlQuery.AppendLine(" 			simulationId     ");
                    sqlQuery.AppendLine(" 			,scenario     ");
                    sqlQuery.AppendLine(" 			,code   ");
                    sqlQuery.AppendLine(" 			,SUM(MVCashflow) as MVCashflow  ");
                    sqlQuery.AppendLine(" 			,SUM(MVCashflow * MVDiscount) / NULLIF(SUM(MVCashflow),0) as discount  ");
                    sqlQuery.AppendLine(" 			FROM Basis_ALP17  ");
                    sqlQuery.AppendLine(" 			WHERE simulationId = " + simulationId + "  ");
                    sqlQuery.AppendLine(" 			GROUP BY simulationId ,scenario ,code) AS alp17 ON ");
                    sqlQuery.AppendLine(" 			ALP1.code = alp17.code AND ");
                    sqlQuery.AppendLine(" 			ALP1.scenario = alp17.scenario ");
                    sqlQuery.AppendLine(" 			) AS alpData ");
                    sqlQuery.AppendLine("			RIGHT JOIN (SELECT code, simulationId, exclude, ParBook, Start_Par, MvRatio, MVRatioOverride, mvAmount, scenario, mvMethod FROM Basis_ALS WHERE simulationId = " + simulationId + ") AS ALS ");
                    sqlQuery.AppendLine(" 			ON als.simulationId = alpData.simulationId  ");
                    sqlQuery.AppendLine(" 			AND als.scenario = alpData.scenario  ");
                    sqlQuery.AppendLine(" 			AND als.code = alpData.code ");
                    sqlQuery.AppendLine("			RIGHT JOIN (SELECT code, cat_Type, exclude, name, simulationid, sequence, isAsset FROM Basis_ALB WHERE simulationId = " + simulationId + ") as ALB  ");
                    sqlQuery.AppendLine(" 			ON ALB.code = als.code  ");
                    sqlQuery.AppendLine(" 			AND ALB.SimulationId = als.simulationId  ");
                    sqlQuery.AppendLine(" 			LEFT JOIN Basis_Scenario AS BS  ");
                    sqlQuery.AppendLine(" 			on bs.SimulationId = als.simulationid  ");
                    sqlQuery.AppendLine(" 			AND bs.number = als.scenario  ");
                    sqlQuery.AppendLine(" 			LEFT JOIN ATLAS_ScenarioType AS ast  ");
                    sqlQuery.AppendLine(" 			ON ast.Id = bs.ScenarioTypeId  ");
                    sqlQuery.AppendLine(" ");
                    sqlQuery.AppendLine("           WHERE alb.exclude = 0  ");
                    sqlQuery.AppendLine("		    AND ISNULL(als.exclude, 0) = 0    ");
                    sqlQuery.AppendLine("		    AND alb.Cat_Type in (0,1,2,3,4,5)  ");
                    sqlQuery.AppendLine("		    AND (bs.scenarioTypeId IN " + eveInClause + "  OR als.exclude is NULL) ");
                    sqlQuery.AppendLine(" 			ORDER BY alb.sequence  ");


                    tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());
                    List<string> foundScens = new List<string>();
                    foreach (DataRow dr in tblData.Rows) {
                        if (!foundScens.Contains(dr["scenario"].ToString())) {
                            foundScens.Add(dr["scenario"].ToString());
                        }
                    }
                    foreach (ASC825MarketValueScenarioType scen in sr.EveScenarioTypes) {
                        if (!foundScens.Contains(scen.ScenarioType.Name)) {
                            warnings.Add(String.Format(Utilities.GetErrorTemplate(1), scen.ScenarioType.Name, sr.SimulationType.Name, sr.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
                        }
                    }
                    tblData.Columns.Add(new DataColumn("marketPrice", typeof(double)));

                    foreach (DataColumn dc in tblData.Columns) {
                        dc.ReadOnly = false;
                    }
                    //Calculate Market Price
                    foreach (DataRow dr in tblData.Rows) {
                        if (dr["MvMethod"].ToString() == "2" && dr["MVRatioOverride"].ToString() != "0") {
                            dr["marketPrice"] = (double.Parse(dr["BookValue"].ToString()) * double.Parse(dr["MvRatio"].ToString())) / 100;
                        }
                        else {
                            if (dr["ParValue"].ToString() != "0" && dr["ParValue"].ToString() != "") {
                                dr["marketPrice"] = (double.Parse(dr["MVAmount"].ToString()) / double.Parse(dr["BookValue"].ToString())) * 100;
                            }

                        }
                    }

                    var totalingItems = new LinkedList<BalanceSheetTotaler.TotalingItem>();
                    totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("ParValue"));
                    totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("marketPrice", BalanceSheetTotaler.TotalingType.Average, "parValue"));
                    totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("MVAmount"));
                    totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("BookValue"));
                    //Market Value
                    if (sr.ReportType == "2") {
                        totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("discount", BalanceSheetTotaler.TotalingType.Average, "bookValue"));
                    }
                    else//Market Price
            {
                        totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("discount", BalanceSheetTotaler.TotalingType.Average, "parValue"));
                    }



                    tblData = BalanceSheetTotaler.CalcTotalsFor(tblData, totalingItems, BalanceSheetTotaler.ViewByType.AccountTypes, groupByCol: "scenario");
                }
            }
            



            return new
            {
                tblData = tblData,
                scens = eveScens,
                reportType = sr.ReportType,
                errors = errors,
                warnings = warnings

            };
        }
    }
}