﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class BalanceSheetMixService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object BalanceSheetMixViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            var balanceSheetMix = gctx.BalanceSheetMixes.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            if (balanceSheetMix == null) throw new Exception("BalanceSheetMix Not Found");

            GlobalController gc = new GlobalController();
            Dictionary<string, DataTable> policyOffsets = new Dictionary<string, DataTable>();
            policyOffsets = gc.PolicyOffsets(balanceSheetMix.InstitutionDatabaseName);

            DataTable curData = new DataTable();
            string aod = "";
            string offsetAOD = "";
            var instService = new InstitutionService(balanceSheetMix.InstitutionDatabaseName);
            var curInfo = instService.GetSimulation(rep.Package.AsOfDate, balanceSheetMix.AsOfDateOffset , balanceSheetMix.SimulationType);
            var priorInfo = instService.GetSimulation(rep.Package.AsOfDate, balanceSheetMix.PriorAsOfDateOffset, instService.GetSimulationType(balanceSheetMix.PriorSimulationTypeId));

            if (curInfo == null) {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), balanceSheetMix.SimulationType.Name, balanceSheetMix.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else if (priorInfo == null) {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), balanceSheetMix.SimulationType.Name, balanceSheetMix.PriorAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {



                if (!balanceSheetMix.OverrideSimulationId)
                {
                    if (policyOffsets.Keys.Contains(curInfo.Information.AsOfDate.ToString("MM/dd/yyyy")))
                    {
                        balanceSheetMix.SimulationTypeId = Convert.ToInt32(policyOffsets[curInfo.Information.AsOfDate.ToString("MM/dd/yyyy")].Rows[0]["niiId"].ToString());
                    }

                }


                if (!balanceSheetMix.OverridePriorSimulationId)
                {
                    if (policyOffsets.Keys.Contains(priorInfo.Information.AsOfDate.ToString("MM/dd/yyyy")))
                    {
                        balanceSheetMix.PriorSimulationTypeId = Convert.ToInt32(policyOffsets[priorInfo.Information.AsOfDate.ToString("MM/dd/yyyy")].Rows[0]["niiId"].ToString());
                    }

                }




                aod = curInfo.Information.AsOfDate.ToShortDateString();
                offsetAOD = priorInfo.Information.AsOfDate.ToShortDateString();
                
                DateTime curDate = new DateTime(curInfo.Information.AsOfDate.Year, curInfo.Information.AsOfDate.Month, 1);
                DateTime priorDate = new DateTime(priorInfo.Information.AsOfDate.Year, priorInfo.Information.AsOfDate.Month, 1);

                //Custom Groupings SQL that will be used when grouping categories
                StringBuilder customGroupings = new StringBuilder();


                //Hanldes Loans Custom Groupings
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	cl.Name ");
                sqlQuery.AppendLine(" 	,alb.Acc_TypeOR  ");
                sqlQuery.AppendLine(" 	,alb.ClassOR  ");
                sqlQuery.AppendLine(" 	,ISNULL(SUM(ala.ENd_Bal),0) as balance ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" (SELECT code, classOr, isAsset, Acc_TypeOr FROM basis_ALB where simulationid =  " + curInfo.Id.ToString() + " and acc_typeOr = 2 and Cat_Type in (0,1,5) AND exclude <> 1) as alb ");
                sqlQuery.AppendLine(" LEFT JOIN ");
                sqlQuery.AppendLine(" (SELECT code, end_Bal FROM Basis_ALA WHERE simulationId = " + curInfo.Id.ToString() + " AND month = '" + curDate.ToShortDateString() + "') as ala ");
                sqlQuery.AppendLine(" ON ala.code = alb.code ");
                sqlQuery.AppendLine(" INNER JOIN Atlas_Classification as cl ");
                sqlQuery.AppendLine(" ON cl.IsAsset = alb.isAsset ");
                sqlQuery.AppendLine(" AND cl.AccountTypeId = alb.Acc_TypeOR  ");
                sqlQuery.AppendLine(" AND cl.RbcTier = alb.classOR ");
                sqlQuery.AppendLine(" GROUP BY cl.Name, alb.Acc_TypeOR, alb.classOr ");
                sqlQuery.AppendLine(" ORDER BY balance DESC ");
                DataTable topLoans = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(balanceSheetMix.InstitutionDatabaseName), sqlQuery.ToString());

                for (int i = 0; i < topLoans.Rows.Count; i++) {
                    DataRow dr = topLoans.Rows[i];
                    if (i < 3) {
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, {5} as priority ", "1", dr["Acc_TypeOR"].ToString(), dr["ClassOR"].ToString(), "Loans", dr["Name"].ToString(), 5 + i));
                    }
                    else {
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 8 as priority  ", "1", dr["Acc_TypeOR"].ToString(), dr["ClassOR"].ToString(), "Loans", "Other"));
                    }
                }


                //Handles Radio Buttom Options

                switch (balanceSheetMix.LiabilityOption) {
                    case "0":
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 10 as Priority", "0", "3", "15", "Brokered Deposits", "Brokered Deposits"));
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 10 as Priority", "0", "8", "9", "Brokered Deposits", "Brokered Deposits"));
                        break;
                    case "1":
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 4 as Priority ", "0", "3", "15", "Non-Maturity", "MMDA"));
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 6 as Priority ", "0", "8", "9", "Time Deposits", "Time Deposits"));
                        break;
                    case "2":
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 9 as Priority ", "0", "3", "15", "Brokered Reciprocal Deposits", "Brokered Reciprocal Deposits"));
                        customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 9 as Priority ", "0", "8", "9", "Brokered Reciprocal Deposits", "Brokered Reciprocal Deposits"));
                        break;
                }


                //Handles National Cd Checkbox
                if (balanceSheetMix.NationalCD) {
                    customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 8 as Priority ", "0", "8", "6", "National CD", "National CD"));
                }
                else {
                    customGroupings.AppendLine(string.Format(" UNION ALL SELECT {0} as isAsset, {1} as acc_type, {2} as rbc_tier, '{3}' as parentName, '{4}' as Name, 6 as Priority ", "0", "8", "6", "Time Deposits", "Time Deposits"));
                }

                curData = BalanceSheetClassifications(curInfo.Id, curDate, priorInfo.Id, priorDate, customGroupings.ToString(), balanceSheetMix.InstitutionDatabaseName);
            }

                 

            return new
            {
                AsOfDateOffset = balanceSheetMix.AsOfDateOffset,
                InstitutionName = Utilities.InstName(balanceSheetMix.InstitutionDatabaseName),
                SimulationName = balanceSheetMix.SimulationType.Name,
                tableData = curData,
                AsOfDate = aod,
                OffSetDate = offsetAOD,
                warnings = warnings,
                errors = errors
            };
        }

        private DataTable BalanceSheetClassifications(int curSimulationId, DateTime curAsOfDate, int priorSimulationId, DateTime priorAsOfDate, string customGroupings, string database)
        {
            StringBuilder sqlQuery = new StringBuilder();
            
            sqlQuery.AppendLine("SELECT ISNULL(cur.IsAsset, pr.IsAsset) as IsAsset ");
            sqlQuery.AppendLine("    ,ISNULL(cur.parentName, pr.parentName) as ParentName ");
            sqlQuery.AppendLine("    ,ISNULL(cur.name, pr.name) as Name ");
            sqlQuery.AppendLine("    ,ISNULL(cur.priority, pr.priority) as priority ");
            sqlQuery.AppendLine("    ,cur.Balance as curBalance ");
            sqlQuery.AppendLine("    ,pr.Balance as priorBalance ");
            sqlQuery.AppendLine(" FROM ");
            sqlQuery.AppendLine(" (SELECT ");
            sqlQuery.AppendLine(" 	cl.IsAsset   ");
            sqlQuery.AppendLine(" 	,cl.parentName as parentName ");
            sqlQuery.AppendLine(" 	,cl.Name as name ");
            sqlQuery.AppendLine(" 	,SUM(ala.End_Bal) as balance ");
            sqlQuery.AppendLine("   ,Max(Priority) as priority ");
            sqlQuery.AppendLine(" FROM ");
            sqlQuery.AppendLine(" (SELECT code, end_bal, simulationId FROM Basis_ALA WHERE simulationID = " + curSimulationId.ToString() + " and month = '" + curAsOfDate.ToShortDateString() + "') as ala ");
            sqlQuery.AppendLine(" INNER JOIN (SELECT SimulationId, code, isAsset, acc_TypeOR as accType, ClassOR as rbcTier FROM Basis_ALB WHERE SimulationId =  " + curSimulationId.ToString() + " and (Cat_Type = 0 OR cat_Type = 1 OR cat_Type = 5) and exclude <> 1) as alb ");
            sqlQuery.AppendLine(" ON alb.code = ala.code AND alb.simulationId = ala.simulationId ");
            sqlQuery.AppendLine(" INNER JOIN (SELECT isAsset, acc_Type, rbc_tier, parentName, name, priority FROM ATLAS_BalanceSheetMixLookUp " + customGroupings + ") AS cl ");
            sqlQuery.AppendLine(" ON cl.acc_Type = alb.accType  ");
            sqlQuery.AppendLine(" AND cl.rbc_Tier = alb.rbcTier ");
            sqlQuery.AppendLine(" AND cl.isAsset = alb.IsAsset ");
            sqlQuery.AppendLine(" GROUP BY cl.IsAsset, cl.parentNAme, cl.name) as cur ");
            sqlQuery.AppendLine(" FULL OUTER JOIN ");
            sqlQuery.AppendLine(" (SELECT ");
            sqlQuery.AppendLine(" 	cl.IsAsset   ");
            sqlQuery.AppendLine(" 	,cl.parentName as parentName ");
            sqlQuery.AppendLine(" 	,cl.Name as name ");
            sqlQuery.AppendLine(" 	,SUM(ala.End_Bal) as balance ");
            sqlQuery.AppendLine("   ,Max(Priority) as priority ");
            sqlQuery.AppendLine(" FROM ");
            sqlQuery.AppendLine(" (SELECT code, end_bal, simulationId FROM Basis_ALA WHERE simulationID = " + priorSimulationId.ToString() + " and month = '" + priorAsOfDate.ToShortDateString() + "') as ala ");
            sqlQuery.AppendLine(" INNER JOIN (SELECT SimulationId, code, isAsset, acc_TypeOR as accType, ClassOR as rbcTier FROM Basis_ALB WHERE SimulationId = " + priorSimulationId.ToString() + " and (Cat_Type = 0 OR cat_Type = 1 OR cat_Type = 5) and exclude <> 1) as alb ");
            sqlQuery.AppendLine(" ON alb.code = ala.code AND alb.simulationId = ala.simulationId ");
            sqlQuery.AppendLine(" INNER JOIN (SELECT isAsset, acc_Type, rbc_tier, parentName, name, priority FROM ATLAS_BalanceSheetMixLookUp " + customGroupings + ") AS cl ");
            sqlQuery.AppendLine(" ON cl.acc_Type = alb.accType  ");
            sqlQuery.AppendLine(" AND cl.rbc_Tier = alb.rbcTier ");
            sqlQuery.AppendLine(" AND cl.isAsset = alb.IsAsset ");
            sqlQuery.AppendLine(" GROUP BY cl.IsAsset, cl.parentNAme, cl.name) as pr ");
            sqlQuery.AppendLine(" ON pr.IsAsset = cur.IsAsset AND pr.parentName = cur.parentName AND pr.name = cur.Name");
            sqlQuery.AppendLine(" ORDER BY ISNULL(cur.Isasset, pr.IsAsset), ISNULL(cur.priority, pr.priority) ");


            return Utilities.ExecuteSql(Utilities.BuildInstConnectionString(database), sqlQuery.ToString());
        }
    }
}