﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class EveNevAssumptionsService
    {
        public object EveNevAssumptionsViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var c = gctx.EveNevAssumptions.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (c == null) throw new Exception("ASC825 Eve Nev Assumptions Not Found");

            var instService = new InstitutionService(c.InstitutionDatabaseName);
            var info = instService.GetInformation(rep.Package.AsOfDate, c.AsOfDateOffset);

            var mod = gctx.ModelSetups.Where(s => s.AsOfDate == info.AsOfDate);

            string loanType = "N/A";
            string investmentType = "N/A";

            if (mod.Count() > 0)
            {
                loanType = mod.FirstOrDefault().PrepaymentType;
                if (loanType == "BK")
                {
                    loanType = "Black Knight Financial Services (BKFS).";
                }
                else{
                    loanType = "The Client.";
                }

                investmentType = mod.FirstOrDefault().InvestmentType; 
            } 

            return new
            {
                asOfDate = info.AsOfDate.ToShortDateString(),
                instName = Utilities.InstName(c.InstitutionDatabaseName),
                loanType = loanType,
                investmentType = investmentType,
                enhanced = c.EnhancedEve,
                depositType = c.DepositType,
                depositNotes = c.DepositNotes
            };
        }
    }
}