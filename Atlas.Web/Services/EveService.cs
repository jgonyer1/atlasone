﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using P360.Web.Controllers;
namespace Atlas.Web.Services
{
    public class EveService
    {
       
       public List<string> errors = new List<string>();
       public List<string> warnings = new List<string>();
        public object GetConsolidatedEveData(int eveId)
        {

            InstitutionContext _gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            var eve = _gctx.Eves.Where(s => s.Id == eveId).FirstOrDefault();
            var rep = _gctx.Reports.Where(s => s.Id == eveId).FirstOrDefault();
            GlobalController globalController = new GlobalController();
            //Get Information For Institution
            DateTime offsetAsOfDate = globalController.OffsetAsOfDate(eve.InstitutionDatabaseName, eve.AsOfDateOffset);
            Dictionary<string, DataTable> policyOffsets = globalController.PolicyOffsets(eve.InstitutionDatabaseName);
            string aod = offsetAsOfDate.ToString("MM/dd/yyyy");

            int? polEveSimTypeId;
            DataTable policies;

            if(! policyOffsets.TryGetValue(aod, out policies))
            {
                polEveSimTypeId = null;
            }
            else
            {
                polEveSimTypeId = Convert.ToInt32(policies.Rows[0]["eveId"].ToString());
            }

            SimulationType simTypeToUse;
            
            if (eve.OverrideSimulationId)
            {
                simTypeToUse = eve.SimulationType;

            }
            else if(! polEveSimTypeId.HasValue)
            {
                throw new ArgumentOutOfRangeException("As of date", "Policy information for date " + aod + " doesn't exist and simulation override is not set.");
            }
            else
            {
                simTypeToUse = _gctx.SimulationTypes.Where(s => s.Id == polEveSimTypeId).FirstOrDefault();
            }

            //SimulationType simulationToUse = _gctx.SimulationTypes.Where(s => s.Id = )
            var instService = new InstitutionService(eve.InstitutionDatabaseName);
            var simulation = instService.GetSimulation(rep.Package.AsOfDate, eve.AsOfDateOffset, eve.SimulationType);

            if(simulation == null)
            {
                throw new ArgumentOutOfRangeException("As of date", "Simulation information for date " + aod + " doesn't exist.");
            }            

            bool ncua = false;
            int defiendGroupId = 0;

            if (eve.ViewOption != "1")
            {
                defiendGroupId = int.Parse(eve.ViewOption.Substring(3)); 
            }

            if (eve.ReportFormat == "2")
            {
                ncua = true;
            }

            DataTable eveScens = new DataTable();
            DataTable eveBook = new DataTable();
            DataTable eveChangeLimit = new DataTable();
            DataTable eveRatioLimit = new DataTable();
            DataTable eveBPChangeRatio = new DataTable();

            Dictionary<string, double> CalcedEvePolicyRatioValues = PolicyCalculations.EvePoliciesValues(eve.SimulationType, new DateTime(simulation.Information.AsOfDate.Year, simulation.Information.AsOfDate.Month, simulation.Information.AsOfDate.Day), eve.InstitutionDatabaseName, ncua, defiendGroupId);
            
            string scenIn = "(";
            object[] scens = new object[eve.EveScenarioTypes.Count];

            int counter = 0;
            foreach (var eveSim in eve.EveScenarioTypes.OrderBy(s => s.Priority))
            {
                scens[counter] = new
                {
                    name = eveSim.ScenarioType.Name
                };
                if (scenIn == "(")
                {
                    scenIn += eveSim.ScenarioTypeId.ToString();
                }
                else
                {
                    scenIn += "," + eveSim.ScenarioTypeId;
                }
                counter += 1;
            }
            scenIn += ')';

            if (simulation == null) {
                errors.Add(String.Format(Utilities.GetErrorTemplate(2), eve.SimulationType.Name, eve.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            else {
                var lastProjectionBucketDate = instService.LastProjectionBucketDate(simulation.Id);
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	ast.name as scenarioName ");
                sqlQuery.AppendLine(" 	,alb.isAsset ");


                switch (eve.ViewOption) {
                    case "1":
                        sqlQuery.AppendLine(" 	,ag.name as accountName ");
                        break;
                    default:
                        sqlQuery.AppendLine("  ,dgs.name as accountName");
                        break;
                }

                if (ncua)
                {
                    sqlQuery.AppendLine(" 	,SUM(CASE WHEN alb.acc_TypeOr = 3 THEN CASE WHEN nc.percentage is NULL THEN ala.End_Bal ELSE ala.End_Bal * nc.percentage END ELSE  MVMValue END) as marketValue ");
                }
                else
                {
                    sqlQuery.AppendLine(" 	,SUM(MVMValue) as marketValue ");
                }
             
                sqlQuery.AppendLine(" 	,SUM(ala.End_Bal) as bookValue ");
                sqlQuery.AppendLine(" FROM (SELECT simulationId, code, scenario, MvAmount as MVMValue FROM Basis_ALS WHERE SimulationId = " + simulation.Id.ToString() + ") as als  ");
                sqlQuery.AppendLine(" LEFT JOIN ");
               
                sqlQuery.AppendLine(" (SELECT simulationId, code,End_Bal FROM Basis_ALA WHERE SimulationId = " + simulation.Id.ToString() + " AND month = '" + Utilities.GetFirstOfMonthStr(simulation.Information.AsOfDate.ToShortDateString()) + "') as ala  ");
                sqlQuery.AppendLine(" ON ala.code = als.code ");
                sqlQuery.AppendLine(" INNER JOIN ");             
                sqlQuery.AppendLine(" Basis_ALB AS alb ON ");
                sqlQuery.AppendLine(" alb.simulationId = als.SimulationId AND ");
                sqlQuery.AppendLine(" alb.code = als.code ");
                sqlQuery.AppendLine(" INNER JOIN Basis_ALS AS bals ON ");
                sqlQuery.AppendLine(" bals.code = als.code AND ");
                sqlQuery.AppendLine(" bals.Scenario = als.Scenario AND ");
                sqlQuery.AppendLine(" bals.simulationId = als.simulationid ");




                switch (eve.ViewOption) {
                    case "1":

                        sqlQuery.AppendLine(" INNER JOIN ");
                        sqlQuery.AppendLine(" ATLAS_Classification AS ac ON ");
                        sqlQuery.AppendLine(" ac.IsAsset = alb.IsAsset AND ");
                        sqlQuery.AppendLine(" ac.AccountTypeId = alb.Acc_TypeOR AND  ");
                        sqlQuery.AppendLine(" ac.RbcTier = alb.ClassOr ");
                        sqlQuery.AppendLine(" INNER JOIN ");
                        sqlQuery.AppendLine(" ATLAS_AccountType AS ag ON ");
                        sqlQuery.AppendLine(" alb.acc_typeOR = ag.Id  ");
                        sqlQuery.AppendLine(" INNER JOIN ATLAS_AccountTypeOrder AS acto ON acto.isAsset = alb.isAsset AND acto.acc_type = alb.acc_typeOr ");
                        break;
                    default:
                        int id = int.Parse(eve.ViewOption.Substring(3));
                        sqlQuery.AppendLine(" INNER JOIN ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine(" ");
                        sqlQuery.AppendLine(" (SELECT   ");
                        sqlQuery.AppendLine(" 	dgg.name  ");
                        sqlQuery.AppendLine(" 	,dgg.[Priority]  ");
                        sqlQuery.AppendLine(" 	,dgc.IsAsset   ");
                        sqlQuery.AppendLine(" 	,dgc.Acc_Type   ");
                        sqlQuery.AppendLine(" 	,dgc.RbcTier   ");
                        sqlQuery.AppendLine("   FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg  ");
                        sqlQuery.AppendLine("   INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg  ");
                        sqlQuery.AppendLine("   ON dg.id = dgg.DefinedGroupingId   ");
                        sqlQuery.AppendLine("   INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc  ");
                        sqlQuery.AppendLine("   ON dgg.id = dgc.DefinedGroupingGroupId   ");
                        sqlQuery.AppendLine("   WHERE dg.id = " + id.ToString() + ") as dgs ON  ");
                        sqlQuery.AppendLine("   dgs.acc_Type = alb.acc_Type AND dgs.rbcTier = alb.rbcTier AND dgs.IsAsset = alb.isAsset  ");

                        break;
                }


                sqlQuery.AppendLine(" INNER JOIN Basis_Scenario AS bs ON ");
                sqlQuery.AppendLine(" bs.SimulationId = als.SimulationId  AND ");
                sqlQuery.AppendLine(" bs.number = als.Scenario  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType AS ast ON ");
                sqlQuery.AppendLine(" ast.id = bs.ScenarioTypeId  ");
                sqlQuery.AppendLine(" INNER JOIN [" + Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() + "].dbo.ATLAS_EveScenarioType AS aest ON aest.scenarioTypeid = ast.id ");

                if (ncua)
                {
                    sqlQuery.AppendLine("LEFT JOIN ATLAS_NCUAHelper as nc ");
                    sqlQuery.AppendLine(" ON ast.Id = nc.scenarioTypeId and alb.Acc_TypeOR = nc.AccountTypeId ");
                }


                sqlQuery.AppendLine(" WHERE als.simulationid = " + simulation.Id.ToString() + " AND alb.Cat_Type IN (0,1,5) AND alb.exclude = 0 AND bals.Exclude = 0 AND alb.acc_TypeOR <> 4 AND aest.EveId = " + eve.Id + " ");

                switch (eve.ViewOption) {
                    case "1":
                        sqlQuery.AppendLine("  GROUP BY ast.Name, alb.IsAsset, ag.Name, acto.[Order], aest.Priority ");
                        sqlQuery.AppendLine(" ORDER BY aest.Priority, alb.isAsset, acto.[Order] ");
                        break;
                    default:
                        sqlQuery.AppendLine("  GROUP BY ast.Name, alb.IsAsset, dgs.name, aest.Priority, dgs.Priority ");
                        sqlQuery.AppendLine(" ORDER BY aest.Priority, alb.isAsset, dgs.Priority  ");
                        break;
                }



                eveScens = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(eve.InstitutionDatabaseName), sqlQuery.ToString());

                //check to see if we found data for all scenarios in the list
                List<string> foundScens = new List<string>();
                foreach (DataRow r in eveScens.Rows) {
                    if (!foundScens.Contains(r["scenarioName"])) {
                        foundScens.Add(r["scenarioName"].ToString());
                    }
                }
                if (foundScens.Count < eve.EveScenarioTypes.Count) {
                    foreach (EveScenarioType eveSc in eve.EveScenarioTypes) {
                        if (!foundScens.Contains(eveSc.ScenarioType.Name)) {
                            warnings.Add(String.Format(Utilities.GetErrorTemplate(2), eveSc.ScenarioType.Name, simulation.Name, rep.Package.AsOfDate));
                        }
                    }
                }

                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	alb.IsAsset ");

                switch (eve.ViewOption) {
                    case "1":
                        sqlQuery.AppendLine(" 	,ag.name as accountName ");
                        break;
                    default:
                        sqlQuery.AppendLine(" ,dgs.name as accountName ");
                        break;
                }





                sqlQuery.AppendLine(" 	,SUM(End_Bal) as bookValue ");
                sqlQuery.AppendLine(" FROM basis_ALA as ala  ");
                sqlQuery.AppendLine(" INNER JOIN ");
                sqlQuery.AppendLine(" Basis_ALB AS alb ON ");
                sqlQuery.AppendLine(" alb.simulationId = ala.SimulationId AND ");
                sqlQuery.AppendLine(" alb.code = ala.code ");


                switch (eve.ViewOption) {
                    case "1":
                        sqlQuery.AppendLine(" INNER JOIN ");
                        sqlQuery.AppendLine(" ATLAS_Classification AS ac ON ");
                        sqlQuery.AppendLine(" ac.IsAsset = alb.IsAsset AND ");
                        sqlQuery.AppendLine(" ac.AccountTypeId = alb.Acc_TypeOR AND  ");
                        sqlQuery.AppendLine(" ac.RbcTier = alb.ClassOr ");
                        sqlQuery.AppendLine(" INNER JOIN ");
                        sqlQuery.AppendLine(" ATLAS_AccountType AS ag ON  ");
                        sqlQuery.AppendLine(" alb.Acc_TypeOr = ag.Id  ");

                        break;
                    default:
                        if (eve.ViewOption.IndexOf("dg") != -1) {
                            int id = int.Parse(eve.ViewOption.Substring(3));
                            sqlQuery.AppendLine("			INNER JOIN ");
                            sqlQuery.AppendLine(" ");
                            sqlQuery.AppendLine(" ");
                            sqlQuery.AppendLine("			(SELECT  ");
                            sqlQuery.AppendLine("				dgg.name ");
                            sqlQuery.AppendLine("				,dgg.[Priority] ");
                            sqlQuery.AppendLine("				,dgc.IsAsset  ");
                            sqlQuery.AppendLine("				,dgc.Acc_Type  ");
                            sqlQuery.AppendLine("				,dgc.RbcTier  ");
                            sqlQuery.AppendLine("			  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg ");
                            sqlQuery.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg ");
                            sqlQuery.AppendLine("			  ON dg.id = dgg.DefinedGroupingId  ");
                            sqlQuery.AppendLine("			  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                            sqlQuery.AppendLine("			  ON dgg.id = dgc.DefinedGroupingGroupId  ");
                            sqlQuery.AppendLine("			  WHERE dg.id = " + id.ToString() + ") as dgs ON ");
                            sqlQuery.AppendLine("			  dgs.acc_Type = alb.acc_TypeOr AND dgs.rbcTier = alb.classOr AND dgs.IsAsset = alb.isAsset ");
                        }

                        break;
                }

                sqlQuery.AppendLine(" WHERE ala.simulationid = " + simulation.Id.ToString() + " AND alb.Cat_Type IN (0,1,5) AND alb.exclude = 0 AND alb.acc_TypeOR <> 4 AND month = '" + new DateTime(simulation.Information.AsOfDate.Year, simulation.Information.AsOfDate.Month, 1).ToShortDateString() + "' ");


                switch (eve.ViewOption) {
                    case "1":
                        sqlQuery.AppendLine(" GROUP BY alb.isAsset, ag.Name, ag.id ");
                        break;
                    default:
                        sqlQuery.AppendLine(" GROUP BY alb.isAsset, dgs.Name, dgs.[Priority] ");
                        sqlQuery.AppendLine(" ORDER BY alb.IsAsset, dgs.[Priority]");
                        break;
                }

                eveBook = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(eve.InstitutionDatabaseName), sqlQuery.ToString());


                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ep.policyLimit, ats.name, ep.riskAssessment FROM ATLAS_Policy as p ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicyParent as epp ");
                sqlQuery.AppendLine(" ON p.Id = epp.PolicyId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicy as ep ");
                sqlQuery.AppendLine(" ON ep.EvePolicyParentId = epp.id ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType AS ats ON ");
                sqlQuery.AppendLine(" ats.Id = ep.ScenarioTypeId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EveScenarioType as est ON est.ScenarioTypeId = ats.id ");
                sqlQuery.AppendLine(" WHERE p.asOfdate = '" + simulation.Information.AsOfDate.ToShortDateString() + "' and epp.ShortName = 'eveChange' AND est.EveId = " + eve.Id + " ");
                sqlQuery.AppendLine(" ORDER BY est.Priority  ");
                eveChangeLimit = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(eve.InstitutionDatabaseName), sqlQuery.ToString());


                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ep.policyLimit, ats.name, ep.riskAssessment FROM ATLAS_Policy as p ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicyParent as epp ");
                sqlQuery.AppendLine(" ON p.Id = epp.PolicyId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicy as ep ");
                sqlQuery.AppendLine(" ON ep.EvePolicyParentId = epp.id ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType AS ats ON ");
                sqlQuery.AppendLine(" ats.Id = ep.ScenarioTypeId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EveScenarioType as est ON est.ScenarioTypeId = ats.id ");
                sqlQuery.AppendLine(" WHERE p.asOfdate = '" + simulation.Information.AsOfDate.ToShortDateString() + "' and epp.ShortName = 'postShock'  AND est.EveId = " + eve.Id + " ");
                sqlQuery.AppendLine(" ORDER BY est.Priority  ");
                eveRatioLimit = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(eve.InstitutionDatabaseName), sqlQuery.ToString());

                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT ep.policyLimit, ats.name, ep.riskAssessment FROM ATLAS_Policy as p ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicyParent as epp ");
                sqlQuery.AppendLine(" ON p.Id = epp.PolicyId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicy as ep ");
                sqlQuery.AppendLine(" ON ep.EvePolicyParentId = epp.id ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType AS ats ON ");
                sqlQuery.AppendLine(" ats.Id = ep.ScenarioTypeId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_EveScenarioType as est ON est.ScenarioTypeId = ats.id ");
                sqlQuery.AppendLine(" WHERE p.asOfdate = '" + simulation.Information.AsOfDate.ToShortDateString() + "' and epp.ShortName = 'bpChange' AND est.EveId = " + eve.Id + " ");
                sqlQuery.AppendLine(" ORDER BY est.Priority  ");
                eveBPChangeRatio = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(eve.InstitutionDatabaseName), sqlQuery.ToString());
            }
            

            bool creditUnion = false;
            if (Utilities.ExecuteSql(Utilities.BuildInstConnectionString(eve.InstitutionDatabaseName), "SELECT * FROM DWR_FedRates.dbo.DatabaseHighlineMapping WHERE [DatabaseName] = '" + eve.InstitutionDatabaseName + "' AND [InstType] = 'CU'").Rows.Count > 0)
            {
                creditUnion = true;
            }

            return new
            {
                eveBook = eveBook,
                eveScens = eveScens,
                scens = scens,
                eveChangeLimit = eveChangeLimit,
                eveRatioLimit = eveRatioLimit,
                eveChangeRatioLimit = eveBPChangeRatio,
                policyLimits = eve.PolicyLimits,
                riskSummaryGrid = eve.RiskSummaryTable ,
                reportFormat= eve.ReportFormat,
                showNCUA = eve.ShowNCUA,
                creditUnion = creditUnion,
                errors = errors,
                warnings = warnings,
                eveRatios = CalcedEvePolicyRatioValues
            };
           
        }
    }
}