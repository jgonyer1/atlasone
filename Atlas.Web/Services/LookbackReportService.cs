﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class LookbackReportService
    {
        public object LookbackReportViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            LookbackReport lb = gctx.LookbackReport.Where(l => l.Id == reportId).FirstOrDefault();
            Report rep = gctx.Reports.Where(r => r.Id == lb.Id).FirstOrDefault();
            Package package = gctx.Packages.Where(p => p.Id == rep.PackageId).FirstOrDefault();
            //lb.Sections = (ICollection<LookbackReportSection>)lb.Sections.OrderBy(s => s.Priority);
            GlobalController gc = new GlobalController();
            DataTable aods = gc.AsOfDates("", true, 12, false);

            var instService = new InstitutionService(lb.InstitutionDatabaseName);
            
            StringBuilder sb = new StringBuilder();
            DataTable[][] sections = new DataTable[lb.Sections.Count][];
            int sectionCount = 0;
            List<string> lbReportTypeIds = new List<string>();

            //order some stuff
            lb.Sections = lb.Sections.OrderBy(s => s.Priority).ToList();

            foreach (LookbackReportSection section in lb.Sections) {
                int lookbackCount = 0;
                int sectionGroupCount = 0;
                section.LookbackTypes = section.LookbackTypes.OrderBy(l => l.Priority).ToList();
                //sections[sectionCount] = new DataTable[section.LookbackTypes.Count];
                for (var i = 0; i < 2; i++) {
                    foreach (LookbackType lbType in section.LookbackTypes) {

                        //because copying from ATlas Template can result in lookback types that point to lookbacks that don't actually exist in any given bank...we need to try and find one that matches
                        if (rep.Defaults)
                        {
                            string aod = DateTime.Parse(aods.Rows[lbType.AsOfDateOffset]["asOfDateDescript"].ToString()).ToShortDateString();
                            int? res = LookbackGroupDataService.GetLBLookbackIdToMatchTemplate(lbType.LBLookbackId.ToString(), false, aod);
                            if(res != null && res > 0)
                            {
                                lbType.LBLookbackId = (int)res;
                            }
                        }

                        if (i == 0) {
                            lbReportTypeIds.Add(lbType.Id.ToString());
                        }

                        //need to get the LBGroups manually, system doesn't seem to want to make virtual relationships across banks
                        DataTable lbGroups = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT Id FROM ATLAS_LBLookbackGroup where LBLookbackId = " + lbType.LBLookbackId + " ORDER BY  Priority");

                        
                        
                        //then we can put the lookback group data into the array
                        foreach (DataRow dr in lbGroups.Rows) {
                            if (i > 0) {
                                DataTable temp = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT * FROM ATLAS_LBLookbackData where LBLookbackGroupId = "+dr[0] + " ORDER BY  Priority");
                                sections[sectionCount][sectionGroupCount] = temp;
                            }
                            sectionGroupCount++;
                        }
                    }
                    if (i == 0) {
                        sections[sectionCount] = new DataTable[sectionGroupCount];
                    }
                    sectionGroupCount = 0;
                }
                sectionCount++;
            }
            //save in case we actually had to update lookback ids
            gctx.SaveChanges();

            //some data about the lookback groups that we need, can't seem to get the GlobaContextProvider to cooperate for this
            //string q = "SELECT RateVol, StartDate, EndDate FROM ATLAS_LBLookbackGroup inner join ATLAS_LookbackType on ATLAS_LookbackType.LBLookbackId = ATLAS_LBLookbackGroup.LBLookbackId where ATLAS_LBLookbackGroup.LBLookbackId in (" + String.Join(",", lbIds) + ") order by ATLAS_LookbackType.Priority";
            string q = "";
            sb.Clear();
            //sb.AppendLine("SELECT ATLAS_LBLookbackGroup.id as groupId,RateVol, StartDate, EndDate FROM ATLAS_LBLookbackGroup");
            //sb.AppendLine("INNER JOIN  ATLAS_LookbackType on ATLAS_LookbackType.LBLookbackId = ATLAS_LBLookbackGroup.LBLookbackId");
            //sb.AppendLine("INNER JOIN ATLAS_LookbackReportSection on ATLAS_LookbackReportSection.id = ATLAS_LookbackType.LookbackReportSectionId");
            //sb.AppendLine("WHERE ATLAS_LBLookbackGroup.LBLookbackId in (" + String.Join(",", lbReportTypeIds) + ") ORDER BY ATLAS_LookbackReportSection.Priority,  ATLAS_LookbackType.Priority");
            sb.AppendLine("select lbg.id as groupId, lbg.RateVol, lbg.StartDate, lbg.EndDate ");
            sb.AppendLine("from ATLAS_LookbackType lbType ");
            sb.AppendLine("inner join ATLAS_LookbackReportSection sec on sec.id = lbType.LookbackReportSectionId ");
            sb.AppendLine("inner join ATLAS_LBLookback lbLookback on lbType.LBLookbackId = lbLookback.id ");
            sb.AppendLine("inner join ATLAS_LBLookbackGroup lbg on lbLookback.id = lbg.LBLookbackId ");
            sb.AppendLine("where lbType.id in (" + String.Join(",", lbReportTypeIds) + ")	 ");
            sb.AppendLine("order by sec.Priority, lbType.Priority ");
            q = sb.ToString();
            DataTable lbgData = Utilities.ExecuteSql(instService.ConnectionString(), q);
            int n = 0;
            return new {
                lbReport = lb,
                sections = sections,
                lbgData = lbgData
            };
        }

    }
}