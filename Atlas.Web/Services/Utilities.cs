﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atlas.Institution.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text;
using Atlas.Institution.Model;

namespace Atlas.Web.Services
{
    public static class Utilities
    {
        public static double SafeDoubleConvert(string inputStr)
        {
            double ret = 0;
            if(double.TryParse(inputStr, out ret))
            {
                return ret;
            }
            else
            {
                return ret;
            }
        }

        public static string BuildInstConnectionString(string institution = "")
        {
            //Generates the Institution Context For Whatever Database the User is in if not in use grab default one so it does not blow up
            string conStr = "";
            if (institution != null && institution != "")
            {
                var conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
                {
                    InitialCatalog = institution
                };
                conStr = conenctionStringBuilder.ConnectionString;
            }
            else if (Utilities.GetAtlasUserData().Rows.Count > 0 && Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() != null && Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() != "")
            {
                var conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
                {
                    InitialCatalog = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString()
                };
                conStr = conenctionStringBuilder.ConnectionString;
            }
            else
            {
                conStr = ConfigurationManager.ConnectionStrings["InstitutionContext"].ToString();
            }
            return conStr;

        }

        public static DataTable SimulationScenarioTable(bool isQuarterly, DateTime asOfDate, int asOfDateOffSet, SimulationType simulationType, string instDatabase, string scenarioTypeIn, bool taxEquiv, string dgId)
        {
            StringBuilder sqlQuery = new StringBuilder();
            InstitutionService instService = new InstitutionService(instDatabase);
            Simulation bankSim = instService.GetSimulation(asOfDate, asOfDateOffSet, simulationType);
            DataTable retTable = new DataTable();
            if (bankSim == null)
            {
                //errors.Add(String.Format(Utilities.GetErrorTemplate(2), simulationType.Name, asOfDateOffSet, asOfDate.ToShortDateString()));
            }
            else
            {
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	MAX(baseSim.month) as month ");
                sqlQuery.AppendLine("   ,baseSim.yearGroup as yearGroup ");
                sqlQuery.AppendLine(" 	,scenNames.name ");
                sqlQuery.AppendLine(" 	,SUM(CASE WHEN alb.isAsset = 1 THEN baseSim.IeTotAmount ELSE -baseSim.IETotAmount END) as Amount ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" (SELECT simulationid ");
                sqlQuery.AppendLine(" 		,month ");
                sqlQuery.AppendLine(" 		,scenario ");
                sqlQuery.AppendLine(" 		,code ");
                if (taxEquiv)
                {
                    sqlQuery.AppendLine(" 		,AveTEAmt as IETotAmount ");
                }
                else
                {
                    sqlQuery.AppendLine(" 		,IETotAmount ");
                }



                if (isQuarterly)
                {
                    sqlQuery.AppendLine(Utilities.GroupQtrsCaseStatement(bankSim.Information.AsOfDate));
                }

                sqlQuery.AppendLine("		,CASE ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 12, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 1' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 24, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 2' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 36, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 3' ");
                sqlQuery.AppendLine("			WHEN month <= DATEADD(m, 48, '" + bankSim.Information.AsOfDate.ToShortDateString() + "') THEN ");
                sqlQuery.AppendLine("				'Year 4' ");
                sqlQuery.AppendLine("			ELSE ");
                sqlQuery.AppendLine("				'Year 5' ");
                sqlQuery.AppendLine("		END as yearGroup ");

                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine("  FROM Basis_ALP13 WHERE simulationId = " + bankSim.Id + "");
                if (isQuarterly)
                {
                    sqlQuery.AppendLine(" AND month <= DATEADD(m, 60, '" + bankSim.Information.AsOfDate.ToShortDateString() + "')");
                }
                else
                {
                    sqlQuery.AppendLine(" AND month <= DATEADD(m, 24, '" + bankSim.Information.AsOfDate.ToShortDateString() + "')");
                }

                sqlQuery.AppendLine(" ) as baseSim ");
                sqlQuery.AppendLine(" INNER JOIN Basis_ALB as alb ON  ");
                sqlQuery.AppendLine(" alb.simulationId = baseSim.simulationId and alb.code = baseSim.code ");
                sqlQuery.AppendLine(" INNER JOIN Basis_ALS as als ON ");
                sqlQuery.AppendLine(" als.simulationId = baseSim.simulationId and als.code = baseSim.code and als.scenario = baseSim.scenario ");
                sqlQuery.AppendLine(" INNER JOIN Basis_Scenario  as scen ON ");
                sqlQuery.AppendLine(" scen.number = als.scenario AND scen.SimulationId = als.SimulationId  ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as scenNames ON ");
                sqlQuery.AppendLine(" scennames.Id = scen.ScenarioTypeId  ");

                if (dgId != "-1")
                {
                    sqlQuery.AppendLine("INNER JOIN  ( ");
                    sqlQuery.AppendLine("	SELECT ");
                    sqlQuery.AppendLine("dgg.name, ");
                    sqlQuery.AppendLine("dgg.[Priority], ");
                    sqlQuery.AppendLine("dgc.IsAsset, ");
                    sqlQuery.AppendLine("dgc.acc_type, ");
                    sqlQuery.AppendLine("dgc.RbcTier ");
                    sqlQuery.AppendLine("FROM ");
                    sqlQuery.AppendLine("DDW_Atlas_Templates.dbo.ATLAS_DefinedGrouping as dg ");
                    sqlQuery.AppendLine("INNER JOIN  DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups as dgg ");
                    sqlQuery.AppendLine("ON dg.id = dgg.DefinedGroupingId ");
                    sqlQuery.AppendLine("INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc ");
                    sqlQuery.AppendLine("ON dgg.id = dgc.DefinedGroupingGroupId ");
                    sqlQuery.AppendLine("where dg.id = "+ dgId +") as dgs ON ");
                    sqlQuery.AppendLine("dgs.Acc_Type = alb.Acc_TypeOR and dgs.RbcTier = alb.RbcTier and dgs.IsAsset = alb.IsAsset ");
                }

                sqlQuery.AppendLine(" WHERE alb.exclude <> 1 AND als.exclude <> 1 AND alb.cat_Type IN (0,1,5) AND scen.scenarioTypeId IN " + scenarioTypeIn + " ");
                if (isQuarterly)
                {
                    sqlQuery.AppendLine(" GROUP BY baseSim.qtr,  baseSim.yearGroup, scenNames.name ");
                }
                else
                {
                    sqlQuery.AppendLine(" GROUP BY baseSim.month, baseSim.yearGroup, scenNames.name ");
                }

                sqlQuery.AppendLine(" ORDER BY scenNames.name, MAX(baseSim.month) ");
                retTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());
            }

            return retTable;
        }

        public static string GetErrorTemplate(int errorNum)
        {
            string retStr = "";
            switch (errorNum)
            {
                case 1:
                    retStr = "Scenario {0} not found in Simulation {1} at offset {2} from package As Of Date {3}";
                    break;
                case 2:
                    retStr = "Simulation {0} could not be found in offset {1} from package As Of Date {2}";
                    break;
                case 3:
                    retStr = "No As Of Date found at offset {0} from package As Of Date {1}";
                    break;
                case 4:
                    retStr = "No Policy found for As Of Date {0}";
                    break;
                case 5:
                    //generic kind of message
                    retStr = "No {0} found for {1}.";
                    break;
                default:
                    throw new Exception("There is no error template #" + errorNum);

            }

            return retStr;
        }

        public static DataTable ExecuteParameterizedSql(string conStr, string sqlquery, string[] parameters, string[] parameterVals)
        {

            DataTable retTable = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(conStr))
            {
                SqlCommand sqlCmd = new SqlCommand(sqlquery, sqlCon);
                for (var i = 0; i < parameters.Length; i++)
                {
                    sqlCmd.Parameters.AddWithValue(parameters[i], parameterVals[i]);
                }
                try
                {
                    sqlCon.Open();
                    retTable.Load(sqlCmd.ExecuteReader());
                    sqlCon.Close();
                }
                catch (Exception ex)
                {
                    sqlCon.Close();
                    return new DataTable();
                    //throw ex;
                }
            }
            return retTable;
        }

        public static DataTable ExecuteSql(string conStr, string query, IReadOnlyCollection<SqlParameter> parameters = null)
        {
            parameters = parameters ?? new SqlParameter[0];

            DataTable retTable = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(conStr))
            {
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);

                sqlCmd.Parameters.AddRange(parameters.ToArray());

                sqlCmd.CommandTimeout = 0;
                try
                {
                    sqlCon.Open();
                    retTable.Load(sqlCmd.ExecuteReader());
                    sqlCon.Close();
                }
                catch (Exception ex)
                {
                    sqlCon.Close();
                    return new DataTable();
                    //throw ex;
                }
            }
            return retTable;
        }

        // <summary>
        /// Gets a Inverted DataTable
        /// </summary>
        /// <param name="table">Provided DataTable</param>
        /// <param name="columnX">X Axis Column</param>
        /// <param name="columnY">Y Axis Column</param>
        /// <param name="columnZ">Z Axis Column (values)</param>
        /// <param name="columnsToIgnore">Whether to ignore some column, it must be 
        /// provided here</param>
        /// <param name="nullValue">null Values to be filled</param> 
        /// <returns>C# Pivot Table Method  - Felipe Sabino</returns>
        public static DataTable GetInversedDataTable(DataTable table, string columnX, string columnY, string columnZ, string nullValue, bool sumValues, string totalRowTitle)
        {
            //Create a DataTable to Return
            DataTable returnTable = new DataTable();

            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            //Add a Column at the beginning of the table
            returnTable.Columns.Add(columnY);


            //Read all DISTINCT values from columnX Column in the provided DataTale
            List<string> columnXValues = new List<string>();

            foreach (DataRow dr in table.Rows)
            {

                string columnXTemp = dr[columnX].ToString();
                if (!columnXValues.Contains(columnXTemp))
                {
                    //Read each row value, if it's different from others provided, add to 
                    //the list of values and creates a new Column with its value.
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
            }

            //Verify if Y and Z Axis columns re provided
            if (columnY != "" && columnZ != "")
            {
                //Read DISTINCT Values for Y Axis Column
                List<string> columnYValues = new List<string>();

                foreach (DataRow dr in table.Rows)
                {
                    if (!columnYValues.Contains(dr[columnY].ToString()))
                        columnYValues.Add(dr[columnY].ToString());
                }

                //Loop all Column Y Distinct Value
                foreach (string columnYValue in columnYValues)
                {
                    //Creates a new Row
                    DataRow drReturn = returnTable.NewRow();
                    drReturn[0] = columnYValue;
                    //foreach column Y value, The rows are selected distincted
                    DataRow[] rows = table.Select(columnY + "='" + columnYValue + "'");

                    //Read each row to fill the DataTable
                    foreach (DataRow dr in rows)
                    {
                        string rowColumnTitle = dr[columnX].ToString();

                        //Read each column to fill the DataTable
                        foreach (DataColumn dc in returnTable.Columns)
                        {
                            if (dc.ColumnName == rowColumnTitle)
                            {
                                //If Sum of Values is True it try to perform a Sum
                                //If sum is not possible due to value types, the value 
                                // displayed is the last one read
                                if (sumValues)
                                {
                                    try
                                    {
                                        drReturn[rowColumnTitle] =
                                             Convert.ToDecimal(drReturn[rowColumnTitle]) +
                                             Convert.ToDecimal(dr[columnZ]);
                                    }
                                    catch
                                    {
                                        drReturn[rowColumnTitle] = dr[columnZ];
                                    }
                                }
                                else
                                {
                                    drReturn[rowColumnTitle] = dr[columnZ];
                                }
                            }
                        }
                    }
                    returnTable.Rows.Add(drReturn);
                }
            }
            else
            {
                throw new Exception("The columns to perform inversion are not provided");
            }

            //if a nullValue is provided, fill the datable with it
            if (nullValue != "")
            {
                foreach (DataRow dr in returnTable.Rows)
                {
                    foreach (DataColumn dc in returnTable.Columns)
                    {
                        if (dr[dc.ColumnName].ToString() == "")
                            dr[dc.ColumnName] = nullValue;
                    }
                }
            }

            //if total row name is provided, add a total row
            if (totalRowTitle != "")
            {
                DataRow totalRow = returnTable.NewRow();
                totalRow[columnY] = totalRowTitle;
                foreach (DataRow dr in returnTable.Rows)
                {
                    foreach (DataColumn dc in returnTable.Columns)
                    {
                        if (dc.ColumnName != columnY)
                        {
                            if (totalRow[dc.ColumnName] == DBNull.Value)
                            {
                                totalRow[dc.ColumnName] = 0;
                            }
                            try
                            {
                                totalRow[dc.ColumnName] = Convert.ToDecimal(totalRow[dc.ColumnName]) + Convert.ToDecimal(dr[dc.ColumnName]);
                            }
                            catch
                            {
                                totalRow[dc.ColumnName] = totalRow[dc.ColumnName];
                            }
                            
                        }
                    }
                }
                returnTable.Rows.Add(totalRow);
            }

            return returnTable;
        }

        //public static DataTable SimpleTablePivot(DataTable sourceTable, string colNameToBecomeHeader, string[] otherColNames, Type[] otherColTypes)
        //{
        //    DataTable retTable = new DataTable();
        //    Dictionary<string, List<double>> pivotData = new Dictionary<string, List<double>>();
        //    if (!sourceTable.Columns.Contains(colNameToBecomeHeader))
        //    {
        //        return retTable;
        //    }

        //    //create the columns of the table
        //    if(includeNameColumn){
        //        retTable.Columns.Add("Name", typeof(string));
        //    }
        //    foreach (DataRow dr in sourceTable.Rows)
        //    {
        //        if (!sourceTable.Columns.Contains(dr[colNameToBecomeHeader].ToString()))
        //        {
        //            retTable.Columns.Add(dr[colNameToBecomeHeader].ToString(), sourceTable.Columns[colNameToBecomeHeader].GetType());
        //        }
        //    }

        //    return retTable;
        //}

        public static DataTable PivotTable(DataTable sourceTable, string lineItemTitle, string[] extraDataColumns, string asOfDate, bool monthly, int nPeriods, bool hasActuals)
        {
            DataRow tempRow;
            DataTable t = new DataTable();
            Dictionary<string, DataRow> dataRowDict = new Dictionary<string, DataRow>();
            DateTime _aodDateTime = DateTime.Parse(asOfDate).AddMonths(-1);
            DateTime aodDateTime = new DateTime(_aodDateTime.Year, _aodDateTime.Month, DateTime.DaysInMonth(_aodDateTime.Year, _aodDateTime.Month));
            aodDateTime = aodDateTime.AddDays(1);

            t.Columns.Add(new DataColumn("name"));
            foreach (string s in extraDataColumns)
            {
                t.Columns.Add(new DataColumn(s));
            }

            for (var m = hasActuals ? 0 : 1; m <= nPeriods; m++)
            {
                string month = aodDateTime.AddMonths(monthly ? m : (m * 3)).ToString("d");
                t.Columns.Add(month);
            }

            //t.Columns.Add(new DataColumn("cat_type"));

            for (int r = 0; r < sourceTable.Rows.Count; r++)
            {
                string thisName = (string)sourceTable.Rows[r][lineItemTitle];
                string tempMonth = sourceTable.Rows[r]["month"].ToString();
                string thisMonth = ((DateTime)sourceTable.Rows[r]["month"]).ToString("d");
                string thisBal = sourceTable.Rows[r]["endBal"].ToString();
                //string thisCat = sourceTable.Rows[r]["cat_type"].ToString();


                if (dataRowDict.Keys.Contains(thisName))
                {
                    dataRowDict[thisName][thisMonth] = thisBal;
                    foreach (string s in extraDataColumns)
                    {
                        dataRowDict[thisName][s] = sourceTable.Rows[r][s].ToString();
                    }
                    //dataRowDict[thisName]["cat_type"] = thisCat;
                }
                else
                {
                    tempRow = t.NewRow();
                    tempRow["name"] = thisName;
                    tempRow[thisMonth] = thisBal;
                    //tempRow["cat_type"] = thisCat;
                    foreach (string s in extraDataColumns)
                    {
                        DataRow dr = sourceTable.Rows[r];
                        string isAsset = dr[s].ToString();


                        tempRow[s] = sourceTable.Rows[r][s].ToString();
                    }

                    dataRowDict.Add(thisName, tempRow);
                }
            }
            foreach (DataRow dRow in dataRowDict.Values)
            {
                t.Rows.Add(dRow);
            }


            return t;
        }


        public static bool ExecuteCommand(string conStr, string query)
        {
            using (SqlConnection sqlCon = new SqlConnection(conStr))
            {
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                try
                {
                    sqlCon.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlCon.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    sqlCon.Close();
                    return false;
                    //throw ex;
                }
            }

        }

        public static bool CheckIfValidDatabase()
        {
            if (ExecuteCommand(Utilities.BuildInstConnectionString(""), "SELECT TOP 1 acctNumber FROM MortgageLoans"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static string InstName(string institutionDatabase)
        {

            string conStr = BuildInstConnectionString(institutionDatabase);
            DataTable retTable = ExecuteSql(conStr, "SELECT instName FROM DWR_FedRates.dbo.DatabasehighlineMapping WHERE databaseName = '" + institutionDatabase + "'");

            //If record found which always should be then return it otherwise return blank
            if (retTable.Rows.Count > 0)
            {
                return retTable.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }

        public static string RegulatoryName(string institutionDatabase)
        {

            string conStr = BuildInstConnectionString(institutionDatabase);
            DataTable retTable = ExecuteSql(conStr, "SELECT RegulatoryBankName FROM DWR_FedRates.dbo.DatabasehighlineMapping WHERE databaseName = '" + institutionDatabase + "'");

            //If record found which always should be then return it otherwise return blank
            if (retTable.Rows.Count > 0)
            {
                return retTable.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }

        public static DataTable ScenarionInSimulationType(int simulationTypeId, string asOfDate, string inst)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	scen.name ");
            sqlQuery.AppendLine(" 	,scen.ScenarioTypeId  ");
            sqlQuery.AppendLine(" FROM simulations AS sim ");
            sqlQuery.AppendLine(" INNER JOIN asOfDateInfo AS aod ON ");
            sqlQuery.AppendLine(" aod.id = sim.informationId ");
            sqlQuery.AppendLine(" INNER JOIN Basis_Scenario AS scen ");
            sqlQuery.AppendLine(" ON scen.SimulationId = sim.id  ");
            sqlQuery.AppendLine("  WHERE SimulationTypeId = " + simulationTypeId.ToString() + " AND asOfDate = '" + asOfDate + "' ");
            return ExecuteSql(BuildInstConnectionString(inst), sqlQuery.ToString());
        }

        public static string GroupQtrsCaseStatement(DateTime month)
        {
            StringBuilder sqlQuery = new StringBuilder();
            switch (month.Month)
            {
                case 3:
                case 6:
                case 9:
                case 12:
                    sqlQuery.AppendLine(" 		,CASE  ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (1,2,3) THEN ");
                    sqlQuery.AppendLine(" 				'1' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (4,5,6) THEN ");
                    sqlQuery.AppendLine(" 				'2' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (7,8,9) THEN ");
                    sqlQuery.AppendLine(" 				'3' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (10,11,12) THEN ");
                    sqlQuery.AppendLine(" 				'4' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 		END as qtr ");
                    break;
                case 2:
                case 5:
                case 8:
                case 11:
                    sqlQuery.AppendLine(" 		,CASE  ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (12,1,2) THEN ");
                    sqlQuery.AppendLine(" 				CASE WHEN DATEPART(m, month) = 12 THEN '1' + CAST(DATEPART(yyyy, month) + 1 AS VARCHAR(4)) ELSE '1' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) END ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (3,4,5) THEN ");
                    sqlQuery.AppendLine(" 				'2' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (6,7,8) THEN ");
                    sqlQuery.AppendLine(" 				'3' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (9,10,11) THEN ");
                    sqlQuery.AppendLine(" 				'4' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 		END as qtr ");
                    break;
                case 1:
                case 4:
                case 7:
                case 10:
                    sqlQuery.AppendLine(" 		,CASE  ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (11,12,1) THEN ");
                    sqlQuery.AppendLine(" 				CASE WHEN DATEPART(m, month) <> 1 THEN '1' + CAST(DATEPART(yyyy, month) + 1 AS VARCHAR(4)) ELSE '1' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) END ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (2,3,4) THEN ");
                    sqlQuery.AppendLine(" 				'2' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (5,6,7) THEN ");
                    sqlQuery.AppendLine(" 				'3' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 			WHEN DATEPART(m, month) IN (8,9,10) THEN ");
                    sqlQuery.AppendLine(" 				'4' + CAST(DATEPART(yyyy, month) AS VARCHAR(4)) ");
                    sqlQuery.AppendLine(" 		END as qtr ");
                    break;
            }
            return sqlQuery.ToString();

        }

        public static DataTable ProjectionLabels(string conStr, string tblName, string simulationId, string scenario, string numMonths, string asOfDate)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	month ");
            sqlQuery.AppendLine(" 	,CASE  ");
            sqlQuery.AppendLine(" 		WHEN NMonths =1 THEN ");
            sqlQuery.AppendLine(" 			SUBSTRING(CONVERT(varchar(10), DATEADD(m, nMonths - 1, MONTH), 6), CHARINDEX(' ', CONVERT(varchar(10), DATEADD(m, nMonths - 1, MONTH), 6)) + 1, LEN(CONVERT(varchar(10), DATEADD(m, nMonths - 1, MONTH), 6))) ");
            sqlQuery.AppendLine(" 		WHEN NMonths = 3 THEN ");
            sqlQuery.AppendLine(" 			SUBSTRING(CONVERT(varchar(10), DATEADD(m, nMonths - 1, MONTH), 6), CHARINDEX(' ', CONVERT(varchar(10), DATEADD(m, nMonths - 1, MONTH), 6)) + 1, LEN(CONVERT(varchar(10), DATEADD(m, nMonths - 1, MONTH), 6))) + 'q' ");
            sqlQuery.AppendLine(" 		WHEN NMonths = 12 THEN ");
            sqlQuery.AppendLine(" 			'Y' + CAST(DATEDIFF(yyyy, '" + asOfDate + "', month)  + 1  as varchar(10)) ");
            sqlQuery.AppendLine(" 		ELSE ");
            sqlQuery.AppendLine(" 			'Y' + CAST(DATEDIFF(yyyy, '" + asOfDate + "', month)  + 1  as varchar(10)) + '-' + CAST(DATEDIFF(yyyy, '" + asOfDate + "', DATEADD(m, nMonths, month)) AS varchar(10)) ");
            sqlQuery.AppendLine(" 	END as label ");
            sqlQuery.AppendLine(" FROM " + tblName + " WHERE simulationId = " + simulationId.ToString() + " and scenario = '" + scenario + "' ");

            //If number of months passed in only get that many  
            if (numMonths != "")
            {
                sqlQuery.AppendLine(" AND month <= DATEADD(m, " + numMonths + ",'" + asOfDate + "' )");
            }

            sqlQuery.AppendLine(" GROUP BY Month, nMonths ");
            sqlQuery.AppendLine(" ORDER BY month ");
            return Utilities.ExecuteSql(conStr, sqlQuery.ToString());


        }

        //Basic Atlas One Profile Stuff

        public static bool CheckCurrentUserExists()
        {
            string qry = "SELECT COUNT(*) FROM DWR_FedRates.dbo.Atlas_UserData WHERE UserName = @UserName";
            string[] paramArr = new string[] { "@UserName" };
            string[] paramVals = new string[] { GetUserNameNoEmail() };
            string conStr = ConfigurationManager.ConnectionStrings["DDwConnection"].ToString();
            int test = (int)Utilities.ExecuteParameterizedSql(conStr, qry, paramArr, paramVals).Rows[0][0];
            return test > 0;
        }
        public static string GetUserNameNoEmail()
        {

            string userName = HttpContext.Current.User.Identity.Name;
            if (userName.IndexOf("@") >= 0)
            {
                userName = userName.Substring(0, userName.IndexOf("@"));
            }

            return userName;
        }


        public static void SetUserData(string dataField, string value)
        {
            string conStr = ConfigurationManager.ConnectionStrings["DDwConnection"].ToString();
            string qry = String.Format("UPDATE DWR_FedRates.dbo.Atlas_UserData SET [{0}] = @Value WHERE [UserName] = @UserName", dataField);
            string[] paramArr = new string[] { "@Value", "@UserName" };
            string[] paramVals = new string[] { value, GetUserNameNoEmail() };

            if (!CheckCurrentUserExists())
            {
                InsertNewUserData();
            }
            Utilities.ExecuteParameterizedSql(conStr, qry, paramArr, paramVals);

        }

        public static bool IsAtlasBank(string databaseName)
        {
            string conStr = ConfigurationManager.ConnectionStrings["DDWConnection"].ToString();


            DataTable atlasClient = Utilities.ExecuteSql(conStr, "SELECT databaseNAme FROM DWR_FedRates.dbo.databasehighlinemapping WHERE databaseNAme = '" + databaseName + "' AND AtlasOneClient = 1 ");

            if (atlasClient.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static DataTable GetAtlasUserData()
        {
            string qry = "SELECT aud.[UserName], aud.[databaseName], aud.[asOfDate], dbm.instName as atlasBankName, dbm.regulatoryBankName FROM DWR_FedRates.dbo.Atlas_UserData as aud INNER JOIN DWR_FedRates.dbo.databasehighlinemapping as dbm ON aud.databaseName = dbm.databaseName  WHERE [UserName] = @UserName";
            string[] paramArr = new string[] { "@UserName" };
            string[] paramVals = new string[] { GetUserNameNoEmail() };
            string conStr = ConfigurationManager.ConnectionStrings["DDWConnection"].ToString();
            return Utilities.ExecuteParameterizedSql(conStr, qry, paramArr, paramVals);
        }

        public static void InsertNewUserData()
        {
            string[] paramArr = new string[] { "@UserName", "@databaseName" };
            string[] paramVals = new string[] { GetUserNameNoEmail(), "" };
            string conStr = ConfigurationManager.ConnectionStrings["DDWConnection"].ToString();

            string insertQry = "INSERT INTO DWR_FedRates.dbo.Atlas_UserData ([UserName], [databaseName], [asOfDate]) VALUES(@UserName, @databaseName, '')\n";

            DataTable availBanks = AvailableBanksTable();
            if (availBanks.Rows.Count > 0)
            {
                paramVals[1] = availBanks.Rows[0]["databaseName"].ToString();
            }


            Utilities.ExecuteParameterizedSql(conStr, insertQry, paramArr, paramVals);
        }



        public static DataTable AvailableBanksTable()
        {
            //System.Web.Profile.ProfileBase profile = HttpContext.Current.Profile;
            string conStr = ConfigurationManager.ConnectionStrings["DDWConnection"].ToString();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT dbm.databaseName ");
            sqlQuery.AppendLine(" 		,dbm.instName  ");
            sqlQuery.AppendLine("       ,dbm.RegulatoryBankName ");
            sqlQuery.AppendLine(" 		FROM DDWM.dbo.SecurityInfo as si ");
            sqlQuery.AppendLine(" 		INNER JOIN DWR_FedRates.dbo.DatabaseHighlineMapping as dbm ON ");
            sqlQuery.AppendLine(" 		si.DBName = dbm.DatabaseName ");
            sqlQuery.AppendLine(" 		WHERE si.userID = '" + Utilities.GetUserNameNoEmail() + "' AND dbm.AtlasOneClient = 1 ");

            return Utilities.ExecuteSql(conStr, sqlQuery.ToString());

        }

        public static string GetFirstOfMonthStr(string aod)
        {
            return new DateTime(DateTime.Parse(aod).Year, DateTime.Parse(aod).Month, 1).ToShortDateString();
        }

    }

    public class TableOfContentItem
    {
        public string title = "";
        public string type = "";
        public int pageNum = 0;

        public TableOfContentItem(int pageNum, string title, string type)
        {
            this.pageNum = pageNum;
            this.type = type;
            this.title = title;
        }
    }
}