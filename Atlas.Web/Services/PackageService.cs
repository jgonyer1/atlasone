﻿using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Institution.Model.Atlas;
using P360.Web.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Atlas.Web.Services
{
    public class ReportTableMapping
    {
        public string TableName;
        public string TypeName;
        public string LinkColumn;
        public List<ReportTableMapping> ChildTables;
    }

    public class PackageService
    {
        bool ReportHasStaticDate = false;
        string NewPackageAod = "";
        string[] DoNotCopy = new string[] { "ScenarioType", 
            "SimulationType", "BasicSurplusAdmin", 
            "Information", "LBLookback", 
            "LBCustomLayout", "LBCustomLayoutRowItem", 
            "LBCustomLayoutClassification", "LBCustomType", "WebRateScenarioType" };

        //put these in all caps because lord knows how many different ways we can't capitalize and spell something for an asofdate
        //upperColName == "OFFSET" || upperColName == "CUROFFSET" || upperColName == "PREVOFFSET"
        readonly string[] AodOffsetTypeFields = new string[] { "OFFSET" , "CUROFFSET" , "PREVOFFSET" , "DEPHISOTRYSTARTDATE", "DEPHISTORYENDDATE" };
        //public void Delete(int packageId)
        //{
        //    //Build It Based Off Of Profile Object
        //    string conStr = Utilities.BuildInstConnectionString("");

        //    DataTable repsToDelete = Utilities.ExecuteSql(conStr, "SELECT  [Id], [PackageId], [Name] ,[FirstPageNumber] ,[Priority] ,[HasSplitFootnotes] ,[sourceReportId] ,[Defaults] FROM [ATLAS_Report] WHERE packageId = " + packageId + " ");
        //    foreach (DataRow dr in repsToDelete.Rows)
        //    {
        //        DeleteReport(true, dr["Id"].ToString(), conStr);
        //    }

        //    Utilities.ExecuteCommand(conStr, String.Format(" DELETE FROM [dbo].[ATLAS_Packages] WHERE id = {0}", packageId));
        //}

        private IReadOnlyCollection<int> getReportIDsByPackageId(int packageID)
        {
            const string QUERY = @"
                SELECT r.Id
                FROM [dbo].[ATLAS_Report] r
                INNER JOIN [dbo].[ATLAS_PACKAGES] p ON r.[PackageId] = p.[Id]
                WHERE p.[Id] = @packageID AND p.[AsOfDate] = @asOfDate
            ";

            string asOfDate = Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString();

            DataTable results = Utilities.ExecuteSql(
                Utilities.BuildInstConnectionString(""),
                QUERY,
                new[]
                {
                    new SqlParameter("packageId", SqlDbType.Int) { Value = packageID },
                    new SqlParameter("asOfDate", SqlDbType.DateTime) { Value = asOfDate }
                }
            );

            return results.Rows.Cast<DataRow>().Select(dr => Convert.ToInt32(dr["Id"])).ToArray();
        }

        public string DeleteReportingItem(string sourceName, string sourceId, string srcDB, string parentName)
        {
            switch(sourceName)
            {
                case "Package":
                    FileProcessingService fps = new FileProcessingService();
                    foreach(int reportID in getReportIDsByPackageId(int.Parse(sourceId)))
                    {
                        fps.Delete(FileOwnerType.Report, reportID);
                    }

                    break;

                case "Report":
                    new FileProcessingService().Delete(FileOwnerType.Report, int.Parse(sourceId));
                    break;
            }

            ReportTableMapping mapping = new ReportTableMapping();
            mapping = getTableMapping(sourceName, parentName, mapping);
            string sql = getDeleteSql(mapping, sourceId, srcDB);
            //Execute SQL
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(srcDB), sql);
            return sql;
        }

        public string NewCopyReportingItem(string parentName, string parentId, string idToUse, string srcDB, string destDB, Dictionary<string, string> otherLiterals, Dictionary<string, string> bsAdminLookup, Dictionary<string, string> lookbackIdLookup, bool isCopy)
        {
            ReportTableMapping mapping = new ReportTableMapping();
            mapping = getTableMapping(parentName, "", mapping);
            mapping.TypeName = parentName;
            InsertNew(mapping, parentId, idToUse, srcDB, destDB, otherLiterals, true, bsAdminLookup, lookbackIdLookup, isCopy, false);

            string sql = "SELECT IDENT_CURRENT('" + mapping.TableName + "')";
            DataTable lastInsertedId = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(destDB), sql);
            if (lastInsertedId.Rows.Count > 0)
            {
                sql = lastInsertedId.Rows[0][0].ToString();
            }
            else
            {
                sql = "";
            }
            return sql;
        }

        public string getDeleteSql(ReportTableMapping mapping, string parentId, string srcDB)
        {
            StringBuilder qry = new StringBuilder();
            List<string> deletes = new List<string>();
            DataTable childIds = new DataTable();
            string tempQry = "";
            int d = 0;
            ReportTableMapping tempMap = new ReportTableMapping();

            //handling for non-true child relationships
            string[] columns = getColumns(mapping.TableName, srcDB, srcDB, true);
            List<string> depCols = getDependentColumns(columns, mapping.LinkColumn);
            DataTable srcData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(srcDB), "SELECT * FROM " + mapping.TableName + " WHERE Id = " + parentId);

            //BAIL OUT, nothing to delete anyway
            if (srcData.Rows.Count == 0)
            {
                //don't add anything to the sql statement, there's nothing to delete
                return "";
            }

            if (srcData.Columns.Contains("ShortName"))
            {
                string shName = srcData.Rows[0]["ShortName"].ToString();
                tempMap = new ReportTableMapping();
                tempMap = getTableMapping(shName, "", tempMap);
                if (tempMap != null)
                {
                    deletes.Add(getDeleteSql(tempMap, parentId, srcDB));
                }
            }
            foreach (string dep in depCols)
            {
                string depName = dep.Substring(0, dep.Length - 2);
                tempMap = new ReportTableMapping();
                tempMap = getTableMapping(depName, mapping.TypeName, tempMap);
                if (tempMap != null && tempMap.TypeName != "SecuredLiabilitiesType" && tempMap.TypeName != "CollateralType")
                {
                    deletes.Add(getDeleteSql(tempMap, srcData.Rows[0][dep].ToString(), srcDB));
                }
            }

            //if it's a config, we need to also delete the Report record backing it up
            //deletes.Add("DELETE FROM ATLAS_Report where Id = " + parentId + " AND ShortName = '" + mapping.TypeName + "'");

            deletes.Add("DELETE FROM " + mapping.TableName + " WHERE Id = " + parentId);

           
            foreach (ReportTableMapping childMap in mapping.ChildTables)
            {
                if (childMap != null && childMap.TypeName != "SecuredLiabilitiesType")
                {
                    tempQry = "SELECT Id FROM " + childMap.TableName + " WHERE " + childMap.LinkColumn + " = " + parentId;
                    childIds = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(srcDB), tempQry);
                    foreach (DataRow dr in childIds.Rows)
                    {
                        deletes.Add(getDeleteSql(childMap, dr[0].ToString(), srcDB));
                    }
                }

            }

            d = deletes.Count - 1;
            for (d = deletes.Count - 1; d >= 0; d--)
            {
                qry.AppendLine(deletes[d]);
            }
            string Q = qry.ToString();
            return qry.ToString();
        }

        internal static int? GetTemplateReportId(string sourceDatabaseName, int reportID, int packageID, int sourceReportID)
        {
            string query = @"
                SELECT tr.Id AS TemplateID
                FROM [DDW_ATLAS_Templates].[dbo].[ATLAS_Report] tr
                INNER JOIN [DDW_ATLAS_Templates].[dbo].[ATLAS_Packages] tp ON tr.PackageId = tp.Id
                INNER JOIN [" + sourceDatabaseName + @"].[dbo].[ATLAS_Packages] lp ON CAST(tp.AsOfDate AS DATE) = CAST(lp.AsOfDate AS DATE)
                WHERE tr.SourceReportId = @sourceReportID
                  AND lp.Id = @localPackageID
            ";

            DataTable result = Utilities.ExecuteSql(
                Utilities.BuildInstConnectionString(""),
                query,
                new SqlParameter[]
                {
                    new SqlParameter("sourceReportID", SqlDbType.Int) {Value = sourceReportID},
                    new SqlParameter("localPackageID", SqlDbType.Int) {Value = packageID},
                }
            );

            if (result.Rows.Count == 0) //this indicates that we're referencing the template directly, not a copy from the template package
            {
                query = @"
                    SELECT tr.Id AS TemplateID
                    FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Report] tr
                    INNER JOIN [" + sourceDatabaseName + @"].[dbo].[ATLAS_Report] lr ON tr.Id = lr.SourceReportId
                    WHERE lr.Id = @localReportID
                ";

                result = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), query, new SqlParameter[] { new SqlParameter("localReportID", SqlDbType.Int) { Value = reportID } });
            }

            return result.Rows.Cast<DataRow>().Select(dr => (int?)dr["TemplateID"]).FirstOrDefault();
        }

        public string InsertNew(ReportTableMapping mapping, string sourceId, string idToUse, string srcDB, string destDB, Dictionary<string, string> otherLiteralValues, bool returnInsertedId, Dictionary<string, string> bsAdminIdLookup, Dictionary<string, string> lbIdLookup, bool isCopy, bool turnReportIdInsertOn)
        {
            StringBuilder qry = new StringBuilder();
            StringBuilder tempQry = new StringBuilder();
            List<string> inserts = new List<string>();
            DataTable childIds = new DataTable();
            string srcDBConStr = Utilities.BuildInstConnectionString(srcDB);
            DataTable srcData = Utilities.ExecuteSql(srcDBConStr, "SELECT * FROM [" + srcDB + "].[dbo].[" + mapping.TableName + "] WHERE Id = " + sourceId);
            DataTable templateData = new DataTable();
            if (srcData.Rows.Count == 0)
            {
                return "-1";
            }

            if (otherLiteralValues.ContainsKey("AsOfDate"))
            {
                NewPackageAod = otherLiteralValues["AsOfDate"];
            }

            //set up some global class variables that can get referenced in iterations farther down the report chain
            if(mapping.TypeName == "Report")
            {
                ReportHasStaticDate = (bool)srcData.Rows[0]["StaticDates"];
            }
            if(mapping.TypeName == "Package")
            {
                //NewPackageAod = otherLiteralValues["AsOfDate"];
            }
            string srcPkgId = "";
            string srcRepId = "";
            bool usingDefaults = false;
            if(srcData.Columns.Contains("SourcePackageId"))
            {
                srcPkgId = srcData.Rows[0]["SourcePackageId"].ToString();
            }
            if(srcData.Columns.Contains("SourceReportId"))
            {
                srcRepId = srcData.Rows[0]["SourceReportId"].ToString();
            }
            if (srcData.Columns.Contains("Defaults"))
            {
                usingDefaults = Convert.ToBoolean(srcData.Rows[0]["Defaults"].ToString());
            }

            //ok between those three variables above, we should have enough to know where to copy from
            //sameAOD check is in there to skip all this if we're just COPYING a package
            if (!String.IsNullOrEmpty(srcRepId) && usingDefaults && !isCopy)
            {
                //we're copying a Report that needs to pull from ATLAS_Templates
                qry.AppendLine("SELECT [DDW_ATLAS_Templates].[dbo].[ATLAS_Report].Id FROM [DDW_ATLAS_Templates].[dbo].[ATLAS_Report] INNER JOIN [DDW_ATLAS_Templates].[dbo].[ATLAS_Packages] ");
                qry.AppendLine("ON [DDW_ATLAS_Templates].[dbo].[ATLAS_Report].PackageId = [DDW_ATLAS_Templates].[dbo].[ATLAS_Packages].Id ");
                qry.AppendLine("WHERE CAST([DDW_ATLAS_Templates].[dbo].[ATLAS_Packages].AsOfDate as date) = (SELECT AsOfDate from ["+ destDB +"].[dbo].[ATLAS_Packages] WHERE Id = "+ otherLiteralValues["PackageId"] +") ");
                qry.AppendLine("AND [DDW_ATLAS_Templates].[dbo].[ATLAS_Report].SourceReportId = " + srcRepId);
                templateData = Utilities.ExecuteSql(srcDBConStr, qry.ToString());
                if(templateData.Rows.Count > 0){
                    sourceId = templateData.Rows[0]["Id"].ToString();
                    srcData = Utilities.ExecuteSql(srcDBConStr, "SELECT * FROM [DDW_ATLAS_Templates].[dbo].[" + mapping.TableName + "] WHERE Id = " + sourceId);
                    srcDB = "DDW_ATLAS_Templates";
                }
                else if(destDB != "DDW_Atlas_Templates")//if we're rolling atlast templates we can't expect to find the corresponding AT record...becuase we're currently creating it. Just continue with the src data we've grabbed all ready
                {
                    //if you can't find an atlas_templates report with the SourceReportID BUT SourceReportId is indeed present, the template report must have been deleted. Do Not Insert Anything
                    //sourceId = srcRepId;
                    //srcData = Utilities.ExecuteSql(srcDBConStr, "SELECT * FROM [DDW_ATLAS_Templates].[dbo].[" + mapping.TableName + "] WHERE Id = " + sourceId);
                    //srcDB = "DDW_ATLAS_Templates";
                    return "-1";
                }
                qry.Clear();
                templateData.Clear();
            }

            //kind of a hack
            //for BasicSurplusCollateralType we need to go get the new CollateralType Id. The CollateralTypes SHOULD already be inserted ... hopefully
            if (mapping.TypeName == "BasicSurplusCollateralType")
            {
                qry.AppendLine("select new.Id from ( ");
                qry.AppendLine("select collatType.Id,fieldName from ATLAS_BasicSurplusCollateralType bsCollat");
                qry.AppendLine("INNER JOIN ATLAS_BasicSurplusSecuredLiabilitiesType secLiab");
                qry.AppendLine("ON bsCollat.BasicSurplusSecuredLiabilitiesTypeId = secLiab.Id");
                qry.AppendLine("INNER JOIN ATLAS_BasicSurplusAdmin bsAdmin ");
                qry.AppendLine("ON secLiab.BasicSurplusAdminId = bsAdmin.Id");
                qry.AppendLine("INNER JOIN ATLAS_CollateralType collatType ");
                qry.AppendLine("ON collatType.BasicSurplusAdminId = bsAdmin.Id AND collatType.Id = bsCollat.CollateralTypeId");
                qry.AppendLine("WHERE bsCollat.Id = " + sourceId);
                qry.AppendLine(") old");
                qry.AppendLine("INNER JOIN (");
                qry.AppendLine("select collatType.Id, collatType.Name, collatType.fieldName from ATLAS_CollateralType collatType ");
                qry.AppendLine("inner join ATLAS_BasicSurplusAdmin bsAdmin on collatType.BasicSurplusAdminId = bsAdmin.Id");
                qry.AppendLine("inner join ATLAS_BasicSurplusSecuredLiabilitiesType secLiabType on secLiabType.BasicSurplusAdminId = bsAdmin.Id ");
                qry.AppendLine("and secLiabType.BasicSurplusAdminId = collatType.BasicSurplusAdminId");
                qry.AppendLine("where secLiabType.Id = " + otherLiteralValues["BasicSurplusSecuredLiabilitiesTypeId"]);
                qry.AppendLine(") new");
                qry.AppendLine("on old.FieldName = new.FieldName");
                templateData = Utilities.ExecuteSql(srcDBConStr, qry.ToString());
                if(templateData.Rows.Count > 0)
                {
                    if (otherLiteralValues.ContainsKey("CollateralTypeId"))
                    {
                        otherLiteralValues["CollateralTypeId"] = templateData.Rows[0]["Id"].ToString();
                    }
                    else
                    {
                        otherLiteralValues.Add("CollateralTypeId", templateData.Rows[0]["Id"].ToString());
                    }
                }
                qry.Clear();
            }

            //lookback report LBLookback Id's matching
            if(mapping.TypeName == "LookbackType" &&  srcDB.ToUpper() == "DDW_ATLAS_TEMPLATES")
            {
                GlobalController gc = new GlobalController();
                DataTable userInfo = Utilities.GetAtlasUserData();
                string curDate = DateTime.Parse(userInfo.Rows[0]["asOfDate"].ToString()).ToShortDateString();
                DataTable aods = gc.AsOfDates("", true, 100, false);
                int targetAodIndx = Convert.ToInt32(srcData.Rows[0]["AsOfDateOffset"].ToString());
                DateTime targetAod = DateTime.Parse(aods.Rows[targetAodIndx]["asOfDateDescript"].ToString());

                //If we're rolling we need to do some adjustments to get the correct lookback ids
                if (!isCopy)
                {
                    aods = gc.AsOfDates("", false, 100, false);
                    int newPkgAodIndx = -1;
                    for (int r = 0; r < aods.Rows.Count; r++)
                    {
                        if(DateTime.Parse(aods.Rows[r]["asOfDateDescript"].ToString()).ToShortDateString() == DateTime.Parse(NewPackageAod).ToShortDateString())
                        {
                            newPkgAodIndx = r;
                        }
                    }
                    targetAodIndx = targetAodIndx + newPkgAodIndx;
                    targetAod = DateTime.Parse(aods.Rows[targetAodIndx]["asOfDateDescript"].ToString());
                }

                
                
                int? res = LookbackGroupDataService.GetLBLookbackIdToMatchTemplate(srcData.Rows[0]["LBLookbackId"].ToString(), true, targetAod.ToShortDateString());
                if(res > 0)
                {
                    if (otherLiteralValues.ContainsKey("LBLookbackId"))
                    {
                        otherLiteralValues["LBLookbackId"] = res.ToString();
                    }
                    else
                    {
                        otherLiteralValues.Add("LBLookbackId", res.ToString());
                    }
                    
                }
            }

            DataTable result = new DataTable();
            Dictionary<string, string> childLitValues = new Dictionary<string, string>();
            string insertedId = "";
            List<string> dependencyIds = new List<string>();
            string dependencyPattern = @"\w+Id\b";
            Regex depRegx = new Regex(dependencyPattern);

            int d = 0;
            string[] columns;// = getColumnsNoId(mapping.TableName, srcDB, destDB);
            string[] colsBrackets;
            if (idToUse.Length > 0)
            {
                columns = getColumns(mapping.TableName, srcDB, destDB, false);
            }
            else
            {
                columns = getColumns(mapping.TableName, srcDB, destDB, true);
            }
            colsBrackets = new string[columns.Length];
            for(int k = 0; k < columns.Length; k++)
            {
                colsBrackets[k] = "[" + columns[k] + "]";
            }


            //won't ever be the case with a COPY
            //should only happen when ROLLING
            if (srcDB.ToUpper() == "DDW_ATLAS_TEMPLATES")
            {
                if (mapping.TypeName == "Package")
                {
                    //srcPkgId = srcData.Rows[0]["SourcePackageId"].ToString();
                    if (String.IsNullOrEmpty(srcPkgId))
                    {
                        otherLiteralValues.Add("SourcePackageId", sourceId);
                    }
                }
                else if (mapping.TypeName == "Report")
                {
                    //srcRepId = srcData.Rows[0]["SourceReportId"].ToString();
                    //usingDefaults = Convert.ToBoolean(srcData.Rows[0]["Defaults"].ToString());
                    if (String.IsNullOrEmpty(srcRepId))
                    {

                        //this could already exist, passed on from the previous report
                        if (otherLiteralValues.ContainsKey("SourceReportId"))
                        {
                            otherLiteralValues["SourceReportId"] = sourceId;
                        }else{
                            otherLiteralValues.Add("SourceReportId", sourceId);
                        }
                    }
                }
            }

            if (idToUse.Length > 0 && !otherLiteralValues.ContainsKey("Id"))
            {
                otherLiteralValues.Add("Id", idToUse);
            }

            dependencyIds = getDependentColumns(columns, mapping.LinkColumn);
            foreach (string depCol in dependencyIds)
            {
                string depName = depCol.Substring(0, depCol.Length - 2);

                //trying to be as "catch all" as I can be. The above will strip out 'Id' from the column name. Lets also get rid of 'Left' and 'Right'
                depName = depName.Replace("Left", "");
                depName =  depName.Replace("Right", "");
                ReportTableMapping tempMapping = new ReportTableMapping();
                tempMapping = getTableMapping(depName, mapping.TypeName, tempMapping);
                if (tempMapping != null && mapping.TypeName != "BasicSurplusCollateralType" && depName != "CollateralType" && tempMapping.TypeName != "SecuredLiabilitiesType")
                {
                    string newDepId = InsertNew(tempMapping, srcData.Rows[0][depCol].ToString(), "", srcDB, destDB, otherLiteralValues, true, bsAdminIdLookup, lbIdLookup, isCopy, false);
                    
                    if (!otherLiteralValues.ContainsKey(depCol))
                    {
                        otherLiteralValues.Add(depCol, newDepId);
                    }
                    else
                    {
                        otherLiteralValues[depCol] = newDepId;
                    }
                }

            }

            //Hopefully all the other dependencies should be in place and now we can write the insert statement

            tempQry.AppendLine("INSERT INTO [" + destDB + "].[dbo]." + mapping.TableName + "(" + String.Join(",", colsBrackets) + ")");
            tempQry.AppendLine("SELECT ");

            string upperColName = "";
            for (d = 0; d < columns.Length; d++)
            {
                upperColName = columns[d].ToUpper();


                //Dictionary of literals takes priority
                if (otherLiteralValues.ContainsKey(columns[d]))
                {
                    tempQry.Append("'" + otherLiteralValues[columns[d]] + "'");
                }
                else if (columns[d] == mapping.LinkColumn)
                {
                    tempQry.Append(srcData.Rows[0][mapping.LinkColumn].ToString());
                }
                else if (upperColName.IndexOf("INSTITUTIONDATABASENAME") > -1)
                {
                    tempQry.Append("'" + destDB + "'");
                }
                else if (upperColName == "BASICSURPLUSADMINID")
                {
                    if (bsAdminIdLookup.ContainsKey(srcData.Rows[0][columns[d]].ToString()))
                    {
                        tempQry.Append("'" + bsAdminIdLookup[srcData.Rows[0][columns[d]].ToString()] + "'");
                    }
                    else//if bsAdminLookup is empty means we should be copying just a package, not rolling an asofdate
                    {
                        //grab the one selected in policies / first BsAdmin
                        
                        GlobalController gc = new GlobalController();
                        List<BasicSurplusAdmin> bsAdmins = gc.BasicSurplusAdmins().ToList();
                        Dictionary<string, DataTable> polOffsets = gc.PolicyOffsets();
                        if (!polOffsets.Keys.Contains(NewPackageAod))
                        {
                            tempQry.Append("'" + bsAdmins[0].Id + "'");
                        }
                        else if(polOffsets.Keys.Contains(NewPackageAod))
                        {
                            tempQry.Append("'" + polOffsets[NewPackageAod].Rows[0]["bsId"].ToString() + "'");
                        }
                        else{//there's two checks ahead of this, shouldn't happen
                            tempQry.Append("[" + columns[d] + "]");
                        }
                        
                    }
                }
                else if (upperColName == "LBLOOKBACKID")
                {
                    if (lbIdLookup.ContainsKey(srcData.Rows[0][columns[d]].ToString()))
                    {
                        tempQry.Append("'" + lbIdLookup[srcData.Rows[0][columns[d]].ToString()] + "'");
                    }
                    else
                    {
                        tempQry.Append("[" + columns[d] + "]");
                    }
                }
                else if((upperColName.IndexOf("DATEOFFSET") > -1 && mapping.TableName.ToUpper() != "ATLAS_BASICSURPLUSADMIN") || Array.IndexOf(AodOffsetTypeFields, upperColName) > -1)
                {
                    if (ReportHasStaticDate && !isCopy)
                    {
                        int o = Convert.ToInt32(srcData.Rows[0][columns[d]].ToString());
                        int oo = 0;
                        string aod = Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString();
                        string newPkgAod = DateTime.Parse(NewPackageAod).ToShortDateString();
                        string q = "SELECT COUNT(*) as Count FROM asOfDateInfo WHERE BasisDate = 1 and asOfDate <= '"+ newPkgAod +"' AND asOfDate > '"+ aod +"'";
                        DataTable r = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(destDB), q);
                        if(Int32.TryParse(r.Rows[0]["Count"].ToString(), out oo))
                        {
                            o = o + oo;
                        }
                        tempQry.Append(o);
                    }
                    else
                    {
                        //this should indicate that we're not copying a whole package, only a report config via the ResetToDefaults functionality
                        //gotta go get it
                        if (String.IsNullOrEmpty(NewPackageAod))
                        {
                            
                        }
                        tempQry.AppendLine(getDateOffsetSafeQuery(srcData.Rows[0][columns[d]].ToString(), columns[d], NewPackageAod));
                    }
                }
                else
                {
                    tempQry.Append("[" + columns[d] + "]");
                }
                if (d < columns.Length - 1)
                {
                    tempQry.Append(", ");
                }
            }


            tempQry.AppendLine(" FROM [" + srcDB + "].[dbo].[" + mapping.TableName + "] WHERE Id = " + sourceId + ";");
            inserts.Add(tempQry.ToString());
            inserts.Add("SELECT SCOPE_IDENTITY() as newId FROM " + mapping.TableName + ";");
            d = 0;
            if(turnReportIdInsertOn && mapping.TypeName == "Report")
            {
                qry.AppendLine("SET IDENTITY_INSERT [" + destDB + "].[dbo].[ATLAS_Report] ON");
            }
            for (d = 0; d < inserts.Count; d++)
            {
                qry.AppendLine(inserts[d]);
            }
            if (turnReportIdInsertOn && mapping.TypeName == "Report")
            {
                qry.AppendLine("SET IDENTITY_INSERT [" + destDB + "].[dbo].[ATLAS_Report] OFF");
            }
            
            //Insert the current item
            result = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(destDB), qry.ToString());

            


            if (result.Rows.Count > 0 || idToUse.Length > 0)
            {
                if (idToUse.Length > 0)
                {
                    insertedId = idToUse;
                }
                else
                {
                    insertedId = result.Rows[0]["newId"].ToString();
                }

                if (mapping.TypeName == "Document")
                {
                    FileProcessingService fs = new FileProcessingService();
                    fs.CopyByOwner(FileOwnerType.Report, Convert.ToInt32(sourceId), Convert.ToInt32(insertedId), srcDB, destDB);
                }

                foreach (ReportTableMapping childMap in mapping.ChildTables)
                {

                    result = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(srcDB), "SELECT " + childMap.TypeName + "Id FROM " + mapping.TableName + " WHERE Id = " + sourceId);
                    if (result.Rows.Count > 0)
                    {
                        sourceId = result.Rows[0][0].ToString();
                    }

                    childIds = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(srcDB), "SELECT Id FROM [" + srcDB + "].[dbo].[" + childMap.TableName + "] WHERE [" + srcDB + "].[dbo].[" + childMap.TableName + "]." + childMap.LinkColumn + " = " + sourceId);
                    childLitValues.Clear();
                    childLitValues.Add(childMap.LinkColumn, insertedId);

                    foreach (DataRow dr in childIds.Rows)
                    {
                        InsertNew(childMap, dr[0].ToString(), "", srcDB, destDB, childLitValues, false, bsAdminIdLookup, lbIdLookup, isCopy, false);
                    }
                }
            }

            if (mapping.TableName == "ATLAS_Report")
            {
                
                result = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(destDB), "SELECT ShortName FROM [" + destDB + "].[dbo]." + mapping.TableName + " WHERE Id = " + insertedId);
                if (result.Rows.Count > 0)
                {
                    string configName = result.Rows[0][0].ToString();
                    ReportTableMapping configMapping = new ReportTableMapping();
                    configMapping = getTableMapping(configName, "", configMapping);
                    if(!String.IsNullOrEmpty(srcRepId) && usingDefaults && !isCopy)
                    {
                        srcDB = "DDW_ATLAS_Templates";
                    }
                    
                    if (configMapping != null)
                    {
                        //insert the config that corresponds to 
                        InsertNew(configMapping, sourceId, insertedId, srcDB, destDB, new Dictionary<string, string>(), true, bsAdminIdLookup, lbIdLookup, isCopy, false);
                    }
                }
            }

            tempQry.Clear();

            return insertedId;
        }

        public DataTable getAtlasTemplatesSourceReportData(string tblName, string[] cols, string srcRepId, string pkgId, string conStr)
        {
            DataTable ret = new DataTable();
            StringBuilder q = new StringBuilder();
            int d = 0;
            q.AppendLine("SELECT ");
            for (d = 0; d < cols.Length; d++)
            {
                if (d == 0)
                {
                    q.AppendLine(tblName + "." + cols[d]);
                }
                else
                {
                    q.AppendLine(","+ tblName +"." + cols[d]);
                }
            }
            q.AppendLine("FROM [DDW_Atlas_Templates].[dbo]." + tblName);
            q.AppendLine("INNER JOIN [DDW_Atlas_Templates].[dbo].ATLAS_Packages");
            q.AppendLine("ON [DDW_Atlas_Templates].[dbo].ATLAS_Report.PackageId = [DDW_Atlas_Templates].[dbo].ATLAS_Packages.id");
            q.AppendLine("WHERE CAST([DDW_Atlas_Templates].[dbo].ATLAS_Packages.AsOfDate as date) = (SELECT AsOfDate FROM ATLAS_Packages WHERE Id = " + pkgId + ")");
            q.AppendLine("AND ATLAS_Report.SourceReportId = " + srcRepId);
            ret = Utilities.ExecuteSql(conStr, q.ToString());
            return ret;
        }

        public string getTemplateReportId(string rootDB, string rootReportId, string newRootAsOfDate)
        {
            StringBuilder qry = new StringBuilder();
            qry.AppendLine("SELECT templateReport.Id from [DDW_Atlas_Templates].dbo.ATLAS_Report templateReport");
            qry.AppendLine("INNER JOIN  [DDW_Atlas_Templates].dbo.ATLAS_Packages templatePkg");
            qry.AppendLine("ON templatePkg.Id = templateReport.PackageId");
            qry.AppendLine("INNER JOIN (");
            qry.AppendLine("SELECT ATLAS_Packages.SourcePackageId, ATLAS_Report.SourceReportId FROM [" + rootDB + "].[dbo].ATLAS_Report");
            qry.AppendLine("INNER JOIN [" + rootDB + "].[dbo].ATLAS_Packages");
            qry.AppendLine("ON [" + rootDB + "].[dbo].ATLAS_Report.PackageId = [" + rootDB + "].[dbo].ATLAS_Packages.Id");
            qry.AppendLine(" WHERE [" + rootDB + "].[dbo].Atlas_Report.Id = " + rootReportId);
            qry.AppendLine(") as rootTbl");
            qry.AppendLine("ON templatePkg.SourcePackageId = rootTbl.SourcePackageId");
            qry.AppendLine("AND templateReport.SourceReportId = rootTbl.SourceReportId");
            qry.AppendLine("WHERE templatePkg.AsOfDate = '" + newRootAsOfDate + "'");

            return "";
        }

        public List<string> getDependentColumns(string[] columns, string linkCol)
        {
            //So this is for a relationship like OneChart -> Chart
            //Chart is not a child of One Chart but the flow of the data would imply that.
            //We pretty much need to go back up the tree, insert a Chart, then use that new Id as a literal in the OneChartInsert
            string dependencyPattern = @"\w+Id\b";
            Regex depRegx = new Regex(dependencyPattern);
            List<string> ret = new List<string>();
            int x = 0;
            for (x = 0; x < columns.Length; x++)
            {
                if (depRegx.IsMatch(columns[x]) && columns[x] != linkCol)
                {
                    ret.Add(columns[x]);
                }
            }
            return ret;
        }

        public string[] getColumns(string tableName, string srcDB, string destDB, bool includeId)
        {
            string qry = "SELECT name from sys.columns WHERE object_id = OBJECT_ID('dbo." + tableName + "')";
            if (includeId)
            {
                qry += " AND name <> 'Id'";
            }
            DataTable srcCols = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(srcDB), qry);
            DataTable destCols = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(destDB), qry);
            if (srcCols.Rows.Count != destCols.Rows.Count)
            {
                return new string[] { };
            }
            else
            {
                string[] cols = new string[srcCols.Rows.Count];
                for (int r = 0; r < srcCols.Rows.Count; r++)
                {
                    cols[r] = srcCols.Rows[r]["name"].ToString();
                }
                return cols;
            }
        }

        private ReportTableMapping getTableMapping(string name, string parentName, ReportTableMapping mapping)
        {

            //When trying to map out Chart objects, the "parent" isn't actually a parent. For example OneChart is not really a Parent of Chart but they are linked. 
            //In these cases we don't want to give OneChartId as a link column for the Chart Table Mapping
            //string[] DoNotCopy = new string[] { "ScenarioType", "SimulationType", "BasicSurplusAdmin", "Information", "LBLookback"};
            Type thisType = getTypeByName(name);
            if (thisType == null || (DoNotCopy.Contains(name) && parentName.Length > 0))
            {
                return null;
            }
            PropertyInfo[] props = thisType.GetProperties();
            bool hasParentLink = false;
            string currentName = name;
            foreach (var prop in props)
            {
                if (prop.Name == parentName + "Id")
                {
                    hasParentLink = true;
                }
            }
            if (!hasParentLink)
            {
                parentName = "";
            }

            mapping.TableName = getDbName(name);
            if (mapping.TableName.Length > 0)
            {
                mapping.LinkColumn = parentName + "Id";
                mapping.ChildTables = new List<ReportTableMapping>();
                mapping.TypeName = name;

                string[] childNames = getChildrenNames(name, parentName);
                for (var c = 0; c < childNames.Length; c++)
                {
                    ReportTableMapping newMap = new ReportTableMapping();
                    newMap = getTableMapping(childNames[c], currentName, newMap);
                    mapping.ChildTables.Add(newMap);
                }
                return mapping;
            }
            else
            {
                return null;
            }

        }

        public string getDateOffsetSafeQuery(string srcOffset, string colName, string newAOD)
        {
            string ret = "CASE WHEN ";
            int maxOffset = Int32.Parse(Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), "select count(*) - 1 from asofDAteInfo where basisDate = 1 and asOfDate <= '"+ newAOD +"'").Rows[0][0].ToString());
            ret += srcOffset + " >= "+ maxOffset + " THEN " + maxOffset;
            ret += " ELSE "+ srcOffset +" END AS " + colName;
            return ret;
        }

        public Type getTypeByName(string name)
        {
            Type tempType = typeof(ExecutiveRiskSummary);
            try
            {
                Type curType = tempType.Assembly.GetTypes().Where(t => String.Equals(t.Namespace, "Atlas.Institution.Model", StringComparison.Ordinal) && t.Name == name).ToArray()[0];
                return curType;
            }
            catch (Exception e)
            {
                return null;
            }


        }

        public string getDbName(string entityName)
        {
            Type curType = getTypeByName(entityName);
            try
            {
                TableAttribute tAttr = (TableAttribute)(curType.GetCustomAttributes(typeof(TableAttribute), true)[0]);
                return tAttr.Name;
            }
            catch (Exception e)
            {
                return "";
            }

        }

        public string[] getChildrenNames(string parentName, string parentParentName)
        {
            List<string> names = new List<string>();
            //string[] DoNotCopy = new string[] { "ScenarioType", "SimulationType", "BasicSurplusAdmin", "Information", "LBLookback" };
            Type pType = getTypeByName(parentName);
            PropertyInfo[] props = pType.GetProperties();
            foreach (var prop in props)
            {
                if (Array.IndexOf(DoNotCopy, prop.Name) > -1)
                {
                    continue;
                }
                if(false)//if (prop.PropertyType.FullName.IndexOf("Atlas.Institution.Model") > -1 && prop.PropertyType.Name.IndexOf("ICollection") < 0 && prop.PropertyType.Name != parentParentName && !names.Contains(prop.PropertyType.Name))
                {
                    names.Add(prop.PropertyType.Name);
                }
                else if (prop.PropertyType.Name.IndexOf("ICollection") > -1)
                {
                    names.Add(prop.PropertyType.GenericTypeArguments[0].Name);
                }
            }
            return names.ToArray();
        }

        public string[] getChildDbNames(string entityName)
        {
            List<string> tblNames = new List<string>();
            Type eType = getTypeByName(entityName);
            PropertyInfo[] props = eType.GetProperties();
            foreach (var prop in props)
            {
                if (prop.PropertyType.Name.IndexOf("ICollection") > -1)
                {
                    tblNames.Add(getDbName(prop.PropertyType.GenericTypeArguments[0].Name));
                }
            }
            return tblNames.ToArray();
        }

        public string NewRollPackage(string pkgIdToCopy, string asOfDate, string srcDB, string destDB, Dictionary<string, string> bsAdminIdLookup, Dictionary<string, string> lookbackIdLookup)
        {
            //string atlasDb = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            Dictionary<string, string> otherLits = new Dictionary<string, string>();
            otherLits.Add("AsOfDate", asOfDate);

            string sql = NewCopyReportingItem("Package", pkgIdToCopy, "", srcDB, destDB, otherLits, bsAdminIdLookup, lookbackIdLookup, false);
            return sql;
        }

        public string NewCopyPackage(string pkgIdToCopy, bool copyFromTemplates, string pkgName)
        {
            string srcDB = copyFromTemplates ? "DDW_ATLAS_Templates" : Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string destDB = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string curAOD = Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString();
            Dictionary<string, string> lits = new Dictionary<string, string>();
            lits.Add("AsOfDate", curAOD);
            if (!copyFromTemplates)
            {
                lits.Add("Name", pkgName);
            }
            string sql = NewCopyReportingItem("Package", pkgIdToCopy, "", srcDB, destDB, lits, new Dictionary<string, string>(), new Dictionary<string, string>(), true);
            return sql;
        }

        public int CopyPackage(int id, string asOfDate)
        {

            string atlasDb = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();

            //Use Atlas Database Name From Profile Object to Build Connection String
            string conStr = Utilities.BuildInstConnectionString("");

            //If Copying a source tempalte set flag becuase we have to do special things
            bool sourceTemplate = false;
            if (atlasDb == "DDW_Atlas_Templates")
            {
                sourceTemplate = true;

                DataTable retTable = Utilities.ExecuteSql(conStr, string.Format("SELECT id FROM Atlas_Packages WHERE sourcePackageId = {0} AND asOfDate = '{1}'", id, asOfDate));
                if (retTable.Rows.Count > 0)
                {
                    throw new Exception("A Template has already been created for that as of date from this template. There can only be one");
                }

            }

            StringBuilder sqlQuery = new StringBuilder();
            //This SQL will execute and then get the new id of the report, this is needed to we can insert all relational configuration stuff
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Packages] ");
            sqlQuery.AppendLine("            ([AsOfDate] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[ModifiedBy] ");
            sqlQuery.AppendLine("            ,[DateModified] ");
            sqlQuery.AppendLine("            ,[SourcePackageId] ");
            sqlQuery.AppendLine("            ,[Consolidation_Id]) ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			'{1}' ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[ModifiedBy] ");
            sqlQuery.AppendLine("            ,[DateModified] ");
            if (sourceTemplate)
            {
                sqlQuery.AppendLine("            ,CASE WHEN [SourcePackageId] IS NULL THEN id ELSE sourcePackageId END "); //If copying for first time set source package Id to id otherwise leave it as is so it can go back to the original
            }
            else
            {
                sqlQuery.AppendLine("            ,sourcePackageId "); // if not copying source template use whatever is in there
            }

            sqlQuery.AppendLine("            ,[Consolidation_Id] ");
            sqlQuery.AppendLine(" 		   FROM [{0}].[dbo].[ATLAS_Packages] WHERE id = {2} ");
            sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; "); //To return new package Id

            //Get new Id
            DataTable dt = Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), atlasDb, asOfDate, id));
            int newId = int.Parse(dt.Rows[0][0].ToString());


            //Check to See if source package was created from template
            DataTable packageTable = Utilities.ExecuteSql(conStr, string.Format("SELECT id, sourcePackageId FROM Atlas_Packages WHERE Id = {0}", id));
            int templatePackageId = 0;
            if (packageTable.Rows[0]["sourcePackageId"].ToString() != "")
            {
                templatePackageId = GetSourcePackageId(packageTable.Rows[0]["sourcePackageId"].ToString(), asOfDate);
            }

            //Select All Reports In Template and Copy Them Over
            DataTable initReports = Utilities.ExecuteSql(conStr, "SELECT  [Id], [PackageId], [Name] ,[FirstPageNumber] ,[Priority] ,[HasSplitFootnotes] ,[sourceReportId] ,[Defaults] FROM [ATLAS_Report] WHERE packageId = '" + id + "' ");
            foreach (DataRow dr in initReports.Rows)
            {
                int sourceReportId = 0;
                //Check to See if we need to update  based off of template
                if (dr["sourceReportId"].ToString() != "" && dr["Defaults"].ToString().ToUpper() == "TRUE")
                {
                    sourceReportId = GetSourceReportId(templatePackageId.ToString(), dr["sourceReportId"].ToString());
                }

                //First Do Report Table
                ReportInsert(atlasDb, atlasDb, id, newId, int.Parse(dr["Id"].ToString()), sourceTemplate, sourceReportId, false, 0, asOfDate);
            }

            return newId;
        }

        public int GetSourcePackageId(string sourcePackageId, string asOfDate)
        {
            DataTable srcPkgTable = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), String.Format(" SELECT id FROM DDW_Atlas_Templates.dbo.Atlas_Packages WHERE (sourcePackageId = {0} OR id = {0}) and asOfDate = '{1}' ", sourcePackageId, asOfDate));
            if (srcPkgTable.Rows.Count > 0)
            {
                return int.Parse(srcPkgTable.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }

        public int GetSourceReportId(string templatePackageId, string sourceReportId)
        {

            DataTable srcTable = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), string.Format("SELECT id FROM DDW_Atlas_Templates.dbo.Atlas_Report WHERE packageId = {0} and (sourceReportId = {1})", templatePackageId, sourceReportId));
            if (srcTable.Rows.Count == 0)
            {
                srcTable = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), string.Format("SELECT id FROM DDW_Atlas_Templates.dbo.Atlas_Report WHERE packageId = {0} and (id = {1})", templatePackageId, sourceReportId));
            }

            int srcRepId = 0;
            if (srcTable.Rows.Count > 0)
            {
                srcRepId = int.Parse(srcTable.Rows[0][0].ToString());
            }

            return srcRepId;
        }

        public bool CopyPackageFromTemplate(int packageId, int templateId)
        {

            string atlasDatabase = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string atlasTemplateDatabase = "DDW_Atlas_Templates";
            string templateConStr = Utilities.BuildInstConnectionString("DDW_Atlas_Templates");
            string bankConStr = Utilities.BuildInstConnectionString(atlasDatabase);

            //Select All Reports In Template and Copy Them Over but first update source package Id 
            Utilities.ExecuteCommand(templateConStr, string.Format("UPDATE [{0}].[dbo].[ATLAS_Packages] SET sourcePackageId = (SELECT CASE WHEN sourcePackageId IS NULL THEN id ELSE sourcePackageId END as id FROM [{1}].[dbo].ATLAS_Packages WHERE Id = {2}) WHERE Id = {3}", atlasDatabase, atlasTemplateDatabase, templateId, packageId));

            InstitutionContext ctx = new InstitutionContext(templateConStr);
            string newAsOfDate = ctx.Packages.Where(s => s.Id == templateId).FirstOrDefault().AsOfDate.ToShortDateString();



            DataTable initReports = Utilities.ExecuteSql(templateConStr, "SELECT  [Id], [PackageId], [Name] ,[FirstPageNumber] ,[Priority] ,[HasSplitFootnotes] ,[sourceReportId] ,[Defaults] FROM [ATLAS_Report] WHERE packageId = '" + templateId + "' ");
            foreach (DataRow dr in initReports.Rows)
            {
                //First Do Report Table
                ReportInsert(atlasTemplateDatabase, atlasDatabase, templateId, packageId, int.Parse(dr["Id"].ToString()), true, 0, true, 0, newAsOfDate);

            }

            return true;
        }

        public bool ResetReportToDefaults(string reportId)
        {
            StringBuilder qry = new StringBuilder();
            //Build It Based Off Of Profile Object
            string conStr = Utilities.BuildInstConnectionString("");
            string destDB = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string srcDB = "DDW_ATLAS_Templates";

            //Delete the configuration counter part to the ATLAS_Report we have the Id of
            qry.AppendLine("select  ");
            qry.AppendLine("rep.Id as ReportId,");
            qry.AppendLine("rep.Priority as Priority,");
            qry.AppendLine("pkg.Id as PkgId,");
            qry.AppendLine("tempRep.Id as TemplateReportId,");
            qry.AppendLine("tempPkg.Id as TemplatePackageId,");
            qry.AppendLine("rep.ShortName,");
            qry.AppendLine("rep.PackageId,");
            qry.AppendLine("rep.SourceReportId,");
            qry.AppendLine("pkg.SourcePackageId,");
            qry.AppendLine("pkg.AsOfDate");
            qry.AppendLine("from ATLAS_Report rep ");
            qry.AppendLine("inner join ATLAS_Packages pkg on rep.PackageId = pkg.Id");
            qry.AppendLine("INNER JOIN [DDW_ATLAS_Templates].[dbo].[ATLAS_Packages] tempPkg");
            qry.AppendLine("ON CAST(tempPkg.AsOfDate as date) = CAST(pkg.AsOfDate as date) AND ISNULL(tempPkg.SourcePackageId, tempPkg.Id) = pkg.SourcePackageId");
            qry.AppendLine("INNER JOIN [DDW_ATLAS_Templates].[dbo].[ATLAS_Report] tempRep ");
            qry.AppendLine("ON tempRep.PackageId = tempPkg.Id AND ISNULL(tempRep.SourceReportId, tempRep.Id) = rep.SourceReportId");
            qry.AppendLine("where rep.Id = " + reportId);

            DataTable reportInfo = Utilities.ExecuteSql(conStr, qry.ToString());
            Dictionary<string, string> lits = new Dictionary<string, string>();
            lits.Add("PackageId", reportInfo.Rows[0]["PkgId"].ToString());

            //Set the NewPackageAsOfDate so when we reget the report config it knows what it's package's aod is
            qry.Clear();
            qry.AppendLine("SELECT p.AsOfDate FROM ATLAS_Packages p inner join ATLAS_Report r ON p.Id = r.PackageId WHERE r.Id = " + reportId);
            DataTable aod = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), qry.ToString());
            DateTime dt = DateTime.Parse(aod.Rows[0][0].ToString());
            NewPackageAod = dt.ToShortDateString();

            if (reportInfo.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                string configType = reportInfo.Rows[0]["ShortName"].ToString();
                string idToUse = reportInfo.Rows[0]["ReportId"].ToString();
                string parentId = reportInfo.Rows[0]["TemplateReportId"].ToString();
                string priority = reportInfo.Rows[0]["Priority"].ToString();
                ReportTableMapping configMapping = new ReportTableMapping();
                configMapping = getTableMapping(configType, "", configMapping);

                ReportTableMapping repMapping = new ReportTableMapping();
                repMapping = getTableMapping("Report", "Package", repMapping);

                DeleteReportingItem("Report", reportId, destDB, "Package");
                
                lits.Add("Priority", priority);

                //lits.Clear();
                //DeleteReportingItem(reportInfo.Rows[0]["ShortName"].ToString(), reportId, destDB, "");
                //Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), "SET IDENTITY_INSERT [" + destDB + "].[dbo].[ATLAS_Report] ON;");
                InsertNew(repMapping, reportInfo.Rows[0]["TemplateReportId"].ToString(), reportInfo.Rows[0]["ReportId"].ToString(), srcDB, destDB, lits, false, new Dictionary<string, string>(), new Dictionary<string, string>(), false, true);
                //Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), "SET IDENTITY_INSERT [" + destDB + "].[dbo].[ATLAS_Report] OFF;");
                //InsertNew(configMapping, reportInfo.Rows[0]["TemplateReportId"].ToString(), reportInfo.Rows[0]["ReportId"].ToString(), srcDB, destDB, lits, false, new Dictionary<string, string>(), new Dictionary<string, string>(), false);

            }

            return true;
        }

        public string ReportInsert(string templateDb, string bankDb, int templatePackageId, int newPackageId, int reportId, bool sourceTemplate, int sourceReportId, bool copyFromTemplate, int resetId, string newAsOfDate)
        {
            string ddwConStr = ConfigurationManager.ConnectionStrings["DDwConnection"].ToString();
            StringBuilder sqlQuery = new StringBuilder();

            int newId = resetId;
            if (resetId == 0)
            {

                //This SQL will execute and then get the new id of the report, this is needed to we can insert all relational configuration stuff
                sqlQuery.AppendLine(" /* Handles Report Generic Class */ ");
                sqlQuery.AppendLine(" INSERT INTO [" + bankDb + "].[dbo].[ATLAS_Report] ");
                sqlQuery.AppendLine("            ([PackageId] ");
                sqlQuery.AppendLine("            ,[Name] ");
                sqlQuery.AppendLine("            ,[FirstPageNumber] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                sqlQuery.AppendLine("            ,[HasSplitFootnotes] ");
                sqlQuery.AppendLine("            ,[sourceReportId] ");
                sqlQuery.AppendLine("            ,[Defaults], [ShortName], [DisplayName]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 			 " + newPackageId + " --Destination Package ");
                sqlQuery.AppendLine("            ,[Name] ");
                sqlQuery.AppendLine("            ,[FirstPageNumber] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                sqlQuery.AppendLine("            ,[HasSplitFootnotes] ");
                if (sourceTemplate)
                {
                    sqlQuery.AppendLine("            ,CASE WHEN sourceReportId IS NULL THEN [id] ELSE sourceReportId END "); //If source Report Id is null use id otherwise use source report id
                }
                else
                {
                    sqlQuery.AppendLine("            ,sourceReportId "); //Use whatever is in there for sourceReport Id
                }
                if (copyFromTemplate)
                {
                    sqlQuery.AppendLine("            ,1 --Sets Defaults to True ");
                }
                else
                {
                    sqlQuery.AppendLine("            ,[Defaults] --Sets Defaults to True ");
                }
                sqlQuery.AppendLine(" ,[ShortName], [DisplayName]");
                sqlQuery.AppendLine(" 		   FROM [" + templateDb + "].dbo.ATLAS_Report WHERE packageId = " + templatePackageId + " AND id = " + reportId + " ;");
                sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");

                //Get new Id
                DataTable dt = Utilities.ExecuteSql(ddwConStr, sqlQuery.ToString());
                newId = int.Parse(dt.Rows[0][0].ToString());
            }

            //Interest Rate Policy Guidelines
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO  [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelines] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	{1}  ");
            sqlQuery.AppendLine("   ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelines] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelines] WHERE id = {3} ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));
            //Interest Rate Polciy Guidelines Dates
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelinesDate] ");
            sqlQuery.AppendLine("            ([InterestRatePolicyGuidelinesId] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[NIISimulationTypeId] ");
            sqlQuery.AppendLine("            ,[EVESimulationTypeId] ");
            sqlQuery.AppendLine("            ,[Graph] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	{1} ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[NIISimulationTypeId] ");
            sqlQuery.AppendLine("            ,[EVESimulationTypeId] ");
            sqlQuery.AppendLine("            ,[Graph] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesDate] WHERE InterestRatePolicyGuidelinesId = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesDate] WHERE InterestRatePolicyGuidelinesId = {3} ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Load each nii comparative and insert one at a time because it has children a long with its children children
            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT id  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIComparative] WHERE InterestRatePolicyGuidelinesId = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIComparative] WHERE InterestRatePolicyGuidelinesId = {3} ");
            }
            //Pulls down all nii comps
            DataTable niiComps = Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Loop through nii comps
            foreach (DataRow niiComp in niiComps.Rows)
            {
                //First Insert Nii comp into table and get new Id 
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIComparative] ");
                sqlQuery.AppendLine("            ([InterestRatePolicyGuidelinesId] ");
                sqlQuery.AppendLine("            ,[Type] ");
                sqlQuery.AppendLine("            ,[Graph] ");
                sqlQuery.AppendLine("            ,[Priority]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	{1} ");
                sqlQuery.AppendLine("            ,[Type] ");
                sqlQuery.AppendLine("            ,[Graph] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIComparative] WHERE id = " + niiComp[0].ToString() + " ");
                }
                else
                {
                    sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIComparative] WHERE id = " + niiComp[0].ToString() + "");
                }
                sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");
                string newCompId = Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId)).Rows[0][0].ToString();

                //Pull all nii comp Scenario  Types
                sqlQuery.Clear();
                sqlQuery.AppendLine(" SELECT id ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenarioType] WHERE InterestRatePolicyGuidelinesNIIComparativeId = " + niiComp[0].ToString() + " ");
                }
                else
                {
                    sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenarioType] WHERE InterestRatePolicyGuidelinesNIIComparativeId = " + niiComp[0].ToString() + " ");
                }

                DataTable niiScenTypes = Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


                foreach (DataRow niiScenType in niiScenTypes.Rows)
                {
                    //First Inser nii SCen Type and get new Id
                    sqlQuery.Clear();
                    sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenarioType] ");
                    sqlQuery.AppendLine("            ([InterestRatePolicyGuidelinesNIIComparativeId] ");
                    sqlQuery.AppendLine("            ,[Type] ");
                    sqlQuery.AppendLine("            ,[CustomName] ");
                    sqlQuery.AppendLine("            ,[Graph] ");
                    sqlQuery.AppendLine("            ,[Priority]) ");
                    sqlQuery.AppendLine(" SELECT ");
                    sqlQuery.AppendLine(" 	        " + newCompId + " ");
                    sqlQuery.AppendLine("            ,[Type] ");
                    sqlQuery.AppendLine("            ,[CustomName] ");
                    sqlQuery.AppendLine("            ,[Graph] ");
                    sqlQuery.AppendLine("            ,[Priority] ");
                    if (sourceReportId > 0)
                    {
                        sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenarioType] WHERE id = " + niiScenType[0].ToString() + " ");
                    }
                    else
                    {
                        sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenarioType] WHERE id = " + niiScenType[0].ToString() + " ");
                    }
                    sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");
                    string newScenTypeId = Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId)).Rows[0][0].ToString();


                    //After Insert that REcord insert all of its children
                    sqlQuery.Clear();
                    sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenario] ");
                    sqlQuery.AppendLine("            ([InterestRatePolicyGuidelinesScenarioTypeId] ");
                    sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                    sqlQuery.AppendLine("            ,[Graph] ");
                    sqlQuery.AppendLine("            ,[Priority]) ");
                    sqlQuery.AppendLine(" SELECT ");
                    sqlQuery.AppendLine("            " + newScenTypeId + " ");
                    sqlQuery.AppendLine(" 			,[ScenarioTypeId] ");

                    sqlQuery.AppendLine("            ,[Graph] ");
                    sqlQuery.AppendLine("            ,[Priority] ");
                    if (sourceReportId > 0)
                    {
                        sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenario] WHERE InterestRatePolicyGuidelinesScenarioTypeId = " + niiScenType[0].ToString() + " ");
                    }
                    else
                    {
                        sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesNIIScenario] WHERE InterestRatePolicyGuidelinesScenarioTypeId = " + niiScenType[0].ToString() + " ");
                    }

                    Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));
                }



            }

            //interest rate policy Eve Stuff
            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT id  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEComparative] WHERE InterestRatePolicyGuidelinesId = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEComparative] WHERE InterestRatePolicyGuidelinesId = {3} ");
            }
            //Pulls down all nii comps
            DataTable eveComps = Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Loop throuch each eve comp
            foreach (DataRow eveComp in eveComps.Rows)
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEComparative] ");
                sqlQuery.AppendLine("            ([InterestRatePolicyGuidelinesId] ");
                sqlQuery.AppendLine("            ,[Type] ");
                sqlQuery.AppendLine("            ,[Graph] ");
                sqlQuery.AppendLine("            ,[Priority]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	{1} ");
                sqlQuery.AppendLine("            ,[Type] ");
                sqlQuery.AppendLine("            ,[Graph] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEComparative] WHERE id = " + eveComp[0].ToString() + " ");
                }
                else
                {
                    sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEComparative] WHERE id = " + eveComp[0].ToString() + " ");
                }
                sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");
                string newEveCompId = Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId)).Rows[0][0].ToString();

                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEScenario] ");
                sqlQuery.AppendLine("            ([InterestRatePolicyGuidelinesEVEComparativeId] ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Graph] ");
                sqlQuery.AppendLine("            ,[Priority]) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	" + newEveCompId + " ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Graph] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEScenario] WHERE InterestRatePolicyGuidelinesEVEComparativeId = " + eveComp[0].ToString() + " ");
                }
                else
                {
                    sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InterestRatePolicyGuidelinesEVEScenario] WHERE InterestRatePolicyGuidelinesEVEComparativeId = " + eveComp[0].ToString() + " ");
                }

                Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));
            }



            //Clear Sql Query And Do Static Gaps          
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_StaticGaps] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[LeftInstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[LeftSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[LeftScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[RightInstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[RightScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[ReportFormat] ");
            sqlQuery.AppendLine("            ,[ViewOption] ");
            sqlQuery.AppendLine("            ,[TaxEqivalentYields] ");
            sqlQuery.AppendLine("            ,[GapRatios]) ");
            sqlQuery.AppendLine(" SELECT  ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [LeftInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [LeftInstitutionDatabaseName] END ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[LeftSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[LeftScenarioTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [RightInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [RightInstitutionDatabaseName] END ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[RightScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[ReportFormat] ");
            sqlQuery.AppendLine("            ,[ViewOption] ");
            sqlQuery.AppendLine("            ,[TaxEqivalentYields] ");
            sqlQuery.AppendLine("            ,[GapRatios] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_StaticGaps] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_StaticGaps] WHERE id = {3} ");
            }


            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Clear Sql Query and Do Funding Matrix
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_FundingMatrices] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[TaxEqivalentYields]) ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	        {1} ");
            sqlQuery.AppendLine("           ,[TaxEqivalentYields] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_FundingMatrices] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_FundingMatrices] WHERE id = {3} ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Clear Sql Query and Do Funding Matrix Offsets
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_FundingMatrixOffsets] ");
            sqlQuery.AppendLine("            ([InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]");
            sqlQuery.AppendLine("            ,[SimulationTypeId]");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]");
            sqlQuery.AppendLine("            ,[FundingMatrixId]");
            sqlQuery.AppendLine("            ,[Priority]");
            sqlQuery.AppendLine("            ,[TaxEqivalentYields])");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("           [InstitutionDatabaseName] ");
            sqlQuery.AppendLine("           ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("           ,[SimulationTypeId] ");
            sqlQuery.AppendLine("           ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("           ,{1} ");
            sqlQuery.AppendLine("           ,[Priority] ");
            sqlQuery.AppendLine("           ,[TaxEqivalentYields] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_FundingMatrixOffsets] WHERE [FundingMatrixId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_FundingMatrixOffsets] WHERE [FundingMatrixId] = {3} ");
            }
            string w = sqlQuery.ToString();
            string q = String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId);
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Clear Sql Query and Do Yield Curve Shifts
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_YieldCurveShifts] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[SimulationType_Id] ");
            sqlQuery.AppendLine("            ,[WebRateAsOfDate] ");
            sqlQuery.AppendLine("            ,[Index] ");
            sqlQuery.AppendLine("            ,[FHLBAdvanceDistrict] ");
            sqlQuery.AppendLine("            ,[HistoricRateDate1] ");
            sqlQuery.AppendLine("            ,[HistoricRateDate2] ");
            sqlQuery.AppendLine("            ,[CompareStartDate] ");
            sqlQuery.AppendLine("            ,[CompareEndDate]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[SimulationType_Id] ");
            sqlQuery.AppendLine("            ,[WebRateAsOfDate] ");
            sqlQuery.AppendLine("            ,[Index] ");
            sqlQuery.AppendLine("            ,[FHLBAdvanceDistrict] ");
            sqlQuery.AppendLine("            ,[HistoricRateDate1] ");
            sqlQuery.AppendLine("            ,[HistoricRateDate2] ");
            sqlQuery.AppendLine("            ,[CompareStartDate] ");
            sqlQuery.AppendLine("            ,[CompareEndDate] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_YieldCurveShifts] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_YieldCurveShifts] WHERE id = {3};");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_YieldCurveShiftPeriodCompare] ");
            sqlQuery.AppendLine("            ([YieldCurveShiftId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[IndexA] ");
            sqlQuery.AppendLine("            ,[IndexB]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[IndexA] ");
            sqlQuery.AppendLine("            ,[IndexB] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_YieldCurveShiftPeriodCompare] WHERE YieldCurveShiftId = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_YieldCurveShiftPeriodCompare] WHERE YieldCurveShiftId = {3};");
            }

            //Yield Curve Shift Scenarios
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_YieldCurveShiftScenarioType] ");
            sqlQuery.AppendLine("            ([YieldCurveShiftId] ");
            sqlQuery.AppendLine("            ,[WebRateScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[WebRateScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");

            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_YieldCurveShiftScenarioType] WHERE [YieldCurveShiftId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 	FROM [{2}].[dbo].[ATLAS_YieldCurveShiftScenarioType] WHERE [YieldCurveShiftId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Core Funding Utilizsation
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CoreFundingUtilizations] ");
            sqlQuery.AppendLine("    ([Id]");
            sqlQuery.AppendLine("    ,[InstitutionDatabaseName]");
            sqlQuery.AppendLine("    ,[SimulationTypeId]");
            sqlQuery.AppendLine("    ,[ShowPolicies]");
            sqlQuery.AppendLine("    ,[AssetDetails])");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ShowPolicies]");
            sqlQuery.AppendLine("            ,[AssetDetails] ");

            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CoreFundingUtilizations] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 	FROM [{2}].[dbo].[ATLAS_CoreFundingUtilizations] WHERE [Id] = {3}; ");
            }

            //Core Funding Utilizsation DateOffsets
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CoreFundingUtilizationDateOffsets] ");
            sqlQuery.AppendLine("            ([CoreFundingUtilizationId] ");
            sqlQuery.AppendLine("            ,[OffSet] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[OffSet]  ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CoreFundingUtilizationDateOffsets] WHERE [CoreFundingUtilizationId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CoreFundingUtilizationDateOffsets] WHERE [CoreFundingUtilizationId] = {3}; ");
            }

            //Core Funding Utilizsation ScenarioTypes
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CoreFundingUtilizationScenarioType] ");
            sqlQuery.AppendLine("            ([CoreFundingUtilizationId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]  ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CoreFundingUtilizationScenarioType] WHERE [CoreFundingUtilizationId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CoreFundingUtilizationScenarioType] WHERE [CoreFundingUtilizationId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Balance Sheet Mix
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_BalanceSheetMixes] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[PriorAsOfDateOffset]");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[LiabilityOption] ");
            sqlQuery.AppendLine("            ,[NationalCD] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[PriorAsOfDateOffset]");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[LiabilityOption] ");
            sqlQuery.AppendLine("            ,[NationalCD] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_BalanceSheetMixes] WHERE [id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_BalanceSheetMixes] WHERE [id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Balance Sheet Compare
            sqlQuery.Clear();
            sqlQuery.AppendLine("INSERT INTO [{0}].[dbo].[ATLAS_BalanceSheetCompares]");
            sqlQuery.AppendLine("           ([Id]");
            sqlQuery.AppendLine("           ,[LeftInstitutionDatabaseName]");
            sqlQuery.AppendLine("           ,[LeftAsOfDateOffset]");
            sqlQuery.AppendLine("           ,[LeftSimulationTypeId]");
            sqlQuery.AppendLine("           ,[LeftScenarioTypeId]");
            sqlQuery.AppendLine("           ,[LeftStartDate]");
            sqlQuery.AppendLine("           ,[LeftEndDate]");
            sqlQuery.AppendLine("           ,[RightInstitutionDatabaseName]");
            sqlQuery.AppendLine("           ,[RightAsOfDateOffset]");
            sqlQuery.AppendLine("           ,[RightSimulationTypeId]");
            sqlQuery.AppendLine("           ,[RightScenarioTypeId]");
            sqlQuery.AppendLine("           ,[RightStartDate]");
            sqlQuery.AppendLine("           ,[RightEndDate]");
            sqlQuery.AppendLine("           ,[ViewOption]");
            sqlQuery.AppendLine("           ,[BalanceType]");
            sqlQuery.AppendLine("           ,[TaxEqivalentYields]");
            sqlQuery.AppendLine("           ,[Balances]");
            sqlQuery.AppendLine("           ,[Rates])");
            sqlQuery.AppendLine("SELECT {1}");
            sqlQuery.AppendLine("      ,CASE WHEN [LeftInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [LeftInstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("      ,[LeftAsOfDateOffset]");
            sqlQuery.AppendLine("      ,[LeftSimulationTypeId]");
            sqlQuery.AppendLine("      ,[LeftScenarioTypeId]");
            sqlQuery.AppendLine("      ,[LeftStartDate]");
            sqlQuery.AppendLine("      ,[LeftEndDate]");
            sqlQuery.AppendLine("       ,CASE WHEN [RightInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [RightInstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("      ,[RightAsOfDateOffset]");
            sqlQuery.AppendLine("      ,[RightSimulationTypeId]");
            sqlQuery.AppendLine("      ,[RightScenarioTypeId]");
            sqlQuery.AppendLine("      ,[RightStartDate]");
            sqlQuery.AppendLine("      ,[RightEndDate]");
            sqlQuery.AppendLine("      ,[ViewOption]");
            sqlQuery.AppendLine("      ,[BalanceType]");
            sqlQuery.AppendLine("      ,[TaxEqivalentYields]");
            sqlQuery.AppendLine("      ,[Balances]");
            sqlQuery.AppendLine("      ,[Rates]");
            if (sourceReportId > 0)
                sqlQuery.AppendLine("FROM [DDW_Atlas_Templates].[dbo].[ATLAS_BalanceSheetCompares] WHERE [id] = " + sourceReportId);
            else
                sqlQuery.AppendLine("FROM [{2}].[dbo].[ATLAS_BalanceSheetCompares] WHERE [id] = {3};");
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //BasicSurplusReport
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_BasicSurplusReport] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_BasicSurplusReport] WHERE [id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_BasicSurplusReport] WHERE [id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Sim Compare
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SimCompares] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[LeftGraphName] ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightGraphName] ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[ComparativeScenario] ");
            sqlQuery.AppendLine("            ,[LeftInstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[RightInstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[LeftGraphName] ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightGraphName] ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[ComparativeScenario] ");
            sqlQuery.AppendLine("            ,CASE WHEN [LeftInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [LeftInstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,CASE WHEN [RightInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [RightInstitutionDatabaseName] END  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SimCompares] WHERE [id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SimCompares] WHERE [id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Sim Compare Scenario Types
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SimCompareScenarioType] ");
            sqlQuery.AppendLine("            ([SimCompareId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SimCompareScenarioType] WHERE [SimCompareId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SimCompareScenarioType] WHERE [SimCompareId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Sim Compare Simulation Types
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SimCompareSimulationType] ");
            sqlQuery.AppendLine("            ([Side] ");
            sqlQuery.AppendLine("            ,[SimCompareId] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            [Side] ");
            sqlQuery.AppendLine("            ,{1} ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SimCompareSimulationType] WHERE [SimCompareId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SimCompareSimulationType] WHERE [SimCompareId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Cover Page 
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CoverPage] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDate] ");
            sqlQuery.AppendLine("            ,[PackageName] ");
            sqlQuery.AppendLine("            ,[MeetingDate] ");
            sqlQuery.AppendLine("            ,[Consultant] ");
            sqlQuery.AppendLine("            ,[Analyst]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,'" + newAsOfDate + "' ");
            sqlQuery.AppendLine("            ,[PackageName] ");
            sqlQuery.AppendLine("            ,[MeetingDate] ");
            sqlQuery.AppendLine("            ,[Consultant] ");
            sqlQuery.AppendLine("            ,[Analyst] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CoverPage] WHERE [id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CoverPage] WHERE [id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Section Page
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SectionPage] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[Appendix]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,Title ");
            sqlQuery.AppendLine("            ,Appendix ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SectionPage] WHERE [id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SectionPage] WHERE [id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //One Chart

            //First Insert Chart From One Chart
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Chart] ");
            sqlQuery.AppendLine("            ([Name] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            [Name] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Chart] WHERE [id] in (SELECT chartId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_OneCharts] WHERE id = " + sourceReportId + ") ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Chart] WHERE [id] in (SELECT chartId FROM [{2}].[dbo].[ATLAS_OneCharts] WHERE id = {3}); ");
            }
            sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");
            int newChartId = 0;
            //Get new Id
            int.TryParse(Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId)).Rows[0][0].ToString(), out newChartId);

            if (newChartId > 0)
            {
                //Now That I have new Id I can insert its scenario Types
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ChartScenarioType] ");
                sqlQuery.AppendLine("            ([ChartId] ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Priority]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("            " + newChartId + " ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ChartScenarioType] WHERE chartId in (SELECT chartId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_OneCharts] WHERE id = " + sourceReportId + "); ");
                }
                else
                {
                    sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ChartScenarioType] WHERE chartId IN (SELECT chartId FROM [{2}].[dbo].[ATLAS_OneCharts] WHERE id = {3}); ");
                }
                Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

                //Now I can finally insert the one chart
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_OneCharts] ");
                sqlQuery.AppendLine("            ([Id] ");
                sqlQuery.AppendLine("            ,[ChartId] ");
                sqlQuery.AppendLine("            ,[TaxEquivalentYields]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("            {1} ");
                sqlQuery.AppendLine("            ," + newChartId + " ");
                sqlQuery.AppendLine("            ,[TaxEquivalentYields] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_OneCharts] WHERE Id = " + sourceReportId + "; ");
                }
                else
                {
                    sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_OneCharts] WHERE Id = {3}; ");
                }
                Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));
            }


            //Two Chart
            //First Insert LEft Chart From Chart
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Chart] ");
            sqlQuery.AppendLine("            ([Name] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            [Name] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Chart] WHERE [id] in (SELECT leftChartId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_TwoCharts] WHERE id = " + sourceReportId + ") ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Chart] WHERE [id] in (SELECT leftChartId FROM [{2}].[dbo].[ATLAS_TwoCharts] WHERE id = {3}); ");
            }
            sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");
            int newLeftChartId = 0;
            //Get new Id
            int.TryParse(Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId)).Rows[0][0].ToString(), out newLeftChartId);

            if (newLeftChartId > 0)
            {
                //Now That I have new Id I can insert its scenario Types
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ChartScenarioType] ");
                sqlQuery.AppendLine("            ([ChartId] ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Priority]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("            " + newLeftChartId + " ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ChartScenarioType] WHERE chartId in (SELECT leftChartId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_TwoCharts] WHERE id = " + sourceReportId + "); ");
                }
                else
                {
                    sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ChartScenarioType] WHERE chartId IN (SELECT leftChartId FROM [{2}].[dbo].[ATLAS_TwoCharts] WHERE id = {3}); ");
                }
                Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            }

            //Right Chart for Two Chart
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Chart] ");
            sqlQuery.AppendLine("            ([Name] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            [Name] ");
            sqlQuery.AppendLine("            ,[IsQuarterly] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Chart] WHERE [id] in (SELECT rightChartId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_TwoCharts] WHERE id = " + sourceReportId + ") ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Chart] WHERE [id] in (SELECT rightChartId FROM [{2}].[dbo].[ATLAS_TwoCharts] WHERE id = {3}); ");
            }
            sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId; ");
            int newRightChartId = 0;
            //Get new Id
            int.TryParse(Utilities.ExecuteSql(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId)).Rows[0][0].ToString(), out newRightChartId);

            if (newRightChartId > 0)
            {
                //Now That I have new Id I can insert its scenario Types
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ChartScenarioType] ");
                sqlQuery.AppendLine("            ([ChartId] ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Priority]) ");
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine("            " + newRightChartId + " ");
                sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                if (sourceReportId > 0)
                {
                    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ChartScenarioType] WHERE chartId in (SELECT rightChartId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_TwoCharts] WHERE id = " + sourceReportId + "); ");
                }
                else
                {
                    sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ChartScenarioType] WHERE chartId IN (SELECT rightChartId FROM [{2}].[dbo].[ATLAS_TwoCharts] WHERE id = {3}); ");
                }
                Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            }

            //Now That Charts Have been Moved Over Move Over Two Chart
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_TwoCharts] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[LeftChartId] ");
            sqlQuery.AppendLine("            ,[RightChartId] ");
            sqlQuery.AppendLine("            ,[TaxEquivalentYields]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ," + newLeftChartId + " ");
            sqlQuery.AppendLine("            ," + newRightChartId + " ");
            sqlQuery.AppendLine("            ,[TaxEquivalentYields] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_TwoCharts] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_TwoCharts] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Net Cash Flow
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_NetCashflow] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[DefinedGrouping] ");
            sqlQuery.AppendLine("            ,[ExcludePrepayments]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[DefinedGrouping] ");
            sqlQuery.AppendLine("            ,[ExcludePrepayments] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_NetCashflow] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_NetCashflow] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //NEt Cashflow SImulation/Scenario/OffSet
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_NetCashflowSimulationScenario] ");
            sqlQuery.AppendLine("            ([AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[NetCashflowId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine("        SELECT ");
            sqlQuery.AppendLine("            [AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,{1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_NetCashflowSimulationScenario] WHERE NetCashflowId = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_NetCashflowSimulationScenario] WHERE NetCashflowId = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //CashFlow Report
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Cashflow] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[ReportType] ");
            sqlQuery.AppendLine("            ,[Compare] ");
            sqlQuery.AppendLine("            ,[DefinedGrouping] ");
            sqlQuery.AppendLine("            ,[ExcludePrepayments]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[ReportType] ");
            sqlQuery.AppendLine("            ,[Compare] ");
            sqlQuery.AppendLine("            ,[DefinedGrouping] ");
            sqlQuery.AppendLine("            ,[ExcludePrepayments] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Cashflow] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Cashflow] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Cashflow Support Tables
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CashflowSimulationScenario] ");
            sqlQuery.AppendLine("            ([AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[CashflowReportId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            [AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,{1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CashflowSimulationScenario] WHERE [cashflowReportId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CashflowSimulationScenario] WHERE [CashflowReportId] = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Summary Of Rates
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SummaryRates] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SummaryRates] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SummaryRates] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Summary Of Rate Support Tables
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SummaryRateScenario] ");
            sqlQuery.AppendLine("            ([SummaryRateId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1}");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SummaryRateScenario] WHERE [SummaryRateId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SummaryRateScenario] WHERE [SummaryRateId] = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Loan Cap Floor
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_LoanCapFloors] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[SimulationType_Id] ");
            sqlQuery.AppendLine("            ,[Type] ");
            sqlQuery.AppendLine("            ,[Mode] ");
            sqlQuery.AppendLine("            ,[BPBreakout] ");
            sqlQuery.AppendLine("            ,[TopAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[BottomAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[BPBreakoutTitles] ");
            sqlQuery.AppendLine("            ,[BpEndpoints] ");
            sqlQuery.AppendLine("            ,[TopInstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[BottomInstitutionDatabaseName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[SimulationType_Id] ");
            sqlQuery.AppendLine("            ,[Type] ");
            sqlQuery.AppendLine("            ,[Mode] ");
            sqlQuery.AppendLine("            ,[BPBreakout] ");
            sqlQuery.AppendLine("            ,[TopAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[BottomAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[BPBreakoutTitles] ");
            sqlQuery.AppendLine("            ,[BpEndpoints] ");
            sqlQuery.AppendLine("            ,CASE WHEN [TopInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [TopInstitutionDatabaseName] END");
            sqlQuery.AppendLine("            ,CASE WHEN [BottomInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [BottomInstitutionDatabaseName] END ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_LoanCapFloors] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_LoanCapFloors] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //EVE/NEV Assumptions
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_EveNevAssumptions] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[EnhancedEve] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[DepositType] ");
            sqlQuery.AppendLine("            ,[DepositNotes]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[EnhancedEve] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[DepositType] ");
            sqlQuery.AppendLine("            ,[DepositNotes] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_EveNevAssumptions] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_EveNevAssumptions] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));




            //ASC825 Balance Sheet And MarketValue
            sqlQuery.Clear();
            sqlQuery.AppendLine("            INSERT INTO [{0}].[dbo].[ATLAS_ASC825BalanceSheet] ");
            sqlQuery.AppendLine("           ([Id] ");
            sqlQuery.AppendLine("           ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("           ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("           ,[SimulationTypeId] ");
            sqlQuery.AppendLine("           ,[ScenarioTypeId]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("           ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("           ,[SimulationTypeId] ");
            sqlQuery.AppendLine("           ,[ScenarioTypeId] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825BalanceSheet] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ASC825BalanceSheet] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //ASC 825 Data Source
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ASC825DataSource] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825DataSource] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ASC825DataSource] WHERE Id = {3}; ");
            }


            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //SimulationSummary
            sqlQuery.Clear();
            sqlQuery.AppendLine("            INSERT INTO [{0}].[dbo].[ATLAS_SimulationSummary] ");
            sqlQuery.AppendLine("           ([Id] ");
            sqlQuery.AppendLine("           ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("           ,[SimulationTypeId] ");
            sqlQuery.AppendLine("           ,[ReportSelection] ");
            sqlQuery.AppendLine("           ,[TaxEquivalent] ");
            sqlQuery.AppendLine("           ,[Grouping] ");
            sqlQuery.AppendLine("           ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("           ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("           ,[MonthlyQuarterly] )");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("           ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("           ,[SimulationTypeId] ");
            sqlQuery.AppendLine("           ,[ReportSelection] ");
            sqlQuery.AppendLine("           ,[TaxEquivalent] ");
            sqlQuery.AppendLine("           ,[Grouping] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("           ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("           ,[MonthlyQuarterly] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SimulationSummary] WHERE Id = " + sourceReportId + "; ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SimulationSummary] WHERE Id = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));




            //ASC825 Market Value
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ASC825MarketValue] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ReportType]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ReportType] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825MarketValue] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_ASC825MarketValue] WHERE id = {3};");
            }

            //ASC825 Market Value Scenarios
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ASC825MarketValueScenarioType] ");
            sqlQuery.AppendLine("            ([ASC825MarketValueId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825MarketValueScenarioType] WHERE [ASC825MarketValueId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ASC825MarketValueScenarioType] WHERE [ASC825MarketValueId] = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Rate Change Matrix
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_RateChangeMatrix] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ReportType]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ReportType] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_RateChangeMatrix] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_RateChangeMatrix] WHERE id = {3};");
            }

            //ASC825 Rate Change Matrix Scenarios
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_RateChangeMatrixScenario] ");
            sqlQuery.AppendLine("            ([RateChangeMatrixId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_RateChangeMatrixScenario] WHERE [RateChangeMatrixId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_RateChangeMatrixScenario] WHERE [RateChangeMatrixId] = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //ASC 825 Discount Rates
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ASC825DiscountRate] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825DiscountRate] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_ASC825DiscountRate] WHERE id = {3};");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Prepayment Details
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_PrepaymentDetails] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_PrepaymentDetails] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_PrepaymentDetails] WHERE id = {3};");
            }

            //Prepayment Details Scenario Types
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_PrepaymentDetailsScenarioType] ");
            sqlQuery.AppendLine("            ([PrepaymentDetailsId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_PrepaymentDetailsScenarioType] WHERE [PrepaymentDetailsId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_PrepaymentDetailsScenarioType] WHERE [PrepaymentDetailsId] = {3}; ");
            }
            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Investment Portfolio Valuation
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InvestmentPortfolioValuation] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InvestmentPortfolioValuation] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InvestmentPortfolioValuation] WHERE id = {3};");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Investment Portfolio Valuation Scenarios
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InvestmentPortfolioValuationScenario] ");
            sqlQuery.AppendLine("            ([InvestmentPortfolioValuationId] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[DDWName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[DDWName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InvestmentPortfolioValuationScenario] WHERE [InvestmentPortfolioValuationId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_InvestmentPortfolioValuationScenario] WHERE [InvestmentPortfolioValuationId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Investment Detail
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InvestmentDetail] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InvestmentDetail] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InvestmentDetail] WHERE id = {3};");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Investment Error
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_InvestmentError] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_InvestmentError] WHERE id = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" 		   FROM [{2}].[dbo].[ATLAS_InvestmentError] WHERE id = {3};");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Historical Balance Sheet NII
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_HistoricalBalanceSheetNIIs] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[BalanceSheetAssetViewOption] ");
            sqlQuery.AppendLine("            ,[BalanceSheetLiabilityViewOption]");
            sqlQuery.AppendLine("            ,[NIIAssetViewOption] ");
            sqlQuery.AppendLine("            ,[NIILiabilityViewOption] ");
            sqlQuery.AppendLine("            ,[ChangeAttribution] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[TaxEquivalent]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 			{1} ");
            sqlQuery.AppendLine("            ,[BalanceSheetAssetViewOption] ");
            sqlQuery.AppendLine("            ,[BalanceSheetLiabilityViewOption] ");
            sqlQuery.AppendLine("            ,[NIIAssetViewOption] ");
            sqlQuery.AppendLine("            ,[NIILiabilityViewOption] ");
            sqlQuery.AppendLine("            ,[ChangeAttribution] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[TaxEquivalent]  ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_HistoricalBalanceSheetNIIs] WHERE [id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_HistoricalBalanceSheetNIIs] WHERE [id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Historical Balance Sheet NII Scenarios
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_HistoricalBalanceSheetNIIScenario] ");
            sqlQuery.AppendLine("            ([HistoricalBalanceSheetNIIId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]");
            sqlQuery.AppendLine("            ,[CompareToThis] ");
            sqlQuery.AppendLine("            ,[Priority]) ");

            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[CompareToThis] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_HistoricalBalanceSheetNIIScenario] WHERE [HistoricalBalanceSheetNIIId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_HistoricalBalanceSheetNIIScenario] WHERE [HistoricalBalanceSheetNIIId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Historical Balance Sheet NII Simulations
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_HistoricalBalanceSheetNIISimulation] ");
            sqlQuery.AppendLine("            ([HistoricalBalanceSheetNIIId] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[DateOffset] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[DateOffset] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_HistoricalBalanceSheetNIISimulation] WHERE [HistoricalBalanceSheetNIIId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_HistoricalBalanceSheetNIISimulation] WHERE [HistoricalBalanceSheetNIIId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Historical Balance Sheet Top Footnotes
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_HistoricalBalanceSheetNIITopFootNotes] ");
            sqlQuery.AppendLine("            ([HistoricalBalanceSheetNIIId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Text]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Text] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_HistoricalBalanceSheetNIITopFootNotes] WHERE [HistoricalBalanceSheetNIIId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_HistoricalBalanceSheetNIITopFootNotes] WHERE [HistoricalBalanceSheetNIIId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Historical Balance Sheet Bottom Footnotes
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_HistoricalBalanceSheetNIIBottomFootNotes] ");
            sqlQuery.AppendLine("            ([HistoricalBalanceSheetNIIId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Text]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Text] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_HistoricalBalanceSheetNIIBottomFootNotes] WHERE [HistoricalBalanceSheetNIIId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_HistoricalBalanceSheetNIIBottomFootNotes] WHERE [HistoricalBalanceSheetNIIId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            ////Sim Compare Simulation Types
            //sqlQuery.Clear();
            //sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SimCompareSimulationType] ");
            //sqlQuery.AppendLine("            ([Side] ");
            //sqlQuery.AppendLine("            ,[SimCompareId] ");
            //sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            //sqlQuery.AppendLine("            ,[Priority]) ");
            //sqlQuery.AppendLine(" SELECT ");
            //sqlQuery.AppendLine("            [Side] ");
            //sqlQuery.AppendLine("            ,{1} ");
            //sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            //sqlQuery.AppendLine("            ,[Priority] ");
            //if (sourceReportId > 0)
            //{
            //    sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SimCompareSimulationType] WHERE [SimCompareId] = " + sourceReportId + " ");
            //}
            //else
            //{
            //    sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SimCompareSimulationType] WHERE [SimCompareId] = {3}; ");
            //}

            //Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Summary Of Results
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SummaryOfResults] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[Ovrd] ");
            sqlQuery.AppendLine("            ,[nmdAvgLifeAssumption] ");
            sqlQuery.AppendLine("            ,[ExtraText]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[Ovrd] ");
            sqlQuery.AppendLine("            ,[nmdAvgLifeAssumption] ");
            sqlQuery.AppendLine("            ,[ExtraText] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SummaryOfResults] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SummaryOfResults] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Marginal Cost Of Funds
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_MarginalCostOfFunds] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[IsReduction] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[AccountName] ");
            sqlQuery.AppendLine("            ,[Rate] ");
            sqlQuery.AppendLine("            ,[Balance] ");
            sqlQuery.AppendLine("            ,[Runoffs] ");
            sqlQuery.AppendLine("            ,[RateChanges]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[IsReduction] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[AccountName] ");
            sqlQuery.AppendLine("            ,[Rate] ");
            sqlQuery.AppendLine("            ,[Balance] ");
            sqlQuery.AppendLine("            ,[Runoffs] ");
            sqlQuery.AppendLine("            ,[RateChanges] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_MarginalCostOfFunds] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_MarginalCostOfFunds] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Lookback Report
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_LookbackReport] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_LookbackReport] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_LookbackReport] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //ASC825Worksheet
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ASC825Worksheet] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825Worksheet] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ASC825Worksheet] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //ASC825AssumptionMethods
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_ASC825AssumptionMethods] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[Enhanced]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[Enhanced] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_ASC825AssumptionMethods] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_ASC825AssumptionMethods] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //AssumptionMethods
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_AssumptionMethods] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_AssumptionMethods] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_AssumptionMethods] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //CapitalAnalysis
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CapitalAnalysis] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[CurrentCapitalDateOffset] ");
            sqlQuery.AppendLine("            ,[LeftCalculatedDefaults] ");
            sqlQuery.AppendLine("            ,[LeftRatio] ");
            sqlQuery.AppendLine("            ,[LeftPolicyType] ");
            sqlQuery.AppendLine("            ,[RightCalculatedDefaults] ");
            sqlQuery.AppendLine("            ,[RightRatio] ");
            sqlQuery.AppendLine("            ,[RightPolicyType] ");
            sqlQuery.AppendLine("            ,[Periods] ");
            sqlQuery.AppendLine("            ,[PeriodType] ");
            sqlQuery.AppendLine("            ,[RetainedOffset] ");
            sqlQuery.AppendLine("            ,[RatioType] ");
            sqlQuery.AppendLine("            ,[OverrideRetainedEarnings] ");
            sqlQuery.AppendLine("            ,[ReatinedEarnings] ");
            sqlQuery.AppendLine("            ,[PreTaxOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[CurrentCapitalDateOffset] ");
            sqlQuery.AppendLine("            ,[LeftCalculatedDefaults] ");
            sqlQuery.AppendLine("            ,[LeftRatio] ");
            sqlQuery.AppendLine("            ,[LeftPolicyType] ");
            sqlQuery.AppendLine("            ,[RightCalculatedDefaults] ");
            sqlQuery.AppendLine("            ,[RightRatio] ");
            sqlQuery.AppendLine("            ,[RightPolicyType] ");
            sqlQuery.AppendLine("            ,[Periods] ");
            sqlQuery.AppendLine("            ,[PeriodType] ");
            sqlQuery.AppendLine("            ,[RetainedOffset] ");
            sqlQuery.AppendLine("            ,[RatioType] ");
            sqlQuery.AppendLine("            ,[OverrideRetainedEarnings] ");
            sqlQuery.AppendLine("            ,[ReatinedEarnings] ");
            sqlQuery.AppendLine("            ,[PreTaxOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CapitalAnalysis] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CapitalAnalysis] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //CapitalAnalysisHistoricalBufferOffSets
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CapitalAnalysisHistoricalBufferOffSets] ");
            sqlQuery.AppendLine("            ([CapitalAnalysisId] ");
            sqlQuery.AppendLine("            ,[OffSet] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[OffSet] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CapitalAnalysisHistoricalBufferOffSets] WHERE [CapitalAnalysisId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CapitalAnalysisHistoricalBufferOffSets] WHERE [CapitalAnalysisId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //CapitalAnalysisPreTaxOffSets
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CapitalAnalysisPreTaxOffSets] ");
            sqlQuery.AppendLine("            ([CapitalAnalysisId] ");
            sqlQuery.AppendLine("            ,[OffSet] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[OffSet] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CapitalAnalysisPreTaxOffSets] WHERE [CapitalAnalysisId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CapitalAnalysisPreTaxOffSets] WHERE [CapitalAnalysisId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //CapitalAnalysisPreTaxScenario
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_CapitalAnalysisPreTaxScenario] ");
            sqlQuery.AppendLine("            ([CapitalAnalysisId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[ScenarioName] ");
            sqlQuery.AppendLine("            ,[DisplayScenarioName]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[ScenarioName] ");
            sqlQuery.AppendLine("            ,[DisplayScenarioName] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_CapitalAnalysisPreTaxScenario] WHERE [CapitalAnalysisId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_CapitalAnalysisPreTaxScenario] WHERE [CapitalAnalysisId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //DetailedSimulationAssumption
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_DetailedSimulationAssumption] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_DetailedSimulationAssumption] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_DetailedSimulationAssumption] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Document
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Document] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[HtmlSource]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[HtmlSource] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Document] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Document] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Eve
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Eves] ");
            sqlQuery.AppendLine("          ([Id] ");
            sqlQuery.AppendLine("          ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("          ,[SimulationTypeId] ");
            sqlQuery.AppendLine("          ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("          ,[ViewOption] ");
            sqlQuery.AppendLine("          ,[PolicyLimits] ");
            sqlQuery.AppendLine("          ,[RiskSummaryTable]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("           {1} ");
            sqlQuery.AppendLine("          ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("          ,[SimulationTypeId] ");
            sqlQuery.AppendLine("          ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("          ,[ViewOption] ");
            sqlQuery.AppendLine("          ,[PolicyLimits] ");
            sqlQuery.AppendLine("          ,[RiskSummaryTable] ");

            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Eves] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Eves] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //EveScenarioTypes
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_EveScenarioType] ");
            sqlQuery.AppendLine("            ([EveId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_EveScenarioType] WHERE [EveId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_EveScenarioType] WHERE [EveId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //FundingMatrix
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_FundingMatrices] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[LeftInstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[LeftSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[LeftScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[RightInstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[RightScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[TaxEqivalentYields]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [LeftInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [LeftInstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[LeftSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[LeftScenarioTypeId] ");
            sqlQuery.AppendLine("            ,CASE WHEN [RightInstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [RightInstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightSimulationTypeId] ");
            sqlQuery.AppendLine("            ,[RightScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[TaxEqivalentYields] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_FundingMatrices] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_FundingMatrices] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //LiabilityPricingAnalysis
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_LiabilityPricingAnalysis] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[ComparativeDateOffset]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[ComparativeDateOffset] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_LiabilityPricingAnalysis] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_LiabilityPricingAnalysis] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Non MAturity Deposit Rate Footnotes



            //LiquidityProjection
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_LiquidityProjection] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[PriorDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[BasicSurplusId] ");
            sqlQuery.AppendLine("            ,[BrokeredRecip] ");
            sqlQuery.AppendLine("            ,[BrokeredDep] ");
            sqlQuery.AppendLine("            ,[NationalDep] ");
            sqlQuery.AppendLine("            ,[SecuredRetail] ");
            sqlQuery.AppendLine("            ,[PublicDep] ");
            sqlQuery.AppendLine("            ,[ReportType] ");
            sqlQuery.AppendLine("            ,[NonPledgeDefinedGroup] ");
            sqlQuery.AppendLine("            ,[AssetDefinedGroup] ");
            sqlQuery.AppendLine("            ,[LiabilityDefinedGroup]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[PriorDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[BasicSurplusId] ");
            sqlQuery.AppendLine("            ,[BrokeredRecip] ");
            sqlQuery.AppendLine("            ,[BrokeredDep] ");
            sqlQuery.AppendLine("            ,[NationalDep] ");
            sqlQuery.AppendLine("            ,[SecuredRetail] ");
            sqlQuery.AppendLine("            ,[PublicDep] ");
            sqlQuery.AppendLine("            ,[ReportType] ");
            sqlQuery.AppendLine("            ,[NonPledgeDefinedGroup] ");
            sqlQuery.AppendLine("            ,[AssetDefinedGroup] ");
            sqlQuery.AppendLine("            ,[LiabilityDefinedGroup] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_LiquidityProjection] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_LiquidityProjection] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //NIIRecon
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_NIIRecon] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[TimePeriod] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[TaxEquiv]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[TimePeriod] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[TaxEquiv] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_NIIRecon] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_NIIRecon] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //NII Recon Scenarios
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_NIIReconScenarioType] ");
            sqlQuery.AppendLine("            ([NIIReconId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_NIIReconScenarioType] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_NIIReconScenarioType] WHERE [NIIReconId] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //TimeDepositMigration
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_TimeDepositMigrations] ");
            sqlQuery.AppendLine("            ([Id] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[CD] ");
            sqlQuery.AppendLine("            ,[JumboCD] ");
            sqlQuery.AppendLine("            ,[PublicCD] ");
            sqlQuery.AppendLine("            ,[PublicJumbo] ");
            sqlQuery.AppendLine("            ,[NationalCD] ");
            sqlQuery.AppendLine("            ,[OnlineCD] ");
            sqlQuery.AppendLine("            ,[OnlineJumbo] ");
            sqlQuery.AppendLine("            ,[BrokeredCD] ");
            sqlQuery.AppendLine("            ,[BrokeredOneWayCD] ");
            sqlQuery.AppendLine("            ,[BrokeredReciprocalCD] ");
            sqlQuery.AppendLine("            ,[Regular] ");
            sqlQuery.AppendLine("            ,[Special]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("            {1} ");
            sqlQuery.AppendLine("            ,CASE WHEN [InstitutionDatabaseName] = '{2}' THEN '{0}' ELSE [InstitutionDatabaseName] END  ");
            sqlQuery.AppendLine("            ,[LeftAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[RightAsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[CD] ");
            sqlQuery.AppendLine("            ,[JumboCD] ");
            sqlQuery.AppendLine("            ,[PublicCD] ");
            sqlQuery.AppendLine("            ,[PublicJumbo] ");
            sqlQuery.AppendLine("            ,[NationalCD] ");
            sqlQuery.AppendLine("            ,[OnlineCD] ");
            sqlQuery.AppendLine("            ,[OnlineJumbo] ");
            sqlQuery.AppendLine("            ,[BrokeredCD] ");
            sqlQuery.AppendLine("            ,[BrokeredOneWayCD] ");
            sqlQuery.AppendLine("            ,[BrokeredReciprocalCD] ");
            sqlQuery.AppendLine("            ,[Regular] ");
            sqlQuery.AppendLine("            ,[Special] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_TimeDepositMigrations] WHERE [Id] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_TimeDepositMigrations] WHERE [Id] = {3}; ");
            }

            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));


            //Foot Notes 
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_Footnote] ");
            sqlQuery.AppendLine("            ([ReportId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Text]) ");
            sqlQuery.AppendLine(" SELECT    {1}, [Priority] ");
            sqlQuery.AppendLine("            ,[Text] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Footnote] WHERE [reportId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_Footnote] WHERE [reportId] = {3}; ");
            }


            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            //Split Foot notes
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [{0}].[dbo].[ATLAS_SplitFootnote] ");
            sqlQuery.AppendLine("            ([ReportId] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Side] ");
            sqlQuery.AppendLine("            ,[Text]) ");
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	{1} ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[Side] ");
            sqlQuery.AppendLine("            ,[Text] ");
            if (sourceReportId > 0)
            {
                sqlQuery.AppendLine(" 		   FROM [DDW_Atlas_Templates].[dbo].[ATLAS_SplitFootnote] WHERE [reportId] = " + sourceReportId + " ");
            }
            else
            {
                sqlQuery.AppendLine(" FROM [{2}].[dbo].[ATLAS_SplitFootnote] WHERE [reportId] = {3}; ");
            }


            Utilities.ExecuteCommand(ddwConStr, String.Format(sqlQuery.ToString(), bankDb, newId, templateDb, reportId));

            return "";
        }


    }



}






