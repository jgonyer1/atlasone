﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

using System.Data;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace Atlas.Web.Services
{
    public class PhantomService
    {
        public void RunPhantomCmd()
        {
            string url = "";
               List<HttpCookie> actualCookies = new List<HttpCookie>();
              foreach (string key in HttpContext.Current.Request.Cookies.AllKeys)
              {
                  //ASPNETFORMSAUTH cookie has null domain. not sure what other ones do, but we want that to come through, at least for development.
                  if (HttpContext.Current.Request.Cookies[key].Domain == null)
                  {
                      HttpContext.Current.Request.Cookies[key].Domain = HttpContext.Current.Request.Url.Host;
                  }

                  //phantom will only use the cookies that have the same domain anyway
                  if (HttpContext.Current.Request.Cookies[key].Domain == HttpContext.Current.Request.Url.Host)
                  {
                      actualCookies.Add(HttpContext.Current.Request.Cookies[key]);
                  }

                  if (HttpContext.Current.Request.Cookies[key].Name == "exportUrl")
                  {
                      url = HttpContext.Current.Request.Cookies[key].Value.ToString();
                  }
              }

              string jsonCookies = new JavaScriptSerializer().Serialize(actualCookies);
              string cookieFileName = "cookies_" + Utilities.GetUserNameNoEmail() + ".txt";
              string cookieFilePath = "C:\\phantom\\bin\\" + cookieFileName;
              System.IO.StreamWriter file = new System.IO.StreamWriter(cookieFilePath);
              file.Write(jsonCookies);
              file.Close();
            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //  System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //  startInfo.FileName = "cmd.exe";
            //  startInfo.WorkingDirectory = @"C:\phantomjs\phantomjs-2.1.1-windows\bin";
            //  startInfo.Arguments = "/K phantomjs rasterize_copyForMulti.js \"" + "http://localhost:11288/#/reportview/1" + "\" test.pdf 1000px*1200px 1 " + cookieFileName;


            //  startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
            //  process.StartInfo = startInfo;
            //  process.EnableRaisingEvents = true;
            //  process.Exited += new EventHandler((sender, e) => CleanUpPhantomProcess(sender, e, cookieFilePath));
            //  process.Start();
            //  process.WaitForExit(500);
        }

        private static void CleanUpPhantomProcess(object sender, System.EventArgs e, string cookiesFileName)
        {
            File.Delete(cookiesFileName);
        }
    }
}
