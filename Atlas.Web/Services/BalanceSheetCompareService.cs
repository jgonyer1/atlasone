﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using System.Text;
using System.Data;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class BalanceSheetCompareService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        const string AtlasTemplates = "DDW_Atlas_Templates";

        private class LineItem
        {
            public string CatName { get; set; }
            public int CatType { get; set; }
            public bool IsAsset { get; set; }
            public double LeftBal { get; set; }
            public double RightBal { get; set; }

            public double LeftRate { get; set; }
            public double RightRate { get; set; }

            public double BalDiff { get { return LeftBal - RightBal; } }
            public double RateDiff { get { return LeftRate - RightRate; } }

            public LineItem()
            {
                CatName = "";
                CatType = 0;

                LeftBal = 0;
                RightBal = 0;

                LeftRate = 0;
                RightRate = 0;

                IsAsset = false;
            }
        }

        public object BalanceSheetCompareViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            GlobalController gc = new GlobalController();
            var balanceSheetCompare = gctx.BalanceSheetCompares.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            if (balanceSheetCompare == null)
                throw new Exception("BalanceSheetCompare Not Found");




            var rightInstService = new InstitutionService(balanceSheetCompare.RightInstitutionDatabaseName);


            //Left Sutff(Top)
            var leftInstService = new InstitutionService(balanceSheetCompare.LeftInstitutionDatabaseName);
            var leftInfo = leftInstService.GetInformation(rep.Package.AsOfDate, balanceSheetCompare.LeftAsOfDateOffset);
            DataTable leftAods = gc.AsOfDates(balanceSheetCompare.LeftInstitutionDatabaseName, false, 8, false);
            DateTime start_endDate = DateTime.Now;
            if(leftAods.Rows.Count > balanceSheetCompare.LeftAsOfDateOffset && rep.Defaults)
            {
                //start_endDate = DateTime.Parse(leftAods.Rows[balanceSheetCompare.LeftAsOfDateOffset]["asOfDateDescript"].ToString());
                //balanceSheetCompare.LeftStartDate = start_endDate;
                //balanceSheetCompare.RightStartDate = start_endDate;
            }
            List<Simulation> leftSims = leftInfo.Simulations.ToList();
            List<Scenario> curScens;
            string leftAod = leftInfo.AsOfDate.ToString("MM/dd/yyyy");
            bool foundBaseScen = false;
            Simulation curSim;
            Scenario curScen;
            Dictionary<string, DataTable> leftPolicyOffsets = gc.PolicyOffsets(balanceSheetCompare.LeftInstitutionDatabaseName);
            int simTypeToUse = -1;
            SimulationType leftSimulationTypeToUse;
            int scenTypeToUse = -1;
            ScenarioType leftScenarioTypeToUse;
            if (balanceSheetCompare.LeftOverrideSimulationId)
            {
                simTypeToUse = balanceSheetCompare.LeftSimulationTypeId;
            }
            else
            {
                simTypeToUse = Convert.ToInt32(leftPolicyOffsets[leftAod].Rows[0]["niiId"].ToString());
            }
            leftSimulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeToUse).FirstOrDefault();
            if (balanceSheetCompare.LeftOverrideScenarioId)
            {
                scenTypeToUse = balanceSheetCompare.LeftScenarioTypeId;
            }
            else
            {
                if (leftPolicyOffsets[leftAod].Rows[0]["niiId"].ToString() == leftPolicyOffsets[leftAod].Rows[0]["bsSimulation"].ToString())
                {
                    scenTypeToUse = Convert.ToInt32(leftPolicyOffsets[leftAod].Rows[0]["bsScenario"].ToString());
                }
                else
                {
                    //look for base scenario
                    for (int i = 0; i < leftSims.Count && !foundBaseScen; i++)
                    {
                        curSim = leftSims[i];
                        if (curSim.SimulationTypeId == simTypeToUse)
                        {
                            curScens = curSim.Scenarios.ToList();
                            for (int j = 0; j < curScens.Count && !foundBaseScen; j++)
                            {
                                curScen = curScens[j];
                                if (j == 0)
                                {

                                    //set the scenario to use to the first scenario we find in the simulation. If Base gets found, it will be set to that
                                    scenTypeToUse = curScen.ScenarioTypeId;
                                }
                                if (curScen.ScenarioTypeId == 1)
                                {
                                    scenTypeToUse = 1;
                                    foundBaseScen = true;
                                }
                            }
                        }
                    }
                }
            }

            //Set left side stuff
            balanceSheetCompare.LeftSimulationTypeId = simTypeToUse;
            balanceSheetCompare.LeftScenarioTypeId = scenTypeToUse;


            DataTable rightDt = new DataTable();
            var rightInfo = rightInstService.GetInformation(rep.Package.AsOfDate, balanceSheetCompare.RightAsOfDateOffset);
            List<Simulation> rightSims = rightInfo.Simulations.ToList();
            //List<Scenario> curScens;
            string rightAod = rightInfo.AsOfDate.ToString("MM/dd/yyyy");
            foundBaseScen = false;
            //Simulation curSim;
            //Scenario curScen;
            Dictionary<string, DataTable> rightPolicyOffsets = gc.PolicyOffsets(balanceSheetCompare.LeftInstitutionDatabaseName);
            simTypeToUse = -1;
            SimulationType rightSimulationTypeToUse;
            scenTypeToUse = -1;
            if (balanceSheetCompare.RightOverrideSimulationId)
            {
                simTypeToUse = balanceSheetCompare.RightSimulationTypeId;
            }
            else
            {
                simTypeToUse = Convert.ToInt32(rightPolicyOffsets[rightAod].Rows[0]["niiId"].ToString());
            }
            rightSimulationTypeToUse = gctx.SimulationTypes.Where(s => s.Id == simTypeToUse).FirstOrDefault();
            if (balanceSheetCompare.RightOverrideScenarioId)
            {
                scenTypeToUse = balanceSheetCompare.RightScenarioTypeId;
            }
            else
            {
                if (rightPolicyOffsets[rightAod].Rows[0]["niiId"].ToString() == rightPolicyOffsets[leftAod].Rows[0]["bsSimulation"].ToString())
                {
                    scenTypeToUse = Convert.ToInt32(rightPolicyOffsets[rightAod].Rows[0]["bsScenario"].ToString());
                }
                else
                {
                    //look for base scenario
                    for (int i = 0; i < rightSims.Count && !foundBaseScen; i++)
                    {
                        curSim = rightSims[i];
                        if (curSim.SimulationTypeId == simTypeToUse)
                        {
                            curScens = curSim.Scenarios.ToList();
                            for (int j = 0; j < curScens.Count && !foundBaseScen; j++)
                            {
                                curScen = curScens[j];
                                if (j == 0)
                                {

                                    //set the scenario to use to the first scenario we find in the simulation. If Base gets found, it will be set to that
                                    scenTypeToUse = curScen.ScenarioTypeId;
                                }
                                if (curScen.ScenarioTypeId == 1)
                                {
                                    scenTypeToUse = 1;
                                    foundBaseScen = true;
                                }
                            }
                        }
                    }
                }
            }

            //Set right side stuff
            balanceSheetCompare.RightSimulationTypeId = simTypeToUse;
            balanceSheetCompare.RightScenarioTypeId = scenTypeToUse;



            var lineItems = LineItems(balanceSheetCompare, leftInstService, rightInstService, rep);
            return new
            {
                leftStartDate = balanceSheetCompare.LeftStartDate,
                leftEndDate = balanceSheetCompare.LeftEndDate,
                rightStartDate = balanceSheetCompare.RightStartDate,
                rightEndDate = balanceSheetCompare.RightEndDate,
                ShowBalances = balanceSheetCompare.Balances,
                ShowRates = balanceSheetCompare.Rates,
                LineItems = lineItems,
                warnings = warnings,
                errors = errors
            };

        }

        private int ParseViewOption(string viewOption)
        {
            return (viewOption.Contains("dg_")) ? 4 : int.Parse(viewOption);
        }
        private int ParseDefinedGrouping(string viewOption)
        {
            return (viewOption.Contains("dg_")) ? int.Parse(viewOption.Substring(3)) : -1;
        }

        private void BuildNumPeriodsTempTable(BalanceSheetCompare balanceSheetCompare, StringBuilder sqlQuery)
        {
            //sqlQuery.AppendLine("DROP TABLE #Periods");
            //sqlQuery.AppendLine("GO");
            //sqlQuery.AppendLine("");
            sqlQuery.AppendLine("CREATE TABLE #Periods (");
            sqlQuery.AppendLine("	Month datetime,");
            sqlQuery.AppendLine("	NumDays int");
            sqlQuery.AppendLine(")");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("INSERT INTO #Periods (Month, NumDays)");
            sqlQuery.AppendLine("VALUES");

            DateTime startDate = balanceSheetCompare.LeftStartDate;
            if (startDate > balanceSheetCompare.RightStartDate)
                startDate = balanceSheetCompare.RightStartDate;
            DateTime endDate = balanceSheetCompare.LeftEndDate;
            if (endDate < balanceSheetCompare.RightEndDate)
                endDate = balanceSheetCompare.RightEndDate;

            while (startDate <= endDate)
            {
                sqlQuery.Append("	('");
                sqlQuery.Append(startDate.Date);
                sqlQuery.Append("', ");
                startDate = startDate.AddMonths(1);
                sqlQuery.Append(startDate.AddDays(-1).Day);
                sqlQuery.AppendLine((startDate <= endDate) ? ")," : ")");
            }
            sqlQuery.AppendLine("");
        }

        private DataTable PullCategoriesActualsProjections(BalanceSheetCompare balanceSheetCompare,
            InstitutionService leftInstService, InstitutionService rightInstService,
            Object leftSimScen, Object rightSimScen)
        {
            string[] balFields, rateFields;
            string actRateType = "Total_End_Rate";
            string projRateType = "";
            string actBalType = "";
            string projBalType = "";
            if (balanceSheetCompare.TaxEqivalentYields)
            {
                if (balanceSheetCompare.BalanceType == "1")//AVG
                {
                    balFields = new string[] { "Avg_Bal", "AveBal" };
                    rateFields = new string[] { "Total_Avg_TEYLD", "TAveTEYLD" };

                    //actRateType = "Total_Avg_TEYLD";
                    //projRateType = "TAveTEYLD";
                    //actBalType = "Avg_Bal";
                    //projBalType = "AveBal";
                    
                }
                else//EOM
                {
                    balFields = new string[] { "End_Bal", "EndBal" };
                    rateFields = new string[] { "Total_End_TEYLD", "TEndTEYLD" };

                    //actRateType = "Total_End_TEYLD";
                    //projRateType = "TEndTEYLD";
                    //actBalType = "End_Bal";
                    //projBalType = "EndBal";
                }
            }
            else
            {
                if (balanceSheetCompare.BalanceType == "1")//AVG
                {
                    balFields = new string[] { "Avg_Bal", "AveBal" };
                    rateFields = new string[] { "Total_Avg_Rate", "TotalYield" };

                    //actRateType = "Total_Avg_Rate";
                    //projRateType = "TotalYield";
                    //actBalType = "Avg_Bal";
                    //projBalType = "AveBal";
                }
                else//EOM
                {
                    balFields = new string[] { "End_Bal", "EndBal" };
                    rateFields = new string[] { "Total_End_Rate", "TotalYieldEnd" };

                    //actRateType = "Total_End_Rate";
                    //projRateType = "TotalYieldEnd";
                    //actBalType = "End_Bal";
                    //projBalType = "EndBal";
                }
            }
            //string rateType = (balanceSheetCompare.TaxEqivalentYields) ? "TEYLD" : "Rate";

            //switch (balanceSheetCompare.BalanceType)
            //{
            //    case "1": //AVG
            //        balFields = new string[] { "Avg_Bal", "AveBal" };
            //        rateFields = new string[] { "Avg_" + rateType, "Ave" + rateType };
            //        break;
            //    default: //EOM
            //        balFields = new string[] { "End_Bal", "EndBal" };
            //        rateFields = new string[] { "End_" + rateType, "End" + rateType };
            //        break;
            //}

            int viewOption = ParseViewOption(balanceSheetCompare.ViewOption);

            StringBuilder sqlQuery = new StringBuilder();
            BuildNumPeriodsTempTable(balanceSheetCompare, sqlQuery);
            sqlQuery.AppendLine("DECLARE @LeftNumDaysTotal int;");
            sqlQuery.AppendLine("DECLARE @RightNumDaysTotal int;");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("SELECT @LeftNumDaysTotal = SUM(NumDays) FROM #Periods WHERE Month >= '" + balanceSheetCompare.LeftStartDate + "' AND Month <= '" + balanceSheetCompare.LeftEndDate + "';");
            sqlQuery.AppendLine("SELECT @RightNumDaysTotal = SUM(NumDays) FROM #Periods WHERE Month >= '" + balanceSheetCompare.RightStartDate + "' AND Month <= '" + balanceSheetCompare.RightEndDate + "';");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	B.IsAsset,");
            
            switch (viewOption)
            {
                case 4:
                    sqlQuery.AppendLine("	E.DefinedGroupingName AS Name,");
                    sqlQuery.AppendLine("	E.DefinedGroupingOrder,");
                    break;
                case 3:
                    sqlQuery.AppendLine("	B.Acc_Type,");
                    sqlQuery.AppendLine("	C.AccountName AS Name,");
                    sqlQuery.AppendLine("	C.AccountOrder,");
                    break;
                case 2:
                    sqlQuery.AppendLine("	B.Acc_Type,");
                    sqlQuery.AppendLine("	C.AccountName,");
                    sqlQuery.AppendLine("	D.ClassificationName AS Name,");
                    sqlQuery.AppendLine("	C.AccountOrder,");
                    sqlQuery.AppendLine("	D.ClassificationOrder,");
                    break;
                default: //0, 1
                    sqlQuery.AppendLine("	B.Acc_Type,");
                    sqlQuery.AppendLine("	B.RbcTier,");
                    sqlQuery.AppendLine("	B.Cat_Type,");
                    sqlQuery.AppendLine("	B.Code,");
                    sqlQuery.AppendLine("	B.Name,");
                    sqlQuery.AppendLine("	B.Sequence,");
                    break;
            }
            if (viewOption > 1)
            {
                sqlQuery.AppendLine("	SUM(A.LeftBalance) AS LeftBalance,");
                sqlQuery.AppendLine("	SUM(A.RightBalance) AS RightBalance,");
                sqlQuery.AppendLine("	--SUM(A.LeftBalance) - SUM(A.RightBalance) AS BalanceDifference,");
                sqlQuery.AppendLine("	CASE WHEN SUM(A.LeftBalance) = 0 THEN 0 ELSE SUM(A.LeftBalance * A.LeftRate) / SUM(A.LeftBalance) END AS LeftRate,");
                sqlQuery.AppendLine("	CASE WHEN SUM(A.RightBalance) = 0 THEN 0 ELSE SUM(A.RightBalance * A.RightRate) / SUM(A.RightBalance) END AS RightRate,");
                sqlQuery.AppendLine("	--CASE WHEN SUM(A.LeftBalance) = 0 THEN 0 ELSE SUM(A.LeftBalance * A.LeftRate) / SUM(A.LeftBalance) END");
                sqlQuery.AppendLine("	--	- CASE WHEN SUM(A.RightBalance) = 0 THEN 0 ELSE SUM(RightBalance * A.RightRate) / SUM(A.RightBalance) END AS RateDifference,");
                sqlQuery.AppendLine("	SUM(A.LeftNumDays) AS LeftNumDays,");
                sqlQuery.AppendLine("	SUM(A.RightNumDays) AS RightNumDays,");
            }
            else
            {
                sqlQuery.AppendLine("	A.LeftBalance,");
                sqlQuery.AppendLine("	A.RightBalance,");
                sqlQuery.AppendLine("	--A.LeftBalance - A.RightBalance AS BalanceDifference,");
                sqlQuery.AppendLine("	A.LeftRate,");
                sqlQuery.AppendLine("	A.RightRate,");
                sqlQuery.AppendLine("	--A.LeftRate - A.RightRate AS RateDifference,");
                sqlQuery.AppendLine("	A.LeftNumDays,");
                sqlQuery.AppendLine("	A.RightNumDays,");
            }
            sqlQuery.AppendLine("	@LeftNumDaysTotal AS LeftNumDaysTotal,");
            sqlQuery.AppendLine("	@RightNumDaysTotal AS RightNumDaysTotal");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	-- Left/Right Actuals/Projections");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		ISNULL(LeftInst.Code, RightInst.Code) AS Code,");
            sqlQuery.AppendLine("		CASE WHEN @LeftNumDaysTotal = 0 THEN 0 ELSE SUM(LeftInst.Balance * LeftInst.NumDays) / @LeftNumDaysTotal END AS LeftBalance,");
            sqlQuery.AppendLine("		CASE WHEN @RightNumDaysTotal = 0 THEN 0 ELSE SUM(RightInst.Balance * RightInst.NumDays) / @RightNumDaysTotal END AS RightBalance,");
            sqlQuery.AppendLine("		CASE WHEN SUM(LeftInst.Balance) = 0 THEN 0 ELSE SUM(LeftInst.Balance * LeftInst.Rate) / SUM(LeftInst.Balance) END AS LeftRate,");
            sqlQuery.AppendLine("		CASE WHEN SUM(RightInst.Balance) = 0 THEN 0 ELSE SUM(RightInst.Balance * RightInst.Rate) / SUM(RightInst.Balance) END AS RightRate,");
            sqlQuery.AppendLine("		SUM(LeftInst.NumDays) AS LeftNumDays,");
            sqlQuery.AppendLine("		SUM(RightInst.NumDays) AS RightNumDays");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		-- Left Actuals/Projections/NumDays");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			A.Code,");
            sqlQuery.AppendLine("			A.Month,");
            sqlQuery.AppendLine("			A.Balance,");
            sqlQuery.AppendLine("			A.Rate,");
            sqlQuery.AppendLine("			B.NumDays");
            sqlQuery.AppendLine("		FROM (");
            sqlQuery.AppendLine("			-- Left Actuals");
            sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				Code,");
            sqlQuery.AppendLine("				Month,");
            sqlQuery.AppendLine("				" + balFields[0] + " AS Balance,");
            sqlQuery.AppendLine("				" + rateFields[0] + " AS Rate");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("			WHERE SimulationId = " + leftSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND Month >= '" + balanceSheetCompare.LeftStartDate + "'");
            sqlQuery.AppendLine("				AND Month <= '" + balanceSheetCompare.LeftEndDate + "'");
            sqlQuery.AppendLine("			GROUP BY SimulationId, Code, Month, " + balFields[0] + ", " + rateFields[0]);
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("			-- Left Projections");
            sqlQuery.AppendLine("			UNION ALL");
            sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				" + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Code,");
            sqlQuery.AppendLine("				" + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month,");
            sqlQuery.AppendLine("				" + balFields[1] + " AS Balance,");
            sqlQuery.AppendLine("				" + rateFields[1] + " AS Rate");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1");
            sqlQuery.AppendLine("           INNER JOIN " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13");
            sqlQuery.AppendLine("           ON " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.SimulationId");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.Scenario ");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.code = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.code");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.month = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.month");
            sqlQuery.AppendLine("			WHERE " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId = " + leftSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario = '" + leftSimScen.GetDynamicProperty("scenario") + "'");
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month >= '" + balanceSheetCompare.LeftStartDate + "'");
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month <= '" + balanceSheetCompare.LeftEndDate + "'");
            sqlQuery.AppendLine("			GROUP BY " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId, " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario, " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Code, " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month, " + balFields[1] + ", " + rateFields[1]);
            sqlQuery.AppendLine("		) AS A");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("		-- Left NumDays");
            sqlQuery.AppendLine("		INNER JOIN #Periods AS B");
            sqlQuery.AppendLine("		ON A.Month = B.Month");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("		-- Left Per-Scenario Exclude");
            sqlQuery.AppendLine("		INNER JOIN (");
            sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				Code,");
            sqlQuery.AppendLine("				Exclude");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALS");
            sqlQuery.AppendLine("			WHERE SimulationId = " + leftSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND Scenario = '" + leftSimScen.GetDynamicProperty("scenario") + "'");
            sqlQuery.AppendLine("				AND Exclude <> 1");
            sqlQuery.AppendLine("		) AS C");
            sqlQuery.AppendLine("		ON A.Code = C.Code");
            sqlQuery.AppendLine("	) AS LeftInst");
            sqlQuery.AppendLine("	FULL OUTER JOIN (");
            sqlQuery.AppendLine("		-- Right Actuals/Projections/NumDays");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			A.Code,");
            sqlQuery.AppendLine("			A.Month,");
            sqlQuery.AppendLine("			A.Balance,");
            sqlQuery.AppendLine("			A.Rate,");
            sqlQuery.AppendLine("			B.NumDays");
            sqlQuery.AppendLine("		FROM (");
            sqlQuery.AppendLine("			-- Right Actuals");
            sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				Code,");
            sqlQuery.AppendLine("				Month,");
            sqlQuery.AppendLine("				" + balFields[0] + " AS Balance,");
            sqlQuery.AppendLine("				" + rateFields[0] + " AS Rate");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("			WHERE SimulationId = " + rightSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND Month >= '" + balanceSheetCompare.RightStartDate + "'");
            sqlQuery.AppendLine("				AND Month <= '" + balanceSheetCompare.RightEndDate + "'");
            sqlQuery.AppendLine("			GROUP BY SimulationId, Code, Month, " + balFields[0] + ", " + rateFields[0]);
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("			-- Right Projections");
            sqlQuery.AppendLine("			UNION ALL");
            /*
             sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				" + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Code,");
            sqlQuery.AppendLine("				" + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month,");
            sqlQuery.AppendLine("				" + balFields[1] + " AS Balance,");
            sqlQuery.AppendLine("				" + rateFields[1] + " AS Rate");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1");
            sqlQuery.AppendLine("           INNER JOIN " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13");
            sqlQuery.AppendLine("           ON " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.SimulationId");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.Scenario ");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.code = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.code");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.month = " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP13.month");
            sqlQuery.AppendLine("			WHERE " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId = " + leftSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario = '" + leftSimScen.GetDynamicProperty("scenario") + "'");
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month >= '" + balanceSheetCompare.LeftStartDate + "'");
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month <= '" + balanceSheetCompare.LeftEndDate + "'");
            sqlQuery.AppendLine("			GROUP BY " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId, " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario, " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Code, " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALP1.Month, " + balFields[1] + ", " + rateFields[1]);
             */
            sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				" + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Code,");
            sqlQuery.AppendLine("				" + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Month,");
            sqlQuery.AppendLine("				" + balFields[1] + " AS Balance,");
            sqlQuery.AppendLine("				" + rateFields[1] + " AS Rate");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1");
            sqlQuery.AppendLine("           INNER JOIN " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP13");
            sqlQuery.AppendLine("           ON " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId = " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP13.SimulationId");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario = " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP13.Scenario ");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.code = " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP13.code");
            sqlQuery.AppendLine("           and " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.month = " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP13.month");
            sqlQuery.AppendLine("			WHERE " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId = " + rightSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario = '" + rightSimScen.GetDynamicProperty("scenario") + "'");
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Month >= '" + balanceSheetCompare.RightStartDate + "'");
            sqlQuery.AppendLine("				AND " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Month <= '" + balanceSheetCompare.RightEndDate + "'");
            sqlQuery.AppendLine("			GROUP BY " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.SimulationId, " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Scenario, " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Code, " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALP1.Month, " + balFields[1] + ", " + rateFields[1]);
            sqlQuery.AppendLine("		) AS A");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("		-- Right NumDays");
            sqlQuery.AppendLine("		INNER JOIN #Periods AS B");
            sqlQuery.AppendLine("		ON A.Month = B.Month");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("		-- Right Per-Scenario Exclude");
            sqlQuery.AppendLine("		INNER JOIN (");
            sqlQuery.AppendLine("			SELECT");
            sqlQuery.AppendLine("				Code,");
            sqlQuery.AppendLine("				Exclude");
            sqlQuery.AppendLine("			FROM " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALS");
            sqlQuery.AppendLine("			WHERE SimulationId = " + rightSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("				AND Scenario = '" + rightSimScen.GetDynamicProperty("scenario") + "'");
            sqlQuery.AppendLine("				AND Exclude <> 1");
            sqlQuery.AppendLine("		) AS C");
            sqlQuery.AppendLine("		ON A.Code = C.Code");
            sqlQuery.AppendLine("	) AS RightInst");
            sqlQuery.AppendLine("	ON LeftInst.Code = RightInst.Code AND LeftInst.Month = RightInst.Month");
            sqlQuery.AppendLine("	GROUP BY ISNULL(LeftInst.Code, RightInst.Code)");
            sqlQuery.AppendLine(") AS A");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("-- Left/Right Categories");
            sqlQuery.AppendLine("FULL OUTER JOIN (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		ISNULL(LeftInst.Code, RightInst.Code) AS Code,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.Name, RightInst.Name) AS Name,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.Sequence, RightInst.Sequence) AS Sequence,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.IsAsset, RightInst.IsAsset) AS IsAsset,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.Cat_Type, RightInst.Cat_Type) AS Cat_Type,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.Acc_Type, RightInst.Acc_Type) AS Acc_Type,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.RbcTier, RightInst.RbcTier) AS RbcTier,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.LocDivExclu, RightInst.LocDivExclu) AS LocDivExclu,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.StaDivExclu, RightInst.StaDivExclu) AS StaDivExclu,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.FedDivExclu, RightInst.FedDivExclu) AS FedDivExclu,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.LocTaxExempt, RightInst.LocTaxExempt) AS LocTaxExempt,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.StaTaxExempt, RightInst.StaTaxExempt) AS StaTaxExempt,");
            sqlQuery.AppendLine("		ISNULL(LeftInst.FedTaxExempt, RightInst.FedTaxExempt) AS FedTaxExempt");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			IsAsset,");
            //TODO Should Acc_Type / Classificaiton Override codes be a toggle?
            sqlQuery.AppendLine("			Acc_TypeOR AS Acc_Type,");
            sqlQuery.AppendLine("			ClassOR AS RbcTier,");
            sqlQuery.AppendLine("			Cat_Type,");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			Name,");
            sqlQuery.AppendLine("			Sequence,");
            sqlQuery.AppendLine("			LocDivExclu,");
            sqlQuery.AppendLine("			StaDivExclu,");
            sqlQuery.AppendLine("			FedDivExclu,");
            sqlQuery.AppendLine("			LocTaxExempt,");
            sqlQuery.AppendLine("			StaTaxExempt,");
            sqlQuery.AppendLine("			FedTaxExempt");
            sqlQuery.AppendLine("		FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + leftSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("			AND Exclude <> 1");
            sqlQuery.AppendLine("	) AS LeftInst");
            sqlQuery.AppendLine("	FULL OUTER JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			IsAsset,");
            //TODO Should Acc_Type / Classificaiton Override codes be a toggle?
            sqlQuery.AppendLine("			Acc_TypeOR AS Acc_Type,");
            sqlQuery.AppendLine("			ClassOR AS RbcTier,");
            sqlQuery.AppendLine("			Cat_Type,");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			Name,");
            sqlQuery.AppendLine("			Sequence,");
            sqlQuery.AppendLine("			LocDivExclu,");
            sqlQuery.AppendLine("			StaDivExclu,");
            sqlQuery.AppendLine("			FedDivExclu,");
            sqlQuery.AppendLine("			LocTaxExempt,");
            sqlQuery.AppendLine("			StaTaxExempt,");
            sqlQuery.AppendLine("			FedTaxExempt");
            sqlQuery.AppendLine("		FROM " + balanceSheetCompare.RightInstitutionDatabaseName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + rightSimScen.GetDynamicProperty("simulation"));
            sqlQuery.AppendLine("			AND Exclude <> 1 AND cat_Type IN (0,1,5)");
            sqlQuery.AppendLine("	) AS RightInst");
            sqlQuery.AppendLine("	ON LeftInst.Code = RightInst.Code");
            sqlQuery.AppendLine(") AS B");
            sqlQuery.AppendLine("ON A.Code = B.Code");
            if (viewOption == 4)
            {
                sqlQuery.AppendLine("");
                sqlQuery.AppendLine("-- Left/Right Defined Groupings");
                sqlQuery.AppendLine("INNER JOIN (");
                sqlQuery.AppendLine("	SELECT");
                sqlQuery.AppendLine("		C.IsAsset,");
                sqlQuery.AppendLine("		C.Acc_Type,");
                sqlQuery.AppendLine("		C.RbcTier,");
                sqlQuery.AppendLine("		B.Name AS DefinedGroupingName,");
                sqlQuery.AppendLine("		B.Priority AS DefinedGroupingOrder");
                sqlQuery.AppendLine("	FROM " + AtlasTemplates + ".dbo.ATLAS_DefinedGrouping AS A");
                sqlQuery.AppendLine("	INNER JOIN " + AtlasTemplates + ".dbo.ATLAS_DefinedGroupingGroups AS B");
                sqlQuery.AppendLine("	ON A.Id = B.DefinedGroupingId");
                sqlQuery.AppendLine("	INNER JOIN " + AtlasTemplates + ".dbo.ATLAS_DefinedGroupingClassifications AS C");
                sqlQuery.AppendLine("	ON B.Id = C.DefinedGroupingGroupId");
                sqlQuery.AppendLine("	WHERE A.Id = " + ParseDefinedGrouping(balanceSheetCompare.ViewOption));
                sqlQuery.AppendLine(") AS E");
                sqlQuery.AppendLine("ON B.IsAsset = E.IsAsset AND B.Acc_Type = E.Acc_Type AND B.RbcTier = E.RbcTier");
            }
            if (viewOption > 1)
            {
                sqlQuery.AppendLine("");
                sqlQuery.AppendLine("-- Left/Right Account Types (should be same for both)");
                sqlQuery.AppendLine("INNER JOIN (");
                sqlQuery.AppendLine("	SELECT");
                sqlQuery.AppendLine("		B.IsAsset,");
                sqlQuery.AppendLine("		B.Acc_Type,");
                sqlQuery.AppendLine("		A.Name AS AccountName,");
                sqlQuery.AppendLine("		B.AccountOrder");
                sqlQuery.AppendLine("	FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.ATLAS_AccountType AS A");
                sqlQuery.AppendLine("	INNER JOIN (");
                sqlQuery.AppendLine("		SELECT");
                sqlQuery.AppendLine("			IsAsset,");
                sqlQuery.AppendLine("			Acc_Type,");
                sqlQuery.AppendLine("			[Order] AS AccountOrder");
                sqlQuery.AppendLine("		FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.ATLAS_AccountTypeOrder");
                sqlQuery.AppendLine("	) AS B");
                sqlQuery.AppendLine("	ON A.Id = B.Acc_Type");
                sqlQuery.AppendLine(") AS C");
                sqlQuery.AppendLine("ON B.IsAsset = C.IsAsset AND B.Acc_Type = C.Acc_Type");
                if (viewOption == 2)
                {
                    sqlQuery.AppendLine("");
                    sqlQuery.AppendLine("-- Left/Right Classificiations (should be same for both)");
                    sqlQuery.AppendLine("INNER JOIN (");
                    sqlQuery.AppendLine("	SELECT");
                    sqlQuery.AppendLine("		A.IsAsset,");
                    sqlQuery.AppendLine("		B.Acc_Type,");
                    sqlQuery.AppendLine("		A.RbcTier,");
                    sqlQuery.AppendLine("		A.Name AS ClassificationName,");
                    sqlQuery.AppendLine("		B.ClassificationOrder");
                    sqlQuery.AppendLine("	FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.ATLAS_Classification AS A");
                    sqlQuery.AppendLine("	INNER JOIN (");
                    sqlQuery.AppendLine("		SELECT");
                    sqlQuery.AppendLine("			Acc_Type,");
                    sqlQuery.AppendLine("			RBC_Tier,");
                    sqlQuery.AppendLine("			[Order] AS ClassificationOrder");
                    sqlQuery.AppendLine("		FROM " + balanceSheetCompare.LeftInstitutionDatabaseName + ".dbo.ATLAS_ClassificationOrder");
                    sqlQuery.AppendLine("	) AS B");
                    sqlQuery.AppendLine("	ON A.AccountTypeId = B.Acc_Type AND A.RbcTier = B.RBC_Tier");
                    sqlQuery.AppendLine(") AS D");
                    sqlQuery.AppendLine("ON B.IsAsset = D.IsAsset AND B.Acc_Type = D.Acc_Type AND B.RbcTier = D.RbcTier");
                }
            }
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("-- Groupings for everythings");
            sqlQuery.AppendLine("WHERE A.LeftBalance <> 0");
            sqlQuery.AppendLine("	OR A.RightBalance <> 0");
            sqlQuery.AppendLine("	OR A.LeftRate <> 0");
            sqlQuery.AppendLine("	OR A.RightRate <> 0");
            switch (viewOption)
            {
                case 4:
                    sqlQuery.AppendLine("GROUP BY");
                    sqlQuery.AppendLine("	B.IsAsset,");
                    //sqlQuery.AppendLine("	B.Acc_Type,");
                    sqlQuery.AppendLine("	E.DefinedGroupingName,");
                    sqlQuery.AppendLine("	E.DefinedGroupingOrder");
                    sqlQuery.AppendLine("ORDER BY B.IsAsset DESC, E.DefinedGroupingOrder");
                    break;
                case 3:
                    sqlQuery.AppendLine("GROUP BY");
                    sqlQuery.AppendLine("	B.IsAsset,");
                    sqlQuery.AppendLine("	B.Acc_Type,");
                    sqlQuery.AppendLine("	C.AccountName,");
                    sqlQuery.AppendLine("	C.AccountOrder");
                    sqlQuery.AppendLine("ORDER BY B.IsAsset DESC, C.AccountOrder");
                    break;
                case 2:
                    sqlQuery.AppendLine("GROUP BY");
                    sqlQuery.AppendLine("	B.IsAsset,");
                    sqlQuery.AppendLine("	B.Acc_Type,");
                    sqlQuery.AppendLine("	B.RbcTier,");
                    sqlQuery.AppendLine("	C.AccountName,");
                    sqlQuery.AppendLine("	D.ClassificationName,");
                    sqlQuery.AppendLine("	C.AccountOrder,");
                    sqlQuery.AppendLine("	D.ClassificationOrder");
                    sqlQuery.AppendLine("ORDER BY B.IsAsset DESC, C.AccountOrder, D.ClassificationOrder");
                    break;
                default: //0, 1
                    sqlQuery.AppendLine("	OR B.Cat_Type IN (2, 3, 4, 6, 7)");
                    sqlQuery.AppendLine("ORDER BY B.Sequence");
                    break;
            }
            return Utilities.ExecuteSql(leftInstService.ConnectionString(), sqlQuery.ToString());
        }

        /// <summary>
        /// Return the dayth day of the month for given date. (what?)
        /// </summary>
        /// <param name="date">Input Date</param>
        /// <param name="day">Input Day</param>
        /// <returns>Dayth day of month for given date. (what?)</returns>
        static private DateTime DayOfMonth(DateTime date, int day = 1)
        {
            return new DateTime(date.Year, date.Month, day);
        }

        /// <summary>
        /// Helper method to safely parse a double from a string.
        /// </summary>
        /// <param name="input">String to parse.</param>
        /// <returns>input as double if successfully parsed; 0 otherwise.</returns>
        static private double DoubleSafeParse(string input)
        {
            double ret;
            return (double.TryParse(input, out ret)) ? ret : 0;
        }

        public IEnumerable<object> LineItems(BalanceSheetCompare balanceSheetCompare,
            InstitutionService leftInstService, InstitutionService rightInstService, Report rep)
        {


            var leftSimScen = leftInstService.GetSimulationScenario(rep.Package.AsOfDate, balanceSheetCompare.LeftAsOfDateOffset, balanceSheetCompare.LeftSimulationTypeId, balanceSheetCompare.LeftScenarioTypeId);
            var rightSimScen = rightInstService.GetSimulationScenario(rep.Package.AsOfDate, balanceSheetCompare.RightAsOfDateOffset, balanceSheetCompare.RightSimulationTypeId, balanceSheetCompare.RightScenarioTypeId);

            if (leftSimScen.GetDynamicProperty("simulation").ToString() == "" || leftSimScen.GetDynamicProperty("scenario").ToString() == "")
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), balanceSheetCompare.LeftScenarioType.Name, balanceSheetCompare.LeftSimulationType.Name, balanceSheetCompare.LeftAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }
            if (rightSimScen.GetDynamicProperty("simulation").ToString() == "" || rightSimScen.GetDynamicProperty("scenario").ToString() == "")
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), balanceSheetCompare.RightScenarioType.Name, balanceSheetCompare.RightSimulationType.Name, balanceSheetCompare.RightAsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
            }


            var cats = new LinkedList<LineItem>();

            //Before we do anything, fix up dates to make sure they're at the first of the month
            balanceSheetCompare.LeftStartDate = DayOfMonth(balanceSheetCompare.LeftStartDate);
            balanceSheetCompare.LeftEndDate = DayOfMonth(balanceSheetCompare.LeftEndDate, 2);
            balanceSheetCompare.RightStartDate = DayOfMonth(balanceSheetCompare.RightStartDate);
            balanceSheetCompare.RightEndDate = DayOfMonth(balanceSheetCompare.RightEndDate, 2);

            var fullTable = PullCategoriesActualsProjections(balanceSheetCompare,
                leftInstService, rightInstService,
                leftSimScen, rightSimScen);

            var totalingItems = new LinkedList<BalanceSheetTotaler.TotalingItem>();
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("LeftBalance"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("RightBalance"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("LeftRate", BalanceSheetTotaler.TotalingType.Average, "LeftBalance"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("RightRate", BalanceSheetTotaler.TotalingType.Average, "RightBalance"));
            fullTable = BalanceSheetTotaler.CalcTotalsFor(fullTable, totalingItems, (BalanceSheetTotaler.ViewByType)ParseViewOption(balanceSheetCompare.ViewOption));

            LineItem lineItem;
            int catType;
            foreach (DataRow row in fullTable.Rows)
            {
                lineItem = new LineItem();
                cats.AddLast(lineItem);

                lineItem.CatName = row["Name"].ToString();
                lineItem.CatType = (!int.TryParse(row["Cat_Type"].ToString(), out catType)) ? 0 : catType;
                switch (ParseViewOption(balanceSheetCompare.ViewOption))
                {
                    case 4:
                    case 3:
                        if (lineItem.CatName == "Other")
                            lineItem.CatName += (bool.Parse(row["IsAsset"].ToString())) ? " Asset" : " Liability";
                        break;
                }

                lineItem.LeftBal = DoubleSafeParse(row["LeftBalance"].ToString());
                lineItem.LeftRate = DoubleSafeParse(row["LeftRate"].ToString());
                lineItem.RightBal = DoubleSafeParse(row["RightBalance"].ToString());
                lineItem.RightRate = DoubleSafeParse(row["RightRate"].ToString());


                if (row["IsAsset"].ToString() == "")
                {
                    lineItem.IsAsset = false;
                }
                else
                {
                    lineItem.IsAsset = (bool.Parse(row["IsAsset"].ToString()));
                }
                
            }

            return cats.Where(c => c.LeftBal != 0 || c.LeftRate != 0 || c.RightBal != 0 || c.RightRate != 0);
        }
    }
}