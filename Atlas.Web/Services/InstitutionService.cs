﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Atlas.Institution.Model;
using Atlas.Institution.Data;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;

namespace Atlas.Web.Services
{
    public class InstitutionService
    {
        // private readonly Global.Model.Institution _institution;
        private readonly string _institution;
        private readonly InstitutionContext _ctx;
        private string _conStr;

        public InstitutionService(string institution)
        {
            _institution = institution;
            _conStr = Utilities.BuildInstConnectionString(institution);
            _ctx = new InstitutionContext(_conStr);
            if (!_ctx.Database.Exists()) { throw new Exception("Could not connect to institution database"); }
        }

        public string ConnectionString()
        {
            return _conStr;
        }

        public SimulationType GetSimulationType(int simId)
        {
           return  _ctx.SimulationTypes.FirstOrDefault(s => s.Id == simId);
        }

        public Information GetInformation(DateTime asOfDate, int asOfDateOffset)
        {
            var infoArray = _ctx.Information.AsNoTracking()
                                .Where(i => i.AsOfDate <= asOfDate && i.BasisDate == 1)
                                .OrderByDescending(i => i.AsOfDate).ToArray();

            if (infoArray.Length < asOfDateOffset + 1)
            {
                throw new ArgumentOutOfRangeException("asOfDateOffset", " Can not find an as of date for" + _institution
                                                                        + " " + asOfDateOffset + " reviews prior to the" + asOfDate + " review");
            }
            return infoArray[asOfDateOffset];
        }

        public Simulation GetSimulation(DateTime asOfDate, int asOfDateOffset, SimulationType simulationType)
        {
            var info = GetInformation(asOfDate, asOfDateOffset);
            if (simulationType == null)
            {
                return null;
            }
           // return null;
            var sim = info.Simulations.FirstOrDefault(s => s.SimulationTypeId == simulationType.Id);

            return sim;
        }

        public Scenario GetScenario(DateTime asOfDate, int asOfDateOffset, SimulationType simulationType, Institution.Model.ScenarioType scenarioType)
        {
            var sim = GetSimulation(asOfDate, asOfDateOffset, simulationType);

            var scen = sim.Scenarios.FirstOrDefault(s => s.StandardName() == scenarioType.Name.ToLower());
            if (scen == null)
            {
                //throw new ArgumentOutOfRangeException("scenarioType", "Can not find the " + scenarioType.Name
                //+ " scenario in the " + sim.Name + "simulation " + " of the " + asOfDate.ToShortDateString() + " review of " +
                // _institution);
            }
            return scen;
        }


        public DateTime LastProjectionBucketDate(int simulationId)
        {
            //var lastProjectionBucketDate = _ctx.BalancesAndRates.Where(bal => bal.SimulationId == simulationId).Max(bal => bal.Month);
            //DataTable dt = Utilities.ExecuteSql(_conStr, "SELECT MAX(Month) FROM Basis_ALP1 WHERE SimulationId = " + simulationId);
            DataTable dt = Utilities.ExecuteSql(_conStr, "SELECT FProjection FROM Basis_Bank WHERE SimulationId = " + simulationId);
            return DateTime.Parse(dt.Rows[0][0].ToString());
        }

        public object GetSimulationScenario(DateTime asOfDate, int offset, int simulationTypeId, int scenarioTypeId)
        {
            DataTable dt = new DataTable();
            dt = Utilities.ExecuteSql(_conStr, "SELECT id, asOfDate FROM asOfDateInfo WHERE asOfDate <= '" + asOfDate.ToShortDateString() + "' AND BasisDate = 1 ORDER BY asOfDate DESC");
            if (dt.Rows.Count > offset)
            {
                int infoId = int.Parse(dt.Rows[offset][0].ToString());
                DateTime aod = DateTime.Parse(dt.Rows[offset][1].ToString());
                StringBuilder sqlQuery = new StringBuilder();

                sqlQuery.AppendLine(" SELECT s.id, bs.number from simulations as s ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_SimulationType as st ON ");
                sqlQuery.AppendLine(" st.id = s.SimulationTypeId  ");
                sqlQuery.AppendLine(" INNER JOIN ");
                sqlQuery.AppendLine(" BASIS_Scenario as bs ON ");
                sqlQuery.AppendLine(" bs.simulationId = s.id  ");
                sqlQuery.AppendLine(" WHERE informationId = '" + infoId.ToString() + "' and s.simulationTypeId = '" + simulationTypeId.ToString() + "' and bs.scenarioTypeId = '" + scenarioTypeId.ToString() + "' ");

                dt = Utilities.ExecuteSql(_conStr, sqlQuery.ToString());

                if (dt.Rows.Count > 0)
                {
                    return new
                   {
                       asOfDate = aod.ToShortDateString(),
                       simulation = dt.Rows[0][0].ToString(),
                       scenario = dt.Rows[0][1].ToString()
                   };
                }
                else
                {
                    return new
                   {
                       asOfDate = "",
                       simulation = "",
                       scenario = ""
                   };
                }



            }
            else
            {
                return new
                {
                    asOfDate = "",
                    simulation = "",
                    scenario = ""
                };
            }


        }
    }
}