﻿using System;
using System.Data.SqlClient;
using System.Configuration;

using Atlas.Institution.Data;
using Atlas.Institution.Model;

using Breeze.ContextProvider.EF6;
using Breeze.ContextProvider;

namespace Atlas.Web.Services
{
    public class GlobalContextProvider : EFContextProvider<InstitutionContext>
    {
        public string User { get; set; }
        public string DatabaseName { get; set; }

        public GlobalContextProvider(string dbName) : base()
        {
            DatabaseName = dbName;
        }

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            // return false if we don’t want the entity saved.
            // prohibit any additions of entities of type 'Role'
            if (entityInfo.Entity.GetType() == typeof(Package))
            {
                Package package = (Package)entityInfo.Entity;
                package.DateModified = DateTime.Now;
                package.ModifiedBy = User ?? "Unknown";
                entityInfo.ForceUpdate = true;
                if (entityInfo.EntityState == EntityState.Deleted)
                {
                    PackageService packageService = new PackageService();
                   // packageService.Delete(package.Id); KGN NEED TO REVIST
                }
            }

            return true;
        }

        protected override InstitutionContext CreateContext()
        {
            //Generates the Institution Context For Whatever Database the User is in
            string conStr = "";

            if (DatabaseName != null && DatabaseName != "")
            {
                SqlConnectionStringBuilder conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
                {
                    InitialCatalog = DatabaseName
                };

                conStr = conenctionStringBuilder.ConnectionString;
            }
            else if (Utilities.GetAtlasUserData().Rows.Count > 0 && Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() != "")
            {
                SqlConnectionStringBuilder conenctionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString())
                {
                    InitialCatalog = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString()
                };

                conStr = conenctionStringBuilder.ConnectionString;
            }
            else
            {
                conStr = ConfigurationManager.ConnectionStrings["InstitutionContext"].ToString();
            }

            return new InstitutionContext(conStr);
        }
    }
}