﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Atlas.Web.Services
{
    public class ASC825BalanceSheetService
    {
        public List<string> errors = new List<string>();
        public List<string> warnings = new List<string>();
        public object ASC825BalanceSheetViewModel(int reportId)
        {

            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));

            //Look Up Funding Matrix Settings
            var sr = gctx.ASC825BalanceSheets.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            //If Not Found Kick Out
            if (sr == null) throw new Exception("ASC825 Report Not Found");


            DataTable tblData = new DataTable();

            var instService = new InstitutionService(sr.InstitutionDatabaseName);
            //Find simulation Id
            object obj = instService.GetSimulationScenario(rep.Package.AsOfDate, sr.AsOfDateOffset, sr.SimulationTypeId, sr.ScenarioTypeId);
            string simulationId = obj.GetType().GetProperty("simulation").GetValue(obj).ToString();
            string scenario = obj.GetType().GetProperty("scenario").GetValue(obj).ToString();
            string asOfDate = obj.GetType().GetProperty("asOfDate").GetValue(obj).ToString();

            //If I did not find the simulation ID then tell user what they are looking for does not exist
            if (simulationId == "")
            {
                errors.Add(String.Format(Utilities.GetErrorTemplate(1), sr.ScenarioType.Name, sr.SimulationType.Name, sr.AsOfDateOffset.ToString(), rep.Package.AsOfDate.ToShortDateString()));
                //throw new Exception(String.Format("Simulation {0} and scenario {1} do not exist in the date offset of {2} for as of date {3}",sr.SimulationType.Name, sr.ScenarioType.Name,  sr.AsOfDateOffset.ToString(), rep.Package.AsOfDate.ToShortDateString()));
            }
            else
            {


                asOfDate = new DateTime(DateTime.Parse(asOfDate).AddMonths(1).Year, DateTime.Parse(asOfDate).AddMonths(1).Month, 1).ToShortDateString();
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" 	alb.name ");
                sqlQuery.AppendLine("   ,alb.cat_type");
                sqlQuery.AppendLine(" 	,alb.sequence ");
                sqlQuery.AppendLine(" 	,ISNULL(alpData.rate, 0) as rate ");
                sqlQuery.AppendLine(" 	,als.MVFuncCost  ");
                sqlQuery.AppendLine(" 	,alpdata.discount ");
                sqlQuery.AppendLine("   ,ISNULL(alpData.stBal, 0)  as bookBalance ");
                sqlQuery.AppendLine(" 	,CASE  ");
                sqlQuery.AppendLine(" 		WHEN ALS.parBook <> 0 THEN  ");
                sqlQuery.AppendLine(" 			alpData.stBal  ");
                sqlQuery.AppendLine(" 		ELSE ");
                sqlQuery.AppendLine(" 			CASE  ");
                sqlQuery.AppendLine(" 				WHEN ALS.Start_Par = 0 AND alpData.steBal <> 0 THEN ");
                sqlQuery.AppendLine(" 					alpData.steBal ");
                sqlQuery.AppendLine(" 				ELSE ");
                sqlQuery.AppendLine(" 					als.start_Par ");
                sqlQuery.AppendLine(" 			END ");
                sqlQuery.AppendLine(" 	END as parValue  ");
                sqlQuery.AppendLine(" 	,ISNULL(als.MVAmount, 0) as MVAmount ");
                sqlQuery.AppendLine(" 	,ISNULL((als.MVAmount / NULLIF(alpData.bookBalance, 0)) * 100, 0) AS marketPrice ");
                sqlQuery.AppendLine(" 	,ISNULL(als.MVAmount, 0) - ISNULL(alpData.BookBalance, 0) AS gain ");
                sqlQuery.AppendLine(" 	,alb.Int_Bearing as intBearing ");
                sqlQuery.AppendLine(" 	,AlS.MvRatio ");
                sqlQuery.AppendLine(" 	,ALS.mvMethod ");
                sqlQuery.AppendLine(" 	,ALS.MVRatioOverride  ");
                sqlQuery.AppendLine(" 	,als.MvNewFlag  ");
                sqlQuery.AppendLine(" 	,als.newIndexO ");
                sqlQuery.AppendLine(" 	,als.MvnewIndexO ");
                sqlQuery.AppendLine(" 	,als.MvIndexO ");
                sqlQuery.AppendLine(" 	,als.MVIndexC  ");
                sqlQuery.AppendLine(" 	,idxc.name as indexCName ");
                sqlQuery.AppendLine(" 	,mvidxc.name as MVindexCName ");
                sqlQuery.AppendLine("	,evIdx.name AS evIndexName ");
                sqlQuery.AppendLine("	,repidxc.name as repIndexName ");
                sqlQuery.AppendLine("	,als.RepEIndexO  ");
                sqlQuery.AppendLine("	,als.EVIndexO  ");
                sqlQuery.AppendLine("   ,alb.IsAsset ");
                sqlQuery.AppendLine(" FROM ");
                sqlQuery.AppendLine(" (SELECT ");
                sqlQuery.AppendLine(" 	ISNULL(ALP6.simulationId, ALP17.simulationId) as simulationId ");
                sqlQuery.AppendLine(" 	,ISNULL(ALP6.scenario, ALP17.scenario) as scenario ");
                sqlQuery.AppendLine(" 	,ISNULL(ALP6.code, ALP17.code) as code ");
                sqlQuery.AppendLine(" 	,alp6.Balance as bookBalance ");
                sqlQuery.AppendLine(" 	,alp6.rate as rate ");
                sqlQuery.AppendLine(" 	,alp17.discount as discount ");
                sqlQuery.AppendLine("   ,alp1.stBal ");
                sqlQuery.AppendLine(" 	,alp1.steBal ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" 	FROM ");
                sqlQuery.AppendLine(" 	(SELECT    ");
                sqlQuery.AppendLine(" 		simulationId    ");
                sqlQuery.AppendLine(" 		,scenario    ");
                sqlQuery.AppendLine(" 		,code  ");
                sqlQuery.AppendLine(" 		,SUM(ep.SGapBal / ep.nMonths) as Balance    ");
                sqlQuery.AppendLine(" 		,SUM(ep.SGapRate * (ep.sGapBal / ep.NMonths)) / NULLIF(SUM(ep.SGapBal / ep.nMonths), 0) as  rate    ");
                sqlQuery.AppendLine(" 		FROM    ");
                sqlQuery.AppendLine(" 		(SELECT    ");
                sqlQuery.AppendLine(" 			simulationId    ");
                sqlQuery.AppendLine(" 			,scenario    ");
                sqlQuery.AppendLine(" 			,code  ");
                sqlQuery.AppendLine(" 			,Month    ");
                sqlQuery.AppendLine(" 			,SGapBal    ");
                sqlQuery.AppendLine(" 			,SGapRate    ");
                sqlQuery.AppendLine(" 			,NMonths    ");
                sqlQuery.AppendLine(" 			FROM Basis_ALP6 WHERE simulationId = " + simulationId.ToString() + " AND scenario = '" + scenario + "') as ep    ");
                sqlQuery.AppendLine(" 			INNER JOIN    ");
                sqlQuery.AppendLine(" 			ATLAS_PeriodSmoother as ps    ");
                sqlQuery.AppendLine(" 			ON ep.NMonths >= ps.period ");
                sqlQuery.AppendLine(" 			GROUP BY simulationId, scenario, code) AS alp6 ");
                sqlQuery.AppendLine(" 			FULL OUTER JOIN ");
                sqlQuery.AppendLine(" 			(SELECT ");
                sqlQuery.AppendLine(" 				simulationId    ");
                sqlQuery.AppendLine(" 				,scenario    ");
                sqlQuery.AppendLine(" 				,code  ");
                sqlQuery.AppendLine(" 				,SUM(MVCashflow) as MVCshflow ");
                sqlQuery.AppendLine(" 				,SUM(MVCashflow * MVDiscount) / NULLIF(SUM(MVCashflow),0) as discount ");
                sqlQuery.AppendLine(" 				FROM Basis_ALP17 ");
                sqlQuery.AppendLine(" 				WHERE simulationId = " + simulationId.ToString() + " AND scenario = '" + scenario + "' ");
                sqlQuery.AppendLine(" 				GROUP BY simulationId ,scenario ,code) AS alp17 ");
                sqlQuery.AppendLine(" 		ON alp6.code = alp17.code ");
                sqlQuery.AppendLine("       LEFT JOIN ");
                sqlQuery.AppendLine(" 		(SELECT ");
                sqlQuery.AppendLine(" 			code ");
                sqlQuery.AppendLine(" 			,simulationId ");
                sqlQuery.AppendLine(" 			,scenario ");
                sqlQuery.AppendLine(" 			,stBal ");
                sqlQuery.AppendLine(" 			,steBal ");
                sqlQuery.AppendLine(" 		FROM Basis_ALP1 ");
                sqlQuery.AppendLine(" 		WHERE simulationId = " + simulationId + " AND month = '" + asOfDate + "' AND scenario = '" + scenario + "' ) AS ALP1 ");
                sqlQuery.AppendLine(" 		ON alp6.code = alp1.code) ");
                sqlQuery.AppendLine(" 		AS alpData ");
                sqlQuery.AppendLine(" 		INNER JOIN Basis_ALS AS als   ");
                sqlQuery.AppendLine(" 		ON als.simulationId = alpData.SimulationId AND    ");
                sqlQuery.AppendLine(" 		als.scenario = alpData.scenario AND    ");
                sqlQuery.AppendLine(" 		als.code = alpData.code    ");
                sqlQuery.AppendLine(" 		RIGHT JOIN Basis_ALB AS alb    ");
                sqlQuery.AppendLine(" 		ON alpData.SimulationId = alb.SimulationId     ");
                sqlQuery.AppendLine(" 		AND alpData.code = alb.code	   ");
                sqlQuery.AppendLine(" 		LEFT JOIN Basis_IndexB AS idxc ");
                sqlQuery.AppendLine(" 		ON idxc.SimulationId = alpdata.simulationId  ");
                sqlQuery.AppendLine(" 		AND idxc.Code = als.NewIndexC ");
                sqlQuery.AppendLine(" 		LEFT JOIN Basis_IndexB AS mvidxc ");
                sqlQuery.AppendLine(" 		ON mvidxc.SimulationId = alpdata.simulationId  ");
                sqlQuery.AppendLine(" 		AND mvidxc.Code = als.MVIndexC ");

                sqlQuery.AppendLine("			LEFT JOIN Basis_IndexB AS repidxc ");
                sqlQuery.AppendLine("			ON mvidxc.SimulationId = alpdata.simulationId  ");
                sqlQuery.AppendLine("			AND mvidxc.Code = als.RepEIndexC ");
                sqlQuery.AppendLine("			LEFT JOIN Basis_IndexB AS evidx ON ");
                sqlQuery.AppendLine("			evidx.simulationId  = alpdata.simulationId AND ");
                sqlQuery.AppendLine("			evIdx.code = als.EVIndexC  ");
                sqlQuery.AppendLine(" 		WHERE ISNULL(als.exclude,0) = 0 AND alb.simulationid = " + simulationId.ToString() + " AND alb.exclude = 0 AND alb.Cat_Type in (0,1,2,3,4,5)  AND alb.Acc_TypeOR <> 4		 ");
                sqlQuery.AppendLine(" 		ORDER BY Alb.Sequence  ");

                tblData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(sr.InstitutionDatabaseName), sqlQuery.ToString());

                tblData.Columns.Add("DiscountIndex");
                tblData.Columns.Add("DiscountOffset");
                tblData.Columns.Add("NewIndex");
                tblData.Columns.Add("IndexOffSet");
            }

            foreach (DataColumn col in tblData.Columns)
            {
                col.ReadOnly = false;
            }
            double nOffset = 0;
            foreach (DataRow dr in tblData.Rows)
            {


                //if (dr["name"].ToString().Contains("Swap") && dr["name"].ToString().Contains("Pay"))
                //{
                //    for (int i = 0; i < 1; i++)
                //    {
                //        int xz = 11;
                //    }
                //}
                //Perform Logic On actual records only
                if (dr["cat_Type"].ToString() == "0" || dr["cat_Type"].ToString() == "1" || dr["cat_Type"].ToString() == "5")
                {


                    if (dr["MvMethod"].ToString() == "2" && dr["MVRatioOverride"].ToString() != "0")
                    {
                        dr["marketPrice"] = (double.Parse(dr["ParValue"].ToString()) * double.Parse(dr["MvRatio"].ToString())) / 100;
                    }
                    else
                    {
                        if (dr["ParValue"].ToString() != "0" && dr["ParValue"].ToString() != "")
                        {
                            dr["marketPrice"] = (double.Parse(dr["MVAmount"].ToString()) / double.Parse(dr["ParValue"].ToString())) * 100;
                        }

                    }


                    if (dr["ParValue"].ToString() == "")
                    {
                        dr["ParValue"] = 0;
                    }


                    if (dr["intBearing"].ToString() == "0")
                    {
                        if (dr["MVMethod"].ToString() == "2")
                        {
                            dr["discountIndex"] = "Override Calc MV";
                            dr["discountOffSet"] = "";
                        }
                        else
                        {
                            dr["discountIndex"] = "Starting Balance";
                            dr["discountOffSet"] = "";
                        }
                        dr["discount"] = 0;
                        dr["newIndex"] = "";
                        dr["indexOffSet"] = "";

                    }
                    else
                    {
                        switch (dr["MvMethod"].ToString())
                        {
                            case "0":
                                dr["newIndex"] = dr["indexCName"];
                                nOffset = 0;
                                if (dr["MvNewFlag"].ToString() == "1")
                                {
                                    nOffset = double.Parse(dr["MvnewIndexO"].ToString()) * 100;
                                }
                                else
                                {
                                    nOffset = double.Parse(dr["newIndexO"].ToString()) * 100;
                                }


                                if (nOffset == 0)
                                {
                                    dr["indexOffSet"] = "";
                                }
                                else
                                {
                                    dr["indexOffSet"] = nOffset;
                                }


                                dr["discountIndex"] = "";
                                dr["discountOffSet"] = "";
                                break;
                            case "1":
                                dr["discountIndex"] = dr["MVindexCName"];
                                nOffset = 0;
                                nOffset = double.Parse(dr["MvIndexO"].ToString()) * 100;

                                if (nOffset == 0)
                                {
                                    dr["discountOffSet"] = "";
                                }
                                else
                                {
                                    dr["discountOffSet"] = nOffset;
                                }

                                dr["newIndex"] = "";
                                dr["indexOffSet"] = "";
                                break;
                            case "2":
                                dr["discountIndex"] = "Override Calc MV";
                                dr["discountOffSet"] = "";
                                dr["discount"] = 0;
                                dr["newIndex"] = "";
                                dr["indexOffSet"] = "";
                                break;
                            case "3":
                                dr["discountIndex"] = "Starting Balance";
                                dr["discountOffSet"] = "";
                                dr["discount"] = 0;
                                dr["newIndex"] = "";
                                dr["indexOffSet"] = "";
                                break;
                            case "4":
                                dr["discountIndex"] = dr["MVindexCName"];

                                nOffset = 0;
                                nOffset = double.Parse(dr["MvIndexO"].ToString()) * 100;

                                if (nOffset == 0)
                                {
                                    dr["discountOffSet"] = "";
                                }
                                else
                                {
                                    dr["discountOffSet"] = nOffset;
                                }

                                dr["newIndex"] = dr["indexCName"];

                                nOffset = 0;
                                if (dr["MvNewFlag"].ToString() == "1")
                                {
                                    nOffset = double.Parse(dr["MvnewIndexO"].ToString()) * 100;
                                }
                                else
                                {
                                    nOffset = double.Parse(dr["newIndexO"].ToString()) * 100;
                                }

                                if (nOffset == 0)
                                {
                                    dr["indexOffSet"] = "";
                                }
                                else
                                {
                                    dr["indexOffSet"] = nOffset;
                                }

                                break;
                            case "5":
                                dr["discountIndex"] = dr["MVindexCName"];

                                nOffset = 0;
                                nOffset = double.Parse(dr["MvIndexO"].ToString()) * 100;
                                if (nOffset == 0)
                                {
                                    dr["discountOffSet"] = "";
                                }
                                else
                                {
                                    dr["discountOffSet"] = nOffset;
                                }

                                dr["newIndex"] = dr["repIndexName"];

                                nOffset = 0;
                                if (dr["MvNewFlag"].ToString() == "1")
                                {
                                    nOffset = double.Parse(dr["MvnewIndexO"].ToString()) * 100;
                                }
                                else
                                {
                                    nOffset = double.Parse(dr["RepEIndexO"].ToString()) * 100;
                                }

                                if (nOffset == 0)
                                {
                                    dr["indexOffSet"] = "";
                                }
                                else
                                {
                                    dr["indexOffSet"] = nOffset;
                                }

                                break;
                            case "6":
                                dr["discountIndex"] = dr["MVindexCName"];

                                nOffset = 0;
                                nOffset = double.Parse(dr["MvIndexO"].ToString()) * 100;
                                if (nOffset == 0)
                                {
                                    dr["discountOffSet"] = "";
                                }
                                else
                                {
                                    dr["discountOffSet"] = nOffset;
                                }

                                dr["newIndex"] = dr["evIndexName"];

                                nOffset = 0;
                                nOffset = double.Parse(dr["EVIndexO"].ToString()) * 100;
                                if (nOffset == 0)
                                {
                                    dr["indexOffSet"] = "";
                                }
                                else
                                {
                                    dr["indexOffSet"] = nOffset;
                                }

                                break;
                        }

                    }

                }




            }

            var totalingItems = new LinkedList<BalanceSheetTotaler.TotalingItem>();
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("bookBalance"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("MVAmount"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("marketPrice", BalanceSheetTotaler.TotalingType.Average, "parValue"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("gain"));
            totalingItems.AddLast(new BalanceSheetTotaler.TotalingItem("parValue"));
            tblData = BalanceSheetTotaler.CalcTotalsFor(tblData, totalingItems, BalanceSheetTotaler.ViewByType.AccountTypes);

            return new
            {
                tableData = tblData,
                errors = errors,
                warnings = warnings
            };
        }
    }
}