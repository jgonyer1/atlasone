﻿namespace Atlas.Web.Services
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Returns a dynamic property from an object.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="property">Property to fetch.</param>
        /// <returns></returns>
        public static object GetDynamicProperty(this object obj, string property)
        {
            return obj.GetType().GetProperty(property).GetValue(obj);
        }
    }
}
