﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web;


namespace Atlas.Web.Services
{
    public class LookbackGroupDataService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();

        //assumes we need to compare against Atlas Templates
        //this check will run if a given report.Default == true
        //so it might be that the given report already has it's lbLookbackId's properly set
        internal static int? GetLBLookbackIdToMatchTemplate(string lbLookbackId, bool skipLocalCheck, string asOfDate)
        {
            //default is to return the id we've passed in.
            int ret = Convert.ToInt32(lbLookbackId);
            StringBuilder qry = new StringBuilder();
            DataTable data = new DataTable();
            //check to see if we actually have the given Id / Name combo in our bank
            qry.AppendLine("SELECT Id FROM ATLAS_LBLookback WHERE Id = " + lbLookbackId);
            data = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), qry.ToString());
            qry.Clear();
            if (data.Rows.Count > 0 && !skipLocalCheck)
            {
                ret = Convert.ToInt32(data.Rows[0]["Id"].ToString());
            }
            else
            {
                data.Clear();
                qry.AppendLine("select ");
                qry.AppendLine("lb.Id as LBLookbackId ");
                qry.AppendLine(",lb.AsOfDate as LBLookbackAod ");
                qry.AppendLine(",lb.[Name]");
                qry.AppendLine(",lb.SimulationTypeId ");
                qry.AppendLine(",lb.ScenarioTypeId ");
                qry.AppendLine(",lbg.StartDateOffset, ");
                qry.AppendLine("lbg.EndDateOffset, ");
                qry.AppendLine("lbType.StartDateOffset as CustomTypeStartDateOffset ");
                qry.AppendLine(",lbType.EndDateOffset as CustomTypeEndDateOffset ");
                qry.AppendLine("from ATLAS_LBLookback lb ");
                qry.AppendLine("inner join ATLAS_LBLookbackGroup lbg ");
                qry.AppendLine("on lb.id = lbg.LBLookbackId ");
                qry.AppendLine("inner join ATLAS_LBCustomType lbType ");
                qry.AppendLine("on lbType.id = lbg.LBCustomTypeId ");
                qry.AppendLine("where lb.id =  " + lbLookbackId);
                data = Utilities.ExecuteSql(Utilities.BuildInstConnectionString("DDW_Atlas_Templates"), qry.ToString());

                if (data.Rows.Count > 0)
                {
                    qry.Clear();
                    DataTable tData = new DataTable();
                    qry.AppendLine("SELECT Id FROM Atlas_LBLookback WHERE cast(AsOfDate as date) = '" + asOfDate + "' AND Name = '" + data.Rows[0]["Name"].ToString() + "'");
                    tData = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), qry.ToString());

                    if (tData.Rows.Count > 0)
                    {
                        ret = Convert.ToInt32(tData.Rows[0]["Id"]);
                    }
                }
            }
            return ret;
        }

        public object GetLBGDProjectionData(int layoutId, string stSimulationID, string stScenario, string stAsOfDate, string endAsOfDate, string endSimulationID, bool taxEquiv, InstitutionService instService)
        {
            DataTable retTable = new DataTable();
            if (stAsOfDate == "")
            {
                errors.Add("Cannot find data for start date requested");
            }
            else if (endAsOfDate == "")
            {
                errors.Add("Cannot find data for end date requested");
            }
            else
            {
                int days = DateTime.Parse(endAsOfDate).Subtract(DateTime.Parse(stAsOfDate)).Days;
                InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
                StringBuilder sb = new StringBuilder();
                double[][] retArr = new double[2][];

                //sb.AppendLine("select ");
                //sb.AppendLine("isnull(lb.title, 'Blank') as title");
                //sb.AppendLine(",lb.RowType");
                //sb.AppendLine(",lb.Id");
                //sb.AppendLine(",lb.IsAsset");
                //sb.AppendLine(",lb.Priority");
                //sb.AppendLine(",ISNULL( sum(alb_alp." + (taxEquiv ? "AveTEAmt" : "IETotAmount") + "), 0 ) as incExp -- this field changes based on tax Equiv or not");
                //sb.AppendLine(",ISNULL(sum(alb_alp.AveBal / " + days + "), 0) as AvgBal");
                //sb.AppendLine("from");
                //sb.AppendLine("(select rowitems.id, title, Acc_Type, RbcTier, RowType, rowitems.IsAsset, Priority from ATLAS_LBCustomLayoutRowItem as rowitems left join ATLAS_LBCustomLayoutClassifications as classifications on classifications.LBCustomLayoutRowItemId = rowitems.id");
                //sb.AppendLine("where rowitems.LBCustomLayoutId = " + layoutId + ") as lb");
                //sb.AppendLine("left join ");
                //sb.AppendLine("ATLAS_Classification as c on lb.Acc_Type = c.AccountTypeId and lb.RbcTier = c.RbcTier and c.IsAsset = lb.IsAsset");
                //sb.AppendLine("left join");
                //sb.AppendLine("(select ");
                //sb.AppendLine("IETotAmount, ");
                //sb.AppendLine("AveTEAmt, ");
                //sb.AppendLine("Acc_TypeOR, ");
                //sb.AppendLine("ClassOR, ");
                //sb.AppendLine("AveBal * datepart(DAY, EOMONTH(alp1Month)) as AveBal,");
                //sb.AppendLine("");
                //sb.AppendLine("IsAsset,	");
                //sb.AppendLine("alb.code,");
                //sb.AppendLine("alb.name,");
                //sb.AppendLine("alp13Month as alp13Month,");
                //sb.AppendLine("alp1Month as alp1Month");
                //sb.AppendLine("from basis_alb as alb ");
                //sb.AppendLine("inner join ");
                //sb.AppendLine("(select ISNULL(alp1.code, alp13.code) as code, ISNULL(alp1.SimulationId, alp13.SimulationId) as SimulationId, IETotAmount, AveTEAmt, AveBal, alp1.month as alp1Month, alp13.month as alp13Month from basis_alp1 as alp1 full outer join basis_alp13 as alp13");
                //sb.AppendLine("	on alp1.code = alp13.code and alp1.month = alp13.month and alp1.SimulationId = alp13.SimulationId and alp1.Scenario = alp13.Scenario");
                //sb.AppendLine(" where ISNULL(alp1.month, alp13.month) >= '" + stAsOfDate + "' and ISNULL(alp1.month, alp13.month) <= '" + endAsOfDate + "' and ISNULL(alp1.Scenario, alp13.Scenario) = '" + stScenario + "' and ISNULL(alp1.SimulationId, alp13.SimulationId) = " + stSimulationID + " ) as alps");
                //sb.AppendLine(" on alb.code = alps.Code and alb.SimulationId = alps.SimulationId");
                //sb.AppendLine("where ");
                //sb.AppendLine("alb.Cat_Type in(0,1,5) and alb.Exclude = 0) as alb_alp");
                //sb.AppendLine("on lb.Acc_Type = alb_alp.Acc_TypeOR and lb.RbcTier = alb_alp.ClassOR and alb_alp.IsAsset = lb.IsAsset");
                //sb.AppendLine("group by LB.title, LB.Priority, lb.IsAsset, lb.RowType, lb.Id");
                //sb.AppendLine("order by lb.isAsset desc,lb.Priority");
                sb.AppendLine("select ");
                sb.AppendLine("isnull(lb.title, 'Blank') as title");
                sb.AppendLine(",lb.RowType");
                sb.AppendLine(",lb.Id");
                sb.AppendLine(",lb.IsAsset");
                sb.AppendLine(",lb.Priority");
                sb.AppendLine(",ISNULL( sum(alb_alp." + (taxEquiv ? "AveTEAmt" : "IETotAmount") + "), 0 ) as incExp -- this field changes based on tax Equiv or not");
                sb.AppendLine(",ISNULL(sum(alb_alp.AveBal / " + days + "), 0) as AvgBal");
                sb.AppendLine("from");
                sb.AppendLine("(select rowitems.id, title, Acc_Type, RbcTier, RowType, rowitems.IsAsset, Priority from ATLAS_LBCustomLayoutRowItem as rowitems left join ATLAS_LBCustomLayoutClassifications as classifications on classifications.LBCustomLayoutRowItemId = rowitems.id");
                sb.AppendLine("where rowitems.LBCustomLayoutId = " + layoutId + ") as lb");
                sb.AppendLine("left join ");
                sb.AppendLine("ATLAS_Classification as c on lb.Acc_Type = c.AccountTypeId and lb.RbcTier = c.RbcTier and c.IsAsset = lb.IsAsset");
                sb.AppendLine("left join");
                sb.AppendLine("(select ");
                sb.AppendLine("IETotAmount, ");
                sb.AppendLine("AveTEAmt, ");
                sb.AppendLine("Acc_TypeOR, ");
                sb.AppendLine("ClassOR, ");
                sb.AppendLine("AveBal * datepart(DAY, EOMONTH(alp1Month)) as AveBal,");
                sb.AppendLine("");
                sb.AppendLine("IsAsset,                ");
                sb.AppendLine("alb.code,");
                sb.AppendLine("alb.name,");
                sb.AppendLine("alp13Month as alp13Month,");
                sb.AppendLine("alp1Month as alp1Month");
                sb.AppendLine("from basis_alb as alb ");
                sb.AppendLine("inner join ");
                sb.AppendLine("(select ");
                sb.AppendLine("       ISNULL(alp1.code, alp13.code) as code,");
                sb.AppendLine("       ISNULL(alp1.SimulationId, alp13.SimulationId) as SimulationId, ");
                sb.AppendLine("       IETotAmount, ");
                sb.AppendLine("       AveTEAmt, ");
                sb.AppendLine("       AveBal,");
                sb.AppendLine("       alp1.month as alp1Month, ");
                sb.AppendLine("       alp13.month as alp13Month ");
                sb.AppendLine("       from ");
                sb.AppendLine("       (SELECT code, simulationId, scenario, month, AveBal  FROM basis_alp1 WHERE simulationId = " + stSimulationID + " AND scenario = '" + stScenario + "' AND month >= '" + stAsOfDate + "' AND month <= '" + endAsOfDate + "') as alp1 full outer join ");
                sb.AppendLine("       (SELECT code, simulationId, scenario, month, AveTEAmt, IETotAmount  FROM basis_alp13 WHERE simulationId = " + stSimulationID + " AND scenario = '" + stScenario + "' AND month >= '" + stAsOfDate + "' AND month <= '" + endAsOfDate + "') as alp13");
                sb.AppendLine("                on alp1.code = alp13.code and alp1.month = alp13.month and alp1.SimulationId = alp13.SimulationId and alp1.Scenario = alp13.Scenario");
                sb.AppendLine("where ISNULL(alp1.month, alp13.month) >= '" + stAsOfDate + "' and ISNULL(alp1.month, alp13.month) <= '" + endAsOfDate + "' and ISNULL(alp1.Scenario, alp13.Scenario) = '" + stScenario + "' and ISNULL(alp1.SimulationId, alp13.SimulationId) = " + stSimulationID + " ) as alps");
                sb.AppendLine("on alb.code = alps.Code and alb.SimulationId = alps.SimulationId");
                sb.AppendLine("where ");
                sb.AppendLine("alb.Cat_Type in(0,1,5) and alb.Exclude = 0) as alb_alp");
                sb.AppendLine("on lb.Acc_Type = alb_alp.Acc_TypeOR and lb.RbcTier = alb_alp.ClassOR and alb_alp.IsAsset = lb.IsAsset");
                sb.AppendLine("group by LB.title, LB.Priority, lb.IsAsset, lb.RowType, lb.Id");
                sb.AppendLine("order by lb.isAsset desc,lb.Priority");

                string q = sb.ToString();
                retTable = Utilities.ExecuteSql(instService.ConnectionString(), sb.ToString());
                //return Utilities.ExecuteSql(instService.ConnectionString(), sb.ToString());
            }
            return new
            {
                errors = errors,
                warnings = warnings,
                retTable = retTable
            };
        }
    }
}