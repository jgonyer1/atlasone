﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;

using Atlas.Institution.Model.Atlas;

using Spire.Doc;

namespace Atlas.Web.Services
{
    public class FileProcessingService
    {
        private static readonly string __baseReportPath;

        private GlobalContextProvider _contextProvider;

        static FileProcessingService()
        {
            __baseReportPath = GetBasePath();
            SpireLicenseLoader.LoadLicense();
        }

        public static string GetBasePath()
        {
            string result = ConfigurationManager.AppSettings["BaseReportDirectory"];
            result = result ?? "C:\\GeneratedReportFiles";
            result = result.TrimEnd('\\');
            result += '\\';

            return result;
        }

        public FileProcessingService()
        {
            _contextProvider = new GlobalContextProvider("");
        }

        #region Core File Methods

        public FileSummary Upload(byte[] blobBytes, string name, string contentType, FileOwnerType ownerType, int ownerID, string tag, string databaseName = null)
        {
            if (!Enum.IsDefined(typeof(FileOwnerType), ownerType))
            {
                throw new ArgumentException("ownerType", "FileOwnerType " + ownerType + "is not defined");
            }

            if (ownerID < 1) throw new ArgumentException("ownerID", "Invalid owner id " + ownerID);

            GlobalContextProvider context = string.IsNullOrEmpty(databaseName) ? _contextProvider : new GlobalContextProvider(databaseName);

            FileSummary result = new FileSummary
            {
                FileID = Guid.NewGuid(),
                Size = blobBytes.Length,
                ContentType = contentType,
                UploadName = name,
                OwnerType = (int)ownerType,
                OwnerID = ownerID,
                Tag = tag,
                DateUploaded = DateTimeOffset.UtcNow,
                UploadedBy = Utilities.GetUserNameNoEmail()
            };

            DirectoryInfo di = new DirectoryInfo(getFileDirectoryName(result, databaseName));

            if (!di.Exists)
                di.Create();

            FileInfo fi = new FileInfo(di.FullName + getFileName(result));

            try
            {
                using (FileStream fs = fi.OpenWrite())
                {
                    fs.Write(blobBytes, 0, blobBytes.Length);
                }

                context.Context.FileSummaries.Add(result);
                context.Context.SaveChanges();
            }
            catch
            {
                fi.Delete();
                throw;
            }

            return result;
        }

        public IReadOnlyCollection<FileSummary> GetByOwner(FileOwnerType ownerType, int ownerID, string tag = null, string databaseName = null)
        {
            if (!Enum.IsDefined(typeof(FileOwnerType), ownerType)) throw new ArgumentException("ownerType", "FileOwnerType " + ownerType + " is not defined");

            if (ownerID < 1)
            {
                throw new ArgumentException("ownerID", "Invalid owner id " + ownerID);
            }

            GlobalContextProvider context = string.IsNullOrEmpty(databaseName)
                ? _contextProvider
                : new GlobalContextProvider(databaseName);

            IQueryable<FileSummary> query = context.Context.FileSummaries.Where(fs => fs.OwnerType == (int)ownerType && fs.OwnerID == ownerID);
            
            if(! string.IsNullOrWhiteSpace(tag))
            {
                query = query.Where(fs => fs.Tag == tag);
            }
            
            return query.ToArray();
        }

        public IReadOnlyCollection<FileSummary> CopyByOwner(FileOwnerType ownerType, int sourceOwnerID, int targetOwnerID, string sourceDatabaseName = null, string targetDatabaseName = null)
        {
            List<FileSummary> results = new List<FileSummary>();

            foreach(FileSummary fileToCopy in GetByOwner(ownerType, sourceOwnerID, databaseName: sourceDatabaseName))
            {
                results.Add(Upload(getData(fileToCopy, sourceDatabaseName), fileToCopy.UploadName, fileToCopy.ContentType, ownerType, targetOwnerID, fileToCopy.Tag, targetDatabaseName));
            }

            return results;
        }

        public FileSummary Get(Guid id)
        {
            string ignored;
            return get(id, out ignored);
        }

        private FileSummary get(Guid id, out string databaseName)
        {
            if (id == Guid.Empty) throw new ArgumentException("id", "Invalid id");
            FileSummary result = _contextProvider.Context.FileSummaries.FirstOrDefault(fs => fs.FileID == id);

            if(! ReferenceEquals(null, result))
            {
                databaseName = _contextProvider.DatabaseName;
                return result;
            }

            databaseName = "DDW_Atlas_Templates";
            GlobalContextProvider templateGtx = new GlobalContextProvider(databaseName);
            return templateGtx.Context.FileSummaries.FirstOrDefault(fs => fs.FileID == id);
        }

        public FileSummary GetWithData(Guid id, out byte[] fileBlob)
        {
            string databaseName;
            FileSummary result = get(id, out databaseName);

            if(result == null)
            {
                fileBlob = new byte[0];
                return null;
            }

            fileBlob = getData(result, databaseName);

            return result;
        }

        private byte[] getData(FileSummary fileSummary, string databaseName)
        {
            FileInfo fi = new FileInfo(getFileDirectoryName(fileSummary, databaseName) + getFileName(fileSummary));

            if (!fi.Exists)
            {
                return new byte[0];
            }

            byte[] result = new byte[fi.Length];

            using (FileStream fs = fi.OpenRead())
            {
                fs.Read(result, 0, result.Length);
            }

            return result;
        }

        public static string GetData(Guid id)
        {
            throw new NotImplementedException("todo jdk");
        }

        public void Delete(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentException("id", "Invalid id");

            FileSummary fileToDelete = Get(id);

            if (fileToDelete == null) return;

            _contextProvider.Context.FileSummaries.Remove(fileToDelete);

            FileInfo fi = new FileInfo(getFileDirectoryName(fileToDelete) + getFileName(fileToDelete));

            if (fi.Exists) fi.Delete();

            _contextProvider.Context.SaveChanges();

            cleanUpDirectory((FileOwnerType) fileToDelete.OwnerType, fileToDelete.OwnerID);
        }

        public void Delete(FileOwnerType ownerType, int ownerID)
        {
            if (!Enum.IsDefined(typeof(FileOwnerType), ownerType))
            {
                throw new ArgumentException("ownerType", "FileOwnerType " + ownerType + " is not defined");
            }

            if (ownerID < 1) throw new ArgumentException("ownerID", "Invalid owner id " + ownerID);

            IReadOnlyCollection<FileSummary> filesToDelete = GetByOwner(ownerType, ownerID);

            if (!filesToDelete.Any()) return;

            _contextProvider.Context.FileSummaries.RemoveRange(filesToDelete);

            foreach (FileSummary fileToDelete in filesToDelete)
            {
                FileInfo fi = new FileInfo(getFileDirectoryName(fileToDelete) + getFileName(fileToDelete));

                if (!fi.Exists)
                    continue;

                fi.Delete();
            }

            _contextProvider.Context.SaveChanges();

            cleanUpDirectory(ownerType, ownerID);
        }

        #endregion Core File Methods

        #region Word File Methods

        public IReadOnlyCollection<FileSummary> UploadWord(byte[] blobBytes, string name, FileOwnerType ownerType, int ownerID, DocumentOrientation orientation)
        {
            Document wordDocument;

            using (MemoryStream inStream = new MemoryStream())
            {
                inStream.Write(blobBytes, 0, blobBytes.Length);
                inStream.Seek(0, SeekOrigin.Begin);
                wordDocument = new Document(inStream);
            }

            List<FileSummary> results = new List<FileSummary>();

            foreach (DocumentOrientation saveOrientation in Enum.GetValues(typeof(DocumentOrientation)))
            {
                wordDocument.Sections[0].PageSetup.Orientation = saveOrientation == DocumentOrientation.Landscape
                    ? Spire.Doc.Documents.PageOrientation.Landscape
                    : Spire.Doc.Documents.PageOrientation.Portrait;

                using (MemoryStream outStream = new MemoryStream())
                {
                    wordDocument.SaveToStream(outStream, FileFormat.PDF);
                    results.Add(Upload(outStream.ToArray(), name, "application/pdf", ownerType, ownerID, saveOrientation.ToString()));
                }
            }

            return results;
        }

        #endregion Word File Methods

        #region Helpers

        private static void cleanUpDirectory(FileOwnerType ownerType, int ownerID)
        {
            DirectoryInfo di = new DirectoryInfo(getFileDirectoryName(ownerType, ownerID));

            if (!di.Exists) return;

            while(di.Parent != null && di.GetDirectories().Length == 0 && di.GetFiles().Length == 0)
            {
                di.Delete();
                di = di.Parent;
            }
        }

        private static string getFileDirectoryName(FileSummary fileSummary, string databaseName = null)
        {
            return getFileDirectoryName((FileOwnerType)fileSummary.OwnerType, fileSummary.OwnerID, databaseName);
        }

        private static string getFileDirectoryName(FileOwnerType ownerType, int ownerID, string databaseName = null)
        {
            DataTable userData = Utilities.GetAtlasUserData();

            if (userData.Rows.Count != 1) throw new DataException("Cannot obtain database name in current session");

            if (string.IsNullOrEmpty(databaseName))
            {
                databaseName = userData.Rows[0]["databaseName"].ToString();
            }

            return __baseReportPath + databaseName + "\\Files\\" + ownerType + "\\" + ownerID + "\\";
        }

        private static string getFileName(FileSummary fileSummary)
        {
            return fileSummary.FileID + "." + extensionFromContentType(fileSummary.ContentType);
        }

        private static string extensionFromContentType(string contentType)
        {
            switch(contentType)
            {
                case "application/pdf":
                    return "pdf";

                case "image/png":
                    return "png";

                case "image/jpeg":
                    return "jpg";

                default:
                    return "dat";
            }
        }

        #endregion Helpers
    }
}