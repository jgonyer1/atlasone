﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Institution.Data;
using Atlas.Institution.Model;
using Atlas.Web.ViewModels;
using System.Data;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using P360.Web.Controllers;

namespace Atlas.Web.Services
{
    public class ChartSeries
    {
        public string name;
        public double[] data;

        public ChartSeries(string _name, int dataLen)
        {
            name = _name;
            data = new double[dataLen];
        }
    }

    public class LiquidityProjectionService
    {
        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();
        public double GetTier2BasicSuplus(string bsaId, string conStr)
        {
            string qry = "SELECT [MidS1BasicSurplusFHLBAmount] from ATLAS_BasicSurplusAdmin where Id =  " + bsaId;
            DataTable dt = Utilities.ExecuteSql(conStr, qry);
            if (dt.Rows.Count > 0)
            {
                return Double.Parse(dt.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }
        public object LiquidityProjectionViewModel(int reportId)
        {
            InstitutionContext gctx = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            GlobalController gc = new GlobalController();
            var lq = gctx.LiquidityProjections.Where(s => s.Id == reportId).FirstOrDefault();
            var rep = gctx.Reports.Where(s => s.Id == reportId).FirstOrDefault();
            List<LiquidityProjectionDateOffset> offsets = lq.LiquidityProjectionDateOffsets.OrderBy(l => l.Priority).ToList();
            InstitutionService instServ = new InstitutionService(lq.InstitutionDatabaseName);
            List<object> curOffsetSimulationScenarios = new List<object>();
            List<object> prevOffsetSimulationScenarios = new List<object>();

            DataTable topLeftTable = new DataTable();
            topLeftTable.Columns.Add("name", typeof(string));

            //field not show, just put in here whatever classes you want to be applied to each row of the table
            topLeftTable.Columns.Add("classes", typeof(string));
            topLeftTable.Columns.Add("tier2BS", typeof(double));
            topLeftTable.Columns.Add("newLoans", typeof(double));
            topLeftTable.Columns.Add("coreDep", typeof(double));
            topLeftTable.Columns.Add("nonPledge", typeof(double));
            topLeftTable.Columns.Add("projBS", typeof(double));

            string[] topRightChartCategories = new string[offsets.Count];
            string topRight_leftChartName = "Net New Loan Fundings";
            ChartSeries topRight_leftChartSeries1 = new ChartSeries("Actual", offsets.Count);
            ChartSeries topRight_leftChartSeries2 = new ChartSeries("Forecast", offsets.Count);

            string topRight_rightChartName = "Net Core Deposit Flows*";
            ChartSeries topRight_rightChartSeries1 = new ChartSeries("Actual", offsets.Count);
            ChartSeries topRight_rightChartSeries2 = new ChartSeries("Forecast", offsets.Count);

            string middleSectionTitle = "24 Month Cash Flow Projections";
            string[] middleChartCategories = new string[8];
            ChartSeries middle_leftChartSeries1 = new ChartSeries("Asset", 8);
            ChartSeries middle_leftChartSeries2 = new ChartSeries("Liability", 8);

            ChartSeries middle_rightChartSeries1 = new ChartSeries("Net", 8);
            ChartSeries middle_rightChartSeries2 = new ChartSeries("Cumulative Net", 8);

            DataTable bottomCFTableAssets = new DataTable();
            DataTable bottomCFTableLiabs = new DataTable();

            if (lq == null)
            {
                throw new Exception("Liquidty Projection Not Found");
            }
            var instService = new InstitutionService(lq.InstitutionDatabaseName);
            string conStr = Utilities.BuildInstConnectionString(lq.InstitutionDatabaseName);
            DataTable aods = gc.AsOfDates(lq.InstitutionDatabaseName, true, 8, false);

            if (aods.Rows.Count == 0)
            {
                throw new Exception("No As Of Dates Found");
            }

            DataTable curModelSetupVals = new DataTable();
            DataTable prevModelSetupVals = new DataTable();
            LiquidityProjectionDateOffset liqOffset;
            string curOffsetAod = "";
            string prevOffsetAod = "";
            DataTable tempPolicyData = new DataTable();
            int curNiiSimulationId = -1;
            int prevNiiSimulationId = -1;

            string policyQry = "SELECT NIISimulationTypeId, BasicSurplusAdminId FROM Atlas_Policy WHERE AsOfDate = ";

            //for each date offset get the  Tier 2 Basic Suplus, Net New Loan Fundings, Net Core Deposit Flows, Non-Pledgable Investment Cash flows
            //projected basic surplus (total) will be calculated off of all of these
            //these directly correspond to numbers in the upper left table of the report
            //the charts in the upper right use Net New Loan Fundings and Net Core Deposit Flows
            double[][] curTopVals = new double[lq.LiquidityProjectionDateOffsets.Count][];
            double[][] prevTopVals = new double[lq.LiquidityProjectionDateOffsets.Count][];

            for (int o = 0; o < offsets.Count; o++)
            {
                liqOffset = offsets[o];
                curOffsetAod = aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString();
                prevOffsetAod = aods.Rows[liqOffset.PrevOffSet]["asOfDateDescript"].ToString();
                DateTime curOffsetAodDT = DateTime.Parse(curOffsetAod);
                DateTime prevOffsetAodDT = DateTime.Parse(prevOffsetAod);

                curTopVals[o] = new double[4];
                prevTopVals[o] = new double[4];

                if(liqOffset.Override){
                    curNiiSimulationId = liqOffset.CurNIISimulationTypeId;
                    prevNiiSimulationId = liqOffset.PrevNIISimulationTypeId;

                    curTopVals[o][0] = GetTier2BasicSuplus(liqOffset.CurBasicSurplusAdminId.ToString(), conStr);
                    prevTopVals[o][0] = GetTier2BasicSuplus(liqOffset.PrevBasicSurplusAdminId.ToString(), conStr);
                }
                else
                {
                    tempPolicyData = Utilities.ExecuteSql(conStr, policyQry + "'" + curOffsetAod + "'");
                    if (tempPolicyData.Rows.Count == 0)
                    {
                        curNiiSimulationId = liqOffset.CurNIISimulationTypeId;
                        curTopVals[o][0] = GetTier2BasicSuplus(liqOffset.CurBasicSurplusAdminId.ToString(), conStr);
                    }
                    else
                    {
                        curNiiSimulationId = Convert.ToInt32(tempPolicyData.Rows[0]["NIISimulationTypeId"].ToString());
                        curTopVals[o][0] = GetTier2BasicSuplus(tempPolicyData.Rows[0]["BasicSurplusAdminId"].ToString(), conStr);
                    }
                    tempPolicyData = Utilities.ExecuteSql(conStr,  policyQry + "'" + prevOffsetAod + "'");
                    if (tempPolicyData.Rows.Count == 0)
                    {
                        prevNiiSimulationId = liqOffset.PrevNIISimulationTypeId;
                        prevTopVals[o][0] = GetTier2BasicSuplus(liqOffset.PrevBasicSurplusAdminId.ToString(), conStr);
                    }
                    else
                    {
                        prevNiiSimulationId = Convert.ToInt32(tempPolicyData.Rows[0]["NIISimulationTypeId"].ToString());
                        prevTopVals[o][0] = GetTier2BasicSuplus(tempPolicyData.Rows[0]["BasicSurplusAdminId"].ToString(), conStr);
                    }
                }
                //REVERSE
                topRightChartCategories[topRightChartCategories.Length - 1 - o] = prevOffsetAodDT.ToString("MM/yy") + "-" + curOffsetAodDT.ToString("MM/yy");
                //topRightChartCategories[o] = prevOffsetAodDT.ToString("MM/yy") + "-" + curOffsetAodDT.ToString("MM/yy");

                curOffsetSimulationScenarios.Add(instServ.GetSimulationScenario(rep.Package.AsOfDate, liqOffset.CurOffSet, curNiiSimulationId, liqOffset.CurScenarioTypeId));
                prevOffsetSimulationScenarios.Add(instServ.GetSimulationScenario(rep.Package.AsOfDate, liqOffset.PrevOffSet, prevNiiSimulationId, liqOffset.PrevScenarioTypeId));

                curModelSetupVals = Utilities.ExecuteSql(conStr, "SELECT NetNewLoans, NetNewDeposits FROM ATLAS_ModelSetup WHERE asofdate = '" + curOffsetAod + "'");
                prevModelSetupVals = Utilities.ExecuteSql(conStr, "SELECT NetNewLoans, NetNewDeposits FROM ATLAS_ModelSetup WHERE asofdate = '" + prevOffsetAod + "'");

                //tier 2 basic surplus gets handled above with the other policy related stuff
                
                if (curModelSetupVals.Rows.Count > 0)
                {
                    curTopVals[o][1] = Double.Parse(curModelSetupVals.Rows[0][0].ToString());//new loans
                    curTopVals[o][2] = Double.Parse(curModelSetupVals.Rows[0][1].ToString());//new deps
                }
                else
                {
                    curTopVals[o][1] = 0;
                    curTopVals[o][2] = 0;
                }
                if (prevModelSetupVals.Rows.Count > 0)
                {
                    prevTopVals[o][1] = Double.Parse(prevModelSetupVals.Rows[0][0].ToString()) ;//new loans
                    prevTopVals[o][2] = Double.Parse(prevModelSetupVals.Rows[0][1].ToString()) ;//new dpes
                }
                else
                {
                    prevTopVals[o][1] = 0;
                    prevTopVals[o][2] = 0;
                }


                curTopVals[o][3] = NonPledgeAmount(curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), curOffsetSimulationScenarios[o].GetDynamicProperty("scenario").ToString(),
                    curOffsetSimulationScenarios[o].GetDynamicProperty("asOfDate").ToString(), lq.NonPledgeGroupOption, conStr, lq.ProjectionPeriod);
                prevTopVals[o][3] = NonPledgeAmount(prevOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[o].GetDynamicProperty("scenario").ToString(),
                    prevOffsetSimulationScenarios[o].GetDynamicProperty("asOfDate").ToString(), lq.NonPledgeGroupOption, conStr, lq.ProjectionPeriod);

                //forecast chart series
                //NEED TO REVERSE THESE
                topRight_leftChartSeries1.data[topRight_leftChartSeries1.data.Length - 1 - o] = ActualLoans(curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), conStr,
                   Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString()), Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.PrevOffSet]["asOfDateDescript"].ToString())) * 1000;
                topRight_leftChartSeries2.data[topRight_leftChartSeries2.data.Length - 1 - o] = prevTopVals[o][1] * 1000;
                topRight_rightChartSeries1.data[topRight_rightChartSeries1.data.Length - 1 - o] = ActualDeposits(curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), conStr,
                    lq.IncludePublicDep,
                    lq.IncludeNationalDep,
                    lq.IncludeBrokeredDep,
                    lq.IncludeBrokeredRecip,
                    lq.IncludeBrokeredOneWay,
                    lq.IncludeSecuredRetail,
                    Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString()), Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.PrevOffSet]["asOfDateDescript"].ToString())) * 1000;
                topRight_rightChartSeries2.data[topRight_rightChartSeries2.data.Length - 1 - o] = prevTopVals[o][2] * 1000;
                //topRight_leftChartSeries1.data[o] = ActualLoans(curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), conStr,
                //    Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString()), Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.PrevOffSet]["asOfDateDescript"].ToString()));
                //topRight_leftChartSeries2.data[o] = prevTopVals[o][1];
                //topRight_rightChartSeries1.data[o] = ActualDeposits(curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(), conStr,
                //    lq.IncludePublicDep,
                //    lq.IncludeNationalDep,
                //    lq.IncludeBrokeredDep,
                //    lq.IncludeBrokeredRecip,
                //    lq.IncludeBrokeredOneWay,
                //    lq.IncludeSecuredRetail,
                //    Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString()), Utilities.GetFirstOfMonthStr(aods.Rows[liqOffset.PrevOffSet]["asOfDateDescript"].ToString()));
                //topRight_rightChartSeries2.data[o] = prevTopVals[o][2];

                

                if (o == 0)
                {
                    bottomCFTableAssets = CashflowTable(aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString(),
                        curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(),
                        curOffsetSimulationScenarios[o].GetDynamicProperty("scenario").ToString(),
                        lq.ViewOption, conStr, "1");
                    bottomCFTableLiabs = CashflowTable(aods.Rows[liqOffset.CurOffSet]["asOfDateDescript"].ToString(),
                        curOffsetSimulationScenarios[o].GetDynamicProperty("simulation").ToString(),
                        curOffsetSimulationScenarios[o].GetDynamicProperty("scenario").ToString(),
                        lq.ViewOption, conStr, "0");

                    bottomCFTableAssets = Utilities.GetInversedDataTable(bottomCFTableAssets, "qtrs", "Name", "cashFlow", "", true, "Total Asset Cash Flow");
                    bottomCFTableLiabs = Utilities.GetInversedDataTable(bottomCFTableLiabs, "qtrs", "Name", "cashFlow", "", true, "Total Liability Cash Flow");
                    bottomCFTableAssets.Columns.Add("classes", typeof(string));
                    bottomCFTableLiabs.Columns.Add("classes", typeof(string));

                    bottomCFTableAssets.Rows[bottomCFTableAssets.Rows.Count - 1]["classes"] = "shaded-row";
                    bottomCFTableLiabs.Rows[bottomCFTableLiabs.Rows.Count - 1]["classes"] = "shaded-row";

                    decimal outA;
                    decimal outB;
                    bool succA;
                    bool succB;
                    DataRow netCFRow = bottomCFTableLiabs.NewRow();
                    netCFRow["Name"] = "Net Cash Flow";
                    netCFRow["classes"] = "net-row";
                    foreach (DataColumn c in bottomCFTableAssets.Columns)
                    {
                        if (c.ColumnName != "Name" && c.ColumnName != "classes")
                        {
                            if (netCFRow[c.ColumnName] == DBNull.Value)
                            {
                                netCFRow[c.ColumnName] = 0;
                            }
                            netCFRow[c.ColumnName] = Convert.ToDecimal(netCFRow[c.ColumnName].ToString()) + Convert.ToDecimal(bottomCFTableAssets.Rows[bottomCFTableAssets.Rows.Count - 1][c.ColumnName].ToString());
                        }
                    }
                    foreach (DataColumn c in bottomCFTableLiabs.Columns)
                    {
                        if (c.ColumnName != "Name" && c.ColumnName != "classes")
                        {
                            if (netCFRow[c.ColumnName] == DBNull.Value)
                            {
                                netCFRow[c.ColumnName] = 0;
                            }
                            netCFRow[c.ColumnName] = Convert.ToDecimal(netCFRow[c.ColumnName].ToString()) - Convert.ToDecimal(bottomCFTableLiabs.Rows[bottomCFTableLiabs.Rows.Count - 1][c.ColumnName].ToString());
                        }
                    }
                    bottomCFTableLiabs.Rows.Add(netCFRow);

                    //cumulative cash flow
                    DataRow cumCFRow = bottomCFTableLiabs.NewRow();
                    cumCFRow["Name"] = "Cumulative Net Cash Flow";
                    cumCFRow["classes"] = "net-row";
                    for (int c = 0; c < bottomCFTableLiabs.Columns.Count - 1; c++)
                    {
                        DataColumn col = bottomCFTableLiabs.Columns[c];
                        if (c > 0)
                        {
                            succA = Decimal.TryParse(netCFRow[col.ColumnName].ToString(), out outA);
                            succB = Decimal.TryParse(cumCFRow[bottomCFTableLiabs.Columns[c - 1].ColumnName].ToString(), out outB);
                            if (!succA)
                                outA = 0;
                            if (!succB)
                                outB = 0;
                            cumCFRow[col.ColumnName] = outA + outB;
                        }
                    }
                    bottomCFTableLiabs.Rows.Add(cumCFRow);

                    //chart series
                    //we don't want the net cash flow row in this series
                    //int count = 0;
                    for (int c = 1; c < bottomCFTableAssets.Columns.Count - 1; c++)
                    {
                        middleChartCategories[c - 1] = bottomCFTableAssets.Columns[c].ColumnName;
                        middle_leftChartSeries1.data[c - 1] = Convert.ToDouble(bottomCFTableAssets.Rows[bottomCFTableAssets.Rows.Count - 1][c].ToString());
                        middle_leftChartSeries2.data[c - 1] = Convert.ToDouble(bottomCFTableLiabs.Rows[bottomCFTableLiabs.Rows.Count - 3][c].ToString());

                        middle_rightChartSeries1.data[c - 1] = Convert.ToDouble(bottomCFTableLiabs.Rows[bottomCFTableLiabs.Rows.Count - 2][c].ToString());
                        middle_rightChartSeries2.data[c - 1] = Convert.ToDouble(bottomCFTableLiabs.Rows[bottomCFTableLiabs.Rows.Count - 1][c].ToString());
                    }
                }
            }

            //double prevTier2;
            //actual tier 2 is the same as prev tier2

            //double prevNewLoans;
            double actualNewLoans;
            double varNewLoans;
            //double prevCoreDep;
            double actualCoreDep;
            double varCoreDep;
            double prevProjBs;
            double actualProjBs;
            double varProjBs;

            //these Actual methods handle the cur date - prev date math
            actualNewLoans = ActualLoans(curOffsetSimulationScenarios[0].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[0].GetDynamicProperty("simulation").ToString(), conStr, Utilities.GetFirstOfMonthStr(aods.Rows[offsets[0].CurOffSet]["asOfDateDescript"].ToString()), Utilities.GetFirstOfMonthStr(aods.Rows[offsets[0].PrevOffSet]["asOfDateDescript"].ToString()));

            actualCoreDep = ActualDeposits(curOffsetSimulationScenarios[0].GetDynamicProperty("simulation").ToString(), prevOffsetSimulationScenarios[0].GetDynamicProperty("simulation").ToString(), conStr,
                lq.IncludePublicDep,
                lq.IncludeNationalDep,
                lq.IncludeBrokeredDep,
                lq.IncludeBrokeredRecip,
                lq.IncludeBrokeredOneWay,
                lq.IncludeSecuredRetail,
                Utilities.GetFirstOfMonthStr(aods.Rows[offsets[0].CurOffSet]["asOfDateDescript"].ToString()), Utilities.GetFirstOfMonthStr(aods.Rows[offsets[0].PrevOffSet]["asOfDateDescript"].ToString()));
            prevProjBs = prevTopVals[0][0] - prevTopVals[0][1] + prevTopVals[0][2] + prevTopVals[0][3];  //TODO : add in non pledge
            actualProjBs = prevTopVals[0][0] - actualNewLoans + actualCoreDep + prevTopVals[0][3];//TODO: add in non pledge

            varNewLoans = actualNewLoans - prevTopVals[0][1];
            varCoreDep = actualCoreDep - prevTopVals[0][2];
            varProjBs = actualProjBs - prevProjBs;

            //current
            DataRow newRow = topLeftTable.NewRow();

            //if first offset aod is not package asofdate , put the specific asofdate in the title
            if (rep.Package.AsOfDate.Equals(DateTime.Parse(aods.Rows[offsets[0].CurOffSet]["asOfDateDescript"].ToString())))
            {
                newRow["Name"] = "CURRENT FORECAST";
            }
            else
            {
                newRow["Name"] = aods.Rows[offsets[0].CurOffSet]["asOfDateDescript"].ToString() + " FORECAST";
            }
            newRow["classes"] = "shaded-row";
            newRow["tier2BS"] = curTopVals[0][0];
            newRow["newLoans"] = curTopVals[0][1];
            newRow["coreDep"] = curTopVals[0][2];
            newRow["nonPledge"] = curTopVals[0][3];
            newRow["projBS"] = curTopVals[0][0] - curTopVals[0][1] + curTopVals[0][2] + curTopVals[0][3];
            topLeftTable.Rows.Add(newRow);

            newRow = topLeftTable.NewRow();
            newRow["Name"] = aods.Rows[offsets[0].PrevOffSet]["asOfDateDescript"].ToString() + " FORECAST";
            newRow["classes"] = "";
            newRow["tier2BS"] = prevTopVals[0][0];
            newRow["newLoans"] = prevTopVals[0][1];
            newRow["coreDep"] = prevTopVals[0][2];
            newRow["nonPledge"] = prevTopVals[0][3];
            newRow["projBS"] = prevProjBs;
            topLeftTable.Rows.Add(newRow);

            newRow = topLeftTable.NewRow();
            newRow["Name"] = "ACTUAL";
            newRow["classes"] = "";
            newRow["tier2BS"] = prevTopVals[0][0];
            newRow["newLoans"] = actualNewLoans;
            newRow["coreDep"] = actualCoreDep;
            newRow["nonPledge"] = prevTopVals[0][3];
            newRow["projBS"] = actualProjBs;
            topLeftTable.Rows.Add(newRow);

            newRow = topLeftTable.NewRow();
            newRow["Name"] = "FORECAST VARIANCE";
            newRow["classes"] = "";
            newRow["tier2BS"] = DBNull.Value;
            newRow["newLoans"] = varNewLoans;
            newRow["coreDep"] = varCoreDep;
            newRow["nonPledge"] = DBNull.Value;
            newRow["projBS"] = varProjBs;
            topLeftTable.Rows.Add(newRow);

            return new
            {
                topLeftTable = topLeftTable,
                topRightChartCategories = topRightChartCategories,
                topRight_leftChartName = topRight_leftChartName,
                topRightLeftChartSeriesActuals = topRight_leftChartSeries1,
                topRightLeftChartSeriesForecast = topRight_leftChartSeries2,
                topRight_rightChartName = topRight_rightChartName,
                topRightRightChartSeriesActuals = topRight_rightChartSeries1,
                topRightRightChartSeriesForecast = topRight_rightChartSeries2,
                middleSectionTitle = middleSectionTitle,
                middleChartCategories = middleChartCategories,
                middle_leftChartSeries1 = middle_leftChartSeries1,
                middle_leftChartSeries2 = middle_leftChartSeries2,
                middle_rightChartSeries1 = middle_rightChartSeries1,
                middle_rightChartSeries2 = middle_rightChartSeries2,
                liqProjSettings = lq,
                bottomCFTableAssets = bottomCFTableAssets,
                bottomCFTableLiabs = bottomCFTableLiabs
            };

            /*  

              //Look Up Funding Matrix Settings
              
              //If Not Found Kick Out
              

              

              //Get Info for Simulation and Scenario For Both Offsets
              var curInfo = instService.GetSimulationScenario(rep.Package.AsOfDate, lq.AsOfDateOffset, lq.SimulationTypeId, lq.ScenarioTypeId);
              var priorInfo = instService.GetSimulationScenario(rep.Package.AsOfDate, lq.PriorDateOffset, lq.SimulationTypeId, lq.ScenarioTypeId);

              string curSimulationId = curInfo.GetType().GetProperty("simulation").GetValue(curInfo).ToString();
              string curScenario = curInfo.GetType().GetProperty("scenario").GetValue(curInfo).ToString();
              string curAsOfDate = curInfo.GetType().GetProperty("asOfDate").GetValue(curInfo).ToString();

              string priorSimulationId = priorInfo.GetType().GetProperty("simulation").GetValue(priorInfo).ToString();
              string priorScenario = priorInfo.GetType().GetProperty("scenario").GetValue(priorInfo).ToString();
              string priorAsOfDate = priorInfo.GetType().GetProperty("asOfDate").GetValue(priorInfo).ToString();

              if (curSimulationId == "" || curScenario == "" || curAsOfDate == "") {
                  errors.Add(String.Format(Utilities.GetErrorTemplate(1), lq.ScenarioType.Name, lq.SimulationType.Name, lq.AsOfDateOffset, rep.Package.AsOfDate.ToShortDateString()));
              }
              else if (priorSimulationId == "" || priorScenario == "" || priorAsOfDate == "") {
                  errors.Add(String.Format(Utilities.GetErrorTemplate(1), lq.ScenarioType.Name, lq.SimulationType.Name, lq.PriorDateOffset, rep.Package.AsOfDate.ToShortDateString()));
              }
              else {
                  string priorFirstDay = new DateTime(DateTime.Parse(priorAsOfDate).Year, DateTime.Parse(priorAsOfDate).Month, 1).ToShortDateString();
                  string curFirstDay = new DateTime(DateTime.Parse(curAsOfDate).Year, DateTime.Parse(curAsOfDate).Month, 1).ToShortDateString();


                  //Get Basic Surplus Data
                  StringBuilder sqlQuery = new StringBuilder();
                  sqlQuery.AppendLine(" SELECT ");
                  sqlQuery.AppendLine(" 	aod.asOfDate ");
                  sqlQuery.AppendLine(" 	,bs.[MidS1BasicSurplusFHLBAmount] as FHLB ");
                  sqlQuery.AppendLine("  FROM ATLAS_BasicSurplusAdmin as bs ");
                  sqlQuery.AppendLine(" INNER JOIN AsOfDateInfo as aod ");
                  sqlQuery.AppendLine(" ON aod.Id = bs.informationId ");
                  sqlQuery.AppendLine(" WHERE name = '" + gctx.BasicSurplusAdmins.Where(s => s.Id == lq.BasicSurplusId).FirstOrDefault().Name + "' AND aod.asOfDate in ('" + curAsOfDate + "','" + priorAsOfDate + "') ");
                  DataTable bs = Utilities.ExecuteSql(conStr, sqlQuery.ToString());


                  decimal currentFHLB = gctx.BasicSurplusAdmins.Where(s => s.Id == lq.BasicSurplusId).FirstOrDefault().MidS1BasicSurplusFHLBAmount;
                  decimal priorFHLB = gctx.BasicSurplusAdmins.Where(s => s.Id == lq.BasicSurplusOffsetId).FirstOrDefault().MidS1BasicSurplusFHLBAmount;
                  decimal actualFHLB = priorFHLB;



                  //Net New Loans And Deposits
                  DataTable mds = Utilities.ExecuteSql(conStr, "SELECT netNewLoans, netNewDeposits, asOfdate FROM ATLAS_ModelSetUp WHERE asOfDate in ('" + curAsOfDate + "','" + priorAsOfDate + "')");
                  double currentLoans = 0;
                  double priorLoans = 0;
                  double currentDeposits = 0;
                  double priorDeposits = 0;
                  foreach (DataRow dr in mds.Rows) {
                      if (DateTime.Parse(dr["asOfDate"].ToString()) == DateTime.Parse(curAsOfDate)) {
                          currentLoans = double.Parse(dr["netNewLoans"].ToString());
                          currentDeposits = double.Parse(dr["netNewDeposits"].ToString());
                      }
                      else {
                          priorLoans = double.Parse(dr["netNewLoans"].ToString());
                          priorDeposits = double.Parse(dr["netNewDeposits"].ToString());
                      }
                  }

                  //Actual Loans and Deposits

                  List<int> nonMats = new List<int>();
                  List<int> timeDeps = new List<int>();
                  List<int> secured = new List<int>();

                  //Default Non Maturitues
                  nonMats.Add(0);
                  nonMats.Add(1);
                  nonMats.Add(3);
                  nonMats.Add(5);
                  nonMats.Add(7);
                  nonMats.Add(9);
                  nonMats.Add(10);
                  nonMats.Add(11);
                  nonMats.Add(12);

                  //Default Time Deposits
                  timeDeps.Add(0);
                  timeDeps.Add(1);
                  timeDeps.Add(3);
                  timeDeps.Add(7);
                  timeDeps.Add(8);

                  if (lq.BrokeredRecip) {
                      nonMats.Add(15);
                      timeDeps.Add(9);
                  }

                  if (lq.BrokeredDep) {
                      nonMats.Add(16);
                      nonMats.Add(13);
                      nonMats.Add(14);
                      timeDeps.Add(10);
                      timeDeps.Add(5);
                  }

                  if (lq.NationalDep) {
                      timeDeps.Add(6);
                  }

                  if (lq.SecuredRetail) {
                      secured.Add(3);
                  }

                  if (lq.PublicDep) {
                      nonMats.Add(2);
                      nonMats.Add(4);
                      nonMats.Add(6);
                      nonMats.Add(8);
                      timeDeps.Add(2);
                      timeDeps.Add(4);
                  }


                  double actualLoans = ActualLoans(curSimulationId, priorSimulationId, conStr, curFirstDay, priorFirstDay);
                  double actualDeposits = ActualDeposits(curSimulationId, priorSimulationId, conStr, nonMats, timeDeps, secured, curFirstDay, priorFirstDay);
                  double currentNonPledge = NonPledgeAmount(curSimulationId, curScenario, curAsOfDate, lq.NonPledgeDefinedGroup, conStr, lq.ReportType);
                  double priorNonPledge = NonPledgeAmount(priorSimulationId, priorScenario, priorAsOfDate, lq.NonPledgeDefinedGroup, conStr, lq.ReportType);


                  DataTable assets = CashflowTable(curAsOfDate, curSimulationId, curScenario, lq.AssetDefinedGroup, conStr, "1");

                  //always going to use the AssetDefinedGroup
                  DataTable liabilities = CashflowTable(curAsOfDate, curSimulationId, curScenario, lq.AssetDefinedGroup, conStr, "0");
                  return new
                  {
                      currentFHLB = currentFHLB,
                      priorFHLB = priorFHLB,
                      actualFHLB = actualFHLB,
                      currentLoans = currentLoans,
                      priorLoans = priorLoans,
                      currentDeposits = currentDeposits,
                      priorDeposits = priorDeposits,
                      actualLoans = actualLoans,
                      actualDeposits = actualDeposits,
                      currentNonPledge = currentNonPledge,
                      priorNonPledge = priorNonPledge,
                      actualNonPledge = priorNonPledge,
                      assets = assets,
                      liabilities = liabilities,
                      reportType = lq.ReportType,
                      scenarioName = lq.ScenarioType.Name,
                      warnings = warnings,
                      errors = errors
                  };
              }

              return new
              {
                  warnings = warnings,
                  errors = errors
              };*/
        }


        //public double ActualDeposits(string curSim, string priorSim, string conStr, List<int> nonMats, List<int> timeDeps, List<int> secured, string curDate, string priorDate)
        public double ActualDeposits(string curSim, string priorSim, string conStr, bool includePublicDep, bool includeNationalDep, bool includeBrokeredDep, bool includeBrokeredRecip, bool includeBrokeredOneWay, bool includeSecuredRetail, string curDate, string priorDate)
        {
            List<int> nonMats = new List<int>(new int[] { 0, 1, 3, 5, 7, 9, 10, 11, 12 });
            List<int> timeDeps = new List<int>(new int[] { 0, 1, 3, 7, 8 });
            List<int> secured = new List<int>();

            if (includeBrokeredRecip)
            {
                nonMats.Add(15);
                timeDeps.Add(9);
            }

            if (includeBrokeredDep)
            {
                //nonMats.Add(16);
                nonMats.Add(13);
                nonMats.Add(14);
                timeDeps.Add(10);
                timeDeps.Add(5);
            }

            if (includeNationalDep)
            {
                timeDeps.Add(6);
            }

            if (includeSecuredRetail)
            {
                secured.Add(3);
            }

            if (includePublicDep)
            {
                nonMats.Add(2);
                nonMats.Add(4);
                nonMats.Add(6);
                nonMats.Add(8);
                timeDeps.Add(2);
                timeDeps.Add(4);
            }
            if (includeBrokeredOneWay)
            {
                nonMats.Add(16);
                timeDeps.Add(10);
            }

            StringBuilder sqlQuery = new StringBuilder();


            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	SUM(CASE WHEN ala.simulationId = {0} THEN End_Bal ELSE -End_Bal END) as actualLoans ");
            sqlQuery.AppendLine(" FROM ");
            sqlQuery.AppendLine(" (SELECT code, simulationId, End_Bal  FROM Basis_ALA WHERE (simulationid = {0} AND month = '{2}') OR (simulationid = '{1}' AND month = '{3}')) as ala INNER JOIN ");
            sqlQuery.AppendLine(" (SELECT code, simulationId FROM Basis_ALB WHERE simulationId in ({0},{1}) AND exclude = 0 ");
            sqlQuery.AppendLine(" AND ( 1= 2  ");
            if (nonMats.Count() > 0)
            {
                sqlQuery.AppendLine(" OR (acc_typeOr = 3 AND classOr in (");
                bool firstPass = true;
                foreach (int cls in nonMats)
                {
                    if (firstPass)
                    {
                        sqlQuery.AppendLine(cls.ToString());
                        firstPass = false;
                    }
                    else
                    {
                        sqlQuery.AppendLine("," + cls.ToString());
                    }

                }
                sqlQuery.AppendLine(" ))");
            }

            if (timeDeps.Count() > 0)
            {
                sqlQuery.AppendLine(" OR (acc_typeOr = 8 AND classOr in (");
                bool firstPass = true;
                foreach (int cls in timeDeps)
                {
                    if (firstPass)
                    {
                        sqlQuery.AppendLine(cls.ToString());
                        firstPass = false;
                    }
                    else
                    {
                        sqlQuery.AppendLine("," + cls.ToString());
                    }

                }
                sqlQuery.AppendLine(" ))");
            }

            if (secured.Count() > 0)
            {
                sqlQuery.AppendLine(" OR (acc_typeOr = 6 AND classOr in (");
                bool firstPass = true;
                foreach (int cls in secured)
                {
                    if (firstPass)
                    {
                        sqlQuery.AppendLine(cls.ToString());
                        firstPass = false;
                    }
                    else
                    {
                        sqlQuery.AppendLine("," + cls.ToString());
                    }

                }
                sqlQuery.AppendLine(" ))");
            }


            sqlQuery.AppendLine(" ) ");



            sqlQuery.AppendLine(" ) as alb ");
            sqlQuery.AppendLine(" ON ala.code = alb.code AND ala.simulationId = alb.simulationId ");

            DataTable retTable = Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), curSim.ToString(), priorSim.ToString(), curDate, priorDate));

            if (retTable.Rows.Count > 0)
            {

                if (retTable.Rows[0][0].ToString() == "")
                {
                    return 0;
                }
                else
                {
                    return double.Parse(retTable.Rows[0][0].ToString()) / 1000;
                }

            }
            else
            {
                return 0;
            }
        }

        public double ActualLoans(string curSim, string priorSim, string conStr, string curDate, string priorDate)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	SUM(CASE WHEN ala.simulationId = {0} THEN End_Bal ELSE -End_Bal END) as actualLoans ");
            sqlQuery.AppendLine(" FROM ");
            sqlQuery.AppendLine(" (SELECT code, simulationId, End_Bal  FROM Basis_ALA WHERE  (simulationid = {0} AND month = '{2}') OR (simulationid = '{1}' AND month = '{3}')) as ala INNER JOIN ");
            sqlQuery.AppendLine(" (SELECT code, simulationId FROM Basis_ALB WHERE simulationId in ({0},{1}) AND exclude = 0 AND acc_TypeOr = 2) as alb ");
            sqlQuery.AppendLine(" ON ala.code = alb.code AND ala.simulationId = alb.simulationId ");
            DataTable retTable = Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), curSim.ToString(), priorSim.ToString(), curDate, priorDate));

            if (retTable.Rows.Count > 0)
            {
                return (double)retTable.Rows[0][0] / 1000;
            }
            else
            {
                return 0;
            }
        }

        public double NonPledgeAmount(string simId, string scenNum, string asOfDate, string definedGroup, string conStr, string reportType)
        {
            int defGroupId = int.Parse(definedGroup.Substring(3));
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("	SELECT ");
            sqlQuery.AppendLine("		SUM(alpdata.principalPayDown )  ");
            sqlQuery.AppendLine("	     + ISNULL(SUM(  ");
            sqlQuery.AppendLine("				CASE  ");
            sqlQuery.AppendLine("					WHEN endEBal >= 0 AND ABS(negNew) > endEBal  THEN  ");
            sqlQuery.AppendLine("						PayEBal + ABS(EndEBal)  ");
            sqlQuery.AppendLine("					WHEN endEBal <= 0 AND ABS(negNew) < endEBal THEN  ");
            sqlQuery.AppendLine("						PayEBal + ABS(EndEBal)  ");
            sqlQuery.AppendLine("					ELSE  ");
            sqlQuery.AppendLine("						PayEBal + ABS(negNew)  ");
            sqlQuery.AppendLine("					END  ");
            sqlQuery.AppendLine("			), 0) 	 ");
            sqlQuery.AppendLine("	     as cashFlow 	 ");
            sqlQuery.AppendLine("		FROM 		 ");
            sqlQuery.AppendLine("		(SELECT code, classOr, acc_typeOr, isAsset FROM Basis_ALb WHERE SimulationId = {0} AND exclude <> 1) AS alb  ");
            sqlQuery.AppendLine("		INNER JOIN 	 ");
            sqlQuery.AppendLine("		(SELECT   ");
            sqlQuery.AppendLine("			ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ALP1.code, ALP3.code), ALP4.code), ALP8.code), ALP7.code), ALP5.code), ALP6.code) as code  ");
            sqlQuery.AppendLine("			,ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ALP1.month, ALP3.month), ALP4.month), ALP8.month), ALP7.month), ALP5.month), ALP6.month) as month  ");
            sqlQuery.AppendLine("			,ISNULL(alp3.matebal, 0) + ISNULL(alp4.AmtEBal, 0) AS principalPayDown  ");
            sqlQuery.AppendLine("			,alp1.stBal AS startBal  ");
            sqlQuery.AppendLine("			,ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) AS relNew  ");
            sqlQuery.AppendLine("			,ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) as selfNew  ");
            sqlQuery.AppendLine("			,CASE   ");
            sqlQuery.AppendLine("				WHEN ALP1.stBal > 0 THEN  ");
            sqlQuery.AppendLine("					CASE  ");
            sqlQuery.AppendLine("						WHEN ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) > 0 THEN  ");
            sqlQuery.AppendLine("							0  ");
            sqlQuery.AppendLine("						ELSE  ");
            sqlQuery.AppendLine("							ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0)  ");
            sqlQuery.AppendLine("					END  ");
            sqlQuery.AppendLine("				WHEN ALP1.stBal < 0 THEN  ");
            sqlQuery.AppendLine("					CASE  ");
            sqlQuery.AppendLine("					WHEN ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) < 0 THEN  ");
            sqlQuery.AppendLine("						0  ");
            sqlQuery.AppendLine("					ELSE  ");
            sqlQuery.AppendLine("						 ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0)  ");
            sqlQuery.AppendLine("					END  ");
            sqlQuery.AppendLine("				ELSE  ");
            sqlQuery.AppendLine("					 ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0)  ");
            sqlQuery.AppendLine("				END as negNew  ");
            sqlQuery.AppendLine("			,ALP7.newBal as newBal  ");
            sqlQuery.AppendLine("			,ALP6.repEFreqBal as repEFreqBal  ");
            sqlQuery.AppendLine("			,ALP6.repEFreqBal as repEBal  ");
            sqlQuery.AppendLine("			,ALP1.EndEbal as endEBal  ");
            sqlQuery.AppendLine("			,NULLIF(alp5.PayEBal, 0) as PayEBal   ");
            sqlQuery.AppendLine("		 FROM  ");
            sqlQuery.AppendLine("		 ");
            sqlQuery.AppendLine("		(SELECT code, month, AmtEBal FROM Basis_ALP4 WHERE SimulationId = {0} AND scenario = '{1}') AS alp4  ");
            sqlQuery.AppendLine("		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code, month, mateBal  FROM Basis_ALP3 WHERE SimulationId = {0} AND scenario = '{1}') AS alp3  ");
            sqlQuery.AppendLine("		ON alp4.code = alp3.code AND alp4.month = alp3.month  ");
            sqlQuery.AppendLine("		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code, month,  stBal, endEBal FROM Basis_ALP1 WHERE SimulationId = {0} AND scenario = '{1}') AS alp1  ");
            sqlQuery.AppendLine("		ON alp3.code = alp1.code AND alp3.month = alp1.month  ");
            sqlQuery.AppendLine("		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code  ");
            sqlQuery.AppendLine("			,month   ");
            sqlQuery.AppendLine("			,RelNewFBal  ");
            sqlQuery.AppendLine("			,relNewMBal  ");
            sqlQuery.AppendLine("			,relNewLBal  ");
            sqlQuery.AppendLine("			,selfFBal  ");
            sqlQuery.AppendLine("			,selfMBal  ");
            sqlQuery.AppendLine("			,selfLBal FROM basis_ALP8 WHERE SimulationId = {0} AND scenario = '{1}') AS alp8  ");
            sqlQuery.AppendLine("		ON alp1.code = alp8.code AND alp1.Month = alp8.month  ");
            sqlQuery.AppendLine("		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code, month,  newBal FROM basis_ALP7 WHERE SimulationId = {0} AND scenario = '{1}') AS alp7   ");
            sqlQuery.AppendLine("		ON alp8.code = alp7.code AND alp8.month = alp7.month  ");
            sqlQuery.AppendLine("		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code, month,  PayEBal FROM basis_ALP5 WHERE SimulationId = {0} AND scenario = '{1}') AS alp5  ");
            sqlQuery.AppendLine("		ON alp7.code = alp5.code AND alp7.month = alp5.month  ");
            sqlQuery.AppendLine("		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code, month,  repEFreqBal, RepEBal FROM Basis_ALP6 WHERE SimulationId = {0} AND scenario = '{1}') AS alp6  ");
            sqlQuery.AppendLine("		ON alp5.code = alp6.code AND alp5.month = alp6.month) AS alpData  ");
            sqlQuery.AppendLine("		ON alpData.code = alb.code  ");
            sqlQuery.AppendLine("		INNER JOIN  ");
            sqlQuery.AppendLine("		(SELECT code, exclude FROM Basis_ALS WHERE exclude <> 1 AND scenario = '{1}' and SimulationId = {0}) as als  ");
            sqlQuery.AppendLine("		ON alb.code = als.Code  ");
            sqlQuery.AppendLine("		INNER JOIN   ");
            sqlQuery.AppendLine("		 ");
            sqlQuery.AppendLine("		(SELECT    ");
            sqlQuery.AppendLine("			dgg.name   ");
            sqlQuery.AppendLine("			,dgg.[Priority]   ");
            sqlQuery.AppendLine("			,dgc.IsAsset    ");
            sqlQuery.AppendLine("			,dgc.Acc_Type    ");
            sqlQuery.AppendLine("			,dgc.RbcTier    ");
            sqlQuery.AppendLine("		  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg   ");
            sqlQuery.AppendLine("		  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg   ");
            sqlQuery.AppendLine("		  ON dg.id = dgg.DefinedGroupingId    ");
            sqlQuery.AppendLine("		  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc   ");
            sqlQuery.AppendLine("		  ON dgg.id = dgc.DefinedGroupingGroupId    ");
            sqlQuery.AppendLine("		  WHERE dg.id = {3}) as dgs ON   ");
            sqlQuery.AppendLine("		  dgs.acc_Type = alb.acc_TypeOr AND dgs.rbcTier =alb.classOr AND dgs.IsAsset = alb.isAsset   ");
            sqlQuery.AppendLine("		  WHERE alpData.month > DATEADD(m,1,'{2}')  ");
            if (reportType == "90Day")
            {
                sqlQuery.AppendLine(" AND alpData.month < DATEADD(m, 3,'{2}')");
            }
            else
            {
                sqlQuery.AppendLine(" AND alpData.month < DATEADD(m, 6,'{2}')");
            }
            DataTable retTable = Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), simId.ToString(), scenNum, asOfDate, defGroupId));

            if (retTable.Rows.Count > 0 && retTable.Rows[0][0].ToString() != "")
            {
                return double.Parse(retTable.Rows[0][0].ToString()) / 1000;
            }
            else
            {
                return 0;
            }
        }

        public DataTable CashflowTable(string asOfDate, string simulationId, string scenario, string viewOption, string constr, string isAsset)
        {
            int defGroupId = -1;
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("                	SELECT ");
            sqlQuery.AppendLine("                		CASE  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 3, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q1Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 6, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q2Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 9, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q3Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 12, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q4Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 15, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q1Y2'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 18, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q2Y2'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 21, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q3Y2'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 24, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q4Y2'  ");
            sqlQuery.AppendLine("							END as qtrs ");

            if (viewOption == "0")
            {
                sqlQuery.AppendLine("                		,CASE WHEN at.name = 'Non-Maturity Deposit' THEN 'Non-Maturity' ");
                sqlQuery.AppendLine("                		ELSE at.Name ");
                sqlQuery.AppendLine("                		END as Name ");
            }
            else
            {
                sqlQuery.AppendLine("                		,dgs.name  ");
            }


            //sqlQuery.AppendLine("                		,SUM(alpdata.principalPayDown )  ");
            //sqlQuery.AppendLine("                        + ISNULL(SUM(  ");
            //sqlQuery.AppendLine("                    			CASE  ");
            //sqlQuery.AppendLine("                    				WHEN endEBal >= 0 AND ABS(negNew) > endEBal  THEN  ");
            //sqlQuery.AppendLine("                    					PayEBal + ABS(EndEBal)  ");
            //sqlQuery.AppendLine("                    				WHEN endEBal <= 0 AND ABS(negNew) < endEBal THEN  ");
            //sqlQuery.AppendLine("                    					PayEBal + ABS(EndEBal)  ");
            //sqlQuery.AppendLine("                    				ELSE  ");
            //sqlQuery.AppendLine("                    					PayEBal + ABS(negNew)  ");
            //sqlQuery.AppendLine("                    				END  ");
            //sqlQuery.AppendLine("                    		), 0)  ");
            //sqlQuery.AppendLine("                 as cashFlow  ");
            sqlQuery.AppendLine("                		,SUM(alpdata.principalPayDown + PayEBal ) as cashFlow ");
            sqlQuery.AppendLine("                  ");
            sqlQuery.AppendLine("                		FROM  ");
            sqlQuery.AppendLine("                  ");
            sqlQuery.AppendLine("                		(SELECT code, classOr, acc_typeOr, isAsset FROM Basis_ALb WHERE SimulationId = {1} AND exclude <> 1 and isAsset = " + isAsset + ") AS alb  ");
            sqlQuery.AppendLine("                		INNER JOIN  ");
            sqlQuery.AppendLine("                  ");
            sqlQuery.AppendLine("                		(SELECT   ");
            sqlQuery.AppendLine("                			ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ALP1.code, ALP3.code), ALP4.code), ALP8.code), ALP7.code), ALP5.code), ALP6.code) as code  ");
            sqlQuery.AppendLine("                			,ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ISNULL(ALP1.month, ALP3.month), ALP4.month), ALP8.month), ALP7.month), ALP5.month), ALP6.month) as month  ");
            sqlQuery.AppendLine("                			,ISNULL(alp3.matebal, 0) + ISNULL(alp4.AmtEBal, 0) AS principalPayDown  ");
            sqlQuery.AppendLine("                			,alp1.stBal AS startBal  ");
            sqlQuery.AppendLine("                			,ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) AS relNew  ");
            sqlQuery.AppendLine("                			,ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) as selfNew  ");
            sqlQuery.AppendLine("                			,CASE   ");
            sqlQuery.AppendLine("                				WHEN ALP1.stBal > 0 THEN  ");
            sqlQuery.AppendLine("                					CASE  ");
            sqlQuery.AppendLine("                						WHEN ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) > 0 THEN  ");
            sqlQuery.AppendLine("                							0  ");
            sqlQuery.AppendLine("                						ELSE  ");
            sqlQuery.AppendLine("                							ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0)  ");
            sqlQuery.AppendLine("                					END  ");
            sqlQuery.AppendLine("                				WHEN ALP1.stBal < 0 THEN  ");
            sqlQuery.AppendLine("                					CASE  ");
            sqlQuery.AppendLine("                					WHEN ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0) < 0 THEN  ");
            sqlQuery.AppendLine("                						0  ");
            sqlQuery.AppendLine("                					ELSE  ");
            sqlQuery.AppendLine("                						 ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0)  ");
            sqlQuery.AppendLine("                					END  ");
            sqlQuery.AppendLine("                				ELSE  ");
            sqlQuery.AppendLine("                					 ISNULL(ALP7.newBal, 0) + ISNULL(ALP8.relNewFBal, 0) + ISNULL(ALP8.relNewMBal, 0) + ISNULL(ALP8.relNewLBal, 0) + ISNULL(ALP8.SelfFBal, 0) + ISNULL(ALP8.SelfMBal,0) + ISNULL(ALP8.selfLBal,0)  ");
            sqlQuery.AppendLine("                				END as negNew  ");
            sqlQuery.AppendLine("                			,ALP7.newBal as newBal  ");
            sqlQuery.AppendLine("                			,ALP6.repEFreqBal as repEFreqBal  ");
            sqlQuery.AppendLine("                			,ALP6.repEFreqBal as repEBal  ");
            sqlQuery.AppendLine("                			,ALP1.EndEbal as endEBal  ");
            sqlQuery.AppendLine("                			,ISNULL(alp5.PayEBal, 0) as PayEBal   ");
            sqlQuery.AppendLine("                		 FROM  ");
            sqlQuery.AppendLine("                	  ");
            sqlQuery.AppendLine("                		(SELECT code, month, AmtEBal FROM Basis_ALP4 WHERE SimulationId = {1} AND scenario = '{2}') AS alp4  ");
            sqlQuery.AppendLine("                		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code, month, mateBal  FROM Basis_ALP3 WHERE SimulationId = {1} AND scenario = '{2}') AS alp3  ");
            sqlQuery.AppendLine("                		ON alp4.code = alp3.code AND alp4.month = alp3.month  ");
            sqlQuery.AppendLine("                		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code, month,  stBal, endEBal FROM Basis_ALP1 WHERE SimulationId = {1} AND scenario = '{2}') AS alp1  ");
            sqlQuery.AppendLine("                		ON alp3.code = alp1.code AND alp3.month = alp1.month  ");
            sqlQuery.AppendLine("                		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code  ");
            sqlQuery.AppendLine("                			,month   ");
            sqlQuery.AppendLine("                			,RelNewFBal  ");
            sqlQuery.AppendLine("                			,relNewMBal  ");
            sqlQuery.AppendLine("                			,relNewLBal  ");
            sqlQuery.AppendLine("                			,selfFBal  ");
            sqlQuery.AppendLine("                			,selfMBal  ");
            sqlQuery.AppendLine("                			,selfLBal FROM basis_ALP8 WHERE SimulationId = {1} AND scenario = '{2}') AS alp8  ");
            sqlQuery.AppendLine("                		ON alp1.code = alp8.code AND alp1.Month = alp8.month  ");
            sqlQuery.AppendLine("                		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code, month,  newBal FROM basis_ALP7 WHERE SimulationId = {1} AND scenario = '{2}') AS alp7   ");
            sqlQuery.AppendLine("                		ON alp8.code = alp7.code AND alp8.month = alp7.month  ");
            sqlQuery.AppendLine("                		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code, month,  PayEBal FROM basis_ALP5 WHERE SimulationId = {1} AND scenario = '{2}') AS alp5  ");
            sqlQuery.AppendLine("                		ON alp7.code = alp5.code AND alp7.month = alp5.month  ");
            sqlQuery.AppendLine("                		FULL OUTER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code, month,  repEFreqBal, RepEBal FROM Basis_ALP6 WHERE SimulationId = {1} AND scenario = '{2}') AS alp6  ");
            sqlQuery.AppendLine("                		ON alp5.code = alp6.code AND alp5.month = alp6.month) AS alpData  ");
            sqlQuery.AppendLine("                		ON alpData.code = alb.code  ");
            sqlQuery.AppendLine("                		INNER JOIN  ");
            sqlQuery.AppendLine("                		(SELECT code, exclude FROM Basis_ALS WHERE exclude <> 1 AND scenario = '{2}' and SimulationId = {1}) as als  ");
            sqlQuery.AppendLine("                		ON alb.code = als.Code  ");

            if (viewOption == "0")
            {
                sqlQuery.AppendLine("                   INNER JOIN  ATLAS_AccountType as at");
                sqlQuery.AppendLine("                   on at.id = Acc_TypeOR");
                sqlQuery.AppendLine("                   INNER JOIN ATLAS_AccountTypeOrder as actOrder");
                sqlQuery.AppendLine("                   ON actOrder.Acc_Type = Acc_TypeOR and actOrder.isAsset = alb.IsAsset");
            }
            else
            {
                sqlQuery.AppendLine("                		INNER JOIN   ");
                sqlQuery.AppendLine("                		(SELECT    ");
                sqlQuery.AppendLine("                			dgg.name   ");
                sqlQuery.AppendLine("                			,dgg.[Priority]   ");
                sqlQuery.AppendLine("                			,dgc.IsAsset    ");
                sqlQuery.AppendLine("                			,dgc.Acc_Type    ");
                sqlQuery.AppendLine("                			,dgc.RbcTier    ");
                sqlQuery.AppendLine("                		  FROM DDW_Atlas_Templates.dbo.[ATLAS_DefinedGrouping] AS dg   ");
                sqlQuery.AppendLine("                		  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingGroups AS dgg   ");
                sqlQuery.AppendLine("                		  ON dg.id = dgg.DefinedGroupingId    ");
                sqlQuery.AppendLine("                		  INNER JOIN DDW_Atlas_Templates.dbo.ATLAS_DefinedGroupingClassifications as dgc   ");
                sqlQuery.AppendLine("                		  ON dgg.id = dgc.DefinedGroupingGroupId    ");
                sqlQuery.AppendLine("                		  WHERE dg.id = {3}) as dgs ON   ");
                sqlQuery.AppendLine("                		  dgs.acc_Type = alb.acc_TypeOr AND dgs.rbcTier =alb.classOr AND dgs.IsAsset = alb.isAsset   ");
            }


            sqlQuery.AppendLine("						  WHERE alpData.month < DATEADD(m,24,'{0}') ");

            if (viewOption == "0")
            {
                sqlQuery.AppendLine("                			GROUP BY at.name, actOrder.IsAsset");
            }
            else
            {
                sqlQuery.AppendLine("                			GROUP BY dgs.name");
            }


            sqlQuery.AppendLine("                       ,CASE ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 3, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q1Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 6, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q2Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 9, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q3Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 12, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q4Y1'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 15, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q1Y2'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 18, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q2Y2'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 21, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q3Y2'  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 24, '{0}') THEN  ");
            sqlQuery.AppendLine("                				'Q4Y2'  ");
            sqlQuery.AppendLine("                			END   ");

            //getting proper order
            sqlQuery.AppendLine("                       ,CASE ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 3, '{0}') THEN  ");
            sqlQuery.AppendLine("                				0  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 6, '{0}') THEN  ");
            sqlQuery.AppendLine("                				1  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 9, '{0}') THEN  ");
            sqlQuery.AppendLine("                				2  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 12, '{0}') THEN  ");
            sqlQuery.AppendLine("                				3  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 15, '{0}') THEN  ");
            sqlQuery.AppendLine("                				4  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 18, '{0}') THEN  ");
            sqlQuery.AppendLine("                				5 ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 21, '{0}') THEN  ");
            sqlQuery.AppendLine("                				6  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 24, '{0}') THEN  ");
            sqlQuery.AppendLine("                				7  ");
            sqlQuery.AppendLine("                			END   ");

            if (viewOption == "0")
            {
                sqlQuery.AppendLine(" ORDER BY actOrder.IsAsset desc, max(actOrder.[Order])");
            }
            else
            {
                sqlQuery.AppendLine(" ORDER BY MAX(dgs.priority)   ");
                defGroupId = int.Parse(viewOption.Substring(3));
            }
            sqlQuery.AppendLine("                       ,CASE ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 3, '{0}') THEN  ");
            sqlQuery.AppendLine("                				0  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 6, '{0}') THEN  ");
            sqlQuery.AppendLine("                				1  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 9, '{0}') THEN  ");
            sqlQuery.AppendLine("                				2  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 12, '{0}') THEN  ");
            sqlQuery.AppendLine("                				3  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 15, '{0}') THEN  ");
            sqlQuery.AppendLine("                				4  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 18, '{0}') THEN  ");
            sqlQuery.AppendLine("                				5 ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 21, '{0}') THEN  ");
            sqlQuery.AppendLine("                				6  ");
            sqlQuery.AppendLine("                			WHEN alpData.month < DATEADD(m, 24, '{0}') THEN  ");
            sqlQuery.AppendLine("                				7  ");
            sqlQuery.AppendLine("                			END   ");


            return Utilities.ExecuteSql(constr, String.Format(sqlQuery.ToString(), asOfDate, simulationId, scenario, defGroupId.ToString()));
        }

    }
}