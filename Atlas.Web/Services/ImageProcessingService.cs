﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Atlas.Web.Services
{
    public class ImageProcessingService
    {
        private static Image resizeImage(Image image, int newWidth, int newHeight)
        {
            Bitmap result = new Bitmap(newWidth, newHeight);

            using (Graphics g = Graphics.FromImage(result))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return result;
        }

        public static string ConvertImageToConstrainedPNG(string base64Data, int maxWidthOrHeight)
        {
            Image image;

            using (MemoryStream ms = new MemoryStream())
            {
                byte[] blobBytes = Convert.FromBase64String(base64Data);

                ms.Write(blobBytes, 0, blobBytes.Length);
                ms.Seek(0, SeekOrigin.Begin);

                image = Image.FromStream(ms);
            }

            double newWidth = image.Width;
            double newHeight = image.Height;

            if(newWidth > newHeight)
            {
                if(newWidth > maxWidthOrHeight)
                {
                    newHeight = (maxWidthOrHeight / newWidth) * newHeight;
                    newWidth = maxWidthOrHeight;
                }
            }
            else
            {
                if (newHeight > maxWidthOrHeight)
                {
                    newWidth = (maxWidthOrHeight / newHeight) * newWidth;
                    newHeight = maxWidthOrHeight;
                }
            }

            Image resizedImage = resizeImage(image, (int) newWidth, (int) newHeight);

            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Save(ms, ImageFormat.Png);
                return Convert.ToBase64String(ms.ToArray());
            }  
        }
    }
}