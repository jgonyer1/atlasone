﻿/**
 * Copyright 2017 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let asyncawait = true;
try {
    new Function('async function test(){await 1}');
} catch (error) {
    asyncawait = false;
}

// If node does not support async await, use the compiled version.
if (asyncawait)
    module.exports = require('./lib/Puppeteer');
else
    module.exports = require('./node6/lib/Puppeteer');

const puppeteer = require('puppeteer');

var firstReportInPackageURL = '{{firstURL}}';
var onlyOne = {{onlyOne}};

(async () => {
    const browser = await puppeteer.launch({
        headless: true, args: [
            '--window-size=1280,1696'
        ]
    });

    const page = await browser.newPage();

    var fs = require('fs');

    //write status out here to say starting PDF Wizard
    fs.writeFileSync('{{folderPath}}status.json', '{ "IsSuccess": true, "Message": "Starting PDF Machine", "TotalReportCount": {{totalReportCount}} }', );

    var contents = fs.readFileSync('{{folderPath}}cookies_.txt', 'utf8');

    var cookieJson = JSON.parse(contents);
    for (var i = 0; i < cookieJson.length; i++) {
        var tempCookie = {};

        tempCookie["Name"] = cookieJson[i]["Name"];
        tempCookie["Domain"] = cookieJson[i]["Domain"];
        tempCookie["Value"] = cookieJson[i]["Value"];
        tempCookie["Path"] = cookieJson[i]["Path"];
        tempCookie["Secure"] = cookieJson[i]["Secure"];
        tempCookie["Shareable"] = cookieJson[i]["Shareable"];
        tempCookie["HasKeys"] = cookieJson[i]["HasKeys"];
        tempCookie["Expires"] = cookieJson[i]["Expires"];
        // Here we are adding the relevant values as needed and in the proper format

        var tempADD = {
            "domain": tempCookie["Domain"],
            "expires": Date.now() / 1000 + 10000,
            "httponly": tempCookie["HttpOnly"],
            "name": tempCookie["Name"],
            "path": tempCookie["Path"],
            "secure": tempCookie["Secure"],
            "value": tempCookie["Value"],
            "shareable": tempCookie["Shareable"],
            "domain": tempCookie["Domain"],
            "hasKeys": tempCookie["HasKeys"]

        };

        // Finally, we add the cookie. phantom.addCookie returns true if successful
        await page.setCookie(tempADD);
    }

    //write status out here to say starting PDF Wizard
    fs.writeFileSync('{{folderPath}}status.json', '{ "IsSuccess": true, "Message": "Turning Turbo Mode On", "TotalReportCount": {{totalReportCount}} }');

    try {
        page.on('load', () => console.log("Loaded: " + page.url()));
        page.on('console', msg => {

            for (let i = 0; i < msg.args().length; ++i) {
                fs.appendFile('{{folderPath}}message.txt', `${i}: ${msg.args()[i]}`, function (err) {
                });
            }
        });

        await page.setViewport({ width: 9000, height: 1161 })
        await page.goto(firstReportInPackageURL);

        //This emulates old exporting so do not need to updat a ton of code

        await page.evaluate(() => {
            window.hiqPdfInfo = 'FYU'; hiqPdfConverter = { startConversion: function () { var doShit = 1; } }
            //  $('html').css('transform', 'scaleX(.75) scaleY(.75)');
			/*$("#mainContainer").addClass('overrideWidth');
			$("#mainContainer").removeClass('container');*/
            return true;
        });

        fs.writeFileSync('{{folderPath}}status.json', '{ "IsSuccess": true, "Message": "Automating Unstructured Blockchain ETL", "TotalReportCount": {{totalReportCount}} }');

        var reportCounter = 0;
        var keepExporting = true;

        do {
            // loop until page is loaded
            var currentReport;
            var giveUp = 40000;
			
            do {
                currentReport = await page.evaluate(() => {
                    try {
                        var fatalMessage = $('body').attr('data-fatal');

                        if (fatalMessage) {
                            return { loaded: 0, fatalMessage: fatalMessage, name: $('#reportName').text() };
                        }

                        var val = $('#reportLoaded').text();

                        if (val != "" && val != undefined) {
                            return {
                                loaded: parseInt(val),
                                isLandscape: $('*[data-pdf-orientation=portrait]').length === 0,
                                name: $('#reportName').text()
                            };
                        }
                        else {
                            return { loaded: 0, isLandscape: true, name: '' };
                        }
                    }
                    catch (err) {
                        return { loaded: 0, isLandscape: true, name: '' };
                    }
                });

                await page.waitFor(250);
                giveUp = giveUp - 250;
            }
            while (currentReport.loaded == 0 && giveUp > 0 && !currentReport.fatalMessage);

            if (!currentReport.loaded || currentReport.fatal) {
                var failMessage = `Failed to export Report ${reportCounter}.pdf (${currentReport.name})`;
                console.log(failMessage);
                fs.writeFileSync(
                    '{{folderPath}}status.json',
                    `{ "IsSuccess": false, "ReportIndex": ${reportCounter}, "ReportName": "${currentReport.name}", "Message": "${currentReport.fatalMessage}", "TotalReportCount": {{totalReportCount}} }`);
                break;
            }

            //After loaded way to export 
            await page.pdf({
                path: '{{folderPath}}' + reportCounter + '.pdf',
                landscape: currentReport.isLandscape,
                foramt: 'A4',
                // width: 1100,
                //height: 794,
                margin: { top: 40, left: 10, right: 10, bottom: 30 },
                padding: { top: 0, left: 0, right: 0, bottom: 0 },
                printBackground: false
            });

            if (!onlyOne) {
                //click next and now export next report
                keepExporting = await page.evaluate(() => {
                    if ($('#nextReport').length && !$('#nextReport').is(':disabled')) {
                        //reset report loaded and go to next report
                        $('#reportLoaded').text('0');
                        $('#nextReport').click();
                        return true;
                    }
                    else {
						return false;
                    }
                });
            }
            else {
                keepExporting = false;
            }

            var logMessage = `Exporting Report ${reportCounter}.pdf (${currentReport.name}) { isLandscape: ${currentReport.isLandscape} }`;
			
            //write status out here to say starting PDF Wizard
            if (currentReport.name != '') {
                fs.writeFileSync(
                    '{{folderPath}}status.json',
                    `{ "IsSuccess": true, "ReportIndex": ${reportCounter + 1}, "ReportName": "${currentReport.name}", "TotalReportCount": {{totalReportCount}} }`
                );
            }
            
            console.log(logMessage);
            reportCounter += 1;
			
            //Write File Here To Update Status
        }
        while (keepExporting)

        await browser.close();
    }
    catch (ex) {
        console.log(ex);
    }

})();