﻿using System;
using System.Data;
using System.Linq;
using System.IO;
using System.Web.Mvc;

using Atlas.Web.Services;
using Atlas.Institution.Model;

using Spire.Pdf;

namespace Atlas.Web.Controllers
{
    public class ReportController : Controller
    {
        const string PUPPET_PATH = @"C:\DCG Source\AtlasOne\node_modules\puppeteer\";

        private readonly PdfService _pdfService = new PdfService();
        private GlobalContextProvider _contextProvider = new GlobalContextProvider("");

        public FileContentResult Pdf(int id = 0)
        {
            Report rep = _contextProvider.Context.Reports.Where(z => z.Id == id).FirstOrDefault();

            string dbName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string packageId = rep.PackageId.ToString();
            string folderPath = FileProcessingService.GetBasePath() + dbName + "\\" + packageId + "\\";

            //Check to make sure directory exists for whatever reason
            if (!Directory.Exists(folderPath))
            {
                throw new Exception("Cant do that!");
            }

            PdfDocument packagePdf = new PdfDocument(folderPath + "singleExport.pdf");

            Response.AppendHeader("Content-Disposition", "inline; filename=\"" + rep.Name + ".pdf\"");

            using (MemoryStream ms = new MemoryStream())
            {
                packagePdf.SaveToStream(ms);

                return File(ms.ToArray(), "application/pdf"); //, rep.Name + ".pdf");
            }
        }

        public FileContentResult Excel(int id = 0)
        {
            //InstitutionContext _db = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            //var report = _db.Reports.Find(id);
            //Response.AppendHeader("Content-Disposition", "inline; filename=foo.pdf");
            //var baseUrl = Request.Url.GetLeftPart(UriPartial.Authority);
            //return File(_pdfService.ToPdf(report, 0, baseUrl).WriteToMemory(), "application/vnd.ms-excel");

            /*   InstitutionContext _db = new InstitutionContext(Utilities.BuildInstConnectionString(""));

               NIIRecon nr = _db.NIIRecons.Where(s => s.Id == id).FirstOrDefault();
               var rep = _db.Reports.Where(s => s.Id == id).FirstOrDefault();
               if (nr == null) throw new Exception("NII Recon Not Found");

               MemoryStream msZip = new MemoryStream();
               C1ZipFile zip = new C1ZipFile(msZip, true);
               NIIReconService nrs = new NIIReconService();
               MemoryStream ms = new MemoryStream();
               nrs.NIIReconSheet(id, nr, nr.TimePeriod, rep).Save(ms, FileFormat.OpenXml);

               string fileName = Utilities.InstName(nr.InstitutionDatabaseName);
               return File(ms.ToArray(), "application/vnd.ms-excel", fileName + ".xlsx");
               */

            return null;
        }
    }
}
