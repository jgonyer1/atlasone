﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using Atlas.Web.Services;
using Breeze.WebApi2;
using System.Data.Entity;
using Atlas.Web.Models;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Web;
using System.IO;
using System.Data;
using Atlas.Institution.Model;
using Atlas.Institution.Data;
using System.Text;
using System.Web.Security;
using System.Net.Mail;
using Breeze.ContextProvider;
using Atlas.Institution.Model.Atlas;

namespace P360.Web.Controllers
{
    [BreezeController]
    public class GlobalController : ApiController
    {
        private GlobalContextProvider _contextProvider = new GlobalContextProvider("");


        //Me replacing atlas one bank key stuff
        [HttpGet]
        public DataTable AtlasUserProfile()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            DataTable result = new DataTable();
            result = Utilities.GetAtlasUserData();

            //if no user found
            if (result.Rows.Count == 0)
            {
                Utilities.InsertNewUserData();
                result = Utilities.GetAtlasUserData();
            }

            //Check if their current bankkey is in fact a D360 Bank
            //I don't see how, with this set up, it ever would be but it can't hurt

            if (!Utilities.IsAtlasBank(result.Rows[0]["databaseName"].ToString()))
            {
                //get users first D360 bankkey
                //SetUserBankKey();

                DataTable availBanks = Utilities.AvailableBanksTable();

                if (availBanks.Rows.Count > 0)
                {
                    Utilities.SetUserData("databaseName", availBanks.Rows[0]["databaseName"].ToString());
                    //getUserData again
                    result = Utilities.GetAtlasUserData();
                }


            }

            return result;
        }

        [HttpGet]
        public string Metadata()
        {
            string username = HttpContext.Current.User.Identity.Name;
            //If users last visit bank still valid use it, otherwise use connection string in web.config
            if (Utilities.CheckIfValidDatabase())
            {
                try
                {
                    return _contextProvider.Metadata();
                }
                catch (Exception ex)
                {
                    SqlConnectionStringBuilder conStrBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["InstitutionContext"].ToString());

                    //

                    Utilities.SetUserData("databaseName", conStrBuilder.InitialCatalog);

                    // HttpContext.Current.Profile.SetPropertyValue("AtlasDatabase", conStrBuilder.InitialCatalog);
                    _contextProvider = new GlobalContextProvider("");
                    return _contextProvider.Metadata();
                }

            }
            else
            {
                SqlConnectionStringBuilder conStrBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["InstitutionContext"].ToString());
                Utilities.SetUserData("databaseName", conStrBuilder.InitialCatalog);
                //HttpContext.Current.Profile.SetPropertyValue("AtlasDatabase", conStrBuilder.InitialCatalog);
                _contextProvider = new GlobalContextProvider("");
                return _contextProvider.Metadata();
            }
        }

        //MetadataCheck
        [HttpGet]
        public string MetadataCheck()
        {
            string username = HttpContext.Current.User.Identity.Name;
            //If users last visit bank still valid use it, otherwise use connection string in web.config
            if (Utilities.CheckIfValidDatabase())
            {
                try
                {
                    return _contextProvider.Metadata();
                }
                catch (Exception ex)
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            _contextProvider.User = User.Identity.GetLogin();
            JArray ents = (JArray)saveBundle["entities"];
            string id = "";
            string state = "";
            string entityName = "";
            string message = "";
            foreach (JObject ent in ents)
            {
                JObject eAspect = (JObject)ent["entityAspect"];
                JObject origValsMap = (JObject)eAspect["originalValuesMap"];

                id = ent["Id"].ToString();
                entityName = eAspect["entityTypeName"].ToString();
                entityName = entityName.Substring(0, entityName.IndexOf(":"));
                state = eAspect["entityState"].ToString();
                message = state + " " + entityName;
                LogDataAudit(message, ent.ToString());
            }

            return _contextProvider.SaveChanges(saveBundle);
        }

        [HttpGet]
        public string SqlDatabase()
        {
            return "Packages";
        }

        [HttpGet]
        public string CodeVersion()
        {
            return FileVersionInfo.GetVersionInfo(this.GetType().Assembly.Location).FileVersion;
        }

        [HttpGet]
        public PDFExportStatus PackagePDF(int pckId)
        {
            PdfService srv = new PdfService();

            return srv.ExportPackage(pckId);
        }

        [HttpGet]
        public PDFExportStatus ReportPDF(int reportId)
        {
            PdfService srv = new PdfService();

            return srv.ExportReport(reportId);
        }

        [HttpGet]
        public PDFExportStatus PDFStatus(int pckId)
        {
            PdfService pdfs = new PdfService();

            return pdfs.GetPDFStatus(pckId);
        }

        //Gets User Profile Object and Returns
        [HttpGet]
        public DataTable UpdateProfile(string AtlasDatabase, string AtlasAsOfDate)
        {

            //Update Profile
            Utilities.SetUserData("databaseName", AtlasDatabase);
            Utilities.SetUserData("asOfDate", AtlasAsOfDate);
            //HttpContext.Current.Profile.SetPropertyValue("AtlasDatabase", AtlasDatabase);
            //HttpContext.Current.Profile.SetPropertyValue("AtlasAsOfDate", AtlasAsOfDate);

            //Set Bank Name For easier access to get
            //if (dt.Rows.Count > 0)
            //  {
            //     HttpContext.Current.Profile.SetPropertyValue("AtlasBankName", dt.Rows[0][0].ToString());
            //   }


            return AtlasUserProfile();
        }

        //Gets Available Banks Based Off Of Who Is Logged In
        [HttpGet]
        public DataTable AvailableBanks()
        {
            return Utilities.AvailableBanksTable();
        }

        //Gets the Institution Name for a database
        [HttpGet]
        public DataTable BankName(string dbName)
        {
            //Strips Off email domain and checks DDW Security Settings
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT  ");
            sqlQuery.AppendLine(" 		dbm.instName  ");
            sqlQuery.AppendLine("       ,dbm.regulatoryBankName ");
            sqlQuery.AppendLine(" 		FROM DDWM.dbo.SecurityInfo as si ");
            sqlQuery.AppendLine(" 		INNER JOIN DWR_FedRates.dbo.DatabaseHighlineMapping as dbm ON ");
            sqlQuery.AppendLine(" 		si.DBName = dbm.DatabaseName ");
            sqlQuery.AppendLine(" 		WHERE dbm.databaseName = '" + dbName + "'");
            var q = sqlQuery.ToString();
            return Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());
        }

        //Gets the Most recent dates from fed rates
        [HttpGet]
        public string[] FedRatesDates()
        {
            //Strips Off email domain and checks DDW Security Settings
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT DISTINCT ");
            sqlQuery.AppendLine(" 		RateDate  ");
            sqlQuery.AppendLine(" 		FROM DWR_FedRates.dbo.FedRates ");
            sqlQuery.AppendLine("       ORDER BY RateDate desc");

            DataTable retTab = Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());
            string[] ret = new string[retTab.Rows.Count];

            for (int i = 0; i < retTab.Rows.Count; i++)
            {
                ret[i] = retTab.Rows[i][0].ToString();
            }
            return ret;
        }

        [HttpGet]
        public Dictionary<string, DataTable> PolicyOffsets(string instName = "")
        {

            if (instName == "" || instName == null)
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }


            Dictionary<string, DataTable> pols = new Dictionary<string, DataTable>();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT CONVERT(VARCHAR(10), p.asOfDate, 101) as asOfDate, p.NIISimulationTypeId, p.EveSimulationTypeId, p.BasicSurplusAdminId as bsId, p.LiquidityRiskAssessment , p.NIIRiskAssessment , p.CapitalRiskAssessment, bs.ScenarioTypeId as bsScenario, CASE WHEN bs.OverrideSimulationTypeId = 1 THEN bs.SimulationTypeId ELSE p.NIISimulationTypeId END  as bsSimulation   FROM [" + instName + "].[dbo].ATLAS_Policy as p INNER JOIN [" + instName + "].[dbo].ATLAS_BasicSurplusAdmin as bs ON bs.Id = p.BasicSurplusAdminId ");

            DataTable polTable = Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());

            foreach (DataRow dr in polTable.Rows)
            {
                pols.Add(dr["asOfDate"].ToString(), new DataTable());

                pols[dr["asOfDate"].ToString()].Columns.Add("eveId");
                pols[dr["asOfDate"].ToString()].Columns.Add("niiId");
                pols[dr["asOfDate"].ToString()].Columns.Add("bsId");
                pols[dr["asOfDate"].ToString()].Columns.Add("liquidityRiskAssessment");
                pols[dr["asOfDate"].ToString()].Columns.Add("nIIRiskAssessment");
                pols[dr["asOfDate"].ToString()].Columns.Add("capitalRiskAssessment");

                pols[dr["asOfDate"].ToString()].Columns.Add("bsSimulation");
                pols[dr["asOfDate"].ToString()].Columns.Add("bsScenario");


                DataRow newRow = pols[dr["asOfDate"].ToString()].NewRow();
                newRow["niiId"] = dr["NIISimulationTypeId"].ToString();
                newRow["eveId"] = dr["EveSimulationTypeId"].ToString();
                newRow["bsId"] = dr["bsId"].ToString();

                newRow["liquidityRiskAssessment"] = dr["liquidityRiskAssessment"].ToString();
                newRow["nIIRiskAssessment"] = dr["nIIRiskAssessment"].ToString();
                newRow["capitalRiskAssessment"] = dr["capitalRiskAssessment"].ToString();

                newRow["bsSimulation"] = dr["bsSimulation"].ToString();
                newRow["bsScenario"] = dr["bsScenario"].ToString();


                pols[dr["asOfDate"].ToString()].Rows.Add(newRow);
            }

            return pols;
        }

        [HttpGet]
        public Dictionary<string, DataTable> ModelOffsets(string instName = "")
        {

            if (instName == "")
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }


            Dictionary<string, DataTable> pols = new Dictionary<string, DataTable>();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT CONVERT(VARCHAR(10), asOfDate, 101) as asOfDate,[ReportTaxEquivalent] ,[ReportTaxEquivalentYield] FROM  [" + instName + "].[dbo].[ATLAS_ModelSetup]");

            DataTable polTable = Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());

            foreach (DataRow dr in polTable.Rows)
            {
                pols.Add(dr["asOfDate"].ToString(), new DataTable());

                pols[dr["asOfDate"].ToString()].Columns.Add("taxIncome");
                pols[dr["asOfDate"].ToString()].Columns.Add("taxYield");


                DataRow newRow = pols[dr["asOfDate"].ToString()].NewRow();
                newRow["taxIncome"] = dr["ReportTaxEquivalent"].ToString();
                newRow["taxYield"] = dr["ReportTaxEquivalentYield"].ToString();

                pols[dr["asOfDate"].ToString()].Rows.Add(newRow);
            }

            return pols;
        }


        [HttpGet]
        public Dictionary<string, DataTable> BasicSurplusAdminOffsets(string instName = "")
        {

            if (instName == "")
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }


            Dictionary<string, DataTable> bs = new Dictionary<string, DataTable>();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT bs.name, bs.id, aod.id, CONVERT(VARCHAR(10), aod.asOfDate, 101) as asOfDate ");
            sqlQuery.AppendLine(" FROM [" + instName + "].[dbo].[ATLAS_BasicSurplusAdmin] as bs ");
            sqlQuery.AppendLine(" INNER JOIN [" + instName + "].[dbo].asOfDateInfo as aod ");
            sqlQuery.AppendLine(" ON aod.id = bs.InformationId");

            DataTable bsTable = Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());

            foreach (DataRow dr in bsTable.Rows)
            {
                if (!bs.ContainsKey(dr["asOfDate"].ToString()))
                {
                    bs.Add(dr["asOfDate"].ToString(), new DataTable());

                    bs[dr["asOfDate"].ToString()].Columns.Add("id");
                    bs[dr["asOfDate"].ToString()].Columns.Add("name");

                }
                DataRow newRow = bs[dr["asOfDate"].ToString()].NewRow();
                newRow["id"] = dr["id"].ToString();
                newRow["name"] = dr["name"].ToString();

                bs[dr["asOfDate"].ToString()].Rows.Add(newRow);
            }

            return bs;
        }


        //Gets Available As of Dates Based Off Of What database is selected
        [HttpGet]
        public DataTable AsOfDates(string instName = "", bool prevDatesOnly = false, int numDates = 0, bool excludeCurrent = false)
        {
            DataRow cachedUserDataRow = null;
            Func<DataRow> getCachedDataRow = () => { cachedUserDataRow = cachedUserDataRow ?? Utilities.GetAtlasUserData().Rows[0]; return cachedUserDataRow; };

            if (string.IsNullOrEmpty(instName))
                instName = getCachedDataRow()["databaseName"].ToString();

            if (string.IsNullOrEmpty(instName))
            {
                DataTable tmpTable = new DataTable();
                tmpTable.Columns.Add("asOfDateDescript");
                return tmpTable;
            }

            //Strips Off email domain and checks DDW Security Settings
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Append("SELECT");
            if (numDates > 0) sqlQuery.Append(" TOP " + numDates);

            sqlQuery.AppendLine(" CONVERT(VARCHAR(10), asOfDate, 101) as asOfDateDescript FROM [" + instName + "].[dbo].[asOfDateInfo]");
            sqlQuery.AppendLine(" WHERE BasisDate = 1");

            if (prevDatesOnly)
            {
                sqlQuery.Append("AND asOfDate <");
                sqlQuery.Append(excludeCurrent ? "" : "=");
                sqlQuery.AppendLine(" '" + getCachedDataRow()["asOfDate"].ToString() + "'");
            }

            sqlQuery.AppendLine("ORDER BY asOfdate DESC");
            return Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());
        }

        [HttpGet]
        public DateTime OffsetAsOfDate(string instName, int offset)
        {
            DataTable aods = AsOfDates(instName, true, 8, false);
            if (aods.Rows.Count < offset + 1)
            {
                return DateTime.Parse(aods.Rows[0]["asOfDateDescript"].ToString());
            }
            else
            {
                return DateTime.Parse(aods.Rows[offset]["asOfDateDescript"].ToString());
            }
        }

        [HttpGet]
        public DataTable CreditUnionStatus()
        {
            if (Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() != "")
            {
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine("SELECT InstType FROM DWR_FedRates.dbo.DatabaseHighlineMapping WHERE [DatabaseName] = '" + Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() + "'");
                return Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());
            }
            else
            {
                DataTable tmpTable = new DataTable();
                tmpTable.Columns.Add("InstType");
                DataRow newRow = tmpTable.NewRow();
                newRow["InstType"] = "BK";
                tmpTable.Rows.Add(newRow);
                return tmpTable;
            }
        }

        //Get Status of Database 
        [HttpGet]
        public DataTable DatabaseStatus()
        {
            if (Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() != "")
            {
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine("SELECT atlas_lock FROM [DWR_FedRates].[dbo].[databasehighlinemapping] WHERE databaseNAme = '" + Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() + "' ");
                DataTable tmp = Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());
                if (tmp.Rows.Count != 0)
                {
                    tmp.Columns[0].ReadOnly = false;
                    if (tmp.Rows[0][0].ToString() == "")
                    {
                        tmp.Rows[0][0] = "Not Locked";
                    }

                    return tmp;
                }
            }

            DataTable tmpTable = new DataTable();
            tmpTable.Columns.Add("atlas_lock");
            DataRow newRow = tmpTable.NewRow();
            newRow["atlas_lock"] = "Not Locked";
            tmpTable.Rows.Add(newRow);
            return tmpTable;


        }


        //Set Database Status
        [HttpGet]
        public DataTable SetDatabaseStatus(bool lockDatabase)
        {
            StringBuilder sqlQuery = new StringBuilder();
            LogEvent("Is Locking Database: " + lockDatabase.ToString(), "Landing");
            if (lockDatabase)
            {
                sqlQuery.AppendLine(" UPDATE [DWR_Fedrates].[dbo].[databasehighlinemapping] SET atlas_lock = 'Atlas_" + Utilities.GetUserNameNoEmail() + "' WHERE databaseName  = '" + Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString() + "'");
            }
            else
            {
                sqlQuery.AppendLine(" UPDATE [DWR_Fedrates].[dbo].[databasehighlinemapping] SET atlas_lock = 'Not Locked'");
            }

            if (Utilities.ExecuteCommand(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString()))
            {
                return DatabaseStatus();
            }
            else
            {
                throw new Exception("Error connection to database, contact support.");
            }

        }


        //Get Atlas Template by ID
        [HttpGet]
        public DataTable GetAtlasTemplateById(int id)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("SELECT [id] ,[name] FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Packages] WHERE asOfDate = '" + Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString() + "' AND Id = " + id);
            return Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());
        }

        //Get Atlas Templates
        [HttpGet]
        public DataTable GetAtlasTemplates()
        {
            //Gets Templates Based off Of As Of Date
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("SELECT [id] ,[name] FROM [DDW_Atlas_Templates].[dbo].[ATLAS_Packages] WHERE asOfDate = '" + Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString() + "'");

            return Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDwConnection"].ToString(), sqlQuery.ToString());

        }

        //Copy Basic Surplus, used directly and also in RollForward
        [HttpGet]
        public int CopyBasicSurplus(int basicSurplusId, string newName)
        {
            InstitutionService instServ = new InstitutionService("");
            PackageService ps = new PackageService();
            string constr = Utilities.BuildInstConnectionString("");
            string aod = Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString();
            string infoId = instServ.GetInformation(DateTime.Parse(aod), 0).Id.ToString();
            Dictionary<string, string> otherLiterals = new Dictionary<string, string>();
            string db = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            int newPriority = int.Parse(Utilities.ExecuteSql(constr, "SELECT COUNT(Id) FROM ATLAS_BasicSurplusAdmin WHERE informationId = " + infoId).Rows[0][0].ToString());
            otherLiterals.Add("InformationId", infoId);
            otherLiterals.Add("Priority", newPriority.ToString());
            otherLiterals.Add("Name", newName);

            string id = ps.NewCopyReportingItem("BasicSurplusAdmin", basicSurplusId.ToString(), "", db, db, otherLiterals, new Dictionary<string, string>(), new Dictionary<string, string>(), false);
            return Convert.ToInt32(id);
            //return CopyBasicSurplus(basicSurplusId, new InstitutionService("").GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), 0).Id);

        }
        [HttpGet]
        public int CopyBasicSurplus(int basicSurplusId, int newInformationId)
        {
            //Connection String
            string constr = Utilities.BuildInstConnectionString("");

            StringBuilder sqlQuery = new StringBuilder();

            //Get new priority
            int newPriority = int.Parse(Utilities.ExecuteSql(constr, "SELECT COUNT(Id) FROM ATLAS_BasicSurplusAdmin WHERE informationId = " + newInformationId).Rows[0][0].ToString());

            //Insert main Basic Suplus Parent Object
            sqlQuery.Clear();
            sqlQuery.AppendLine(" DECLARE @TempBSOutput TABLE(Id int); ");
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdmin] ");
            sqlQuery.AppendLine("            ([InformationId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine("            ,[LeftS1HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValUSTAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValMBSAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColLessSecPledgedOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColLessSecPledgedTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColUSTAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColMBSAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1BlankTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1BlankTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1BlankTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS1LiqAssetTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1LiqAssetTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1LiqAssetTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2MatUnsecLiabOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2MatUnsecLiabTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2MatUnsecLiabAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2DepCovOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2DepCovTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2DepCovOption] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepPct] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclBrokeredOneWay] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclBrokeredRecip] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclPublicDeposits] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclNationalCDs] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatPct] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatPct] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepPct] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2AddLiqResOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2AddLiqResTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2AddLiqResAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabInclPublicCD] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabInclPublicJumboCD] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabInclPublicDDANOWMMDA] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2SubHeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2SubHeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColAmountPledged] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColCollateralValue] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColNetCollateral] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColL360Collateral] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColAmountPledged] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColCollateralValue] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColNetCollateral] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColL360Collateral] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColAmountPledged] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColCollateralValue] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColNetCollateral] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColL360Collateral] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBType] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBPct] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBL360] ");
            sqlQuery.AppendLine("            ,[LeftS3ExcessLoanColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3ExcessLoanColTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3ExcessLoanColAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorCapOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorCapTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorCapAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3ColEncOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3ColEncTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3ColEncAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3SubHeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3SubHeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapPct] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapAmount] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalExclBrokeredOneWay] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalExclBrokeredRecip] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalInclNationalCDs] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS4SubHeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4SubHeaderTitle] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusAmount] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusPercent] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusFHLBAmount] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusFHLBPercent] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusBrokAmount] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusBrokPercent] ");
            sqlQuery.AppendLine("            ,[RightS1HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS1HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalOverride] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalTitle] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalMarketValue] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalPledged] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalAvailable] ");
            sqlQuery.AppendLine("            ,[RightS2HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS2HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalOverride] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalTitle] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalLine] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalOutstanding] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalAvailable] ");
            sqlQuery.AppendLine("            ,[RightS3HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS3HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalOverride] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalTitle] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalLine] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalOutstanding] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalAvailable] ");
            sqlQuery.AppendLine("            ,[RightS4HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS4HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS5HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS5HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS5AuthBorCapTitle] ");
            sqlQuery.AppendLine("            ,[RightS5AuthBorCapAmount] ");
            sqlQuery.AppendLine("            ,[RightS6HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS6HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingOverride] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingTitle] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingCurrentQuarter] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingPreviousQuarter] ");
            sqlQuery.AppendLine("            ,[RightS5BorCapsPolicyLimit] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValMBSPvt] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColMBSPvt] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepType] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepPolicyLimit]) ");
            sqlQuery.AppendLine("     OUTPUT INSERTED.Id INTO @TempBSOutput "); //Need to store the new Id, may be copying into same AsOfDate
            sqlQuery.AppendLine("     SELECT ");
            sqlQuery.AppendLine(" 		      " + newInformationId + " as [InformationId] ");
            sqlQuery.AppendLine("            ,[InstitutionDatabaseName] ");
            sqlQuery.AppendLine("            ,[AsOfDateOffset] ");
            sqlQuery.AppendLine("            ,[SimulationTypeId] ");
            sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ," + newPriority + " as [Priority] ");
            sqlQuery.AppendLine("            ,[LeftS1HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValUSTAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValMBSAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColLessSecPledgedOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColLessSecPledgedTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColUSTAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColMBSAgency] ");
            sqlQuery.AppendLine("            ,[LeftS1BlankTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1BlankTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1BlankTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS1LiqAssetTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS1LiqAssetTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS1LiqAssetTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2MatUnsecLiabOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2MatUnsecLiabTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2MatUnsecLiabAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2DepCovOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2DepCovTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2DepCovOption] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepPct] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclBrokeredOneWay] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclBrokeredRecip] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclPublicDeposits] ");
            sqlQuery.AppendLine("            ,[LeftS2TotalDepExclNationalCDs] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatPct] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2RegCDMatAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatPct] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2JumboCDMatAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepPct] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2OtherDepAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2AddLiqResOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2AddLiqResTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2AddLiqResAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabInclPublicCD] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabInclPublicJumboCD] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabInclPublicDDANOWMMDA] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS2VolLiabTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS2SubHeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS2SubHeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColAmountPledged] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColCollateralValue] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColNetCollateral] ");
            sqlQuery.AppendLine("            ,[LeftS3FamResReColL360Collateral] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColAmountPledged] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColCollateralValue] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColNetCollateral] ");
            sqlQuery.AppendLine("            ,[LeftS3HELOCOtherColL360Collateral] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColAmountPledged] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColCollateralValue] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColNetCollateral] ");
            sqlQuery.AppendLine("            ,[LeftS3CREColL360Collateral] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBType] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBPct] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorLineFHLBAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanColFHLBL360] ");
            sqlQuery.AppendLine("            ,[LeftS3ExcessLoanColOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3ExcessLoanColTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3ExcessLoanColAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorCapOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorCapTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3MaxBorCapAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3ColEncOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3ColEncTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3ColEncAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS3LoanTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS3SubHeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS3SubHeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4HeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4HeaderTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapPct] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepCapAmount] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalExclBrokeredOneWay] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalExclBrokeredRecip] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepBalInclNationalCDs] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepTotalOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepTotalTitle] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepTotalAmount] ");
            sqlQuery.AppendLine("            ,[LeftS4SubHeaderOverride] ");
            sqlQuery.AppendLine("            ,[LeftS4SubHeaderTitle] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusAmount] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusPercent] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusFHLBAmount] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusFHLBPercent] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusBrokAmount] ");
            sqlQuery.AppendLine("            ,[MidS1BasicSurplusBrokPercent] ");
            sqlQuery.AppendLine("            ,[RightS1HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS1HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalOverride] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalTitle] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalMarketValue] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalPledged] ");
            sqlQuery.AppendLine("            ,[RightS1OtherLiqTotalAvailable] ");
            sqlQuery.AppendLine("            ,[RightS2HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS2HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalOverride] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalTitle] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalLine] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalOutstanding] ");
            sqlQuery.AppendLine("            ,[RightS2UnsecBorTotalAvailable] ");
            sqlQuery.AppendLine("            ,[RightS3HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS3HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalOverride] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalTitle] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalLine] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalOutstanding] ");
            sqlQuery.AppendLine("            ,[RightS3SecBorTotalAvailable] ");
            sqlQuery.AppendLine("            ,[RightS4HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS4HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS5HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS5HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS5AuthBorCapTitle] ");
            sqlQuery.AppendLine("            ,[RightS5AuthBorCapAmount] ");
            sqlQuery.AppendLine("            ,[RightS6HeaderOverride] ");
            sqlQuery.AppendLine("            ,[RightS6HeaderTitle] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingOverride] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingTitle] ");
            sqlQuery.AppendLine("            ,[RightS6PubDepReqPledgingCurrentQuarter] ");
            //If AsOfDate is different, we want next quarter's PreviousQuarter to be our CurrentQuarter! What?
            string PrevQuarterField = (new InstitutionService("").GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), 0).Id == newInformationId)
                ? "RightS6PubDepReqPledgingPreviousQuarter"
                : "RightS6PubDepReqPledgingCurrentQuarter";
            sqlQuery.AppendLine("            ,[" + PrevQuarterField + "] ");
            sqlQuery.AppendLine("            ,[RightS5BorCapsPolicyLimit] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecMktValMBSPvt] ");
            sqlQuery.AppendLine("            ,[LeftS1SecColTotalSecColMBSPvt] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepType] ");
            sqlQuery.AppendLine("            ,[LeftS4BrokDepPolicyLimit] ");
            sqlQuery.AppendLine(" 		   FROM [dbo].[ATLAS_BasicSurplusAdmin] WHERE Id = " + basicSurplusId + " ");
            sqlQuery.AppendLine("     SELECT TOP 1 Id FROM @TempBSOutput");

            //Get new It From Basic Surplus Table Now
            //int newId = int.Parse(Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_BasicSurplusAdmin WHERE informationId = " + newInformationId + " ORDER BY Id DESC").Rows[0][0].ToString());
            int newId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

            //Market Values
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusMarketValue] ");
            sqlQuery.AppendLine("            ([BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[TreasuryAgencyBonds] ");
            sqlQuery.AppendLine("            ,[MBSCMOAgency] ");
            sqlQuery.AppendLine("            ,[MBSCMOPvt] ");
            sqlQuery.AppendLine("            ,[CorporateSecurities] ");
            sqlQuery.AppendLine("            ,[MunicipalSecurities] ");
            sqlQuery.AppendLine("            ,[EquitySecurities] ");
            sqlQuery.AppendLine("            ,[OtherInvestments] ");
            sqlQuery.AppendLine("            ,[Total]) ");
            sqlQuery.AppendLine("        SELECT ");
            sqlQuery.AppendLine(" 			" + newId + " ");
            sqlQuery.AppendLine("            ,[Name] ");
            sqlQuery.AppendLine("            ,[TreasuryAgencyBonds] ");
            sqlQuery.AppendLine("            ,[MBSCMOAgency] ");
            sqlQuery.AppendLine("            ,[MBSCMOPvt] ");
            sqlQuery.AppendLine("            ,[CorporateSecurities] ");
            sqlQuery.AppendLine("            ,[MunicipalSecurities] ");
            sqlQuery.AppendLine("            ,[EquitySecurities] ");
            sqlQuery.AppendLine("            ,[OtherInvestments] ");
            sqlQuery.AppendLine("            ,[Total] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusMarketValue] WHERE BasicSurplusAdminId = " + basicSurplusId + " ");
            //Insert Market Values
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Loop Through Collateral Types (sneakily named ATLAS_CollateralType because it used to be common but is now per-BS)
            DataTable colTypes = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_CollateralType WHERE BasicSurplusAdminId = " + basicSurplusId);
            Dictionary<int, int> oldColTypeIds = new Dictionary<int, int>();
            foreach (DataRow dr in colTypes.Rows)
            {
                int colTypeId = int.Parse(dr[0].ToString());
                sqlQuery.Clear();
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_CollateralType]");
                sqlQuery.AppendLine("           ([Name]");
                sqlQuery.AppendLine("           ,[CollateralValue]");
                sqlQuery.AppendLine("           ,[AllowCollateralValueEdit]");
                sqlQuery.AppendLine("           ,[BasicSurplusAdminId]");
                sqlQuery.AppendLine("           ,[Override]");
                sqlQuery.AppendLine("           ,[FieldName]");
                sqlQuery.AppendLine("           ,[Priority]");
                sqlQuery.AppendLine("           ,[Enabled])");
                sqlQuery.AppendLine("     SELECT");
                sqlQuery.AppendLine("           [Name]");
                sqlQuery.AppendLine("           ,[CollateralValue]");
                sqlQuery.AppendLine("           ,[AllowCollateralValueEdit]");
                sqlQuery.AppendLine("           ," + newId + " AS [BasicSurplusAdminId] ");
                sqlQuery.AppendLine("           ,[Override]");
                sqlQuery.AppendLine("           ,[FieldName]");
                sqlQuery.AppendLine("           ,[Priority]");
                sqlQuery.AppendLine("           ,[Enabled]");
                sqlQuery.AppendLine("     FROM [dbo].[ATLAS_CollateralType] WHERE Id = " + colTypeId);
                sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId;");
                int newColTypeId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());
                oldColTypeIds.Add(newColTypeId, colTypeId);
            }

            //Loop Through Secured Liabilities
            DataTable securedLiabs = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_BasicSurplusSecuredLiabilitiesType WHERE BasicSurplusAdminId = " + basicSurplusId + "");
            foreach (DataRow dr in securedLiabs.Rows)
            {
                int securedId = int.Parse(dr[0].ToString());
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusSecuredLiabilitiesType] ");
                sqlQuery.AppendLine("            ([BasicSurplusAdminId] ");
                sqlQuery.AppendLine("            ,[SecuredLiabilitiesTypeId] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                sqlQuery.AppendLine("            ,[Override] ");
                sqlQuery.AppendLine("            ,[Outstanding] ");
                sqlQuery.AppendLine("            ,[PledgingFactor] ");
                sqlQuery.AppendLine("            ,[PledgingRequired]) ");
                sqlQuery.AppendLine("   SELECT ");
                sqlQuery.AppendLine("            " + newId + " ");
                sqlQuery.AppendLine("            ,[SecuredLiabilitiesTypeId] ");
                sqlQuery.AppendLine("            ,[Priority] ");
                sqlQuery.AppendLine("            ,[Override] ");
                sqlQuery.AppendLine("            ,[Outstanding] ");
                sqlQuery.AppendLine("            ,[PledgingFactor] ");
                sqlQuery.AppendLine("            ,[PledgingRequired] ");
                sqlQuery.AppendLine("   FROM [dbo].[ATLAS_BasicSurplusSecuredLiabilitiesType] WHERE Id = " + securedId);
                sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId;");
                int newSecureId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

                //Update Basic Surplus Collateral Types based on new Collateral Type ID
                DataTable newColTypes = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_CollateralType WHERE BasicSurplusAdminId = " + newId);
                foreach (DataRow drSub in newColTypes.Rows)
                {
                    int newColTypeId = int.Parse(drSub[0].ToString());
                    int colTypeId = oldColTypeIds[newColTypeId];
                    sqlQuery.Clear();
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusCollateralType] ");
                    sqlQuery.AppendLine("            ([BasicSurplusSecuredLiabilitiesTypeId] ");
                    sqlQuery.AppendLine("            ,[CollateralTypeId] ");
                    sqlQuery.AppendLine("            ,[Priority] ");
                    sqlQuery.AppendLine("            ,[AmountPledgedMktVal] ");
                    sqlQuery.AppendLine("            ,[CollateralValuePercentage] ");
                    sqlQuery.AppendLine("            ,[NetCollateralValue] ");
                    sqlQuery.AppendLine("            ,[MaxCollateralUsed] ");
                    sqlQuery.AppendLine("            ,[RemainingPledgingRequired] ");
                    sqlQuery.AppendLine("            ,[ExcessCollateralMktVal] ");
                    sqlQuery.AppendLine("            ,[OverCollateralizedBasicSurplus] ");
                    sqlQuery.AppendLine("            ,[PledgingRequired]) ");
                    sqlQuery.AppendLine(" SELECT ");
                    sqlQuery.AppendLine("            " + newSecureId + " AS [BasicSurplusSecuredLiabilitiesTypeId] ");
                    sqlQuery.AppendLine("            ," + newColTypeId + " AS [CollateralTypeId] ");
                    sqlQuery.AppendLine("            ,[Priority] ");
                    sqlQuery.AppendLine("            ,[AmountPledgedMktVal] ");
                    sqlQuery.AppendLine("            ,[CollateralValuePercentage] ");
                    sqlQuery.AppendLine("            ,[NetCollateralValue] ");
                    sqlQuery.AppendLine("            ,[MaxCollateralUsed] ");
                    sqlQuery.AppendLine("            ,[RemainingPledgingRequired] ");
                    sqlQuery.AppendLine("            ,[ExcessCollateralMktVal] ");
                    sqlQuery.AppendLine("            ,[OverCollateralizedBasicSurplus] ");
                    sqlQuery.AppendLine("            ,[PledgingRequired] ");
                    sqlQuery.AppendLine("   FROM [dbo].[ATLAS_BasicSurplusCollateralType] WHERE BasicSurplusSecuredLiabilitiesTypeId = " + securedId + " AND CollateralTypeId = " + colTypeId);
                    Utilities.ExecuteSql(constr, sqlQuery.ToString());
                }
            }

            //Admin Overnight Funds
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminOvernightFund] ");
            sqlQuery.AppendLine("            ([Balance] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		   [Balance] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 		FROM [dbo].[ATLAS_BasicSurplusAdminOvernightFund] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Security Collateral
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminSecurityCollateral] ");
            sqlQuery.AppendLine("            ([USTAgency] ");
            sqlQuery.AppendLine("            ,[MBSAgency] ");
            sqlQuery.AppendLine("            ,[MBSPvt] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[USTAgency] ");
            sqlQuery.AppendLine("            ,[MBSAgency] ");
            sqlQuery.AppendLine("            ,[MBSPvt] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminSecurityCollateral]  WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Liquid Asset
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminLiquidAsset] ");
            sqlQuery.AppendLine("            ([Percent] ");
            sqlQuery.AppendLine("            ,[Amount] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		    [Percent] ");
            sqlQuery.AppendLine("            ,[Amount] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminLiquidAsset] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Volatile Liability
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminVolatileLiability] ");
            sqlQuery.AppendLine("            ([Percent] ");
            sqlQuery.AppendLine("            ,[Amount] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Percent] ");
            sqlQuery.AppendLine("            ,[Amount] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminVolatileLiability] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Loan Collateral
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminLoanCollateral] ");
            sqlQuery.AppendLine("            ([AmountPledged] ");
            sqlQuery.AppendLine("            ,[CollateralValue] ");
            sqlQuery.AppendLine("            ,[NetCollateral] ");
            sqlQuery.AppendLine("            ,[L360Collateral] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine("    SELECT ");
            sqlQuery.AppendLine(" 			[AmountPledged] ");
            sqlQuery.AppendLine("            ,[CollateralValue] ");
            sqlQuery.AppendLine("            ,[NetCollateral] ");
            sqlQuery.AppendLine("            ,[L360Collateral] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminLoanCollateral] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Loan
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminLoan] ");
            sqlQuery.AppendLine("            ([Amount] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Amount] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminLoan] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Brokered DEposit
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminBrokeredDeposit] ");
            sqlQuery.AppendLine("            ([Amount] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Amount] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminBrokeredDeposit] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + "  ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Misc
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminMisc] ");
            sqlQuery.AppendLine("            ([Amount] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Amount] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminMisc] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + "  ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());


            //Admin Other Liquidity
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminOtherLiquidity] ");
            sqlQuery.AppendLine("            ([MarketValue] ");
            sqlQuery.AppendLine("            ,[Pledged] ");
            sqlQuery.AppendLine("            ,[Available] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine("    SELECT ");
            sqlQuery.AppendLine(" 			[MarketValue] ");
            sqlQuery.AppendLine("            ,[Pledged] ");
            sqlQuery.AppendLine("            ,[Available] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminOtherLiquidity] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Unsercured borrowing
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminUnsecuredBorrowing] ");
            sqlQuery.AppendLine("            ([Line] ");
            sqlQuery.AppendLine("            ,[Outstanding] ");
            sqlQuery.AppendLine("            ,[Available] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Line] ");
            sqlQuery.AppendLine("            ,[Outstanding] ");
            sqlQuery.AppendLine("            ,[Available] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminUnsecuredBorrowing] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Secured Borrowing
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminSecuredBorrowing] ");
            sqlQuery.AppendLine("            ([Line] ");
            sqlQuery.AppendLine("            ,[Outstanding] ");
            sqlQuery.AppendLine("            ,[Available] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Line] ");
            sqlQuery.AppendLine("            ,[Outstanding] ");
            sqlQuery.AppendLine("            ,[Available] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminSecuredBorrowing] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Other Secured
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminOtherSecured] ");
            sqlQuery.AppendLine("            ([Amount] ");
            sqlQuery.AppendLine("            ,[Percent] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Amount] ");
            sqlQuery.AppendLine("            ,[Percent] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminOtherSecured] WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Borrowing Capacity
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminBorrowingCapacity] ");
            sqlQuery.AppendLine("            ([Type] ");
            sqlQuery.AppendLine("            ,[Percent] ");
            sqlQuery.AppendLine("            ,[Amount] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Type] ");
            sqlQuery.AppendLine("            ,[Percent] ");
            sqlQuery.AppendLine("            ,[Amount] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminBorrowingCapacity]  WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            //Admin Foot notes
            sqlQuery.Clear();
            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_BasicSurplusAdminFootnote] ");
            sqlQuery.AppendLine("            ([Text] ");
            sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority]) ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 			[Text] ");
            sqlQuery.AppendLine("            ," + newId + " as [BasicSurplusAdminId] ");
            sqlQuery.AppendLine("            ,[Override] ");
            sqlQuery.AppendLine("            ,[Title] ");
            sqlQuery.AppendLine("            ,[FieldName] ");
            sqlQuery.AppendLine("            ,[Priority] ");
            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_BasicSurplusAdminFootnote]  WHERE [BasicSurplusAdminId] =  " + basicSurplusId + " ");
            Utilities.ExecuteSql(constr, sqlQuery.ToString());

            return newId;
        }

        [HttpGet]
        public string NewCopyPackage(string pkgIdToCopy, bool copyFromTemplates, string pkgName)
        {
            var bsAdmins = BasicSurplusAdmins();
            //extra check to make sure they ahve a BSAdmin set up
            if (bsAdmins.Count() == 0)
            {
                return "-1";
            }
            else
            {
                PackageService ps = new PackageService();
                //ps.NewCopyPackage(pkgIdToCopy, asOfDate);
                return ps.NewCopyPackage(pkgIdToCopy, copyFromTemplates, pkgName);
            }

        }

        [HttpGet]
        public bool NewRollDate(DateTime asOfDate)
        {
            //Connection String
            string constr = Utilities.BuildInstConnectionString("");
            PackageService ps = new PackageService();

            //Get Information For As Of Date to Roll To
            var instService = new InstitutionService("");
            string curAOD = Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString();
            var newInfo = instService.GetInformation(asOfDate, 0);
            var oldInfo = instService.GetInformation(DateTime.Parse(curAOD), 0);
            string srcDB = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            DataTable idsToCopy = new DataTable();
            DataTable tempDataSet = new DataTable();
            Dictionary<string, string> otherLiterals = new Dictionary<string, string>();
            Dictionary<string, string> bsIdLookup = new Dictionary<string, string>();
            Dictionary<string, string> lbIdLookup = new Dictionary<string, string>();

            StringBuilder qry = new StringBuilder();

            //Basic Surplus Admin

            //Delete first
            tempDataSet = Utilities.ExecuteSql(constr, "SELECT Id from ATLAS_BasicSurplusAdmin WHERE informationId = " + newInfo.Id.ToString());
            foreach (DataRow dr in tempDataSet.Rows)
            {
                ps.DeleteReportingItem("BasicSurplusAdmin", dr[0].ToString(), srcDB, "");
            }
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id from ATLAS_BasicSurplusAdmin WHERE informationId = " + oldInfo.Id.ToString());
            otherLiterals.Add("InformationId", newInfo.Id.ToString());
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.NewCopyReportingItem("BasicSurplusAdmin", dr["Id"].ToString(), "", srcDB, srcDB, otherLiterals, new Dictionary<string, string>(), new Dictionary<string, string>(), true);
            }
            otherLiterals.Clear();
            //Policies

            //basic surpluses have unique names so we can look at the just inserted bs admins and find which one matches the selected one in the current policies
            string bsId = "";
            qry.AppendLine("SELECT Id FROM ATLAS_BasicSurplusAdmin WHERE [Name] = (");
            qry.AppendLine("SELECT [Name] FROM ATLAS_BasicSurplusAdmin WHERE Id = (");
            qry.AppendLine("SELECT BasicSurplusAdminId from ATLAS_Policy WHERE CAST(AsOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            qry.AppendLine(")) AND InformationId = " + newInfo.Id.ToString());
            tempDataSet = Utilities.ExecuteSql(constr, qry.ToString());
            if (tempDataSet.Rows.Count > 0)
            {
                bsId = tempDataSet.Rows[0][0].ToString();
            }
            else
            {
                //should probably roll back and error out or something
            }

            //populate this dictionary for use when rolling packages
            qry.Clear();
            qry.AppendLine("SELECT oldBS.Id as oldId, newBS.Id as newId FROM atlas_basicsurplusadmin oldBS ");
            qry.AppendLine("INNER JOIN  (select Id, InformationId, Name FROM ATLAS_BasicSurplusAdmin WHERE InformationId = " + newInfo.Id + ") as newBS");
            qry.AppendLine("ON oldBS.Name = newBS.Name");
            qry.AppendLine("WHERE oldBS.InformationId = " + oldInfo.Id);
            tempDataSet = Utilities.ExecuteSql(constr, qry.ToString());
            foreach (DataRow dr in tempDataSet.Rows)
            {
                bsIdLookup.Add(dr["oldId"].ToString(), dr["newId"].ToString());
            }

            otherLiterals.Add("BasicSurplusAdminId", bsId);
            if (newInfo.AsOfDate >= DateTime.Now)
            {
                otherLiterals.Add("MeetingDate", newInfo.AsOfDate.AddHours(4).ToString());
            }
            else
            {
                otherLiterals.Add("MeetingDate", DateTime.Now.AddHours(4).ToString());
            }
            //otherLiterals.Add("MeetingDate", newInfo.AsOfDate.ToString());
            otherLiterals.Add("AsOfDate", newInfo.AsOfDate.ToString());
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id from ATLAS_Policy WHERE CAST(AsOfDate as date) = '" + newInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.DeleteReportingItem("Policy", dr[0].ToString(), srcDB, "");
            }
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id from ATLAS_Policy WHERE CAST(AsOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.NewCopyReportingItem("Policy", dr[0].ToString(), "", srcDB, srcDB, otherLiterals, new Dictionary<string, string>(), new Dictionary<string, string>(), false);
            }

            otherLiterals.Clear();
            //Model Setup
            //delete first just in case
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id FROM ATLAS_ModelSetup WHERE CAST(AsOfDate as date) = '" + newInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.DeleteReportingItem("ModelSetup", dr[0].ToString(), srcDB, "");
            }

            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id FROM ATLAS_ModelSetup WHERE CAST(AsOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            otherLiterals.Add("AsOfDate", newInfo.AsOfDate.ToString());
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.NewCopyReportingItem("ModelSetup", dr[0].ToString(), "", srcDB, srcDB, otherLiterals, new Dictionary<string, string>(), new Dictionary<string, string>(), false);
            }
            otherLiterals.Clear();
            //Liability Pricing
            otherLiterals.Add("AsOfDate", newInfo.AsOfDate.ToString());
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id from ATLAS_LiabilityPricing WHERE CAST(AsOfDate as date) = '" + newInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.DeleteReportingItem("LiabilityPricing", dr[0].ToString(), srcDB, "");
            }
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id from ATLAS_LiabilityPricing WHERE CAST(AsOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.NewCopyReportingItem("LiabilityPricing", dr[0].ToString(), "", srcDB, srcDB, otherLiterals, new Dictionary<string, string>(), new Dictionary<string, string>(), false);
            }
            otherLiterals.Clear();
            //LookBack
            otherLiterals.Add("AsOfDate", newInfo.AsOfDate.ToString());
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id FROM ATLAS_LBLookback WHERE CAST(AsOfDate as date) = '" + newInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.DeleteReportingItem("LBLookback", dr[0].ToString(), srcDB, "");
            }
            idsToCopy = Utilities.ExecuteSql(constr, "SELECT Id FROM ATLAS_LBLookback WHERE CAST(AsOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in idsToCopy.Rows)
            {
                ps.NewCopyReportingItem("LBLookback", dr[0].ToString(), "", srcDB, srcDB, otherLiterals, new Dictionary<string, string>(), new Dictionary<string, string>(), false);
            }

            //populate this dictionary for use when rolling packages
            qry.Clear();
            qry.AppendLine("SELECT oldLB.Id as oldId, newLB.Id as newId from ATLAS_LBLookback as oldLB ");
            qry.AppendLine("INNER JOIN (SELECT Id, Name FROM ATLAS_LBLookback where cast(asofdate as date) = '" + newInfo.AsOfDate.ToShortDateString() + "') as newLB");
            qry.AppendLine("ON oldLB.Name = newLB.Name");
            qry.AppendLine("WHERE cast(oldLB.AsOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            tempDataSet = Utilities.ExecuteSql(constr, qry.ToString());
            foreach (DataRow dr in tempDataSet.Rows)
            {
                lbIdLookup.Add(dr["oldId"].ToString(), dr["newId"].ToString());
            }

            //Packages
            //We need to somehow get the rolled reports to look at the rolled Lookbacks and the rolled basic surpluses

            //Delete whatever packages might be in this asofdate already
            otherLiterals.Clear();
            tempDataSet = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_Packages WHERE CAST(asOfDate as date) = '" + newInfo.AsOfDate.ToShortDateString() + "'");
            foreach (DataRow dr in tempDataSet.Rows)
            {
                ps.DeleteReportingItem("Package", dr[0].ToString(), srcDB, "");
            }

            idsToCopy = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_Packages WHERE CAST(asOfDate as date) = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
            otherLiterals.Add("AsOfDate", newInfo.AsOfDate.ToShortDateString());
            foreach (DataRow dr in idsToCopy.Rows)
            {
                //ps.NewCopyReportingItem("Policy", dr["id"].ToString(), "", srcDB, srcDB, otherLiterals);
                ps.NewRollPackage(dr[0].ToString(), newInfo.AsOfDate.ToString(), srcDB, srcDB, bsIdLookup, lbIdLookup);
            }
            otherLiterals.Clear();
            //Set To New As Of Date
            //HttpContext.Current.Profile.SetPropertyValue("AtlasAsOfDate", asOfDate.ToShortDateString());
            string userName = Utilities.GetAtlasUserData().Rows[0]["UserName"].ToString();//HttpContext.Current.Profile.UserName;
            Utilities.SetUserData("asOfDate", asOfDate.ToShortDateString());
            return true;
        }

        //[HttpGet]
        //public bool RollDate(DateTime asOfDate)
        //{

        //    //Connection String
        //    string constr = Utilities.BuildInstConnectionString("");

        //    //Get Information For As Of Date to Roll To
        //    var instService = new InstitutionService("");
        //    var newInfo = instService.GetInformation(asOfDate, 0);
        //    var oldInfo = instService.GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), 0);

        //    StringBuilder sqlQuery = new StringBuilder();

        //    //Copy Over All Packages After We Delete them all
        //    DataTable existPackages = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_Packages WHERE asOfDate = '" + newInfo.AsOfDate.ToShortDateString() + "'");
        //    var packageService = new PackageService();
        //    foreach (DataRow dr in existPackages.Rows)
        //    {
        //        packageService.Delete(int.Parse(dr[0].ToString()));
        //    }

        //    DataTable pkgs = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_Packages WHERE asOfDate = '" + Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString() + "'");
        //    foreach (DataRow dr in pkgs.Rows)
        //    {
        //        packageService.CopyPackage(int.Parse(dr[0].ToString()), asOfDate.ToShortDateString());
        //    }

        //    //Basic Surplus Delete existing one First
        //    Utilities.ExecuteSql(constr, "DELETE FROM ATLAS_BasicSurplusAdmin WHERE informationId = " + newInfo.Id.ToString() + "");
        //    DataTable existingSurplus = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_BasicSurplusAdmin WHERE informationId = " + newInfo.Id.ToString() + "");
        //    DataTable oldSurplus = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_BasicSurplusAdmin WHERE informationId = " + oldInfo.Id.ToString() + "");

        //    int policyBasicSurlusAdminId = 0;
        //    int newPolicyBasicSurplusAdminId = 0;
        //    int oldPolId = 0;
        //    DataTable oldPol = Utilities.ExecuteSql(constr, "SELECT BasicSurplusAdminId, id FROM ATLAS_Policy WHERE asOfdate = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
        //    if (oldPol.Rows.Count > 0)
        //    {
        //        policyBasicSurlusAdminId = int.Parse(oldPol.Rows[0][0].ToString());
        //        oldPolId = int.Parse(oldPol.Rows[0][1].ToString());
        //    }

        //    //Only Do it if it does not exist yet
        //    if (existingSurplus.Rows.Count == 0 && oldSurplus.Rows.Count > 0)
        //    {
        //        foreach (DataRow drow in oldSurplus.Rows)
        //        {
        //            //Get Basic Suplus that we are copying 
        //            int oldBsId = int.Parse(drow[0].ToString());

        //            int newBsId = CopyBasicSurplus(oldBsId, newInfo.Id);

        //            if (newBsId == policyBasicSurlusAdminId)
        //                newPolicyBasicSurplusAdminId = newBsId;
        //        }
        //    }

        //    //Policies Delete Existing First
        //    Utilities.ExecuteSql(constr, "DELETE FROM ATLAS_Policy WHERE asOfDate = '" + newInfo.AsOfDate.ToShortDateString() + "'");
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_Policy] ");
        //    sqlQuery.AppendLine("            ([AsOfDate] ");
        //    sqlQuery.AppendLine("            ,[NIISimulationTypeId] ");
        //    sqlQuery.AppendLine("            ,[EveSimulationTypeId] ");
        //    sqlQuery.AppendLine("            ,[ReportedEveScenarioId] ");
        //    sqlQuery.AppendLine("            ,[UseDefault] ");
        //    sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
        //    sqlQuery.AppendLine("            ,[MeetingDate]) ");
        //    sqlQuery.AppendLine(" 	SELECT ");
        //    sqlQuery.AppendLine(" 			'" + newInfo.AsOfDate.ToShortDateString() + "' as asOfDate ");
        //    sqlQuery.AppendLine("            ,[NIISimulationTypeId] ");
        //    sqlQuery.AppendLine("            ,[EveSimulationTypeId] ");
        //    sqlQuery.AppendLine("            ,[ReportedEveScenarioId] ");
        //    sqlQuery.AppendLine("            ,[UseDefault] ");
        //    sqlQuery.AppendLine("            ," + newPolicyBasicSurplusAdminId.ToString() + " as [BasicSurplusAdminId] ");
        //    sqlQuery.AppendLine("            ,'" + newInfo.AsOfDate.ToShortDateString() + "' as [MeetingDate] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_Policy] WHERE asOfDate = '" + oldInfo.AsOfDate.ToShortDateString() + "' ");
        //    sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId;");
        //    int newPolId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //    //Loop Through Liquidity Policies
        //    DataTable liqPols = Utilities.ExecuteSql(constr, "SELECT id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE PolicyId = " + oldPolId.ToString() + "");
        //    foreach (DataRow dr in liqPols.Rows)
        //    {
        //        int liqId = int.Parse(dr[0].ToString());

        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy] ");
        //        sqlQuery.AppendLine("            ([PolicyId] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[WellCap] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[WellCapAvailable] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[FormatType]) ");
        //        sqlQuery.AppendLine(" SELECT ");
        //        sqlQuery.AppendLine(" 			" + newPolId.ToString() + " as PolicyId ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[WellCap] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[WellCapAvailable] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[FormatType] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_LiquidityPolicy] WHERE id = " + liqId.ToString() + " ");
        //        sqlQuery.AppendLine(" SELECT SCOPE_IDENTITY() as newId;");
        //        int newLiqId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ");
        //        sqlQuery.AppendLine("            ([LiquidityPolicyId] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine("            ,[Include] ");
        //        sqlQuery.AppendLine("            ,[Priority]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			  " + newLiqId.ToString() + " ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine("            ,[Include] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_LiquidityPolicySetting] WHERE LiquidityPolicyId =  " + liqId.ToString() + " ");
        //        Utilities.ExecuteSql(constr, sqlQuery.ToString());
        //    }

        //    //NII Policy Parents
        //    DataTable niis = Utilities.ExecuteSql(constr, "SELECT id FROM [dbo].[ATLAS_NIIPolicyParent] WHERE PolicyId = " + oldPolId.ToString() + " ");
        //    foreach (DataRow dr in niis.Rows)
        //    {
        //        int niiId = int.Parse(dr[0].ToString());
        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicyParent] ");
        //        sqlQuery.AppendLine("            ([PolicyId] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			" + newPolId.ToString() + " ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_NIIPolicyParent] WHERE id = " + niiId.ToString() + " ; ");
        //        sqlQuery.AppendLine("   SELECT SCOPE_IDENTITY() as newId;");
        //        int newniiId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ");
        //        sqlQuery.AppendLine("            ([NIIPolicyParentId] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			 " + newniiId.ToString() + " as id ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_NIIPolicy] WHERE NIIPolicyParentId = " + niiId + " ");
        //        Utilities.ExecuteSql(constr, sqlQuery.ToString());
        //    }

        //    //Core Fund Parents
        //    DataTable cfs = Utilities.ExecuteSql(constr, "SELECT id FROM [dbo].[ATLAS_CoreFundParent] WHERE PolicyId = " + oldPolId.ToString() + " ");
        //    foreach (DataRow dr in cfs.Rows)
        //    {
        //        int cfId = int.Parse(dr[0].ToString());
        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundParent] ");
        //        sqlQuery.AppendLine("            ([PolicyId] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			  " + newPolId.ToString() + " as id ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_CoreFundParent] WHERE id = " + cfId.ToString() + "; ");
        //        sqlQuery.AppendLine("   SELECT SCOPE_IDENTITY() as newId;");
        //        int newCfID = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ");
        //        sqlQuery.AppendLine("            ([CoreFundParentId] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			 " + newCfID.ToString() + " as id");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_CoreFundPolicy] WHERE CoreFundParentId = " + cfId.ToString() + " ");
        //        Utilities.ExecuteSql(constr, sqlQuery.ToString());
        //    }

        //    //Eve Parents
        //    DataTable eves = Utilities.ExecuteSql(constr, "SELECT id FROM [dbo].[ATLAS_EvePolicyParent] WHERE PolicyId = " + oldPolId.ToString() + " ");
        //    foreach (DataRow dr in eves.Rows)
        //    {
        //        int eveId = int.Parse(dr[0].ToString());
        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicyParent] ");
        //        sqlQuery.AppendLine("            ([PolicyId] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			" + newPolId.ToString() + " as id ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine("            ,[Name] ");
        //        sqlQuery.AppendLine("            ,[ShortName] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_EvePolicyParent] WHERE id = " + eveId.ToString() + "; ");
        //        sqlQuery.AppendLine("   SELECT SCOPE_IDENTITY() as newId;");
        //        int newEveID = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ");
        //        sqlQuery.AppendLine("            ([EvePolicyParentId] ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			" + newEveID.ToString() + " as id ");
        //        sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //        sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //        sqlQuery.AppendLine("            ,[Priority] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_EvePolicy] WHERE EvePolicyParentId = " + eveId.ToString() + "");
        //        Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    }


        //    //Capital Policies
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ");
        //    sqlQuery.AppendLine("            ([PolicyId] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[ShortName] ");
        //    sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //    sqlQuery.AppendLine("            ,[WellCap] ");
        //    sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //    sqlQuery.AppendLine("            ,[WellCapAvailable] ");
        //    sqlQuery.AppendLine("            ,[RiskAssesmentAvailable] ");
        //    sqlQuery.AppendLine("            ,[PolicyLimitAvailable] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine("            ,[CurrentRatioFormat] ");
        //    sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //    sqlQuery.AppendLine("            ,[FormatType]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			 " + newPolId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[ShortName] ");
        //    sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //    sqlQuery.AppendLine("            ,[WellCap] ");
        //    sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //    sqlQuery.AppendLine("            ,[WellCapAvailable] ");
        //    sqlQuery.AppendLine("            ,[RiskAssesmentAvailable] ");
        //    sqlQuery.AppendLine("            ,[PolicyLimitAvailable] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine("            ,[CurrentRatioFormat] ");
        //    sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //    sqlQuery.AppendLine("            ,[FormatType] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_CapitalPolicy] WHERE PolicyId = " + oldPolId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    //Other Policies
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_OtherPolicy] ");
        //    sqlQuery.AppendLine("            ([PolicyId] ");
        //    sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //    sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[ShortName] ");
        //    sqlQuery.AppendLine("            ,[FormatType]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			 " + newPolId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[PolicyLimit] ");
        //    sqlQuery.AppendLine("            ,[RiskAssessment] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine("            ,[CurrentRatio] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[ShortName] ");
        //    sqlQuery.AppendLine("            ,[FormatType] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_OtherPolicy] WHERE PolicyId = " + oldPolId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    //Model Setup
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" DELETE FROM ATLAS_ModelSetup WHERE asOFDate = '" + newInfo.AsOfDate.ToShortDateString() + "';");
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_ModelSetup] ");
        //    sqlQuery.AppendLine("            ([AsOfDate] ");
        //    sqlQuery.AppendLine("            ,[PrepaymentType] ");
        //    sqlQuery.AppendLine("            ,[OtherLoanPrepaymentType] ");
        //    sqlQuery.AppendLine("            ,[LoanRatesDate] ");
        //    sqlQuery.AppendLine("            ,[InvestmentType] ");
        //    sqlQuery.AppendLine("            ,[ReportTaxEquivalent] ");
        //    sqlQuery.AppendLine("            ,[AvgLifeAssumption] ");
        //    sqlQuery.AppendLine("            ,[AvgLifeYears] ");
        //    sqlQuery.AppendLine("            ,[AvgLifeMessage] ");
        //    sqlQuery.AppendLine("            ,[NetNewLoans] ");
        //    sqlQuery.AppendLine("            ,[NetNewDeposits]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			'" + newInfo.AsOfDate.ToShortDateString() + "' as asOfDate ");
        //    sqlQuery.AppendLine("            ,[PrepaymentType] ");
        //    sqlQuery.AppendLine("            ,[OtherLoanPrepaymentType] ");
        //    sqlQuery.AppendLine("            ,[LoanRatesDate] ");
        //    sqlQuery.AppendLine("            ,[InvestmentType] ");
        //    sqlQuery.AppendLine("            ,[ReportTaxEquivalent] ");
        //    sqlQuery.AppendLine("            ,[AvgLifeAssumption] ");
        //    sqlQuery.AppendLine("            ,[AvgLifeYears] ");
        //    sqlQuery.AppendLine("            ,[AvgLifeMessage] ");
        //    sqlQuery.AppendLine("            ,[NetNewLoans] ");
        //    sqlQuery.AppendLine("            ,[NetNewDeposits] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_ModelSetup] WHERE asOfDate = '" + oldInfo.AsOfDate.ToShortDateString() + "' ");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());


        //    int oldLpId = 0;
        //    DataTable oldLp = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_LiabilityPricing WHERE asOfDate = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
        //    if (oldLp.Rows.Count > 0)
        //    {
        //        oldLpId = int.Parse(oldLp.Rows[0][0].ToString());
        //    }


        //    //Liability Pricing Delete Existing First
        //    Utilities.ExecuteCommand(constr, "DELETE FROM [ATLAS_LiabilityPricing]  WHERE asOfDate = '" + newInfo.AsOfDate.ToShortDateString() + "'");
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LiabilityPricing] ");
        //    sqlQuery.AppendLine("            ([AsOfDate] ");
        //    sqlQuery.AppendLine("            ,[WebRateAsOfDate] ");
        //    sqlQuery.AppendLine("            ,[FHLBDistrict] ");
        //    sqlQuery.AppendLine("            ,[DepositRateAsOfDate] ");
        //    sqlQuery.AppendLine("            ,[HistoricalWebRateDate] ");
        //    sqlQuery.AppendLine("            ,[HistoricalRateCurve] ");
        //    sqlQuery.AppendLine("            ,[HistoricalRateEffDate]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			'" + newInfo.AsOfDate.ToShortDateString() + "' as asOfDate ");
        //    sqlQuery.AppendLine("            ,[WebRateAsOfDate] ");
        //    sqlQuery.AppendLine("            ,[FHLBDistrict] ");
        //    sqlQuery.AppendLine("            ,[DepositRateAsOfDate] ");
        //    sqlQuery.AppendLine("            ,[HistoricalWebRateDate] ");
        //    sqlQuery.AppendLine("            ,[HistoricalRateCurve] ");
        //    sqlQuery.AppendLine("            ,[HistoricalRateEffDate] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_LiabilityPricing] WHERE asOfDAte = '" + oldInfo.AsOfDate.ToShortDateString() + "'");
        //    sqlQuery.AppendLine("   SELECT SCOPE_IDENTITY() as newId;");
        //    int newLpId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //    //Non Maturity Deposit
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NonMaturityDepositRate] ");
        //    sqlQuery.AppendLine("            ([LiabilityPricingId] ");
        //    sqlQuery.AppendLine("            ,[Type] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine("            ,[Priority]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			" + newLpId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[Type] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_NonMaturityDepositRate] WHERE LiabilityPricingId = " + oldLpId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());


        //    //Time Deposit RAte
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ");
        //    sqlQuery.AppendLine("            ([LiabilityPricingId] ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Rate]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			" + newLpId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_TimeDepositRate]  WHERE LiabilityPricingId = " + oldLpId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    //Time Deposit Special Rate
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositSpecialRate] ");
        //    sqlQuery.AppendLine("            ([LiabilityPricingId] ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine("            ,[Priority]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			" + newLpId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_TimeDepositSpecialRate] WHERE LiabilityPricingId = " + oldLpId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    //Historiucal TIme Deposit Rate
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ");
        //    sqlQuery.AppendLine("            ([LiabilityPricingId] ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Rate]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			" + newLpId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_HistoricalTimeDepositlRate] WHERE LiabilityPricingId = " + oldLpId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    //Historical Time Depsit Special Rate
        //    sqlQuery.Clear();
        //    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositSpecialRate] ");
        //    sqlQuery.AppendLine("            ([LiabilityPricingId] ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine("            ,[Priority]) ");
        //    sqlQuery.AppendLine(" SELECT ");
        //    sqlQuery.AppendLine(" 			" + newLpId.ToString() + " as id ");
        //    sqlQuery.AppendLine("            ,[Term] ");
        //    sqlQuery.AppendLine("            ,[Name] ");
        //    sqlQuery.AppendLine("            ,[Rate] ");
        //    sqlQuery.AppendLine("            ,[Priority] ");
        //    sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_HistoricalTimeDepositSpecialRate] WHERE LiabilityPricingId = " + oldLpId.ToString() + "");
        //    Utilities.ExecuteSql(constr, sqlQuery.ToString());

        //    Utilities.ExecuteCommand(constr, "DELETE FROM [ATLAS_LBLookback] WHERE asOfDate = '" + newInfo.AsOfDate.ToShortDateString() + "' ");

        //    DataTable oldLookBack = Utilities.ExecuteSql(constr, "SELECT id FROM [dbo].[ATLAS_LBLookback] WHERE asOfDate = '" + oldInfo.AsOfDate.ToShortDateString() + "' ");
        //    int oldLookBackId = 0;
        //    if (oldLookBack.Rows.Count > 0)
        //    {
        //        oldLookBackId = int.Parse(oldLookBack.Rows[0][0].ToString());
        //    }


        //    if (oldLookBackId != 0)
        //    {
        //        //LookBack
        //        sqlQuery.Clear();
        //        sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LBLookback] ");
        //        sqlQuery.AppendLine("            ([Name] ");
        //        sqlQuery.AppendLine("            ,[AsOfDate] ");
        //        sqlQuery.AppendLine("            ,[SimulationTypeId] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId]) ");
        //        sqlQuery.AppendLine(" 	SELECT ");
        //        sqlQuery.AppendLine(" 			[Name] ");
        //        sqlQuery.AppendLine("            ,'" + newInfo.AsOfDate.ToShortDateString() + "' as asOfDate ");
        //        sqlQuery.AppendLine("            ,[SimulationTypeId] ");
        //        sqlQuery.AppendLine("            ,[ScenarioTypeId] ");
        //        sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_LBLookback] WHERE asOfDate = '" + oldInfo.AsOfDate.ToShortDateString() + "'; ");
        //        sqlQuery.AppendLine("   SELECT SCOPE_IDENTITY() as newId;");
        //        int newLookBackId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //        //ATLAS_LBLookbackGroup Loop through them
        //        DataTable lbgs = Utilities.ExecuteSql(constr, "SELECT id FROM ATLAS_LBLookbackGroup WHERE id = " + oldLookBackId.ToString() + "");

        //        foreach (DataRow dr in lbgs.Rows)
        //        {
        //            int oldLookBackGroupId = int.Parse(dr[0].ToString());

        //            sqlQuery.Clear();
        //            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LBLookbackGroup] ");
        //            sqlQuery.AppendLine("            ([LBLookbackId] ");
        //            sqlQuery.AppendLine("            ,[Priority] ");
        //            sqlQuery.AppendLine("            ,[LayoutId] ");
        //            sqlQuery.AppendLine("            ,[LBCustomTypeId] ");
        //            sqlQuery.AppendLine("            ,[StartDateOffset] ");
        //            sqlQuery.AppendLine("            ,[EndDateOffset] ");
        //            sqlQuery.AppendLine("            ,[RateVol] ");
        //            sqlQuery.AppendLine("            ,[TaxEquivalent] ");
        //            sqlQuery.AppendLine("            ,[StartDate] ");
        //            sqlQuery.AppendLine("            ,[EndDate] ");
        //            sqlQuery.AppendLine("            ,[StartToEndDays] ");
        //            sqlQuery.AppendLine("            ,[YearFromStartDays]) ");
        //            sqlQuery.AppendLine(" SELECT ");
        //            sqlQuery.AppendLine(" 			" + newLookBackId.ToString() + " as lookBack ");
        //            sqlQuery.AppendLine("            ,[Priority] ");
        //            sqlQuery.AppendLine("            ,[LayoutId] ");
        //            sqlQuery.AppendLine("            ,[LBCustomTypeId] ");
        //            sqlQuery.AppendLine("            ,[StartDateOffset] ");
        //            sqlQuery.AppendLine("            ,[EndDateOffset] ");
        //            sqlQuery.AppendLine("            ,[RateVol] ");
        //            sqlQuery.AppendLine("            ,[TaxEquivalent] ");
        //            sqlQuery.AppendLine("            ,[StartDate] ");
        //            sqlQuery.AppendLine("            ,[EndDate] ");
        //            sqlQuery.AppendLine("            ,[StartToEndDays] ");
        //            sqlQuery.AppendLine("            ,[YearFromStartDays] ");
        //            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_LBLookbackGroup] WHERE id = " + oldLookBackGroupId + " ");
        //            sqlQuery.AppendLine("   SELECT SCOPE_IDENTITY() as newId;");
        //            int newLookBackGroupId = int.Parse(Utilities.ExecuteSql(constr, sqlQuery.ToString()).Rows[0][0].ToString());

        //            sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LBLookbackData] ");
        //            sqlQuery.AppendLine("            ([LBLookbackGroupId] ");
        //            sqlQuery.AppendLine("            ,[Priority] ");
        //            sqlQuery.AppendLine("            ,[IsAsset] ");
        //            sqlQuery.AppendLine("            ,[Title] ");
        //            sqlQuery.AppendLine("            ,[Proj_AvgBal] ");
        //            sqlQuery.AppendLine("            ,[Proj_AvgBal_Adj] ");
        //            sqlQuery.AppendLine("            ,[Proj_AvgRate] ");
        //            sqlQuery.AppendLine("            ,[Proj_IncExp] ");
        //            sqlQuery.AppendLine("            ,[Proj_IncExp_Adj] ");
        //            sqlQuery.AppendLine("            ,[Actual_AvgBal] ");
        //            sqlQuery.AppendLine("            ,[Actual_AvgRate] ");
        //            sqlQuery.AppendLine("            ,[Actual_IncExp] ");
        //            sqlQuery.AppendLine("            ,[Variance_ValDueToRate] ");
        //            sqlQuery.AppendLine("            ,[Variance_ValDueToVol] ");
        //            sqlQuery.AppendLine("            ,[Variance_Val] ");
        //            sqlQuery.AppendLine("            ,[Variance_Perc] ");
        //            sqlQuery.AppendLine("            ,[IsSectionHeader] ");
        //            sqlQuery.AppendLine("            ,[IsTotalRow] ");
        //            sqlQuery.AppendLine("            ,[HideFromAdmin] ");
        //            sqlQuery.AppendLine("            ,[IsCalculatedRow] ");
        //            sqlQuery.AppendLine("            ,[RowType]) ");
        //            sqlQuery.AppendLine("     SELECT ");
        //            sqlQuery.AppendLine(" 			" + newLookBackGroupId.ToString() + " as id");
        //            sqlQuery.AppendLine("            ,[Priority] ");
        //            sqlQuery.AppendLine("            ,[IsAsset] ");
        //            sqlQuery.AppendLine("            ,[Title] ");
        //            sqlQuery.AppendLine("            ,[Proj_AvgBal] ");
        //            sqlQuery.AppendLine("            ,[Proj_AvgBal_Adj] ");
        //            sqlQuery.AppendLine("            ,[Proj_AvgRate] ");
        //            sqlQuery.AppendLine("            ,[Proj_IncExp] ");
        //            sqlQuery.AppendLine("            ,[Proj_IncExp_Adj] ");
        //            sqlQuery.AppendLine("            ,[Actual_AvgBal] ");
        //            sqlQuery.AppendLine("            ,[Actual_AvgRate] ");
        //            sqlQuery.AppendLine("            ,[Actual_IncExp] ");
        //            sqlQuery.AppendLine("            ,[Variance_ValDueToRate] ");
        //            sqlQuery.AppendLine("            ,[Variance_ValDueToVol] ");
        //            sqlQuery.AppendLine("            ,[Variance_Val] ");
        //            sqlQuery.AppendLine("            ,[Variance_Perc] ");
        //            sqlQuery.AppendLine("            ,[IsSectionHeader] ");
        //            sqlQuery.AppendLine("            ,[IsTotalRow] ");
        //            sqlQuery.AppendLine("            ,[HideFromAdmin] ");
        //            sqlQuery.AppendLine("            ,[IsCalculatedRow] ");
        //            sqlQuery.AppendLine("            ,[RowType] ");
        //            sqlQuery.AppendLine(" 	FROM [dbo].[ATLAS_LBLookbackData] WHERE LBLookbackGroupId = " + oldLookBackGroupId.ToString() + " ");
        //            Utilities.ExecuteSql(constr, sqlQuery.ToString());
        //        }

        //    }


        //    //Set To New As Of Date
        //    //HttpContext.Current.Profile.SetPropertyValue("AtlasAsOfDate", asOfDate.ToShortDateString());
        //    string userName = HttpContext.Current.Profile.UserName;
        //    Utilities.SetUserData("asOfDate", asOfDate.ToShortDateString());
        //    return true;
        //}

        //Profile InformationId (AsOfDate)
        [HttpGet]
        public int ProfileInformationId()
        {
            string conStr = Utilities.BuildInstConnectionString("");
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            int asOfDateId = (int)Utilities.ExecuteSql(conStr, "SELECT Id FROM asOfDateInfo WHERE asOfDate = '" + date.ToShortDateString() + "' AND BasisDate = 1").Rows[0][0];
            return asOfDateId;
        }

        //Basic Surplus Admins
        [HttpGet]
        public IQueryable<BasicSurplusAdmin> BasicSurplusAdmins()
        {
            string conStr = Utilities.BuildInstConnectionString("");
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            int asOfDateId = (int)Utilities.ExecuteSql(conStr, "SELECT Id FROM asOfDateInfo WHERE asOfDate = '" + date.ToShortDateString() + "' AND BasisDate = 1").Rows[0][0];
            return _contextProvider.Context.BasicSurplusAdmins
                .Where(s => s.InformationId == asOfDateId);
        }

        [HttpGet]
        public IQueryable<BasicSurplusAdmin> BasicSurplusAdminsByAsOfDate(string instName, string aod)
        {
            string conStr = Utilities.BuildInstConnectionString(instName);
            int asOfDateId = (int)Utilities.ExecuteSql(conStr, "SELECT Id FROM asOfDateInfo WHERE asOfDate = '" + aod + "' AND BasisDate = 1").Rows[0][0];
            return _contextProvider.Context.BasicSurplusAdmins
                .Where(s => s.InformationId == asOfDateId);
        }

        [HttpGet]
        public DataTable BasicSurplusMarketValGainLoss(string inst)
        {

            //Take current as of date user is in and look for policies for eve neve sim if no policies default to eve/neve
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());

            DataTable pol = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), "SELECT EveSimulationTypeId FROM Atlas_Policy WHERE asOfDate = '" + date.ToShortDateString() + "'");

            int simType = 2;

            if (pol.Rows.Count > 0)
            {
                simType = int.Parse(pol.Rows[0][0].ToString());
            }

            //Now that we have simulation type look up simulation id
            DataTable sim = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), "SELECT s.id FROM asOfDateInfo as aod INNER JOIN Simulations as s ON aod.id = s.InformationId AND aod.asOfDate = '" + date.ToShortDateString() + "' AND s.SimulationTypeId = " + simType + "");

            int simId = 0;

            if (sim.Rows.Count > 0)
            {
                simId = int.Parse(sim.Rows[0][0].ToString());
            }
            else
            {
                return null;
            }


            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	'AgencyBacked' as name ");
            sqlQuery.AppendLine(" 	,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 	,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 	,SUM(als.MVAmount) as mktValue ");
            sqlQuery.AppendLine(" 	FROM ");
            sqlQuery.AppendLine(" (SELECT code FROM Basis_ALB WHERE ((classOr IN (3,4) AND (ILPFlag = 0 OR Acc_Type <> Acc_TypeOR)) OR ((SUBSTRING ( WizCode ,5 , 1 ) in ('c','g','d','h','e','i', 'j', 'r', 'f', 's') and SUBSTRING ( WizCode ,10 , 1 ) in ('a','b')) AND Acc_Type = 1)) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0   ) as alb ");
            sqlQuery.AppendLine(" INNER JOIN ");
            sqlQuery.AppendLine(" (SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" ON als.code = alb.code  ");
            sqlQuery.AppendLine(" INNER JOIN  ");
            sqlQuery.AppendLine(" (SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month =  '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "'and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" ala.code = alb.code  ");
            sqlQuery.AppendLine(" INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" GROUP BY bs.ScenarioTypeId  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		'PrivateLabel' as name ");
            sqlQuery.AppendLine(" 		,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount) as mktValue ");
            sqlQuery.AppendLine(" 		FROM ");
            sqlQuery.AppendLine(" 	(SELECT code FROM Basis_ALB WHERE ((SUBSTRING ( WizCode ,5 , 1 ) in ('c','g','d','h','e','i', 'j', 'r', 'f') and SUBSTRING ( WizCode ,10 , 1 ) not in ('a','b') and Acc_Type = 1 )) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0   ) as alb ");
            sqlQuery.AppendLine(" 	INNER JOIN ");
            sqlQuery.AppendLine(" 	(SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" 	ON als.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 	(SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month =  '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "' and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" 	ala.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" 	ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" 	GROUP BY bs.ScenarioTypeId  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		'Treas' as name ");
            sqlQuery.AppendLine(" 		,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount) as mktValue ");
            sqlQuery.AppendLine(" 		FROM ");
            sqlQuery.AppendLine(" 	(SELECT code FROM Basis_ALB WHERE ((classOr IN (1,8) AND (ILPFlag = 0 OR Acc_Type <> Acc_TypeOR)) OR (SUBSTRING ( WizCode ,5 , 1 ) in ('a','b') and Acc_Type = 1 )) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0) as alb ");
            sqlQuery.AppendLine(" 	INNER JOIN ");
            sqlQuery.AppendLine(" 	(SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" 	ON als.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 	(SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month = '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "' and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" 	ala.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" 	ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" 	GROUP BY bs.ScenarioTypeId  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		'Corp' as name ");
            sqlQuery.AppendLine(" 		,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount) as mktValue ");
            sqlQuery.AppendLine(" 		FROM ");
            sqlQuery.AppendLine(" 	(SELECT code FROM Basis_ALB WHERE ((classOr IN (2) AND (ILPFlag = 0 OR Acc_Type <> Acc_TypeOR)) OR (SUBSTRING ( WizCode ,5 , 1 ) in ('l') AND Acc_Type = 1 )) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0) as alb ");
            sqlQuery.AppendLine(" 	INNER JOIN ");
            sqlQuery.AppendLine(" 	(SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" 	ON als.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 	(SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month =  '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "' and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" 	ala.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" 	ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" 	GROUP BY bs.ScenarioTypeId  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		'Muni' as name ");
            sqlQuery.AppendLine(" 		,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount) as mktValue ");
            sqlQuery.AppendLine(" 		FROM ");
            sqlQuery.AppendLine(" 	(SELECT code FROM Basis_ALB WHERE ((classOr IN (5) AND (ILPFlag = 0 OR Acc_Type <> Acc_TypeOR)) OR (SUBSTRING ( WizCode ,5 , 1 ) in ('k') and Acc_Type = 1 )) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0) as alb ");
            sqlQuery.AppendLine(" 	INNER JOIN ");
            sqlQuery.AppendLine(" 	(SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" 	ON als.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 	(SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month =  '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "' and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" 	ala.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" 	ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" 	GROUP BY bs.ScenarioTypeId  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		'Stock' as name ");
            sqlQuery.AppendLine(" 		,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount) as mktValue ");
            sqlQuery.AppendLine(" 		FROM ");
            sqlQuery.AppendLine(" 	(SELECT code FROM Basis_ALB WHERE ((classOr IN (7) AND (ILPFlag = 0 OR Acc_Type <> Acc_TypeOR)) OR (SUBSTRING ( WizCode ,5 , 1 ) in ('p') AND Acc_Type = 1 )) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0) as alb ");
            sqlQuery.AppendLine(" 	INNER JOIN ");
            sqlQuery.AppendLine(" 	(SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" 	ON als.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 	(SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month =  '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "' and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" 	ala.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" 	ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" 	GROUP BY bs.ScenarioTypeId  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" 	SELECT ");
            sqlQuery.AppendLine(" 		'Other' as name ");
            sqlQuery.AppendLine(" 		,bs.ScenarioTypeId ");
            sqlQuery.AppendLine(" 		,SUM(als.MVAmount - ala.End_Bal) as gainLoss ");
            sqlQuery.AppendLine(" 		,SUM(CASE WHEN (classOr = 11 AND (ILPFlag = 0 OR Acc_Type <> Acc_TypeOR)) OR (SUBSTRING ( WizCode ,5 , 1 ) = 'n' AND Acc_Type = 1) THEN 0 ELSE als.MVAmount END) as mktValue ");
            sqlQuery.AppendLine(" 		FROM ");
            sqlQuery.AppendLine(" 	(SELECT code, classOr, ilpFlag, wizCode, Acc_Type, Acc_TypeOR FROM Basis_ALB WHERE ((classOr IN (11,9,0) AND (ILPFlag = 0  OR Acc_Type <> Acc_TypeOR)) OR (((SUBSTRING ( WizCode ,5 , 1 ) in ('n','o', 'q')) OR (SUBSTRING ( WizCode ,5 , 1 ) in ('s') and SUBSTRING ( WizCode ,10 , 1 ) not in ('a','b'))) AND Acc_Type = 1 )) AND Acc_TypeOR = 1 AND SimulationId = " + simId + " and cat_type in (0,1,5) and exclude  = 0) as alb ");
            sqlQuery.AppendLine(" 	INNER JOIN ");
            sqlQuery.AppendLine(" 	(SELECT code, scenario, MVAmount, SimulationId, exclude  FROM BASIS_ALS WHERE simulationId = " + simId + " and exclude = 0) as als  ");
            sqlQuery.AppendLine(" 	ON als.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 	(SELECT end_Bal, simulationId, code FROM Basis_ALA WHERE month =  '" + Utilities.GetFirstOfMonthStr(date.ToShortDateString()) + "' and simulationid = " + simId + ") as ala ON ");
            sqlQuery.AppendLine(" 	ala.code = alb.code  ");
            sqlQuery.AppendLine(" 	INNER JOIN Basis_Scenario AS bs   ");
            sqlQuery.AppendLine(" 	ON bs.number = als.scenario AND bs.SimulationId = als.SimulationId   ");
            sqlQuery.AppendLine(" 	GROUP BY bs.ScenarioTypeId ");
            DataTable vals = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), sqlQuery.ToString());

            return vals;
        }

        [HttpGet]
        public object CurrentRiskAss(string instName, int offSet)
        {

            //Load Actual Policy Id based off of offset
            InstitutionService instService = new InstitutionService(instName);

            Information info = instService.GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), offSet);
            DataTable polTable = Utilities.ExecuteSql(instService.ConnectionString(), "SELECT id FROM Atlas_Policy WHERE asOfDate = '" + info.AsOfDate.ToShortDateString() + "' ");

            if (polTable.Rows.Count == 0)
            {
                return null;
            }


            string polId = polTable.Rows[0][0].ToString();
            // Array

            //Loads UP All NII Policies
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("SELECT ");
            sqlQuery.AppendLine("   NIP.[RiskAssessment] as ra ");
            sqlQuery.AppendLine("  ,[scenarioTypeId] as scenId ");
            sqlQuery.AppendLine("  ,NIPP.name ");
            sqlQuery.AppendLine(" ");
            sqlQuery.AppendLine(" FROM [ATLAS_NIIPolicy]  AS NIP ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_NIIPolicyParent AS NIPP ");
            sqlQuery.AppendLine(" ON NIPP.Id = NIP.NIIPolicyParentId ");
            sqlQuery.AppendLine(" WHERE NIPP.PolicyId  = " + polId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine("  SELECT ");
            sqlQuery.AppendLine("      NIP.[RiskAssessment] as ra ");
            sqlQuery.AppendLine("	  ,[scenarioTypeId] as scenId ");
            sqlQuery.AppendLine("	  ,name ");
            sqlQuery.AppendLine(" ");
            sqlQuery.AppendLine("  FROM[dbo].[ATLAS_CoreFundPolicy] ");
            sqlQuery.AppendLine("        AS NIP ");
            sqlQuery.AppendLine("  INNER JOIN ATLAS_CoreFundParent AS NIPP ");
            sqlQuery.AppendLine("  ON NIPP.Id = NIP.CoreFundParentId ");
            sqlQuery.AppendLine(" WHERE NIPP.PolicyId  = " + polId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine("           SELECT ");
            sqlQuery.AppendLine("     NIP.[RiskAssessment] as ra ");
            sqlQuery.AppendLine("	  ,[scenarioTypeId] as scenId ");
            sqlQuery.AppendLine("	  ,name ");
            sqlQuery.AppendLine(" ");
            sqlQuery.AppendLine(" FROM[dbo].[ATLAS_EvePolicy] ");
            sqlQuery.AppendLine("       AS NIP ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_EvePolicyParent AS NIPP ");
            sqlQuery.AppendLine(" ON NIPP.Id = NIP.EvePolicyParentId ");
            sqlQuery.AppendLine(" WHERE NIPP.PolicyId  = " + polId + " ");


            DataTable IRRTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());


            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT ");
            sqlQuery.AppendLine("     name ");
            sqlQuery.AppendLine("     ,RiskAssessment as ra ");
            sqlQuery.AppendLine("   FROM[ATLAS_LiquidityPolicy] ");
            sqlQuery.AppendLine("   WHERE PolicyId = " + polId + " ");
            sqlQuery.AppendLine("   UNION ALL ");
            sqlQuery.AppendLine("   SELECT ");
            sqlQuery.AppendLine("     name ");
            sqlQuery.AppendLine("     ,RiskAssessment as ra ");
            sqlQuery.AppendLine(" FROM ATLAS_OtherPolicy ");
            sqlQuery.AppendLine(" WHERE PolicyId = " + polId + " ");

            DataTable liqTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());

            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine("     name ");
            sqlQuery.AppendLine("     ,RiskAssessment as ra ");
            sqlQuery.AppendLine("   FROM[ATLAS_CapitalPolicy] ");
            sqlQuery.AppendLine("   WHERE PolicyId = " + polId + " ");
            sqlQuery.AppendLine("   UNION ALL ");
            sqlQuery.AppendLine("   SELECT ");
            sqlQuery.AppendLine("     name ");
            sqlQuery.AppendLine("     ,RiskAssessment as ra ");
            sqlQuery.AppendLine(" FROM ATLAS_OtherPolicy ");
            sqlQuery.AppendLine(" WHERE PolicyId = " + polId + " ");

            DataTable capTable = Utilities.ExecuteSql(instService.ConnectionString(), sqlQuery.ToString());

            return new
            {
                irrTable = IRRTable,
                liqTable = liqTable,
                capTable = capTable
            };
        }



        [HttpGet]
        public DataTable BasicSurplusAdminsTable(string instName)
        {
            if (instName == "")
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }

            string conStr = ConfigurationManager.ConnectionStrings["DDwConnection"].ToString();
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            DataTable aodTable = Utilities.ExecuteSql(conStr, "SELECT Id FROM [" + instName + "].dbo.asOfDateInfo WHERE asOfDate = '" + date.ToShortDateString() + "'  AND BasisDate = 1");

            int asOfDateId = 0;
            if (aodTable.Rows.Count > 0)
            {
                asOfDateId = (int)aodTable.Rows[0][0];
            }

            return Utilities.ExecuteSql(conStr, "SELECT id, name FROM  [" + instName + "].dbo.ATLAS_BasicSurplusAdmin WHERE informationId = " + asOfDateId + " AND BasisDate = 1");
        }



        //Basic Surplus Admin by ID w/ All Types
        [HttpGet]
        public IQueryable<BasicSurplusAdmin> BasicSurplusAdminById(int id, string inst)
        {

            var instContext = new GlobalContextProvider(inst);


            return instContext.Context.BasicSurplusAdmins
                .Include(s => s.BasicSurplusMarketValues)
                .Include(s => s.BasicSurplusSecuredLiabilitiesTypes.Select(t => t.SecuredLiabilitiesType))
                .Include(s => s.BasicSurplusSecuredLiabilitiesTypes.Select(c => c.BasicSurplusCollateralTypes.Select(t => t.CollateralType)))
                .Include(s => s.CollateralTypes)
                .Include(s => s.LeftS1OvntFunds)
                .Include(s => s.LeftS1SecCols)
                .Include(s => s.LeftS1LiqAssets)
                .Include(s => s.LeftS2VolLiabs)
                .Include(s => s.LeftS3LoanCols)
                .Include(s => s.LeftS3Loans)
                .Include(s => s.LeftS4BrokDeps)
                .Include(s => s.MidS1Miscs)
                .Include(s => s.RightS1OtherLiqs)
                .Include(s => s.RightS3UnsecBors)
                .Include(s => s.RightS2SecBors)
                //.Include(s => s.RightS4OtherSecs)
                .Include(s => s.RightS4UnrealGL)
                .Include(s => s.RightS5BorCaps)
                .Include(s => s.RightS6Footnotes)
                .Where(s => s.Id == id);
        }

        //Basic Surplus Secured Liabilities Types by Basic Surplus Admin ID
        [HttpGet]
        public IQueryable<BasicSurplusSecuredLiabilitiesType> BasicSurplusSecuredLiabilitiesTypesByBasicSurplusAdminId(int id)
        {
            return _contextProvider.Context.BasicSurplusSecuredLiabilitiesTypes.Where(s => s.BasicSurplusAdminId == id);
        }

        //Basic Surplus Secured Liabilities Type by ID
        [HttpGet]
        public IQueryable<BasicSurplusSecuredLiabilitiesType> BasicSurplusSecuredLiabilitiesTypeById(int id)
        {
            return _contextProvider.Context.BasicSurplusSecuredLiabilitiesTypes.Where(s => s.Id == id);
        }

        //Basic Surplus Collateral Types by Basic Surplus Admin ID
        [HttpGet]
        public IQueryable<CollateralType> CollateralTypesByBasicSurplusAdminId(int id)
        {
            return _contextProvider.Context.CollateralTypes.Where(s => s.BasicSurplusAdminId == id);
        }

        [HttpGet]
        public string[] ShockScenarioTypeNames(bool includeBase)
        {
            //this is a really picky set of scen types to return, with no discernable keys besides Id. So that's what we'll use
            string ids = "17,18,19,20, 21, 22, 23, 49";
            //List<string> ids = new List<string>(new string[] { "19", "20", "21", "22", "23" });
            if (includeBase)
            {
                ids += ", 1";
            }
            string qry = "SELECT Name From Atlas_ScenarioType WHERE Id in (" + ids + ") and isEve = 0 ORDER BY [Priority]";
            DataTable result = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), qry);
            string[] ret = new string[result.Rows.Count];
            int r = 0;
            for (r = 0; r < result.Rows.Count; r++)
            {
                ret[r] = result.Rows[r][0].ToString();
            }
            return ret;
        }

        //Basic Surplus Admin Data Pull Stuff
        [HttpGet]
        public DataTable BasicSurplusAdminData(string instName, int asOfDateOffset, int simulationTypeId, int scenarioTypeId)
        {
            string conStr = Utilities.BuildInstConnectionString("");

            //Setup
            var scen = new InstitutionService(instName).GetSimulationScenario(
                DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()),
                asOfDateOffset, simulationTypeId, scenarioTypeId);
            string asOfDate = scen.GetDynamicProperty("asOfDate").ToString(),
                simulation = scen.GetDynamicProperty("simulation").ToString(),
                scenario = scen.GetDynamicProperty("scenario").ToString();

            //TODO don't know if TryParse handles a null ref exception, so we'll wrap it in a try-catch just to be safe
            int policyId;
            try
            {
                policyId = (int.TryParse(Utilities.ExecuteSql(conStr, "SELECT Id FROM ATLAS_Policy WHERE AsOfDate = '" + asOfDate + "'").Rows[0][0].ToString(), out policyId)) ? policyId : -1;
            }
            catch (Exception)
            {
                policyId = -1;
            }

            //Setup
            var sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("DECLARE @MaxAMonth datetime;");
            sqlQuery.AppendLine("SELECT @MaxAMonth = LActual FROM " + instName + ".dbo.Basis_Bank WHERE SimulationId = " + simulation + ";");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("DECLARE @MinPMonth datetime;");
            sqlQuery.AppendLine("SELECT @MinPMonth = FProjection FROM " + instName + ".dbo.Basis_Bank WHERE SimulationId = " + simulation + ";");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'collateralFHLBOutstanding' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'FHLB'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR = 1");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'collateralFedDiscountOtherSecuredOutstanding' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'FRB'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR = 8");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'collateralRetailReposSweepsOutstanding' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Secured Retail'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR = 3");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'collateralWholesaleReposOutstanding' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Secured Repo Wholesale'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR = 7");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS1OvntFundsSrtTrmInv' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Overnight'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 1 AND ClassOR = 6");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS1LiqAssetsGovLoans' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Gov & Agy Guaranteed'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 2 AND ClassOR = 10");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS1LiqAssetsCashflow' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		(ISNULL(P3.Balance, 0) + ISNULL(P4.Balance, 0) + ISNULL(P5.Balance, 0)) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Muni', 'Corporate', 'Stock', 'Trust Preferred', 'Other Investment'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 1 AND ClassOR IN (2, 5)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(MatEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP3");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P3");
            sqlQuery.AppendLine("	ON B.Code = P3.Code");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(AmtEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP4");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P4");
            sqlQuery.AppendLine("	ON B.Code = P4.Code");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(PayEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP5");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P5");
            sqlQuery.AppendLine("	ON B.Code = P5.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS1LiqAssetsOther' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		(ISNULL(A.Balance, 0)) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Other Liquid Asset'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 1 AND ClassOR = 11");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2MatUnsecLiabAmount' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(P3.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Unsecured Retail', 'Unsecured Wholesale', 'Unsecured Repo Wholesale'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR IN (2, 4, 6)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(MatEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP3");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P3");
            sqlQuery.AppendLine("	ON B.Code = P3.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2TotalDepAmountBrokered' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			AND (");
            sqlQuery.AppendLine("				-- 'Brokered NOW', 'Brokered MMDA'");
            sqlQuery.AppendLine("				(Acc_TypeOR = 3 AND ClassOR IN (13, 14))");
            sqlQuery.AppendLine("				-- 'Brokered CD'");
            sqlQuery.AppendLine("				OR (Acc_TypeOR = 8 AND ClassOR = 5)");
            sqlQuery.AppendLine("			)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2TotalDepAmountBrokeredOneWay' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			AND (");
            sqlQuery.AppendLine("				-- 'Brokered One-Way MMDA'");
            sqlQuery.AppendLine("				(Acc_TypeOR = 3 AND ClassOR = 16)");
            sqlQuery.AppendLine("				-- 'Brokered One-Way CD'");
            sqlQuery.AppendLine("				OR (Acc_TypeOR = 8 AND ClassOR = 10)");
            sqlQuery.AppendLine("			)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2TotalDepAmountBrokeredRecip' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			AND (");
            sqlQuery.AppendLine("				-- 'Brokered Reciprocal MMDA'");
            sqlQuery.AppendLine("				(Acc_TypeOR = 3 AND ClassOR = 15)");
            sqlQuery.AppendLine("				-- 'Brokered Reciprocal CD'");
            sqlQuery.AppendLine("				OR (Acc_TypeOR = 8 AND ClassOR = 9)");
            sqlQuery.AppendLine("			)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2TotalDepAmountPublicDeposits' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			AND (");
            sqlQuery.AppendLine("				-- 'Public DDA', 'Public Now', 'Public Savings', 'Public MMDA'");
            sqlQuery.AppendLine("				(Acc_TypeOR = 3 AND ClassOR IN (2, 4, 6, 8))");
            sqlQuery.AppendLine("				-- 'Public CD', 'Public Jumbo CD'");
            sqlQuery.AppendLine("				OR (Acc_TypeOR = 8 AND ClassOR IN (2, 4))");
            sqlQuery.AppendLine("			)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2TotalDepAmountNationalCDs' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'National CD'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 8 AND ClassOR = 6");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2RegCDMatAmount' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(P3.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'CD', 'Online CD'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 8 AND ClassOR IN (1, 7)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(MatEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP3");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P3");
            sqlQuery.AppendLine("	ON B.Code = P3.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2RegCDMatAmountPublic' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(P3.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Public CD'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 8 AND ClassOR = 2");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(MatEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP3");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P3");
            sqlQuery.AppendLine("	ON B.Code = P3.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2RegJumboCDMatAmount' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(P3.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Jumbo CD', 'Online Jumbo CD'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 8 AND ClassOR IN (3, 8)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(MatEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP3");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P3");
            sqlQuery.AppendLine("	ON B.Code = P3.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2RegJumboCDMatAmountPublic' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(P3.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Public Jumbo CD'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 8 AND ClassOR = 4");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(MatEBal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALP3");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("			AND Month = @MinPMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS P3");
            sqlQuery.AppendLine("	ON B.Code = P3.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2OtherDepAmount' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			AND (");
            sqlQuery.AppendLine("				-- 'DDA', 'Online DDA', 'NOW', 'Online NOW', 'Savings', 'Online Savings', 'MMDA', 'Online MMDA'");
            sqlQuery.AppendLine("				(Acc_TypeOR = 3 AND ClassOR IN (1, 3, 5, 7, 9, 10, 11, 12))");
            sqlQuery.AppendLine("				-- 'Other Time Deposit'");
            //  sqlQuery.AppendLine("				OR (Acc_TypeOR = 8 AND ClassOR = 0)"); PER ATO-590
            sqlQuery.AppendLine("			)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'leftS2OtherDepAmountPublic' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Public DDA', 'Public NOW', 'Public MMDA', 'Public Savings'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 3 AND ClassOR IN (2, 4, 6, 8)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'midS1MiscsTotalDeposit' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Non-Maturity Deposit', 'Time Deposit'");
            sqlQuery.AppendLine("			AND Acc_TypeOR IN (3, 8)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'midS1MiscsTotalAsset' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			AND IsAsset = 1");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'rightS3UnsecBorsFedFunds' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Unsecured Wholesale', 'Unsecured Repo Wholesale'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR IN (4, 6)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'rightS5BorCapsCurOutBorAll' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Borrowing'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'rightS5BorCapsCurOutBorRetail' AS FieldName,");
            sqlQuery.AppendLine("	SUM(Balance) AS Data");
            sqlQuery.AppendLine("FROM (");
            sqlQuery.AppendLine("	SELECT");
            sqlQuery.AppendLine("		1 AS DontCare,");
            sqlQuery.AppendLine("		ISNULL(A.Balance, 0) / 1000.0 AS Balance");
            sqlQuery.AppendLine("	FROM (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALB");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			--AND Cat_Type IN (0, 1, 5)");
            sqlQuery.AppendLine("			-- 'Secured Retail', 'Unsecured Retail'");
            sqlQuery.AppendLine("			AND Acc_TypeOR = 6 AND ClassOR IN (2, 3)");
            sqlQuery.AppendLine("	) AS B");
            sqlQuery.AppendLine("	LEFT JOIN (");
            sqlQuery.AppendLine("		SELECT");
            sqlQuery.AppendLine("			Code,");
            sqlQuery.AppendLine("			SUM(End_Bal) AS Balance");
            sqlQuery.AppendLine("		FROM " + instName + ".dbo.Basis_ALA");
            sqlQuery.AppendLine("		WHERE SimulationId = " + simulation);
            sqlQuery.AppendLine("			AND Month = @MaxAMonth");
            sqlQuery.AppendLine("		GROUP BY SimulationId, Code");
            sqlQuery.AppendLine("	) AS A");
            sqlQuery.AppendLine("	ON B.Code = A.Code");
            sqlQuery.AppendLine(") AS I");
            sqlQuery.AppendLine("GROUP BY I.DontCare");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'brokMaxPolicyLimit' AS FieldName,");
            sqlQuery.AppendLine("	PolicyLimit AS Data");
            sqlQuery.AppendLine("FROM " + instName + ".dbo.ATLAS_LiquidityPolicy");
            sqlQuery.AppendLine("WHERE PolicyId = " + policyId);
            sqlQuery.AppendLine("	AND ShortName = 'brokeredMax'");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'brokAssetPolicyLimit' AS FieldName,");
            sqlQuery.AppendLine("	PolicyLimit AS Data");
            sqlQuery.AppendLine("FROM " + instName + ".dbo.ATLAS_LiquidityPolicy");
            sqlQuery.AppendLine("WHERE PolicyId = " + policyId);
            sqlQuery.AppendLine("	AND ShortName = 'brokeredAsset'");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'brokDepositPolicyLimit' AS FieldName,");
            sqlQuery.AppendLine("	PolicyLimit AS Data");
            sqlQuery.AppendLine("FROM " + instName + ".dbo.ATLAS_LiquidityPolicy");
            sqlQuery.AppendLine("WHERE PolicyId = " + policyId);
            sqlQuery.AppendLine("	AND ShortName = 'brokeredDeposits'");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'borMaxPolicyLimit' AS FieldName,");
            sqlQuery.AppendLine("	PolicyLimit AS Data");
            sqlQuery.AppendLine("FROM " + instName + ".dbo.ATLAS_LiquidityPolicy");
            sqlQuery.AppendLine("WHERE PolicyId = " + policyId);
            sqlQuery.AppendLine("	AND ShortName = 'borrowMax'");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'borAssetPolicyLimit' AS FieldName,");
            sqlQuery.AppendLine("	PolicyLimit AS Data");
            sqlQuery.AppendLine("FROM " + instName + ".dbo.ATLAS_LiquidityPolicy");
            sqlQuery.AppendLine("WHERE PolicyId = " + policyId);
            sqlQuery.AppendLine("	AND ShortName = 'borrowAsset'");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'borDepositPolicyLimit' AS FieldName,");
            sqlQuery.AppendLine("	PolicyLimit AS Data");
            sqlQuery.AppendLine("FROM " + instName + ".dbo.ATLAS_LiquidityPolicy");
            sqlQuery.AppendLine("WHERE PolicyId = " + policyId);
            sqlQuery.AppendLine("	AND ShortName = 'borrowDeposits'");

            //Go!
            return Utilities.ExecuteSql(conStr, sqlQuery.ToString());
        }

        //Secured Liabilities Type
        [HttpGet]
        public IQueryable<SecuredLiabilitiesType> SecuredLiabilitiesTypes(string inst)
        {

            var instContext = new GlobalContextProvider(inst);

            return instContext.Context.SecuredLiabilitiesTypes;
        }

        //Push Log
        [HttpGet]
        public DataTable GetPushLog(string asOfDate = "")
        {
            string instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string conStr = Utilities.BuildInstConnectionString(instName);

            //If we haven't passed in a date, use the profile AsOfDate - either way, always get the ToShortDateString from it
            if (asOfDate == "")
                asOfDate = Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString();
            asOfDate = DateTime.Parse(asOfDate).ToShortDateString();

            var sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	S.Name AS SimName,");
            sqlQuery.AppendLine("	CONVERT(char, CONVERT(datetime, BA.LastAtlasPush), 20) AS LastSimPush,");
            sqlQuery.AppendLine("	BA.AtlasPushUserId AS SimPushId,");
            sqlQuery.AppendLine("	SC.Name AS ScenName,");
            sqlQuery.AppendLine("	CONVERT(char, CONVERT(datetime, SC.LastAtlasPush), 20) AS LastScenPush,");
            sqlQuery.AppendLine("	SC.AtlasPushUserId AS ScenPushId");
            sqlQuery.AppendLine("FROM asOfDateInfo AS A");
            sqlQuery.AppendLine("INNER JOIN Simulations AS S ON S.InformationId = A.Id");
            sqlQuery.AppendLine("INNER JOIN Basis_Scenario AS SC ON SC.SimulationId = S.Id");
            sqlQuery.AppendLine("INNER JOIN Basis_Bank AS BA ON SC.SimulationId = BA.SimulationId");
            sqlQuery.AppendLine("WHERE A.asOfDate = '" + asOfDate + "'");
            return Utilities.ExecuteSql(conStr, sqlQuery.ToString());
        }

        //L360 Push Stuff
        [HttpGet]
        public DataTable L360Push(int simulationTypeId, int scenarioTypeId, int basicSurplusId)
        {
            string instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string conStr = Utilities.BuildInstConnectionString(instName);
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());

            //Setup
            var scen = new InstitutionService(instName).GetSimulationScenario(date, 0, simulationTypeId, scenarioTypeId);
            string asOfDate = scen.GetDynamicProperty("asOfDate").ToString(),
                simulation = scen.GetDynamicProperty("simulation").ToString(),
                scenario = scen.GetDynamicProperty("scenario").ToString();
            scen = new InstitutionService(instName).GetSimulationScenario(date, 1, simulationTypeId, scenarioTypeId);
            string priorAsOfDate = scen.GetDynamicProperty("asOfDate").ToString(),
                //TODO HACK Legacy seems to always use current simulation for prior data, so just force current simulation / scenario
                priorSimulation = simulation, //scen.GetDynamicProperty("simulation").ToString(),
                priorScenario = scenario; //scen.GetDynamicProperty("scenario").ToString();
            DateTime priorDate = DateTime.Parse(priorAsOfDate);

            DateTime fProjection = DateTime.Parse(Utilities.ExecuteSql(conStr, "SELECT FProjection FROM " + instName + ".dbo.Basis_Bank WHERE SimulationId = " + simulation).Rows[0][0].ToString());

            BasicSurplusAdmin bsAdmin = BasicSurplusAdminById(basicSurplusId, instName).FirstOrDefault();//BasicSurplusAdmins().Where(x => x.Id == basicSurplusId).FirstOrDefault();
            Policy policy = Policy().FirstOrDefault();

            //Setup
            var sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'ALA' AS FieldName,");
            sqlQuery.AppendLine("	SUM(End_Bal) AS Data,");
            sqlQuery.AppendLine("	Acc_TypeOR,");
            sqlQuery.AppendLine("	ClassOR,");
            sqlQuery.AppendLine("	NULL AS Month,");
            sqlQuery.AppendLine("	NULL AS NMonths");
            sqlQuery.AppendLine("FROM Basis_ALA AS A");
            sqlQuery.AppendLine("INNER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("ON A.SimulationId = B.SimulationId AND A.Code = B.Code");
            sqlQuery.AppendLine("WHERE A.SimulationId = " + simulation + " AND A.Month = '" + new DateTime(date.Year, date.Month, 1).ToShortDateString() + "'");
            sqlQuery.AppendLine("GROUP BY Acc_TypeOR, ClassOR");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'ALA-Prior' AS FieldName,");
            sqlQuery.AppendLine("	SUM(End_Bal) AS Data,");
            sqlQuery.AppendLine("	Acc_TypeOR,");
            sqlQuery.AppendLine("	ClassOR,");
            sqlQuery.AppendLine("	NULL AS Month,");
            sqlQuery.AppendLine("	NULL AS NMonths");
            sqlQuery.AppendLine("FROM Basis_ALA AS A");
            sqlQuery.AppendLine("INNER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("ON A.SimulationId = B.SimulationId AND A.Code = B.Code");
            sqlQuery.AppendLine("WHERE A.SimulationId = " + priorSimulation + " AND A.Month = '" + new DateTime(priorDate.Year, priorDate.Month, 1).ToShortDateString() + "'");
            sqlQuery.AppendLine("GROUP BY Acc_TypeOR, ClassOR");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'ALP1' AS FieldName,");
            sqlQuery.AppendLine("	SUM(EndBal) AS Data,");
            sqlQuery.AppendLine("	Acc_TypeOR,");
            sqlQuery.AppendLine("	ClassOR,");
            sqlQuery.AppendLine("	CAST(Month AS date) AS Month,");
            sqlQuery.AppendLine("	NMonths");
            sqlQuery.AppendLine("FROM Basis_ALP1 AS A");
            sqlQuery.AppendLine("INNER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("ON A.SimulationId = B.SimulationId AND A.Code = B.Code");
            sqlQuery.AppendLine("WHERE A.SimulationId = " + simulation + " AND A.Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("GROUP BY Acc_TypeOR, ClassOR, Month, NMonths");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("--UNION ALL");
            sqlQuery.AppendLine("--SELECT");
            sqlQuery.AppendLine("--	'ALP345' AS FieldName,");
            sqlQuery.AppendLine("--	SUM(ISNULL(MatEBal, 0) + ISNULL(AmtEBal, 0) + ISNULL(PayEBal, 0)) AS Data,");
            sqlQuery.AppendLine("--	Acc_TypeOR,");
            sqlQuery.AppendLine("--	ClassOR,");
            sqlQuery.AppendLine("--	ISNULL(ISNULL(P3.Month, P4.Month), P5.Month) AS Month,");
            sqlQuery.AppendLine("--	ISNULL(ISNULL(P3.NMonths, P4.NMonths), P5.NMonths) AS NMonths");
            sqlQuery.AppendLine("--FROM Basis_ALP3 AS P3");
            sqlQuery.AppendLine("--FULL OUTER JOIN Basis_ALP4 AS P4");
            sqlQuery.AppendLine("--ON P3.SimulationId = P4.SimulationId AND P3.Scenario = P4.Scenario");
            sqlQuery.AppendLine("--	AND P3.Code = P4.Code AND P3.Month = P4.Month");
            sqlQuery.AppendLine("--FULL OUTER JOIN Basis_ALP5 AS P5");
            sqlQuery.AppendLine("--ON P3.SimulationId = P5.SimulationId AND P3.Scenario = P5.Scenario");
            sqlQuery.AppendLine("--	AND P3.Code = P5.Code AND P3.Month = P5.Month");
            sqlQuery.AppendLine("--FULL OUTER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("--ON P3.SimulationId = B.SimulationId AND P3.Code = B.Code");
            sqlQuery.AppendLine("--WHERE P3.SimulationId = " + simulation + " AND P3.Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("--GROUP BY Acc_TypeOR, ClassOR, ISNULL(ISNULL(P3.Month, P4.Month), P5.Month), ISNULL(ISNULL(P3.NMonths, P4.NMonths), P5.NMonths)");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'ALP3' AS FieldName,");
            sqlQuery.AppendLine("	SUM(MatEBal) AS Data,");
            sqlQuery.AppendLine("	Acc_TypeOR,");
            sqlQuery.AppendLine("	ClassOR,");
            sqlQuery.AppendLine("	CAST(Month AS date) AS Month,");
            sqlQuery.AppendLine("	NMonths");
            sqlQuery.AppendLine("FROM Basis_ALP3 AS A");
            sqlQuery.AppendLine("INNER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("ON A.SimulationId = B.SimulationId AND A.Code = B.Code");
            sqlQuery.AppendLine("WHERE A.SimulationId = " + simulation + " AND A.Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("GROUP BY Acc_TypeOR, ClassOR, Month, NMonths");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'ALP4' AS FieldName,");
            sqlQuery.AppendLine("	SUM(AmtEBal) AS Data,");
            sqlQuery.AppendLine("	Acc_TypeOR,");
            sqlQuery.AppendLine("	ClassOR,");
            sqlQuery.AppendLine("	CAST(Month AS date) AS Month,");
            sqlQuery.AppendLine("	NMonths");
            sqlQuery.AppendLine("FROM Basis_ALP4 AS A");
            sqlQuery.AppendLine("INNER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("ON A.SimulationId = B.SimulationId AND A.Code = B.Code");
            sqlQuery.AppendLine("WHERE A.SimulationId = " + simulation + " AND A.Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("GROUP BY Acc_TypeOR, ClassOR, Month, NMonths");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("UNION ALL");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	'ALP5' AS FieldName,");
            sqlQuery.AppendLine("	SUM(PayEBal) AS Data,");
            sqlQuery.AppendLine("	Acc_TypeOR,");
            sqlQuery.AppendLine("	ClassOR,");
            sqlQuery.AppendLine("	CAST(Month AS date) AS Month,");
            sqlQuery.AppendLine("	NMonths");
            sqlQuery.AppendLine("FROM Basis_ALP5 AS A");
            sqlQuery.AppendLine("INNER JOIN Basis_ALB AS B");
            sqlQuery.AppendLine("ON A.SimulationId = B.SimulationId AND A.Code = B.Code");
            sqlQuery.AppendLine("WHERE A.SimulationId = " + simulation + " AND A.Scenario = '" + scenario + "'");
            sqlQuery.AppendLine("GROUP BY Acc_TypeOR, ClassOR, Month, NMonths");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("ORDER BY FieldName, Acc_TypeOR, ClassOR, Month, NMonths");

            //Go!
            DataTable dt = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            //Point ourselves at L360 database
            conStr = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString;
            int bankKey;
            try
            {
                bankKey = (int)Utilities.ExecuteSql(conStr, "SELECT TOP 1 [BankKey] FROM [DIP_Data].[dbo].[DDWBanks] WHERE [DDWDBName] = '" + instName + "'").Rows[0][0];
            }
            catch (IndexOutOfRangeException ex)
            {
                throw new IndexOutOfRangeException("This bank does not exist in L360.", ex.InnerException); //Figured a more helpful message might explain what's up
            }
            //else just explode

            //Setup L360PushHelper
            var groupFields = new Dictionary<string, string>();
            groupFields.Add("ALP3", "ALP345");
            groupFields.Add("ALP4", "ALP345");
            groupFields.Add("ALP5", "ALP345");

            var ar = new L360PushHelper(dt, groupFields: groupFields, rollFields: new L360PushHelper.FieldRollup[] { new L360PushHelper.FieldRollup("ALP345", fProjection, 24, 24), new L360PushHelper.FieldRollup("ALP1", fProjection, 60, 0) });
            ar.SQLQueueConStr = conStr;
            ar.SQLQueueAutoExecuteThreshold = 30;

            //Setup actual data push
            string fieldName;
            double data, a, b, c, d, e, f;
            double[][] dataArrays = new double[2][];

            //TODO for now load up a new dt to return pushed data so we can view it in web debugger; this may also be useful later if we decide to display the pushed data. Otherwise delete this.
            dt = new DataTable();
            dt.Columns.Add("FieldName", typeof(string));
            dt.Columns.Add("Data", typeof(double));
            dt.Columns.Add("DataArrays", typeof(double[][]));

            //LRmReport Push
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM [DIP_Data].[dbo].[LRmReport] WHERE asOfDate = '1/1/1910' AND BankKey = {0}");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("INSERT INTO [DIP_Data].[dbo].[LRmReport]");
            sqlQuery.AppendLine("	([asOfDate],");
            sqlQuery.AppendLine("	[BankKey],");
            sqlQuery.AppendLine("	[SrcGroup],");
            sqlQuery.AppendLine("	[SrcRow],");
            sqlQuery.AppendLine("	[stressLevel1],");
            sqlQuery.AppendLine("	[stressLevel2],");
            sqlQuery.AppendLine("	[stressLevel3],");
            sqlQuery.AppendLine("	[currentTrend],");
            sqlQuery.AppendLine("	[outOfCompliance],");
            sqlQuery.AppendLine("	[userAction],");
            sqlQuery.AppendLine("	[responseLevel],");
            sqlQuery.AppendLine("	[riskLevel])");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	[asOfDate],");
            sqlQuery.AppendLine("	{0},");
            sqlQuery.AppendLine("	[SrcGroup],");
            sqlQuery.AppendLine("	[SrcRow],");
            sqlQuery.AppendLine("	[stressLevel1],");
            sqlQuery.AppendLine("	[stressLevel2],");
            sqlQuery.AppendLine("	[stressLevel3],");
            sqlQuery.AppendLine("	[currentTrend],");
            sqlQuery.AppendLine("	[outOfCompliance],");
            sqlQuery.AppendLine("	[userAction],");
            sqlQuery.AppendLine("	[responseLevel],");
            sqlQuery.AppendLine("	[riskLevel]");
            sqlQuery.AppendLine("FROM [DIP_Data].[dbo].[LRmReport]");
            sqlQuery.AppendLine("WHERE asOfDate = '1/1/1910' AND BankKey = -99999");
            Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey));

            //(Cur.Loans - Pr.Loans) - ((Cur.NonMat + Cur.TimeDep) - (Pr.NonMat + Pr.TimeDep)) AS ChangeInLoanGrowth,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 2 THEN ALA.End_Bal ELSE 0 END) AS Loans,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 3 AND ALB.ClassOR NOT IN (13, 15, 16, 14) THEN ALA.End_Bal ELSE 0 END) AS NonMat,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 8 AND ALB.ClassOR NOT IN (5, 9, 10) THEN ALA.End_Bal ELSE 0 END) AS TimeDep,
            fieldName = "ChangeInLoanGrowth";
            a = ar.GetFieldsExcl("ALA", 2, new int[] { 11, 12 });
            b = ar.GetFieldsExcl("ALA", 3, new int[] { 13, 15, 16, 14 });
            c = ar.GetFieldsExcl("ALA", 8, new int[] { 5, 9, 10 });
            d = ar.GetFieldsExcl("ALA-Prior", 2, new int[] { 11, 12 });
            e = ar.GetFieldsExcl("ALA-Prior", 3, new int[] { 13, 15, 16, 14 });
            f = ar.GetFieldsExcl("ALA-Prior", 8, new int[] { 5, 9, 10 });
            data = (a - d) - ((b + c) - (e + f));
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_HFN", "3MDCHLG");

            //Cur.NonMatTotal - Pr.NonMatTotal AS ChangeNonMat,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 3 THEN ALA.End_Bal ELSE 0 END) AS NonMatTotal,
            fieldName = "ChangeNonMat";
            a = ar.GetFieldsExcl("ALA", 3, new int[] { 13, 14, 15, 16 });
            b = ar.GetFieldsExcl("ALA-Prior", 3, new int[] { 13, 14, 15, 16 });
            data = ((b == 0) ? 0 : (a - b) / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_HFN", "CNMDB");

            //(CASE WHEN Cur.NonMatTotal + Cur.TimeDepTotal = 0 THEN 0 ELSE Cur.Loans / (Cur.NonMatTotal + Cur.TimeDepTotal) END
            //    - CASE WHEN Pr.NonMatTotal + Pr.TimeDepTotal = 0 THEN 0 ELSE Pr.Loans / (Pr.NonMatTotal + Pr.TimeDepTotal) END) * 100 AS ChangeInLoanDeposits,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 8 THEN ALA.End_Bal ELSE 0 END) AS TimeDepTotal,
            fieldName = "ChangeInLoanDeposits";
            a = ar.GetFields("ALA", 2);
            b = ar.GetFields("ALA", 3) + ar.GetFields("ALA", 8);
            c = ar.GetFields("ALA-Prior", 2);
            d = ar.GetFields("ALA-Prior", 3) + ar.GetFields("ALA-Prior", 8);
            data = (((b == 0) ? 0 : a / b) - ((d == 0) ? 0 : c / d)) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_HFN", "LD_C3M");

            //CASE WHEN @BasicSurplusAssets = 0 THEN 0 ELSE CASE WHEN @BorrowAssetIncRetailRepo = 1 THEN Cur.BorrowTotal ELSE Cur.Borrow END / @BasicSurplusAssets END AS BorrowAssets,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 6 AND ALB.ClassOR NOT IN (6, 7) THEN ALA.End_Bal ELSE 0 END) AS Borrow,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 6 THEN ALA.End_Bal ELSE 0 END) AS BorrowTotal,
            fieldName = "BorrowAssets";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = (policy.LiquidityPolicies.Where(s => s.ShortName == "borrowAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incRetailRepo").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                ? ar.GetFields("ALA", 6)
                : ar.GetFieldsExcl("ALA", 6, new int[] { 2, 3 });
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "BORASS");

            fieldName = "BrokAssets";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = ar.GetFields("ALA", 3, new int[] { 16, 14, 13 }) + ar.GetFields("ALA", 8, new int[] { 10, 5 });
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incBrokeredRecip").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                b += ar.GetField("ALA", 3, 15) + ar.GetField("ALA", 8, 9);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incNationalDep").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                b += ar.GetField("ALA", 8, 6);
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "BROKDEPASS");

            fieldName = "BrokTotalDeposits";
            a = 0; //(double)bsAdmin.MidS1TotalDepositAmount; //TODO use this or not? Add to Total Deposits??
            b = ar.GetFields("ALA", 3, new int[] { 16, 14, 13 }) + ar.GetFields("ALA", 8, new int[] { 10, 5 });
            a += b;
            c = ar.GetField("ALA", 3, 15) + ar.GetField("ALA", 8, 9);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredDeposits").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incBrokeredRecip").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                b += c;
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "BROKTOTDEP");

            fieldName = "WholesaleAssets";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = ar.GetFieldsExcl("ALA", 6, new int[] { 3, 2 }) + ar.GetFields("ALA", 3, new int[] { 16, 14, 13 }) + ar.GetField("ALA", 8, 10);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incBrokeredRecip").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                b += ar.GetField("ALA", 3, 15) + ar.GetField("ALA", 8, 9);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incNationalDep").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                b += ar.GetField("ALA", 8, 6);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "borrowAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incRetailRepo").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                b += ar.GetFields("ALA", 6, new int[] { 3, 2 });
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "WFOVEASS");

            //(CASE WHEN Cur.NonMatTotal + Cur.TimeDepTotal = 0 THEN 0 ELSE Cur.Loans / (Cur.NonMatTotal + Cur.TimeDepTotal) END) * 100 AS LoanDepositRiskLevel,
            fieldName = "LoanDepositRiskLevel";
            a = ar.GetFields("ALA", 2);
            b = ar.GetFields("ALA", 3) + ar.GetFields("ALA", 8);
            data = ((b == 0) ? 0 : a / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "LOANSDEP");

            fieldName = "BasicSurplusCore";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = (double)bsAdmin.MidS1BasicSurplusAmount;
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "CBS");

            fieldName = "BasicSurplusFHLBBrokered";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = (double)bsAdmin.MidS1BasicSurplusBrokAmount;
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "BSWFHLBBDA");

            fieldName = "BasicSurplusFHLBLoanCollateral";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = (double)bsAdmin.MidS1BasicSurplusFHLBAmount;
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "BSWFHLBLC");

            //(CASE WHEN Cur.Equity = 0 THEN 0 ELSE Cur.CreLoans / Cur.Equity END) * 100 AS CreCapital,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 2 AND ALB.ClassOR = 1 THEN ALA.End_Bal ELSE 0 END) AS CreLoans,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 4 AND ALB.ClassOR IN (7, 1) THEN ALA.End_Bal ELSE 0 END) AS Equity,
            fieldName = "CreCapital";
            a = ar.GetFields("ALA", 2, new int[] { 1 });
            //a = ar.GetFields("ALP1", 2, new int[] { 1 }, fProjection);
            //b = ar.GetFields("ALA", 4, new int[] { 1, 4 });
            b = (double)policy.CapitalPolicies.Where(s => s.ShortName == "tier1CapitalBal").Select(s => s.CurrentRatio).FirstOrDefault();
            data = ((b == 0) ? 0 : a / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "COMRECAP");

            //(CASE WHEN Cur.Equity = 0 THEN 0 ELSE Cur.Construction / Cur.Equity END) * 100 AS ConstructionCapital,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 2 AND ALB.ClassOR IN (7, 8) THEN ALA.End_Bal ELSE 0 END) AS Construction,
            fieldName = "ConstructionCapital";
            a = ar.GetFields("ALA", 2, new int[] { 7, 8 });
            //a = ar.GetFields("ALP1", 2, new int[] { 7, 8 }, fProjection);
            //b = ar.GetFields("ALA", 4, new int[] { 1, 4 });
            b = (double)policy.CapitalPolicies.Where(s => s.ShortName == "tier1CapitalBal").Select(s => s.CurrentRatio).FirstOrDefault();
            data = ((b == 0) ? 0 : a / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "CONSTCAP");

            //(CASE WHEN Cur.Equity = 0 THEN 0 ELSE Cur.Ci / Cur.Equity END) * 100 AS CiCapital,
            //SUM(CASE WHEN ALB.Acc_TypeOR = 2 AND ALB.ClassOR = 2 THEN ALA.End_Bal ELSE 0 END) AS Ci
            fieldName = "CiCapital";
            a = ar.GetFields("ALA", 2, new int[] { 2 });
            //a = ar.GetFields("ALP1", 2, new int[] { 2 }, fProjection);
            //b = ar.GetFields("ALA", 4, new int[] { 1 });
            b = (double)policy.CapitalPolicies.Where(s => s.ShortName == "tier1CapitalBal").Select(s => s.CurrentRatio).FirstOrDefault();
            //a = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 60, "ALP1", 2, new int[] { 2 }).Sum();
            //b = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 60, "ALP1", 4, new int[] { 1, 4 }).Sum()
            //    - ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 60, "ALP1", 4, new int[] { 3 }).Sum();
            data = ((b == 0) ? 0 : a / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "CANDICAP");

            fieldName = "FreeInvCollateral";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = (double)(bsAdmin.LeftS1BlankTotalAmount
                + bsAdmin.LeftS1LiqAssets.Where(s => s.FieldName == "leftS1LiqAssetsColSecPos" || s.FieldName == "leftS1LiqAssetsOther").Select(s => s.Amount).Sum()
                + bsAdmin.LeftS1OvntFunds.Where(s => s.FieldName == "leftS1OvntFundsSrtTrmInv").Select(s => s.Balance).FirstOrDefault());
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "FICST");

            fieldName = "FreeFHLBCollateral";
            a = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            b = (double)bsAdmin.LeftS3LoanTotalAmount;
            data = ((a == 0) ? 0 : b / a) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "FFHLBLCA");

            fieldName = "PledgedSecInv";
            a = (double)(Math.Abs(bsAdmin.LeftS1SecCols.Where(s => s.FieldName != "").Select(s => s.USTAgency + s.MBSAgency + s.MBSPvt).Sum()) //TODO use user-added rows here?
                + bsAdmin.RightS1OtherLiqTotalPledged);
            b = (double)(bsAdmin.LeftS1SecColTotalSecMktValUSTAgency + bsAdmin.LeftS1SecColTotalSecMktValMBSAgency + bsAdmin.LeftS1SecColTotalSecMktValMBSPvt
                + bsAdmin.RightS1OtherLiqTotalMarketValue);
            data = ((b == 0) ? 0 : a / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "CORE_FA", "PLSECINV");

            fieldName = "NonAgencyCapital";
            a = (double)(bsAdmin.RightS1OtherLiqs.Where(s => s.FieldName == "rightS1OtherLiqsCorpSec").Select(s => s.MarketValue).FirstOrDefault()
                + bsAdmin.RightS1OtherLiqs.Where(s => s.FieldName == "rightS1OtherLiqsMuniSec").Select(s => s.MarketValue).FirstOrDefault()
                + bsAdmin.LeftS1SecColTotalSecMktValMBSPvt);
            //b = ar.GetFields("ALA", 4, new int[] { 1, 4 });
            b = (double)policy.CapitalPolicies.Where(s => s.ShortName == "tier1CapitalBal").Select(s => s.CurrentRatio).FirstOrDefault();
            data = ((b == 0) ? 0 : a / b) * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "NABCAP");

            fieldName = "TotalCapitalRatio";
            data = (double)policy.CapitalPolicies.Where(s => s.ShortName == "totalCap").Select(s => s.CurrentRatio).FirstOrDefault() * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "TRBCR");

            fieldName = "Tier1CapitalRatio";
            data = (double)policy.CapitalPolicies.Where(s => s.ShortName == "tier1Capital").Select(s => s.CurrentRatio).FirstOrDefault() * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "T1RBCR");

            fieldName = "Tier1LeverageCapitalRatio";
            data = (double)policy.CapitalPolicies.Where(s => s.ShortName == "tier1Leverage").Select(s => s.CurrentRatio).FirstOrDefault() * 100;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "INDC_CAR", "T1LCR");

            //TODO this is a new field and not implemented yet
            fieldName = "CommonEquityTier1";
            data = ar.GetFields("ALA", 4, new int[] { 1, 4 }); //TODO 7 (Common Equity Tier 1) doesn't actually exist? Replaced this (and others) with 4 (Trust Equity Tier 1) which legacy seems to use instead?
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushLRM(bankKey, data, "IDUNNO1", "IDUNNO2");

            Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            //Sourcesoffunds Push
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM [DIP_Data].[dbo].[Sourcesoffunds] WHERE AsOfDate = '1/1/1910' AND BankKey = {0}");
            for (int i = 0; i < 2; i++)
            {
                sqlQuery.AppendLine("");
                sqlQuery.AppendLine("INSERT INTO [DIP_Data].[dbo].[Sourcesoffunds]");
                sqlQuery.AppendLine("	([BankKey],");
                sqlQuery.AppendLine("	[AsOfDate],");
                sqlQuery.AppendLine("	[colName],");
                sqlQuery.AppendLine("	[PeriodType],");
                sqlQuery.AppendLine("	[SrcName],");
                sqlQuery.AppendLine("	[Q1],");
                sqlQuery.AppendLine("	[Q2],");
                sqlQuery.AppendLine("	[Q3],");
                sqlQuery.AppendLine("	[Q4],");
                sqlQuery.AppendLine("	[Q5],");
                sqlQuery.AppendLine("	[Q6],");
                sqlQuery.AppendLine("	[Q7],");
                sqlQuery.AppendLine("	[Q8],");
                sqlQuery.AppendLine("	[Q9],");
                sqlQuery.AppendLine("	[Q10],");
                sqlQuery.AppendLine("	[Q11],");
                sqlQuery.AppendLine("	[Q12],");
                sqlQuery.AppendLine("	[Q13],");
                sqlQuery.AppendLine("	[Q14],");
                sqlQuery.AppendLine("	[Q15],");
                sqlQuery.AppendLine("	[Q16],");
                sqlQuery.AppendLine("	[Q17],");
                sqlQuery.AppendLine("	[Q18],");
                sqlQuery.AppendLine("	[Q19],");
                sqlQuery.AppendLine("	[Q20],");
                sqlQuery.AppendLine("	[Q21],");
                sqlQuery.AppendLine("	[Q22],");
                sqlQuery.AppendLine("	[Q23],");
                sqlQuery.AppendLine("	[Q24])");
                sqlQuery.AppendLine("SELECT");
                sqlQuery.AppendLine("	{0},");
                sqlQuery.AppendLine("	[AsOfDate],");
                sqlQuery.AppendLine("	[colName],");
                sqlQuery.AppendLine(String.Format("    {0}, --[PeriodType],", i));
                sqlQuery.AppendLine("	[SrcName],");
                sqlQuery.AppendLine("	[Q1],");
                sqlQuery.AppendLine("	[Q2],");
                sqlQuery.AppendLine("	[Q3],");
                sqlQuery.AppendLine("	[Q4],");
                sqlQuery.AppendLine("	[Q5],");
                sqlQuery.AppendLine("	[Q6],");
                sqlQuery.AppendLine("	[Q7],");
                sqlQuery.AppendLine("	[Q8],");
                sqlQuery.AppendLine("	[Q9],");
                sqlQuery.AppendLine("	[Q10],");
                sqlQuery.AppendLine("	[Q11],");
                sqlQuery.AppendLine("	[Q12],");
                sqlQuery.AppendLine("	[Q13],");
                sqlQuery.AppendLine("	[Q14],");
                sqlQuery.AppendLine("	[Q15],");
                sqlQuery.AppendLine("	[Q16],");
                sqlQuery.AppendLine("	[Q17],");
                sqlQuery.AppendLine("	[Q18],");
                sqlQuery.AppendLine("	[Q19],");
                sqlQuery.AppendLine("	[Q20],");
                sqlQuery.AppendLine("	[Q21],");
                sqlQuery.AppendLine("	[Q22],");
                sqlQuery.AppendLine("	[Q23],");
                sqlQuery.AppendLine("	[Q24]");
                sqlQuery.AppendLine("FROM [DIP_Data].[dbo].[Sourcesoffunds]");
                sqlQuery.AppendLine("WHERE AsOfDate = '1/1/1910' AND BankKey = -99999");
            }
            Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey));

            fieldName = "SFP-LoansAgriculture";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 2, new int[] { 5, 6 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 2, new int[] { 5, 6 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "LCF", "AGGR");

            fieldName = "SFP-LoansCommercialIndustrial";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 2, new int[] { 2, 13 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 2, new int[] { 2, 13 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "LCF", "C_AND_I");

            fieldName = "SFP-LoansCommercial";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 2, new int[] { 1 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 2, new int[] { 1 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "LCF", "CRE");

            fieldName = "SFP-LoansConstruction";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 2, new int[] { 7, 8 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 2, new int[] { 7, 8 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "LCF", "CONST");

            fieldName = "SFP-LoansResidential";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 2, new int[] { 4 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 2, new int[] { 4 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "LCF", "REE");

            fieldName = "SFP-LoansOther";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 2, new int[] { 14, 9, 3, 15, 10, 16, 0 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 2, new int[] { 14, 9, 3, 15, 10, 16, 0 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "LCF", "OTHER");

            fieldName = "SFP-InvestmentsMBSCMO";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 1, new int[] { 4, 3 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 1, new int[] { 4, 3 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "INVCSH", "MBS_CMOB");

            fieldName = "SFP-InvestmentsMunicipals";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 1, new int[] { 5 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 1, new int[] { 5 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "INVCSH", "MUNIB");

            fieldName = "SFP-InvestmentsOtherInvestments";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 1, new int[] { 2, 7, 9 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 1, new int[] { 2, 7, 9 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "INVCSH", "OTHERINV");

            //TODO this may not actually be from Basic Surplus?
            fieldName = "SFP-InvestmentsOvernightInvestments";
            data = (double)bsAdmin.LeftS1OvntFunds.Where(s => s.FieldName == "leftS1OvntFundsSrtTrmInv").Select(s => s.Balance).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "INVCSH", "OVERNI");

            fieldName = "SFP-InvestmentsOtherLiquidInvestments";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 1, new int[] { 11, 0 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 1, new int[] { 11, 0 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "INVCSH", "OTHLIQI");

            fieldName = "SFP-InvestmentsUSTGSEBonds";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 1, new int[] { 8, 1 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 1, new int[] { 8, 1 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "INVCSH", "UST_GSEDEB");

            fieldName = "SFP-TimeDepositsBrokered";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 8, new int[] { 9, 5 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 8, new int[] { 9, 5 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "DEPMAT", "BROK_CD");

            fieldName = "SFP-TimeDepositsCDARS1Way";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 8, new int[] { 10 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 8, new int[] { 10 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "DEPMAT", "CDARSDM");

            fieldName = "SFP-TimeDepositsInternetNational";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 8, new int[] { 6, 7, 8 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 8, new int[] { 6, 7, 8 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "DEPMAT", "INATCDDM");

            fieldName = "SFP-TimeDepositsJumboCDs";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 8, new int[] { 3 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 8, new int[] { 3 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "DEPMAT", "JUMBOMAT");

            fieldName = "SFP-TimeDepositsRegularLocalCD";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 8, new int[] { 1, 0 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 8, new int[] { 1, 0 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "DEPMAT", "CDM");

            fieldName = "SFP-TimeDepositsSecuredTimeDeposits";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 8, new int[] { 2, 4 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 8, new int[] { 2, 4 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "DEPMAT", "STDSM");

            fieldName = "SFP-BorrowingsFHLBAdvances";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 6, new int[] { 1 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 6, new int[] { 1 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "BORMAT", "BOW_LOAN");

            fieldName = "SFP-BorrowingsFRBBorrowings";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 6, new int[] { 8 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 6, new int[] { 8 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "NMDB", "IDUNNO2"); //TODO ???

            fieldName = "SFP-BorrowingsOtherCollateralizedBorrowings";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 6, new int[] { 0, 5, 7 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 6, new int[] { 0, 5, 7 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "BORMAT", "OTHER_BOR");

            fieldName = "SFP-BorrowingsUnsecuredBorrowings";
            dataArrays[0] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Monthly, 24, "ALP345", 6, new int[] { 2, 4, 6 });
            dataArrays[1] = ar.GetFieldsRange(fProjection, L360PushHelper.PeriodType.Quarterly, 24, "ALP345", 6, new int[] { 2, 4, 6 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, dataArrays, "BORMAT", "BORUNSEC");

            fieldName = "SFP-NonMaturityDepositBrokeredMMDA";
            data = ar.GetFields("ALA", 3, new int[] { 13, 14, 15, 16 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "BROKMMDA");

            fieldName = "SFP-NonMaturityDepositCoreSavingsMMDA";
            data = ar.GetFields("ALA", 3, new int[] { 5, 7, 11, 12 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "CSVMMDA");

            fieldName = "SFP-NonMaturityDepositDDA";
            data = ar.GetFields("ALA", 3, new int[] { 1, 9 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "DDA");

            fieldName = "SFP-NonMaturityDepositInterestChecking";
            data = ar.GetFields("ALA", 3, new int[] { 3, 10 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "INTCHECK");

            fieldName = "SFP-NonMaturityDepositOtherSecuredNonMaturityDeposits";
            data = ar.GetFields("ALA", 3, new int[] { 2, 4, 6, 8 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "OSNMDS");

            fieldName = "SFP-NonMaturityDepositPremiumSavings";
            data = 0; //ar.GetFields("ALA", 3, new int[] {  }); //TODO Not imported?
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "IDUNNO2"); //TODO ???

            fieldName = "SFP-NonMaturityDepositRetailRepos";
            data = ar.GetFields("ALA", 6, new int[] { 2, 3 }); //Note: These are actually in Borrowings, not NMDs
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushSOF(bankKey, data, "NMDB", "RETREPP");

            //AlcoRatios Push
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM [DIP_Data].[dbo].[AlcoRatios] WHERE AsOfDate = '1/1/1910' AND BankKey = {0}");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("INSERT INTO [DIP_Data].[dbo].[AlcoRatios]");
            sqlQuery.AppendLine("	([BankKey],");
            sqlQuery.AppendLine("	[AsOfDate],");
            sqlQuery.AppendLine("	[SrcCat],");
            sqlQuery.AppendLine("	[ALCOValue],");
            sqlQuery.AppendLine("	[SrcGroup],");
            sqlQuery.AppendLine("	[SrcDescription],");
            sqlQuery.AppendLine("	[Exclude],");
            sqlQuery.AppendLine("	[ShowFootNote],");
            sqlQuery.AppendLine("	[DisplayOrder],");
            sqlQuery.AppendLine("	[RatioType],");
            sqlQuery.AppendLine("	[MinMaxType])");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	{0},");
            sqlQuery.AppendLine("	[AsOfDate],");
            sqlQuery.AppendLine("	[SrcCat],");
            sqlQuery.AppendLine("	[ALCOValue],");
            sqlQuery.AppendLine("	[SrcGroup],");
            sqlQuery.AppendLine("	[SrcDescription],");
            sqlQuery.AppendLine("	[Exclude],");
            sqlQuery.AppendLine("	[ShowFootNote],");
            sqlQuery.AppendLine("	[DisplayOrder],");
            sqlQuery.AppendLine("	[RatioType],");
            sqlQuery.AppendLine("	[MinMaxType]");
            sqlQuery.AppendLine("FROM [DIP_Data].[dbo].[AlcoRatios]");
            sqlQuery.AppendLine("WHERE asOfDate = '1/1/1910' AND BankKey = -99999");
            Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey));

            fieldName = "DTP-Assets";
            data = (double)bsAdmin.MidS1Miscs.Where(s => s.FieldName == "midS1MiscsTotalAsset").Select(s => s.Amount).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Assets", "ARASSETS");

            fieldName = "DTP-LoansTotalBalance";
            data = ar.GetFields("ALA", 2);
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Loans", "ARTOTBALLO");

            //TODO why is this not actually Total Investments in legacy?
            fieldName = "DTP-TotalInvestments";
            //data = ar.GetFields("ALA", 1);
            data = (double)bsAdmin.BasicSurplusMarketValues.Where(s => s.Name == "Market Value").Select(s => s.Total).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Investments", "ARINVEST");

            fieldName = "DTP-DepositsBrokered";
            data = ar.GetFields("ALA", 3, new int[] { 16, 14, 13, 15 }) + ar.GetFields("ALA", 8, new int[] { 10, 5, 9 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Deposits", "ARBROK");

            fieldName = "DTP-DepositsTotalBalance";
            data = ar.GetFields("ALA", 3) + ar.GetFields("ALA", 8);
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Deposits", "ARDEPTB");

            fieldName = "DTP-Borrowings";
            data = (policy.LiquidityPolicies.Where(s => s.ShortName == "borrowAsset").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incRetailRepo").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                ? ar.GetFields("ALA", 6)
                //: ar.GetFieldsExcl("ALA", 6, new int[] { 2, 3 });
                : ar.GetFieldsExcl("ALA", 6, new int[] { 3 }); //TODO legacy only pulls Secured Retail (3) as "Retail Repos" instead of Un/Secured Retail (2, 3) - should this use both?
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Borrowings", "ARBORROW");

            fieldName = "DTP-WholesaleFunds";
            data = ar.GetFieldsExcl("ALA", 6, new int[] { 3, 2 }) + ar.GetFields("ALA", 3, new int[] { 16, 14, 13 }) + ar.GetField("ALA", 8, 10);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredDeposits").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incBrokeredRecip").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                data += ar.GetField("ALA", 3, 15) + ar.GetField("ALA", 8, 9);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredDeposits").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incNationalDep").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                data += ar.GetField("ALA", 8, 6);
            if (policy.LiquidityPolicies.Where(s => s.ShortName == "borrowDeposits").Select(s => s.LiquidityPolicySettings.Where(t => t.ShortName == "incRetailRepo").Select(t => t.Include).FirstOrDefault()).FirstOrDefault())
                data += ar.GetFields("ALA", 6, new int[] { 3, 2 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Deposits", "ARWSFUND");

            fieldName = "DTP-FairValueOfInvestmentCollateralBase";
            data = (double)(bsAdmin.LeftS1SecColTotalSecMktValUSTAgency + bsAdmin.LeftS1SecColTotalSecMktValMBSAgency + bsAdmin.LeftS1SecColTotalSecMktValMBSPvt
                + bsAdmin.RightS1OtherLiqTotalMarketValue); //TODO do we actually push this?
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Investments", "ARFVCB");

            fieldName = "DTP-FairValueOfInvestmentCollateral300";
            data = 0; //TODO huh? (We apparently don't push this?)
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Investments", "ARFV300");

            fieldName = "DTP-FreeInvCollateralAndShortTermInv";
            data = (double)(bsAdmin.LeftS1BlankTotalAmount
                + bsAdmin.LeftS1LiqAssets.Where(s => s.FieldName == "leftS1LiqAssetsColSecPos" || s.FieldName == "leftS1LiqAssetsOther").Select(s => s.Amount).Sum()
                + bsAdmin.LeftS1OvntFunds.Where(s => s.FieldName == "leftS1OvntFundsSrtTrmInv").Select(s => s.Balance).FirstOrDefault());
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Investments", "ARFINVST");

            fieldName = "DTP-PledgedSecurities";
            data = (double)bsAdmin.BasicSurplusMarketValues.Where(s => s.Name == "Amount Pledged").Select(s => s.Total).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Investments", "ARPLSEC");

            fieldName = "DTP-LoansCommercialIndustrial";
            data = ar.GetFields("ALA", 2, new int[] { 2 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Loans", "ARCANDI");

            fieldName = "DTP-LoansConstruction";
            data = ar.GetFields("ALA", 2, new int[] { 7, 8 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Loans", "ARConst");

            fieldName = "DTP-LoansCommercialRE";
            data = ar.GetFields("ALA", 2, new int[] { 1 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Loans", "ARCOMRE");

            fieldName = "DTP-AllowanceForLoanLoss";
            data = ar.GetFields("ALA", 2, new int[] { 12 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Loans", "ARALLOWLL");

            fieldName = "DTP-DepositsInternetNational";
            data = ar.GetFields("ALA", 8, new int[] { 7, 8, 9 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Deposits", "ARINTNAT");

            fieldName = "DTP-DepositsNonMaturity";
            data = ar.GetFields("ALA", 3);
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Deposits", "ARNONMAT");

            fieldName = "DTP-BorrowingsSecuredRetailSweeps";
            data = ar.GetFields("ALA", 6, new int[] { 3 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "Borrowings", "ARSRSWEEP");

            fieldName = "DTP-BorrowingsAssets";
            data = (double)policy.LiquidityPolicies.Where(s => s.ShortName == "borrowAsset").Select(s => s.PolicyLimit).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "MIN/MAX Value", "MMRBORASS");

            fieldName = "DTP-BrokeredDepositsAssets";
            data = (double)policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredAsset").Select(s => s.PolicyLimit).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "MIN/MAX Value", "MMRBRKDASS");

            fieldName = "DTP-BrokeredDepositsDeposits";
            data = (double)policy.LiquidityPolicies.Where(s => s.ShortName == "brokeredDeposits").Select(s => s.PolicyLimit).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "MIN/MAX Value", "MMRBRKDDEP");

            fieldName = "DTP-NetLoansAssets";
            data = (double)policy.OtherPolicies.Where(s => s.ShortName == "netLoanAssets" && s.Name == "Net Loans / Assets").Select(s => s.PolicyLimit).FirstOrDefault(); //TODO two "netLoansAssets" ShortNames
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "MIN/MAX Value", "MMRNLASS");

            fieldName = "DTP-NetLoansDeposits";
            data = (double)policy.OtherPolicies.Where(s => s.ShortName == "netLoanDeposits").Select(s => s.PolicyLimit).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "MIN/MAX Value", "MMRNLDEP");

            fieldName = "DTP-WholesaleFundsAssets";
            data = (double)policy.LiquidityPolicies.Where(s => s.ShortName == "wholeAsset").Select(s => s.PolicyLimit).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAR(bankKey, data, "MIN/MAX Value", "MMRWFASS");

            //AllocatedExCol Push
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM [DIP_Data].[dbo].[AllocatedExCol] WHERE AsOfDate = '1/1/1910' AND BankKey = {0}");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("INSERT INTO [DIP_Data].[dbo].[AllocatedExCol]");
            sqlQuery.AppendLine("	([BankKey],");
            sqlQuery.AppendLine("	[AsOfDate],");
            sqlQuery.AppendLine("	[SrcCol],");
            sqlQuery.AppendLine("	[srcKey],");
            sqlQuery.AppendLine("	[TotalPortBal],");
            sqlQuery.AppendLine("	[AvailCol],");
            sqlQuery.AppendLine("	[GrossQPer])");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	{0},");
            sqlQuery.AppendLine("	[AsOfDate],");
            sqlQuery.AppendLine("	[SrcCol],");
            sqlQuery.AppendLine("	[srcKey],");
            sqlQuery.AppendLine("	[TotalPortBal],");
            sqlQuery.AppendLine("	[AvailCol],");
            sqlQuery.AppendLine("	[GrossQPer]");
            sqlQuery.AppendLine("FROM [DIP_Data].[dbo].[AllocatedExCol]");
            sqlQuery.AppendLine("WHERE asOfDate = '1/1/1910' AND BankKey = -99999");
            Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey));

            fieldName = "AEC-InvestmentsMBSCMOGovt";
            data = (double)bsAdmin.LeftS1SecColTotalSecMktValMBSAgency;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCINVFEC", "MBSGVTAEC");

            fieldName = "AEC-InvestmentsMBSCMOPvt";
            data = (double)bsAdmin.LeftS1SecColTotalSecMktValMBSPvt;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCINVFEC", "MBSPVTAEC");

            fieldName = "AEC-InvestmentsMunicipals";
            data = (double)bsAdmin.RightS1OtherLiqs.Where(s => s.FieldName == "rightS1OtherLiqsMuniSec").Select(s => s.MarketValue).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCINVFEC", "MUNIAEC");

            fieldName = "AEC-InvestmentsOtherInvestments";
            data = (double)bsAdmin.RightS1OtherLiqs.Where(s => s.FieldName == "rightS1OtherLiqsCorpSec" || s.FieldName == "rightS1OtherLiqsOther" || s.FieldName == "rightS1OtherLiqsEqSec").Select(s => s.MarketValue).Sum();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCINVFEC", "OINVAEC");

            fieldName = "AEC-InvestmentsUSTGSEBonds";
            data = (double)bsAdmin.LeftS1SecColTotalSecMktValUSTAgency;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCINVFEC", "USTGEAEC");

            fieldName = "AEC-LoansAgricultural";
            data = ar.GetFields("ALA", 2, new int[] { 5, 6 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCLOFEC", "AGRIAEC");

            fieldName = "AEC-LoansCommercialIndustrial";
            data = ar.GetFields("ALA", 2, new int[] { 2, 13 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCLOFEC", "CANDIAEC");

            fieldName = "AEC-LoansCommercialRE";
            data = ar.GetField("ALA", 2, 1);
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCLOFEC", "COMREAEC");

            fieldName = "AEC-LoansConstruction";
            data = ar.GetFields("ALA", 2, new int[] { 7, 8 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCLOFEC", "CONSTAEC");

            fieldName = "AEC-LoansOtherLoans";
            data = ar.GetFields("ALA", 2, new int[] { 3, 9, 14, 15, 10, 16, 0 });
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCLOFEC", "OTHLOAEC");

            fieldName = "AEC-LoansResidentialMortgages";
            data = ar.GetField("ALA", 2, 4);
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushAEC(bankKey, data, "ALLOCLOFEC", "RESMTGAEC");

            //Collateral Push
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM [DIP_Data].[dbo].[Collateral] WHERE AsOfDate = '1/1/1910' AND BankKey = {0}");
            sqlQuery.AppendLine("");
            sqlQuery.AppendLine("INSERT INTO [DIP_Data].[dbo].[Collateral]");
            sqlQuery.AppendLine("	([BankKey],");
            sqlQuery.AppendLine("	[AsOfDate],");
            sqlQuery.AppendLine("	[SrcCol],");
            sqlQuery.AppendLine("	[SrcKey],");
            sqlQuery.AppendLine("	[RetailRepos],");
            sqlQuery.AppendLine("	[SecuredDeposits],");
            sqlQuery.AppendLine("	[FHLB],");
            sqlQuery.AppendLine("	[OtherBorrows],");
            sqlQuery.AppendLine("	[FRB])");
            sqlQuery.AppendLine("SELECT");
            sqlQuery.AppendLine("	{0},");
            sqlQuery.AppendLine("	[AsOfDate],");
            sqlQuery.AppendLine("	[SrcCol],");
            sqlQuery.AppendLine("	[SrcKey],");
            sqlQuery.AppendLine("	[RetailRepos],");
            sqlQuery.AppendLine("	[SecuredDeposits],");
            sqlQuery.AppendLine("	[FHLB],");
            sqlQuery.AppendLine("	[OtherBorrows],");
            sqlQuery.AppendLine("	[FRB]");
            sqlQuery.AppendLine("FROM [DIP_Data].[dbo].[Collateral]");
            sqlQuery.AppendLine("WHERE asOfDate = '1/1/1910' AND BankKey = -99999");
            Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey));

            dataArrays[0] = new double[5];

            fieldName = "COL-InvestmentsMBSCMOGovt";
            dataArrays[0][0] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsRetRepos").Select(s => s.MBSAgency).FirstOrDefault();
            dataArrays[0][1] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsMuniDep").Select(s => s.MBSAgency).FirstOrDefault();
            dataArrays[0][2] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsFHLB").Select(s => s.MBSAgency).FirstOrDefault();
            dataArrays[0][3] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsOther" || s.FieldName == "leftS1SecColsWholeRepos").Select(s => s.MBSAgency).Sum();
            dataArrays[0][4] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsOtherSec").Select(s => s.MBSAgency).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLESLINVESL", "MBSGVTESL");

            fieldName = "COL-InvestmentsMBSCMOPvt";
            dataArrays[0][0] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsRetRepos").Select(s => s.MBSPvt).FirstOrDefault();
            dataArrays[0][1] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsMuniDep").Select(s => s.MBSPvt).FirstOrDefault();
            dataArrays[0][2] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsFHLB").Select(s => s.MBSPvt).FirstOrDefault();
            dataArrays[0][3] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsOther" || s.FieldName == "leftS1SecColsWholeRepos").Select(s => s.MBSPvt).Sum();
            dataArrays[0][4] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsOtherSec").Select(s => s.MBSPvt).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLESLINVESL", "MBSPVTESL");

            fieldName = "COL-InvestmentsMunicipals";
            dataArrays[0][0] = 0; //"default to 0"
            dataArrays[0][1] = (double)bsAdmin.RightS1OtherLiqs.Where(s => s.FieldName == "rightS1OtherLiqsMuniSec").Select(s => s.Pledged).FirstOrDefault();
            dataArrays[0][2] = 0; //same here et al.
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLESLINVESL", "MUNIESL");

            fieldName = "COL-InvestmentsOtherInvestments";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = 0;
            dataArrays[0][3] = (double)bsAdmin.RightS1OtherLiqs.Where(s => s.FieldName == "rightS1OtherLiqsCorpSec" || s.FieldName == "rightS1OtherLiqsOther" || s.FieldName == "rightS1OtherLiqsEqSec").Select(s => s.Pledged).Sum();
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLESLINVESL", "OINVESL");

            fieldName = "COL-InvestmentsUSTGSEBonds";
            dataArrays[0][0] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsRetRepos").Select(s => s.USTAgency).FirstOrDefault();
            dataArrays[0][1] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsMuniDep").Select(s => s.USTAgency).FirstOrDefault();
            dataArrays[0][2] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsFHLB").Select(s => s.USTAgency).FirstOrDefault();
            dataArrays[0][3] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsOther" || s.FieldName == "leftS1SecColsWholeRepos").Select(s => s.USTAgency).Sum();
            dataArrays[0][4] = (double)bsAdmin.LeftS1SecCols.Where(s => s.FieldName == "leftS1SecColsOtherSec").Select(s => s.USTAgency).FirstOrDefault();
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLESLINVESL", "USTGEESL");

            fieldName = "COL-LoansAgricultural";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = 0;
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLOCLOESL", "AGRIESL");

            fieldName = "COL-LoansCommercialIndustrial";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = 0;
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLOCLOESL", "CANDIESL");

            fieldName = "COL-LoansCommercialRE";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = 0;
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLOCLOESL", "COMREESL");

            fieldName = "COL-LoansConstruction";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = 0;
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLOCLOESL", "CONSTESL");

            fieldName = "COL-LoansOtherLoans";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = 0;
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLOCLOESL", "OTHLOESL");

            fieldName = "COL-LoansResidentialMortgages";
            dataArrays[0][0] = 0;
            dataArrays[0][1] = 0;
            dataArrays[0][2] = (double)bsAdmin.LeftS3ColEncAmount;
            dataArrays[0][3] = 0;
            dataArrays[0][4] = 0;
            dt.Rows.Add(new object[] { fieldName, data, dataArrays.ToArray() });
            ar.SQLPushCOL(bankKey, dataArrays[0], "ALLOCLOESL", "RESMTGESL");

            //Execute anything remaining
            ar.SQLExecuteQueue();

            //Update CL_Settings
            string userName = Utilities.GetAtlasUserData().Rows[0]["UserName"].ToString();//HttpContext.Current.Profile.UserName;
            sqlQuery.Clear();
            sqlQuery.AppendLine("UPDATE [DIP_Data].[dbo].[CL_Settings]");
            sqlQuery.AppendLine("SET [lastDCGUpdate] = '{1}', [lastUpdater] = '{2}', [pushDate] = '{3}'");
            sqlQuery.AppendLine("WHERE [BankKey] = {0}");
            if (userName.IndexOf("@") > -1)
            {
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey, date, userName.Substring(0, userName.IndexOf("@")), DateTime.Now.ToShortDateString()));
            }
            else
            {
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), bankKey, date, userName, DateTime.Now.ToShortDateString()));
            }


            //TODO TEST thing
            return dt;
        }

        //Defined Groupings For Config Part
        [HttpGet]
        public IQueryable<DefinedGrouping> DefinedGroupings()
        {
            return _contextProvider.Context.DefinedGroupings.Include(s => s.DefinedGroupingGroups).AsQueryable();
        }


        //Get All Defined Groupings For Drop Down Part
        [HttpGet]
        public DataTable GetDefinedGroupings()
        {
            return Utilities.ExecuteSql(Utilities.BuildInstConnectionString("DDW_Atlas_Templates"), "SELECT 'dg_'+ CAST(id as varchar(5)) as  id, name, id as intId FROM [DDW_Atlas_Templates].[dbo].[ATLAS_DefinedGrouping]");
        }

        //Delete Defined Grouping
        [HttpGet]
        public string DeleteDefinedGrouping(int id)
        {

            //Delete Lowest Level
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" DELETE FROM ATLAS_DefinedGroupingClassifications WHERE id IN( ");
            sqlQuery.AppendLine(" SELECT dc.id FROM ATLAS_DefinedGroupingGroups as dg ");
            sqlQuery.AppendLine(" INNER JOIN  ");
            sqlQuery.AppendLine(" ATLAS_DefinedGroupingClassifications as dc ");
            sqlQuery.AppendLine(" ON dc.DefinedGroupingGroupId = dg.id ");
            sqlQuery.AppendLine("  where dg.DefinedGroupingId = " + id.ToString() + ") ");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());


            //Delete Second Level
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM ATLAS_DefinedGroupingGroups WHERE DefinedGroupingId = " + id.ToString() + ";");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            //Delete Top Level
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM ATLAS_DefinedGrouping WHERE id = " + id.ToString() + ";");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            return "SUCCESS";
        }

        //Delete Defined Grouping Group
        [HttpGet]
        public string DeleteDefinedGroupingGroup(int id)
        {

            //Delete Lowest Level
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" DELETE FROM ATLAS_DefinedGroupingClassifications WHERE DefinedGroupingGroupId = " + id.ToString() + "");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());


            //Delete Top Level
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM ATLAS_DefinedGroupingGroups WHERE id = " + id.ToString() + ";");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());


            return "SUCCESS";
        }

        //Get All Defined Grouping Groups
        [HttpGet]
        public IQueryable<DefinedGroupingGroup> DefinedGroupingGroups()
        {
            return _contextProvider.Context.DefinedGroupingGroups.Include(s => s.DefinedGroupingClassification).Include(s => s.DefinedGrouping).AsQueryable();
        }

        //Get All Classification based off of asset flag
        [HttpGet]
        public IQueryable<Classification> Classifications(bool isAsset)
        {
            return _contextProvider.Context.Classifications.Include(s => s.AccountType).Where(s => s.IsAsset == isAsset).OrderBy(s => s.AccountType.Name).AsQueryable();
        }

        [HttpGet]
        public IQueryable<Policy> Policy(string asOfDate = null)
        {
            //Eve Config, If does not exist create it
            string conStr = Utilities.BuildInstConnectionString("");
            DateTime date = DateTime.Parse(asOfDate ?? Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            bool newEve = (int)Utilities.ExecuteSql(conStr, "SELECT COUNT(*) FROM ATLAS_Policy WHERE asOfDate = '" + date.ToShortDateString() + "'").Rows[0][0] == 0;
            string instType = CreditUnionStatus().Rows[0]["InstType"].ToString();
            var sqlQuery = new StringBuilder();
            if (newEve)
            {
                int basicSurplusId = 0;
                DataTable bs = Utilities.ExecuteSql(conStr, "SELECT TOP 1 bs.id FROM ATLAS_BasicSurplusAdmin as bs INNER JOIN asOfdateInfo as aod ON aod.id = bs.InformationId WHERE asOfDate = '" + date.ToShortDateString() + "'");
                if (bs.Rows.Count == 0)
                {
                    basicSurplusId = -1;
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_Policy] ");
                    sqlQuery.AppendLine("            ([AsOfDate] ");
                    sqlQuery.AppendLine("            ,[NIISimulationTypeId] ");
                    sqlQuery.AppendLine("            ,[EveSimulationTypeId] ");
                    sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
                    sqlQuery.AppendLine("            ,[ReportedEveScenarioId] ");
                    sqlQuery.AppendLine("            ,[UseDefault] ");
                    sqlQuery.AppendLine("            ,[MeetingDate] ");
                    sqlQuery.AppendLine("            ,[LiquidityRiskAssessment] ");
                    sqlQuery.AppendLine("            ,[NIIRiskAssessment] ");
                    sqlQuery.AppendLine("            ,[CapitalRiskAssessment]) ");
                    sqlQuery.AppendLine("      VALUES ");
                    sqlQuery.AppendLine("	('{0}',1,2,{1},30,1, '{2}',0,0,0)");
                    sqlQuery.AppendLine("");
                    Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString(), "-1", date.ToShortDateString()));
                }
                else
                {
                    basicSurplusId = (int)bs.Rows[0][0];
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_Policy] ");
                    sqlQuery.AppendLine("            ([AsOfDate] ");
                    sqlQuery.AppendLine("            ,[NIISimulationTypeId] ");
                    sqlQuery.AppendLine("            ,[EveSimulationTypeId] ");
                    sqlQuery.AppendLine("            ,[BasicSurplusAdminId] ");
                    sqlQuery.AppendLine("            ,[ReportedEveScenarioId] ");
                    sqlQuery.AppendLine("            ,[UseDefault]");
                    sqlQuery.AppendLine("            ,[MeetingDate] ");
                    sqlQuery.AppendLine("            ,[LiquidityRiskAssessment] ");
                    sqlQuery.AppendLine("            ,[NIIRiskAssessment] ");
                    sqlQuery.AppendLine("            ,[CapitalRiskAssessment]) ");
                    sqlQuery.AppendLine("      VALUES ");
                    sqlQuery.AppendLine("	('{0}',1,2,{1},30,1, '{2}',0,0,0)");
                    sqlQuery.AppendLine("");
                    string qS = String.Format(sqlQuery.ToString(), date.ToShortDateString(), basicSurplusId.ToString(), date.ToShortDateString());
                    Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString(), basicSurplusId.ToString(), date.ToShortDateString()));
                }

            }

            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT Id FROM [dbo].[ATLAS_Policy] WHERE asOfDate = '{0}'");
            sqlQuery.AppendLine("");
            int eveId = (int)Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString())).Rows[0][0];

            if (newEve)
            {
                sqlQuery.Clear();
                //Basic Surplus Rows For Liquidity Section
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Basic Surplus (Min.)", "bsmin", "1", "0"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Basic Surplus w/ FHLB (Min.)", "bsminFHLB", "1", "1"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Basic Surplus w/ FHLB & Brokered (Min.)", "bsminBrokered", "1", "2"));

                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Total Liquid Assets", "bsminLiqAssets", "1", "3"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "On Balance Sheet Liquidity", "bsminOnBalance", "1", "4"));

                //FHLB Sections for liquidity sections 
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "FHLB/Assets (Max.)", "fhlbAssets", "1", "5"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "FHLB/Deposits (Max.)", "fhlbDeposits", "1", "6"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Maximum FHLB Capacity", "fhlbMax", "1", "7"));

                //Borrowings rows For Liquidity Section                
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}',-999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Borrowings / Assets (Max.)", "borrowAsset", "0", "8"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}',-999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Borrowings / Deposits", "borrowDeposits", "0", "9"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}',-999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Maximum Borrowing Capacity", "borrowMax", "0", "10"));

                //Brokered Deposits Rows For Liquidity Section
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Brokered Deposits / Assets (Max.)", "brokeredAsset", "0", "11"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Brokered Deposits / Deposits (Max.)", "brokeredDeposits", "0", "12"));
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Maximum Brokered Capacity", "brokeredMax", "0", "13"));
                //Wholesale Funds 
                sqlQuery.AppendLine(String.Format(" INSERT INTO [dbo].[ATLAS_LiquidityPolicy]  ([PolicyId] ,[Name] ,[ShortName], [PolicyLimit] ,[WellCap] ,[RiskAssessment] ,[WellCapAvailable] ,[Priority], [CurrentRatio])  VALUES ({0}, '{1}', '{2}', -999, -999, 1, {3}, {4}, 0) ", eveId.ToString(), "Total Wholesale Funds / Assets (Max.)", "wholeAsset", "0", "14"));


                //Insert Recoreds To Policies
                Utilities.ExecuteSql(conStr, sqlQuery.ToString());


                //Insert Liquidity Policy Settings For The Above Rows

                //Borrowing Options

                //Borrowings Assets Include Options
                int polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND PolicyId = {1}", "borrowAsset", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Retail Repos In Borrowings", "incRetailRepo", "0", "0"));
                //Borrowinfs Deposit Include Options
                polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND PolicyId = {1}", "borrowDeposits", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Retail Repos In Borrowings", "incRetailRepo", "0", "0"));
                //Maximum Borrowing Capcity Options 
                polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND PolicyId = {1}", "borrowMax", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Retail Repos In Borrowings", "incRetailRepo", "0", "0"));


                //Brokered DEposits options

                //Brokered Deposits Assets Include Options
                polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND PolicyId = {1}", "brokeredAsset", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Brokered Recip in Brokered Deposits", "incBrokeredRecip", "1", "0"));
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc National Deposits in Brokered Deposits", "incNationalDep", "0", "1"));
                //Brokered Deposits Deposits Include Options
                polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND  PolicyId = {1}", "brokeredDeposits", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Brokered Recip in Brokered Deposits", "incBrokeredRecip", "1", "0"));
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc National Deposits in Brokered Deposits", "incNationalDep", "0", "1"));
                //Maximum Brokered Capcaity Include Options
                polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND  PolicyId = {1}", "brokeredMax", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Brokered Recip in Brokered Deposits", "incBrokeredRecip", "1", "0"));
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc National Deposits in Brokered Deposits", "incNationalDep", "0", "1"));


                //WholeSale Options

                //Total Wholesale Funds Assets
                polId = (int)Utilities.ExecuteSql(conStr, String.Format("SELECT Id FROM [dbo].[ATLAS_LiquidityPolicy] WHERE shortName = '{0}' AND PolicyId = {1}", "wholeAsset", eveId.ToString())).Rows[0][0];
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Brokered Recip in Wholesale Funds", "incBrokeredRecip", "1", "0"));
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc Retail Repos in Wholesale Funds", "incRetailRepos", "0", "1"));
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_LiquidityPolicySetting] ([LiquidityPolicyId] ,[Name] ,[ShortName] ,[Include] ,[Priority]) VALUES ({0}, '{1}', '{2}', {3}, {4})", polId.ToString(), "Inc National Deposits in Wholesale Funds", "incNationalDep", "0", "2"));


                //NII Policies
                Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_NIIPolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 0,0, '% Change in NII w +/- 200BP ramp (12mths)', 'changeInNII'); SELECT SCOPE_IDENTITY() as newId;", eveId.ToString()));


                //First Insert PArent For Year 1 NII % Change from Year 1 Base
                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_NIIPolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 0,0, 'Year 1 NII % Change from Year 1 Base', 'year1Year1'); SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());

                //Now Insert All Scenarios For Above Parent
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,38,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,2,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,3,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,4,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,5,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,6,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,7,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,8,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,8,9,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,9,10,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,10,11,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,11,12,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,12,13,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,13,14,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,15,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,16,0) ");
                //sqlQuery.AppendLine("  ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,13,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,14,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,15,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,16,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,19,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,18,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,18,17,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,19,20,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,20,21,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,21,22,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,22,23,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,23,37,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));

                //First Insert PArent For Year 2 NII % Change from Year 1 Base
                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_NIIPolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 1,0, 'Year 2 NII % Change from Year 1 Base', 'year2Year1');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());

                //Now Insert All Scenarios For Above Parent
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,-1,1,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,38,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,2,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,3,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,4,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,5,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,6,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,7,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,8,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,8,9,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,9,10,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,10,11,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,11,12,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,12,13,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,13,14,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,15,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,16,0) ");
                //sqlQuery.AppendLine("  ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,13,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,14,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,15,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,16,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,19,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,18,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,18,17,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,19,20,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,20,21,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,21,22,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,22,23,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,23,37,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));


                //First Insert PArent For Year 2 NII % Change from Year 2 Base
                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_NIIPolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 1,0, 'Year 2 NII % Change from Year 2 Base', 'year2Year2');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());

                //Now Insert All Scenarios For Above Parent
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,38,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,2,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,3,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,4,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,5,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,6,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,7,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,8,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,8,9,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,9,10,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,10,11,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,11,12,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,12,13,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,13,14,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,15,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,16,0) ");
                //sqlQuery.AppendLine("  ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,13,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,14,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,15,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,16,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,19,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,18,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,18,17,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,19,20,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,20,21,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,21,22,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,22,23,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,23,37,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));


                //First Insert PArent For 24 Month Change From Base
                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_NIIPolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 1,0, '24 Month % Change From Base', '24Month');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());

                //Now Insert All Scenarios For Above Parent
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,38,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,2,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,3,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,4,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,5,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,6,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,7,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,8,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,8,9,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,9,10,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,10,11,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,11,12,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,12,13,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,13,14,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,15,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,16,0) ");
                sqlQuery.AppendLine("  ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,13,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,14,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,15,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,16,0) ");
                //sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,19,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,18,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,18,17,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,19,20,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,20,21,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,21,22,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,22,23,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_NIIPolicy] ([NIIPolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,23,37,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));

                //First Insert PArent For Core Fund
                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_CoreFundParent] ([PolicyId] ,[PolicyLimit],[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1,0, 'Core Funding Utilization (Max.)', 'coreFund');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());
                //Core Fund Scenarios
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,-1,1,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,38,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,2,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,3,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,4,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,5,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,6,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,7,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,8,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,8,9,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,9,10,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,10,11,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,11,12,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,12,13,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,13,14,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,15,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,16,0) ");
                //sqlQuery.AppendLine("  ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,14,13,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,15,14,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,15,0) ");
                //sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,16,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,16,19,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,17,18,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,18,17,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,19,20,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,20,21,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,21,22,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,22,23,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CoreFundPolicy] ([CoreFundParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,23,37,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));

                //EVE/NEV

                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_EvePolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 2,0, 'Post-Shock EVE/NEV Ratio (EVE/EVA)', 'postShock');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,-1,30,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,24,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,25,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,26,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,28,0) ");
                sqlQuery.AppendLine("  ");

                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,32,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,34,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,35,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,36,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));

                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_EvePolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 3,0, 'BP Change in EVE/NEV Ratio (EVE/EVA) from 0 Shock', 'bpChange');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,24,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,25,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,26,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,28,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,32,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,34,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,35,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,36,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));


                polId = int.Parse(Utilities.ExecuteSql(conStr, String.Format("INSERT INTO [dbo].[ATLAS_EvePolicyParent] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName]) VALUES ({0}, 0, 1, 4,0, 'EVE/NEV % Change from 0 Shock', 'eveChange');SELECT SCOPE_IDENTITY() as newId;", eveId.ToString())).Rows[0][0].ToString());
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,0,24,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,1,25,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,2,26,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,3,28,0) ");
                sqlQuery.AppendLine("  ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,4,32,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,5,34,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,6,35,0) ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_EvePolicy] ([EvePolicyParentId] ,[PolicyLimit] ,[RiskAssessment] ,[Priority] ,[ScenarioTypeId] ,[CurrentRatio]) VALUES({0},-999,1,7,36,0) ");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), polId));

                //Capital Ratios
                sqlQuery.Clear();
                if (instType == "BK")
                //if(true)
                {
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Tier 1 Leverage Ratio', 'tier1Leverage', -999, -999, 1, 1,1,1, 0, 'perc', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Tier 1 Common Capital (CET1) Risk Based', 'tier1Common',  -999, -999, 1, 1,1,1, 1, 'perc', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Tier 1 Capital Ratio Risk Based', 'tier1Capital',  -999, -999, 1, 1,1,1, 2, 'perc', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Total Capital Ratio Risk Based', 'totalCap',  -999, -999, 0, 0, 0, 0, 3, 'perc', 0); ");


                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Common Capital (CET1)', 'commonCap',  -999, -999, 0, 0,0,0, 4, 'balance', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Tier 1 Capital', 'tier1CapitalBal',  -999, -999, 0, 0,0,0, 4, 'balance', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Total Capital', 'totalCapital',  -999, -999, 0, 0,0,0, 5, 'balance', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Unrealized G/L (Liabilities)', 'unrealized',  -999, -999, 0, 0,0,0, 6, 'balance', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Total Assets for Leverage Ratio', 'averageAssets',  -999, -999, 0, 0,0,0, 6, 'balance', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Total Risk Weighted Assets', 'totalRisk',  -999, -999, 0, 0,0,0, 6, 'balance', 0); ");
                    Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), eveId.ToString()));
                }
                else
                {
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Net Worth Ratio', 'netWorthRatio', -999, -999, 1, 1,1,1, 0, 'perc', 0); ");

                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Net Worth', 'netWorth',  -999, -999, 0, 0,0,0, 1, 'balance', 0); ");
                    sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_CapitalPolicy] ([PolicyId],[Name],[ShortName],[PolicyLimit],[WellCap],[RiskAssessment],[WellCapAvailable],[RiskAssesmentAvailable],[PolicyLimitAvailable],[Priority],[CurrentRatioFormat],[CurrentRatio]) VALUES ({0},'Total Assets for Net Worth Ratio', 'totAssetsNetWorthRatio',  -999, -999, 0, 0,0,0, 2, 'balance', 0); ");
                    Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), eveId.ToString()));
                }

                //Other Ratios
                sqlQuery.Clear();
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 0,0, 'Net Loans / Assets', 'netLoanAssets', 'perc');");
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 1,0, 'Net Loans / Deposits', 'netLoanDeposits', 'perc');");
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 2,0, 'Loan Loss / Loans', 'loanLoss', 'perc');");

                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 3,0, 'Balance Sheet Spread', 'balSheetSpead', 'perc');");
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 4,0, 'Total Assets', 'totAssets', 'balance');");
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 5,0, 'Total Investments', 'totInvests', 'balance');");


                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 6,0, 'Gross Loans', 'grossLoans', 'balance');");
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 7,0, 'Deposits', 'deposits', 'balance');");
                sqlQuery.AppendLine("INSERT INTO [dbo].[ATLAS_OtherPolicy] ([PolicyId] ,[PolicyLimit],[RiskAssessment] ,[Priority],[CurrentRatio] ,[Name] ,[ShortName], [CurrentRatioFormat]) VALUES ({0}, -999, 1, 8,0, 'Borrowings', 'borrowings', 'balance');");


                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), eveId.ToString()));
            }


            SetLiquidityAndOtherRatios(eveId);
            SetNIIPolicies(eveId);
            SetCoreFundPolicies(eveId);
            SetEvePolicies(eveId);
            List<ScenarioType> sc = _contextProvider.Context.ScenarioTypes.ToList();
            List<Policy> l = _contextProvider.Context.Policies
                .Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings))
                 .Include(s => s.NIIPolicyParents.Select(z => z.NIIPolicies.Select(y => y.ScenarioType)))
                 .Include(s => s.CoreFundParents.Select(z => z.CoreFundPolicies.Select(y => y.ScenarioType)))
                 .Include(s => s.EvePolicyParents.Select(z => z.EvePolicies.Select(y => y.ScenarioType)))
                 .Include(s => s.CapitalPolicies)
                 .Include(s => s.OtherPolicies)
                .Where(s => s.Id == eveId).ToList();

            //BIG OLD HACK to order policy scenarios by ScenarioType table priority
            foreach(Policy p in l)
            {
                foreach(NIIPolicyParent niiP in p.NIIPolicyParents)
                {
                    foreach(NIIPolicy niiPol in niiP.NIIPolicies)
                    {
                        ScenarioType scT = sc.Where(s => s.Id == niiPol.ScenarioTypeId).FirstOrDefault();
                        niiPol.Priority = scT.Priority;
                    }
                    niiP.NIIPolicies = niiP.NIIPolicies.OrderBy(n => n.Priority).ToList();
                }
            }

            return l.AsQueryable();
            //return _contextProvider.Context.Policies
            //    .Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings))
            //     .Include(s => s.NIIPolicyParents.Select(z => z.NIIPolicies.Select(y => y.ScenarioType)))
            //     .Include(s => s.CoreFundParents.Select(z => z.CoreFundPolicies.Select(y => y.ScenarioType)))
            //     .Include(s => s.EvePolicyParents.Select(z => z.EvePolicies.Select(y => y.ScenarioType)))
            //     .Include(s => s.CapitalPolicies)
            //     .Include(s => s.OtherPolicies)
            //     .Include(s => s.ReportedEveScenario)
            //    .Where(s => s.Id == eveId);
        }


        [HttpGet]
        public void LogEvent(string ev, string page)
        {
            try
            {
                DataTable profile = Utilities.GetAtlasUserData();
                string application = "AtlasOne";
                string userName = profile.Rows[0]["UserName"].ToString();
                string timeStamp = System.DateTime.Now.ToString();
                string instName = profile.Rows[0]["atlasBankName"].ToString();
                string qry = String.Format("INSERT INTO [Tool_Analytics].[dbo].[WebAnalytics] ([UserId] ,[Application],[Page],[Event] ,[Timestamp],[Institution])  VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", userName, application, page, ev, timeStamp, instName);
                Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString(), qry);
            }
            catch (Exception ex)
            {
                //Send email to me so I can read the error message because the event log just says unhandeld exception
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress("ybprocessor@darlingconsulting.com");
                mail.To.Add("knajem@darlingconsulting.com");


                mail.Subject = "Error ";

                mail.IsBodyHtml = true;

                string body = ex.ToString();
                mail.Body = body;

                SmtpClient smtp = new SmtpClient();

                smtp.UseDefaultCredentials = false;

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Host = "10.0.0.15";
                smtp.Port = 25;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("ybprocessor", "DCG_yb101", "darlingconsulting.com");
                smtp.Credentials = new NetworkCredential("ybprocessor", "DCG_yb101");

                smtp.Send(mail);
            }

        }

        [HttpGet]
        public void LogDataAudit(string description, string json )
        {
            try
            {
                DataTable profile = Utilities.GetAtlasUserData();
                string userName = profile.Rows[0]["UserName"].ToString();
                string timeStamp = System.DateTime.Now.ToString();
                string instName = profile.Rows[0]["atlasBankName"].ToString();
                string qry = String.Format("INSERT INTO [Tool_Analytics].[dbo].[AtlasOneDataAudit] ([UserId] ,[Description],[JSONDump],[Timestamp] ,[Institution])  VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')", userName, description, json, timeStamp, instName);
                Utilities.ExecuteSql(ConfigurationManager.ConnectionStrings["DDWConnection"].ToString(), qry);
            }
            catch (Exception ex)
            {
                //Send email to me so I can read the error message because the event log just says unhandeld exception
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress("ybprocessor@darlingconsulting.com");
                mail.To.Add("knajem@darlingconsulting.com");


                mail.Subject = "Error ";

                mail.IsBodyHtml = true;

                string body = ex.ToString();
                mail.Body = body;

                SmtpClient smtp = new SmtpClient();

                smtp.UseDefaultCredentials = false;

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Host = "10.0.0.15";
                smtp.Port = 25;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("ybprocessor", "DCG_yb101", "darlingconsulting.com");
                smtp.Credentials = new NetworkCredential("ybprocessor", "DCG_yb101");

                smtp.Send(mail);
            }

        }

        //Gets The Past Three historic Eves
        [HttpGet]
        public IQueryable<Policy> HistoricPolicies(int eveId)
        {

            var eveConfig = _contextProvider.Context.Policies.Where(s => s.Id == eveId).Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings)).FirstOrDefault();

            return _contextProvider.Context.Policies
               .Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings))
               .Include(s => s.NIIPolicyParents.Select(z => z.NIIPolicies.Select(y => y.ScenarioType)))
               .Include(s => s.CoreFundParents.Select(z => z.CoreFundPolicies.Select(y => y.ScenarioType)))
               .Include(s => s.EvePolicyParents.Select(z => z.EvePolicies.Select(y => y.ScenarioType)))
               .Include(s => s.CapitalPolicies)
               .Include(s => s.OtherPolicies)
              .Where(s => s.AsOfDate < eveConfig.AsOfDate).OrderByDescending(s => s.AsOfDate).Take(3).AsNoTracking();

        }

        //Sets Liquidity Ratios
        private void SetLiquidityAndOtherRatios(int eveId)
        {



            var eveConfig = _contextProvider.Context.Policies.Where(s => s.Id == eveId).Include(s => s.LiquidityPolicies.Select(z => z.LiquidityPolicySettings)).Include(s => s.OtherPolicies).FirstOrDefault();

            Dictionary<string, double> vals = PolicyCalculations.LiquidyAndOtherRatioValues(eveId, eveConfig.NIISimulationTypeId, eveConfig.AsOfDate, eveConfig.BasicSurplusAdminId, "", false, false);


            //Now that I have All My totals Calculate Ratios
            foreach (LiquidityPolicy lp in eveConfig.LiquidityPolicies)
            {
                if (vals.ContainsKey(lp.Name))
                {

                    if (!double.IsInfinity(vals[lp.Name]) && !double.IsNaN(vals[lp.Name]))
                    {
                        lp.CurrentRatio = vals[lp.Name];
                    }
                }
            }

            foreach (OtherPolicy op in eveConfig.OtherPolicies)
            {

                if (!double.IsInfinity(vals[op.Name]) && !double.IsNaN(vals[op.Name]))
                {
                    op.CurrentRatio = vals[op.Name];
                }
            }


            _contextProvider.Context.SaveChanges();
        }





        private void SetNIIPolicies(int eveId)
        {
            StringBuilder sqlQuery = new StringBuilder();
            var eveConfig = _contextProvider.Context.Policies.Where(s => s.Id == eveId).Include(s => s.NIIPolicyParents.Select(z => z.NIIPolicies.Select(y => y.ScenarioType))).Include(s => s.NIISimulationType).FirstOrDefault();

            Dictionary<string, Dictionary<string, double>> niis = PolicyCalculations.NIIPoliciesValues(eveConfig.NIISimulationType, eveConfig.AsOfDate, "", false, false);

            if (niis != null)
            {
                foreach (NIIPolicyParent niip in eveConfig.NIIPolicyParents)
                {
                    if (niis.ContainsKey(niip.ShortName))
                    {
                        niip.CurrentRatio = niis[niip.ShortName]["Min"];
                        foreach (NIIPolicy ni in niip.NIIPolicies)
                        {
                            if (niis[niip.ShortName].ContainsKey(ni.ScenarioType.Name))
                            {
                                ni.CurrentRatio = niis[niip.ShortName][ni.ScenarioType.Name];
                            }
                            else
                            {
                                ni.CurrentRatio = 0;
                            }
                        }
                    }

                }

            }


            _contextProvider.Context.SaveChanges();
        }

        private void SetCoreFundPolicies(int eveId)
        {
            string conStr = Utilities.BuildInstConnectionString("");


            var eveConfig = _contextProvider.Context.Policies.Where(s => s.Id == eveId).Include(s => s.CoreFundParents.Select(z => z.CoreFundPolicies.Select(y => y.ScenarioType))).Include(s => s.NIISimulationType).FirstOrDefault();

            Dictionary<string, double> cfs = PolicyCalculations.CoreFundPoliciesValues(eveConfig.NIISimulationType, eveConfig.AsOfDate, "");

            if (cfs != null)
            {
                foreach (CoreFundParent cf in eveConfig.CoreFundParents)
                {
                    cf.CurrentRatio = cfs["MaxCoreFund"];
                    foreach (CoreFundPolicy cfp in cf.CoreFundPolicies)
                    {
                        if (cfs.ContainsKey(cfp.ScenarioType.Name))
                        {
                            cfp.CurrentRatio = cfs[cfp.ScenarioType.Name];
                        }
                        else
                        {
                            cfp.CurrentRatio = 0;
                        }
                    }

                }
            }


            _contextProvider.Context.SaveChanges();
        }





        private void SetEvePolicies(int eveId)
        {
            string conStr = Utilities.BuildInstConnectionString("");

            StringBuilder sqlQuery = new StringBuilder();
            var eveConfig = _contextProvider.Context.Policies.Where(s => s.Id == eveId).Include(s => s.EvePolicyParents.Select(z => z.EvePolicies.Select(y => y.ScenarioType))).Include(s => s.EveSimulationType).FirstOrDefault();

            var instService = new InstitutionService("");
            var sim = instService.GetSimulation(eveConfig.AsOfDate, 0, eveConfig.EveSimulationType);

            Dictionary<string, double> vals = PolicyCalculations.EvePoliciesValues(eveConfig.EveSimulationType, eveConfig.AsOfDate, "", false, 0);

            if (vals != null)
            {
                foreach (EvePolicyParent epp in eveConfig.EvePolicyParents)
                {

                    if (epp.ShortName == "preShock")
                    {
                        if (vals.ContainsKey("preShock0 Shock"))
                        {
                            epp.CurrentRatio = vals["preShock0 Shock"];
                        }
                    }

                    foreach (EvePolicy ep in epp.EvePolicies)
                    {
                        if (vals.ContainsKey(epp.ShortName + ep.ScenarioType.Name))
                        {
                            ep.CurrentRatio = vals[epp.ShortName + ep.ScenarioType.Name]; //eves[ep.ScenarioType.Name][0] / eves[ep.ScenarioType.Name][1];
                        }

                    }

                }
            }


            _contextProvider.Context.SaveChanges();

        }

        [HttpGet]
        public bool ModelSetupContextCheck(string inst)
        {
            var instContext = new GlobalContextProvider(inst);
            
            try
            {
                bool isGood = instContext.Context.Database.CompatibleWithModel(true);
                return isGood;
            }
            catch(Exception e)
            {
                return false;
            }
        }


        [HttpGet]
        public IQueryable<ModelSetup> ModelSetup()
        {
            string conStr = Utilities.BuildInstConnectionString("");
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            bool newModel = (int)Utilities.ExecuteSql(conStr, "SELECT COUNT(*) FROM ATLAS_ModelSetup WHERE asOfDate = '" + date.ToShortDateString() + "'").Rows[0][0] == 0;

            var sqlQuery = new StringBuilder();
            //[IncludeOffBalanceSheet]
            if (newModel)
            {
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_ModelSetup] ");
                sqlQuery.AppendLine("            ([AsOfDate] ");
                sqlQuery.AppendLine("            ,[PrepaymentType] ");
                sqlQuery.AppendLine("            ,[OtherLoanPrepaymentType] ");
                sqlQuery.AppendLine("            ,[LoanRatesDate] ");
                sqlQuery.AppendLine("            ,[InvestmentType] ");
                sqlQuery.AppendLine("            ,[ReportTaxEquivalent] ");
                sqlQuery.AppendLine("            ,[ReportTaxEquivalentYield] ");
                sqlQuery.AppendLine("            ,[IncludeOffBalanceSheet] ");
                sqlQuery.AppendLine("            ,[AvgLifeAssumption] ");
                sqlQuery.AppendLine("            ,[AvgLifeYears] ");
                sqlQuery.AppendLine("            ,[AvgLifeMessage] ");
                sqlQuery.AppendLine("            ,[NetNewLoans] ");
                sqlQuery.AppendLine("            ,[NetNewDeposits]) ");
                sqlQuery.AppendLine("      VALUES ");
                sqlQuery.Append(" ('{0}', 'BK', 'Client Provided',  '{0}', 'Yield Book', 0, 1, 1, '5', '5', 'This assessment assumed a 5 year estimated life on the core deposit base', 0, 0);");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString()));
            }

            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT Id FROM [dbo].[ATLAS_ModelSetup] WHERE asOfDate = '{0}'");
            sqlQuery.AppendLine("");
            int msId = (int)Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString())).Rows[0][0];


            return _contextProvider.Context.ModelSetups
                .Where(s => s.Id == msId);
        }

        [HttpGet]
        public DataTable ModelSetupData(string dbName)
        {
            List<string> errors = new List<string>();
            string conStr = Utilities.BuildInstConnectionString(dbName);
            DataTable asofdates = AsOfDates(dbName, false, 8, false);
            DataTable ret = new DataTable(); ;
            DataTable temp = new DataTable();
            DateTime date;//= DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());

            foreach (DataRow dr in asofdates.Rows)
            {
                date = DateTime.Parse(dr["asOfDateDescript"].ToString());
                temp = Utilities.ExecuteSql(conStr, "SELECT * FROM Atlas_ModelSetup WHERE AsOfDate = '" + date.ToShortDateString() + "'");
                if (temp.Rows.Count == 0)
                {
                    ret.Rows.Add(ret.NewRow());
                    throw new Exception("Cannot find Model Setup for " + dbName + " asOfDate: " + date.ToShortDateString());
                }
                else
                {

                    ret.ImportRow(temp.Rows[0]);
                }
            }
            return ret;
        }

        //Gets The Previous Model Setup
        [HttpGet]
        public IQueryable<ModelSetup> HistoricModelSetup(int id)
        {
            var ms = _contextProvider.Context.ModelSetups.Where(s => s.Id == id).FirstOrDefault();
            DataTable histAodDT = AsOfDates("", true, 1, true);
            if (histAodDT.Rows.Count == 0)
            {
                return null;
            }


            DateTime histAOD = DateTime.Parse(histAodDT.Rows[0]["asOfDateDescript"].ToString());

            return _contextProvider.Context.ModelSetups
              .Where(s => s.AsOfDate.Year == histAOD.Year && s.AsOfDate.Month == histAOD.Month && s.AsOfDate.Day == histAOD.Day).Take(1);
        }

        //Liability Pricing Set Up
        [HttpGet]
        public IQueryable<LiabilityPricing> LiabilityPricings()
        {
            string conStr = Utilities.BuildInstConnectionString("");
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            bool newLiab = (int)Utilities.ExecuteSql(conStr, "SELECT COUNT(*) FROM ATLAS_LiabilityPricing WHERE asOfDate = '" + date.ToShortDateString() + "'").Rows[0][0] == 0;

            var sqlQuery = new StringBuilder();
            if (newLiab)
            {
                //Load up the pricing from 


                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_LiabilityPricing] ");
                sqlQuery.AppendLine("            ([AsOfDate] ");
                sqlQuery.AppendLine("            ,[WebRateAsOfDate] ");
                sqlQuery.AppendLine("            ,[FHLBDistrict] ");
                sqlQuery.AppendLine("            ,[DepositRateAsOfDate] ");
                sqlQuery.AppendLine("            ,[HistoricalWebRateDate] ");
                sqlQuery.AppendLine("            ,[HistoricalRateCurve] ");
                sqlQuery.AppendLine("            ,[HistoricalRateEffDate]) ");
                sqlQuery.AppendLine("      VALUES ");
                sqlQuery.Append(" ('{0}', '{0}', 'None',  '{0}', '{0}', 'Swap', '{0}');");
                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString()));
            }

            sqlQuery.Clear();
            sqlQuery.AppendLine("SELECT Id FROM [dbo].[ATLAS_LiabilityPricing] WHERE asOfDate = '{0}'");
            sqlQuery.AppendLine("");
            int id = (int)Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), date.ToShortDateString())).Rows[0][0];

            if (newLiab)
            {
                sqlQuery.Clear();
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '3 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '6 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '9 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '12 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '24 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '36 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '48 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_TimeDepositRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '60 Month', 0); ");

                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '3 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '6 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '9 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '12 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '24 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '36 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '48 Month', 0); ");
                sqlQuery.AppendLine(" INSERT INTO [dbo].[ATLAS_HistoricalTimeDepositlRate] ([LiabilityPricingId] ,[Term],[Rate]) VALUES ({0}, '60 Month', 0); ");

                Utilities.ExecuteSql(conStr, String.Format(sqlQuery.ToString(), id.ToString()));
            }

            try
            {
                return _contextProvider.Context.LiabilityPricings
                .Include(s => s.NonMaturityDepositRates)
                .Include(s => s.TimeDepositRates)
                .Include(s => s.TimeDepositSpecialRates)
                .Include(s => s.HistoricalTimeDepositRates)
                .Include(s => s.HistoricalTimeDepositSpecialRates)
                .Where(s => s.Id == id);
            }
            catch (Exception e)
            {
                return null;
            }

        }


        //Liability Pricing Set Up
        [HttpGet]
        public object InstLiabilityPricings(int offset, int compOffset, string databaseName)
        {
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            string conStr = Utilities.BuildInstConnectionString(databaseName);
            InstitutionService instServ = new InstitutionService(databaseName);

            string date1 = instServ.GetInformation(date, offset).AsOfDate.ToShortDateString();
            string date2 = instServ.GetInformation(date, compOffset).AsOfDate.ToShortDateString();

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	FHLBDistrict ");
            sqlQuery.AppendLine(" 	,WebRateAsOfDate ");
            sqlQuery.AppendLine(" 	,depositRateAsOfDate ");
            sqlQuery.AppendLine(" 	,HistoricalWebRateDate  ");
            sqlQuery.AppendLine(" 	,HistoricalRateEffDate  ");
            sqlQuery.AppendLine("   ,HistoricalRateCurve ");
            sqlQuery.AppendLine("  From ATLAS_LiabilityPricing WHERE asOfDate = '" + date1 + "' ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine("  UNION ALL ");
            sqlQuery.AppendLine("  ");
            sqlQuery.AppendLine("  SELECT  ");
            sqlQuery.AppendLine(" 	FHLBDistrict ");
            sqlQuery.AppendLine(" 	,WebRateAsOfDate ");
            sqlQuery.AppendLine(" 	,depositRateAsOfDate ");
            sqlQuery.AppendLine(" 	,HistoricalWebRateDate  ");
            sqlQuery.AppendLine(" 	,HistoricalRateEffDate  ");
            sqlQuery.AppendLine("   ,HistoricalRateCurve ");
            sqlQuery.AppendLine("  From ATLAS_LiabilityPricing WHERE asOfDate = '" + date2 + "' ");

            return Utilities.ExecuteSql(conStr, sqlQuery.ToString());
        }


        //Create a New Package Based off of a template
        [HttpGet]
        public string CopyPackageFromTemplate(int packageId, int templatePackageId)
        {
            PackageService ps = new PackageService();
            ps.CopyPackageFromTemplate(packageId, templatePackageId);
            return "YES";
        }

        //[HttpGet]
        //public IQueryable<Package> DeletePackage(int packageId)
        //{
        //    //var ctx = InstContext();
        //    var packageService = new PackageService();
        //    packageService.Delete(packageId);
        //    return Packages();
        //}

        //[HttpGet]
        //public string DeleteReport(int reportId)
        //{
        //    //var ctx = InstContext();
        //    var packageService = new PackageService();
        //    packageService.DeleteReport(true, reportId.ToString(), Utilities.BuildInstConnectionString(""));
        //    return "SUCCESS";
        //}

        [HttpGet]
        public int CopyPackage(int packageId, string asOfDate)
        {
            var packageService = new PackageService();
            int newId = packageService.CopyPackage(packageId, asOfDate);

            string userName = Utilities.GetAtlasUserData().Rows[0]["UserName"].ToString();//HttpContext.Current.Profile.UserName;
            Utilities.SetUserData("asOfDate", asOfDate);

            //  HttpContext.Current.Profile.SetPropertyValue("AtlasAsOfDate", asOfDate);
            return newId;
        }

        [HttpGet]
        public IQueryable<Package> Packages()
        {
            DateTime dt = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());

            var packages = _contextProvider.Context.Packages.Where(p => p.AsOfDate.Day == dt.Day && p.AsOfDate.Month == dt.Month && p.AsOfDate.Year == dt.Year);//.Include(s => s.Reports);
            //  .Where(i => userInstitutionDbNames.Contains(i.DatabaseName))
            //  .SelectMany(i => i.ConsolidationInstitutions
            //  .SelectMany(c => c.Consolidation.Packages))
            //  .Include(p => p.Consolidation).Distinct();
            return packages;//.Include(p => p.Reports).AsQueryable();
        }

        [HttpGet]
        public IQueryable<Package> PackageById(int packageId)
        {
            DateTime dt = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            var packages = _contextProvider.Context.Packages.Where(p => p.Id == packageId).Include(s => s.Reports);
            //  .Where(i => userInstitutionDbNames.Contains(i.DatabaseName))
            //  .SelectMany(i => i.ConsolidationInstitutions
            //  .SelectMany(c => c.Consolidation.Packages))
            //  .Include(p => p.Consolidation).Distinct();
            return packages;//.Include(p => p.Reports).AsQueryable();

        }

        //[HttpGet]
        //public DataTable PackageReports(int packageId)
        //{
        //    string conStr = Utilities.BuildInstConnectionString("");
        //    DataTable tmpDt = Utilities.ExecuteSql(conStr, "SELECT id From ATLAS_Report WHERE packageId = " + packageId + " ORDER BY Priority");
        //    if (tmpDt.Rows.Count > 0)
        //    {
        //        return (int)tmpDt.Rows[0][0];
        //    }
        //    else
        //    {
        //        return -99;
        //    }
        //}

        //public static class ContextExtensions
        //{
        //    public static string GetTableName<T>(this DbContext context) where T : class
        //    {
        //        ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;

        //        return objectContext.GetTableName<T>();
        //    }

        //    public static string GetTableName<T>(this ObjectContext context) where T : class
        //    {
        //        string sql = context.CreateObjectSet<T>().ToTraceString();
        //        Regex regex = new Regex("FROM (?<table>.*) AS");
        //        Match match = regex.Match(sql);

        //        string table = match.Groups["table"].Value;
        //        return table;
        //    }
        //}


        [HttpGet]
        public string DeleteReportingItem(string sourceName, string sourceId, string srcDB, string parentName)
        {
            PackageService ps = new PackageService();
            return ps.DeleteReportingItem(sourceName, sourceId, srcDB, parentName);
        }


        [HttpGet]
        public IQueryable<Report> Reports(int id)
        {
            var reports = _contextProvider.Context.Reports.Where(s => s.Id == id)
                .Include(r => r.Footnotes)
                .Include(r => r.SplitFootnotes)
                .Include(p => p.Package);

            return reports;
        }

        [HttpGet]
        public object ReportByPackagePriority(int packageID, int currentPriority, PriorityMode mode)
        {
            StringBuilder query = new StringBuilder("SELECT TOP 1 id FROM ATLAS_Report WHERE packageId = ");
            query.Append(packageID);

            switch (mode)
            {
                case PriorityMode.Equal:
                    query.Append("AND priority = " + currentPriority);
                    break;

                case PriorityMode.First:
                    query.Append("ORDER BY priority");
                    break;

                case PriorityMode.Last:
                    query.Append("ORDER BY priority DESC");
                    break;

                case PriorityMode.Previous:
                    query.Append("AND priority < " + currentPriority + "ORDER BY priority DESC");
                    break;

                case PriorityMode.Next:
                    query.Append("AND priority > " + currentPriority + "ORDER BY priority");
                    break;

                default:
                    throw new Exception("Invalid PriorityMode " + mode);
            }

            DataTable retTable = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), query.ToString());

            if (retTable.Rows.Count > 0)
            {
                return new { repId = int.Parse(retTable.Rows[0][0].ToString()) };
            }

            return new { repId = -99 };
        }

        [HttpGet]
        public string DefaultReport(int reportId)
        {
            PackageService ps = new PackageService();
            ps.ResetReportToDefaults(reportId.ToString());

            return "";
        }


        [HttpGet]
        public IQueryable<Footnote> Footnotes(int id)
        {
            var footnotes = _contextProvider.Context.Footnotes.Include(f => f.Report).Where(f => f.ReportId == id);
            return footnotes;
        }
        [HttpGet]
        public IQueryable<SplitFootnote> SplitFootnotes()
        {
            var footnotes = _contextProvider.Context.SplitFootnotes.Include(f => f.Report);
            return footnotes;
        }

        [HttpGet]
        public IQueryable<OneChart> OneCharts()
        {
            var oneCharts = _contextProvider.Context.OneCharts
                                            .Include(oc => oc.Chart.ChartScenarioTypes.Select(s => s.ScenarioType))
                                            .Include(oc => oc.Chart.ChartSimulationTypes);
            foreach (OneChart ch in oneCharts)
            {
                ch.Chart.ChartSimulationTypes = ch.Chart.ChartSimulationTypes.OrderBy(c => c.Priority).ToList();
            }

            return oneCharts.AsQueryable();
        }

        [HttpGet]
        public object OneChartViewDataById(int oneChartId)
        {
            var incomeService = new SimCompareService();
            DataTable asOfDates = AsOfDates(Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString(), true, 10);
            return incomeService.OneChartViewModel(oneChartId, asOfDates);
        }


        [HttpGet]
        public IQueryable<TwoChart> TwoCharts()
        {
            var twoCharts = _contextProvider.Context.TwoCharts
                                            .Include(tc => tc.LeftChart.ChartScenarioTypes.Select(scst => scst.ScenarioType))
                                            .Include(tc => tc.RightChart.ChartScenarioTypes.Select(scst => scst.ScenarioType))
                                            .Include(tc => tc.RightChart.ChartSimulationTypes)
                                            .Include(tc => tc.LeftChart.ChartSimulationTypes);

            foreach (TwoChart tc in twoCharts)
            {
                tc.LeftChart.ChartSimulationTypes = tc.LeftChart.ChartSimulationTypes.OrderBy(l => l.Priority).ToList();
                tc.RightChart.ChartSimulationTypes = tc.RightChart.ChartSimulationTypes.OrderBy(r => r.Priority).ToList();
            }
            return twoCharts.AsQueryable();
        }

        [HttpGet]
        public object TwoChartViewDataById(int twoChartId)
        {
            var incomeService = new SimCompareService();
            return incomeService.TwoChartViewModel(twoChartId, AsOfDates(Utilities.GetAtlasUserData().Rows[0]["databaseNAme"].ToString(), true, 10));
        }

        [HttpGet]
        public IQueryable<SimCompare> SimCompares()
        {
            var simCompares = _contextProvider.Context.SimCompares
                                              .Include(s => s.SimCompareScenarioTypes.Select(scst => scst.ScenarioType))
                                              .Include(s => s.SimCompareSimulationTypes);
            foreach (SimCompare s in simCompares)
            {
                s.SimCompareSimulationTypes = s.SimCompareSimulationTypes.OrderBy(si => si.Priority).ToList();
            }

            return simCompares;
        }

        [HttpGet]
        public object SimCompareViewDataById(int simCompareId)
        {
            var incomeService = new IncomeService();
            DataTable asOfDates = AsOfDates(Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString(), true, 10);
            return incomeService.SimCompareViewModel(simCompareId, asOfDates);
        }

        //Summary Of Results
        [HttpGet]
        public IQueryable<SummaryOfResults> SummaryOfResults(int id)
        {
            var sumResults = _contextProvider.Context.SummaryOfResults.Where(s => s.Id == id);
            //.Include(s => s.SimCompareScenarioTypes.Select(scst => scst.ScenarioType))
            //.Include(s => s.SimCompareSimulationTypes.Select(scst => scst.SimulationType));
            return sumResults;
        }

        [HttpGet]
        public IQueryable<MarginalCostOfFunds> MarginalCostOfFunds(int id)
        {
            var mcf = _contextProvider.Context.MarginalCostOfFunds.Where(s => s.Id == id);
            //.Include(s => s.SimCompareScenarioTypes.Select(scst => scst.ScenarioType))
            //.Include(s => s.SimCompareSimulationTypes.Select(scst => scst.SimulationType));
            return mcf;
        }
        [HttpGet]
        public object MarginalCostOfFundsViewDataById(int id)
        {
            var mcfService = new MarginalCostOfFundsService();
            return mcfService.MarginalCostOfFundsViewModel(id);
        }

        [HttpGet]
        public object SummaryOfResultsViewDataById(int id)
        {
            var sumResultsService = new SummaryOfResultsService();
            return sumResultsService.SummaryOfResultsViewModel(id);
        }

        //Lookback stuff
        [HttpGet]
        public IQueryable<LBCustomLayout> LBCustomLayouts()
        {
            string conStr = Utilities.BuildInstConnectionString("");
            bool needsDefaultLayouts = (int)Utilities.ExecuteSql(conStr, "SELECT COUNT(*) FROM ATLAS_LBCustomLayout WHERE UserAdded = 0").Rows[0][0] == 0;
            bool needsDefaultTypes = (int)Utilities.ExecuteSql(conStr, "SELECT COUNT(*) FROM ATLAS_LBCustomType WHERE UserAdded = 0").Rows[0][0] == 0;
            int newId = 0;
            StringBuilder sql = new StringBuilder();

            if (needsDefaultTypes)
            {
                //add the types, no dependencies or children
                sql.AppendLine("insert into ATLAS_LBCustomType ([StartDateOffset],[EndDateOffset],[Name],[UserAdded]) values (1,0,'LB1',0)");
                sql.AppendLine("insert into ATLAS_LBCustomType ([StartDateOffset],[EndDateOffset],[Name],[UserAdded]) values (2,0,'LB2',0)");
                sql.AppendLine("insert into ATLAS_LBCustomType ([StartDateOffset],[EndDateOffset],[Name],[UserAdded]) values (4,0,'LB4',0)");
                Utilities.ExecuteSql(conStr, sql.ToString());
                sql.Clear();
            }

            if (needsDefaultLayouts)
            {
                sql.Clear();
                #region defaults to loop through
                string[] layouts = new string[] { "Summary", "Expanded" };
                string[] summaryRowItems = new string[]{
                    " {0},0,1,'Master', 'Investments'",
                    "{0},1,1,'Master', 'Loans'",
                    "{0},2,1,'Master', 'Other Assets'",
                    "{0},0,0,'Section Header', 'Deposits'",
                    "{0},1,0,'Sub', 'Non-Maturity Deposits'",
                    "{0},2,0,'Sub', 'Time Deposits'",
                    "{0},3,0,'Master', 'Borrowings'",
                    "{0},4,0,'Master', 'All Other Liabilities'"};

                #region rowitems definition
                string[] expandedRowItems = new string[]{
                    "{0},0,1,'Section Header','Investments'",
                    "{0},1,1,'Sub','Overnight'",
                    "{0},2,1,'Sub','Other Liquid Assets'",
                    "{0},2,1,'Sub','Treasury'",
                    "{0},4,1,'Sub','Agency'",
                    "{0},5,1,'Sub','MBS'",
                    "{0},6,1,'Sub','CMO'",
                    "{0},7,1,'Sub','Muni'",
                    "{0},8,1,'Sub','Corporate'",
                    "{0},9,1,'Sub','Stock'",
                    "{0},10,1,'Sub','Trust Preferred'",
                    "{0},11,1,'Sub','Other Investments'",
                    "{0},12,1,'Sub','Unrealized G/L'",
                    "{0},13,1,'Section Header','Loans'",
                    "{0},14,1,'Sub','Residential RE'",
                    "{0},15,1,'Sub','Residential RE Construction'",
                    "{0},16,1,'Sub','C & I'",
                    "{0},17,1,'Sub','Commercial RE'",
                    "{0},18,1,'Sub','Commercial RE Construction'",
                    "{0},19,1,'Sub','Commercial Leasing'",
                    "{0},20,1,'Sub','Land'",
                    "{0},21,1,'Sub','Agriculture'",
                    "{0},22,1,'Sub','Agriculture RE'",
                    "{0},23,1,'Sub','Home Equity'",
                    "{0},24,1,'Sub','Installments'",
                    "{0},25,1,'Sub','Auto'",
                    "{0},26,1,'Sub','Gov & Agy Guaranteed'",
                    "{0},27,1,'Sub','Municipals'",
                    "{0},28,1,'Sub','Other Loans'",
                    "{0},29,1,'Sub','Adjustments'",
                    "{0},30,1,'Sub','Loan Loss Reserve'",
                    "{0},31,1,'Section Header','Other Assets'",
                    "{0},32,1,'Sub','Fixed Assets'",
                    "{0},33,1,'Sub','OREO'",
                    "{0},34,1,'Sub','BOLI'",
                    "{0},35,1,'Sub','Intangible'",
                    "{0},36,1,'Sub','Other Assets'",
                    "{0},37,1,'Sub','Swap'",
                    "{0},38,1,'Sub','Cap'",
                    "{0},39,1,'Sub','Floor'",
                    "{0},40,1,'Sub','Other Off Bal Sheet'",
                    "{0},0,0,'Section Header','Non-Maturity Deposits'",
                    "{0},1,0,'Sub','DDA'",
                    "{0},2,0,'Sub','Public DDA'",
                    "{0},3,0,'Sub','Online DDA'",
                    "{0},4,0,'Sub','NOW'",
                    "{0},5,0,'Sub','Public NOW'",
                    "{0},6,0,'Sub','Online NOW'",
                    "{0},7,0,'Sub','Brokered NOW'",
                    "{0},8,0,'Sub','Savings'",
                    "{0},9,0,'Sub','Public Savings'",
                    "{0},10,0,'Sub','Online Savings'",
                    "{0},11,0,'Sub','MMDA'",
                    "{0},12,0,'Sub','Public MMDA'",
                    "{0},13,0,'Sub','Online MMDA'",
                    "{0},14,0,'Sub','Brokered Reciprocal MDDA'",
                    "{0},15,0,'Sub','Brokered One-Way MMDA'",
                    "{0},16,0,'Sub','Brokered MMDA'",
                    "{0},17,0,'Sub','Other Non-Maturity Deposit'",
                    "{0},18,0,'Section Header','Time Deposits'",
                    "{0},19,0,'Sub','CD'",
                    "{0},20,0,'Sub','Jumbo CD'",
                    "{0},21,0,'Sub','Public CD'",
                    "{0},22,0,'Sub','Public Jumbo CD'",
                    "{0},23,0,'Sub','Online CD'",
                    "{0},24,0,'Sub','Online Jumbo CD'",
                    "{0},25,0,'Sub','National CD'",
                    "{0},26,0,'Sub','Brokered Reiprocal CD'",
                    "{0},27,0,'Sub','Brokered One-Way CD'",
                    "{0},28,0,'Sub','Brokered CD'",
                    "{0},29,0,'Sub','Other Time Deposits'",
                    "{0},30,0,'Section Header','Borrowings'",
                    "{0},31,0,'Sub','Unsecured Retail Repo'",
                    "{0},32,0,'Sub','Secured Retail Repo'",
                    "{0},33,0,'Sub','Unsecured Wholesale'",
                    "{0},34,0,'Sub','Secured Wholesale'",
                    "{0},35,0,'Sub','FHLB'",
                    "{0},36,0,'Sub','FRB'",
                    "{0},37,0,'Sub','Unsecured Repo Wholesale'",
                    "{0},38,0,'Sub','Secured Repo Wholesale'",
                    "{0},39,0,'Sub','Other Borrowings'",
                    "{0},40,0,'Section Header','Other Liabilities'",
                    "{0},41,0,'Sub','Other Liabilities'",
                    "{0},42,0,'Sub','Swap'",
                    "{0},43,0,'Sub','Cap'",
                    "{0},44,0,'Sub','Floor'",
                    "{0},45,0,'Sub','Other Off Bal Sheet'",
                    "{0},46,0,'Section Header','Equity'",
                    "{0},47,0,'Sub','Equity Tier 1'",
                    "{0},48,0,'Sub','Equity Tier 2'",
                    "{0},49,0,'Sub','Trust Preferred Tier 1'",
                    "{0},50,0,'Sub','Trust Preferred Tier 2'",
                    "{0},51,0,'Sub','Unrealized G/L (L)'",
                    "{0},52,0,'Sub','Other Equity'"
                };
                #endregion

                #region summaryClassificaitons definition

                string[][] summaryClassifications = {
                  new string[]{
                    "1, 1, 0,  'Other Investment', {0}",
                    "1, 1, 1,  'Agency', {0}",
                    "1, 1, 2,  'Corporate', {0}",
                    "1, 1, 3,  'CMO', {0}",
                    "1, 1, 4,  'MBS', {0}",
                    "1, 1, 5,  'Muni', {0}",
                    "1, 1, 6,  'Overnight', {0}",
                    "1, 1, 7,  'Stock', {0}",
                    "1, 1, 8,  'Treasury', {0}",
                    "1, 1, 9,  'Trust Preferred', {0}",
                    "1, 1, 10,  'Unrealized G/L', {0}",
                    "1, 1, 11,  'Other Liquid Asset', {0}",
                  },
                  new string[]{
                    "1, 2, 0,  'Other Loan', {0}",
                    "1, 2, 1,  'Commercial RE', {0}",
                    "1, 2, 2,  'C & I', {0}",
                    "1, 2, 3,  'Installments', {0}",
                    "1, 2, 4,  'Residential', {0}",
                    "1, 2, 5,  'Agriculture', {0}",
                    "1, 2, 6,  'Agriculture RE', {0}",
                    "1, 2, 7,  'Residential - Constr', {0}",
                    "1, 2, 8,  'Commercial RE - Constr', {0}",
                    "1, 2, 9,  'Home Equity', {0}",
                    "1, 2, 10,  'Gov & Agy Guaranteed', {0}",
                    "1, 2, 11,  'Adjustments', {0}",
                    "1, 2, 12,  'Loan Loss Reserve', {0}",
                    "1, 2, 13,  'Commercial Leasing', {0}",
                    "1, 2, 14,  'Land', {0}",
                    "1, 2, 15,  'Auto', {0}",
                    "1, 2, 16,  'Municipals', {0}",
                  },
                  new string[]{
                    "1, 0, 0,  'Other Asset',{0}",
                    "1, 0, 1,  'Intangible', {0}",
                    "1, 0, 2,  'Loan Loss Reserve', {0}",
                    "1, 0, 3,  'Fixed Assets', {0}",
                    "1, 0, 4,  'BOLI', {0}",
                    "1, 0, 5,  'OREO', {0}",
                    "1, 7, 0,  'Other Off Bal Sheet (A)', {0}",
                    "1, 7, 1,  'Swap (A)', {0}",
                    "1, 7, 2,  'Cap (A)', {0}",
                    "1, 7, 3,  'Floor (A)', {0}"
                  },
                  new string[]{
                    "0, 3, 0,  'Other Non-Mat Deposit', {0}",
                    "0, 3, 1,  'DDA', {0}",
                    "0, 3, 2,  'Public DDA', {0}",
                    "0, 3, 3,  'Now', {0}",
                    "0, 3, 4,  'Public Now', {0}",
                    "0, 3, 5,  'Savings', {0}",
                    "0, 3, 6,  'Public Savings', {0}",
                    "0, 3, 7,  'MMDA', {0}",
                    "0, 3, 8,  'Public MMDA', {0}",
                    "0, 3, 9,  'Online DDA', {0}",
                    "0, 3, 10,  'Online Now', {0}",
                    "0, 3, 11,  'Online Savings', {0}",
                    "0, 3, 12,  'Online MMDA', {0}",
                    "0, 3, 13,  'Brokered Now', {0}",
                    "0, 3, 14,  'Brokered MMDA', {0}",
                    "0, 3, 15,  'Brokered Reciprocal MMDA', {0}",
                    "0, 3, 16,  'Brokered One-Way MMDA', {0}"
                  },
                  new string[]{
                    "0, 8, 0,  'Other Time Deposit', {0}",
                    "0, 8, 1,  'CD', {0}",
                    "0, 8, 2,  'Public CD', {0}",
                    "0, 8, 3,  'Jumbo CD', {0}",
                    "0, 8, 4,  'Public Jumbo CD', {0}",
                    "0, 8, 5,  'Brokered CD ', {0}",
                    "0, 8, 6,  'National CD', {0}",
                    "0, 8, 7,  'Online CD', {0}",
                    "0, 8, 8,  'Online Jumbo CD', {0}",
                    "0, 8, 9,  'Brokered Reciprocal CD', {0}",
                    "0, 8, 10,  'Brokered One-Way CD', {0}"
                  },
                  new string[]{
                    "0, 6, 0,  'Other Borrowings', {0}",
                    "0, 6, 1,  'FHLB', {0}",
                    "0, 6, 2,  'Unsecured Retail', {0}",
                    "0, 6, 3,  'Secured Retail', {0}",
                    "0, 6, 4,  'Unsecured Wholesale', {0}",
                    "0, 6, 5,  'Secured Wholesale', {0}",
                    "0, 6, 6,  'Unsecured Repo Wholesale', {0}",
                    "0, 6, 7,  'Secured Repo Wholesale', {0}",
                    "0, 6, 8,  'FRB', {0}"
                  },
                  new string[]{
                    "0, 0, 0,  'Other Liability', {0}",
                    "0, 4, 0,  'Other Equity', {0}",
                    "0, 4, 1,  'Equity Tier 1', {0}",
                    "0, 4, 2,  'Equity Tier 2', {0}",
                    "0, 4, 3,  'Tangible', {0}",
                    "0, 4, 4,  'Trust Preferred Tier 1', {0}",
                    "0, 4, 5,  'Trust Preferred Tier 2', {0}",
                    "0, 4, 6,  'Unrealized G/L (L)', {0}",
                    "0, 9, 0,  'Other Off Bal Sheet (L)', {0}",
                    "0, 9, 1,  'Swap (L)', {0}",
                    "0, 9, 2,  'Cap (L)', {0}",
                    "0, 9, 3,  'Floor (L)', {0}"
                  }
                };
                #endregion

                #region expanded classifications definition
                string[][] expandedClassifications = {
                    new string[]{"1, 1, 6,  'Overnight', {0}"},
                    new string[]{"1, 1, 11,  'Other Liquid Asset', {0}"},
                    new string[]{"1, 1, 8,  'Treasury', {0}"},
                    new string[]{"1, 1, 1,  'Agency', {0}"},
                    new string[]{"1, 1, 4,  'MBS', {0}"},
                    new string[]{"1, 1, 3,  'CMO', {0}"},
                    new string[]{"1, 1, 5,  'Muni', {0}"},
                    new string[]{"1, 1, 2,  'Corporate', {0}"},
                    new string[]{"1, 1, 7,  'Stock', {0}"},
                    new string[]{"1, 1, 9,  'Trust Preferred', {0}"},
                    new string[]{"1, 1, 0,  'Other Investment', {0}"},
                    new string[]{"1, 1, 10,  'Unrealized G/L', {0}"},
                    new string[]{"1, 2, 4,  'Residential', {0}"},
                    new string[]{"1, 2, 7,  'Residential - Constr', {0}"},
                    new string[]{"1, 2, 2,  'C & I', {0}"},
                    new string[]{"1, 2, 1,  'Commercial RE', {0}"},
                    new string[]{"1, 2, 8,  'Commercial RE - Constr', {0}"},
                    new string[]{"1, 2, 13,  'Commercial Leasing', {0}"},
                    new string[]{"1, 2, 14,  'Land', {0}"},
                    new string[]{"1, 2, 5,  'Agriculture', {0}"},
                    new string[]{"1, 2, 6,  'Agriculture RE', {0}"},
                    new string[]{"1, 2, 9,  'Home Equity', {0}"},
                    new string[]{"1, 2, 3,  'Installments', {0}"},
                    new string[]{"1, 2, 15,  'Auto', {0}"},
                    new string[]{"1, 2, 10,  'Gov & Agy Guaranteed', {0}"},
                    new string[]{"1, 2, 16,  'Municipals', {0}"},
                    new string[]{"1, 2, 0,  'Other Loan', {0}"},
                    new string[]{"1, 2, 11,  'Adjustments', {0}"},
                    new string[]{"1, 2, 12,  'Loan Loss Reserve', {0}"},
                    new string[]{"1, 0, 3,  'Fixed Assets', {0}"},
                    new string[]{"1, 0, 5,  'OREO', {0}"},
                    new string[]{"1, 0, 4,  'BOLI', {0}"},
                    new string[]{"1, 0, 1,  'Intangible', {0}"},
                    new string[]{"1, 0, 0,  'Other Asset', {0}"},
                    new string[]{"1, 7, 5,  'Swap (A)', {0}"},
                    new string[]{"1, 7, 2,  'Cap (A)', {0}"},
                    new string[]{"1, 7, 3,  'Floor (A)', {0}"},
                    new string[]{"1, 7, 0,  'Other Off Bal Sheet (A)', {0}"},
                    new string[]{"0, 3, 1,  'DDA', {0}"},
                    new string[]{"0, 3, 2,  'Public DDA', {0}"},
                    new string[]{"0, 3, 9,  'Online DDA', {0}"},
                    new string[]{"0, 3, 3,  'Now', {0}"},
                    new string[]{"0, 3, 4,  'Public Now', {0}"},
                    new string[]{"0, 3, 10,  'Online Now', {0}"},
                    new string[]{"0, 3, 13,  'Brokered Now', {0}"},
                    new string[]{"0, 3, 5,  'Savings', {0}"},
                    new string[]{"0, 3, 6,  'Public Savings', {0}"},
                    new string[]{"0, 3, 11,  'Online Savings', {0}"},
                    new string[]{"0, 3, 7,  'MMDA', {0}"},
                    new string[]{"0, 3, 8,  'Public MMDA', {0}"},
                    new string[]{"0, 3, 12,  'Online MMDA', {0}"},
                    new string[]{"0, 3, 15,  'Brokered Reciprocal MMDA', {0}"},
                    new string[]{"0, 3, 16,  'Brokered One-Way MMDA', {0}"},
                    new string[]{"0, 3, 14,  'Brokered MMDA', {0}"},
                    new string[]{"0, 3, 0,  'Other Non-Mat Deposit', {0}"},
                    new string[]{"0, 8, 1,  'CD', {0}"},
                    new string[]{"0, 8, 3,  'Jumbo CD', {0}"},
                    new string[]{"0, 8, 2,  'Public CD', {0}"},
                    new string[]{"0, 8, 4,  'Public Jumbo CD', {0}"},
                    new string[]{"0, 8, 7,  'Online CD', {0}"},
                    new string[]{"0, 8, 8,  'Online Jumbo CD', {0}"},
                    new string[]{"0, 8, 6,  'National CD', {0}"},
                    new string[]{"0, 8, 9,  'Brokered Reciprocal CD', {0}"},
                    new string[]{"0, 8, 10, 'Brokered One-Way CD', {0}"},
                    new string[]{"0, 8, 5,  'Brokered CD ', {0}"},
                    new string[]{"0, 8, 0,  'Other Time Deposit', {0}"},
                    new string[]{"0, 6, 2,  'Unsecured Retail', {0}"},
                    new string[]{"0, 6, 3,  'Secured Retail', {0}"},
                    new string[]{"0, 6, 4,  'Unsecured Wholesale', {0}"},
                    new string[]{"0, 6, 5,  'Secured Wholesale', {0}"},
                    new string[]{"0, 6, 1,  'FHLB', {0}"},
                    new string[]{"0, 6, 8,  'FRB', {0}"},
                    new string[]{"0, 6, 6,  'Unsecured Repo Wholesale', {0}"},
                    new string[]{"0, 6, 7,  'Secured Repo Wholesale', {0}"},
                    new string[]{"0, 6, 0,  'Other Borrowings', {0}"},
                    new string[]{"0, 0, 0,  'Other Liability', {0}"},
                    new string[]{"0, 9, 1,  'Swap (L)', {0}"},
                    new string[]{"0, 9, 2,  'Cap (L)', {0}"},
                    new string[]{"0, 9, 3,  'Floor (L)', {0}"},
                    new string[]{"0, 9, 0,  'Other Off Bal Sheet (L)', {0}"},
                    new string[]{"0, 4, 1,  'Equity Tier 1', {0}"},
                    new string[]{"0, 4, 2,  'Equity Tier 2', {0}"},
                    new string[]{"0, 4, 4,  'Trust Preferred Tier 1', {0}"},
                    new string[]{"0, 4, 5,  'Trust Preferred Tier 2', {0}"},
                    new string[]{"0, 4, 6,  'Unrealized G/L (L)', {0}"},
                    new string[]{"0, 4, 0,  'Other Equity', {0}"}
                };

                #endregion

                #endregion

                for (int l = 0; l < layouts.Length; l++)
                {
                    int rowItemsLen = l == 0 ? summaryRowItems.Length : expandedRowItems.Length;
                    string[] rowItems = new string[rowItemsLen];
                    Array.Copy((l == 0 ? summaryRowItems : expandedRowItems), rowItems, rowItemsLen);

                    sql.Clear();
                    sql.AppendLine("insert into ATLAS_LBCustomLayout ([Name], [UserAdded], [Priority]) values ('" + layouts[l] + "', 0, " + l + ")");
                    sql.AppendLine("SELECT SCOPE_IDENTITY() as newId;");
                    DataTable dt = Utilities.ExecuteSql(conStr, sql.ToString());
                    int layoutId = int.Parse(dt.Rows[0][0].ToString());
                    int rowAdjustment = 0;
                    for (int r = 0; r < rowItems.Length; r++)
                    {
                        sql.Clear();
                        sql.AppendLine("insert into ATLAS_LBCustomLayoutRowItem ([LBCustomLayoutId],[Priority],[IsAsset],[RowType],[Title]) values (" + String.Format(rowItems[r], layoutId) + ")");
                        sql.AppendLine("SELECT SCOPE_IDENTITY() as newId;");
                        DataTable dtl = Utilities.ExecuteSql(conStr, sql.ToString());
                        int rowitemId = int.Parse(dtl.Rows[0][0].ToString());
                        int adjR = r;
                        string rowType = rowItems[r].Split(',')[3];

                        if (rowType.Equals("'Section Header'"))
                        {
                            rowAdjustment += 1;
                        }
                        else
                        {
                            adjR = r - rowAdjustment;
                            int classificationsLen = l == 0 ? summaryClassifications[adjR].Length : expandedClassifications[adjR].Length;
                            string[] classifications = new string[classificationsLen];
                            Array.Copy((l == 0 ? summaryClassifications[adjR] : expandedClassifications[adjR]), classifications, classificationsLen);

                            for (var c = 0; c < classificationsLen; c++)
                            {
                                sql.Clear();
                                sql.AppendLine("insert into ATLAS_LBCustomLayoutClassifications ([IsAsset], [Acc_Type], [RbcTier], [Name], [LBCustomLayoutRowItemId]) values (" + String.Format(classifications[c], rowitemId) + ")");
                                Utilities.ExecuteSql(conStr, sql.ToString());
                            }
                        }
                    }
                }

                sql.Clear();

            }
            var lbcl = _contextProvider.Context.LBCustomLayouts
                                              .Include(l => l.RowItems.Select(r => r.LBCustomLayoutClassifications)).OrderBy(l => l.Priority);
            return lbcl;
        }
        [HttpGet]
        public IQueryable<LBCustomType> LBCustomTypes()
        {
            string conStr = Utilities.BuildInstConnectionString("");
            bool needsDefaults = (int)Utilities.ExecuteSql(conStr, "SELECT COUNT(*) FROM ATLAS_LBCustomType WHERE UserAdded = 0").Rows[0][0] == 0;
            if (needsDefaults)
            {
                StringBuilder sql = new StringBuilder();
                sql.AppendLine("insert into ATLAS_LBCustomType ([StartDateOffset],[EndDateOffset],[Name], [UserAdded], [Priority]) values (1, 0, 'LB1', 0, 0);");
                sql.AppendLine("insert into ATLAS_LBCustomType ([StartDateOffset],[EndDateOffset],[Name], [UserAdded], [Priority]) values (2, 0, 'LB2', 0, 1);");
                sql.AppendLine("insert into ATLAS_LBCustomType ([StartDateOffset],[EndDateOffset],[Name], [UserAdded], [Priority]) values (4, 0, 'LB4', 0, 2);");
                Utilities.ExecuteSql(conStr, sql.ToString());
            }
            var lbct = _contextProvider.Context.LBCustomTypes.OrderBy(t => t.Priority);
            return lbct;
        }
        [HttpGet]
        public IQueryable<LBLookback> LBLookbacks()
        {
            DateTime aod = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString().ToString());
            var lbct = _contextProvider.Context.LBLookbacks.Where(l => l.AsOfDate.Year == aod.Year && l.AsOfDate.Month == aod.Month && l.AsOfDate.Day == aod.Day).Include(l => l.LookbackGroups).OrderBy(l => l.Priority);//.Select(t => t.LBLookbackData)
            List<LBLookback> list = lbct.ToList();
            return lbct;
        }

        [HttpGet]
        public IQueryable<LBLookback> LBLookbacksById(int id)
        {
            var lbct = _contextProvider.Context.LBLookbacks.Where(s => s.Id == id).Include(l => l.LookbackGroups.Select(t => t.LBLookbackData));//
            return lbct;
        }

        [HttpGet]
        public List<LBLookback> LBLookbacksByOffset(int aodOffset, string instName)
        {
            string conStr = Utilities.BuildInstConnectionString(instName);
            DateTime date = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            InstitutionService instService = new InstitutionService(instName);
            int asOfDateId = (int)Utilities.ExecuteSql(conStr, "SELECT Id FROM asOfDateInfo WHERE asOfDate = '" + date.ToShortDateString() + "' AND basisDate = 1").Rows[0][0];
            DateTime aod = instService.GetInformation(date, aodOffset).AsOfDate;
            List<LBLookback> lookbacks = _contextProvider.Context.LBLookbacks.Where(l => l.AsOfDate.Day == aod.Day && l.AsOfDate.Month == aod.Month && l.AsOfDate.Year == aod.Year).Include(l => l.LookbackGroups).ToList();
            return lookbacks;
            //return Utilities.ExecuteSql(conStr, "SELECT Id, Name FROM ATLAS_LBLookback WHERE datediff(d, AsOfDate, '" + aod.ToShortDateString() + "') = 0");

            //return _contextProvider.Context.LBLookbacks
            //    .Where(s => s.AsOfDate == aod);
        }

        [HttpGet]
        public IQueryable<LBLookbackGroup> LookbackGroups(int lookbackId)
        {
            var lbct = _contextProvider.Context.LBLookbackGroup.Where(a => a.LBLookbackId == lookbackId).Include(l => l.LBLookbackData);
            return lbct;
        }

        [HttpGet]
        public IQueryable<LBLookback> LBLookbacksByLayoutId(int layoutId)
        {
            var lbgs = _contextProvider.Context.LBLookbackGroup.Where(a => a.LayoutId == layoutId).Include(a => a.LBCustomType);
            var lbs = lbgs.Select(a => a.LBLookback);

            foreach (LBLookback lb in lbs)
            {
                var i = lb.Id;
            }

            foreach (LBLookbackGroup group in lbgs)
            {
                int n = 0;

            }
            return lbs;
        }

        [HttpGet]
        public object AsOfDateInformation(string aod, int asOfDateOffset, string instName)
        {
            var instService = new InstitutionService(instName);
            DateTime AOD = new DateTime();
            if (aod == null || aod.Length == 0)
            {
                AOD = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            }
            else
            {
                AOD = DateTime.Parse(aod);
            }
            var ret = instService.GetInformation(AOD, asOfDateOffset);
            return ret;

        }

        [HttpGet]
        public bool DefaultBasicSurplus(string asOfDate)
        {

            //Update all basic surplu
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" UPDATE bsa SET bsa.SimulationTypeId = p.NIISimulationTypeId ");
            sqlQuery.AppendLine(" 	 from ATLAS_BasicSurplusAdmin as bsa ");
            sqlQuery.AppendLine(" 	INNER JOIN asOfDateInfo as aod ");
            sqlQuery.AppendLine(" 	ON aod.id = bsa.InformationId ");
            sqlQuery.AppendLine(" 	INNER JOIN ATLAS_Policy as p  ");
            sqlQuery.AppendLine(" 	ON aod.asOfDate = p.asOfDate ");
            sqlQuery.AppendLine(" 	WHERE bsa.OverrideSimulationTypeId = 0 AND p.asOfDate = '" + asOfDate + "' ");


            Utilities.ExecuteCommand(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            return true;
        }


        [HttpGet]
        public object SimulationScenarioInfo(string aod, int dateOffset, int simulationTypeId, int scenarioTypeId)
        {
            DateTime AOD = new DateTime();
            if (aod == null || aod.Length == 0)
            {
                AOD = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            }
            else
            {
                AOD = DateTime.Parse(aod);
            }
            var instService = new InstitutionService("");

            var ret = instService.GetSimulationScenario(AOD, dateOffset, simulationTypeId, scenarioTypeId);

            return ret;
        }

        [HttpGet]
        public DataTable AvailableSimulations(string inst, int offset)
        //public List<SimulationType> AvailableSimulations(string inst, int offset)
        {
            var instService = new InstitutionService(inst);
            List<int> simTypeIds = new List<int>();
            List<SimulationType> simTypes = new List<SimulationType>();

            if (inst == null)
            {
                inst = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }
            Information aod;
            //Capture as of date
            try
            {
                aod = instService.GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), offset);
            }
            catch (Exception ex)
            {
                aod = instService.GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), 0);
            }

            StringBuilder sqlQuery = new StringBuilder();
            if (inst == null || inst.ToUpper() == "DDW_ATLAS_TEMPLATES")
            {
                sqlQuery.AppendLine("SELECT * FROM ATLAS_SimulationType");
            }
            else
            {
                //Now load all simulations associated with that as of date

                sqlQuery.AppendLine(" select ");
                sqlQuery.AppendLine(" 	st.name ");
                sqlQuery.AppendLine(" 	,st.id ");
                sqlQuery.AppendLine("  from Simulations as s  ");
                sqlQuery.AppendLine(" INNER JOIN ");
                sqlQuery.AppendLine(" asOfDateInfo as aod ");
                sqlQuery.AppendLine(" ON s.informationId = aod.id and aod.id = " + aod.Id + " ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_SimulationType as st ");
                sqlQuery.AppendLine(" ON s.SimulationTypeId = st.id ");
                sqlQuery.AppendLine(" ORDER BY st.id ");
            }


            //DataTable ids =  Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), sqlQuery.ToString());
            return Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), sqlQuery.ToString());
            //foreach (DataRow dr in ids.Rows)
            //{
            //    simTypeIds.Add(Convert.ToInt32(dr["id"].ToString()));
            //}
            //simTypes = _contextProvider.Context.SimulationTypes.Where(sim => simTypeIds.Contains(sim.Id)).ToList();
            //return simTypes;
        }

        [HttpGet]
        public List<ScenarioType> AvailableSimulationScenarios(string inst, int offset, int simId, bool eve)
        {
            //if sim id is 0 assume 1 for base
            if (simId == 0)
            {
                simId = 1;
            }


            var instService = new InstitutionService(inst);

            var simType = instService.GetSimulationType(simId);
            //Capture as of date
            var sim = instService.GetSimulation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), offset, simType);
            List<int> scenTypeIds = new List<int>();
            List<ScenarioType> retList = new List<ScenarioType>();
            //Now load all simulations associated with that as of date
            StringBuilder sqlQuery = new StringBuilder();
            if (inst == null || inst.ToUpper() == "DDW_ATLAS_TEMPLATES")
            {
                sqlQuery.AppendLine("SELECT * FROM ATLAS_ScenarioType ORDER BY [priority]");
            }
            else
            {
                sqlQuery.AppendLine(" SELECT ");
                sqlQuery.AppendLine(" st.id, ");
                sqlQuery.AppendLine(" st.name ");
                sqlQuery.AppendLine(" FROM Basis_Scenario as s ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as st ");
                sqlQuery.AppendLine(" ON s.ScenarioTypeId = st.id ");
                sqlQuery.AppendLine(" WHERE s.SimulationId = " + sim.Id + " AND st.isEve = " + (eve ? 1 : 0).ToString() + " ");
                sqlQuery.AppendLine(" ORDER BY st.priority ");
            }
            DataTable ids = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), sqlQuery.ToString());
            foreach (DataRow dr in ids.Rows)
            {
                scenTypeIds.Add(Int32.Parse(dr["id"].ToString()));
            }
            retList = _contextProvider.Context.ScenarioTypes.Where(st => scenTypeIds.Contains(st.Id)).OrderBy(st => st.Priority).ToList();
            return retList;
            //return Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), sqlQuery.ToString());
        }


        [HttpGet]
        //public DataTable AvailableSimulationScenariosMultipleInst(string insts, string offsets, string simIds, string eveSims)
        public List<ScenarioType> AvailableSimulationScenariosMultipleInst(string insts, string offsets, string simIds, string eveSims)
        {
            List<ScenarioType> retList = new List<ScenarioType>();
            List<int> scenTypeIds = new List<int>();
            string[] instArray = insts.Split(',');

            //early bail out
            if (Array.IndexOf(instArray, "DDW_Atlas_Templates") > -1)
            {
                return ScenarioTypes().OrderBy(s => s.Priority).ToList();
            }

            DataTable[] dts = new DataTable[2];
            for (var z = 0; z < instArray.Length; z++)
            {
                var instService = new InstitutionService(instArray[z]);


                if (simIds == "" || simIds == null)
                {
                    return null;
                }


                string[] simIdsArray = simIds.Split(',');
                string[] eveSimsArray = eveSims.Split(',');
                string[] offsetArray = offsets.Split(',');

                string[] actualIds = new string[simIdsArray.Length];

                string[] actualEveIds = new string[simIdsArray.Length];

                for (int i = 0; i < simIdsArray.Length; i++)
                {
                    var simType = instService.GetSimulationType(int.Parse((simIdsArray[i].ToString() == "" ? "1" : simIdsArray[i].ToString())));
                    //Capture as of date
                    var sim = instService.GetSimulation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), int.Parse(offsetArray[i]), simType);

                    //Set array
                    actualIds[i] = (sim == null ? "-2" : sim.Id.ToString());
                }

                for (int i = 0; i < eveSimsArray.Length; i++)
                {
                    var simType = instService.GetSimulationType(int.Parse((eveSimsArray[i].ToString() == "" ? "1" : eveSimsArray[i].ToString())));
                    //Capture as of date
                    var sim = instService.GetSimulation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), int.Parse(offsetArray[i]), simType);

                    //Set array
                    actualEveIds[i] = (sim == null ? "-2" : sim.Id.ToString());
                }

                if (actualIds.Length == 0)
                {
                    return null;
                }

                //Now load all simulations associated with that as of date
                StringBuilder sqlQuery = new StringBuilder();
                sqlQuery.AppendLine(" SELECT DISTINCT");
                sqlQuery.AppendLine(" st.id, ");
                sqlQuery.AppendLine(" st.name, ");
                sqlQuery.AppendLine(" st.isEve, ");
                sqlQuery.AppendLine(" st.Priority ");
                sqlQuery.AppendLine(" FROM Basis_Scenario as s ");
                sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as st ");
                sqlQuery.AppendLine(" ON s.ScenarioTypeId = st.id ");
                sqlQuery.AppendLine(" WHERE s.SimulationId IN (" + String.Join(",", actualIds) + ") OR s.simulationId IN (" + String.Join(",", actualEveIds) + ") ");
                sqlQuery.AppendLine(" ORDER BY st.priority ");
                dts[z] = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(instArray[z]), sqlQuery.ToString());

                dts[z].PrimaryKey = new DataColumn[] { dts[z].Columns["id"], dts[z].Columns["name"], dts[z].Columns["isEve"], dts[z].Columns["Priority"] };
            }

            for (int d = 1; d < dts.Length; d++)
            {
                dts[0].Merge(dts[d]);
            }
            foreach (DataRow dr in dts[0].Rows)
            {
                scenTypeIds.Add(Int32.Parse(dr["id"].ToString()));
            }
            retList = _contextProvider.Context.ScenarioTypes.Where(s => scenTypeIds.Contains(s.Id)).OrderBy(s => s.Priority).ToList();
            //return dts[0];
            return retList;
        }


        [HttpGet]
        //public DataTable AvailableSimulationScenariosMultiple(string inst, string offsets, string simIds, string eveSims)
        public List<ScenarioType> AvailableSimulationScenariosMultiple(string inst, string offsets, string simIds, string eveSims)
        {
            if (inst == "DDW_Atlas_Templates")
            {
                return ScenarioTypes().OrderBy(s => s.Priority).ToList();
            }

            var instService = new InstitutionService(inst);
            List<ScenarioType> retList = new List<ScenarioType>();
            List<int> scenTypeIds = new List<int>();
            DataTable dt = new DataTable();
            if (simIds == "" || simIds == null || eveSims == "" || eveSims == null)
            {
                return null;
            }


            string[] simIdsArray = simIds.Split(',');
            string[] eveSimsArray = eveSims.Split(',');
            string[] offsetArray = offsets.Split(',');

            string[] actualIds = new string[simIdsArray.Length];

            string[] actualEveIds = new string[simIdsArray.Length];

            for (int i = 0; i < simIdsArray.Length; i++)
            {
                var simType = instService.GetSimulationType(int.Parse((simIdsArray[i].ToString() == "" ? "1" : simIdsArray[i].ToString())));
                //Capture as of date
                var sim = instService.GetSimulation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), int.Parse(offsetArray[i]), simType);

                //Set array
                actualIds[i] = (sim == null ? "-2" : sim.Id.ToString());
            }

            for (int i = 0; i < eveSimsArray.Length; i++)
            {
                var simType = instService.GetSimulationType(int.Parse((eveSimsArray[i].ToString() == "" ? "1" : eveSimsArray[i].ToString())));
                //Capture as of date
                var sim = instService.GetSimulation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), int.Parse(offsetArray[i]), simType);

                //Set array
                actualEveIds[i] = (sim == null ? "-2" : sim.Id.ToString());
            }

            if (actualIds.Length == 0)
            {
                return null;
            }

            //Now load all simulations associated with that as of date
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT DISTINCT");
            sqlQuery.AppendLine(" st.id, ");
            sqlQuery.AppendLine(" st.name, ");
            sqlQuery.AppendLine(" st.isEve, ");
            sqlQuery.AppendLine(" st.Priority ");
            sqlQuery.AppendLine(" FROM Basis_Scenario as s ");
            sqlQuery.AppendLine(" INNER JOIN ATLAS_ScenarioType as st ");
            sqlQuery.AppendLine(" ON s.ScenarioTypeId = st.id ");
            sqlQuery.AppendLine(" WHERE s.SimulationId IN (" + String.Join(",", actualIds) + ") OR s.simulationId IN (" + String.Join(",", actualEveIds) + ") ");
            sqlQuery.AppendLine(" ORDER BY st.priority ");
            dt = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(inst), sqlQuery.ToString());
            foreach (DataRow dr in dt.Rows)
            {
                scenTypeIds.Add(Int32.Parse(dr["id"].ToString()));
            }
            retList = _contextProvider.Context.ScenarioTypes.Where(s => scenTypeIds.Contains(s.Id)).OrderBy(s => s.Priority).ToList();
            return retList;
        }

        [HttpGet]
        public object LookbackGroupDataProjections(int layoutId, int startOffset, int endOffset, int simTypeId, int scenTypeId, bool taxEquiv, string lbAod)
        {
            var lbgdService = new LookbackGroupDataService();
            //DateTime aod = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            DateTime aod = DateTime.Parse(lbAod);
            var instService = new InstitutionService("");
            var startObj = instService.GetSimulationScenario(aod, startOffset, simTypeId, scenTypeId);
            var endObj = instService.GetSimulationScenario(aod, endOffset, simTypeId, scenTypeId);
            string stSimulationId = startObj.GetType().GetProperty("simulation").GetValue(startObj).ToString();
            string stScenario = startObj.GetType().GetProperty("scenario").GetValue(startObj).ToString();
            string stAsOfDate = startObj.GetType().GetProperty("asOfDate").GetValue(startObj).ToString();
            string endSimulationId = endObj.GetType().GetProperty("simulation").GetValue(endObj).ToString();
            string endScenario = endObj.GetType().GetProperty("scenario").GetValue(endObj).ToString();
            string endAsOfDate = endObj.GetType().GetProperty("asOfDate").GetValue(endObj).ToString();

            var ret = lbgdService.GetLBGDProjectionData(layoutId, stSimulationId, stScenario, stAsOfDate, endAsOfDate, endSimulationId, taxEquiv, instService);

            return ret;
        }

        [HttpGet]
        public IQueryable<LBLookbackGroup> UpdateLookbackGroup(int startOffset, int endOffset, int groupId, int lookbackId)
        {
            var lbgdService = new LookbackGroupDataService();
            var lookback = _contextProvider.Context.LBLookbacks.Where(l => l.Id == lookbackId).Include(n => n.LookbackGroups);
            DateTime aod = DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString());
            var instService = new InstitutionService("");
            var startObj = instService.GetSimulationScenario(aod, startOffset, lookback.Select(a => a.SimulationTypeId).FirstOrDefault(), lookback.Select(a => a.ScenarioTypeId).FirstOrDefault());
            var endObj = instService.GetSimulationScenario(aod, endOffset, lookback.Select(a => a.SimulationTypeId).FirstOrDefault(), lookback.Select(a => a.ScenarioTypeId).FirstOrDefault());

            string stAsOfDate = startObj.GetType().GetProperty("asOfDate").GetValue(startObj).ToString();
            string endAsOfDate = startObj.GetType().GetProperty("asOfDate").GetValue(endObj).ToString();
            int StartToEndDays = DateTime.Parse(endAsOfDate).Subtract(DateTime.Parse(stAsOfDate)).Days;
            int YearFromStartDays = (DateTime.Parse(stAsOfDate).AddMonths(12)).Subtract(DateTime.Parse(endAsOfDate)).Days;

            string updateGroupDates = "UPDATE ATLAS_LBLookbackGroup SET [StartDate] = '" + stAsOfDate + "', [EndDate] = '" + endAsOfDate + "', [StartToEndDays] = " + StartToEndDays + ", [YearFromStartDays] = " + YearFromStartDays + " WHERE Id = " + groupId;
            Utilities.ExecuteSql(instService.ConnectionString(), updateGroupDates);

            var group = _contextProvider.Context.LBLookbackGroup.Where(g => g.Id == groupId).Where(g => g.LBLookbackId == lookbackId);

            return group;
        }

        [HttpGet]
        public IQueryable<LBLookbackData> LookbackGroupDataConfigById(int groupId)
        {
            var d = _contextProvider.Context.LBLookbackData.Where(a => a.LBLookbackGroupId == groupId).OrderBy(a => a.Priority);
            return d;
        }

        [HttpGet]
        public object LookbackReportViewDataById(int id)
        {
            var lbvService = new LookbackReportService();
            return lbvService.LookbackReportViewModel(id);

        }

        [HttpGet]
        public List<LBLookback> LBLookbacksByReportId(int reportId)
        {
            List<LBLookback> retList = new List<LBLookback>();
            var set = _contextProvider.Context.LookbackReport.Where(rep => rep.Id == reportId).Include(rep => rep.Sections.Select(s => s.LookbackTypes)).FirstOrDefault();
            List<LBLookback> subLbs = new List<LBLookback>();
            foreach (LookbackReportSection section in set.Sections)
            {
                foreach (LookbackType type in section.LookbackTypes.ToList())
                {
                    LBLookback curLB = _contextProvider.Context.LBLookbacks.Where(lb => lb.Id == type.LBLookbackId).Include(lb => lb.LookbackGroups.Select(lbg => lbg.LBCustomType)).FirstOrDefault();
                    if (curLB != null)
                    {
                        LBLookbackGroup curGrp = curLB.LookbackGroups.First();
                        curGrp.LBLookbackData = _contextProvider.Context.LBLookbackData.Where(lbd => lbd.LBLookbackGroupId == curGrp.Id).ToList();
                        retList.Add(curLB);
                    }

                }
            }
            return retList;
        }

        //Delete Lookbacks
        [HttpGet]
        public string DeleteLookback(int id)
        {

            //Delete Lowest Level
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" DELETE FROM ATLAS_LBLookbackData WHERE id IN( ");
            sqlQuery.AppendLine(" SELECT lbd.id FROM ATLAS_LBLookbackGroup as lbg ");
            sqlQuery.AppendLine(" INNER JOIN  ");
            sqlQuery.AppendLine(" ATLAS_LBLookbackData as lbd ");
            sqlQuery.AppendLine(" ON lbd.LBLookbackGroupId = lbg.id ");
            sqlQuery.AppendLine("  where lbg.LBLookbackId = " + id.ToString() + ") ");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());


            //Delete Second Level
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM ATLAS_LBLookbackGroup WHERE LBLookbackId = " + id.ToString() + ";");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            //Delete Top Level
            sqlQuery.Clear();
            //sqlQuery.AppendLine("DELETE FROM ATLAS_LBLookback WHERE id = " + id.ToString() + ";");
            // Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            return "SUCCESS";
        }

        //Delete LookbacksGroup
        [HttpGet]
        public string DeleteLookbackGroup(int id)
        {

            //Delete Lowest Level
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine("DELETE FROM ATLAS_LBLookbackData WHERE LBLookbackGroupId = " + id.ToString());
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            //Delete Top Level
            //sqlQuery.Clear();
            //sqlQuery.AppendLine("DELETE from ATLAS_LBLookbackGroup WHERE id = " + id.ToString());
            //Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            return "SUCCESS";
        }


        //Delete LookbacksLayouts
        [HttpGet]
        public string DeleteLayout(int id)
        {

            //Delete Lowest Level
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" DELETE FROM ATLAS_LBCustomLayoutClassifications WHERE id IN( ");
            sqlQuery.AppendLine(" SELECT lbc.id FROM ATLAS_LBCustomLayoutRowItem as lbr ");
            sqlQuery.AppendLine(" INNER JOIN  ");
            sqlQuery.AppendLine(" ATLAS_LBCustomLayoutClassifications as lbc ");
            sqlQuery.AppendLine(" ON lbc.LBCustomLayoutRowItemId = lbr.id ");
            sqlQuery.AppendLine("  where lbr.LBCustomLayoutId = " + id.ToString() + ") ");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());


            //Delete Second Level
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM ATLAS_LBLayoutRowItem WHERE LBCustomLayoutId = " + id.ToString() + ";");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            //Delete Top Level
            sqlQuery.Clear();
            sqlQuery.AppendLine("DELETE FROM ATLAS_LBCustomLayout WHERE id = " + id.ToString() + ";");
            Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), sqlQuery.ToString());

            return "SUCCESS";
        }



        [HttpGet]
        public IQueryable<Eve> Eves()
        {
            var eves = _contextProvider.Context.Eves
                                              .Include(e => e.EveScenarioTypes.Select(est => est.ScenarioType));
            return eves;
        }

        [HttpGet]
        public object EveViewDataById(int eveId)
        {
            var eveService = new EveService();
            return eveService.GetConsolidatedEveData(eveId);
        }

        [HttpGet]
        public IQueryable<Document> Documents()
        {
            var documents = _contextProvider.Context.Documents;
            return documents;
        }

        [HttpGet]
        public IQueryable<Document> DocumentsViewById(string docId)
        {
            const string TEMPLATE_DB_NAME = "DDW_Atlas_Templates";

            int docIDInt;

            if (!int.TryParse(docId, out docIDInt))
            {
                return Enumerable.Empty<Document>().AsQueryable();
            }

            string connectionString = Utilities.BuildInstConnectionString("");

            string query = @"
                SELECT
                    p.Id AS PackageID,
                    r.Id AS ReportID,
                    r.Orientation,
                    r.SourceReportID,
                    r.[Defaults]
                FROM ATLAS_Packages p
                INNER JOIN ATLAS_Report r ON p.Id = r.PackageId
                WHERE r.Id = " + docId;

            DataRow rawResult = Utilities.ExecuteSql(connectionString, query).Rows.Cast<DataRow>().FirstOrDefault();

            // this means that we can't even find the Report in our bank, not good
            if (ReferenceEquals(null, rawResult))
            {
                return _contextProvider.Context.Documents.Where(d => d.Id == docIDInt);
            }

            int packageID = Convert.ToInt32(rawResult["PackageID"]);
            int reportID = Convert.ToInt32(rawResult["ReportID"]);

            string currentDatabaseName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();

            bool usingDefaults = Convert.ToBoolean(rawResult["Defaults"]);

            Document result;

            // not from templates, or we've overridden Templates. just return the doc
            if (string.Equals(currentDatabaseName, TEMPLATE_DB_NAME, StringComparison.InvariantCultureIgnoreCase) || !usingDefaults)
            {
                result = _contextProvider.Context.Documents.FirstOrDefault(d => d.Id == docIDInt);

                if (ReferenceEquals(null, result))
                {
                    return Enumerable.Empty<Document>().AsQueryable();
                }

                result.Orientation = rawResult.IsNull("Orientation")
                    ? (DocumentOrientation?)null
                    : (DocumentOrientation)Convert.ToByte(rawResult["Orientation"]);

                return new[] { result }.AsQueryable();
            }

            int? templateReportID = rawResult.IsNull("SourceReportID")
                ? reportID
                : PackageService.GetTemplateReportId(currentDatabaseName, reportID, packageID, Convert.ToInt32(rawResult["SourceReportID"]));

            if (!templateReportID.HasValue)
            {
                throw new InvalidOperationException("Potential data corruption, cannot find template id for report " + reportID);
            }

            docIDInt = templateReportID.Value;
            GlobalContextProvider templateGTX = new GlobalContextProvider(TEMPLATE_DB_NAME);
            DocumentOrientation? orientation = templateGTX.Context.Reports.Where(r => r.Id == docIDInt).Select(r => r.Orientation).FirstOrDefault();

            result = templateGTX.Context.Documents.FirstOrDefault(d => d.Id == docIDInt);

            if (ReferenceEquals(null, result))
            {
                return Enumerable.Empty<Document>().AsQueryable();
            }

            result.Orientation = orientation;

            return new[] { result }.AsQueryable();
        }

        [HttpGet]
        public IQueryable<object> Files()
        {
            var files = _contextProvider.Context.Files.OrderByDescending(f => f.UploadDate).Select(f => new { f.Id, f.Name, f.UploadDate });
            return files;
        }

        [HttpGet]
        public HttpResponseMessage ServeFile(int id)
        {
            var file = _contextProvider.Context.Files.Find(id);

            HttpResponseMessage response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new ByteArrayContent(file.Data);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

            return response;
        }

        [HttpPost]
        public HttpResponseMessage UploadFile()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var description = httpRequest.Form["description"];
                    var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);

                    MemoryStream target = new MemoryStream();
                    postedFile.InputStream.CopyTo(target);
                    byte[] data = target.ToArray();

                    _contextProvider.Context.Files.Add(new Atlas.Institution.Model.File()
                    {
                        Name = postedFile.FileName,
                        UploadDate = DateTime.Now,
                        Data = data
                    }
                    );
                }

                _contextProvider.Context.SaveChanges();
                result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }

        [HttpGet]
        public IQueryable<BalanceSheetCompare> BalanceSheetCompares()
        {
            var eves = _contextProvider.Context.BalanceSheetCompares;
            return eves;
        }

        [HttpGet]
        public object BalanceSheetCompareViewDataById(int balanceSheetCompareId)
        {
            var balanceSheetCompare = new BalanceSheetCompareService();
            return balanceSheetCompare.BalanceSheetCompareViewModel(balanceSheetCompareId);
        }

        [HttpGet]
        public IQueryable<BalanceSheetMix> BalanceSheetMixes()
        {
            var mixes = _contextProvider.Context.BalanceSheetMixes;
            return mixes;
        }

        [HttpGet]
        public object BalanceSheetMixViewDataById(int balanceSheetMixId)
        {
            var balanceSheetMix = new BalanceSheetMixService();
            return balanceSheetMix.BalanceSheetMixViewModel(balanceSheetMixId);
        }

        //Basic Surplus Reports
        [HttpGet]
        public IQueryable<BasicSurplusReport> BasicSurplusReports()
        {
            var eves = _contextProvider.Context.BasicSurplusReports;
            return eves;
        }

        [HttpGet]
        public IQueryable<BasicSurplusReport> BasicSurplusReportViewDataById(int basicSurplusReportId)
        {

            //NEed to check for polic
            BasicSurplusReport bs = _contextProvider.Context.BasicSurplusReports.Where(s => s.Id == basicSurplusReportId).FirstOrDefault();


            //Load as of date from 
            var instService = new InstitutionService(bs.InstitutionDatabaseName);
            var info = instService.GetInformation(DateTime.Parse(Utilities.GetAtlasUserData().Rows[0]["asOfDate"].ToString()), bs.AsOfDateOffset);

            //Load BS id from 
            DataTable bsId = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(bs.InstitutionDatabaseName), "SELECT BasicSurplusAdminId FROM ATLAS_Policy WHERE asOfDate = '" + info.AsOfDate.ToShortDateString() + "'");

            if (bsId.Rows.Count > 0 && !bs.OverrideBasicSurplus)
            {
                bs.BasicSurplusAdminId = int.Parse(bsId.Rows[0][0].ToString());
            }

            _contextProvider.Context.SaveChanges();

            return _contextProvider.Context.BasicSurplusReports.Where(s => s.Id == basicSurplusReportId);
        }

        //Interest Rate Policy Guidelines Reports
        [HttpGet]
        public IQueryable<InterestRatePolicyGuidelines> InterestRatePolicyGuidelines(int id)
        {
            return _contextProvider.Context.InterestRatePolicyGuidelines
                .Include(s => s.Dates)
                .Include(s => s.NIIComparatives.Select(c => c.ScenarioTypes.Select(f => f.Scenarios)))
                .Include(s => s.EVEComparatives.Select(c => c.Scenarios))
                .Where(s => s.Id == id);
        }

        [HttpGet]
        public object interestRatePolicyGuidelinesViewDataById(int interestRatePolicyGuidelinesId)
        {
            var interestRatePolicyGuidelines = new InterestRatePolicyGuidelinesService();
            return interestRatePolicyGuidelines.InterestRatePolicyGuidelinesViewModel(interestRatePolicyGuidelinesId);
        }

        [HttpGet]
        public IQueryable<LoanCapFloor> LoanCapFloors()
        {
            var loancapfloors = _contextProvider.Context.LoanCapFloors;
            return loancapfloors;
        }


        [HttpGet]
        public object LoanCapFloorViewDataById(int lcfId)
        {
            var loancapfloorService = new LoanCapFloorService();
            return loancapfloorService.GetConsolidatedLoanCapFloorData(lcfId);
        }

        //Cover Pages
        [HttpGet]
        public CoverPage CoverPages(int id)
        {
            var cvs = _contextProvider.Context.CoverPages.Where(c => c.Id == id).FirstOrDefault();
            if (!cvs.OverrideMeetingDate)
            {
                string aod = cvs.AsOfDate.ToShortDateString();
                string qry = "SELECT MeetingDate FROM ATLAS_Policy WHERE AsOfDate = '" + aod + "'";
                DataTable result = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(""), qry);
                if (result.Rows.Count > 0)
                {
                    cvs.MeetingDate = DateTime.Parse(result.Rows[0]["MeetingDate"].ToString());
                }
            }
            return cvs;
        }

        //Section Pages
        [HttpGet]
        public IQueryable<SectionPage> SectionPages(int id)
        {
            var cvs = _contextProvider.Context.SectionPages.Where(s => s.Id == id);
            return cvs;
        }

        //Eve Nev Assumption Methodoligies
        [HttpGet]
        public IQueryable<EveNevAssumptions> EveNevAssumptions(int id)
        {
            var cvs = _contextProvider.Context.EveNevAssumptions.Where(s => s.Id == id);
            return cvs;
        }

        [HttpGet]
        public object EveNevAssumptionsViewDataById(int id)
        {
            var d = new EveNevAssumptionsService();
            return d.EveNevAssumptionsViewModel(id);
        }

        //Simulation Summaries
        [HttpGet]
        public IQueryable<PrepaymentDetails> PrepaymentDetails(int id)
        {
            var prepayDtls = _contextProvider.Context.PrepaymentDetails.Where(s => s.Id == id).Include(p => p.PrePaymentDetailScenarioTypes.Select(pd => pd.PrepaymentDetails));
            return prepayDtls;
        }

        [HttpGet]
        public object PrepaymentDetailsViewDataById(int id)
        {
            var prepayDtls = new PrepaymentDetailsService();
            return prepayDtls.PrepaymentDetailsViewModel(id);
        }

        //Simulation Summaries
        [HttpGet]
        public IQueryable<SimulationSummary> SimulationSummaries()
        {
            var simSum = _contextProvider.Context.SimulationSummaries;
            return simSum;
        }

        [HttpGet]
        public object SimulationSummaryViewDataById(int simSumId)
        {
            var simSum = new SimulationSummaryService();
            return simSum.SimulationSummaryViewModel(simSumId);
        }

        //StaticGap
        [HttpGet]
        public IQueryable<StaticGap> StaticGaps()
        {
            var sg = _contextProvider.Context.StaticGaps;
            return sg;
        }

        [HttpGet]
        public object StaticGapViewDataById(int staticGapId)
        {
            var staticGap = new StaticGapService();
            return staticGap.StaticGapViewModel(staticGapId);
        }

        //Funding Matrix
        [HttpGet]
        public IQueryable<FundingMatrix> FundingMatrices()
        {
            var matrices = _contextProvider.Context.FundingMatrices.Include(f => f.FundingMatrixOffsets);
            return matrices;
        }

        [HttpGet]
        public object FundingMatrixViewDataById(int fundingMatrixId)
        {
            var fundingMatrix = new FundingMatrixService();
            return fundingMatrix.FundingMatrixViewModel(fundingMatrixId);
        }

        //Time Deposit Migrations
        [HttpGet]
        public IQueryable<TimeDepositMigration> TimeDepositMigrations()
        {
            var timeDep = _contextProvider.Context.TimeDepositMigrations;
            return timeDep;
        }

        [HttpGet]
        public object TimeDepositMigrationViewDataById(int timeDepositId)
        {
            var timeDep = new TimeDepositMigrationService();
            return timeDep.TimeDepositMigrationViewModel(timeDepositId);
        }

        //Core Funding Utilization
        [HttpGet]
        public IQueryable<CoreFundingUtilization> CoreFundingUtilizations()
        {
            var coreFund = _contextProvider.Context.CoreFundingUtilizations
                .Include(s => s.CoreFundingUtilizationScenarioTypes.Select(z => z.ScenarioType))
                .Include(s => s.CoreFundingUtilizationDateOffSets);
            return coreFund;
        }

        [HttpGet]
        public object CoreFundingUtilizationViewDataById(int coreFundId)
        {
            var coreFund = new CoreFundingUtilizationService();
            return coreFund.CoreFundingUtilizationViewModel(coreFundId);
        }

        //Yield Curve Shift
        [HttpGet]
        public IQueryable<YieldCurveShift> YieldCurveShifts()
        {
            var yieldShift = _contextProvider.Context.YieldCurveShifts
                .Include(s => s.YieldCurveShiftScenarioTypes.Select(z => z.WebRateScenarioType))
                .Include(s => s.YieldCurveShiftPeriodCompares)
                .Include(s => s.YieldCurveShiftHistoricDates)
                ;
            return yieldShift;
        }

        [HttpGet]
        public object YieldCurveShiftViewDataById(int yieldShiftId)
        {
            var yieldShift = new YieldCurveShiftService();
            return yieldShift.YieldCurveShiftViewModel(yieldShiftId);
        }

        //Cashflow Report
        [HttpGet]
        public IQueryable<CashflowReport> CashflowReports(int id)
        {
            var sg = _contextProvider.Context.CashflowReports.Where(s => s.Id == id).Include(s => s.CashflowSimulationScenario);
            return sg;
        }

        [HttpGet]
        public object CashflowReportViewDataById(int id)
        {
            var cashFlow = new CashflowService();
            return cashFlow.CashflowViewModel(id);
        }


        //Net Cashflow Report
        [HttpGet]
        public IQueryable<NetCashflow> NetCashflow(int id)
        {
            var sg = _contextProvider.Context.NetCashflows.Where(s => s.Id == id).Include(s => s.NetCashflowSimulationScenario);
            return sg;
        }

        [HttpGet]
        public object NetCashflowViewDataById(int id)
        {
            var cashFlow = new NetCashflowService();
            return cashFlow.NetCashflowViewModel(id);
        }


        //ASC825 Balance Sheet
        [HttpGet]
        public IQueryable<ASC825BalanceSheet> ASC825BalanceSheet(int id)
        {
            var sg = _contextProvider.Context.ASC825BalanceSheets.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object ASC825BalanceSheetViewDataById(int id)
        {
            var asc = new ASC825BalanceSheetService();
            return asc.ASC825BalanceSheetViewModel(id);
        }

        //ASC825 Data Source
        [HttpGet]
        public IQueryable<ASC825DataSource> ASC825DataSource(int id)
        {
            var sg = _contextProvider.Context.ASC825DataSources.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object ASC825DataSourceViewDataById(int id)
        {
            var asc = new ASC825DataSourceService();
            return asc.ASC825DataSourceViewModel(id);
        }

        //ASC825 Discount Rate
        [HttpGet]
        public IQueryable<ASC825DiscountRate> ASC825DiscountRate(int id)
        {
            var sg = _contextProvider.Context.ASC825DiscountRates.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object ASC825DiscountRateViewDataById(int id)
        {
            var asc = new ASC825DiscountRateService();
            return asc.ASC825DiscountRateViewModel(id);
        }

        //ASC825 WorkSheet
        [HttpGet]
        public IQueryable<ASC825Worksheet> ASC825Worksheet(int id)
        {
            var sg = _contextProvider.Context.ASC825Worksheets.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object ASC825WorksheetViewDataById(int id)
        {
            var asc = new ASC825WorksheetService();
            return asc.ASC825WorksheetViewModel(id);
        }

        //ASC825 Market Value
        [HttpGet]
        public IQueryable<ASC825MarketValue> ASC825MarketValue(int id)
        {
            var sg = _contextProvider.Context.ASC825MarketValues.Where(s => s.Id == id).Include(s => s.EveScenarioTypes);
            return sg;
        }

        [HttpGet]
        public object ASC825MarketValueViewDataById(int id)
        {
            var asc = new ASC825MarketValueService();
            return asc.ASC825MarketValueViewModel(id);
        }


        //Rate Change Matrix
        [HttpGet]
        public IQueryable<RateChangeMatrix> RateChangeMatrix(int id)
        {
            var sg = _contextProvider.Context.RateChangeMatrices.Where(s => s.Id == id).Include(s => s.RateChangeMatrixScenarios);
            return sg;
        }

        [HttpGet]
        public object RateChangeMatrixViewDataById(int id)
        {
            var asc = new RateChangeMatrixService();
            return asc.RateChangeMatrixViewModel(id);
        }


        //Investment Portfolio Valuation
        [HttpGet]
        public IQueryable<InvestmentPortfolioValuation> InvestmentPortfolioValuation(int id)
        {
            var sg = _contextProvider.Context.InvestmentPortfolioValuations.Where(s => s.Id == id).Include(s => s.Scenarios);
            return sg;
        }

        [HttpGet]
        public object InvestmentPortfolioValuationViewDataById(int id)
        {
            var asc = new InvestmentPortfolioValuationService();
            return asc.InvestmentPortfolioValuationViewModel(id);
        }

        //Investment Detail
        [HttpGet]
        public IQueryable<InvestmentDetail> InvestmentDetail(int id)
        {
            var sg = _contextProvider.Context.InvestmentDetails.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object InvestmentDetailViewDataById(int id)
        {
            var asc = new InvestmentDetailService();
            return asc.InvestmentDetailViewModel(id);
        }

        //Investment Error
        [HttpGet]
        public IQueryable<InvestmentError> InvestmentError(int id)
        {
            var sg = _contextProvider.Context.InvestmentErrors.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object InvestmentErrorViewDataById(int id)
        {
            var asc = new InvestmentErrorService();
            return asc.InvestmentErrorViewModel(id);
        }

        //Summary Of Rates
        [HttpGet]
        public IQueryable<SummaryRate> SummaryRates(int id)
        {
            var sg = _contextProvider.Context.SummaryRates.Where(s => s.Id == id).Include(s => s.SummaryRateScenarios);
            return sg;
        }

        [HttpGet]
        public object SummaryRateViewDataById(int id)
        {

            var sr = new SummaryRateService();
            return sr.SummaryRateViewModel(id);
        }

        //Lookback Report
        [HttpGet]
        public IQueryable<LookbackReport> LookbackReports(int id)
        {
            var lb = _contextProvider.Context.LookbackReport.Where(r => r.Id == id).Include(r => r.Sections.Select(s => s.LookbackTypes));
            return lb;
        }


        [HttpGet]
        //Check NII Simulation
        public object CheckNiiSimulation(int id)
        {
            var instContext = new InstitutionContext(Utilities.BuildInstConnectionString(""));
            var recon = instContext.NIIRecons.Where(s => s.Id == id).Include(s => s.ScenarioTypes).FirstOrDefault();
            var rep = instContext.Reports.Where(s => s.Id == id).FirstOrDefault();
            var check = new NIIReconService();
            return check.CheckNIISimulation(recon.InstitutionDatabaseName, rep.Package.AsOfDate, recon.AsOfDateOffset, recon.SimulationType);
        }

        //NII Recon
        [HttpGet]
        public IQueryable<NIIRecon> NIIRecons(int id)
        {
            var sg = _contextProvider.Context.NIIRecons.Where(s => s.Id == id).Include(s => s.ScenarioTypes);
            return sg;
        }


        //Capital Analysis
        [HttpGet]
        public IQueryable<CapitalAnalysis> CapitalAnalysis(int id)
        {
            var sg = _contextProvider.Context.CapitalAnalysis.Where(s => s.Id == id)
                .Include(s => s.HistoricalBufferOffsets);
            return sg;
        }

        [HttpGet]
        public object CapitalAnalysisViewDataById(int id)
        {
            var c = new CapitalAnalysisService();
            return c.CapitalAnalysisViewModel(id);
        }

        //ASC825 Assumption Methods
        [HttpGet]
        public IQueryable<ASC825AssumptionMethods> ASC825AssumptionMethods(int id)
        {
            var sg = _contextProvider.Context.ASC825AssumptionMethods.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object ASC825AssumptionMethodsViewDataById(int id)
        {
            var c = new ASC825AssumptionMethodsService();
            return c.ASC825AssumptionMethodsViewModel(id);
        }

        //Assumption Methods
        [HttpGet]
        public IQueryable<AssumptionMethods> AssumptionMethods(int id)
        {
            var sg = _contextProvider.Context.AssumptionMethods.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object AssumptionMethodsViewDataById(int id)
        {
            var c = new AssumptionMethodsService();
            return c.AssumptionMethodsViewModel(id);
        }

        //Liability Pricing Analysis
        [HttpGet]
        public IQueryable<LiabilityPricingAnalysis> LiabilityPricingAnalysis(int id)
        {
            var sg = _contextProvider.Context.LiabilityPricingAnalysis.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object LiabilityPricingAnalysisViewDataById(int id)
        {
            var c = new LiabilityPricingAnalysisService();
            return c.LiabilityPricingAnalysisViewModel(id);
        }

        //Historical Balance Sheet Compare NII
        [HttpGet]
        public IQueryable<HistoricalBalanceSheetNII> HistoricalBalanceSheetNIIs()
        {
            var hist = _contextProvider.Context.HistoricalBalanceSheetNIIs
                .Include(s => s.HistoricalBalanceSheetNIISimulations)
                .Include(s => s.HistoricalBalanceSheetNIIScenarios)
                .Include(s => s.HistoricalBalanceSheetNIITopFootNotes)
                .Include(s => s.HistoricalBalanceSheetNIIBottomFootNotes)
                .Include(s => s.LeftChart.ChartScenarioTypes.Select(scst => scst.ScenarioType))
                .Include(s => s.RightChart.ChartScenarioTypes.Select(scst => scst.ScenarioType))
                .Include(s => s.LeftChart.ChartSimulationTypes.Select(sc => sc.SimulationType))
                .Include(s => s.RightChart.ChartSimulationTypes.Select(sc => sc.SimulationType));
            List<HistoricalBalanceSheetNII> list = hist.ToList();
            foreach (HistoricalBalanceSheetNII h in list)
            {
                h.LeftChart.ChartSimulationTypes = h.LeftChart.ChartSimulationTypes.OrderBy(r => r.Priority).ToList();
                h.RightChart.ChartSimulationTypes = h.RightChart.ChartSimulationTypes.OrderBy(r => r.Priority).ToList();
            }
            return hist;
        }

        [HttpGet]
        public object HistoricalBalanceSheetNIIViewDataById(int id)
        {
            var hist = new HistoricalBalanceSheetNIIService();
            return hist.HistoricalBalanceSheetNIIViewModel(id);
        }

        [HttpGet]
        public IQueryable<SimulationType> SimulationTypes()
        {
            return _contextProvider.Context.SimulationTypes.AsNoTracking();
        }


        //Liqduity Projection
        [HttpGet]
        public IQueryable<LiquidityProjection> LiquidityProjections(int id)
        {
            var sg = _contextProvider.Context.LiquidityProjections.Include(s => s.LiquidityProjectionDateOffsets).Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object LiquidityProjectionViewDataById(int id)
        {
            var c = new LiquidityProjectionService();
            return c.LiquidityProjectionViewModel(id);
        }

        //DEtailed Simulation Assumptions
        [HttpGet]
        public IQueryable<DetailedSimulationAssumption> DetailedSimulationAssumption(int id)
        {
            var sg = _contextProvider.Context.DetailedSimulationAssumptions.Where(s => s.Id == id);
            return sg;
        }

        [HttpGet]
        public object DetailedSimulationAssumptionViewDataById(int id)
        {
            var c = new DetailedSimulationAssumptionService();
            return c.DetailedSimulationAssumptionViewModel(id);
        }

        //Executive Risk Summary


        [HttpGet]
        public object ExecutiveSummaryViewDataById(int id)
        {
            ExecutiveRiskSummaryService execService = new ExecutiveRiskSummaryService();
            return execService.ExecutiveRiskSummaryViewModel(id);
        }


        [HttpGet]
        public IQueryable<ExecutiveRiskSummary> ExecutiveRiskSummaries(int id)
        {
            var sg = _contextProvider.Context.ExecutiveRiskSummaries
            .Include(s => s.ExecutiveRiskSummaryLiquiditySections.Select(b => b.ExecutiveRiskSummaryLiquiditySectionDets))
            .Include(s => s.ExecutiveSummaryIRRSections.Select(l => l.ExecutiveRiskSummaryIRRSectionDets.Select(h => h.ExecutiveSummaryIRRScenarios)))
            .Include(s => s.ExecutiveRiskSummaryCapitalSection)
            .Include(s => s.ExecutiveRiskSummaryDateOffsets)
            .Where(s => s.Id == id);
            return sg;
        }


        //Inventory of Liquiduidity Resources

        [HttpGet]
        public IQueryable<InventoryLiquidityResources> InventoryLiquidityResources(int id)
        {
            var sg = _contextProvider.Context.InventoryLiquidityResources.Include(c => c.InventoryLiquidityResourcesDateOffsets).Include(c => c.InventoryLiquidityResourcesWholesaleFunding).Include(x => x.InventoryLiquidityResourcesTier.Select(b => b.InventoryLiquidityResourcesTierDetail.Select(s => s.InventoryLiquidityResourcesTierDetailCollection)))
            .Where(s => s.Id == id);
            return sg;
        }


        [HttpGet]
        public DataTable InventoryLiquidityResourcesTierTotals(int basicSurplusId, string databaseName)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	leftS1LiqAssetTotalAmount as tier1Amount ");
            sqlQuery.AppendLine(" 	,leftS1LiqAssetTotalAmount as tier1CumAmount ");
            sqlQuery.AppendLine(" 	,midS1BasicSurplusAmount as tier1BasicSurplusAmount ");
            sqlQuery.AppendLine(" 	,leftS3LoanTotalAmount as tier2Amount ");
            sqlQuery.AppendLine(" 	,leftS1LiqAssetTotalAmount +leftS3LoanTotalAmount as tier2CumAmount  ");
            sqlQuery.AppendLine(" 	,midS1BasicSurplusFHLBAmount as tier2BasicSurplusAmount ");
            sqlQuery.AppendLine(" 	,leftS4BrokDepTotalAmount as tier3Amount ");
            sqlQuery.AppendLine(" 	,leftS1LiqAssetTotalAmount +leftS3LoanTotalAmount + leftS4BrokDepTotalAmount as tier3CumAmount  ");
            sqlQuery.AppendLine(" 	,midS1BasicSurplusBrokAmount as tier3BasicSurplusAmount ");
            sqlQuery.AppendLine(" 	,(rightS3UnsecBorTotalAvailable + rightS1OtherLiqTotalAvailable) - (SELECT amount FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName = 'leftS1LiqAssetsCashflow' and basicSurplusAdminId = " + basicSurplusId + ")  as otherLiqAmount ");
            sqlQuery.AppendLine(" 	,leftS1LiqAssetTotalAmount +leftS3LoanTotalAmount + leftS4BrokDepTotalAmount + ((rightS3UnsecBorTotalAvailable + rightS1OtherLiqTotalAvailable) - (SELECT amount FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName = 'leftS1LiqAssetsCashflow' and basicSurplusAdminId = " + basicSurplusId + ")) as otherLiqCumAmount  ");
            sqlQuery.AppendLine("  From ATLAS_BasicSurplusAdmin where id = " + basicSurplusId + " ");

            DataTable vals = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(databaseName), sqlQuery.ToString());

            return vals;
        }

        [HttpGet]
        public DataTable InventoryLiquidityResourcesFields(int basicSurplusId, string databaseName)
        {

            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT title, fieldName, Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminOvernightFund WHERE fieldName = 'leftS1OvntFundsSrtTrmInv' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, title as fieldName, Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminOvernightFund WHERE fieldName IS NULL and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Market Value Treasury/Agency Bonds' as title,   'leftS1SecColTotalSecMktValUSTAgency' as fieldName,   leftS1SecColTotalSecMktValUSTAgency as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Market Value MBS/CMO (Agency Backed)' as title,   'leftS1SecColTotalSecMktValMBSAgency' as fieldName,   leftS1SecColTotalSecMktValMBSAgency as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Market Value MBS/CMO (Private Label)' as title,   'leftS1SecColTotalSecMktValMBSPvt' as fieldName,   leftS1SecColTotalSecMktValMBSPvt as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'FHLB – Pledged Market Value from Treasury/Agency Bonds' as title, 'FHLB – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsFHLB' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'FHLB – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'FHLB – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsFHLB' and basicSurplusAdminId = " + basicSurplusId + "");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'FHLB – Pledged Market Value from MBS/CMO (Private Label)' as title, 'FHLB – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsFHLB' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Fed Discount/Other Secured – Pledged Market Value from Treasury/Agency Bonds' as title, 'Fed Discount/Other Secured – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsOtherSec' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Fed Discount/Other Secured – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'Fed Discount/Other Secured – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsOtherSec' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Fed Discount/Other Secured – Pledged Market Value from MBS/CMO (Private Label)' as title, 'Fed Discount/Other Secured – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsOtherSec' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Wholesale Repos – Pledged Market Value from Treasury/Agency Bonds' as title, 'Wholesale Repos – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsWholeRepos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Wholesale Repos – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'Wholesale Repos – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsWholeRepos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Wholesale Repos – Pledged Market Value from MBS/CMO (Private Label)' as title, 'Wholesale Repos – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsWholeRepos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Retail Repos/Sweeps – Pledged Market Value from Treasury/Agency Bonds' as title, 'Retail Repos/Sweeps – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsRetRepos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Retail Repos/Sweeps – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'Retail Repos/Sweeps – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsRetRepos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Retail Repos/Sweeps – Pledged Market Value from MBS/CMO (Private Label)' as title, 'Retail Repos/Sweeps – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsRetRepos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Municipal Deposits – Pledged Market Value from Treasury/Agency Bonds' as title, 'Municipal Deposits – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsMuniDep' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Municipal Deposits – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'Municipal Deposits – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsMuniDep' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Municipal Deposits – Pledged Market Value from MBS/CMO (Private Label)' as title, 'Municipal Deposits – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsMuniDep' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Other – Pledged Market Value from Treasury/Agency Bonds' as title, 'Other – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsOther' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Other – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'Other – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsOther' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Other – Pledged Market Value from MBS/CMO (Private Label)' as title, 'Other – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName = 'leftS1SecColsOther' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' – Pledged Market Value from Treasury/Agency Bonds' as title, title + ' – Pledged Market Value from Treasury/Agency Bonds' as fieldName, USTAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName  is null and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' – Pledged Market Value from MBS/CMO (Agency Backed)' as title, title + ' – Pledged Market Value from MBS/CMO (Agency Backed)' as fieldName, MBSAgency as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName IS NULL and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' – Pledged Market Value from MBS/CMO (Private Label)' as title, title + ' – Pledged Market Value from MBS/CMO (Private Label)' as fieldName, MBSPvt as Balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdminSecurityCollateral WHERE fieldName IS NULL and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS1SecColTotalSecColTitle + ' – Pledged Market Value from MBS/CMO (Private Label)' as title, 'leftS1SecColTotalSecColUSTAgency' as fieldName, leftS1SecColTotalSecColUSTAgency as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS1SecColTotalSecColTitle + ' – Pledged Market Value from MBS/CMO (Agency Backed)' as title, 'leftS1SecColTotalSecColMBSAgency' as fieldName, leftS1SecColTotalSecColMBSAgency as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS1SecColTotalSecColTitle + ' – Pledged Market Value from MBS/CMO (Private Label)' as title, 'leftS1SecColTotalSecColMBSPvt' as fieldName, leftS1SecColTotalSecColMBSPvt as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine("  SELECT 'Total of Unencumbered Securities', 'leftS1BlankTotalAmount' as fieldName, leftS1BlankTotalAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, fieldName, amount, 'Tier1' as section FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName = 'leftS1LiqAssetsColSecPos' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, fieldName, amount, 'Tier1' as section FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName = 'leftS1LiqAssetsGovLoans' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, fieldName, amount, 'Tier1' as section FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName = 'leftS1LiqAssetsCashflow' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, fieldName, amount, 'Tier1' as section FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName = 'leftS1LiqAssetsOther' and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, title as fieldName, amount, 'Tier1' as section FROM ATLAS_BasicSurplusAdminLiquidAsset WHERE fieldName IS NULL and basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS1LiqAssetTotalTitle as title, 'leftS1LiqAssetTotalAmount' as fieldName, leftS1LiqAssetTotalAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2MatUnsecLiabTitle as title, 'leftS2MatUnsecLiabAmount' as fieldName, leftS2MatUnsecLiabAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Total Deposits' as title, 'midS1TotalDepositAmount' as fieldName, midS1TotalDepositAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2TotalDepTitle as title, 'leftS2TotalDepAmount' as fieldName, leftS2TotalDepAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2RegCDMatTitle as title, 'leftS2RegCDMatAmount' as fieldName, leftS2RegCDMatAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2JumboCDMatTitle as title, 'leftS2JumboCDMatAmount' as fieldName, leftS2JumboCDMatAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2OtherDepTitle as title, 'leftS2OtherDepAmount' as fieldName, leftS2OtherDepAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2AddLiqResTitle as title, 'leftS2AddLiqResAmount' as fieldName, leftS2AddLiqResAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, title as fieldName, amount, 'Tier1' as section FROM ATLAS_BasicSurplusAdminVolatileLiability WHERE fieldName is NULL AND basicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS2SubHeaderTitle as title, 'midS1BasicSurplusAmount' as fieldName, midS1BasicSurplusAmount as balance, 'Tier1' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL  ");
            sqlQuery.AppendLine(" SELECT '1-4 Family Res Re Net Collateral' as title, 'leftS3FamResReColNetCollateral' as fieldName, leftS3FamResReColNetCollateral as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL  ");
            sqlQuery.AppendLine(" SELECT 'HELOC/Other Net Collateral' as title, 'leftS3HELOCOtherColNetCollateral' as fieldName, leftS3HELOCOtherColNetCollateral as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL  ");
            sqlQuery.AppendLine(" SELECT 'CRE Net Collateral' as title, 'leftS3CREColNetCollateral' as fieldName, leftS3CREColNetCollateral as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL  ");
            sqlQuery.AppendLine(" SELECT title,  ISNULL(fieldName, title) as fieldName, NetCollateral as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdminLoanCollateral WHERE BasicSurplusAdminId  = " + basicSurplusId + "  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3MaxBorLineFHLBTitle as title, 'leftS3MaxBorLineFHLBAmount' as fieldName, leftS3MaxBorLineFHLBAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3LoanColFHLBTitle as title, 'leftS3LoanColFHLBAmount' as fieldName, leftS3LoanColFHLBAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3ExcessLoanColTitle as title, 'leftS3ExcessLoanColAmount' as fieldName, leftS3ExcessLoanColAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3MaxBorCapTitle as title, 'leftS3MaxBorCapAmount' as fieldName, leftS3MaxBorCapAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3ColEncTitle as title, 'leftS3ColEncAmount' as fieldName, leftS3ColEncAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3LoanTotalTitle as title, 'leftS3LoanTotalAmount' as fieldName, leftS3LoanTotalAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS3SubHeaderTitle as title, 'midS1BasicSurplusFHLBAmount' as fieldName, midS1BasicSurplusFHLBAmount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, fieldName, amount as balance, 'Tier2' as section FROM ATLAS_BasicSurplusAdminLoan WHERE BasicSurplusAdminId = " + basicSurplusId + "  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS4BrokDepCapTitle as title, 'leftS4BrokDepCapAmount' as fieldName, leftS4BrokDepCapAmount as balance, 'Tier3' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS4BrokDepBalTitle as title, 'leftS4BrokDepBalAmount' as fieldName, leftS4BrokDepBalAmount as balance, 'Tier3' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS4BrokDepTotalTitle as title, 'leftS4BrokDepTotalAmount' as fieldName, leftS4BrokDepTotalAmount as balance, 'Tier3' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title, ISNULL(fieldName, title) as fieldName, amount as balance, 'Tier3' as section FROM ATLAS_BasicSurplusAdminBrokeredDeposit WHERE BasicSurplusAdminId = " + basicSurplusId + "  ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT leftS4SubHeaderTitle as title, 'midS1BasicSurplusBrokAmount' as fieldName, midS1BasicSurplusBrokAmount as balance, 'Tier3' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value', fieldName + 'MarketValue', MarketValue as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity WHERE fieldName = 'rightS1OtherLiqsCorpSec' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value', fieldName + 'MarketValue', MarketValue as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity WHERE fieldName = 'rightS1OtherLiqsMuniSec' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value', fieldName + 'MarketValue', MarketValue as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity WHERE fieldName = 'rightS1OtherLiqsEqSec' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value', fieldName + 'MarketValue', MarketValue as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity  WHERE fieldName = 'rightS1OtherLiqsOther' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value', title + 'MarketValue' as fieldName, MarketValue as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity  WHERE fieldName IS NULL and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value Available', fieldName + 'MarketValueAvailable',  Available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity WHERE fieldName = 'rightS1OtherLiqsCorpSec' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value Available', fieldName + 'MarketValueAvailable',  Available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity WHERE fieldName = 'rightS1OtherLiqsMuniSec' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value Available', fieldName + 'MarketValueAvailable',  Available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity WHERE fieldName = 'rightS1OtherLiqsEqSec' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value Available', fieldName + 'MarketValueAvailable',  Available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity  WHERE fieldName = 'rightS1OtherLiqsOther' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Market Value Available', title + 'MarketValueAvailable' as fieldName,  Available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminOtherLiquidity  WHERE fieldName IS NULL and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Total Other Investment Market Value', 'rightS1OtherLiqTotalMarketValue' as fieldName, rightS1OtherLiqTotalMarketValue as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Total Other Investment Market Value Available', 'rightS1OtherLiqTotalAvailable' as fieldName, rightS1OtherLiqTotalAvailable as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Outstanding', fieldName + 'Outstanding' as fieldName,  outstanding as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName = 'rightS2SecBorsFedBIC' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Outstanding', fieldName + 'Outstanding' as fieldName,  outstanding as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName = 'rightS2SecBorsOther' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Outstanding', title + 'Outstanding' as fieldName,  outstanding as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName IS NULL and BasicSurplusAdminId = " + basicSurplusId + " ");

            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Amount', fieldName + 'Amount' as fieldName,  line as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName = 'rightS2SecBorsFedBIC' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Amount', fieldName + 'Amount' as fieldName,  line as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName = 'rightS2SecBorsOther' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Amount', title + 'Amount' as fieldName,  line as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName IS NULL and BasicSurplusAdminId = " + basicSurplusId + " ");

            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Available', fieldName + 'Available' as fieldName,  available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName = 'rightS2SecBorsFedBIC' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Available', fieldName + 'Available' as fieldName,  available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName = 'rightS2SecBorsOther' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Available', title + 'Available' as fieldName,  available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminSecuredBorrowing  WHERE fieldName IS NULL and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Available', fieldName + 'Available' as fieldName,  available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminUnsecuredBorrowing  WHERE fieldName = 'rightS3UnsecBorsFedFunds' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Available', title + 'Available' as fieldName,  available as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminUnsecuredBorrowing  WHERE fieldName is NULL and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Outstanding', fieldName + 'Outstanding' as fieldName,  outstanding as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminUnsecuredBorrowing  WHERE fieldName = 'rightS3UnsecBorsFedFunds' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Outstanding', title + 'Outstanding' as fieldName,  outstanding as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminUnsecuredBorrowing  WHERE fieldName is NULL and BasicSurplusAdminId = " + basicSurplusId + " ");

            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Amount', fieldName + 'Amount' as fieldName,  line as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminUnsecuredBorrowing  WHERE fieldName = 'rightS3UnsecBorsFedFunds' and BasicSurplusAdminId = " + basicSurplusId + " ");
            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT title + ' Amount', title + 'Amount' as fieldName,  line as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdminUnsecuredBorrowing  WHERE fieldName is NULL and BasicSurplusAdminId = " + basicSurplusId + " ");


            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Total Borrowing Lines Amount' as title, 'rightS3UnsecBorTotalLine' as fieldName, rightS3UnsecBorTotalLine as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");

            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Total Borrowing Lines Available' as title, 'rightS3UnsecBorTotalAvailable' as fieldName, rightS3UnsecBorTotalAvailable as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");

            sqlQuery.AppendLine(" UNION ALL ");
            sqlQuery.AppendLine(" SELECT 'Total Borrowing Lines Outstanding' as title, 'rightS3UnsecBorTotalOutstanding' as fieldName, rightS3UnsecBorTotalOutstanding as balance, 'OtherLiq' as section FROM ATLAS_BasicSurplusAdmin WHERE id = " + basicSurplusId + " ");


            DataTable vals = Utilities.ExecuteSql(Utilities.BuildInstConnectionString(databaseName), sqlQuery.ToString());

            return vals;
        }

        [HttpGet]
        public Object InventoryOfLiquidityResourcesFundingCapacity(string instName, int basicSurplusId)
        {
            //Connection String
            string conStr = Utilities.BuildInstConnectionString("");

            if (instName == "")
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }


            /*
             * Query that gets the basic surplus based off of id 
             */
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT  ");
            sqlQuery.AppendLine(" 	CASE WHEN leftS3MaxBorLineFHLBType = 'None' THEN -999 ELSE leftS3MaxBorCapAmount END as a  ");
            sqlQuery.AppendLine(" 	,CASE WHEN leftS3MaxBorLineFHLBType = 'None' THEN -999 ELSE leftS3ColEncAmount END as b  ");
            sqlQuery.AppendLine(" 	,leftS4BrokDepCapAmount as c  ");
            sqlQuery.AppendLine(" 	,leftS4BrokDepBalAmount as d  ");
            sqlQuery.AppendLine(" 	,rightS3UnsecBorTotalLine as e  ");
            sqlQuery.AppendLine(" 	,rightS3UnsecBorTotalOutstanding as f  ");
            sqlQuery.AppendLine(" 	,leftS3LoanTotalAmount as one ");
            sqlQuery.AppendLine(" 	,leftS4BrokDepTotalAmount as four ");
            sqlQuery.AppendLine(" 	,rightS3UnsecBorTotalAvailable as seven ");

            sqlQuery.AppendLine("   FROM [" + instName + "].dbo.[ATLAS_BasicSurplusAdmin] WHERE id = " + basicSurplusId + "");

            DataTable bsValues = Utilities.ExecuteSql(conStr, sqlQuery.ToString());


            //Load Up Policy that is associated with that asOfDate
            DataTable polId = Utilities.ExecuteSql(conStr, "  SELECT id, NIISimulationTypeId, asOfDate FROM [" + instName + "].dbo.ATLAS_Policy WHERE asOFDate = (SELECT TOP 1 asOfDate FROM [" + instName + "].dbo.AsOfDateInfo WHERE id = (SELECT informationId FROM [" + instName + "].[dbo].[ATLAS_BasicSurplusAdmin] WHERE id = " + basicSurplusId + " ))");


            string policyId = "-1";
            string asOfDate = "";
            string simType = "";

            if (polId.Rows.Count > 0)
            {
                policyId = polId.Rows[0][0].ToString();
                asOfDate = polId.Rows[0][2].ToString();
                simType = polId.Rows[0][1].ToString();
            }

            DataTable polValues = Utilities.ExecuteSql(conStr, "SELECT shortName, policyLimit, wellCap from ATLAS_LiquidityPolicy where shortName in ('fhlbAssets','fhlbDeposits','fhlbMax','brokeredAsset','brokeredDeposits','brokeredMax','wholeAsset','bsmin','bsminFHLB','bsminBrokered','bsminLiqAssets','bsminOnBalance') AND policyId = " + policyId + "");

            //Load Up Total Asses and liabliies based off of simulation from the policues
            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" 	SUM(case when alb.isAsset = 1 THEN ala.end_bal ELSE 0 END) as assetTotal ");
            sqlQuery.AppendLine(" 	,SUM(case when alb.isAsset <> 1 THEN ala.end_bal ELSE 0 END) as liabTotal ");
            sqlQuery.AppendLine(" 	,SUM(case when alb.Acc_TypeOR in (3,8) THEN ala.end_bal ELSE 0 END) as depositTotal ");
            sqlQuery.AppendLine(" 	,SUM(case when alb.Acc_TypeOR = 6 AND alb.classOr not in (1,2,3) THEN ala.end_bal ELSE 0 END) as borrowinsMinusSecRetail ");
            sqlQuery.AppendLine(" 	FROM   ");
            sqlQuery.AppendLine(" 	Basis_ALA AS ala  ");
            sqlQuery.AppendLine(" 	INNER JOIN  ");
            sqlQuery.AppendLine(" 		(SELECT s.id, aod.asOfDate FROM simulations as s  ");
            sqlQuery.AppendLine(" 		INNER JOIN AsOfDatEInfo as aod  ");
            sqlQuery.AppendLine(" 		ON aod.id = s.informationid	  ");
            sqlQuery.AppendLine(" 		WHERE asOfDate = '" + asOfDate + "' AND simulationTypeid = " + simType + ") as sims  ");
            sqlQuery.AppendLine(" 		ON ala.SimulationId = sims.id   ");
            sqlQuery.AppendLine(" 		AND ala.month = CAST(CAST(DATEPART(year, sims.asOfDate) AS VARCHAR(10)) + '-' + CAST(DATEPART(month, sims.asOfDate) AS VARCHAR(10)) + '-' + '1' AS DATETIME)  ");
            sqlQuery.AppendLine(" 		INNER JOIN Basis_ALB AS alb ON  ");
            sqlQuery.AppendLine(" 		alb.code = ala.code  ");
            sqlQuery.AppendLine(" 		AND alb.SimulationId = ala.SimulationId   ");
            sqlQuery.AppendLine(" 		WHERE alb.cat_Type in (0,1,5)  AND alb.exclude = 0  ");

            DataTable asses = Utilities.ExecuteSql(conStr, sqlQuery.ToString());

            return new
            {
                bsValues = bsValues,
                polValues = polValues,
                assLiabs = asses
            };
        }

        [HttpGet]
        public DataTable InventoryOfLiquidityResourcesBasicSurplusHistoric(int invId, string inst)
        {

            //Connection String
            string conStr = Utilities.BuildInstConnectionString("");

            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine(" SELECT ");
            sqlQuery.AppendLine(" aod.asOfDate  ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusAmount) ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusFHLBAmount) ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusBrokAmount) ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusAmount) / (SUM(ala.end_bal) / 1000)  as tier1  ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusFHLBAmount) / (SUM(ala.end_bal) / 1000) as tier2  ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusBrokAmount) / (SUM(ala.end_bal) / 1000) as tier3  ");

            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusPercent)  as percTier1  ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusFHLBPercent) as percTier2  ");
            sqlQuery.AppendLine(" ,MAX(bsa.midS1BasicSurplusBrokPercent) as percTier3  ");
            sqlQuery.AppendLine(" FROM  ");
            sqlQuery.AppendLine(" (SELECT   ");
            sqlQuery.AppendLine(" 	BasicSurplusAdminId as id  ");
            sqlQuery.AppendLine(" 	,priority  ");
            sqlQuery.AppendLine(" FROM ATLAS_InventoryLiquidityResourcesDateOffsets where InventoryLiquidityResourcesId = " + invId + ") as bss  ");
            sqlQuery.AppendLine(" INNER JOIN [" + inst + "].dbo.Atlas_BasicSurplusAdmin as bsa   ");
            sqlQuery.AppendLine(" ON bsa.id = bss.id  ");
            sqlQuery.AppendLine(" INNER JOIN [" + inst + "].dbo.asOfDateInfo as aod ON  ");
            sqlQuery.AppendLine(" aod.id = bsa.informationId  ");
            sqlQuery.AppendLine(" INNER JOIN [" + inst + "].dbo.Atlas_Policy  as ap ");
            sqlQuery.AppendLine(" ON ap.AsOfDate = aod.asOfDate ");
            sqlQuery.AppendLine(" INNER JOIN [" + inst + "].dbo.Simulations as s ON ");
            sqlQuery.AppendLine(" s.SimulationTypeId = ap.NIISimulationTypeId AND s.InformationId = bsa.InformationId ");
            sqlQuery.AppendLine(" INNER JOIN [" + inst + "].dbo.Basis_Ala as ala ");
            sqlQuery.AppendLine(" ON ala.SimulationId = s.id AND ala.month =  CAST(CAST(DATEPART(year, aod.asOfDate) AS VARCHAR(10)) + '-' + CAST(DATEPART(month, aod.asOfDate) AS VARCHAR(10)) + '-' + '1' AS DATETIME)   ");
            sqlQuery.AppendLine(" INNER JOIN [" + inst + "].dbo.Basis_ALB as alb ");
            sqlQuery.AppendLine(" ON ala.code = alb.code and alb.SimulationId = ala.SimulationId ");
            sqlQuery.AppendLine(" WHERE alb.cat_Type in (0,1,5)  AND alb.exclude = 0  and isasset = 1 ");
            sqlQuery.AppendLine(" GROUP BY aod.asOfDate, bss.Priority ");
            sqlQuery.AppendLine(" ORDER BY bss.priority  ");

            return Utilities.ExecuteSql(conStr, sqlQuery.ToString());

        }


        [HttpGet]
        public DataTable CapitalAnalysisOptions(string instName)
        {
            //Connection String
            string constr = Utilities.BuildInstConnectionString("");

            StringBuilder sqlQuery = new StringBuilder();


            if (instName == "")
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }

            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT DISTINCT ");
            sqlQuery.AppendLine("     [Name] ");
            sqlQuery.AppendLine(" FROM [" + instName + "].dbo.ATLAS_CapitalPolicy as lp ");
            sqlQuery.AppendLine("   WHERE currentRatioFormat = 'perc' or CurrentRatioFormat is null ");
            DataTable ret = Utilities.ExecuteSql(constr, sqlQuery.ToString());
            DataRow noneRow = ret.NewRow();
            noneRow["Name"] = "None";
            ret.Rows.Add(noneRow);
            return ret;
        }


        [HttpGet]
        public object ESRReportSections(string instName)
        {
            DataTable instType = CreditUnionStatus();
            bool isCU = instType.Rows[0]["InstType"].ToString() == "CU";
            if (instName == "")
            {
                instName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            }

            //Connection String
            string constr = ConfigurationManager.ConnectionStrings["DDwConnection"].ToString();

            DataTable Liq = new DataTable();
            DataTable cap = new DataTable();
            DataTable irrLevel1 = new DataTable();
            DataTable irrLevel2 = new DataTable();


            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.AppendLine(" SELECT DISTINCT");
            sqlQuery.AppendLine("   [Name] ");
            sqlQuery.AppendLine(" FROM [" + instName + "].dbo.[ATLAS_LiquidityPolicy] as lp UNION ALL SELECT DISTINCT [Name] FROM [" + instName + "].dbo.Atlas_OtherPolicy ");

            Liq = Utilities.ExecuteSql(constr, sqlQuery.ToString());

            sqlQuery.Clear();
            sqlQuery.AppendLine(" SELECT DISTINCT ");
            sqlQuery.AppendLine("     [Name] ");
            sqlQuery.AppendLine(" FROM [" + instName + "].dbo.ATLAS_CapitalPolicy as lp ");
            sqlQuery.AppendLine("    WHERE currentRatioFormat = 'perc' or CurrentRatioFormat is null UNION ALL SELECT DISTINCT [Name] FROM [" + instName + "].dbo.Atlas_OtherPolicy ");
            cap = Utilities.ExecuteSql(constr, sqlQuery.ToString());

            DataRow newRow;


            //set up irrlevel1 tabkle
            irrLevel1.Columns.Add("Name");

            newRow = irrLevel1.NewRow();
            newRow[0] = "Earnings at Risk Ramp Scenarios";
            irrLevel1.Rows.Add(newRow);


            newRow = irrLevel1.NewRow();
            newRow[0] = "Earnings at Risk Shock Scenarios";
            irrLevel1.Rows.Add(newRow);

            newRow = irrLevel1.NewRow();
            newRow[0] = "Core Funding Utilization";
            irrLevel1.Rows.Add(newRow);

            newRow = irrLevel1.NewRow();
            newRow[0] = isCU ? "Net Economic Value Shock Scenarios" : "Economic Value of Equity Shock Scenarios";
            irrLevel1.Rows.Add(newRow);

            //set up irrlevel2 tabkle
            irrLevel2.Columns.Add("Name");

            newRow = irrLevel2.NewRow();
            newRow[0] = "Year 1 NII % ∆ from Year 1 Base";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = "Year 2 NII % ∆ from Year 1 Base";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = "Year 2 NII % ∆ from Year 2 Base";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = "24 Month % ∆ from Base";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = "Core Funding Utilization";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = isCU ? "Post Shock NEV Ratio" : "Post Shock EVE Ratio";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = isCU ? "NEV Ratio BP ∆ from 0 Shock" : "EVE Ratio BP ∆ from 0 Shock";
            irrLevel2.Rows.Add(newRow);

            newRow = irrLevel2.NewRow();
            newRow[0] = isCU ? "NEV % ∆ from 0 Shock" : "EVE % ∆ from 0 Shock";
            irrLevel2.Rows.Add(newRow);

            return new
            {
                liq = Liq,
                cap = cap,
                irrLevel1 = irrLevel1,
                irrLevel2 = irrLevel2
            };
        }



        //Basis Scenario Types
        [HttpGet]
        public DataTable SimulationsByInstitution(int packageId)
        {
            var instService = new InstitutionService("");

            return new DataTable();
            //Gets All Unique Simulations Ever run In That Database
            //return _utils.ExecuteSql(conStr, "SELECT st.name FROM Simulations as s INNER JOIN SimulationTypes as st on s.SimulationTypeId = st.id GROUP BY st.name");
        }

        //Basis Scenario Types
        [HttpGet]
        public IQueryable<ScenarioType> ScenarioTypes()
        {
            return _contextProvider.Context.ScenarioTypes.AsNoTracking();
        }

        //Web Rate Scenario Types
        [HttpGet]
        public IQueryable<WebRateScenarioType> WebRateScenarios()
        {
            //Get Web Rates Connection String
            return _contextProvider.Context.WebRateScenarioTypes;
        }

        //Web Rates As Of Dates
        [HttpGet]
        public DataTable WebRateAsOfDates()
        {

            //Get Web Rates Connection String
            string conStr = System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString;

            //Get Web Rates Connection String
            DataTable retTable = new DataTable();
            using (SqlConnection sqlCon = new SqlConnection(conStr))
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT DISTINCT asOfDate, CONVERT(VARCHAR(10), asOfDate, 101) as description FROM [Webrates].[dbo].[RateBaseData] ORDER BY asOfdate DESC", sqlCon);
                try
                {
                    sqlCon.Open();
                    retTable.Load(sqlCmd.ExecuteReader());
                    sqlCon.Close();
                }
                catch (Exception ex)
                {
                    sqlCon.Close();
                    throw ex;
                }
            }
            return retTable;
        }

        [HttpGet]
        public string MostRecentWebRateDate()
        {

            //Get Web Rates Connection String
            string conStr = System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString;

            //Get Web Rates Connection String
            DataTable retTable = Utilities.ExecuteSql(System.Configuration.ConfigurationManager.ConnectionStrings["webRates"].ConnectionString, "SELECT CONVERT(VARCHAR(10), MAX(asOfDate), 101) as asOfDate  FROM [Webrates].[dbo].[RateBaseData] ");

            if (retTable.Rows.Count > 0)
            {
                return retTable.Rows[0][0].ToString();
            }
            else
            {
                return "N/A";
            }

        }

        [HttpGet]
        public string Logout()
        {
            FormsAuthentication.SignOut();
            SetDatabaseStatus(false);
            return "Logged out";
        }
    }
}