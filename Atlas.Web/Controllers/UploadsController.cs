﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using HtmlEditor.Models;
using Atlas.Institution.Data;
using Atlas.Institution.Model;

namespace HtmlEditor.Controllers
{
    public class UploadsController : Controller
    {

        private readonly InstitutionContext _gctx = new InstitutionContext();
        //
        // GET: /Uploads/        

        public ActionResult Browse(string CKEditorFuncNum)
        {
            List<Atlas.Institution.Model.File> fileInfoList = _gctx.Files.ToList();

            var model = new FileListingViewModel
            {
                Files = fileInfoList,
                CKEditorFuncNum = CKEditorFuncNum
            };

            return View(model);
        }


        public FileContentResult Files(int id)
        {
            var image = _gctx.Files.Find(id);
            return image != null ? new FileContentResult(image.Data, "image/png") : null;
        }

        public ActionResult Upload(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            MemoryStream target = new MemoryStream();
            upload.InputStream.CopyTo(target);
            byte[] data = target.ToArray();

            var file = new Atlas.Institution.Model.File()
            {
                Name = upload.FileName,
                Data = data
            };

            _gctx.Files.Add(file);
            _gctx.SaveChanges();

            return View();
        }

        private List<FileInformation> GetCurrentFiles()
        {
            string basePath = Server.MapPath("/Uploads");

            List<FileInformation> fileInfoList = new List<FileInformation>();

            string[] files = Directory.GetFiles(basePath);

            files.ToList().ForEach(file =>
            {
                fileInfoList.Add(new FileInformation { FileName = Path.GetFileName(file) });
            });
            return fileInfoList;
        }
    }
}
