using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;

using Atlas.Institution.Model.Atlas;
using Atlas.Web.Services;

namespace Atlas.Web.Controllers
{
    /// <summary>
    /// An endpoint which processes files and performs file-related operations
    /// </summary>
    public class FileController : Controller
    {
        #region Core

        /// <summary>
        /// Converts an image to a PNG and constrains it to a maximum size
        /// </summary>
        /// <param name="base64Blob">The base64-encoded image data</param>
        /// <param name="maxWidthOrHeight">The maximum size, in pixels, of the width or height of the image</param>
        /// <returns>A content with an image/png type whose value is the base64-encoded image data of the resized image</returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult ConvertImageToConstrainedPNG([FromBody] string base64Data, int maxWidthOrHeight)
        {
            return Content(
                ImageProcessingService.ConvertImageToConstrainedPNG(base64Data, maxWidthOrHeight),
                "image/png"
            );
        }

        /// <summary>
        /// Uploads a file and associates it with an owner object
        /// </summary>
        /// <param name="base64">The base64-encoded file data</param>
        /// <param name="name">The name of the uploaded file</param>
        /// <param name="contentType">The content type of the file</param>
        /// <param name="ownerType">The type of object which owns the file</param>
        /// <param name="ownerID">The id of the object which owns the file</param>
        /// <param name="tag">An optional tag to associate with the FileSummary</param>
        /// <returns>A JSON object which represents a summary of the created file</returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult Upload([FromBody] string base64, string name, string contentType, FileOwnerType ownerType, int ownerID, string tag = null)
        {
            return Json(
                new FileProcessingService().Upload(Convert.FromBase64String(base64), name, contentType, ownerType, ownerID, tag)
            );
        }

        /// <summary>
        /// Uploads a word file, converts it to PDF and associates it with an owner object
        /// </summary>
        /// <param name="base64">The base64-encoded file data</param>
        /// <param name="name">The name of the uploaded file</param>
        /// <param name="ownerType">The type of object which owns the file</param>
        /// <param name="ownerID">The id of the object which owns the file</param>
        /// <param name="orientation">The orienation of the document when converted to PDF</param>
        /// <returns>A JSON object which represents a summaries of the created PDF file, one for each orientation</returns>
        /// <remarks>The content type is application/pdf, but the name will be that of the original Word document</remarks>
        [System.Web.Mvc.HttpPost]
        public ActionResult UploadWord([FromBody] string base64, string name, FileOwnerType ownerType, int ownerID, DocumentOrientation orientation)
        {
            return Json(
                new FileProcessingService().UploadWord(Convert.FromBase64String(base64), name, ownerType, ownerID, orientation)
            );
        }

        /// <summary>
        /// Gets file content by FileSummary id
        /// </summary>
        /// <param name="id">The id of the FileSummary to view</param>
        /// <returns>A File ActionResult with the correct data and content type if the FileSummary exists, an HttpNotFound ActionResult otherwise</returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult View(Guid id)
        {
            byte[] fileBlob;
            FileSummary fileSummary = new FileProcessingService().GetWithData(id, out fileBlob);

            if (fileSummary == null)
            {
                return HttpNotFound();
            }

            return File(fileBlob, fileSummary.ContentType);
        }

        /// <summary>
        /// Gets FileSummaries by owner type and owner id
        /// </summary>
        /// <param name="ownerType">The type of the FileSummaries' owner</param>
        /// <param name="ownerID">The id of the FileSummaries' owner</param>
        /// <param name="tag">An optional tag to filter results by</param>
        /// <returns>A JSON object containing an array of matching FileSummaries, which may be empty</returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult GetByOwner(FileOwnerType ownerType, int ownerID, string tag = null)
        {
            IReadOnlyCollection<FileSummary> results = new FileProcessingService().GetByOwner(ownerType, ownerID, tag);
            return Json(results);
        }

        /// <summary>
        /// Gets a FileSummary by id
        /// </summary>
        /// <param name="id">The id of the FileSummary to get</param>
        /// <returns>A JSON object representing the FileSummary if it exists, an HttpNotFound ActionResult otherwise</returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult Get(Guid id)
        {
            FileSummary result = new FileProcessingService().Get(id);

            return ReferenceEquals(null, result)
                ? (ActionResult) HttpNotFound()
                : Json(result);
        }

        /// <summary>
        /// Deletes tje FileSummary with the given id
        /// </summary>
        /// <param name="id">The id of the FileSummary to delete</param>
        /// <returns>No content</returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult Delete(Guid id)
        {
            new FileProcessingService().Delete(id);
            return new EmptyResult();
        }

        /// <summary>
        /// Deletes all FileSummaries associated with a given owner type and owner id
        /// </summary>
        /// <param name="ownerType">The type of the file summaries' owner</param>
        /// <param name="ownerID">The id of the file summaries' owner</param>
        /// <returns>No content</returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult DeleteByOwner(FileOwnerType ownerType, int ownerID)
        {
            new FileProcessingService().Delete(ownerType, ownerID);
            return new EmptyResult();
        }

        #endregion Core

        #region File Templates

        [System.Web.Mvc.HttpGet]
        public ActionResult GetFileViewTemplateByID(Guid fileID)
        {
            FileProcessingService fps = new FileProcessingService();

            return Content(getFileViewTemplate(fps.Get(fileID), null), "text/html");
        }

        [System.Web.Mvc.HttpGet]
        public ActionResult GetFileViewTemplateByReportID(int reportID, DocumentOrientation? orientation)
        {
            FileProcessingService fps = new FileProcessingService();

            IReadOnlyCollection<FileSummary> files = fps.GetByOwner(FileOwnerType.Report, reportID);

            string content = getFileViewTemplate(
                files.FirstOrDefault(f => !orientation.HasValue || string.Equals(f.Tag, orientation.Value.ToString(), StringComparison.InvariantCultureIgnoreCase)),
                orientation
            );

            return Content(content, "text/html");
        }

        private string getFileViewTemplate(FileSummary fileSummary, DocumentOrientation? orientation)
        {
            if (ReferenceEquals(null, fileSummary)) return string.Empty;

            DocumentOrientation localOrientation;

            if(! orientation.HasValue && Enum.TryParse(fileSummary.Tag, out localOrientation))
            {
                orientation = orientation ?? localOrientation;
            }

            string url = "/File/View/" + fileSummary.FileID;

            StringBuilder sb = new StringBuilder();

            sb.Append("<div data-orientation=\"");
            sb.Append(orientation.HasValue ? orientation.Value.ToString() : "both");
            sb.AppendLine("\">");
            sb.Append("<h1>");
            sb.Append(fileSummary.UploadName);
            sb.AppendLine("</h1>");
            sb.Append("<div><a target=\"_blank\" href=\"");
            sb.Append(url);
            sb.AppendLine("\"> Click here to view file in a new tab</a></div>");
            sb.Append("<div><iframe style=\"width:100%; height:500px;\" src=\"");
            sb.Append(url);
            sb.AppendLine("\"></iframe></div>");
            sb.AppendLine("</div>");

            return sb.ToString();
        }

        #endregion File Templates
    }
}