﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;

using Atlas.Web.Services;
using Atlas.Institution.Model;

using Spire.Pdf;
using Spire.Pdf.Graphics;
using Spire.Pdf.AutomaticFields;

namespace Atlas.Web.Controllers
{
    //string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
    public class PackageController : Controller
    {
        private GlobalContextProvider _contextProvider = new GlobalContextProvider("");

        public ActionResult Pdf(int id = 0)
        {
            Package package = _contextProvider.Context.Packages.Include("Reports").Where(s => s.Id == id).FirstOrDefault();

            string dbName = Utilities.GetAtlasUserData().Rows[0]["databaseName"].ToString();
            string packageId = package.Id.ToString();
            string folderPath = FileProcessingService.GetBasePath() + dbName + "\\\\" + packageId + "\\\\";

            FileInfo fi = new FileInfo(folderPath + "packageExport.pdf");

            if (!fi.Exists) throw new FileNotFoundException("Cannot find the PDF export file " + fi.FullName);

            using (FileStream fs = fi.OpenRead())
            {
                byte[] contents = new byte[fs.Length];
                fs.Read(contents, 0, contents.Length);

                Response.AppendHeader("Content-Disposition", "inline; filename=\"" + package.Name + ".pdf\"");
                return File(contents, "application/pdf");
            }
        }

        private void SetDocumentTemplate(PdfDocument doc, SizeF pageSize, PdfMargins margin)
        {
            PdfPageTemplateElement leftSpace = new PdfPageTemplateElement(margin.Left, pageSize.Height);
            doc.Template.Left = leftSpace;

            PdfPageTemplateElement topSpace = new PdfPageTemplateElement(pageSize.Width, margin.Top);
            topSpace.Foreground = true;
            doc.Template.Top = topSpace;

            //draw header label
            PdfTrueTypeFont font1 = new PdfTrueTypeFont(new Font("Arial", 20f));
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Left);
            String label = "This is header for test";
            float y = 0;
            float x = 0;
            topSpace.Graphics.DrawString(label, font1, PdfBrushes.PaleVioletRed, x, y, format);

            label = "Hello Word";
            SizeF size1 = font1.MeasureString(label, format);
            y = y + size1.Height + 3;
            PdfTrueTypeFont font2 = new PdfTrueTypeFont(new Font("Arial", 10f));
            SizeF size2 = font2.MeasureString("Hello Word ", format);
            PdfTrueTypeFont font3 = new PdfTrueTypeFont(new Font("Arial", 13f));
            SizeF size3 = font3.MeasureString("Hello Word", format);
            float y1 = y + size3.Height - size2.Height;
            topSpace.Graphics.DrawString("Hello Word ", font2, PdfBrushes.PaleVioletRed, x, y1, format);

            x = x + size2.Width;
            topSpace.Graphics.DrawString("Hello Word", font3, PdfBrushes.Black, x, y, format);

            x = x + size3.Width;
            topSpace.Graphics.DrawString(" Hello Word", font2, PdfBrushes.PaleVioletRed, x, y1, format);

            PdfPageTemplateElement rightSpace = new PdfPageTemplateElement(margin.Right, pageSize.Height);
            doc.Template.Right = rightSpace;

            PdfPageTemplateElement bottomSpace = new PdfPageTemplateElement(pageSize.Width, margin.Bottom);
            bottomSpace.Foreground = true;
            doc.Template.Bottom = bottomSpace;
        }

        static void AddFooter(PdfDocument doc, PdfMargins margin, string footerString)
        {
            SizeF pageSize = doc.Pages[0].Size;

            //Create a PdfPageTemplateElement object that will be
            //used as footer space
            PdfPageTemplateElement footerSpace = new PdfPageTemplateElement(pageSize.Width, margin.Bottom);
            footerSpace.Foreground = true;
            doc.Template.Bottom = footerSpace;

            //Draw text at the center of footer space
            PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("Arial", 9f, FontStyle.Bold), true);
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Center);
            float x = pageSize.Width / 2;
            float y = 15f;

            //Create page number automatic field
            PdfPageNumberField number = new PdfPageNumberField();

            //Add the fields in composite field
            PdfCompositeField compositeField = new PdfCompositeField(font, PdfBrushes.Black, footerString, number);
            //Align string of "Page {0} of {1}" to center 
            compositeField.StringFormat = new PdfStringFormat(PdfTextAlignment.Center, PdfVerticalAlignment.Middle);
            compositeField.Bounds = footerSpace.Bounds;
            //Draw composite field at footer space
            compositeField.Draw(footerSpace.Graphics);
        }

        /*     private void Footer(PdfDocument document, Package package)
             {
                 //create the document footer
                 document.CreateFooterCanvas(50);

                 float footerHeight = document.Footer.Height;
                 float footerWidth = document.Footer.Width;

                 // add page numbering
                 Font pageNumberingFont = new Font(new FontFamily(GenericFontFamilies.SansSerif), 12, GraphicsUnit.Point);
                 //pageNumberingFont.Mea
                 PdfText pageNumberingText = new PdfText(5, 5, package.Name + " - Page {CrtPage}", pageNumberingFont);
                 pageNumberingText.HorizontalAlign = PdfTextHAlign.Right;
                 pageNumberingText.EmbedSystemFont = true;
                 pageNumberingText.ForeColor = Color.Black;
                 document.Footer.Layout(pageNumberingText);

                 // create a border for footer
                 PdfLine borderLine = new PdfLine(new PointF(0, 3), new PointF(footerWidth, 3));
                 borderLine.LineStyle.LineWidth = 0.5f;
                 borderLine.ForeColor = Color.Black;
                 document.Footer.Layout(borderLine);

                 foreach (HiQPdf.PdfPage page in document.Pages)
                 {
                     page.DisplayFooter = true;
                 }
             }*/
    }
}
