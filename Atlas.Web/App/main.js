﻿requirejs.config({
    paths: {
        'text': '../Scripts/text',
        'durandal': '../Scripts/durandal',
        'plugins': '../Scripts/durandal/plugins',
        'transitions': '../Scripts/durandal/transitions',
        'scripts': '../Scripts'
    }
});

define('jquery', [], function () { return jQuery; });
define('jquery.ui', [], function () { return; });
define('knockout', [], function () { return ko; });

define(
    [
        'durandal/app',
        'durandal/viewLocator',
        'durandal/system',
        'scripts/knockout-ckeditor',
        'scripts/knockout-sortable',
        'scripts/knockout-bootstrap-datepicker',
        'scripts/knockout-chart',
        'scripts/knockout-readonly'
    ],
    function (app, viewLocator, system) {
        //test
        //>>excludeStart("build", true);
        system.debug(true);
        //>>excludeEnd("build");

        //Internally, Durandal uses jQuery's promise implementation
        //Breeze uses Q's promise system
        //Q's promise system is more correct/full featured
        //This code plugs Q's promise mechanism into Durandal 
        system.defer = function (action) {
            var deferred = Q.defer();
            action.call(deferred, deferred);
            var promise = deferred.promise;
            deferred.promise = function () {
                return promise;
            };
            return deferred;
        };

        app.title = 'Atlas';

        //specify which plugins to install and their configuration
        app.configurePlugins({
            router: true,
            dialog: true,
            widget: {
                kinds: ['expander']
            }
        });

        app.start().then(function () {
            //toastr.options.positionClass = 'toast-bottom-right';
           // toastr.options.backgroundpositionClass = 'toast-bottom-right';

            viewLocator.useConvention();

            //Show the app by setting the root view model for our application with a transition.
            app.setRoot('viewmodels/shell', 'entrance');
        });
    });