﻿define(['durandal/system', 'services/model', 'config', 'services/logger', 'services/globalcontext'],
    function (system, model, config, logger, globalContext) {
        var calcRowData = function (rowObs, hasExisting, existingMatch, projObj, yearFromStartDays, startToEndDays) {
            var calcHolder = 0;
            rowObs.proj_AvgBal(projObj.avgBal);
            rowObs.proj_IncExp(projObj.incExp);

            if (hasExisting) {
                //plug in existing data
                rowObs.proj_AvgBal_Adj(existingMatch.proj_AvgBal_Adj());
                rowObs.proj_IncExp_Adj(existingMatch.proj_IncExp_Adj());
                rowObs.actual_AvgBal(existingMatch.actual_AvgBal());
                rowObs.actual_IncExp(existingMatch.actual_IncExp());
            }

            //proj_AvgBal
            calcHolder = rowObs.proj_AvgBal() + rowObs.proj_AvgBal_Adj();
            rowObs.proj_AvgBal(calcHolder);

            //proj_IncExp
            calcHolder = rowObs.proj_IncExp() + rowObs.proj_IncExp_Adj();
            rowObs.proj_IncExp(calcHolder);

            //proj_AvgRate
            if (rowObs.proj_AvgBal() == 0) {
                calcHolder = 0;
            } else {
                calcHolder = (((rowObs.proj_IncExp() / startToEndDays) * yearFromStartDays) / rowObs.proj_AvgBal()) * 100;
            }
            rowObs.proj_AvgRate(calcHolder);

            //actual_AvgRate
            if (rowObs.actual_AvgBal() == 0) {
                calcHolder = 0;
            } else {
                calcHolder = (((rowObs.actual_IncExp() / startToEndDays) * yearFromStartDays) / rowObs.actual_AvgBal()) * 100;
            }
            rowObs.actual_AvgRate(calcHolder);

            //variance calculations
            if (yearFromStartDays == 0) {
                rowObs.variance_ValDueToRate(0);
                rowObs.variance_ValDueToVol(0);
            } else {
                calcHolder = (((rowObs.proj_AvgRate() - rowObs.actual_AvgRate()) / 100) * rowObs.actual_AvgBal() / yearFromStartDays) * startToEndDays;
                rowObs.variance_ValDueToRate(calcHolder);

                calcHolder = (((rowObs.proj_AvgBal() - rowObs.actual_AvgBal()) * (rowObs.proj_AvgRate() / 100)) / yearFromStartDays) * startToEndDays;
                rowObs.variance_ValDueToVol(calcHolder);
            }
            rowObs.variance_Val((rowObs.proj_IncExp() - rowObs.actual_IncExp()));

            if (rowObs.actual_IncExp() == 0) {
                rowObs.variance_Perc(0);
            } else {
                rowObs.variance_Perc((rowObs.variance_Val() / rowObs.actual_IncExp()) * 100);
            }
        };

        var updateTotalLines = function (lbLookbackGroup, yearFromStartDays, startToEndDays) {
            var assetTotals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var liabTotals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var sectionTotals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var niiTotals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            var inSH = false;
            var totalAssetsObj, totalLiabsObj, niiObj;

            var orderedData = lbLookbackGroup.lBLookbackData().sort(function (a, b) {
                return a.priority() - b.priority();
            });

            for (var d = 0; d < orderedData.length; d++) {
                var thisData = orderedData[d];
                var rowType = thisData.rowType();

                if (rowType < 0) {//section totals, asset/liab total, nii total
                    if (thisData.isTotalRow() && !thisData.hideFromAdmin()) {
                        if (thisData.isAsset()) {
                            totalAssetsObj = thisData;
                            if (lbLookbackGroup.rateVol()) {
                                totalAssetsObj.title("Total Assets/Interest Income");
                            } else {
                                totalAssetsObj.title("Total Interest Income");
                            }
                        } else {
                            totalLiabsObj = thisData;
                            if (lbLookbackGroup.rateVol()) {
                                totalLiabsObj.title("Total Liab & Equity/Int Exp");
                            } else {
                                totalLiabsObj.title("Total Interest Expense");
                            }
                        }
                    }
                    else if (thisData.hideFromAdmin()) {
                        niiObj = thisData;
                        if (lbLookbackGroup.rateVol()) {
                            niiObj.title("Total Assets/Net Interest Income");
                        } else {
                            niiObj.title("Net Interest Income");
                        }
                    }
                    else if (inSH) {
                        populateDataObjFromArr(thisData, sectionTotals, yearFromStartDays, startToEndDays);
                        inSH = false;
                        sectionTotals = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    }
                } else if (rowType == 0) {//hit the section label row
                    inSH = true;
                    continue;
                } else if (rowType > 0) {//1 = sub, 2 = master

                    //Asset / Liability totals
                    if (thisData.isAsset()) {
                        putValsInArr(assetTotals, thisData, 1, false);
                    } else {
                        putValsInArr(liabTotals, thisData, 1, false);
                    }

                    //section totals
                    if (inSH && thisData.rowType() == 1) {
                        putValsInArr(sectionTotals, thisData, 1, false);
                    }
                }
            }

            populateDataObjFromArr(totalAssetsObj, assetTotals, yearFromStartDays, startToEndDays);
            populateDataObjFromArr(totalLiabsObj, liabTotals, yearFromStartDays, startToEndDays);

            niiTotals[0] = assetTotals[0];
            niiTotals[5] = assetTotals[5];
            //niiTotals[1] and niiTotals[4] are blank
            for (var n = 2; n <= 7; n++) {
                if (n != 4 && n != 5) {
                    niiTotals[n] = assetTotals[n] - liabTotals[n];
                }
            }
            niiTotals[10] = niiTotals[3] - niiTotals[7];
            populateDataObjFromArr(niiObj, niiTotals, yearFromStartDays, startToEndDays);

        };

        var populateDataObjFromArr = function (dataObj, arr, yearFromStartDays, startToEndDays) {
            //do some calculations with the array first
            var varToRate = 0, varToVol = 0, varVal = 0;
            arr[2] = arr[0] == 0.0 ? 0 : ((((arr[3] / startToEndDays) * yearFromStartDays) / arr[0]) * 100);
            arr[6] = arr[5] == 0.0 ? 0 : ((((arr[7] / startToEndDays) * yearFromStartDays) / arr[5]) * 100);
            arr[11] = arr[7] == 0 ? 0 : (arr[10] / arr[7]) * 100;

            if (yearFromStartDays != 0) {
                //varToRate = ((((arr[6] - arr[2]) / 100) * arr[5]) / yearFromStartDays) * startToEndDays;
                varToRate = ((((arr[2] - arr[6]) / 100) * arr[5]) / yearFromStartDays) * startToEndDays;
                //varToVol = yearFromStartDays == 0 ? 0 : (((arr[5] - arr[0]) * (arr[2] / 100)) / yearFromStartDays) * startToEndDays;
                varToVol = yearFromStartDays == 0 ? 0 : (((arr[0] - arr[5]) * (arr[2] / 100)) / yearFromStartDays) * startToEndDays;

            } else {
                varToRate = 0;
                varToVol = 0;

            }
            arr[8] = varToRate;
            arr[9] = varToVol;

            //stick the values where they belong
            dataObj.proj_AvgBal(arr[0]);
            dataObj.proj_AvgBal_Adj(arr[1]);
            dataObj.proj_AvgRate(arr[2]);
            dataObj.proj_IncExp(arr[3]);
            dataObj.proj_IncExp_Adj(arr[4]);
            dataObj.actual_AvgBal(arr[5]);
            dataObj.actual_AvgRate(arr[6]);
            dataObj.actual_IncExp(arr[7]);
            dataObj.variance_ValDueToRate(arr[8]);
            dataObj.variance_ValDueToVol(arr[9]);
            dataObj.variance_Val(arr[10]);
            dataObj.variance_Perc(arr[11]);
            return;
        };

        var putValsInArr = function (totalArr, item, factor, isNet) {
            var abal, incEx;
            if (isNet && factor == -1) {
                pbal = 0;
                abal = 0;
            } else {
                pbal = item.proj_AvgBal();
                abal = item.actual_AvgBal();
            }

            totalArr[0] = (totalArr[0] + (pbal * factor));
            totalArr[1] += item.proj_AvgBal_Adj() * factor;
            totalArr[2] += (!isNet ? item.proj_AvgRate() : (((item.proj_AvgBal()) * factor) * (item.proj_AvgRate()) / 100));
            totalArr[3] += item.proj_IncExp() * factor;
            totalArr[4] += item.proj_IncExp_Adj() * factor;
            totalArr[5] += (abal * factor);
            totalArr[6] += (!isNet ? item.actual_AvgRate() : (((item.actual_AvgBal()) * factor) * (item.actual_AvgRate()) / 100));
            totalArr[7] += item.actual_IncExp() * factor;
            totalArr[8] += item.variance_ValDueToRate() * factor;
            totalArr[9] += item.variance_ValDueToVol() * factor;
            totalArr[10] += item.variance_Val() * factor;
            totalArr[11] += item.variance_Perc() * factor;
            return;
        };
        var createDataForLookbackGroup = function (lookback, isQuietSave, projHashObs, changeFromLayout) {
            var group = lookback().lookbackGroups()[0];
            var groupLayoutObs = ko.observable();
            var changedLayout = group.entityAspect.originalValues.layoutId != undefined || changeFromLayout;
            var firstLoad = group.lBLookbackData().length == 0;
            var thisType = group.lBCustomType();
            var sOffset = thisType.startDateOffset();
            var eOffset = thisType.endDateOffset();

            var projObs = ko.observable();
            var existingDataObs = ko.observable();
            var startingObject = ko.observable();
            var endingObject = ko.observable();
            var startToEndDays = 0;
            var yearFromStartDays = 0;

            //if just updating we DO still need to get the new projections to take simulation, scenario, tax equiv into account
            return globalContext.getLookbackGroupDataProjections(group.layoutId(), sOffset, eOffset, lookback().simulationTypeId(), lookback().scenarioTypeId(), group.taxEquivalent(), moment.utc(lookback().asOfDate()).format("MM/DD/YYYY"), projObs).then(function () {
                return globalContext.getLookbackGroupDataById(group.id(), existingDataObs).fin(function () {
                    return globalContext.getSimulationScenarioInfo(moment.utc(lookback().asOfDate()).format("MM/DD/YYYY"), sOffset, lookback().simulationTypeId(), lookback().scenarioTypeId(), startingObject).fin(function () {
                        return globalContext.getSimulationScenarioInfo(moment.utc(lookback().asOfDate()).format("MM/DD/YYYY"), eOffset, lookback().simulationTypeId(), lookback().scenarioTypeId(), endingObject).fin(function () {

                            //Clear old records from the group - mark as deleted and also physically eject from array
                            //WE SHOULD ONLY DO THIS IF THE LAYOUT GETS CHANGED

                            if (changedLayout) {
                                for (var i = group.lBLookbackData().length - 1; i >= 0; i--) {
                                    group.lBLookbackData()[i].entityAspect.setDeleted();
                                }
                                group.lBLookbackData.removeAll();
                            }
                            var totalAssetsDataObj;
                            var totalLiabsDataObj;
                            var netDataObj;
                            var existingTotalRows = group.lBLookbackData().filter(function (d) {
                                return d.rowType() == -1 && d.isTotalRow() && d.isCalculatedRow() && d.lBCustomLayoutRowItemId() == -1;
                            });
                            for (var i = 0; i < existingTotalRows.length; i++) {
                                if ((group.rateVol() && existingTotalRows[i].title() == "Total Assets/Interest Income") || (!group.rateVol() && existingTotalRows[i].title() == "Total Interest Income")) {
                                    totalAssetsDataObj = existingTotalRows[i];
                                } else if ((group.rateVol() && existingTotalRows[i].title() == "Total Liab & Equity/Int Exp") || (!group.rateVol() && existingTotalRows[i].title() == "Total Interest Expense")) {
                                    totalLiabsDataObj = existingTotalRows[i];
                                } else if ((group.rateVol() && existingTotalRows[i].title() == "Total Assets/Net Interest Income") || (!group.rateVol() && existingTotalRows[i].title() == "Net Interest Income")) {
                                    netDataObj = existingTotalRows[i];
                                }
                            }
                            if (totalAssetsDataObj == null || totalAssetsDataObj == undefined) {
                                totalAssetsDataObj = globalContext.createLBLookbackData();
                                totalAssetsDataObj.rowType(-1);
                                if (group.rateVol()) {
                                    totalAssetsDataObj.title("Total Assets/Interest Income");
                                } else {
                                    totalAssetsDataObj.title("Total Interest Income");
                                }

                                totalAssetsDataObj.isAsset(true);
                                totalAssetsDataObj.isSectionHeader(false);
                                totalAssetsDataObj.isTotalRow(true);
                                totalAssetsDataObj.hideFromAdmin(false);
                                totalAssetsDataObj.isCalculatedRow(true);
                                totalAssetsDataObj.lBLookbackGroupId(group.id());
                                totalAssetsDataObj.lBCustomLayoutRowItemId(-1);
                            }
                            if (totalLiabsDataObj == null || totalLiabsDataObj == undefined) {
                                totalLiabsDataObj = globalContext.createLBLookbackData();
                                totalLiabsDataObj.rowType(-1);
                                if (group.rateVol()) {
                                    totalLiabsDataObj.title("Total Liab & Equity/Int Exp");
                                } else {
                                    totalLiabsDataObj.title("Total Interest Expense");
                                }

                                totalLiabsDataObj.isAsset(false);
                                totalLiabsDataObj.isSectionHeader(false);
                                totalLiabsDataObj.isTotalRow(true);
                                totalLiabsDataObj.hideFromAdmin(false);
                                totalLiabsDataObj.isCalculatedRow(true);
                                totalLiabsDataObj.lBLookbackGroupId(group.id());
                                totalLiabsDataObj.lBCustomLayoutRowItemId(-1);
                            }
                            if (netDataObj == null || netDataObj == undefined) {
                                var netDataObj = globalContext.createLBLookbackData();
                                if (group.rateVol()) {
                                    netDataObj.title("Total Assets/Net Interest Income");
                                } else {
                                    netDataObj.title("Net Interest Income");
                                }
                                
                                netDataObj.rowType(-1);
                                netDataObj.isSectionHeader(false);
                                netDataObj.isTotalRow(true);
                                netDataObj.hideFromAdmin(true);
                                netDataObj.isCalculatedRow(true);
                                netDataObj.lBLookbackGroupId(group.id());
                                netDataObj.lBCustomLayoutRowItemId(-1);
                            }

                            var inSectionHeader = false;

                            var sectionTotalTitle = "";
                            var rowTypeIndexer = ["Section Header", "Sub", "Master"];

                            //populate this observable so we can use in the lookack config in anopther function
                            if (projHashObs != null && projHashObs != undefined) {
                                for (var p = 0; p < projObs()[0].retTable.length; p++) {
                                    var dRow = projObs()[0].retTable[p];
                                    if (projHashObs()[dRow.title] == undefined) {
                                        projHashObs()[dRow.title] = {
                                            incExp: dRow.incExp,
                                            avgBal: dRow.avgBal
                                        }
                                    }
                                }
                            }


                            var ms = moment(endingObject().asOfDate).diff(moment(startingObject().asOfDate));
                            var diffInDays = moment(endingObject().asOfDate).diff(moment(startingObject().asOfDate), "days");
                            var d = moment.duration(ms);
                            //startToEndDays = Math.floor(d.asDays());

                            startToEndDays = diffInDays;

                            //should only happen if it can't find data for a data / simulation / scenario / offset combo
                            if (isNaN(startToEndDays)) {
                                startToEndDays = 1;
                            }

                            //ms = moment(startingObject().asOfDate).add(1, 'year').diff(moment(startingObject().asOfDate));
                            diffInDays = moment(startingObject().asOfDate).add(1, 'year').diff(moment(startingObject().asOfDate), "days");
                            d = moment.duration(ms);
                            //yearFromStartDays = Math.floor(d.asDays());
                            yearFromStartDays = diffInDays;
                            if (isNaN(yearFromStartDays)) {
                                yearFromStartDays = 1;
                            }


                            var priorityOffset = 0;
                            var hasExisting = false;

                            //update the group date info
                            //lots of null and NaN checks 
                            if (startingObject().asOfDate == "") {
                                //moment.utc(lookback().asOfDate()).format("MM/DD/YYYY")
                                logger.logError("Could not find data for SimulationType " + lookback().simulationTypeId() + ", ScenarioType " + lookback().scenarioTypeId() + ", AsOfDate " + moment.utc(lookback().asOfDate()).format("MM/DD/YYYY") + ", Offset " + sOffset, null, null, true);
                                group.startDate(moment.utc(lookback().asOfDate()).format("MM/DD/YYYY"));

                            } else {
                                group.startDate(startingObject().asOfDate);
                            }
                            if (endingObject().asOfDate == "") {
                                logger.logError("Could not find data for SimulationType " + lookback().simulationTypeId() + ", ScenarioType " + lookback().scenarioTypeId() + ", AsOfDate " + moment.utc(lookback().asOfDate()).format("MM/DD/YYYY") + ", Offset " + eOffset, null, null, true);
                                group.endDate(moment.utc(lookback().asOfDate()).format("MM/DD/YYYY"));
                            } else {
                                group.endDate(endingObject().asOfDate);
                            }

                            group.startToEndDays(startToEndDays);
                            group.yearFromStartDays(yearFromStartDays);


                            for (var p = 0; p < projObs()[0].retTable.length; p++) {
                                var dataObj;
                                var projection = projObs()[0].retTable[p];
                                var existingMatch = group.lBLookbackData().filter(function (item, index, arr) {
                                    var sameId = item.lBCustomLayoutRowItemId() == projection.id;
                                    return sameId;
                                    //return item.title() == projection.title;
                                });
                                hasExisting = group.lBLookbackData().length > 0 && existingMatch.length > 0

                                //if just updating, we shouldn't have to make new LBLookbackData objects...
                                if (!hasExisting) {
                                    dataObj = globalContext.createLBLookbackData();

                                    dataObj.lBCustomLayoutRowItemId(projection.id);

                                    dataObj.isAsset(projection.isAsset);
                                    dataObj.title(projection.title);
                                    dataObj.rowType(rowTypeIndexer.indexOf(projection.rowType));
                                    dataObj.lBLookbackGroupId(group.id());
                                } else {
                                    dataObj = existingMatch[0];
                                    calcRowData(dataObj, hasExisting, existingMatch[0], projection, yearFromStartDays, startToEndDays);
                                    continue;
                                }


                                //do some checking and clearing
                                if (projection.rowType === "Section Header") {
                                    inSectionHeader = true;
                                    sectionTotalTitle = "Total " + projection.title;
                                    dataObj.isSectionHeader(true);
                                    dataObj.priority(p + priorityOffset);
                                    group.lBLookbackData.push(dataObj);

                                } else {
                                    var existingMatch = existingDataObs().filter(function (item, index, arr) {
                                        var sameId = item.lBCustomLayoutRowItemId() == projection.id;
                                        return sameId;
                                        //return item.title() == projection.title;
                                    });
                                    hasExisting = existingDataObs().length > 0 && existingMatch.length > 0;

                                    calcRowData(dataObj, hasExisting, existingMatch[0], projection, yearFromStartDays, startToEndDays);

                                    //add the normal data row
                                    dataObj.priority(p + priorityOffset);
                                    group.lBLookbackData.push(dataObj);

                                    //add whatever calculated rows we need
                                    if ((p == projObs()[0].retTable.length - 1 && inSectionHeader) ||
                                        (p < projObs()[0].retTable.length - 1 && inSectionHeader && (projObs()[0].retTable[p + 1].rowType == "Section Header" || projObs()[0].retTable[p + 1].rowType == "Master"))) {

                                        priorityOffset++;
                                        var sectionHeaderDataObj = globalContext.createLBLookbackData();

                                        sectionHeaderDataObj.title(sectionTotalTitle);
                                        sectionHeaderDataObj.isAsset(projObs()[0].retTable[p].isAsset);
                                        sectionHeaderDataObj.rowType(-1);
                                        sectionHeaderDataObj.isSectionHeader(false);
                                        sectionHeaderDataObj.isTotalRow(false);
                                        sectionHeaderDataObj.hideFromAdmin(false);
                                        sectionHeaderDataObj.isCalculatedRow(true);

                                        sectionHeaderDataObj.priority(p + priorityOffset);
                                        group.lBLookbackData.push(sectionHeaderDataObj);

                                        sectionTotalTitle = "";
                                        inSectionHeader = false;
                                    }

                                    if (p >= projObs()[0].retTable.length - 1 || dataObj.isAsset() && !projObs()[0].retTable[p + 1].isAsset) {
                                        priorityOffset++;
                                        if (dataObj.isAsset()) {
                                            totalAssetsDataObj.priority(p + priorityOffset);
                                            group.lBLookbackData.push(totalAssetsDataObj);
                                        } else {
                                            totalLiabsDataObj.priority(p + priorityOffset);
                                            group.lBLookbackData.push(totalLiabsDataObj);
                                        }
                                    }
                                }
                            }
                            if (changedLayout || firstLoad) {
                                priorityOffset++;
                                netDataObj.priority(projObs()[0].retTable.length + priorityOffset);
                                group.lBLookbackData.push(netDataObj);
                            }


                            updateTotalLines(group, yearFromStartDays, startToEndDays);
                        });
                    });
                });
            });
        };

        var datacontext = {
            createDataForLookbackGroup: createDataForLookbackGroup,
            calcRowData: calcRowData,
            updateTotalLines: updateTotalLines
        };

        return datacontext;
    });