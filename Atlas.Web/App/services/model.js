﻿define([], function () {

    var entityNames = {
        institution: 'Institution',
        information: 'Information',
        simulation: 'Simulation',
        scenarioType: "ScenarioType",

        incomeGraphScenario: "IncomeGraphScenario",
        simulationCompareScenarioType: "SimulationCompareScenarioType",

        oneChart: 'OneChart',
        twoIncomeGraph: 'TwoIncomeGraph',
        simulationCompare: 'SimulationCompare',
        balanceSheetCompare: 'BalanceSheetCompare',
        balanceSheetMix: 'BalanceSheetMix',
        loanCapFloor: 'LoanCapFloor',
        staticGap: 'StaticGap',
        fundingMatrix: 'FundingMatrix',
        cashflowReport: 'CashflowReport',
        timeDepositMigration: 'TimeDepositMigration',
        coreFundingUtilization: 'CoreFundingUtilization',
        historicalBalanceSheetNII: 'HistoricalBalanceSheetNII',
        simulationSummary: 'SimulationSummary',
        prepaymentDetails: 'PrepaymentDetails',
        summaryOfResults: 'SummaryOfResults',
        marginalCostOfFunds: 'MarginalCostOfFunds'
    };


    //IMPORTANT name property needs to be the exact class name for the report config
    var _reportSections = [
        {
            name: "Most Frequent",
            subSections: [
                {
                    name: "",
                    reports: [
                        { name: 'SimCompare', dispName: 'Sim Compare' },
                        { name: 'Document', dispName: 'Document' },
                        { name: 'BasicSurplusReport', dispName: 'Basic Surplus Report' },
                        { name: 'InventoryLiquidityResources', dispName: 'Inventory of Liquidity Resources' },
                        { name: 'OneChart', dispName: 'One Chart' },
                        { name: 'TwoChart', dispName: 'Two Chart' },
                        { name: 'Eve', dispName: 'EVE/NEV' },
                        { name: 'MarginalCostOfFunds', dispName: 'Marginal Cost of Funds' }
                    ]
                }
            ]
        },
        {
            name: "ALCO",
            subSections: [
                {
                    name: "Executive Summary",
                    reports: [
                        { name: 'ExecutiveRiskSummary', dispName: 'Executive Risk Summary' },
                        { name: 'BalanceSheetMix', dispName: 'Balance Sheet Mix' },
                        { name: 'HistoricalBalanceSheetNII', dispName: 'Historical Balance Sheet & NII' },
                        { name: 'InventoryLiquidityResources', dispName: 'Inventory of Liquidity Resources' },
                        { name: 'CapitalAnalysis', dispName: 'Capital Analysis' }
                    ]
                },
                {
                    name: "Liquidity and Cash Flow",
                    reports: [
                        { name: 'BasicSurplusReport', dispName: 'Basic Surplus Report' },
                        { name: 'LiquidityProjection', dispName: 'Liquidity Projection' },
                        { name: 'CashflowReport', dispName: 'Cash Flow' },
                        { name: 'TimeDepositMigration', dispName: 'Time Deposit Migration' },
                        { name: 'LiabilityPricingAnalysis', dispName: ' Deposit Pricing and Activity Analysis' }
                    ]
                },
                {
                    name: "Interest Rate Risk",
                    reports: [
                        { name: 'SimCompare', dispName: 'Sim Compare' },
                        { name: 'OneChart', dispName: 'One Chart' },
                        { name: 'TwoChart', dispName: 'Two Chart' },
                        { name: 'YieldCurveShift', dispName: 'Yield Curve Shift' },
                        { name: 'Eve', dispName: 'EVE/NEV' },
                        { name: 'CoreFundingUtilization', dispName: 'Core Funding Utilization' },
                        { name: 'FundingMatrix', dispName: 'Funding Matrix' },
                        { name: 'StaticGap', dispName: 'Static Gap' }
                    ]
                },
                {
                    name: "Strategy, Appendix and Other",
                    reports: [
                        { name: 'Document', dispName: 'Document' },
                        { name: 'SectionPage', dispName: 'Section Page' },
                        { name: 'CoverPage', dispName: 'Title Page' },
                        { name: 'MarginalCostOfFunds', dispName: 'Marginal Cost of Funds' },
                        { name: 'AssumptionMethods', dispName: 'Assumption Methodologies' },
                        { name: 'LookbackReport', dispName: 'Lookback' },
                        { name: 'SummaryRate', dispName: 'Summary of Rates' },
                        { name: 'LoanCapFloor', dispName: 'Loan Cap/Floor' },
                        { name: 'DetailedSimulationAssumption', dispName: 'Detailed Simulation Assumptions' },
                        { name: 'BalanceSheetCompare', dispName: 'Balance Sheet Compare' }
                    ]
                }
            ]
        },
        {
            name: "Batch",
            subSections: [
                {
                    name: "",
                    reports: [
                        { name: 'SimulationSummary', dispName: 'Summary of Simulation' },
                        { name: 'NIIRecon', dispName: 'NII Reconciliation' },
                        { name: 'InvestmentDetail', dispName: 'Investment Detail' },
                        { name: 'InvestmentError', dispName: 'Investment Error' }
                    ]
                }
            ]
        },
        {
            name: "EVE/ASC825",
            subSections: [
                {
                    name: "",
                    reports: [
                        { name: 'SummaryOfResults', dispName: 'EVE/NEV Summary of Results' },
                        { name: 'EveNevAssumptions', dispName: 'EVE/NEV Assumption Methodologies' },
                        { name: 'ASC825MarketValue', dispName: 'EVE/NEV Summary of Market Value or Market Price' },
                        { name: 'RateChangeMatrix', dispName: 'EVE/NEV Rate Change Matrix' },
                        { name: 'InvestmentPortfolioValuation', dispName: 'EVE/NEV Investment Portfolio Valuation' },
                        { name: 'PrepaymentDetails', dispName: 'EVE/NEV Prepayment Details' },
                        { name: 'ASC825AssumptionMethods', dispName: 'ASC825 Assumption Methodologies' },
                        { name: 'ASC825BalanceSheet', dispName: 'ASC825/EVE Market Value & Balance Sheet' },
                        { name: 'ASC825DiscountRate', dispName: 'ASC825/EVE Market and Discount Rates' },
                        { name: 'ASC825DataSource', dispName: 'ASC825 Data Source' },
                        { name: 'ASC825Worksheet', dispName: 'ASC825 Asset/Liability Worksheet' }
                        
                    ]
                }
            ]
        }
    ];

    //Quick fix until jake can fix him removing reportNames, maybe he keeps this method maybe he doesnt but work must go on
    var reportNames = [];
    for (var x = 0; x < _reportSections.length; x++) {
        for (var y = 0; y < _reportSections[x].subSections.length; y++) {
            for (var z = 0; z < _reportSections[x].subSections[y].reports.length; z++) {
                reportNames.push(_reportSections[x].subSections[y].reports[z]);
            }
        }
    }

    var model = {
        entityNames: entityNames,
        _reportSections: _reportSections,
        reportNames: reportNames,
        configureMetadataStore: configureMetadataStore,
        isLoading: ko.observable(false)
    };

    return model;

    //#region Internal Methods
    function configureMetadataStore(metadataStore) {
        //metadataStore.registerEntityTypeCtor(
        //    'Institution', function () { this.isPartial = false; }, institutionInitializer);
        metadataStore.registerEntityTypeCtor(
            'Information', null, informationInitializer);
        metadataStore.registerEntityTypeCtor(
            'Simulation', null, simulationInitializer);
        metadataStore.registerEntityTypeCtor(
            'Scenario', function () { }, scenarioInitializer);
        metadataStore.registerEntityTypeCtor(
            'ScenarioType', function () { }, scenarioTypeInitializer);
        metadataStore.registerEntityTypeCtor(
            'BalanceSheetCompare', null, balanceSheetCompareInitializer);
        metadataStore.registerEntityTypeCtor(
            'Package', null, packageInitializer);
        metadataStore.registerEntityTypeCtor(
           'OneChart', null, oneChartInitializer);
        metadataStore.registerEntityTypeCtor(
           'TwoChart', null, twoChartInitializer);
        metadataStore.registerEntityTypeCtor(
           'SimCompare', null, simCompareInitializer);
        metadataStore.registerEntityTypeCtor(
           'Eve', null, eveInitializer);
        metadataStore.registerEntityTypeCtor(
          'Document', null, documentInitializer);
        metadataStore.registerEntityTypeCtor(
          'EveScenarioType', null, eveScenarioTypeInitializer);
        metadataStore.registerEntityTypeCtor(
          'BalanceSheetMix', null, balanceSheetMixInitializer);
        metadataStore.registerEntityTypeCtor(
          'BasicSurplusReport', null, basicSurplusReportInitializer);
        metadataStore.registerEntityTypeCtor(
          'InterestRatePolicyGuidelines', null, interestRatePolicyGuidelinesInitializer);
        metadataStore.registerEntityTypeCtor(
          'LoanCapFloor', null, loanCapFloorInitializer);
        metadataStore.registerEntityTypeCtor(
         'StaticGap', null, staticGapInitializer);
        metadataStore.registerEntityTypeCtor(
         'FundingMatrix', null, fundingMatrixInitializer);
        metadataStore.registerEntityTypeCtor(
        'TimeDepositMigration', null, timeDepositMigrationInitializer);
        metadataStore.registerEntityTypeCtor(
         'CoreFundingUtilization', null, coreFundingUtilizationInitializer);
        metadataStore.registerEntityTypeCtor(
        'YieldCurveShift', null, yieldCurveShiftInitializer);
        metadataStore.registerEntityTypeCtor(
        'HistoricalBalanceSheetNII', null, historicalBalanceSheetNIIInitializer);
        metadataStore.registerEntityTypeCtor(
        'SimulationSummary', null, simulationSummaryInitializer);
        metadataStore.registerEntityTypeCtor(
         'DefinedGrouping', null, null);
        metadataStore.registerEntityTypeCtor(
         'DefinedGroupingGroup', null, null);
        metadataStore.registerEntityTypeCtor(
         'DefinedGroupingClassification', null, null);
        metadataStore.registerEntityTypeCtor(
        'Classification', null, null);
        metadataStore.registerEntityTypeCtor(
        'CoverPage', null, null);
        metadataStore.registerEntityTypeCtor(
        'EveNevAssumptions', null, null);
        metadataStore.registerEntityTypeCtor(
        'CashflowReport', null, null);
        metadataStore.registerEntityTypeCtor(
        'NetCashflow', null, null);
        metadataStore.registerEntityTypeCtor(
        'SummaryRate', null, null);
        metadataStore.registerEntityTypeCtor(
        'ASC825BalanceSheet', null, null);
        metadataStore.registerEntityTypeCtor(
        'PrepaymentDetails', null, null);
        metadataStore.registerEntityTypeCtor(
        'ASC825DataSource', null, null);
        metadataStore.registerEntityTypeCtor(
        'ASC825DiscountRate', null, null);
        metadataStore.registerEntityTypeCtor(
         'ASC825Worksheet', null, null);
        metadataStore.registerEntityTypeCtor(
        'ASC825MarketValue', null, null);
        metadataStore.registerEntityTypeCtor(
        'RateChangeMatrix', null, null);
        metadataStore.registerEntityTypeCtor(
        'InvestmentPortfolioValuation', null, null);
        metadataStore.registerEntityTypeCtor(
        'InvestmentDetail', null, null);
        metadataStore.registerEntityTypeCtor(
        'InvestmentError', null, null);
        metadataStore.registerEntityTypeCtor(
        'ASC825AssumptionMethods', null, null);
        metadataStore.registerEntityTypeCtor(
        'AssumptionMethods', null, null);
        metadataStore.registerEntityTypeCtor(
        'LiabilityPricingAnalysis', null, null);
        metadataStore.registerEntityTypeCtor(
        'LiquidityProjection', null, null);
        metadataStore.registerEntityTypeCtor(
        'NIIRecon', null, null);
        metadataStore.registerEntityTypeCtor(
        'SummaryOfResults', null, null);
        metadataStore.registerEntityTypeCtor(
        'CapitalAnalysis', null, null);
        metadataStore.registerEntityTypeCtor(
         'LBCustomLayout', null, null);
        metadataStore.registerEntityTypeCtor(
        'LBCustomType', null, null);
        metadataStore.registerEntityTypeCtor(
        'LBLookback', null, null);
        metadataStore.registerEntityTypeCtor(
        'LBLookbackGroup', null, null);
        metadataStore.registerEntityTypeCtor(
        'LBLookbackData', null, null);
        metadataStore.registerEntityTypeCtor(
        'LBLookbackReport', null, null);
        metadataStore.registerEntityTypeCtor(
        'Report', null, null);
        metadataStore.registerEntityTypeCtor(
        'DetailedSimulationAssumption', null, null);
        metadataStore.registerEntityTypeCtor(
        'ExecutiveRiskSummary', null, null);
        metadataStore.registerEntityTypeCtor(
        'ExecutiveRiskSummaryIRRScenario', null, null);
        metadataStore.registerEntityTypeCtor(
        'SectionPage', null, null);
        metadataStore.registerEntityTypeCtor(
            'MarginalCostOfFunds', null, null);
        metadataStore.registerEntityTypeCtor(
            'InventoryLiquidityResources', null, null);

    }

    //function institutionInitializer(institution) {
    //    institution.newestName = ko.computed(function() {
    //        return institution.information()[0].bankName();
    //    });
    //}

    function informationInitializer(information) {
        information.isPartial = ko.observable(true);

        information.formattedAsOf = ko.computed(function () {
            return moment.utc(information.asOf().date()).format('MMM Do YYYY');
        });
    }

    function simulationInitializer(simulation) {
        simulation.isPartial = ko.observable(true);
    }

    function scenarioInitializer(scenario) {
        scenario.isDisabled = ko.observable(false);
        scenario.isDisabledLeft = ko.observable(false);
        scenario.isDisabledRight = ko.observable(false);
        scenario.standardName = ko.computed(function () {
            if (scenario.name().toLowerCase().indexOf("base") !== -1) {
                return "base";
            } else {
                return scenario.name().toLowerCase();
            }
        });
        scenario.quarterlyNii = ko.observableArray();
        scenario.monthlyNii = ko.observableArray();
        scenario.niiForYear = function (yearNumber) {
            var yearlyNii = 0;
            for (var i = (yearNumber - 1) * 4 ; i < yearNumber * 4; i++) {
                yearlyNii += scenario.quarterlyNii()[i];
            }
            return yearlyNii;
        };
    }

    function scenarioTypeInitializer(scenario) {
    }


    function balanceSheetMixInitializer() {
    }

    function balanceSheetCompareInitializer(balanceSheetCompare) {
        balanceSheetCompare.lineItems = ko.observableArray();
    }

    function basicSurplusReportInitializer() { }

    function interestRatePolicyGuidelinesInitializer() { }

    function packageInitializer(thePackage) {

    }

    function oneChartInitializer(oneChart) {
        oneChart.delete = function () {
            deleteFootnotes(oneChart.footnotes);
            var chart = oneChart.chart();

            while (chart.chartScenarioTypes().length > 0) {
                var lenght = chart.chartScenarioTypes().length;
                chart.chartScenarioTypes()[lenght - 1].entityAspect.setDeleted();
            }
            chart.entityAspect.setDeleted();
            oneChart.entityAspect.setDeleted();
        };
    }

    function twoChartInitializer(twoChart) {
        twoChart.delete = function () {

            deleteFootnotes(twoChart.footnotes);
            var leftChart = twoChart.leftChart();

            var rightChart = twoChart.rightChart();

            while (leftChart.chartScenarioTypes().length > 0) {
                var leftLenght = leftChart.chartScenarioTypes().length;
                leftChart.chartScenarioTypes()[leftLenght - 1].entityAspect.setDeleted();
            }

            while (rightChart.chartScenarioTypes().length > 0) {
                var rightLenght = rightChart.chartScenarioTypes().length;
                rightChart.chartScenarioTypes()[rightLenght - 1].entityAspect.setDeleted();
            }

            leftChart.entityAspect.setDeleted();
            rightChart.entityAspect.setDeleted();



            twoChart.entityAspect.setDeleted();
        };
    }

    function simCompareInitializer(simCompare) {
        simCompare.delete = function () {
            deleteFootnotes(simCompare.footnotes);
            while (simCompare.simCompareScenarioTypes().length > 0) {
                var lenght = simCompare.simCompareScenarioTypes().length;
                simCompare.simCompareScenarioTypes()[lenght - 1].entityAspect.setDeleted();
            }
            while (simCompare.simCompareSimulationTypes().length > 0) {
                var lenght = simCompare.simCompareSimulationTypes().length;
                simCompare.simCompareSimulationTypes()[lenght - 1].entityAspect.setDeleted();
            }
            simCompare.entityAspect.setDeleted();
        };
    }
    function loanCapFloorInitializer(loancapfloor) {
        loancapfloor.delete = function () {
            deleteFootnotes(loancapfloor.footnotes);
            deleteFootnotes(loancapfloor.splitFootnotes);
            loancapfloor.entityAspect.setDeleted();
        };
    }

    function staticGapInitializer(staticgap) {
        staticgap.delete = function () {
            deleteFootnotes(staticgap.footnotes);
            staticgap.entityAspect.setDeleted();
        };
    }

    function fundingMatrixInitializer(fundingmatrix) {
        fundingmatrix.delete = function () {
            deleteFootnotes(fundingmatrix.footnotes);
            fundingmatrix.entityAspect.setDeleted();
        };
    }

    function timeDepositMigrationInitializer(timeDep) {
        timeDep.delete = function () {
            deleteFootnotes(timeDep.footnotes);
            timeDep.entityAspect.setDeleted();
        };
    }

    function coreFundingUtilizationInitializer(coreFund) {
        coreFund.delete = function () {
            deleteFootnotes(coreFund.footnotes);
            coreFund.entityAspect.setDeleted();
        };
    }


    function yieldCurveShiftInitializer(yieldCurve) {

        yieldCurve.delete = function () {
            deleteFootnotes(yieldCurve.footnotes);
            while (yieldCurve.yieldCurveShiftScenarioTypes().length > 0) {
                var lenght = yieldCurve.yieldCurveShiftScenarioTypes().length;
                yieldCurve.yieldCurveShiftScenarioTypes()[lenght - 1].entityAspect.setDeleted();
            }
            yieldCurve.entityAspect.setDeleted();
        };
    }


    function historicalBalanceSheetNIIInitializer(histBal) {

        histBal.delete = function () {
            deleteFootnotes(histBal.footnotes);
            //Delete Simulations
            //while (histBal.historicalBalanceSheetNII.historicalBalanceSheetNIISimulations().length > 0) {
            //    var lenght = histBal.historicalBalanceSheetNIISimulations().length;
            //    histBal.historicalBalanceSheetNIISimulations()[lenght - 1].entityAspect.setDeleted();
            //}
            //Delete Scenarios
            //while (histBal.historicalBalanceSheetNII.historicalBalanceSheetNIIScenarios().length > 0) {
            //    var lenght = histBal.historicalBalanceSheetNIIScenarios().length;
            //    histBal.historicalBalanceSheetNIIScenarios()[lenght - 1].entityAspect.setDeleted();
            //}


            histBal.entityAspect.setDeleted();
        };
    }

    function simulationSummaryInitializer(simSum) {

        simSum.delete = function () {
            deleteFootnotes(simSum.footnotes);
            simSum.entityAspect.setDeleted();
        };
    }



    function eveInitializer(eve) {
        eve.delete = function () {
            deleteFootnotes(eve.footnotes);
            while (eve.eveScenarioTypes().length > 0) {
                var lenght = eve.eveScenarioTypes().length;
                eve.eveScenarioTypes()[lenght - 1].entityAspect.setDeleted();
            }
            eve.entityAspect.setDeleted();
        };
    }

    function documentInitializer(docuemnt) {
        docuemnt.delete = function () {
            deleteFootnotes(docuemnt.footnotes);
            docuemnt.entityAspect.setDeleted();
        };
    }

    function deleteFootnotes(footnotes) {
        while (footnotes().length > 0) {
            var lenght = footnotes().length;
            footnotes()[lenght - 1].entityAspect.setDeleted();
        }
    }

    function eveScenarioTypeInitializer(eveScenarioType) {

    }

    //#endregion
})