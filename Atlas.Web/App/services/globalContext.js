﻿define(['durandal/system', 'services/model', 'config', 'services/logger', 'durandal/app'],
    function (system, model, config, logger, app) {
        var entityQuery = breeze.EntityQuery;
        var manager = configureBreezeManager();
        var localTimeRegex = /.\d{3}$/;

        // https://stackoverflow.com/questions/20265251/using-breeze-datatype-parsedatefromserver-at-the-breeze-object-define
        breeze.DataType.parseDateFromServer = function (source) {
            if (typeof source === 'string') {
                // convert to UTC string if no time zone specifier.
                var isLocalTime = localTimeRegex.test(source);
                source = isLocalTime ? source + 'Z' : source;
            }
            source = new Date(source);
            return source;
        }

        var getMultiPageHeight = function (viewHeight, footnoteHeight) {

            var h = (776 * Math.ceil(viewHeight / 776)) - footnoteHeight;
            return h;
        }

        var getSqlDatabase = function (sqlDatabaseObservable) {
            var query = entityQuery.from('SqlDatabase');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (sqlDatabaseObservable) {
                    sqlDatabaseObservable(data.results[0]);
                }
                log('Retrieved [SqlDatabase] from remote data source',
                    data, false);
            }
        };

        var getCodeVersion = function (codeVersionObservable) {
            var query = entityQuery.from('CodeVersion');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (codeVersionObservable) {
                    codeVersionObservable(data.results[0]);
                }
                log('Retrieved [Code Version] from remote data source',
                    data, false);
            }
        };

        var completePdfProcess = function (data) {
            var arg = data.results;
            if (arg instanceof Array) arg = arg[0];

            if (!arg.isSuccess || arg.isExportFatal) {
                app.trigger('application:pdfFailed', arg);
                return;
            }

            app.trigger('application:pdfFinished');
            log('Retrieved [Kicked Off Pdf] from remote data source', data, false);
        };

        var startPdfProcess = function (packageId) {
            var query = entityQuery.from('PackagePDF')
                .withParameters({ pckId: packageId });

            return manager.executeQuery(query)
                .then(completePdfProcess)
                .fail(pdfExportFailed);
        };

        var startReportPdfProcess = function (reportId) {
            var query = entityQuery.from('ReportPDF')
                .withParameters({ reportId: reportId });

            return manager.executeQuery(query)
                .then(completePdfProcess)
                .fail(pdfExportFailed);
        };

        function pdfExportFailed(error) {
            app.trigger('application:pdfFailed', { isSuccess: false, message: error, isExportFatal: true });
        }

        var getPDFStatus = function (packageId, obs) {
            var query = entityQuery.from('PDFStatus')
                .withParameters({ pckId: packageId });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    console.log(data.results[0]);
                    obs(data.results[0]);
                }
                log('Retrieved [Retrieved Status] from remote data source',
                    data, false);
            }
        };

        //Clear EntityManager cache - called from switching banks on landing.js
        //Solves issues w/ objects of the same ID in different banks confusing the cache
        //e.g. pulling up package 3 in bank A and then pulling up package 3 in bank B
        var clearCache = function () {
            return manager.clear();
        }

        //Get User Profile Object
        var getUserProfile = function (profObservable) {
            var query = entityQuery.from('AtlasUserProfile');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (profObservable) {
                    profObservable(data.results[0]);
                    if ($('#database').length > 0) {
                        $('#database').text('Current Bank: ' + data.results[0].atlasBankName);
                    }
                    if ($('#asOfDate').length > 0) {
                        $('#asOfDate').text('Current As of Date: ' + data.results[0].asOfDate);
                    }
                    
                    console.log(data.results[0].asOfDate);
                }
                log('Retrieved [User Profile] from remote data source',
                    data, false);
            }
        };

        var getUserName = function () {
            var profile = ko.observable();
            return getUserProfile(profile).then(
                function () {
                    var userName = profile().userName;
                    var splitPoint = userName.indexOf('@');

                    if (splitPoint > -1)
                        userName = userName.substring(0, splitPoint);

                    return userName;
                }
            );
        };

        //Update Profile
        var updateProfile = function (database, asOfDate, profObservable) {

            //If inputs undefined blank them
            if (asOfDate == undefined) {
                asOfDate = '';
            }

            //If inputs undefined blank them
            if (database == undefined) {
                database = '';
            }
            var query = entityQuery.from('UpdateProfile')
                .withParameters({ AtlasDatabase: database, AtlasAsOfDate: asOfDate });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (profObservable) {
                    profObservable(data.results[0]);
                    if ($('#database').length > 0) {
                        $('#database').text('Current Bank: ' + data.results[0].atlasBankName);
                    }
                    if ($('#asOfDate').length > 0) {
                        $('#asOfDate').text('Current As of Date: ' + data.results[0].asOfDate);
                    }
                }
                log('Retrieved [Returning Profile] from remote data source',
                    data, false);
            }
        };

        //currently only hold credit union / regular bank flag...but could be useful for other things
        var getBankInfo = function (infoObs, profile) {
            var cuStatusQuery = entityQuery.from("CreditUnionStatus");

            return manager.executeQuery(cuStatusQuery)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (infoObs) {
                    infoObs({
                        creditUnion: data.results[0]
                    });
                }
            }
        };

        //Get Available Banks
        var getAvailableBanks = function (banksObservable) {
            var query = entityQuery.from('AvailableBanks');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (banksObservable) {
                    banksObservable(data.results);
                }
                log('Retrieved [Available Banks] from remote data source',
                    data, false);
            }
        };

        //Get Available Banks
        var getAvailableSimulations = function (obs, inst, offset) {
            var query = entityQuery.from('AvailableSimulations').withParameters({
                inst: inst,
                offset: (offset == "" ? 0 : offset)
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Available Simulations] from remote data source',
                    data, false);
            }
        };

        var getAvailableSimulationScenarios = function (obs, inst, offset, simId, isEve) {

            //fail sage
            if (simId == null) {
                simId = 0;
            }



            var query = entityQuery.from('AvailableSimulationScenarios').withParameters({
                inst: inst,
                offset: offset,
                simId: simId,
                eve: isEve
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Available Simulations Scenarios] from remote data source',
                    data, false);
            }
        };

        var getAvailableSimulationScenariosMultiple = function (obs, inst, offsets, simIds, eveSims) {
           var query = entityQuery.from('AvailableSimulationScenariosMultiple').withParameters({
                inst: inst,
                offsets: offsets,
                simIds: simIds,
                eveSims: eveSims
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Available Simulations Scenarios Multiple] from remote data source',
                    data, false);
            }
        };

        var getAvailableSimulationScenariosMultipleInst = function (obs, insts, offsets, simIds, eveSims) {
            var query = entityQuery.from('AvailableSimulationScenariosMultipleInst').withParameters({
                insts: insts,
                offsets: offsets,
                simIds: simIds,
                eveSims: eveSims
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Available Simulations Scenarios Multiple Insts] from remote data source',
                    data, false);
            }
        };


        

        //Get bank name
        var getBankName = function (dbName, dbNameObs) {
            var query = entityQuery.from('BankName').withParameters({
                dbName: dbName
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (dbNameObs) {
                    return dbNameObs(data.results[0].instName);
                }
                log('Retrieved [Bank Name] from remote data source',
                    data, false);
            }
        };

        var getRegulatoryBankName = function (dbName, dbNameObs) {
            var query = entityQuery.from('BankName').withParameters({
                dbName: dbName
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (dbNameObs) {
                    return dbNameObs(data.results[0].regulatoryBankName);
                }
                log('Retrieved [regulatoryBankName] from remote data source',
                    data, false);
            }
        };

        //Get FedRates dates
        var getFedRatesDates = function (obs) {
            var query = entityQuery.from('FedRatesDates');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    return obs(data.results);
                }
                log('Retrieved [FedRates Dates] from remote data source',
                    data, false);
            }
        };

        //Get Available As Of Dates Based Off Of What Bank Is Selected
        var getAsOfDateOffsets = function (asOfDateObservable, instName) {
            return getAsOfDates(asOfDateObservable, instName, true, 13, false);
        };

        var getAsOfDateOffsetsCustomCount = function (asOfDateObservable, instName, count) {
            return getAsOfDates(asOfDateObservable, instName, true, count, false);
        };

        var getAsOfDateOffsetsExCurrent = function (asOfDateObservable, instName) {
            return getAsOfDates(asOfDateObservable, instName, true, 8, true);
        };



        var defaultBasicSurplus = function (asOfDate) {
            var query = entityQuery.from('DefaultBasicSurplus')
                .withParameters({
                    asOfDate: asOfDate
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                log('Retrieved [Update Basic Surpluses] from remote data source',
                    data, false);
            }
        }

        var getPolicyOffsets = function (obs, instName) {
            var query = entityQuery.from('PolicyOffsets')
                .withParameters({
                    instName: instName
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                console.log('WTF WTF WETF');
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Policy Off Sets] from remote data source',
                    data, false);
            }
        }

        var getModelOffsets = function (obs, instName) {
            var query = entityQuery.from('ModelOffsets')
                .withParameters({
                    instName: instName
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Model Off Sets] from remote data source',
                    data, false);
            }
        }

        var getCurrentRiskAss = function (offSet, instName, obs) {
            var query = entityQuery.from('CurrentRiskAss')
                .withParameters({
                    instName: instName,
                    offSet: offSet
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results[0]);
                }
                log('Retrieved [Policy Off Sets Vakues for Current OFfset] from remote data source',
                    data, false);
            }
        }

        var getBasicSurplusAdminOffsets = function (obs, instName) {
            var query = entityQuery.from('BasicSurplusAdminOffsets')
                .withParameters({
                    instName: instName
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                log('Retrieved [Basic Surplus Admin Off Sets] from remote data source',
                    data, false);
            }
        }

        var getAsOfDates = function (asOfDateObservable, instName, prevDatesOnly, numDates, excludeCurrent) {
            var query = entityQuery.from('AsOfDates')
                .withParameters({
                    instName: instName,
                    prevDatesOnly: prevDatesOnly,
                    numDates: numDates,
                    excludeCurrent: excludeCurrent
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                //If we're requesting a certain number of dates, pad out blanks
                for (var i = data.results.length; i < numDates; i++) {
                    if (numDates <= 8) {
                        data.results.push({
                            asOfDateDescript: "N/A"
                        });
                    }
                }

                //HACK can't get index in optionsValue, so generate index manually - see https://github.com/knockout/knockout/issues/1871
                var i = 0;
                data.results.forEach(function (elem) {
                    elem.index = i++; //Faster than data.results.indexOf(elem);
                });

                if (asOfDateObservable) {
                    asOfDateObservable(data.results);
                }
                log('Retrieved [As Of Dates] from remote data source',
                    data, false);
            }
        };

        //Get Database Status
        var getDatabaseStatus = function (statusObersvable) {
            var query = entityQuery.from('DatabaseStatus');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (statusObersvable) {
                    statusObersvable(data.results[0]);
                }
                log('Retrieved [Database Status] from remote data source',
                    data, false);
            }
        };

        //Get Available Banks
        var getRetainedEarnings = function (retainedObservable, curOffset, priorOffset, inst, asOfDate) {
            var query = entityQuery.from('RetainedEarnings')
                .withParameters({
                    curOffset: curOffset,
                    priorOffset: priorOffset,
                    inst: inst,
                    asOfDate: asOfDate

                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (retainedObservable) {
                    if (data.results != null && data.results != '') {
                        retainedObservable(numeral(data.results.toString()).value());
                    }

                }
                log('Retrieved [Retained Earnings] from remote data source',
                    data, false);
            }
        };

        //Set Database Status
        var setDatabaseStatus = function (status, statusObersvable) {
            var query = entityQuery.from('SetDatabaseStatus')
                .withParameters({ lockDatabase: status });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (statusObersvable) {
                    statusObersvable(data.results[0]);
                }
            }
        };

        //Get Atlas Template by ID
        var getAtlasTemplateById = function (id, templatesObservable) {
            var query = entityQuery.from('GetAtlasTemplateById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (templatesObservable) {
                    templatesObservable(data.results[0]);
                }
                log('Retrieved [Template] from remote data source',
                    data, false);
            }
        };

        //Get Atlas Templates
        var getAtlasTemplates = function (templatesObservable) {
            var query = entityQuery.from('GetAtlasTemplates');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (templatesObservable) {
                    templatesObservable(data.results);
                }
                log('Retrieved [Templates] from remote data source',
                    data, false);
            }
        };

        //Copy A Package Template
        var copyPackageTemplate = function (packageId, templatePackageId) {
            var query = entityQuery.from('CopyPackageFromTemplate')
                .withParameters({ packageId: packageId, templatePackageId: templatePackageId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

            }
        };


        //Roll Forward an As Of Date
        var rollDate = function (asOfDate) {
            var query = entityQuery.from('NewRollDate')
                .withParameters({ asOfDate: asOfDate });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

            }
        };

        //marginal cost of funds
        var createMarginalCostOfFunds = function () {
            var c = manager.createEntity("MarginalCostOfFunds", {
                id: -1
            });
            c.runoffs("0,0,0");
            c.rateChanges("0,0,0");
            var profile = ko.observable();
            getUserProfile(profile).then(function () {
                c.institutionDatabaseName(profile().databaseName);
            });
            return c
        };
        var getMarginalCostOfFundsById = function (id, obs) {
            var query = entityQuery.from('MarginalCostOfFunds')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results[0];

                if (obs) {
                    obs(type);
                }
                log('Retrieved [MarginalCostOfFunds] from remote data source', data, false);
            }
        };
        var getMarginalCostOfFundsViewDataById = function (id, srObservable) {
            var query = entityQuery.from('MarginalCostOfFundsViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (srObservable) {
                    srObservable(sr);
                }
                log('Retrieved [Marginal Cost Of Funds] from remote data source', data, false);
            }
        };

        //Lookback Stuff

        //Layouts
        var createLBCustomLayout = function () {
            var l = manager.createEntity("LBCustomLayout");
            var profile = ko.observable();
            getUserProfile(profile).then(function () {
                l.institutionDatabaseName(profile().databaseName);
            });
            return l;
        };

        var getLBCustomLayouts = function (obs) {
            var query = entityQuery.from("LBCustomLayouts");

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var lb = data.results;
                if (obs) {
                    obs(lb);
                }
                log('Retrieved [Lookback Custom Layouts] from remote data source', data, false);
            }
        };

        var getLBCustomLayoutById = function (id, obs) {

            var query = entityQuery.from('LBCustomLayouts')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var layout = data.results[0];
                if (obs) {
                    obs(layout);
                }
                log('Retrieved [lb custom layout] from remote data source', data, false);
            }
        };

        var createLBCustomLayoutRowItem = function (isAsset) {
            var item = manager.createEntity("LBCustomLayoutRowItem");
            item.isAsset(isAsset);
            return item;
        };

        var getLookbacksByLayoutId = function (layoutId, lookbackObsArr) {
            var query = entityQuery.from("LBLookbacksByLayoutId")
                .withParameters({ layoutId: layoutId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (lookbackObsArr) {
                    lookbackObsArr(data.results);
                }
            }
        }

        //Types
        var createLBCustomType = function () {
            var t = manager.createEntity("LBCustomType");
            t.name("New Custom Type");
            t.startDateOffset(1);
            t.endDateOffset(0);
            return t;
        };

        var getLBCustomTypes = function (obs) {
            var query = entityQuery.from("LBCustomTypes");

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var lt = data.results;
                if (obs) {
                    obs(lt);
                }
                log('Retrieved [Lookback Custom Types] from remote data source', data, false);
            }
        };

        var getLBCustomTypeById = function (id, obs) {

            var query = entityQuery.from('LBCustomTypes')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var layout = data.results[0];
                if (obs) {
                    obs(layout);
                }
                log('Retrieved [lb custom type] from remote data source', data, false);
            }
        };

        //Lookbacks
        var createLBLookback = function () {
            var lb = manager.createEntity("LBLookback");
            lb.name("New Lookback");
            lb.simulationTypeId(1);
            lb.scenarioTypeId(1);

            return lb;

        };

        var getLBLookbacksByOffset = function (offset, instName, lbObsArr) {
            var query = entityQuery.from("LBLookbacksByOffset")
                .withParameters({
                    aodOffset: offset,
                    instName: instName
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (lbObsArr) {
                    lbObsArr(data.results);
                }
            }
        };

        var getLBLookbacks = function (obs, aod) {
            var query = entityQuery.from("LBLookbacks");

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (obs) {
                    obs(data.results);
                }
                //var lt// = data.results

                //if (aod == undefined) {
                //    lt = data.results;
                //} else {
                //    lt = data.results.filter(function (item) {
                //        return moment(item.asOfDate()).format('MM-DD-YYYY') == moment(aod).format('MM-DD-YYYY');
                //    });
                //}

                
                log('Retrieved [Lookbacks] from remote data source', data, false);
            }
        };

        var getLBLookbackGroupsById = function (obs, id) {
            var query = entityQuery.from("LBLookbackGroups").withParameters({ lookbackId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var lt = data.results;
                if (obs) {
                    obs(lt);
                }
                log('Retrieved [Lookbacks] from remote data source', data, false);
            }
        };

        var getLBLookbackById = function (id, obs) {

            var query = entityQuery.from('LBLookbacksById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var layout = data.results[0];
                if (obs) {
                    obs(layout);
                }
                log('Retrieved [lookback] from remote data source', data, false);
            }
        };

        var createLBLookbackGroup = function () {
            var lbg = manager.createEntity("LBLookbackGroup");
            return lbg;
        };

        var createLBLookbackData = function () {
            var d = manager.createEntity("LBLookbackData");
            d.proj_AvgBal(0);
            d.proj_AvgBal_Adj(0);
            d.proj_AvgRate(0);
            d.proj_IncExp(0);
            d.proj_IncExp_Adj(0);
            d.actual_AvgBal(0);
            d.actual_AvgRate(0);
            d.actual_IncExp(0);
            d.variance_ValDueToRate(0);
            d.variance_ValDueToVol(0);
            d.variance_Val(0);
            d.variance_Perc(0);
            return d;
        };



        var createLBCustomClassification = function (classificationObj) {
            var custClass = manager.createEntity("LBCustomLayoutClassification");
            custClass.isAsset(classificationObj.isAsset);
            custClass.acc_Type(classificationObj.id.split('_')[0]);
            custClass.rbcTier(classificationObj.id.split('_')[1]);
            custClass.name(classificationObj.name);
            return custClass;
        };

        var updateLookbackGroupValues = function (lookbackGroupObs, groupId, lookbackId, startOffset, endOffset) {
            var query = entityQuery.from('UpdateLookbackGroup')
                .withParameters({
                    startOffset: startOffset,
                    endOffset: endOffset,
                    groupId: groupId,
                    lookbackId: lookbackId
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ret = data.results[0];
                if (lookbackGroupObs) {
                    lookbackGroupObs(ret);
                }
                log('Retrieved [LookbackGroup] from remote data source', data, false);
            }

            function queryFailed() { }

        };

        var getLookbackGroupDataProjections = function (layoutId, startOffset, endOffset, simTypeId, scenTypeId, taxEquiv, lbAod, projObs) {
            var query = entityQuery.from('LookbackGroupDataProjections')
                .withParameters({
                    layoutId: layoutId,
                    startOffset: startOffset,
                    endOffset: endOffset,
                    simTypeId: simTypeId,
                    scenTypeId: scenTypeId,
                    taxEquiv: taxEquiv,
                    lbAod: lbAod
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ret = data.results;
                if (projObs) {
                    projObs(data.results);
                }
                log('Retrieved [LookbackGroupData] Projections from remote data source', data, false);
            }
        };

        var getSimulationScenarioInfo = function (aod, dateOffset, simulationTypeId, scenarioTypeId, obs) {
            var query = entityQuery.from("SimulationScenarioInfo")
                .withParameters({
                    aod: aod,
                    dateOffset: dateOffset,
                    simulationTypeId: simulationTypeId,
                    scenarioTypeId: scenarioTypeId
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var layout = data.results[0];
                if (obs) {
                    obs(layout);
                }
                log('Retrieved [SimulationScenarioInfo] from remote data source', data, false);
            }
        };

        var logEvent = function (ev, page) {
            var query = entityQuery.from("LogEvent")
                .withParameters({
                    ev: ev,
                    page: page
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var layout = data.results[0];
                //if (obs) {
                //    obs(layout);
                //}
                log('Logged even for ' + ev + ' on page ' + page, data, false);
            }
        };

        var getLookbackGroupDataById = function (id, obs) {
            var query = entityQuery.from('LookbackGroupDataConfigById').withParameters({ groupId: id });


            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var thisData = data.results;
                if (obs) {
                    obs(thisData);
                }
                log('Retrieved [LookbackGroupData] from remote data source', data, false);
            }
        };
        var createLookbackType = function () {
            var lbg = manager.createEntity("LookbackType");
            return lbg;
        };

        var createLookbackReport = function (packageId, priority, profile) {

            var lr = manager.createEntity("LookbackReport", {
                id: -1,
                institutionDatabaseName: profile().databaseName
            });
            return lr;
        };
        var createLookbackReportSection = function () {
            var s = manager.createEntity("LookbackReportSection");
            return s;
        };
        var getLookbackReportById = function (id, obs) {
            var query = entityQuery.from('LookbackReports').withParameters({ id: id });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var thisData = data.results[0];
                if (obs) {
                    obs(thisData);
                }
                log('Retrieved [LookbackReport] from remote data source', data, false);
            }
        };

        var getLookbackReportViewDataById = function (id, obs) {
            var query = entityQuery.from("LookbackReportViewDataById")
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var lbv = data.results;
                if (obs) {
                    obs(lbv);
                }
                log('Retrieved [LookbackReportViewData] from remote data source', data, false);
            }
        };

        //Delete Lokoback
        var deleteLookback = function (id) {
            var query = entityQuery.from('DeleteLookback')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                log('Retrieved [Define Grouping] from remote data source', data, false);
            }
        };

        //Delete Layout
        var deleteLayout = function (id) {
            var query = entityQuery.from('DeleteLayout')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                log('Retrieved [Define Grouping] from remote data source', data, false);
            }
        };

        //DeleteLookbackGroup
        var deleteLookbackGroup = function (id) {
            var query = entityQuery.from('DeleteLookbackGroup')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

            }
        };

        var getLBLookbacksByReportId = function (id, lookbacksObs) {
            var query = entityQuery.from("LBLookbacksByReportId")
                .withParameters({ reportId: id });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results;
                if (lookbacksObs) {
                    lookbacksObs(data.results);
                }
                log('Retrieved LBLookbacks for report id '+ id +' from remote data source', data, false);
            }
        }

        //End Lookback Stuff

        //Get all available Basic Surplus Admins
        var getBasicSurplusAdmins = function (typeObservable) {
            var query = entityQuery.from('BasicSurplusAdmins');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results;
                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [BasicSurplusAdmins] from remote data source', data, false);
            }
        };

        var getBasicSurplusAdminsTable = function (typeObservable, instName) {
            var query = entityQuery.from('BasicSurplusAdminsTable').withParameters({
                instName: instName
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results;
                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [BasicSurplusAdminsTable] from remote data source', data, false);
            }
        };

        var getBasicSurplusMarketValues = function (inst, typeObservable) {
            var query = entityQuery.from('BasicSurplusMarketValGainLoss').withParameters({
                inst: inst
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results;
                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [Basic Surplus Market Values] from remote data source', data, false);
            }
        };

        //Get Basic Surplus Admin by ID
        var getBasicSurplusAdminById = function (id, inst, typeObservable) {
            var query = entityQuery.from('BasicSurplusAdminById')
                .withParameters({
                    id: id,
                    inst: inst
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results[0];
                if (typeObservable) {
                    console.log(`getBasicSurplusAdminById(${id}, '${inst}') success`);
                    typeObservable(type);
                    console.log(typeObservable());
                }
                else {
                    console.log(`getBasicSurplusAdminById(${id}, '${inst}') success, but typeObservable for result not given`);
                }

                log('Retrieved [BasicSurplusAdmin] from remote data source', data, false);
            }
        };

        //Get Basic Surplus Secured Liability Types by Basic Surplus Admin ID
        var getBasicSurplusSecuredLiabilitiesTypesByBasicSurplusAdminId = function (id, typeObservable) {
            var query = entityQuery.from('BasicSurplusSecuredLiabilitiesTypesByBasicSurplusAdminId')
                .withParameters({
                    id: id
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results;

                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [BasicSurplusSecuredLiabilitiesTypes] from remote data source', data, false);
            }
        };

        //Get Basic Surplus Secured Liability Type by ID
        var getBasicSurplusSecuredLiabilitiesTypeById = function (id, typeObservable) {
            var query = entityQuery.from('BasicSurplusSecuredLiabilitiesTypeById')
                .withParameters({
                    id: id
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results[0];

                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [BasicSurplusSecuredLiabilitiesType] from remote data source', data, false);
            }
        };

        //Get Collateral Types by Basic Surplus Admin ID
        var getCollateralTypesByBasicSurplusAdminId = function (id, typeObservable) {
            var query = entityQuery.from('CollateralTypesByBasicSurplusAdminId')
                .withParameters({
                    id: id
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results;

                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [CollateralTypes] from remote data source', data, false);
            }
        };

        //Pull Basic Surplus Admin Data by ID
        var getBasicSurplusAdminData = function (instName, asOfDateOffset, simulationTypeId, scenarioTypeId, observableArray) {
            var query = entityQuery.from('BasicSurplusAdminData')
                .withParameters({
                    instName: instName,
                    asOfDateOffset: asOfDateOffset,
                    simulationTypeId: simulationTypeId,
                    scenarioTypeId: scenarioTypeId
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ret = data.results;
                if (observableArray)
                    observableArray(ret);
                log('Retrieved [BasicSurplusAdminData] from remote data source', data, false);
            }
        };

        //Get Secured Liabilities Types
        var getSecuredLiabilitiesTypes = function (inst, securedLiabilitiesTypeObservableArray) {
            var query = entityQuery.from('SecuredLiabilitiesTypes').withParameters({
                inst: inst,
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var securedLiabilitiesTypes = data.results;
                if (securedLiabilitiesTypeObservableArray) {
                    securedLiabilitiesTypeObservableArray(securedLiabilitiesTypes);
                }
                log('Retrieved [SecuredLiabilitiesType] from remote data source', data, false);
            }
        };

        //Get Current Profile InformationId (AsOfDate)
        var getProfileInformationId = function (typeObservable) {
            var query = entityQuery.from('ProfileInformationId');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var type = data.results[0];

                if (typeObservable) {
                    typeObservable(type);
                }
                log('Retrieved [ProfileInformationId] from remote data source', data, false);
            }
        };

        //Basic Surplus Create Types
        var createBasicSurplusMarketValuesType = function () {
            return manager.createEntity("BasicSurplusMarketValue");
        };
        var createBasicSurplusSecuredLiabilitiesType = function () {
            var newType = manager.createEntity("BasicSurplusSecuredLiabilitiesType");
            newType.override(true);

            return newType;
        };
        var createBasicSurplusCollateralType = function () {
            return manager.createEntity("BasicSurplusCollateralType");
        };
        var createCollateralType = function () {
            return manager.createEntity("CollateralType");
        };
        var createBasicSurplusAdmin = function () {
            var basicSurplusAdmin = manager.createEntity("BasicSurplusAdmin");

            //Set Report Package and Package Order -- HACK this is done in basicsurpluses.js
            //basicSurplusAdmin.priority(priority);
            //basicSurplusAdmin.institutionDatabaseName(profile().atlasDatabase);

            //Set informationId -- HACK this is done in basicsurpluses.js
            //getProfileInformationId(basicSurplusAdmin.informationId);


            //Set Report Defaults
            basicSurplusAdmin.asOfDateOffset(0);
            basicSurplusAdmin.simulationTypeId(1); //TODO we have to set this now - default this to something reasonable
            basicSurplusAdmin.overrideSimulationTypeId(false); //TODO we have to set this now - default this to something reasonable
            basicSurplusAdmin.scenarioTypeId(1);

            return basicSurplusAdmin;
        };
        var createBasicSurplusAdminOvernightFund = function () {
            var newType = manager.createEntity("BasicSurplusAdminOvernightFund");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.balance(0);

            return newType;
        };
        var createBasicSurplusAdminSecurityCollateral = function () {
            var newType = manager.createEntity("BasicSurplusAdminSecurityCollateral");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.uSTAgency(0);
            newType.mBSAgency(0);
            newType.mBSPvt(0);

            return newType;
        };
        var createBasicSurplusAdminLiquidAsset = function () {
            var newType = manager.createEntity("BasicSurplusAdminLiquidAsset");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.percent(0);
            newType.amount(0);

            return newType;
        };
        var createBasicSurplusAdminVolatileLiability = function () {
            var newType = manager.createEntity("BasicSurplusAdminVolatileLiability");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.percent(0);
            newType.amount(0);

            return newType;
        };
        var createBasicSurplusAdminLoanCollateral = function () {
            var newType = manager.createEntity("BasicSurplusAdminLoanCollateral");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.amountPledged(0);
            newType.collateralValue(0);
            newType.netCollateral(0);
            newType.l360Collateral(0);

            return newType;
        };
        var createBasicSurplusAdminLoan = function () {
            var newType = manager.createEntity("BasicSurplusAdminLoan");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.amount(0);

            return newType;
        };
        var createBasicSurplusAdminBrokeredDeposit = function () {
            var newType = manager.createEntity("BasicSurplusAdminBrokeredDeposit");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.amount(0);

            return newType;
        };
        var createBasicSurplusAdminMisc = function () {
            var newType = manager.createEntity("BasicSurplusAdminMisc");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.amount(0);

            return newType;
        };
        var createBasicSurplusAdminOtherLiquidity = function () {
            var newType = manager.createEntity("BasicSurplusAdminOtherLiquidity");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.marketValue(0);
            newType.pledged(0);
            newType.available(0);

            return newType;
        };
        var createBasicSurplusAdminUnsecuredBorrowing = function () {
            var newType = manager.createEntity("BasicSurplusAdminUnsecuredBorrowing");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.line(0);
            newType.outstanding(0);
            newType.available(0);

            return newType;
        };
        var createBasicSurplusAdminSecuredBorrowing = function () {
            var newType = manager.createEntity("BasicSurplusAdminSecuredBorrowing");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.line(0);
            newType.outstanding(0);
            newType.available(0);

            return newType;
        };
        var createBasicSurplusAdminOtherSecured = function () {
            var newType = manager.createEntity("BasicSurplusAdminOtherSecured");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.amount(0);
            newType.percent(0);

            return newType;
        };
        var createBasicSurplusAdminUnrealizedGL = function () {
            var newType = manager.createEntity("BasicSurplusAdminUnrealizedGL");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.totalPortfolio(0);
            newType.tier1BasicSurplus(0);
            newType.percOfAssets(0);

            return newType;
        };
        var createBasicSurplusAdminBorrowingCapacity = function () {
            var newType = manager.createEntity("BasicSurplusAdminBorrowingCapacity");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.percent(0);
            newType.amount(0);

            return newType;
        };
        var createBasicSurplusAdminFootnote = function () {
            var newType = manager.createEntity("BasicSurplusAdminFootnote");
            newType.override(true);
            newType.title("");
            newType.fieldName(null);

            newType.text("");

            return newType;
        };

        //Copy Basic Surplus Stuff
        var copyBasicSurplus = function (basicSurplusId, observable, newName) {
            var query = entityQuery.from('CopyBasicSurplus')
                .withParameters({
                    basicSurplusId: basicSurplusId,
                    newName: newName
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ret = data.results[0];
                if (observable)
                    observable(ret);
                log('Retrieved [CopyBasicSurplus] from remote data source', data, false);
            }
        };

        //Push Log Stuff
        var getPushLog = function (asOfDate, observableArray) {
            var query = entityQuery.from('GetPushLog')
                .withParameters({
                    asOfDate: asOfDate
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ret = data.results;
                if (observableArray)
                    observableArray(ret);
                log('Retrieved [PushLog] from remote data source', data, false);
            }
        };

        //L360 Push Stuff
        var l360Push = function (simulationTypeId, scenarioTypeId, basicSurplusId, observableArray) {
            var query = entityQuery.from('L360Push')
                .withParameters({
                    simulationTypeId: simulationTypeId,
                    scenarioTypeId: scenarioTypeId,
                    basicSurplusId: basicSurplusId
                });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ret = data.results;
                if (observableArray)
                    observableArray(ret);
                //log('Retrieved [l360Push] from remote data source? Uhh', data, false);
                log('Data Successfully Pushed', data, true);
            }
        };

        //Get Defined Groupings
        var getDefinedGroupings = function (grpObservable) {
            var query = entityQuery.from('DefinedGroupings');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (grpObservable) {
                    grpObservable(data.results);
                }
                log('Retrieved [Defined Groupings] from remote data source',
                    data, false);
            }
        };


        //Get Defined Groupings
        var configDefinedGroupings = function (grpObservable) {
            var query = entityQuery.from('GetDefinedGroupings');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (grpObservable) {
                    grpObservable(data.results);
                }
                log('Retrieved [Config Defined Groupings] from remote data source',
                    data, false);
            }
        };

        //Create Defined Grouping
        var createDefinedGrouping = function () {
            var dg = manager.createEntity("DefinedGrouping");
            dg.name("New Defined Grouping");

            return dg;
        };

        //Get Defined Grouping By Id
        var getDefinedGroupingById = function (id, definedGroupingObservable) {
            var query = entityQuery.from('DefinedGroupings')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var dg = data.results[0];

                //Sort Them In Order They Way the Want Them To Appear
                if (dg.definedGroupingGroups) {
                    dg.definedGroupingGroups.sort(function (a, b) {
                        return a.priority() - b.priority();
                    });
                }

                if (definedGroupingObservable) {
                    definedGroupingObservable(dg);
                }
                log('Retrieved [Define Grouping] from remote data source', data, false);
            }
        };

        //Delete Defined Grouping By Id
        var deleteDefinedGrouping = function (id) {
            var query = entityQuery.from('DeleteDefinedGrouping')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                log('Retrieved [Define Grouping] from remote data source', data, false);
            }
        };


        //Get Defined Grouping Group
        var getDefinedGroupingGroups = function (grpObservable) {
            var query = entityQuery.from('DefinedGroupingGroup');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (grpObservable) {
                    grpObservable(data.results);
                }
                log('Retrieved [Defined Grouping Group] from remote data source',
                    data, false);
            }
        };


        //Delete Defined Grouping By Id
        var deleteDefinedGroupingGroup = function (id) {
            var query = entityQuery.from('DeleteDefinedGroupingGroup')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                log('Retrieved [Define Grouping] from remote data source', data, false);
            }
        };

        //Create Defined Grouping Group
        var createDefinedGroupingGroup = function (definedGroupId, priority) {
            var dg = manager.createEntity("DefinedGroupingGroup");
            dg.name("New Group");
            dg.definedGroupingId(definedGroupId);
            dg.priority(priority);

            return dg;
        };

        //Get Defined Grouping Group By Id
        var getDefinedGroupingGroupById = function (id, definedGroupingObservable) {
            var query = entityQuery.from('DefinedGroupingGroups')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var dg = data.results[0];

                if (definedGroupingObservable) {
                    definedGroupingObservable(dg);
                }
                log('Retrieved [Define Grouping Group] from remote data source', data, false);
            }
        };


        //Get Classifications
        var getClassifications = function (isAsset, classObservable) {
            var query = entityQuery.from('Classifications')
                .withParameters({ isAsset: isAsset != 0 ? true : false });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (classObservable) {
                    classObservable(data.results);
                }
                log('Retrieved [Classifications] from remote data source', data, false);
            }
        };

        //Create Defined Group Classification
        var createDefinedGroupingClassification = function (groupId, cl) {
            var dg = manager.createEntity("DefinedGroupingClassification");
            dg.definedGroupingGroupId(groupId);
            dg.isAsset(cl.isAsset());
            dg.rbcTier(cl.rbcTier());
            dg.acc_Type(cl.accountTypeId());
            dg.name(cl.name());
            return dg;
        };



        var getInstitutions = function (institutionsObservable) {
            var query = entityQuery.from('Institutions');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (institutionsObservable) {
                    institutionsObservable(data.results);
                }
                log('Retrieved [Institutions] from remote data source',
                    data, false);
            }
        };

        var getConsolidationById = function (id, consolidationsObservable) {
            var query = entityQuery.from('Consolidations')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (consolidationsObservable) {
                    consolidationsObservable(data.results);
                }
                log('Retrieved [Consolidations] from remote data source',
                    data, false);
            }
        };

        var getConsolidations = function (consolidationsObservable) {
            var query = entityQuery.from('Consolidations');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (consolidationsObservable) {
                    consolidationsObservable(data.results);
                }
                log('Retrieved [Consolidations] from remote data source',
                    data, false);
            }
        };

        var getPackages = function (packagesObservableArray) {
            var query = entityQuery.from('Packages')
            //.orderBy("asOfDate desc, consolidation.name, name")
            // .expand("Consolidation")
            // .expand("Reports");
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var packages = data.results;
                if (packagesObservableArray) {
                    packagesObservableArray(packages);
                }
                log('Retrieved [Package] from remote data source', data, false);
            }
        };

        var getFirstReportInPackage = function (packageId, firstReport) {
            var query = entityQuery.from('ReportByPackagePriority')
                .withParameters({ packageId: packageId, currentPriority: 0, mode: "First" });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var fr = data.results[0].repId;
                if (firstReport) {
                    firstReport(fr);
                }
                log('Retrieved [First Report In Package] from remote data source', data, false);
            }
        };

        var getPackageById = function (id, packageObservable) {
            try {
                var cachedInvoices = manager.getEntities('Package'); // all invoices in cache
                // This will clear cache so if user opens two packages from different banks with same id
                cachedInvoices.forEach(function (entity) { manager.detachEntity(entity); });
            } catch (e) {
                log(e.description);
            }





            var query = entityQuery.from('PackageById')
                .withParameters({ packageId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var thePackage = data.results[0];

                if (thePackage && thePackage.reports) {
                    thePackage.reports.sort(function (a, b) {
                        return a.priority() - b.priority();
                    });
                }

                if (packageObservable) {
                    packageObservable(thePackage);
                }
                log('Retrieved [Package] from remote data source', data, false);
            }
        };

        var copyPackage = function (sourcePackage, asOfDate, newPackageIdObservable) {
            var query = entityQuery.from('CopyPackage')
                .withParameters({ packageId: sourcePackage, asOfDate: asOfDate });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var thePackage = data.results;
                if (newPackageIdObservable) {
                    newPackageIdObservable(thePackage);
                }
                log('Package copied on remote data source', data, false);
            }
        };

        var newCopyPackage = function (sourcePackage, copyFromTemplates, pkgName, newPackageIdObs) {
            var query = entityQuery.from('NewCopyPackage')
                .withParameters({ pkgIdToCopy: sourcePackage, copyFromTemplates: copyFromTemplates, pkgName: pkgName });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (newPackageIdObs) {
                    newPackageIdObs(data.results);
                }
                log('Package copied on remote data source', data, false);
            }
        };

        var newRollPackage = function (sourcePackage, asOfDate, newPackageIdObs, srcDB, destDB) {
            var query = entityQuery.from('NewRollPackage')
                .withParameters({ pkgIdToCopy: sourcePackage, asOfDate: asOfDate, srcDB: srcDB, destDB: destDB });
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (newPackageIdObs) {
                    newPackageIdObs(data.results);
                }
                log('Package copied on remote data source', data, false);
            }
        };

        var deletePackageById = function (packageId, packagesObservable) {

            var query = entityQuery.from('DeletePackage')
                .withParameters({ packageId: packageId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var thePackage = data.results;

                if (packagesObservable) {
                    packagesObservable(thePackage);
                }
                log('Package deleted from remote data source', data, false);
                return;
            }
        };


        var deleteReportById = function (reportId) {
            var query = entityQuery.from('DeleteReport')
                .withParameters({ reportId: reportId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                return true;
            }
        };

        var deleteReportingItemById = function (sourceName, sourceId, parentName) {
            var query = entityQuery.from('DeleteReportingItem')
                .withParameters({ sourceName: sourceName, sourceId: sourceId, srcDB: "", parentName: parentName });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                return true;
            }
        };

        //var deleteOrInsertReportById = function (isDelete, parentName, parentId) {
        //    var query = entityQuery.from('DeleteOrInsertReport')
        //         .withParameters({ isDelete: false, parentName: parentName, parentId: parentId, srcDB: "", destDB: ""});

        //    return manager.executeQuery(query)
        //        .then(querySucceeded)
        //        .fail(queryFailed);

        //    function querySucceeded(data) {
        //        return true;
        //    }
        //};

        var getReportVmById = function (reportId, reportObservable) {
            var query = entityQuery.from('ReportVm')
                .withParameters({ reportId: reportId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var report = data.results[0];
                if (reportObservable) {
                    reportObservable(report);
                }
                log('Retrieved [Report] from remote data source', data, false);
            }
        };

        var getReportById = function (reportId, reportObservable) {
            var query = entityQuery.from('Reports')
                //.expand("Package")
                .withParameters({ id: reportId });


            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var report = data.results[0];
                if (reportObservable) {
                    reportObservable(report);
                }
                log('Retrieved [Report] from remote data source', data, false);
            }


        };

        var getReportsByPackageId = function (packageId, reportObservable) {
            var query = entityQuery.from('Reports')
                .withParameters({ id: packageId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var report = data.results;
                if (reportObservable) {
                    reportObservable(report);
                }
                log('Retrieved [Report] from remote data source', data, false);
            }
        };

        var getNextReport = function (thisReport, nextReportObservable) {
            var query = entityQuery.from('ReportByPackagePriority')
                .withParameters({ packageID: thisReport().packageId(), currentPriority: thisReport().priority(), mode: "Next" });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var report = data.results[0];
                if (nextReportObservable) {
                    nextReportObservable(report);
                }
                log('Retrieved [Report] from remote data source', data, false);
            }
        };

        var getPreviousReport = function (thisReport, previousReportObservable) {
            var query = entityQuery.from('ReportByPackagePriority')
                .withParameters({ packageID: thisReport().packageId(), currentPriority: thisReport().priority(), mode: "Previous" });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var report = data.results[0];
                if (previousReportObservable) {
                    previousReportObservable(report);
                }
                log('Retrieved [Report] from remote data source', data, false);
            }
        };


        var defaultReport = function (reportId) {
            var query = entityQuery.from('DefaultReport')
                .withParameters({ reportId: reportId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

            }
        };

        var createOneChart = function (packageId, priority, profile) {
            var oneChart = manager.createEntity("OneChart", {
                id: -1
            });
            oneChart.chart(createChart());
            oneChart.chart().institutionDatabaseName(profile().databaseName);
            return oneChart;
        };

        var getOneChartById = function (id, oneChartObservable) {
            var query = entityQuery.from('OneCharts')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var oneChart = data.results[0];
                if (oneChartObservable) {
                    oneChartObservable(oneChart);
                }
                log('Retrieved [OneChart] from remote data source', data, false);
            }
        };

        var getOneChartViewDataById = function (id, oneChartObservable) {

            var query = entityQuery.from('OneChartViewDataById')
                .withParameters({ oneChartId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var oneChart = data.results[0];
                if (oneChartObservable) {
                    oneChartObservable(oneChart);
                }
                log('Retrieved [OneChart] from remote data source', data, false);
            }
        };

        var createTwoChart = function (packageId, priority, profile) {
            var twoChart = manager.createEntity("TwoChart", {
                id: -1
            });
            twoChart.leftChart(createChart());
            twoChart.rightChart(createChart());
            twoChart.leftChart().institutionDatabaseName(profile().databaseName);
            twoChart.rightChart().institutionDatabaseName(profile().databaseName);
            return twoChart;
        };

        var getTwoChartById = function (id, twoChartObservable) {
            var query = entityQuery.from('TwoCharts')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var twoChart = data.results[0];
                if (twoChartObservable) {
                    twoChartObservable(twoChart);
                }
                log('Retrieved [TwoIncomeGraph] from remote data source', data, false);
            }
        };

        var getTwoChartViewDataById = function (id, twoChartObservable) {

            var query = entityQuery.from('TwoChartViewDataById')
                .withParameters({ TwoChartId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var twoChartViewData = data.results[0];
                if (twoChartObservable) {
                    twoChartObservable(twoChartViewData);
                }
                log('Retrieved [TwoChartViewData] from remote data source', data, false);
            }
        };

        var createSimCompare = function (packageId, priority, profile) {
            var simCompare = manager.createEntity("SimCompare", {
                id: -1
            });
            simCompare.leftInstitutionDatabaseName(profile().databaseName);
            simCompare.rightInstitutionDatabaseName(profile().databaseName);
            simCompare.niiOption("nii");
            return simCompare;
        };

        var createLoanCapFloor = function (packageId, priority, profile) {
            var lcf = manager.createEntity("LoanCapFloor", {
                id: -1
            });

            lcf.topInstitutionDatabaseName(profile().databaseName);
            lcf.bottomInstitutionDatabaseName(profile().databaseName);
            lcf.bpEndpoints("50,100,150,200");
            return lcf;
        };

        var createSimCompareSimulationType = function () {
            var simCompareSimulationType = manager.createEntity("SimCompareSimulationType");
            return simCompareSimulationType;
        };

        var getLoanCapFloorById = function (id, loanCapFloorObservable) {
            var query = entityQuery.from('LoanCapFloors')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var loancapfloor = data.results[0];
                if (loanCapFloorObservable) {
                    loanCapFloorObservable(loancapfloor);
                }
                log('Retrieved [loancapfloor] from remote data source', data, false);
            }
        };

        var getLoanCapFloorViewDataById = function (id, loancapfloorObservable) {

            var query = entityQuery.from('LoanCapFloorViewDataById')
                .withParameters({ lcfId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var lcfViewData = data.results[0];
                if (loancapfloorObservable) {
                    loancapfloorObservable(lcfViewData);
                }
                log('Retrieved [LoanCapFloorViewData] from remote data source', data, false);
            }
        };

        var getSimCompareById = function (id, simCompareObservable) {

            var query = entityQuery.from('SimCompares')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var simCompare = data.results[0];
                if (simCompareObservable) {
                    simCompareObservable(simCompare);
                }
                log('Retrieved [simCompare] from remote data source', data, false);
            }
        };

        var getSimCompareViewDataById = function (id, simCompareObservable) {

            var query = entityQuery.from('SimCompareViewDataById')
                .withParameters({ SimCompareId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var simCompareViewData = data.results[0];
                if (simCompareObservable) {
                    simCompareObservable(simCompareViewData);
                }
                log('Retrieved [SimCompareViewData] from remote data source', data, false);
            }
        };

        var createEve = function (packageId, priority, profile) {
            var eve = manager.createEntity("Eve", {
                id: -1,
                institutionDatabaseName: profile().databaseName,
                viewOption: 1,
                policyLimits: true,
                riskSummaryTable: false,
                simulationTypeId: 2
            });

            //automatically include O Shock
            //var scen = createEveScenarioType();
            //scen.scenarioTypeId(30);
            //scen.priority(0);
            //scen.scenarioType = ko.observable({ name: ko.observable('0 Shock') });
            //eve.eveScenarioTypes().push(scen);
            return eve;
        };

        var getEveById = function (id, eveObservable) {

            var query = entityQuery.from('Eves')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var eve = data.results[0];
                if (eveObservable) {
                    eveObservable(eve);
                }
                log('Retrieved [simCompare] from remote data source', data, false);
            }
        };




        var getEveViewDataById = function (id, eveObservable) {

            var query = entityQuery.from('EveViewDataById')
                .withParameters({ EveId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var eveViewData = data.results[0];
                if (eveObservable) {
                    eveObservable(eveViewData);
                }
                log('Retrieved [EveViewData] from remote data source', data, false);
            }
        };

        var createDocument = function (packageId, priority) {
            var doc = manager.createEntity("Document", {
                id: -1
            });
            return doc;
        };

        var getDocumentById = function (id, documentObservable) {

            var query = entityQuery.from('Documents')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var document = data.results[0];
                if (documentObservable) {
                    documentObservable(document);
                }
                log('Retrieved [simCompare] from remote data source', data, false);
            }
        };
        var getDocumentViewById = function (id, documentObservable) {

            var query = entityQuery.from('DocumentsViewById')
                .withParameters({ docId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var document = data.results[0];
                if (documentObservable) {
                    documentObservable(document);
                }
                log('Retrieved [simCompare] from remote data source', data, false);
            }
        };

        var createBalanceSheetCompare = function (packageId, priority, profile) {
            var balanceSheetCompare = manager.createEntity("BalanceSheetCompare", {
                id: -1,
                leftSimulationTypeId: 1,
                rightSimulationTypeId: 1
            });

            //Set Default Instititions
            balanceSheetCompare.leftInstitutionDatabaseName(profile().databaseName);
            balanceSheetCompare.rightInstitutionDatabaseName(profile().databaseName);

            //Set Report Defaults
            balanceSheetCompare.leftAsOfDateOffset(0);
            //balanceSheetCompare.leftSimulationTypeId(0); //These normally defaulted by in-report dropdowns
            //balanceSheetCompare.leftScenarioTypeId(0);
            balanceSheetCompare.leftStartDate(profile().asOfDate);
            balanceSheetCompare.leftEndDate(profile().asOfDate);
            balanceSheetCompare.rightAsOfDateOffset(0);
            //balanceSheetCompare.rightSimulationTypeId(0);
            //balanceSheetCompare.rightScenarioTypeId(0);
            balanceSheetCompare.rightStartDate(profile().asOfDate);
            balanceSheetCompare.rightEndDate(profile().asOfDate);

            balanceSheetCompare.viewOption(0);
            balanceSheetCompare.balanceType(0);
            balanceSheetCompare.taxEqivalentYields(true);
            balanceSheetCompare.balances(true);
            balanceSheetCompare.rates(true);

            return balanceSheetCompare;
        };

        var getBalanceSheetCompareById = function (id, balanceSheetCompareObservable) {
            var query = entityQuery.from('BalanceSheetCompares')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var balanceSheetCompare = data.results[0];
                if (balanceSheetCompareObservable) {
                    balanceSheetCompareObservable(balanceSheetCompare);
                }
                log('Retrieved [BalanceSheetCompares] from remote data source', data, false);
            }
        };

        var getBalanceSheetCompareViewDataById = function (id, balanceSheetCompareObservable) {

            var query = entityQuery.from('BalanceSheetCompareViewDataById')
                .withParameters({ BalanceSheetCompareId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var balanceSheetCompareViewData = data.results[0];
                if (balanceSheetCompareObservable) {
                    balanceSheetCompareObservable(balanceSheetCompareViewData);
                }
                log('Retrieved [BalanceSheetCompareViewData] from remote data source', data, false);
            }
        };


        var createBalanceSheetMix = function (packageId, priority, profile) {
            var balanceSheetMix = manager.createEntity("BalanceSheetMix", {
                id: -1
            });
            balanceSheetMix.asOfDateOffset(0);
            balanceSheetMix.priorAsOfDateOffset(4);
            balanceSheetMix.liabilityOption('0');
            balanceSheetMix.institutionDatabaseName(profile().databaseName);
            return balanceSheetMix;
        };

        var getBalanceSheetMixById = function (id, balanceSheetMixObservable) {
            var query = entityQuery.from('BalanceSheetMixes')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var balanceSheetMix = data.results[0];
                if (balanceSheetMixObservable) {
                    balanceSheetMixObservable(balanceSheetMix);
                }
                log('Retrieved [BalanceSheetMixes] from remote data source', data, false);
            }
        };

        var getBalanceSheetMixViewDataById = function (id, balanceSheetMixObservable) {

            var query = entityQuery.from('BalanceSheetMixViewDataById')
                .withParameters({ BalanceSheetMixId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var balanceSheetMixViewData = data.results[0];
                if (balanceSheetMixObservable) {
                    balanceSheetMixObservable(balanceSheetMixViewData);
                }
                log('Retrieved [BalanceSheetMixViewData] from remote data source', data, false);
            }
        };

        //Basic Surplus Report
        var createBasicSurplusReport = function (packageId, priority, profile) {
            var basicSurplusReport = manager.createEntity("BasicSurplusReport", {
                id: -1,
                institutionDatabaseName: profile().databaseName
            });

            return basicSurplusReport;
        };

        var getBasicSurplusReportById = function (id, basicSurplusReportObservable) {
            var query = entityQuery.from('BasicSurplusReports')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var basicSurplusReport = data.results[0];
                if (basicSurplusReportObservable)
                    basicSurplusReportObservable(basicSurplusReport);
                log('Retrieved [BasicSurplusReports] from remote data source', data, false);
            }
        };

        var getBasicSurplusDefinedInPoliciesFromPreviousQuarter = function (bsObs) {
            return new Promise(function (resolve, reject) {
                var holder = ko.observable();

                getAsOfDates(holder, '', true, 1, true).then(() => {
                    if (!holder().length) {
                        resolve(null);
                        return;
                    }

                    var aod = holder()[0].asOfDateDescript;

                    if (!aod || aod.toLowerCase() == "n/a") {
                        resolve(null);
                        return;
                    }

                    getPolicy(holder, aod).then(() => {
                        var basicSurplusAdminID = holder && holder().basicSurplusAdminId && holder().basicSurplusAdminId();

                        if (!basicSurplusAdminID || basicSurplusAdminID <= 0) {
                            resolve(null);
                            return;
                        }

                        getBasicSurplusAdminById(basicSurplusAdminID, '', holder).then(() => {
                            bsObs(holder());
                            resolve(holder);
                        });
                    });
                }).catch(e => { reject(e); });
            });
        };

        var getBasicSurplusReportViewDataById = function (id, basicSurplusReportObservable) {
            var query = entityQuery.from('BasicSurplusReportViewDataById')
                .withParameters({ BasicSurplusReportId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var basicSurplusReportViewData = data.results[0];
                if (basicSurplusReportObservable)
                    basicSurplusReportObservable(basicSurplusReportViewData);
                log('Retrieved [BasicSurplusReportViewData] from remote data source', data, false);
            }
        };

        //Interest Rate Policy Guidelines Report
        var createInterestRatePolicyGuidelines = function (packageId, priority, profile) {
            var interestRatePolicyGuidelines = manager.createEntity("InterestRatePolicyGuidelines", {
                id: -1,
                institutionDatabaseName: profile().databaseName
            });
            // interestRatePolicyGuidelines.institutionDatabaseName();

            return interestRatePolicyGuidelines;
        };

        var getInterestRatePolicyGuidelinesById = function (id, interestRatePolicyGuidelinesObservable) {
            var query = entityQuery.from('InterestRatePolicyGuidelines')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var interestRatePolicyGuidelines = data.results[0];
                if (interestRatePolicyGuidelinesObservable)
                    interestRatePolicyGuidelinesObservable(interestRatePolicyGuidelines);
                log('Retrieved [InterestRatePolicyGuidelines] from remote data source', data, false);
            }
        };

        var getInterestRatePolicyGuidelinesViewDataById = function (id, interestRatePolicyGuidelinesObservable) {
            var query = entityQuery.from('InterestRatePolicyGuidelinesViewDataById')
                .withParameters({ InterestRatePolicyGuidelinesId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var interestRatePolicyGuidelinesViewData = data.results[0];
                if (interestRatePolicyGuidelinesObservable)
                    interestRatePolicyGuidelinesObservable(interestRatePolicyGuidelinesViewData);
                log('Retrieved [InterestRatePolicyGuidelinesViewData] from remote data source', data, false);
            }
        };

        //Interest Rate Policy Guidelines Create Types
        var createInterestRatePolicyGuidelinesDate = function () {
            return manager.createEntity("InterestRatePolicyGuidelinesDate");
        };
        var createInterestRatePolicyGuidelinesNIIComparative = function () {
            return manager.createEntity("InterestRatePolicyGuidelinesNIIComparative", {
                graph: true
            });
        };
        var createInterestRatePolicyGuidelinesNIIScenarioType = function () {
            return manager.createEntity("InterestRatePolicyGuidelinesNIIScenarioType");
        };
        var createInterestRatePolicyGuidelinesNIIScenario = function () {
            return manager.createEntity("InterestRatePolicyGuidelinesNIIScenario", {
                graph: true
            });
        };
        var createInterestRatePolicyGuidelinesEVEComparative = function () {
            return manager.createEntity("InterestRatePolicyGuidelinesEVEComparative", {
                graph: true
            });
        };
        var createInterestRatePolicyGuidelinesEVEScenario = function () {
            return manager.createEntity("InterestRatePolicyGuidelinesEVEScenario", {
                graph: true
            });
        };

        //Static gap Block
        var createStaticGap = function (profile) {
            var staticGap = manager.createEntity("StaticGap", {
                id: -1
            });
            //Set Report Package and Package Order
            // staticGap.packageId(packageId);
            // staticGap.priority(priority);

            //Set Default Instititions
            staticGap.leftInstitutionDatabaseName(profile().databaseName);
            staticGap.rightInstitutionDatabaseName(profile().databaseName);

            //Set Report Defaults
            staticGap.reportFormat(0);
            staticGap.viewOption(0);
            staticGap.taxEqivalentYields(true);
            staticGap.gapRatios(true);

            return staticGap;
        };

        var getStaticGapById = function (id, staticGapObservable) {
            var query = entityQuery.from('StaticGaps')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var staticGap = data.results[0];
                if (staticGapObservable) {
                    staticGapObservable(staticGap);
                }
                log('Retrieved [StaticGap] from remote data source', data, false);
            }
        };

        var getStaticGapViewDataById = function (id, staticGapObservable) {

            var query = entityQuery.from('StaticGapViewDataById')
                .withParameters({ staticGapId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var staticGapViewData = data.results[0];
                if (staticGapObservable) {
                    staticGapObservable(staticGapViewData);
                }
                log('Retrieved [StaticGapViewData] from remote data source', data, false);
            }
        };

        //Funding Matrix Block
        var createFundingMatrix = function (packageId, priority, profile) {
            var fundingMatrix = manager.createEntity("FundingMatrix", {
                id: -1
            });

            //Set Default Instititions
            //fundingMatrix.leftInstitutionDatabaseName(profile().atlasDatabase);
            //fundingMatrix.rightInstitutionDatabaseName(profile().atlasDatabase);

            return fundingMatrix;
        };

        var createFundingMatrixOffset = function () {
            var offset = manager.createEntity("FundingMatrixOffset");
            return offset;
        };

        var getFundingMatrixById = function (id, fundingMatrixObservable) {
            var query = entityQuery.from('FundingMatrices')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var fundingMatrix = data.results[0];
                if (fundingMatrixObservable) {
                    fundingMatrixObservable(fundingMatrix);
                }
                log('Retrieved [FundingMatrix] from remote data source', data, false);
            }
        };

        var getFundingMatrixViewDataById = function (id, fundingMatrixObservable) {

            var query = entityQuery.from('FundingMatrixViewDataById')
                .withParameters({ fundingMatrixId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var fundingMatrixViewData = data.results[0];
                if (fundingMatrixObservable) {
                    fundingMatrixObservable(fundingMatrixViewData);
                }
                log('Retrieved [FundingMatrixViewData] from remote data source', data, false);
            }
        };

        //Simulation Summary Block
        var createSimulationSummary = function (packageId, priority, profile) {
            var simulationSummary = manager.createEntity("SimulationSummary", {
                id: -1
            });

            //Set Default Instititions
            simulationSummary.institutionDatabaseName(profile().databaseName);


            //Set Report Defaults
            simulationSummary.reportSelection(0);
            simulationSummary.grouping(0);
            simulationSummary.taxEquivalent(true);
            simulationSummary.monthlyQuarterly(0);


            return simulationSummary;
        };

        var getSimulationSummaryById = function (id, simSumObservable) {
            var query = entityQuery.from('SimulationSummaries')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var simSum = data.results[0];
                if (simSumObservable) {
                    simSumObservable(simSum);
                }
                log('Retrieved [SimulationSummary] from remote data source', data, false);
            }
        };

        var getSimulationSummaryViewDataById = function (id, simSumObservable) {

            var query = entityQuery.from('SimulationSummaryViewDataById')
                .withParameters({ simSumId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var simSumViewData = data.results[0];
                if (simSumObservable) {
                    simSumObservable(simSumViewData);
                }
                log('Retrieved [SimulationSummaryViewData] from remote data source', data, false);
            }
        };




        //Time Deposit Migration Block
        var createTimeDepositMigration = function (packageId, priority, profile) {
            var timeDep = manager.createEntity("TimeDepositMigration", {
                id: -1
            });

            //Defaults For Report
            timeDep.institutionDatabaseName(profile().databaseName);
            timeDep.priorAsOfDateOffset(1);
            timeDep.cD(true);
            timeDep.jumboCD(true);
            timeDep.publicCD(true);
            timeDep.publicJumbo(true);
            timeDep.nationalCD(true);
            timeDep.onlineCD(true);
            timeDep.onlineJumbo(true);
            timeDep.brokeredCD(true);
            timeDep.brokeredOneWayCD(true);
            timeDep.brokeredReciprocalCD(true);
            timeDep.otherTimeDeposits(true);
            timeDep.regular(true);
            timeDep.special(true);
            return timeDep;
        };

        var getTimeDepositMigrationById = function (id, timeDepObservable) {
            var query = entityQuery.from('TimeDepositMigrations')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var timeDep = data.results[0];
                if (timeDepObservable) {
                    timeDepObservable(timeDep);
                }
                log('Retrieved [TimeDepositMigration] from remote data source', data, false);
            }
        };

        var getTimeDepositMigrationViewDataById = function (id, timeDepObservable) {

            var query = entityQuery.from('TimeDepositMigrationViewDataById')
                .withParameters({ timeDepositId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var timeDepViewData = data.results[0];
                if (timeDepObservable) {
                    timeDepObservable(timeDepViewData);
                }
                log('Retrieved [TimeDepositMigrationViewData] from remote data source', data, false);
            }
        };

        //Core Funding Utilization Block
        var createCoreFundingUtilization = function (packageId, priority, profile) {
            var coreFund = manager.createEntity("CoreFundingUtilization", {
                id: -1
            });
            coreFund.institutionDatabaseName(profile().databaseName);
            coreFund.assetDetails("0");

            return coreFund;
        };

        var getCoreFundingUtilizationById = function (id, coreFundObservable) {
            var query = entityQuery.from('CoreFundingUtilizations')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var coreFund = data.results[0];
                if (coreFundObservable) {
                    coreFundObservable(coreFund);
                }
                log('Retrieved [CoreFundingUtilization] from remote data source', data, false);
            }
        };

        var getCoreFundingUtilizationViewDataById = function (id, coreFundObservable) {

            var query = entityQuery.from('CoreFundingUtilizationViewDataById')
                .withParameters({ coreFundId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var coreFundViewData = data.results[0];
                if (coreFundObservable) {
                    coreFundObservable(coreFundViewData);
                }
                log('Retrieved [CoreFundingUtilizationViewData] from remote data source', data, false);
            }
        };

        var createCoreFundingUtilizationScenarioType = function () {
            var series = manager.createEntity("CoreFundingUtilizationScenarioType");
            return series;
        };

        var createCoreFundingUtilizationDateOffSet = function () {
            var offset = manager.createEntity("CoreFundingUtilizationDateOffSets");
            return offset;
        };




        //Capital Analysis
        var createCapitalAnalysis = function (packageId, priority, profile) {

            var cp = manager.createEntity("CapitalAnalysis", {
                id: -1
            });
            cp.institutionDatabaseName(profile().databaseName);
            return cp;
        };

        var getCapitalAnalysisById = function (id, cpObservable) {
            var query = entityQuery.from('CapitalAnalysis')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var cp = data.results[0];
                if (cpObservable) {
                    cpObservable(cp);
                }
                log('Retrieved [Capital Analysis] from remote data source', data, false);
            }
        };

        var getCapitalAnalysisViewDataById = function (id, cpObservable) {

            var query = entityQuery.from('CapitalAnalysisViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var cpViewData = data.results[0];
                if (cpObservable) {
                    cpObservable(cpViewData);
                }
                log('Retrieved [CapitalAnalysisViewData] from remote data source', data, false);
            }
        };

        var createCapitalAnalysisBufferOffSet = function () {
            var offset = manager.createEntity("CapitalAnalysisHistoricalBufferOffsets");
            return offset;
        };

        //Historical Balance Sheet NII Blocks
        var createHistoricalBalanceSheetNII = function (packageId, priority, profile) {
            var histBal = manager.createEntity("HistoricalBalanceSheetNII", {
                id: -1
            });

            //we want to start the report with two simulations
            var sim1 = manager.createEntity("HistoricalBalanceSheetNIISimulation");
            var sim2 = manager.createEntity("HistoricalBalanceSheetNIISimulation");
            var leftChart = manager.createEntity("Chart");
            var rightChart = manager.createEntity("Chart");
            sim1.priority(0);
            sim2.priority(1);
            histBal.historicalBalanceSheetNIISimulations.push(sim1);
            histBal.historicalBalanceSheetNIISimulations.push(sim2);

            histBal.leftChart(leftChart);
            histBal.rightChart(rightChart);

            //Set Report Package and Package Order
            histBal.institutionDatabaseName(profile().databaseName);
            histBal.leftChart().institutionDatabaseName(histBal.institutionDatabaseName());
            histBal.rightChart().institutionDatabaseName(histBal.institutionDatabaseName());
            //report defaults
            histBal.balanceSheetAssetViewOption("0");
            histBal.balanceSheetLiabilityViewOption("0");
            histBal.nIIAssetViewOption("0");
            histBal.nIILiabilityViewOption("0");


            return histBal;
        };

        var getHistoricalBalanceSheetNIIById = function (id, histBalObservable) {
            var query = entityQuery.from('HistoricalBalanceSheetNIIs')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var histBal = data.results[0];
                if (histBalObservable) {
                    histBalObservable(histBal);
                }
                log('Retrieved [Historical Balance Sheet NII] from remote data source', data, false);
            }
        };

        var getHistoricalBalanceSheetNIIViewDataById = function (id, histBalObservable) {

            var query = entityQuery.from('HistoricalBalanceSheetNIIViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var histBalViewData = data.results[0];
                if (histBalObservable) {
                    histBalObservable(histBalViewData);
                }
                log('Retrieved [Historical Balance Sheet NII view Data] from remote data source', data, false);
            }
        };

        var createHistoricalBalanceSheetNIISimulation = function () {
            var series = manager.createEntity("HistoricalBalanceSheetNIISimulation");
            return series;
        };

        var createHistoricalBalanceSheetNIIScenario = function () {
            var offset = manager.createEntity("HistoricalBalanceSheetNIIScenario");
            return offset;
        };

        var createHistoricBalanceSheetNIITopFootNote = function () {
            var footnote = manager.createEntity("HistoricalBalanceSheetNIITopFootNotes");
            return footnote;
        };

        var createHistoricBalanceSheetNIIBottomFootNote = function () {
            var footnote = manager.createEntity("HistoricalBalanceSheetNIIBottomFootNotes");
            return footnote;
        };


        //Yield Curve Shift Block
        var createYieldCurveShift = function (packageId, priority) {
            var yieldCurve = manager.createEntity("YieldCurveShift", {
                id: -1
            });

            return yieldCurve;
        };

        var getYieldCurveShiftById = function (id, yieldCurveObservable) {
            var query = entityQuery.from('YieldCurveShifts')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var yieldCurve = data.results[0];
                if (yieldCurveObservable) {
                    yieldCurveObservable(yieldCurve);
                }
                log('Retrieved [YieldCurveShift] from remote data source', data, false);
            }
        };

        var getYieldCurveShiftViewDataById = function (id, yieldShiftObservable) {

            var query = entityQuery.from('YieldCurveShiftViewDataById')
                .withParameters({ yieldShiftId: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var yieldShiftViewData = data.results[0];
                if (yieldShiftObservable) {
                    yieldShiftObservable(yieldShiftViewData);
                }
                log('Retrieved [YieldCurveShiftViewData] from remote data source', data, false);
            }
        };

        var createYieldCurveShiftScenarioType = function () {
            var series = manager.createEntity("YieldCurveShiftScenarioType");
            return series;
        };

        var createYieldCurveShiftPeriodCompare = function () {
            var offset = manager.createEntity("YieldCurveShiftPeriodCompare");
            return offset;
        };

        var createYieldCurveShiftHistoricDate = function () {
            var offset = manager.createEntity("YieldCurveShiftHistoricDates");
            return offset;
        };


        //Cover page Block
        var createCoverPage = function (packageId, priority, packageName, profile) {
            var coverPage = manager.createEntity("CoverPage", {
                id: -1
            });
            try {
                //Set Report Package and Package Order
                coverPage.packageName(packageName);
                //Set Default Instititions
                coverPage.institutionDatabaseName(profile().databaseName);
                coverPage.asOfDate(profile().asOfDate);
            }

            catch (err) {
                alert(err);
            }

            return coverPage;
        };

        var getCoverPageById = function (id, coverPageObservable) {
            var query = entityQuery.from('CoverPages').withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var coverPage = data.results[0];
                if (coverPageObservable) {
                    coverPageObservable(coverPage);
                }
                log('Retrieved [CoverPage] from remote data source', data, false);
            }
        };

        //Section Page
        var createSectionPage = function () {
            var sp = manager.createEntity("SectionPage", {
                id: -1
            });
            return sp;
        };

        var getSectionPageById = function (id, spObservable) {
            var query = entityQuery.from('SectionPages')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sp = data.results[0];
                if (spObservable) {
                    spObservable(sp);
                }
                log('Retrieved [Sectoin Page] from remote data source', data, false);
            }
        };


        //Eve Nev Assumptions
        var createEveNevAssumptions = function (packageId, priority, profile) {
            var eve = manager.createEntity("EveNevAssumptions", {
                id: -1
            });
            //Set Report Package and Package Order
            eve.asOfDateOffset(0);
            eve.override(false);
            //Set Default Instititions
            eve.institutionDatabaseName(profile().databaseName);
            return eve;
        };

        var getEveNevAssumptionsById = function (id, eveObservable) {
            var query = entityQuery.from('EveNevAssumptions')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var eve = data.results[0];
                if (eveObservable) {
                    eveObservable(eve);
                }
                log('Retrieved [CoverPage] from remote data source', data, false);
            }
        };

        var getEveNevAssumptionsViewDataById = function (id, eveObservable) {
            var query = entityQuery.from('EveNevAssumptionsViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var eve = data.results[0];
                if (eveObservable) {
                    eveObservable(eve);
                }
                log('Retrieved [Eve Nev Assumptions] from remote data source', data, false);
            }
        };

        //Cashflow Report
        var createCashflowReport = function (packageId, priority, profile) {
            var cashflow = manager.createEntity("CashflowReport", {
                id: -1
            });

            return cashflow;
        };

        var getCashflowReportById = function (id, cashflowObservable) {
            var query = entityQuery.from('CashflowReports')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var cashflow = data.results[0];
                if (cashflowObservable) {
                    cashflowObservable(cashflow);
                }
                log('Retrieved [Cashflow Report] from remote data source', data, false);
            }
        };

        var getCashflowReportViewDataById = function (id, cashflowObservable) {
            var query = entityQuery.from('CashflowReportViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var cashflowViewData = data.results[0];
                if (cashflowObservable) {
                    cashflowObservable(cashflowViewData);
                }
                log('Retrieved [Cashflow View Data] from remote data source', data, false);
            }
        };

        var createCashflowSimulationScenario = function () {
            var cf = manager.createEntity("CashflowSimulationScenario");
            return cf;
        };

        //Net Cashflow 
        var createNetCashflow = function (packageId, priority, profile) {
            var cashflow = manager.createEntity("NetCashflow", {
                id: -1
            });

            return cashflow;
        };

        var createNetCashflowSimulationScenario = function () {
            var ncf = manager.createEntity("NetCashflowSimulationScenario");
            return ncf;
        };

        var getNetCashflowById = function (id, cashflowObservable) {

            var query = entityQuery.from('NetCashflow')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var cashflow = data.results[0];
                if (cashflowObservable) {
                    cashflowObservable(cashflow);
                }
                log('Retrieved [Net Cashflow Report] from remote data source', data, false);
            }
        };

        var getNetCashflowViewDataById = function (id, cashflowObservable) {
            var query = entityQuery.from('NetCashflowViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var cashflowViewData = data.results[0];
                if (cashflowObservable) {
                    cashflowObservable(cashflowViewData);
                }
                log('Retrieved [Net Cashflow View Data] from remote data source', data, false);
            }
        };

        //SummaryRate
        var createSummaryRate = function (packageId, priority, profile) {
            var sr = manager.createEntity("SummaryRate", {
                id: -1
            });
            //Set Report Package and Package Order
            sr.institutionDatabaseName(profile().databaseName);

            return sr;
        };

        var getSummaryRateById = function (id, srObservable) {
            var query = entityQuery.from('SummaryRates')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (srObservable) {
                    srObservable(sr);
                }
                log('Retrieved [Summary Rate] from remote data source', data, false);
            }
        };

        var getSummaryRateViewDataById = function (id, srObservable) {
            var query = entityQuery.from('SummaryRateViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var srViewData = data.results[0];
                if (srObservable) {
                    srObservable(srViewData);
                }
                log('Retrieved [Summary Rate View Data] from remote data source', data, false);
            }
        };

        var createSummaryRateScenario = function () {
            var cf = manager.createEntity("SummaryRateScenario");
            return cf;
        };


        //ASC825 Balance Sheet
        var createASC825BalanceSheet = function (packageId, priority, profile) {
            var as = manager.createEntity("ASC825BalanceSheet", {
                id: -1,
                simulationTypeId: 2
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getASC825BalanceSheetById = function (id, asObservable) {

            var query = entityQuery.from('ASC825BalanceSheet')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Balance Sheet] from remote data source', data, false);
            }
        };

        var getASC825BalanceSheetViewDataById = function (id, asObservable) {
            var query = entityQuery.from('ASC825BalanceSheetViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 View Data] from remote data source', data, false);
            }
        };

        //ASC825 Data Source
        var createASC825DataSource = function (packageId, priority, profile) {
            var as = manager.createEntity("ASC825DataSource", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getASC825DataSourceById = function (id, asObservable) {

            var query = entityQuery.from('ASC825DataSource')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Data Source] from remote data source', data, false);
            }
        };

        var getASC825DataSourceViewDataById = function (id, asObservable) {
            var query = entityQuery.from('ASC825DataSourceViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Data Source View Data] from remote data source', data, false);
            }
        };


        //ASC825 Assumption Methods
        var createASC825AssumptionMethods = function (packageId, priority, profile) {
            var as = manager.createEntity("ASC825AssumptionMethods", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getASC825AssumptionMethodsById = function (id, asObservable) {

            var query = entityQuery.from('ASC825AssumptionMethods')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Assumption Methods] from remote data source', data, false);
            }
        };

        var getASC825AssumptionMethodsViewDataById = function (id, asObservable) {
            var query = entityQuery.from('ASC825AssumptionMethodsViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Assumption Methods View Data] from remote data source', data, false);
            }
        };


        //Assumption Methods
        var createAssumptionMethods = function (packageId, priority, profile) {
            var as = manager.createEntity("AssumptionMethods", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getAssumptionMethodsById = function (id, asObservable) {

            var query = entityQuery.from('AssumptionMethods')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Assumption Methods] from remote data source', data, false);
            }
        };

        var getAssumptionMethodsViewDataById = function (id, asObservable) {
            var query = entityQuery.from('AssumptionMethodsViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Assumption Methods View Data] from remote data source', data, false);
            }
        };


        //Liability Pricing Analysis
        var createLiabilityPricingAnalysis = function (packageId, priority, profile) {
            var as = manager.createEntity("LiabilityPricingAnalysis", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            as.asOfDateOffset(0);
            as.comparativeDateOffset(1);
            return as;
        };

        var getLiabilityPricingAnalysisById = function (id, asObservable) {

            var query = entityQuery.from('LiabilityPricingAnalysis')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Liability Pricing Analysis] from remote data source', data, false);
            }
        };

        var getLiabilityPricingAnalysisViewDataById = function (id, asObservable) {
            var query = entityQuery.from('LiabilityPricingAnalysisViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Liability Pricing Analysis View Data] from remote data source', data, false);
            }
        };

        var getInstLiabilityPricings = function (offset, compOffset, databaseName, asObservable) {
            var query = entityQuery.from('InstLiabilityPricings')
                .withParameters({ offset: offset, compOffset: compOffset, databaseName: databaseName });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results;
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Liab Pricing Data Recieved] from remote data source', data, false);
            }
        };

        var getMostRecentWebRateDate = function (obs) {
            var query = entityQuery.from('MostRecentWebRateDate');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results;
                if (obs) {
                    obs(as);
                }
                log('Retrieved [Got most recent web rate date] from remote data source', data, false);
            }
        };


        //ASC825 Discount Rate
        var createASC825DiscountRate = function (packageId, priority, profile) {
            var as = manager.createEntity("ASC825DiscountRate", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getASC825DiscountRateById = function (id, asObservable) {

            var query = entityQuery.from('ASC825DiscountRate')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Discount Rate] from remote data source', data, false);
            }
        };

        var getASC825DiscountRateViewDataById = function (id, asObservable) {
            var query = entityQuery.from('ASC825DiscountRateViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Discount Rate View Data] from remote data source', data, false);
            }
        };


        //ASC825 Worksheet
        var createASC825Worksheet = function (packageId, priority, profile) {
            var as = manager.createEntity("ASC825Worksheet", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getASC825WorksheetById = function (id, asObservable) {

            var query = entityQuery.from('ASC825Worksheet')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Worksheet] from remote data source', data, false);
            }
        };

        var getASC825WorksheetViewDataById = function (id, asObservable) {
            var query = entityQuery.from('ASC825WorksheetViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Worksheet View Data] from remote data source', data, false);
            }
        };


        //ASC825 MarketValue
        var createASC825MarketValue = function (packageId, priority, profile) {
            var as = manager.createEntity("ASC825MarketValue", {
                id: -1,
                simulationTypeId: 2
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);

            return as;
        };

        var getASC825MarketValueById = function (id, asObservable) {

            var query = entityQuery.from('ASC825MarketValue')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Market Value] from remote data source', data, false);
            }
        };

        var getASC825MarketValueViewDataById = function (id, asObservable) {
            var query = entityQuery.from('ASC825MarketValueViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [ASC825 Market Value View Data] from remote data source', data, false);
            }
        };

        var createASC825MarketValueScenario = function () {
            var cf = manager.createEntity("ASC825MarketValueScenarioType");
            return cf;
        };


        //Rate Change Matrix
        var createRateChangeMatrix = function (packageId, priority, profile) {
            var as = manager.createEntity("RateChangeMatrix", {
                id: -1,
                simulationTypeId: 2
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getRateChangeMatrixById = function (id, asObservable) {

            var query = entityQuery.from('RateChangeMatrix')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Rate Change Matrix] from remote data source', data, false);
            }
        };

        var getRateChangeMatrixViewDataById = function (id, asObservable) {
            var query = entityQuery.from('RateChangeMatrixViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Rate Change Matrix View Data] from remote data source', data, false);
            }
        };

        var createRateChangeMatrixScenario = function () {
            var cf = manager.createEntity("RateChangeMatrixScenario");

            return cf;
        };

        //Investment Portfolio Valuation
        var createInvestmentPortfolioValuation = function (packageId, priority, profile) {
            var as = manager.createEntity("InvestmentPortfolioValuation", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getInvestmentPortfolioValuationById = function (id, asObservable) {

            var query = entityQuery.from('InvestmentPortfolioValuation')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Investment Portfolio Valuation] from remote data source', data, false);
            }
        };

        var getInvestmentPortfolioValuationViewDataById = function (id, asObservable) {
            var query = entityQuery.from('InvestmentPortfolioValuationViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Investment Portfolio Valuation View Data] from remote data source', data, false);
            }
        };

        var createInvestmentPortfolioValuationScenario = function () {
            var cf = manager.createEntity("InvestmentPortfolioValuationScenario");
            return cf;
        };

        //Investment Detail
        var createInvestmentDetail = function (packageId, priority, profile) {
            var as = manager.createEntity("InvestmentDetail", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getInvestmentDetailById = function (id, asObservable) {

            var query = entityQuery.from('InvestmentDetail')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Investment DEtail] from remote data source', data, false);
            }
        };

        var getInvestmentDetailViewDataById = function (id, asObservable) {
            var query = entityQuery.from('InvestmentDetailViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Investment DEtail View Data] from remote data source', data, false);
            }
        };

        //Investment Error
        var createInvestmentError = function (packageId, priority, profile) {
            var as = manager.createEntity("InvestmentError", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var getInvestmentErrorById = function (id, asObservable) {

            var query = entityQuery.from('InvestmentError')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Investment Error] from remote data source', data, false);
            }
        };

        var getInvestmentErrorViewDataById = function (id, asObservable) {
            var query = entityQuery.from('InvestmentErrorViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Investment Error View Data] from remote data source', data, false);
            }
        };


        //Prepaymeny Details
        var createPrepaymentDetails = function (packageId, priority, profile) {
            var pd = manager.createEntity("PrepaymentDetails", {
                id: -1
            });
            //Set Report Package and Package Order
            pd.asOfDateOffset(0);
            //Set Default Instititions
            pd.institutionDatabaseName(profile().databaseName);
            pd.simulationTypeId(2);
            return pd;
        };

        var getPrepaymentDetailsById = function (id, pdObservable) {
            var query = entityQuery.from('PrepaymentDetails')
                .withParameters({ id: id });
            //.where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var pd = data.results[0];
                if (pdObservable) {
                    pdObservable(pd);
                }
                log('Retrieved [SimulationSummary] from remote data source', data, false);
            }
        };

        var getPrepaymentDetailsViewDataById = function (id, pdObservable) {
            var query = entityQuery.from('PrepaymentDetailsViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var pd = data.results[0];
                if (pdObservable) {
                    pdObservable(pd);
                }
                log('Retrieved [CoverPage] from remote data source', data, false);
            }
        };
        var getPrepaymentDetailsById = function (id, pdObservable) {
            var query = entityQuery.from('PrepaymentDetails')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var pd = data.results[0];
                if (pdObservable) {
                    pdObservable(pd);
                }
                log('Retrieved [CoverPage] from remote data source', data, false);
            }
        };

        var createPrepyamentDetailsScenarioType = function () {
            var pdScenarioType = manager.createEntity("PrepaymentDetailsScenarioType");
            return pdScenarioType;
        };

        var getPrepaymentDetailsScenarioTypes = function (scenarioTypeObservableArray) {

            var query = entityQuery.from('ScenarioTypes')
                .where('isEve', '==', true);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

                var scenaroTypes = data.results;
                if (scenarioTypeObservableArray) {
                    scenarioTypeObservableArray(scenaroTypes);
                }
                log('Retrieved [ScenarioType] from remote data source', data, false);
            }
        }; var getFiles = function (filesObservable) {

            var query = entityQuery.from('Files');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var files = data.results;

                if (filesObservable) {
                    filesObservable(files);
                }
                log('Retrieved [Files] from remote data source', data, false);
            }
        };



        //NII Recon
        var checkNiiSimulation = function (id, observable) {
            var query = entityQuery.from('CheckNiiSimulation').withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (observable) {
                    observable(sr);
                }
            }
        };

        var createNIIRecon = function (packageId, priority, profile, modelSetup) {
            var sr = manager.createEntity("NIIRecon", {
                id: -1
            });
            //Set Report Package and Package Order
            sr.institutionDatabaseName(profile().databaseName);
            sr.taxEquiv(modelSetup().reportTaxEquivalent());

            return sr;
        };

        var getNIIReconById = function (id, srObservable) {
            var query = entityQuery.from('NIIRecons')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (srObservable) {
                    srObservable(sr);
                }
                log('Retrieved [Summary Rate] from remote data source', data, false);
            }
        };

        var createNIIReconScenario = function () {
            var cf = manager.createEntity("NIIReconScenarioType");
            return cf;
        };




        //Liquidity Projection
        var createLiquidityProjection = function (packageId, priority, profile) {
            var as = manager.createEntity("LiquidityProjection", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            return as;
        };

        var createLiquidityProjectionDateOffset = function () {
            var offset = manager.createEntity("LiquidityProjectionDateOffset");
            return offset;
        };

        var getLiquidityProjectionById = function (id, asObservable) {

            var query = entityQuery.from('LiquidityProjections')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Liquidity Projection] from remote data source', data, false);
            }
        };

        var getLiquidityProjectionViewDataById = function (id, asObservable) {
            var query = entityQuery.from('LiquidityProjectionViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Liquidity Projection View Data] from remote data source', data, false);
            }
        };

        //Deatiled SImulation Assumptions
        var createDetailedSimulationAssumption = function (packageId, priority, profile) {
            var as = manager.createEntity("DetailedSimulationAssumption", {
                id: -1
            });
            //Set Report Package and Package Order
            as.institutionDatabaseName(profile().databaseName);
            as.asOfDateOffset(0);
            return as;
        };

        var getDetailedSimulationAssumptionById = function (id, asObservable) {

            var query = entityQuery.from('DetailedSimulationAssumption')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Detailed Simulation Assumption] from remote data source', data, false);
            }
        };

        var getDetailedSimulationAssumptionViewDataById = function (id, asObservable) {
            var query = entityQuery.from('DetailedSimulationAssumptionViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var as = data.results[0];
                if (asObservable) {
                    asObservable(as);
                }
                log('Retrieved [Detailed Simulation Assumption View Data] from remote data source', data, false);
            }
        };


        //Report In Generatl
        var createReport = function (packageId, priority, shortName, dispName) {
            var as = manager.createEntity("Report");
            //Set Report Package and Package Order
            as.packageId(packageId);
            if (shortName == 'CoverPage') {
                as.priority(-1);
            }
            else {
                as.priority(priority);
            }

            as.shortName(shortName);
            as.displayName(dispName);
            as.name(dispName);
            as.hideHeader(false);
            as.hideFooter(false);
            as.contentsFromFile(false);
            as.constrainContentWithinBody(false);

            return as;
        };

        //putting this here because delete report can be called from Pacakge config or report config and lets just define it one place so we don't ahve typos or anything
        var deleteReport = function (reportId) {
            const repTypeName = "Report";
            const parentTypeName = "Package";
            return deleteReportingItemById(repTypeName, reportId, parentTypeName);
        };

        var createSimCompareScenarioType = function () {
            var simCompareScenarioType = manager.createEntity("SimCompareScenarioType");

            return simCompareScenarioType;
        };


        var createEveScenarioType = function () {
            var eveScenarioType = manager.createEntity("EveScenarioType");
            return eveScenarioType;
        };

        var getFootnotesByReportId = function (reportId, footnotesObservable) {
            var query = entityQuery.from('Footnotes')
                .withParameters({ id: reportId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var footnotes = data.results;
                if (footnotesObservable) {
                    footnotesObservable(footnotes);
                }
                log('Retrieved [footnote] from remote data source', data, false);
            }
        };
        var getSplitFootnotesByReportId = function (reportId, footnotesObservable) {
            var query = entityQuery.from('SplitFootnotes')
                .where('reportId', '==', reportId);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var footnotes = data.results;
                if (footnotesObservable) {
                    footnotesObservable(footnotes);
                }
                log('Retrieved [footnote] from remote data source', data, false);
            }
        };

        var createFootnote = function () {
            var footnote = manager.createEntity("Footnote");
            return footnote;
        };
        var createSplitFootnote = function () {
            var footnote = manager.createEntity("SplitFootnote");
            return footnote;
        };

        //var getSimCompareLeftSimulationTypes = function (leftSimulationObservableArray) {

        //};

        var getSimulationTypes = function (simulationTypesObservableArray) {

            var query = entityQuery.from('SimulationTypes');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

                var simulationTypes = data.results;
                if (simulationTypesObservableArray) {
                    simulationTypesObservableArray(simulationTypes);
                }
                log('Retrieved [SimulationTypes] from remote data source', data, false);
            }
        };

        //Get Simulation Types Based Off Of Institution for package
        var getSimulationTypesByInstitution = function (simulationTypesObservableArray, packageId) {


            var query = entityQuery.from('SimulationsByInstitution')
                .withParameters({ packageId: packageId });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

                var simulationTypes = data.results;
                if (simulationTypesObservableArray) {
                    simulationTypesObservableArray(simulationTypes);
                }
                log('Retrieved [Simulation Types By Institution] from remote data source', data, false);
            }
        };

        var getScenarioTypes = function (observer) {

            var query = entityQuery.from('ScenarioTypes')
                .where('isEve', '==', false)
                .where('isActive', '==', true);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var scenarioTypes = data.results;
                if (observer) {
                    observer(scenarioTypes);
                }
                log('Retrieved [ScenarioType] from remote data source', data, false);
            }
        };

        var getAllScenarioTypes = function (scenTypeObsArr) {
            var query = entityQuery.from('ScenarioTypes')
                .where('isActive', '==', true);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function queryFailed() {
                log('Failed retrieving all scenarios');
            }

            function querySucceeded(data) {
                var s = data.results;
                if (scenTypeObsArr)
                    scenTypeObsArr(s);
                log('Retrieved [All Scenario Types] from remote data source');
            }
        };

        //Web Rate Scenarios
        var getWebRateScenarios = function (scenarioTypeObservableArray) {

            var query = entityQuery.from('WebRateScenarios');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

                var scenaroTypes = data.results;
                if (scenarioTypeObservableArray) {
                    scenarioTypeObservableArray(scenaroTypes);
                }
                log('Retrieved [Web Rates Scenario Types] from remote data source', data, false);
            }
        };

        //Web Rate As Of Dates
        var getWebRateAsOfDate = function (asOfDatesObservableArray) {

            var query = entityQuery.from('WebRateAsOfDates');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

                var scenaroTypes = data.results;
                if (asOfDatesObservableArray) {
                    asOfDatesObservableArray(scenaroTypes);
                }
                log('Retrieved [Web Rates As Of Dates] from remote data source', data, false);
            }
        };

        var getEveScenarioTypes = function (scenarioTypeObservableArray) {

            var query = entityQuery.from('ScenarioTypes')
                .where('isEve', '==', true);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {

                var scenaroTypes = data.results;
                if (scenarioTypeObservableArray) {
                    scenarioTypeObservableArray(scenaroTypes);
                }
                log('Retrieved [ScenarioType] from remote data source', data, false);
            }
        };

        var getShockScenarioTypeNames = function (observer) {
            var query = entityQuery.from('ShockScenarioTypeNames').withParameters({ includeBase: true });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var scenaroTypes = data.results;
                if (observer) {
                    observer(scenaroTypes);
                }
                log('Retrieved [ShockScenarioTypeNames] from remote data source', data, false);
            }
        };

        /*summary of results*/
        var createSummaryOfResults = function (packageId, priority, profile) {
            var sr = manager.createEntity("SummaryOfResults", {
                id: -1
            });
            //Set Report Package and Package Order
            sr.institutionDatabaseName(profile().databaseName);
            sr.asOfDateOffset(0);
            sr.ovrd(false);
            sr.nmdAvgLifeAssumption("5");
            return sr;
        };

        var getSummaryOfResultsById = function (id, srObservable) {

            var query = entityQuery.from('SummaryOfResults')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (srObservable) {
                    srObservable(sr);
                }
                log('Retrieved [Detailed Simulation Assumption] from remote data source', data, false);
            }
        };

        var getSummaryOfResultsViewDataById = function (id, srObservable) {
            var query = entityQuery.from('SummaryOfResultsViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (srObservable) {
                    srObservable(sr);
                }
                log('Retrieved [Detailed Simulation Assumption View Data] from remote data source', data, false);
            }
        };
        /*end summary of results*/

        var createChart = function () {
            var chart = manager.createEntity("Chart");
            return chart;
        };

        var createChartScenarioType = function () {
            var series = manager.createEntity("ChartScenarioType");
            return series;
        };

        var createChartSimulationType = function () {
            var series = manager.createEntity("ChartSimulationType");
            return series;
        };


        var createPackage = function () {
            var thePackage = manager.createEntity("Package");
            thePackage.name("New Package");

            return thePackage;
        };

        var cancelChanges = function () {
            manager.rejectChanges();
            log('Canceled changes', null, true);
            app.trigger('application:cancelChanges');
        };

        var saveChanges = function () {
            return manager.saveChanges()
                .then(saveSucceeded)
                .fail(saveFailed);

            function saveSucceeded(saveResult) {
                log('Saved data successfully', saveResult, true);
            }

            function saveFailed(error) {
                var msg = 'Save failed: ' + getErrorMessages(error);
                logError(msg, error);
                error.message = msg;
                console.log(error.entityErrors);
                throw error;
            }
        };

        var saveChangesQuiet = function () {
            console.log("SAVE CHANGES QUIET");
            //console.log(getChanges());
            return manager.saveChanges().then(saveSucceeded).fail(saveFailed);

            function saveSucceeded(saveResult) {
                //log('Save data quietly', saveResult, true);
            }

            function saveFailed(error) {
                try {
                    if (error.indexOf('Concurrent saves not allowed') == -1) {
                        var msg = 'Save failed: ' + getErrorMessages(error);
                        logError(msg, error);
                        error.message = 'Quiet Save Failed';
                        console.log(error.entityErrors);
                        throw error;
                    }
                }
                catch (err) {

                }


            }
        }

        var hasChanges = ko.observable(false);

        manager.hasChangesChanged.subscribe(function (eventArgs) {
            //console.log("MANAGER HAS CHANGES CHANGED");
            //console.log(eventArgs);
            hasChanges(eventArgs.hasChanges);
        });

        var getChanges = function () {
            return manager.getChanges();
        };

        //from https://stackoverflow.com/questions/23838691/breezejs-reject-changes-to-specific-property
        var revertChangesToProperty = function (entityObs, propertyName) {
            if (entityObs().entityAspect.originalValues.hasOwnProperty(propertyName)) {
                var trueEnt = entityObs();
                var origValue = trueEnt.entityAspect.originalValues[propertyName];

                trueEnt.setProperty(propertyName, origValue);
                delete trueEnt.entityAspect.originalValues[propertyName];

                if (Object.getOwnPropertyNames(trueEnt.entityAspect.originalValues).length === 0) {
                    trueEnt.entityAspect.setUnchanged();
                }
            }
        };

        //Hack added 
        var getAvailableOptions = function (id, availOptionsObservable) {
            var query = entityQuery.from('Consolidations')
                .where('id', '==', id);

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                if (consolidationsObservable) {
                    consolidationsObservable(data.results);
                }
                log('Retrieved [Consolidations] from remote data source',
                    data, false);
            }
        };


        //Policies
        var getPolicy = function (eveObservable, asOfDate) {
            var query = entityQuery.from('Policy')
                .withParameters({ asOfDate: asOfDate });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var eve = data.results[0];
                if (eveObservable) {
                    eveObservable(eve);
                }
                log('Retrieved [Eve Config] from remote data source', data, false);
            }
        };

        var getHistoricPolicies = function (eveId, eveObservable) {
            var query = entityQuery.from('HistoricPolicies')
                .withParameters({ eveId: eveId });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var eves = data.results;
                if (eveObservable) {
                    eveObservable(eves);
                }
                log('Retrieved [Historic Policies] from remote data source', data, false);
            }
        };

        //Create Other Policy
        var createOtherPolicy = function () {
            var rt = manager.createEntity("OtherPolicy");
            return rt;
        };


        //Create Capital Policy
        var createCapitalPolicy = function () {
            var rt = manager.createEntity("CapitalPolicy");
            return rt;
        };

        //create Liquidity Policy
        var createLiquidityPolicy = function () {
            var rt = manager.createEntity("LiquidityPolicy");
            return rt;
        };


        //Model Setup
        var getModelSetup = function (msObservable) {
            var query = entityQuery.from('ModelSetup');

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ms = data.results[0];
                if (msObservable) {
                    msObservable(ms);
                }
                log('Retrieved [Model Setup] from remote data source', data, false);
            }
        };

        var getModelSetupData = function (msObs, dbName) {
            var query = entityQuery.from("ModelSetupData")
                .withParameters({ dbName: dbName });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ms = data.results;
                if (msObservable) {
                    msObservable(ms);
                }
                log('Retrieved [Model Setup Data] from remote data source', data, false);
            }
        };

        var getHistoricModelSetups = function (id, msObservable) {
            var query = entityQuery.from('HistoricModelSetup')
                .withParameters({ id: id });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ms = data.results[0];
                if (msObservable) {
                    msObservable(ms);
                }
                log('Retrieved [Historic Model Setup] from remote data source', data, false);
            }
        };


        var getLiabilityPricing = function (lpObservable) {
            var query = entityQuery.from('LiabilityPricings')

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var ms = data.results[0];
                if (lpObservable) {
                    lpObservable(ms);
                }
                log('Retrieved [Liability Pricing] from remote data source', data, false);
            }
        };

        var createNonMaturityDepositRate = function () {
            var rt = manager.createEntity("NonMaturityDepositRate");
            return rt;
        };

        var createTimeDepositSpecialRate = function () {
            var rt = manager.createEntity("TimeDepositSpecialRate");
            return rt;
        };

        var createHistoricalTimeDepositSpecialRate = function () {
            var rt = manager.createEntity("HistoricalTimeDepositSpecialRate");
            return rt;
        };

        //ExecutiveRiskSummary

        var getExecutiveSummaryViewDataById = function (id, obs) {
            var query = entityQuery.from('ExecutiveSummaryViewDataById')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (obs) {
                    obs(sr);
                }
                log('Retrieved [Executive Risk Summary] from remote data source', data, false);
            }
        }

        var getExecutiveRiskSummaryById = function (id, obs) {

            var query = entityQuery.from('ExecutiveRiskSummaries')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (obs) {
                    obs(sr);
                }
                log('Retrieved [Detailed Simulation Assumption] from remote data source', data, false);
            }
        };
        var createExecutiveRiskSummary = function (packageId, priority, profile) {

            var lr = manager.createEntity("ExecutiveRiskSummary", {
                id: -1,
                institutionDatabaseName: profile().databaseName,
                internalAsOfDates: ''
            });
            return lr;
        };


        var createExecRiskSummaryIRRSection = function () {
            var d = manager.createEntity('ExecutiveRiskSummaryIRRSection');
            return d;
        };

        var createExecutiveRiskSummaryIRRSectionDet = function () {
            var d = manager.createEntity('ExecutiveRiskSummaryIRRDet');
            return d;
        };


        var createExecRiskSummaryIRRScenario = function () {
            var d = manager.createEntity('ExecutiveRiskSummaryIRRScenario');
            return d;
        };

        var createExecRiskSummaryLiqSection = function () {
            var d = manager.createEntity('ExecutiveRiskSummaryLiquiditySection');
            return d;
        };

        var createExecutiveRiskSummaryLiquiditySectionDet = function () {
            var d = manager.createEntity('ExecutiveRiskSummaryLiquiditySectionDet');
            return d;
        };

        var createExecutiveRiskSummaryCapitalSection = function () {
            var d = manager.createEntity('ExecutiveRiskSummaryCapitalSection');
            return d;
        }


        var getExecRiskSummarySections = function (obs, instName) {

            var query = entityQuery.from('ESRReportSections').withParameters({
                instName: instName
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {

                if (obs) {
                    obs(data.results[0]);
                }
                log('Retrieved [Executive Risk Summary Sections] from remote data source', data.results[0], false);
            }
            return;
        };
        var getExecRiskSummaryDetails = function (obs, id) {
            var query = entityQuery.from('ESRReportDetails').withParameters({
                instName: instName
            });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d);
                }
                log('Retrieved [Executive Risk Summary Details] from remote data source', data, false);
            }
            return;
        };


        var createExecutiveRiskSummaryDateOffset = function () {
            var offset = manager.createEntity("ExecutiveRiskSummaryDateOffsets");
            return offset;
        };

        //Inventor of liquidity resources
        var createInventoryLiquidityResources = function (packageId, priority, profile) {

            var lr = manager.createEntity("InventoryLiquidityResources", {
                id: -1,
                institutionDatabaseName: profile().databaseName
            });
            return lr;
        };

        var createInventoryLiquidityResourcesWholesaleFunding = function () {

            var lr = manager.createEntity("InventoryLiquidityResourcesWholesaleFunding");
            return lr;
        };

        var createInventoryLiquidityResourcesTier = function () {

            var lr = manager.createEntity("InventoryLiquidityResourcesTier");
            return lr;
        };

        var createInventoryLiquidityResourcesTierDetail = function () {

            var lr = manager.createEntity("InventoryLiquidityResourcesTierDetail");
            return lr;
        };


        var createInventoryLiquidityResourcesTierDetailCollection = function () {

            var lr = manager.createEntity("InventoryLiquidityResourcesTierDetailCollection");
            return lr;
        };

        var createInventoryLiquidityResourcesDateOffset = function () {
            var offset = manager.createEntity("InventoryLiquidityResourcesDateOffsets");
            return offset;
        };


        var getInventoryLiquidityResourcesById = function (id, obs) {

            var query = entityQuery.from('InventoryLiquidityResources')
                .withParameters({ id: id });

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);

            function querySucceeded(data) {
                var sr = data.results[0];
                if (obs) {
                    obs(sr);
                }
                log('Retrieved [Inventory of Liquidity Resources] from remote data source', data, false);
            }
        };

        var getBasicSurplusAdminsByAsOfDate = function (obs, instName, aod) {
            var query = entityQuery.from('BasicSurplusAdminsByAsOfDate').withParameters({
                instName: instName,
                aod: aod
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d);
                }
                log('Retrieved [Basic Surplus Admins By As Of Date] from remote data source', data, false);
            }
            return;
        }

        var getInventoryLiquidityResourcesFields = function (obs, bsId, instName) {
            var query = entityQuery.from('InventoryLiquidityResourcesFields').withParameters({
                basicSurplusId: bsId,
                databaseName: instName
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d);
                }
                log('Retrieved [Inventory Liquidity Resources Fields] from remote data source', data, false);
            }
            return;

        };

        var getInventoryLiquidityResourcesTierTotals = function (obs, bsId, instName) {

            var query = entityQuery.from('InventoryLiquidityResourcesTierTotals').withParameters({
                basicSurplusId: bsId,
                databaseName: instName
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d[0]);
                }
                log('Retrieved [Inventory Liquidity Resources Tier Totals] from remote data source', data, false);
            }
            return;
        }



        var getInventoryOfLiquidityResourcesFundingCapacity = function (obs, bsId, instName) {

            var query = entityQuery.from('InventoryOfLiquidityResourcesFundingCapacity').withParameters({
                instName: instName,
                basicSurplusId: bsId
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d[0]);
                }
                log('Retrieved [Inventory Liquidity Resources Funding Capacity] from remote data source', data, false);
            }
            return;
        }

        var getInventoryOfLiquidityResourcesBasicSurplusHistoric = function (obs, invId, inst) {

            var query = entityQuery.from('InventoryOfLiquidityResourcesBasicSurplusHistoric').withParameters({
                invId: invId,
                inst: inst
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d);
                }
                log('Retrieved [Inventory Liquidity Resources Basic Surplus History] from remote data source', data, false);
            }
            return;
        }




        var getCapitalAnalysisOptions = function (obs, instName) {
            var query = entityQuery.from('CapitalAnalysisOptions').withParameters({
                instName: instName,
            });;

            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (obs) {
                    obs(d);
                }
                log('Retrieved [Capital Analysis Options] from remote data source', data, false);
            }
            return;
        }

        var reportErrors = function (errors, warnings) {
            for (var e = 0; e < errors.length; e++) {
                toastr.error(errors[e], "Error", { timeOut: 10000, extendedTimeOut: 10000 });
            }
            for (var w = 0; w < warnings.length; w++) {
                toastr.warning(warnings[w], "Warning", { timeOut: 10000, extendedTimeOut: 10000 });
            }
        };

        var metaDataCheck = function (mdObs, inst) {
            var query = entityQuery.from('ModelSetupContextCheck').withParameters({inst: inst});
            return manager.executeQuery(query)
                .then(querySucceeded)
                .fail(queryFailed);


            function querySucceeded(data) {
                var d = data.results;
                if (mdObs) {
                    mdObs(d[0]);
                }
                log('Retrieved [Capital Analysis Options] from remote data source', data, false);
            }
        }

        var datacontext = {
            //Clear EntityManager cache - called from switching banks on landing.js
            clearCache: clearCache,
            metaDataCheck: metaDataCheck,
            logEvent: logEvent,

            //New Stuff Ken added for Landing Page
            getUserProfile: getUserProfile,
            getUserName: getUserName,
            updateProfile: updateProfile,
            getAvailableBanks: getAvailableBanks,
            getAsOfDateOffsets: getAsOfDateOffsets,
            getAsOfDates: getAsOfDates,
            getAsOfDateOffsetsExCurrent: getAsOfDateOffsetsExCurrent,
            getAsOfDateOffsetsCustomCount: getAsOfDateOffsetsCustomCount,
            getBasicSurplusAdminOffsets: getBasicSurplusAdminOffsets,
            getPolicyOffsets: getPolicyOffsets,
            getModelOffsets: getModelOffsets,
            getDatabaseStatus: getDatabaseStatus,
            setDatabaseStatus: setDatabaseStatus,
            getAtlasTemplateById: getAtlasTemplateById,
            getAtlasTemplates: getAtlasTemplates,
            copyPackageTemplate: copyPackageTemplate,
            copyPackage: copyPackage,
            newCopyPackage: newCopyPackage,
            newRollPackage: newRollPackage,
            getBankName: getBankName,
            getRegulatoryBankName: getRegulatoryBankName,
            getBankInfo: getBankInfo,
            getFedRatesDates: getFedRatesDates,

            //Basic Surplus
            getBasicSurplusAdmins: getBasicSurplusAdmins,
            getBasicSurplusAdminsTable: getBasicSurplusAdminsTable,
            getBasicSurplusAdminById: getBasicSurplusAdminById,
            getBasicSurplusSecuredLiabilitiesTypesByBasicSurplusAdminId: getBasicSurplusSecuredLiabilitiesTypesByBasicSurplusAdminId,
            getBasicSurplusSecuredLiabilitiesTypeById: getBasicSurplusSecuredLiabilitiesTypeById,
            getCollateralTypesByBasicSurplusAdminId: getCollateralTypesByBasicSurplusAdminId,
            getBasicSurplusAdminData: getBasicSurplusAdminData,
            getBasicSurplusDefinedInPoliciesFromPreviousQuarter: getBasicSurplusDefinedInPoliciesFromPreviousQuarter,
            getSecuredLiabilitiesTypes: getSecuredLiabilitiesTypes,
            getProfileInformationId: getProfileInformationId,
            createBasicSurplusMarketValuesType: createBasicSurplusMarketValuesType,
            createBasicSurplusSecuredLiabilitiesType: createBasicSurplusSecuredLiabilitiesType,
            createBasicSurplusCollateralType: createBasicSurplusCollateralType,
            createCollateralType: createCollateralType,
            createBasicSurplusAdmin: createBasicSurplusAdmin,
            createBasicSurplusAdminOvernightFund: createBasicSurplusAdminOvernightFund,
            createBasicSurplusAdminSecurityCollateral: createBasicSurplusAdminSecurityCollateral,
            createBasicSurplusAdminLiquidAsset: createBasicSurplusAdminLiquidAsset,
            createBasicSurplusAdminVolatileLiability: createBasicSurplusAdminVolatileLiability,
            createBasicSurplusAdminLoanCollateral: createBasicSurplusAdminLoanCollateral,
            createBasicSurplusAdminLoan: createBasicSurplusAdminLoan,
            createBasicSurplusAdminBrokeredDeposit: createBasicSurplusAdminBrokeredDeposit,
            createBasicSurplusAdminMisc: createBasicSurplusAdminMisc,
            createBasicSurplusAdminOtherLiquidity: createBasicSurplusAdminOtherLiquidity,
            createBasicSurplusAdminUnsecuredBorrowing: createBasicSurplusAdminUnsecuredBorrowing,
            createBasicSurplusAdminSecuredBorrowing: createBasicSurplusAdminSecuredBorrowing,
            createBasicSurplusAdminOtherSecured: createBasicSurplusAdminOtherSecured,
            createBasicSurplusAdminUnrealizedGL: createBasicSurplusAdminUnrealizedGL,
            createBasicSurplusAdminBorrowingCapacity: createBasicSurplusAdminBorrowingCapacity,
            createBasicSurplusAdminFootnote: createBasicSurplusAdminFootnote,
            copyBasicSurplus: copyBasicSurplus,
            getBasicSurplusMarketValues: getBasicSurplusMarketValues,

            //Push Log
            getPushLog: getPushLog,

            //L360 Push
            l360Push: l360Push,

            //Defined Grouping
            getDefinedGroupings: getDefinedGroupings,
            createDefinedGrouping: createDefinedGrouping,
            getDefinedGroupingById: getDefinedGroupingById,
            deleteDefinedGrouping: deleteDefinedGrouping,
            //Defined Grouping Groups
            getDefinedGroupingGroups: getDefinedGroupingGroups,
            createDefinedGroupingGroup: createDefinedGroupingGroup,
            getDefinedGroupingGroupById: getDefinedGroupingGroupById,
            deleteDefinedGroupingGroup: deleteDefinedGroupingGroup,
            //Classifications
            createDefinedGroupingClassification: createDefinedGroupingClassification,
            getClassifications: getClassifications,
            //Config Defined Groupings
            configDefinedGroupings: configDefinedGroupings,

            getSqlDatabase: getSqlDatabase,
            getCodeVersion: getCodeVersion,
            startPdfProcess: startPdfProcess,
            startReportPdfProcess: startReportPdfProcess,
            getPDFStatus: getPDFStatus,

            getConsolidations: getConsolidations,
            getConsolidationById: getConsolidationById,

            getInstitutions: getInstitutions,

            createPackage: createPackage,
            getPackages: getPackages,
            getPackageById: getPackageById,
            getFirstReportInPackage: getFirstReportInPackage,
            deletePackageById: deletePackageById,
            deleteReportById: deleteReportById,
            deleteReportingItemById: deleteReportingItemById,
            //deleteOrInsertReportById: deleteOrInsertReportById,

            getReportById: getReportById,
            getReportVmById: getReportVmById,
            getReportsByPackageId: getReportsByPackageId,
            getNextReport: getNextReport,
            getPreviousReport: getPreviousReport,
            defaultReport: defaultReport,

            createOneChart: createOneChart,
            getOneChartById: getOneChartById,
            getOneChartViewDataById: getOneChartViewDataById,

            createTwoChart: createTwoChart,
            getTwoChartById: getTwoChartById,
            getTwoChartViewDataById: getTwoChartViewDataById,

            createSimCompare: createSimCompare,
            createSimCompareSimulationType: createSimCompareSimulationType,
            getSimCompareById: getSimCompareById,
            getSimCompareViewDataById: getSimCompareViewDataById,

            createLoanCapFloor: createLoanCapFloor,
            getLoanCapFloorById: getLoanCapFloorById,
            getLoanCapFloorViewDataById: getLoanCapFloorViewDataById,


            createEve: createEve,
            getEveById: getEveById,
            getEveViewDataById: getEveViewDataById,

            createDocument: createDocument,
            getDocumentById: getDocumentById,
            getDocumentViewById: getDocumentViewById,

            createBalanceSheetCompare: createBalanceSheetCompare,
            getBalanceSheetCompareById: getBalanceSheetCompareById,
            getBalanceSheetCompareViewDataById: getBalanceSheetCompareViewDataById,

            createBalanceSheetMix: createBalanceSheetMix,
            getBalanceSheetMixById: getBalanceSheetMixById,
            getBalanceSheetMixViewDataById: getBalanceSheetMixViewDataById,

            //Basic Surplus Report
            createBasicSurplusReport: createBasicSurplusReport,
            getBasicSurplusReportById: getBasicSurplusReportById,
            getBasicSurplusReportViewDataById: getBasicSurplusReportViewDataById,

            //Interest Rate Policy Guidelines Report
            createInterestRatePolicyGuidelines: createInterestRatePolicyGuidelines,
            getInterestRatePolicyGuidelinesById: getInterestRatePolicyGuidelinesById,
            getInterestRatePolicyGuidelinesViewDataById: getInterestRatePolicyGuidelinesViewDataById,
            createInterestRatePolicyGuidelinesDate: createInterestRatePolicyGuidelinesDate,
            createInterestRatePolicyGuidelinesNIIComparative: createInterestRatePolicyGuidelinesNIIComparative,
            createInterestRatePolicyGuidelinesNIIScenarioType: createInterestRatePolicyGuidelinesNIIScenarioType,
            createInterestRatePolicyGuidelinesNIIScenario: createInterestRatePolicyGuidelinesNIIScenario,
            createInterestRatePolicyGuidelinesEVEComparative: createInterestRatePolicyGuidelinesEVEComparative,
            createInterestRatePolicyGuidelinesEVEScenario: createInterestRatePolicyGuidelinesEVEScenario,

            //Static Gap
            createStaticGap: createStaticGap,
            getStaticGapById: getStaticGapById,
            getStaticGapViewDataById: getStaticGapViewDataById,

            //Funding Matrix
            createFundingMatrix: createFundingMatrix,
            createFundingMatrixOffset: createFundingMatrixOffset,
            getFundingMatrixById: getFundingMatrixById,
            getFundingMatrixViewDataById: getFundingMatrixViewDataById,

            //Time Deposit Migration
            createTimeDepositMigration: createTimeDepositMigration,
            getTimeDepositMigrationById: getTimeDepositMigrationById,
            getTimeDepositMigrationViewDataById: getTimeDepositMigrationViewDataById,

            //Core Funding  Utilization
            createCoreFundingUtilization: createCoreFundingUtilization,
            getCoreFundingUtilizationById: getCoreFundingUtilizationById,
            getCoreFundingUtilizationViewDataById: getCoreFundingUtilizationViewDataById,
            createCoreFundingUtilizationDateOffSet: createCoreFundingUtilizationDateOffSet,

            //Yield Curve Shift
            createYieldCurveShift: createYieldCurveShift,
            getYieldCurveShiftById: getYieldCurveShiftById,
            getYieldCurveShiftViewDataById: getYieldCurveShiftViewDataById,
            createYieldCurveShiftScenarioType: createYieldCurveShiftScenarioType,
            createYieldCurveShiftPeriodCompare: createYieldCurveShiftPeriodCompare,
            createYieldCurveShiftHistoricDate: createYieldCurveShiftHistoricDate,

            //Historical Balance Sheet NII
            createHistoricalBalanceSheetNII: createHistoricalBalanceSheetNII,
            getHistoricalBalanceSheetNIIById: getHistoricalBalanceSheetNIIById,
            getHistoricalBalanceSheetNIIViewDataById: getHistoricalBalanceSheetNIIViewDataById,
            createHistoricalBalanceSheetNIISimulation: createHistoricalBalanceSheetNIISimulation,
            createHistoricalBalanceSheetNIIScenario: createHistoricalBalanceSheetNIIScenario,
            createHistoricBalanceSheetNIITopFootNote: createHistoricBalanceSheetNIITopFootNote,
            createHistoricBalanceSheetNIIBottomFootNote: createHistoricBalanceSheetNIIBottomFootNote,

            //Creation of Policies
            createOtherPolicy: createOtherPolicy,
            createCapitalPolicy: createCapitalPolicy,
            createLiquidityPolicy: createLiquidityPolicy,
            //Simulation Summary
            createSimulationSummary: createSimulationSummary,
            getSimulationSummaryById: getSimulationSummaryById,
            getSimulationSummaryViewDataById: getSimulationSummaryViewDataById,

            //Web Rates Stuff
            getWebRateScenarios: getWebRateScenarios,
            getWebRateAsOfDate: getWebRateAsOfDate,

            //Cover Page
            getCoverPageById: getCoverPageById,
            createCoverPage: createCoverPage,

            //Section Page
            createSectionPage: createSectionPage,
            getSectionPageById: getSectionPageById,

            //Eve Nev Assumptions Methodoligies
            createEveNevAssumptions: createEveNevAssumptions,
            getEveNevAssumptionsById: getEveNevAssumptionsById,
            getEveNevAssumptionsViewDataById: getEveNevAssumptionsViewDataById,

            //Cashflow Report
            getCashflowReportViewDataById: getCashflowReportViewDataById,
            getCashflowReportById: getCashflowReportById,
            createCashflowReport: createCashflowReport,
            createCashflowSimulationScenario: createCashflowSimulationScenario,

            //NII Recon
            createNIIRecon: createNIIRecon,
            getNIIReconById: getNIIReconById,
            createNIIReconScenario: createNIIReconScenario,
            checkNiiSimulation: checkNiiSimulation,

            //Net Cashflow
            getNetCashflowViewDataById: getNetCashflowViewDataById,
            getNetCashflowById: getNetCashflowById,
            createNetCashflow: createNetCashflow,
            createNetCashflowSimulationScenario: createNetCashflowSimulationScenario,

            //Summary Rate
            createSummaryRate: createSummaryRate,
            getSummaryRateById: getSummaryRateById,
            getSummaryRateViewDataById: getSummaryRateViewDataById,
            createSummaryRateScenario: createSummaryRateScenario,

            //ASC825 Balance Sheet
            createASC825BalanceSheet: createASC825BalanceSheet,
            getASC825BalanceSheetById: getASC825BalanceSheetById,
            getASC825BalanceSheetViewDataById: getASC825BalanceSheetViewDataById,

            //Prepayment Details
            createPrepaymentDetails: createPrepaymentDetails,
            getPrepaymentDetailsViewDataById: getPrepaymentDetailsViewDataById,
            getPrepaymentDetailsById: getPrepaymentDetailsById,
            createPrepyamentDetailsScenarioType: createPrepyamentDetailsScenarioType,
            getPrepaymentDetailsScenarioTypes: getPrepaymentDetailsScenarioTypes,

            //ASC825 Data Source
            getASC825DataSourceViewDataById: getASC825DataSourceViewDataById,
            getASC825DataSourceById: getASC825DataSourceById,
            createASC825DataSource: createASC825DataSource,

            //ASC825 Discount Rate
            getASC825DiscountRateViewDataById: getASC825DiscountRateViewDataById,
            getASC825DiscountRateById: getASC825DiscountRateById,
            createASC825DiscountRate: createASC825DiscountRate,

            //ASC825 Worksheet
            createASC825Worksheet: createASC825Worksheet,
            getASC825WorksheetById: getASC825WorksheetById,
            getASC825WorksheetViewDataById: getASC825WorksheetViewDataById,

            //ASC825 Market Value
            createASC825MarketValue: createASC825MarketValue,
            getASC825MarketValueById: getASC825MarketValueById,
            getASC825MarketValueViewDataById: getASC825MarketValueViewDataById,
            createASC825MarketValueScenario: createASC825MarketValueScenario,

            //Rate Change Matrix
            createRateChangeMatrix: createRateChangeMatrix,
            getRateChangeMatrixById: getRateChangeMatrixById,
            getRateChangeMatrixViewDataById: getRateChangeMatrixViewDataById,
            createRateChangeMatrixScenario: createRateChangeMatrixScenario,

            //Investment Portfolio Valuation
            createInvestmentPortfolioValuation: createInvestmentPortfolioValuation,
            getInvestmentPortfolioValuationById: getInvestmentPortfolioValuationById,
            getInvestmentPortfolioValuationViewDataById: getInvestmentPortfolioValuationViewDataById,
            createInvestmentPortfolioValuationScenario: createInvestmentPortfolioValuationScenario,

            //Investment Detail
            createInvestmentDetail: createInvestmentDetail,
            getInvestmentDetailById: getInvestmentDetailById,
            getInvestmentDetailViewDataById: getInvestmentDetailViewDataById,

            //Investment Error
            createInvestmentError: createInvestmentError,
            getInvestmentErrorViewDataById: getInvestmentErrorViewDataById,
            getInvestmentErrorById: getInvestmentErrorById,

            //Policy Junk
            getPolicy: getPolicy,
            getHistoricPolicies: getHistoricPolicies,

            //Model Setup Junk
            getHistoricModelSetups: getHistoricModelSetups,
            getModelSetup: getModelSetup,
            getModelSetupData: getModelSetupData,

            //Liability Pricing Junk
            getLiabilityPricing: getLiabilityPricing,
            createNonMaturityDepositRate: createNonMaturityDepositRate,
            createTimeDepositSpecialRate: createTimeDepositSpecialRate,
            createHistoricalTimeDepositSpecialRate: createHistoricalTimeDepositSpecialRate,

            //ASC825 Assumption Methods
            createASC825AssumptionMethods: createASC825AssumptionMethods,
            getASC825AssumptionMethodsById: getASC825AssumptionMethodsById,
            getASC825AssumptionMethodsViewDataById: getASC825AssumptionMethodsViewDataById,

            //Assumption Methods
            createAssumptionMethods: createAssumptionMethods,
            getAssumptionMethodsById: getAssumptionMethodsById,
            getAssumptionMethodsViewDataById: getAssumptionMethodsViewDataById,

            //Liability Pricing Analysis
            createLiabilityPricingAnalysis: createLiabilityPricingAnalysis,
            getLiabilityPricingAnalysisById: getLiabilityPricingAnalysisById,
            getLiabilityPricingAnalysisViewDataById: getLiabilityPricingAnalysisViewDataById,
            getInstLiabilityPricings: getInstLiabilityPricings,
            getMostRecentWebRateDate: getMostRecentWebRateDate,

            //Liquidity Projection
            createLiquidityProjection: createLiquidityProjection,
            createLiquidityProjectionDateOffset: createLiquidityProjectionDateOffset,
            getLiquidityProjectionById: getLiquidityProjectionById,
            getLiquidityProjectionViewDataById: getLiquidityProjectionViewDataById,

            //Detialed Simulation Assumptions
            createDetailedSimulationAssumption: createDetailedSimulationAssumption,
            getDetailedSimulationAssumptionById: getDetailedSimulationAssumptionById,
            getDetailedSimulationAssumptionViewDataById: getDetailedSimulationAssumptionViewDataById,

            //Summary of Results
            createSummaryOfResults: createSummaryOfResults,
            getSummaryOfResultsById: getSummaryOfResultsById,
            getSummaryOfResultsViewDataById: getSummaryOfResultsViewDataById,

            //Capital Analysis
            createCapitalAnalysis: createCapitalAnalysis,
            getCapitalAnalysisById: getCapitalAnalysisById,
            getCapitalAnalysisViewDataById: getCapitalAnalysisViewDataById,
            createCapitalAnalysisBufferOffSet: createCapitalAnalysisBufferOffSet,
            getRetainedEarnings: getRetainedEarnings,
            getCapitalAnalysisOptions: getCapitalAnalysisOptions,

            //Inventory of Liquidity Resources
            getInventoryLiquidityResourcesById: getInventoryLiquidityResourcesById,
            createInventoryLiquidityResources: createInventoryLiquidityResources,
            createInventoryLiquidityResourcesTier: createInventoryLiquidityResourcesTier,
            createInventoryLiquidityResourcesTierDetail: createInventoryLiquidityResourcesTierDetail,
            createInventoryLiquidityResourcesTierDetailCollection: createInventoryLiquidityResourcesTierDetailCollection,
            getInventoryLiquidityResourcesFields: getInventoryLiquidityResourcesFields,
            getInventoryLiquidityResourcesTierTotals: getInventoryLiquidityResourcesTierTotals,
            getInventoryOfLiquidityResourcesFundingCapacity: getInventoryOfLiquidityResourcesFundingCapacity,
            createInventoryLiquidityResourcesWholesaleFunding: createInventoryLiquidityResourcesWholesaleFunding,
            createInventoryLiquidityResourcesDateOffset: createInventoryLiquidityResourcesDateOffset,
            getInventoryOfLiquidityResourcesBasicSurplusHistoric: getInventoryOfLiquidityResourcesBasicSurplusHistoric,

            //general
            getBasicSurplusAdminsByAsOfDate: getBasicSurplusAdminsByAsOfDate,

            //Report In General
            createReport: createReport,
            deleteReport: deleteReport,

            //Get Files
            getFiles: getFiles,

            createCoreFundingUtilizationScenarioType: createCoreFundingUtilizationScenarioType,
            createChartScenarioType: createChartScenarioType,
            createChartSimulationType: createChartSimulationType,
            createSimCompareScenarioType: createSimCompareScenarioType,
            createEveScenarioType: createEveScenarioType,

            getSimulationTypes: getSimulationTypes,
            getScenarioTypes: getScenarioTypes,
            getEveScenarioTypes: getEveScenarioTypes,
            getShockScenarioTypeNames: getShockScenarioTypeNames,
            getAllScenarioTypes: getAllScenarioTypes,

            //Lookbacks
            getLBCustomLayouts: getLBCustomLayouts,
            createLBCustomLayout: createLBCustomLayout,
            getLBCustomLayoutById: getLBCustomLayoutById,
            createLBCustomLayoutRowItem: createLBCustomLayoutRowItem,
            createLBCustomClassification: createLBCustomClassification,
            createLBCustomType: createLBCustomType,
            getLBCustomTypes: getLBCustomTypes,
            createLBLookback: createLBLookback,
            getLBLookbacks: getLBLookbacks,
            getLBLookbackById: getLBLookbackById,
            getLBLookbacksByOffset: getLBLookbacksByOffset,
            createLBLookbackGroup: createLBLookbackGroup,
            createLBLookbackData: createLBLookbackData,
            getLookbackGroupDataById: getLookbackGroupDataById,
            createLookbackReport: createLookbackReport,
            createLookbackReportSection: createLookbackReportSection,
            createLookbackType: createLookbackType,
            getLookbackReportById: getLookbackReportById,
            getLookbackReportViewDataById: getLookbackReportViewDataById,
            deleteLookback: deleteLookback,
            deleteLayout: deleteLayout,
            deleteLookbackGroup: deleteLookbackGroup,
            getLBLookbackGroupsById: getLBLookbackGroupsById,
            updateLookbackGroupValues: updateLookbackGroupValues,
            getLookbackGroupDataProjections: getLookbackGroupDataProjections,
            getLookbacksByLayoutId: getLookbacksByLayoutId,
            getLBLookbacksByReportId:getLBLookbacksByReportId,

            getSimulationScenarioInfo: getSimulationScenarioInfo,

            //Restreicted Simulations, DateOffSets, and Scenarios
            getSimulationTypesByInstitution: getSimulationTypesByInstitution,

            hasChanges: hasChanges,
            revertChangesToProperty: revertChangesToProperty,
            getChanges: getChanges,
            saveChanges: saveChanges,
            saveChangesQuiet: saveChangesQuiet,
            cancelChanges: cancelChanges,
            createFootnote: createFootnote,
            getFootnotesByReportId: getFootnotesByReportId,
            createSplitFootnote: createSplitFootnote,
            getSplitFootnotesByReportId: getSplitFootnotesByReportId,
            reportErrors: reportErrors,

            //Executive Risk Summary
            getExecutiveRiskSummaryById: getExecutiveRiskSummaryById,
            createExecutiveRiskSummary: createExecutiveRiskSummary,
            getExecRiskSummarySections: getExecRiskSummarySections,
            getExecRiskSummaryDetails: getExecRiskSummaryDetails,
            createExecutiveRiskSummaryLiquiditySectionDet: createExecutiveRiskSummaryLiquiditySectionDet,
            createExecRiskSummaryLiqSection: createExecRiskSummaryLiqSection,
            createExecRiskSummaryIRRScenario: createExecRiskSummaryIRRScenario,
            createExecRiskSummaryIRRSection: createExecRiskSummaryIRRSection,
            createExecutiveRiskSummaryIRRSectionDet: createExecutiveRiskSummaryIRRSectionDet,
            createExecutiveRiskSummaryCapitalSection: createExecutiveRiskSummaryCapitalSection,
            createExecutiveRiskSummaryDateOffset: createExecutiveRiskSummaryDateOffset,
            getExecutiveSummaryViewDataById: getExecutiveSummaryViewDataById,
            getCurrentRiskAss: getCurrentRiskAss,

            createMarginalCostOfFunds: createMarginalCostOfFunds,
            getMarginalCostOfFundsById: getMarginalCostOfFundsById,
            getMarginalCostOfFundsViewDataById: getMarginalCostOfFundsViewDataById,


            defaultBasicSurplus: defaultBasicSurplus,
            //Roll Date
            rollDate: rollDate,
            getMultiPageHeight: getMultiPageHeight,

            //the features that are exacty oppisite of what we set out to do with Atlas One!
            getAvailableSimulations: getAvailableSimulations,
            getAvailableSimulationScenarios: getAvailableSimulationScenarios,
            getAvailableSimulationScenariosMultiple: getAvailableSimulationScenariosMultiple,
            getAvailableSimulationScenariosMultipleInst: getAvailableSimulationScenariosMultipleInst
        };

        return datacontext;

        //#region Internal methods     

        function getLocal(resource, ordering, includeNullos) {
            var query = entityQuery.from(resource)
                .orderBy(ordering);
            if (!includeNullos) {
                query = query.where('id', '!=', 0);
            }
            return manager.executeQueryLocally(query);
        }

        function queryFailed(error) {
            var msg = 'Error retrieving data. ' + error.message;
            logger.logError(msg, error, system.getModuleId(datacontext), true, true);
        }

        function getErrorMessages(error) {
            var msg = error.message;
            if (msg.match(/validation error/i)) {
                return getValidationMessages(error);
            }
            return msg;
        }

        function getValidationMessages(error) {
            try {
                //foreach entity with a validation error
                return error.entitiesWithErrors.map(function (entity) {
                    // get each validation error
                    return entity.entityAspect.getValidationErrors().map(function (valError) {
                        // return the error message from the validation
                        return valError.errorMessage;
                    }).join('; <br/>');
                }).join('; <br/>');
            }
            catch (e) { }
            return 'validation error';
        }

        function configureBreezeManager() {
            breeze.NamingConvention.camelCase.setAsDefault();
            var mgr = new breeze.EntityManager('breeze/global');
            model.configureMetadataStore(mgr.metadataStore);

            mgr.metadataStore.registerEntityTypeCtor(
                'Document',
                function () { this.orientation = null; } // register non-mapped orientation property
            );

            return mgr;
        }

        function log(msg, data, showToast) {
            logger.log(msg, data, system.getModuleId(datacontext), showToast);
        }

        function logError(msg, error) {
            logger.logError(msg, error, system.getModuleId(datacontext), true);
        }
        //#endregion
    });