﻿


define(['durandal/system', 'services/model', 'config', 'services/logger', 'services/globalcontext'],
    function (system, model, config, logger, globalContext) {
        var self = this;

        var calculateTier = function (tierName, tierAmount, tierCum, tierBasic, inv) {
            var idx = inv().inventoryLiquidityResourcesTier().findIndex(function (el) { return el.name() == tierName });
            inv().inventoryLiquidityResourcesTier()[idx].amount(numeral(tierAmount).value());
            inv().inventoryLiquidityResourcesTier()[idx].cumulativeAmount(numeral(tierCum).value());
            inv().inventoryLiquidityResourcesTier()[idx].basicSurplusAmount(numeral(tierBasic).value());
        }

        var calculateTierDetails = function (detail, inv) {
            //loop through detail and sum up collections
            var amount = 0;
            console.log(detail.inventoryLiquidityResourcesTier().name(), inv);

            //do not do if overriding balance sheet
            if (detail.inventoryLiquidityResourcesTier().name() == "On Balance Sheet Liquidity" && inv().overrideBalanceSheetValue()) {
                return;
            }
            for (var y = 0; y < detail.inventoryLiquidityResourcesTierDetailCollection().length; y++) {

                var colItem = detail.inventoryLiquidityResourcesTierDetailCollection()[y];

                if (colItem.sourceRow != null) {
                    if (colItem.addNumber()) {
                        amount = amount + colItem.sourceRow.balance;
                    }
                    else {
                        amount = amount - colItem.sourceRow.balance;
                    }
                }
            }
            detail.amount(amount);
        }

        var setTierDetails = function (name, l, basicSurplusFields) {
            switch (name) {
                case "Tier 1 Liquidity":
                    l.availableDetails(basicSurplusFields().filter(function (val) {
                        return val.section == "Tier1";
                    }));
                    break;
                case "Tier 2 Liquidity":
                    l.availableDetails(basicSurplusFields().filter(function (val) {
                        return val.section == "Tier2";
                    }));
                    break;
                case "Tier 3 Liquidity":
                    l.availableDetails(basicSurplusFields().filter(function (val) {
                        return val.section == "Tier3";
                    }));
                    break;
                case "Other Liquidity":
                    l.availableDetails(basicSurplusFields().filter(function (val) {
                        return val.section == "OtherLiq" || val.fieldName == 'leftS1LiqAssetsCashflow';
                    }));
                    break;
                default:
                    //return everything
                    l.availableDetails(basicSurplusFields().filter(function (val) {
                        return 1 == 1;
                    }));
                    break;
            }

            //Now that we did that loop through the collection that makes upt he detail and find those ones in the available details and set them as available = false
            for (var y = 0; y < l.inventoryLiquidityResourcesTierDetailCollection().length; y++) {

                //Load up name from this list 
                var name = l.inventoryLiquidityResourcesTierDetailCollection()[y].name();

                //now that I have name look it up in index and make it unavaliable
                var obj = l.availableDetails().find(function (element) {
                    return element.fieldName == name;
                });

                var trueIdx = l.availableDetails().indexOf(obj);

                if (trueIdx >= 0) {
                    //rEMOVE FROM available details array but set source row so if they delete it we can re add it
                    l.inventoryLiquidityResourcesTierDetailCollection()[y].sourceRow = obj;
                    l.availableDetails.remove(obj);
                }
                else {
                    //make it null so if they switch back it will show up becuase it was a custom one 
                    l.inventoryLiquidityResourcesTierDetailCollection()[y].sourceRow = null;
                }
            }
        }

        //This function takes array to search for policvies and the order of policies that they should be used
        var getPolValue = function (arr, shortNames) {
            for (var i = 0; i < shortNames.length; i++) {

                var idx = arr.findIndex(function (el) { return el.shortName == shortNames[i] });

                if (idx >= 0) {
                    if (arr[idx].policyLimit != -999) {
                        return {
                            polLimit: arr[idx].policyLimit,
                            shortName : shortNames[i]
                        }
                    }
                }
            }

            return null;
        }

        var calculateFundingPercentage = function (pol, assLiabs, row, fundingCapacity) {
            if (row.percentageType() == '$ Amount') {

                if (row.name() == 'Other Wholesale' && pol.polLimit == -999) {
                    row.amountPerPolicy(numeral(fundingCapacity.bsValues[0].e).value());
                }
                else {
                    row.amountPerPolicy(pol.polLimit);
                }
             
            }
            //it is a percetange of eitehr asses to depoists so calculate that shit
            else {
                var val = numeral(assLiabs.assetTotal).value();

                if (row.percentageType() == '% of Deposits') {
                    val = numeral(assLiabs.depositTotal).value();
                }

                if (numeral(pol.polLimit).value() < -998) {
                    if (row.name() != 'Total Wholesale') {
                        row.amountPerPolicy(0);
                    }       
                }
                else {
                    row.amountPerPolicy(((pol.polLimit / 100) * val) / 1000);
                }            
            }
        }

        var setDefaultValues = function (row, pol) {

            if (!row.overide() && pol != null) {
                row.policy(pol.polLimit);

                if (pol.shortName == 'fhlbAssets' || pol.shortName == 'brokeredAsset' || pol.shortName == 'wholeAsset' ) {
                    row.percentageType('% of Assets');
                }
                else if (pol.shortName == 'fhlbDeposits' || pol.shortName == 'brokeredDeposits') {
                    row.percentageType('% of Deposits');
                }
                else {
                    row.percentageType('$ Amount');
                }

            }
        }

        var getMinimum = function (a, b) {
            if (a < b) {
                if (a < 0) {
                    return 0;
                }
                return a;
            }
            else {
                if (b < 0) {
                    return 0;
                }
                return b;
            }
        }

        var checkForNaN = function (a) {
            if (isNaN(a)) {
                return 0;
            }
            else {
                return a;
            }
        }

        var checkVal = function (a) {
            if (a == -999 || a == -999.0){
                return 0;
            }
            else {
                return a;
            }
        }

        var calculateFinalFundingTable = function (fundingCapacity, rows, inv) {
            //!!!!!!!!!!!!!!!!!!!!!!!!!! WE ARE ALMOST THERE NOW NEED TO CALCULATE ACTUAL DATATABLE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

            var fhlbRow = rows()[rows().findIndex(function (el) { return el.name() == 'FHLB' })];
            var brokeredRow = rows()[rows().findIndex(function (el) { return el.name() == 'Brokered' })];
            var otherRow = rows()[rows().findIndex(function (el) { return el.name() == 'Other Wholesale' })];
            var totalRow = rows()[rows().findIndex(function (el) { return el.name() == 'Total Wholesale' })];


            if (!inv().overrideCalculatedFunding()) {


                //Set 1 from spec

                fhlbRow.availableFunding(checkForNaN(numeral(fundingCapacity.bsValues[0].one).value()));

                //Set 2 from spec

                //if 0 then use 1 else use minimum of 1 
                if (fhlbRow.amountPerPolicy() == -999) {
                    fhlbRow.availableFundingPerPolicy(numeral(fundingCapacity.bsValues[0].one).value());
                }
                else {
                    var diffVal = checkVal(numeral(fhlbRow.amountPerPolicy()).value()) - checkVal(numeral(fhlbRow.outstanding()).value());
                    fhlbRow.availableFundingPerPolicy(getMinimum(diffVal, checkVal(numeral(fundingCapacity.bsValues[0].one).value())));
                }

                //Set 3 from spec
                var ghDiff = numeral(totalRow.amountPerPolicy()).value() - numeral(totalRow.outstanding()).value();
                fhlbRow.availableFundingPerTotalWholesalePolicy(getMinimum(fhlbRow.availableFundingPerPolicy(), ghDiff));

                //Set 4 from spec
                brokeredRow.availableFunding(numeral(fundingCapacity.bsValues[0].four).value());

                //Set 5 from spec
                if (brokeredRow.amountPerPolicy() == -999) {
                    brokeredRow.availableFundingPerPolicy(numeral(fundingCapacity.bsValues[0].four).value());
                }
                else {
                    var diffVal = checkVal(numeral(brokeredRow.amountPerPolicy()).value()) - checkVal(numeral(brokeredRow.outstanding()).value());
                    brokeredRow.availableFundingPerPolicy(getMinimum(diffVal, numeral(fundingCapacity.bsValues[0].four).value()));
                }

                //Set 6 from spec
                var gh3Diff = (checkVal(numeral(totalRow.amountPerPolicy()).value()) - checkVal(numeral(totalRow.outstanding()).value())) - checkVal(numeral(fhlbRow.availableFundingPerTotalWholesalePolicy()).value());
                brokeredRow.availableFundingPerTotalWholesalePolicy(getMinimum(brokeredRow.availableFundingPerPolicy(), gh3Diff));

                //Set 7 From spec
                otherRow.availableFunding(numeral(fundingCapacity.bsValues[0].seven).value());

                //Set 8 From spec
                if (otherRow.amountPerPolicy() == 0) {
                    otherRow.availableFundingPerPolicy(numeral(fundingCapacity.bsValues[0].seven).value());
                }
                else {
                    var diffVal = checkVal(numeral(otherRow.amountPerPolicy()).value()) - checkVal(numeral(otherRow.outstanding()).value());
                    otherRow.availableFundingPerPolicy(getMinimum(diffVal, numeral(fundingCapacity.bsValues[0].seven).value()));
                }

                //Set 9 From spec
                gh36Diff = (checkVal(numeral(totalRow.amountPerPolicy()).value()) - checkVal(numeral(totalRow.outstanding()).value())) - (checkVal(numeral(fhlbRow.availableFundingPerTotalWholesalePolicy()).value()) + checkVal(numeral(brokeredRow.availableFundingPerTotalWholesalePolicy()).value()));
                otherRow.availableFundingPerTotalWholesalePolicy(getMinimum(gh36Diff, otherRow.availableFundingPerPolicy()));

               
            }


            //Set 10,11,12 from spec
            totalRow.availableFunding(fhlbRow.availableFunding() + brokeredRow.availableFunding() + otherRow.availableFunding());
            totalRow.availableFundingPerPolicy(fhlbRow.availableFundingPerPolicy() + brokeredRow.availableFundingPerPolicy() + otherRow.availableFundingPerPolicy());
            totalRow.availableFundingPerTotalWholesalePolicy(fhlbRow.availableFundingPerTotalWholesalePolicy() + brokeredRow.availableFundingPerTotalWholesalePolicy() + otherRow.availableFundingPerTotalWholesalePolicy());

        }

        var calculateFundingCapacity = function (fundingCapacity, rows, inv) {
            //Load Up fhlb row
            var fhlbRow = rows()[rows().findIndex(function (el) { return el.name() == 'FHLB' })];
            var brokeredRow = rows()[rows().findIndex(function (el) { return el.name() == 'Brokered' })];
            var otherRow = rows()[rows().findIndex(function (el) { return el.name() == 'Other Wholesale' })];
            var totalRow = rows()[rows().findIndex(function (el) { return el.name() == 'Total Wholesale' })];

            //Calculate them according to spec in jira using same variable names

            /* 
                FHLB Specefic Row STUFF
            */
            //For var a check all three FHLB policies in asses/dep/max to see if one exists
            var polForA = getPolValue(fundingCapacity.polValues, ["fhlbAssets", "fhlbDeposits", "fhlbMax"]);

            //set defaults for fhlb row
            setDefaultValues(fhlbRow, polForA);

            if (!fhlbRow.overide()) {
                //Custom logic for fhlb row
                if (polForA != null) {
                    calculateFundingPercentage(polForA, fundingCapacity.assLiabs[0], fhlbRow, null);
                }//if no a then take the vield from basic surplus takes the leesser of a and b   
                else if (polForA == null) {

                    //Set policy to 0
                    fhlbRow.policy(-999);

                    if (numeral(fundingCapacity.bsValues[0].a).value() == -999.0) {
                        fundingCapacity.bsValues[0].a = -999;
                    }
                    fhlbRow.amountPerPolicy(numeral(fundingCapacity.bsValues[0].a).value());

                }

                //Set the outstanding value
                fhlbRow.outstanding(fundingCapacity.bsValues[0].b);
            }

            /*
                Brokered Specefic Row
            */

            var polForC = getPolValue(fundingCapacity.polValues, ["brokeredAsset", "brokeredDeposits", "brokeredMax"]);

            //set defaults for row
            setDefaultValues(brokeredRow, polForC);


            if (!brokeredRow.overide()) {
                if (polForC != null) {
                    calculateFundingPercentage(polForC, fundingCapacity.assLiabs[0], brokeredRow, null);
                }//if no a then take the vield from basic surplus takes the leesser of a and b   
                else if (polForC == null) {
                    brokeredRow.policy(-999);
                    brokeredRow.amountPerPolicy(numeral(fundingCapacity.bsValues[0].c).value());
                }

                //Set outstanding value
                brokeredRow.outstanding(fundingCapacity.bsValues[0].d);
            }

            /*
                Other Wholesale Specefic Row
            */

            //This one has no link to policues jsut check if its -999(nothing) and if it inst act as if policy

           // if (!otherRow.overide()) {
            console.log(otherRow.policy());
            if (otherRow.policy() != -999) {
                console.log('Hello');
                calculateFundingPercentage({ polLimit: otherRow.policy() }, fundingCapacity.assLiabs[0], otherRow, fundingCapacity);
                }
            else if (otherRow.policy() == -999) {
                console.log('asfasf');
                otherRow.amountPerPolicy(numeral(fundingCapacity.bsValues[0].e).value());
                console.log('---------------------------------');
                console.log(numeral(fundingCapacity.bsValues[0].e).value());
                console.log(otherRow.amountPerPolicy());
                console.log('*********************************');
                }

                //Set outstanding value
                otherRow.outstanding(numeral(fundingCapacity.bsValues[0].borrowinsMinusSecRetail).value());
          //  }

            /*
               Total Wholesale Specefic Row
           */

            var polForG = getPolValue(fundingCapacity.polValues, ["wholeAsset"]);
            //set defaults for row
            setDefaultValues(totalRow, polForG);

            if (!totalRow.overide()) {
                if (polForG != null) {
                    calculateFundingPercentage(polForG, fundingCapacity.assLiabs[0], totalRow, null);
                }
                else if (polForG == null) {
                    totalRow.policy(-999);
                    totalRow.amountPerPolicy(checkVal(fhlbRow.amountPerPolicy()) + checkVal(brokeredRow.amountPerPolicy()) + checkVal(otherRow.amountPerPolicy()));
                }

                //Set outstraning
                totalRow.outstanding(checkVal(fhlbRow.outstanding()) + checkVal(brokeredRow.outstanding()) + checkVal(otherRow.outstanding()));

            }

        }

        var datacontext = {
            calculateTier: calculateTier,
            calculateTierDetails: calculateTierDetails,
            setTierDetails: setTierDetails,
            calculateFundingCapacity: calculateFundingCapacity,
            calculateFinalFundingTable: calculateFinalFundingTable,
            calculateFundingPercentage: calculateFundingPercentage

        };

        return datacontext;
    });