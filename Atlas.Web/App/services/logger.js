define(['durandal/system', 'durandal/app', 'plugins/router'],
    function (system, app, router) {
        var logger = {
            log: log,
            logError: logError
        };

        return logger;

        function log(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'info');
        }

        function logError(message, data, source, showToast, isFatal) {
            logIt(message, data, source, !isFatal && showToast, 'error');

            if (!isFatal) return;

            document.body.setAttribute('data-fatal', message.replace('\n', '').replace('\r', ''));

            app.showMessage(
                'The system has encountered an error which might corrupt your session, you will be redirected to the home screen. The error was:<br/><br/>' + message,
                'Fatal Error',
                ['Continue']
            ).then(() => {
                document.location = '/';
            });
        }

        function logIt(message, data, source, showToast, toastType) {
            source = source ? '[' + source + '] ' : '';
            if (data) {
                system.log(source, message, data);
            } else {
                system.log(source, message);
            }
            if (showToast) {
                if (toastType === 'error') {
                    toastr.error(message);
                } else {
                    toastr.info(message);
                }
            }
        }
    });