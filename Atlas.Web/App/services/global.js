﻿define([
    'config'
],
function (config) {
    //Global value store, for things commonly used that aren't likely to change between view models
    //TODO maybe cache things like Profile in here to avoid the call in every viewmodel?
    return {
        //e.g. asOfDateOffsets: ko.observableArray()
        navBarLock: ko.observable(true),
        navBarShowAtlasTemplatesOptions: ko.observable(false),
        pdfInProgress: ko.observable(false),
        pdfType: ko.observable(''),
        pdfPackage: ko.observable(0),
        pdfReportId: ko.observable(0),
        pdfStatus: ko.observable(''),
        buildNumber: ko.observable('1.024'),
        loadingReport: ko.observable(false),
        isTesting: window.location.hostname.indexOf("10.0.0") > -1 || window.location.hostname.indexOf("localhost") > -1,
        selectGroup: function (label, children) {
            this.label = ko.observable(label);
            this.children = ko.observableArray(children);
        },
        selectOption: function(label, prop){
            this.title = ko.observable(label);
            this.value = ko.observable(prop);
        },
        getGroupedSelectOptions: function(groupArray){
            //this will usually be a 2 item array, first index being an object with name = name of the group, then array of objects for options. Second index being a pulled down list of defined groupings
            //standard model for these object should be {name: '', id: ''}
            var retArr = [];
            var subArray = [];
            for (var g = 0; g < groupArray.length; g++) {
                if (groupArray[g].options.length > 0) {
                    subArray = [];
                    for (var o = 0; o < groupArray[g].options.length; o++) {
                        subArray.push(new this.selectOption(groupArray[g].options[o].name, groupArray[g].options[o].id));
                    }
                    retArr.push(new this.selectGroup(groupArray[g].name, subArray));
                }
            }
            return retArr;
        },
        exportHeight: 794,
        exportWidth: 1000,
        topMargin: 30,
        bottomMargin: 30,
        updatedTitle: '',
        correctPDFTable: function (tableId) {
            //This is called to split table onto multiple pages with page breaks per page height including margin
            var table = $('#' + tableId);

            var tableHeight = table.height();
            var position = table.offset();

            //if position of table 
            if (tableHeight + position.top <= this.exportHeight - this.topMargin - this.bottomMargin) {
                return true;
            }
        },

        //TODO HACK Used for transient navigation parameters since I can't figure out Durandal Routing parameters
        //This should probably be cleared back to empty object every use
        navigationParams: ko.observable({})
    };
});
