﻿


define(['durandal/app', 'viewmodels/navigateawaymodal', 'services/globalcontext', 'services/global', 'plugins/router',],
    function (app, naModal, globalContext, global, router) {
        var self = this;

        //Vars
        var institutions = ko.observableArray();
        var global = global;
        var asOfDateOffsets = ko.observableArray();
        var simulationTypes = ko.observableArray();
        var scenarioTypes = ko.observableArray();
        var shockScenarioTypeNames = ko.observableArray();
        var basicSurplusAdmins = ko.observableArray();
        var securedLiabilitiesTypes = ko.observableArray();
        var allTypes = ko.observableArray();
        var isSaving = ko.observable(false);
        var profile = ko.observable();
        var thisType = ko.observable();
        var borTypes = ko.observableArray(["Percent", "Dollar", "None"]);
        var borCapsTypes = ko.observableArray(["Assets", "Deposits", "Dollar", "None"]);
        var basicSurplusData = {};

        //View
        var connectClass = "scenario-container";

        //Navigation Helpers
        var hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges();
        });

        var canSave = ko.pureComputed(function () {
            return hasChanges() && !isSaving();
        });

        var canDeactivate = function () {
            if (!hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { save(); }
            );
        };

        //Navigation
        var goBack = function () {
            router.navigateBack();
        };

        var goToDetail = function (type) {
            router.navigate("#/basicsurplustypeconfig/" + type.id());
        };

        //From MasterAM at http://stackoverflow.com/a/17831442
        var goToTab = function (tab) {
            $('.nav-tabs a[href="' + tab + '"]').tab('show');
        };

        var cancel = function () {
            var self = this;
            isSaving(true);
            globalContext.cancelChanges();
            isSaving(false);

            sortTypes();
        };

        var save = function (data, event, quiet) {
            var self = this;
            isSaving(true);

            // no need for validation on quiet save
            if (quiet)
                return globalContext.saveChangesQuiet().fin(complete);

            var id = thisType().id();
            var fname = thisType().name().toLowerCase().trim();
            
            var mappedTypes = basicSurplusAdmins().map(function (v) { return { name: v.name(), id: v.id() } });
            
            for (var i = 0; i < mappedTypes.length; i++) {
                var compareType = mappedTypes[i];
            
                if (id === compareType.id || fname != compareType.name.toLowerCase().trim())
                    continue;
            
                return app.showMessage('There is a conflicting package with name ' + compareType.name, 'Cannot Save', ['OK']).then(complete);
            }
            

            return globalContext.saveChanges().fin(complete);

            function complete() {
                isSaving(false);
            }
        };

        var deleteType = function () {
            var self = this;
            var type = thisType();
            var msg = 'Delete "' + type.name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    isSaving(true);

                    //For some reason we have to first manually delete the subtypes
                    //And for some reason we must build a subTypesToDel to do this
                    var subTypesToDel = [];
                    type.basicSurplusMarketValues().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.basicSurplusSecuredLiabilitiesTypes().forEach(function (subType) {
                        subType.basicSurplusCollateralTypes().forEach(function (row) {
                            subTypesToDel.push(row);
                        });
                        subTypesToDel.push(subType);
                    });
                    type.collateralTypes().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS1OvntFunds().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS1SecCols().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS1LiqAssets().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS2VolLiabs().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS3LoanCols().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS3Loans().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.leftS4BrokDeps().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.midS1Miscs().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.rightS1OtherLiqs().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.rightS3UnsecBors().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.rightS2SecBors().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    //type.rightS4OtherSecs().forEach(function (subType) {
                    //    subTypesToDel.push(subType);
                    //});
                    type.rightS4UnrealGL().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.rightS5BorCaps().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    type.rightS6Footnotes().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    subTypesToDel.forEach(function (subType) {
                        subType.entityAspect.setDeleted();
                    });

                    type.entityAspect.setDeleted();
                    basicSurplusAdmins().forEach(function (aType, index) {
                        aType.priority(index);
                    });

                    //Actually save
                    save().then(function () {
                        goBack();
                    });
                }
                return selectedOption;
            }
        };

        //Sorting
        var sortTypes = function () {

            sortedCollateralTypes(sortedCollateralTypes().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedMarketValues(sortedMarketValues().sort(function (a, b) {
                return marketValuesMap[a.name()][1] - marketValuesMap[b.name()][1];
            }));
            sortedTypes(sortedTypes().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedTypes().forEach(function (type) {
                type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                    return a.priority() - b.priority();
                }));
            });
            sortedLeftS1OvntFunds(sortedLeftS1OvntFunds().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedLeftS1SecCols(sortedLeftS1SecCols().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedLeftS1LiqAssets(sortedLeftS1LiqAssets().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedLeftS2VolLiabs(sortedLeftS2VolLiabs().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedLeftS3LoanCols(sortedLeftS3LoanCols().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedLeftS3Loans(sortedLeftS3Loans().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedLeftS4BrokDeps(sortedLeftS4BrokDeps().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedMidS1Miscs(sortedMidS1Miscs().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedRightS1OtherLiqs(sortedRightS1OtherLiqs().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedRightS3UnsecBors(sortedRightS3UnsecBors().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedRightS2SecBors(sortedRightS2SecBors().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            //sortedRightS4OtherSecs(sortedRightS4OtherSecs().sort(function (a, b) {
            //    return a.priority() - b.priority();
            //}));
            sortedRightS4UnrealGL(sortedRightS4UnrealGL().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedRightS5BorCaps(sortedRightS5BorCaps().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            sortedRightS6Footnotes(sortedRightS6Footnotes().sort(function (a, b) {
                return a.priority() - b.priority();
            }));

            //Also update available types
            securedLiabilitiesTypes.notifySubscribers();
        };

        var sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (type, index) {
                type.priority(index);
            });
        };

        //Add / Remove
        var addSecuredLiabilitiesType = function (type) {
            var self = this;
            var newType = globalContext.createBasicSurplusSecuredLiabilitiesType();
            newType.priority(sortedTypes().length);
            newType.basicSurplusAdminId(thisType().id());
            newType.securedLiabilitiesTypeId(type.id);
            assignDataPullField(newType); //TODO HACK maybe this should become a real db field?
            if (newType.dataPullField != null)
                newType.override(false);
            subAll(newType, doLotsOfWork);
            newType.sortedTypes = ko.observableArray(newType.basicSurplusCollateralTypes());
            thisType().basicSurplusSecuredLiabilitiesTypes.push(newType);
            thisType().basicSurplusSecuredLiabilitiesTypes[secLiabTypesMap[newType.securedLiabilitiesType().name()]] = newType; //Note: We actually need this as these can be added after startup
            sortTypes();
        };
        var dontAddSecuredLiabilitiesType = function (data, event) {
            event.stopPropagation();
        };

        var removeSecuredLiabilitiesType = function (type) {
            var self = this;
            var msg = 'Delete "' + type.securedLiabilitiesType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    //For some reason we have to first manually delete the subtypes
                    //And for some reason we must build a subTypesToDel to do this
                    var subTypesToDel = [];
                    type.basicSurplusCollateralTypes().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    subTypesToDel.forEach(function (subType) {
                        subType.entityAspect.setDeleted();
                    });

                    type.entityAspect.setDeleted();
                    thisType().basicSurplusSecuredLiabilitiesTypes().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    sortTypes();
                }
                return selectedOption;
            }
        };

        var addMarketValues = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusMarketValuesType();
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType, doLotsOfWork); //Note: We actually need this even though we only add these on startup, as these are added after subscriptions
            thisType().basicSurplusMarketValues.push(newType);
            return newType;
        };

        var addCollateralType = function (name, fieldName, collateralValue, allowCollateralValueEdit, enabled) {
            var self = this;
            var newType = globalContext.createCollateralType();
            newType.priority(thisType().collateralTypes().length);
            newType.basicSurplusAdminId(thisType().id());
            newType.override(false);
            newType.name(name);
            newType.fieldName(fieldName);
            newType.collateralValue(collateralValue);
            newType.allowCollateralValueEdit(allowCollateralValueEdit);
            newType.enabled(enabled);
            subAll(newType, doSomeWork);
            subAll(newType); //Note: We actually need this even though we only add these on startup, as these are added after subscriptions
            thisType().collateralTypes.push(newType);
            return newType;
        }

        var addLeftS1OvntFunds = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminOvernightFund();
            newType.priority(sortedLeftS1OvntFunds().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().leftS1OvntFunds.push(newType);
            sortTypes();
            return newType;
        };
        var addLeftS1SecCols = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminSecurityCollateral();
            newType.priority(sortedLeftS1SecCols().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().leftS1SecCols.push(newType);
            sortTypes();
            return newType;
        };
        var addLeftS1LiqAssets = function (includeOverride) {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminLiquidAsset();
            newType.priority(sortedLeftS1LiqAssets().length);
            newType.basicSurplusAdminId(thisType().id());

            //this include flag was only added for government loans
            //it should be true for every other item in this collection because we always want to show them
            //however for govenment loans it should default to false
            if (includeOverride == undefined) {
                newType.include(true);
            } else {
                newType.include(includeOverride);
            }

            subAll(newType);
            thisType().leftS1LiqAssets.push(newType);
            sortTypes();
            return newType;
        };
        var addLeftS2VolLiabs = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminVolatileLiability();
            newType.priority(sortedLeftS2VolLiabs().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().leftS2VolLiabs.push(newType);
            sortTypes();
            return newType;
        };

        var addLeftS3LoanCols = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminLoanCollateral();
            newType.priority(sortedLeftS3LoanCols().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().leftS3LoanCols.push(newType);
            sortTypes();
            return newType;
        };
        var addLeftS3Loans = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminLoan();
            newType.priority(sortedLeftS3Loans().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().leftS3Loans.push(newType);
            sortTypes();
            return newType;
        };
        var addLeftS4BrokDeps = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminBrokeredDeposit();
            newType.priority(sortedLeftS4BrokDeps().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().leftS4BrokDeps.push(newType);
            sortTypes();
            return newType;
        };

        var addMidS1Miscs = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminMisc();
            newType.priority(sortedMidS1Miscs().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().midS1Miscs.push(newType);
            sortTypes();
            return newType;
        };
        var addRightS1OtherLiqs = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminOtherLiquidity();
            newType.priority(sortedRightS1OtherLiqs().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().rightS1OtherLiqs.push(newType);
            sortTypes();
            return newType;
        };
        var addRightS3UnsecBors = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminUnsecuredBorrowing();
            newType.priority(sortedRightS3UnsecBors().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().rightS3UnsecBors.push(newType);
            sortTypes();
            return newType;
        };
        var addRightS2SecBors = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminSecuredBorrowing();
            newType.priority(sortedRightS2SecBors().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().rightS2SecBors.push(newType);
            sortTypes();
            return newType;
        };

        var addRightS4UnrealGL = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminUnrealizedGL();
            newType.priority(sortedRightS4UnrealGL().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().rightS4UnrealGL.push(newType);
            sortTypes();
            return newType;
        };
        var addRightS5BorCaps = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminBorrowingCapacity();
            newType.priority(sortedRightS5BorCaps().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().rightS5BorCaps.push(newType);
            sortTypes();
            return newType;
        };
        var addRightS6Footnotes = function () {
            var self = this;
            var newType = globalContext.createBasicSurplusAdminFootnote();
            newType.priority(sortedRightS6Footnotes().length);
            newType.basicSurplusAdminId(thisType().id());
            subAll(newType);
            thisType().rightS6Footnotes.push(newType);
            sortTypes();
            return newType;
        };

        var removeType = function (type, event, quiet) {
            var self = this;
            if (quiet)
                return confirmDelete('Yes'); //TODO this is a different return type (not a Promise)
            var msg = 'Delete "' + type.title() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    type.entityAspect.setDeleted();

                    //We don't know which one we're deleting (yet), so sort all of them
                    thisType().leftS1OvntFunds().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().leftS1SecCols().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().leftS1LiqAssets().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().leftS2VolLiabs().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().leftS3LoanCols().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().leftS3Loans().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().leftS4BrokDeps().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().midS1Miscs().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().rightS1OtherLiqs().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().rightS3UnsecBors().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().rightS2SecBors().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    //thisType().rightS4OtherSecs().forEach(function (aType, index) {
                    //    aType.priority(index);
                    //});
                    thisType().rightS4UnrealGL().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().rightS5BorCaps().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    thisType().rightS6Footnotes().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    sortTypes();
                }
                return selectedOption;
            }
        };

        var defaultSimulation = function (newValue, thing) {
            var aod = asOfDateOffsets()[thisType().asOfDateOffset()].asOfDateDescript;


            if (policyOffsets()[0][aod] == undefined) {
                return false;
            }

            if (!thisType().overrideSimulationTypeId()) {
               thisType().simulationTypeId(policyOffsets()[0][aod][0].niiId);
            }

            refreshScens();

        }

        var refreshScens = function () {
            globalContext.getAvailableSimulationScenarios(scenarioTypes, thisType().institutionDatabaseName(), 0, thisType().simulationTypeId(), false)
        }


        var calcUnrealized = function (ignoreOverride, fieldame) {
            var self = this;

            var scen = thisType().rightS4UnrealGL[fieldame];

            if (!scen.override() || ignoreOverride) {
                var gls = marketValues().filter(function (val) {
                    return val.scenarioTypeId == scenLookUp[scen.title()];
                });
                var vals = calcUnrealizedGainLoss(gls);

                scen.totalPortfolio(vals.totalPort / 1000);
                scen.tier1BasicSurplus(vals.tier1 / 1000);
            }
        }

        var calcUnrealizedGainLoss = function (arr) {
            var self = this;

            var retObj = {
                totalPort: 0,
                tier1: 0,
                mktValue: 0
            };

            for (var i = 0; i < arr.length; i++) {
                var rec = arr[i];

                switch (rec.name) {
                    case "AgencyBacked":
                    case "PrivateLabel":
                    case "Treas":
                        retObj.tier1 += numeral(rec.gainLoss).value();
                        break;
                    default:
                        break;
                }

                retObj.totalPort += numeral(rec.gainLoss).value();
            }

            return retObj;
        }


        var scenLookUp = {};
        scenLookUp["Shock Down 400BP"] = "24";
        scenLookUp["Shock Down 300BP"] = "25";
        scenLookUp["Shock Down 200BP"] = "26";
        scenLookUp["Shock Down 100BP"] = "28";
        scenLookUp["Shock Up 100BP"] = "32";
        scenLookUp["Shock Up 200BP"] = "34";
        scenLookUp["Shock Up 300BP"] = "35";
        scenLookUp["Shock Up 400BP"] = "36";
        scenLookUp["Base"] = "1";


        var setMarketValue = function (arrVal, bsVal, arr) {
            var self = this;
            //find index in array of arrVal
            var idx = arr.map(function (e) { return e.name; }).indexOf(arrVal);
            if (idx >= 0) {
                thisType().basicSurplusMarketValues['marketValue'][bsVal](numeral(arr[idx].mktValue).value() / 1000);
            }
            else {
                thisType().basicSurplusMarketValues['marketValue'][bsVal](0);
            }

        }

        var defaultMarketValues = function () {
            var self = this;
            if (!thisType().overrideMarketValues()) {
                var baseMktValue = marketValues().filter(function (val) {
                    return val.scenarioTypeId == scenLookUp['Base'];
                });

                console.log('------------------------');
                console.log(baseMktValue);
                console.log('------------------------');

                setMarketValue('AgencyBacked', 'mBSCMOAgency', baseMktValue);

                setMarketValue('PrivateLabel', 'mBSCMOPvt', baseMktValue);

                setMarketValue('Treas', 'treasuryAgencyBonds', baseMktValue);

                setMarketValue('Corp', 'corporateSecurities', baseMktValue);

               // setMarketValue('Stock', 'equitySecurities', baseMktValue);

                setMarketValue('Muni', 'municipalSecurities', baseMktValue);

                setMarketValue('Other', 'otherInvestments', baseMktValue);
            }
        }


        var policyOffsets = ko.observableArray();
        var marketValues = ko.observable();


        var sortedCollateralTypes = ko.observableArray();
        var sortedMarketValues = ko.observableArray();
        var sortedTypes = ko.observableArray();

        var sortedLeftS1OvntFunds = ko.observableArray();
        var sortedLeftS1SecCols = ko.observableArray();
        var sortedLeftS1LiqAssets = ko.observableArray();
        var sortedLeftS2VolLiabs = ko.observableArray();
        var sortedLeftS3LoanCols = ko.observableArray();
        var sortedLeftS3Loans = ko.observableArray();
        var sortedLeftS4BrokDeps = ko.observableArray();
        var sortedMidS1Miscs = ko.observableArray();
        var sortedRightS1OtherLiqs = ko.observableArray();
        var sortedRightS3UnsecBors = ko.observableArray();
        var sortedRightS2SecBors = ko.observableArray();
        var sortedRightS4UnrealGL = ko.observableArray();
        var sortedRightS5BorCaps = ko.observableArray();
        var sortedRightS6Footnotes = ko.observableArray();
        var bsObs = ko.observable();


        //Data Handling
        var activate = async function (id, inst, noCacheClear) {
            if(! noCacheClear) globalContext.clearCache()
            //return globalContext.getBasicSurplusDefinedInPoliciesFromPreviousQuarter()
            //            .then(bs => {
            //                thisType().rightS6PubDepReqPledgingPreviousQuarter._initialValue = (bs && bs())
            //                    ? bs().rightS6PubDepReqPledgingCurrentQuarter() || 0
            //                    : 0;

            //                //All activated and ready to go
            //                isSaving(false);
            //            }); 
            //Pre-Setup
            return Q.all([
                globalContext.getScenarioTypes(scenarioTypes),
                globalContext.getShockScenarioTypeNames(shockScenarioTypeNames),
                globalContext.getAvailableSimulations(simulationTypes, "", 0),        
                globalContext.getAvailableBanks(institutions),
                globalContext.getBasicSurplusAdmins(basicSurplusAdmins),
                globalContext.getBasicSurplusAdminById(id, inst, thisType),
                globalContext.getSecuredLiabilitiesTypes(inst, securedLiabilitiesTypes),
                globalContext.getUserProfile(profile),
                globalContext.getBasicSurplusMarketValues(inst, marketValues),
                globalContext.getBasicSurplusDefinedInPoliciesFromPreviousQuarter(bsObs)
            ]).fin(function () {
                return Q.all([        
                    //AsOfDateOffset Handling (Custom Basic Surplus Template)
                    updateAsOfDateOffsets(thisType().institutionDatabaseName()),
                    //Basic Surplus Data
                    getBasicSurplusData(undefined, undefined, true),
                    globalContext.getPolicyOffsets(policyOffsets, thisType().institutionDatabaseName())
                ]);
                }).then(function () {
                    //Block calculations until cleared by end of activate
                    isSaving(true);
                    thisType().rightS6PubDepReqPledgingPreviousQuarter._initialValue = (bsObs && bsObs())
                        ? bsObs().rightS6PubDepReqPledgingCurrentQuarter() || 0
                        : 0;


                    //Always check to set it and then move on
                    defaultSimulation(null, null);
                    globalContext.saveChangesQuiet();
                    //Aliases and Organization!
                    collateralTypeNameMap = {};
                    //collateralTypeNameMap["treasuryAgencyBonds"] = "Treasury/Agency Bonds";
                    collateralTypeNameMap["treasuryAgencyBonds"] = "Treasuries & Agencies";
                    //collateralTypeNameMap["mBSCMOAgency"] = "MBS/CMO (Agency Backed)";
                    collateralTypeNameMap["mBSCMOAgency"] = "Agency MBS/CMO";
                    //collateralTypeNameMap["mBSCMOPvt"] = "MBS/CMO (Private Label)";
                    collateralTypeNameMap["mBSCMOPvt"] = "Non-Agency MBS/CMO";
                    collateralTypeNameMap["corporateSecurities"] = "Corporates";
                    collateralTypeNameMap["municipalSecurities"] = "Municipals";
                    collateralTypeNameMap["equitySecurities"] = "Equities";
                    collateralTypeNameMap["otherInvestments"] = "Other Investments";
                    collateralTypeNameMap["agLoans"] = "Ag Loans";
                    collateralTypeNameMap["cILoans"] = "C & I Loans";
                    collateralTypeNameMap["constructionLoans"] = "Construction Loans";
                    collateralTypeNameMap["cRELoans"] = "CRE Loans";
                    collateralTypeNameMap["fHLBLetterOfCredit"] = "FHLB Letter of Credit";
                    collateralTypeNameMap["hELOCOtherLoans"] = "HELOC/Other Loans";
                    collateralTypeNameMap["resReLoans"] = "Res Re Loans";

                    //TODO HACK manually map Market Values extra data (fieldName, priority) for now
                    marketValuesMap = {};
                    marketValuesMap["Market Value"] = ["marketValue", 0];
                    marketValuesMap["Amount Pledged"] = ["amountPledged", 1];
                    marketValuesMap["Available/Unencumbered"] = ["availableUnencumbered", 2];
                    marketValuesMap["Available/Unencumbered for BS"] = ["availableUnencumberedBS", 3];
                    marketValuesMap["Excess Collateral"] = ["excessCollateral", 4];
                    marketValuesMap["Over Collateralized BS"] = ["overCollateralizedBS", 5];

                    //TODO HACK do the same with Secured Liabilities Types for now
                    secLiabTypesMap = {};
                    secLiabTypesMap["FHLB"] = "fHLB";
                    secLiabTypesMap["Fed Discount/Other Secured"] = "fedDiscountOtherSecured";
                    secLiabTypesMap["Wholesale Repos"] = "wholesaleRepos";
                    secLiabTypesMap["Retail Repos/Sweeps"] = "retailReposSweeps";
                    secLiabTypesMap["Municipal Deposits"] = "municipalDeposits";
                    secLiabTypesMap["Other"] = "other";
                    secLiabTypesMap["FHLB Letter of Credit"] = "fHLBLetterofCredit";

                    //Available Types
                    availableTypes = ko.computed(function () {
                        var typeIdsInUse = thisType().basicSurplusSecuredLiabilitiesTypes().map(function (type) {
                            return type.securedLiabilitiesTypeId();
                        });
                        var ret = [];
                        securedLiabilitiesTypes().forEach(function (type) {
                            ret.push({
                                id: type.id(),
                                name: type.name(),
                                inUse: typeIdsInUse.indexOf(type.id()) != -1
                            });
                        });
                        return ret;
                    });

                    //Enforce 0 values for !AllowCollateralValueEdit CollateralType values
                    thisType().collateralTypes().forEach(function (type) {
                        if (!type.allowCollateralValueEdit())
                            type.collateralValue(0);
                    });

                    //Subscriptions
                    subs = [];
                    sub(thisType().overrideSimulationTypeId, defaultSimulation);
                    sub(thisType().institutionDatabaseName, defaultSimulation);
                    sub(thisType().asOfDateOffset, defaultSimulation);
                    sub(thisType().simulationTypeId, refreshScens);
                    sub(thisType().overrideMarketValues, defaultMarketValues)
                    sub(thisType().institutionDatabaseName, updateAsOfDateOffsets);
                    sub(thisType().institutionDatabaseName, getBasicSurplusData);
                    sub(thisType().asOfDateOffset, getBasicSurplusData);
                    sub(thisType().simulationTypeId, getBasicSurplusData);
                    sub(thisType().scenarioTypeId, getBasicSurplusData);
                    subAll(thisType());
                    subAll(thisType().collateralTypes(), doSomeWork);
                    subAll(thisType().collateralTypes(), doLotsOfWork);
                    subAll(thisType().basicSurplusMarketValues(), doLotsOfWork);
                    subAll(thisType().basicSurplusSecuredLiabilitiesTypes(), doLotsOfWork);
                    thisType().basicSurplusSecuredLiabilitiesTypes().forEach(function (type) {
                        subAll(type.basicSurplusCollateralTypes(), doLotsOfWork);
                    });
                    subAll(thisType().leftS1OvntFunds());
                    subAll(thisType().leftS1SecCols());
                    subAll(thisType().leftS1LiqAssets());
                    subAll(thisType().leftS2VolLiabs());
                    subAll(thisType().leftS2TotalDepAmount());
                    subAll(thisType().leftS3LoanCols());
                    subAll(thisType().leftS3Loans());
                    subAll(thisType().leftS4BrokDeps());
                    subAll(thisType().midS1Miscs());
                    subAll(thisType().rightS1OtherLiqs());
                    subAll(thisType().rightS3UnsecBors());
                    subAll(thisType().rightS2SecBors());
                    //subAll(thisType().rightS4OtherSecs());
                    subAll(thisType().rightS4UnrealGL());
                    subAll(thisType().rightS5BorCaps());
                    subAll(thisType().rightS6Footnotes());

                    //Sorting
                    sortedCollateralTypes(thisType().collateralTypes());
                    sortedMarketValues(thisType().basicSurplusMarketValues());
                    sortedTypes(thisType().basicSurplusSecuredLiabilitiesTypes());
                    thisType().basicSurplusSecuredLiabilitiesTypes().forEach(function (type) {
                        type.sortedTypes = ko.observableArray(type.basicSurplusCollateralTypes());
                    });
                    sortedLeftS1OvntFunds(thisType().leftS1OvntFunds());
                    sortedLeftS1SecCols(thisType().leftS1SecCols());
                    sortedLeftS1LiqAssets(thisType().leftS1LiqAssets());
                    sortedLeftS2VolLiabs(thisType().leftS2VolLiabs());
                    sortedLeftS3LoanCols(thisType().leftS3LoanCols());

                    sortedLeftS3Loans(thisType().leftS3Loans());
                    sortedLeftS4BrokDeps(thisType().leftS4BrokDeps());
                    sortedMidS1Miscs(thisType().midS1Miscs());
                    sortedRightS1OtherLiqs(thisType().rightS1OtherLiqs());
                    sortedRightS3UnsecBors(thisType().rightS3UnsecBors());
                    sortedRightS2SecBors(thisType().rightS2SecBors());
                    //sortedRightS4OtherSecs = ko.observableArray(thisType().rightS4OtherSecs());
                    sortedRightS4UnrealGL(thisType().rightS4UnrealGL());
                    sortedRightS5BorCaps(thisType().rightS5BorCaps());
                    sortedRightS6Footnotes(thisType().rightS6Footnotes());
                    sortTypes();

                    //Initial Calcs Setup -- TODO somehow automate this in report creation
                    if (thisType().basicSurplusMarketValues().length == 0) {
                        addMarketValues().name("Market Value");
                        addMarketValues().name("Amount Pledged");
                        addMarketValues().name("Available/Unencumbered");
                        addMarketValues().name("Available/Unencumbered for BS");
                        addMarketValues().name("Excess Collateral");
                        addMarketValues().name("Over Collateralized BS");
                        sortTypes();
                    }

                    thisType().basicSurplusMarketValues().forEach(function (row) {
                        thisType().basicSurplusMarketValues[marketValuesMap[row.name()][0]] = row;
                    });

                    defaultMarketValues();

                    //TODO HACK Need a separate collateralTypesHTMLHashHack object because thisType().collateralTypes always resolves to observable in HTML and can't be hashed
                    if (thisType().collateralTypes().length == 0) {
                        //Order by Collateral Data Market Value table first...  (which conveniently puts enabled defaults in correct order too)
                        //addCollateralType("Treasury/Agency Bonds", "treasuryAgencyBonds", 100, true, true);
                        addCollateralType("Treasuries & Agencies", "treasuryAgencyBonds", 100, true, true);
                        //addCollateralType("MBS/CMO (Agency Backed)", "mBSCMOAgency", 95, true, true);
                        addCollateralType("Agency MBS/CMO", "mBSCMOAgency", 95, true, true);
                        //addCollateralType("MBS/CMO (Private Label)", "mBSCMOPvt", 90, true, true);
                        addCollateralType("Non-Agency MBS/CMO", "mBSCMOPvt", 90, true, true);
                        addCollateralType("Corporates", "corporateSecurities", 0, true, false);
                        addCollateralType("Municipals", "municipalSecurities", 0, true, false);
                        addCollateralType("Equities", "equitySecurities", 0, true, false);
                        addCollateralType("Other Investments", "otherInvestments", 0, true, false);

                        //... then by everything else alphabetically
                        addCollateralType("Ag Loans", "agLoans", 0, false, false);
                        addCollateralType("C & I Loans", "cILoans", 0, false, false);
                        addCollateralType("Construction Loans", "constructionLoans", 0, false, false);
                        addCollateralType("CRE Loans", "cRELoans", 0, false, false);
                        addCollateralType("FHLB Letter of Credit", "fHLBLetterOfCredit", 0, false, false);
                        addCollateralType("HELOC/Other Loans", "hELOCOtherLoans", 0, false, false);
                        addCollateralType("Res Re Loans", "resReLoans", 0, false, false);
                    }
                    thisType().collateralTypesHTMLHashHack = {};
                    thisType().collateralTypes().forEach(function (row) {
                        thisType().collateralTypes[row.fieldName()] = row;
                        thisType().collateralTypesHTMLHashHack[row.fieldName()] = row;

                        //Patch corporatesSecurities -> corporateSecurities
                        if (row.fieldName() == "corporatesSecurities") {
                            row.fieldName("corporateSecurities");
                            if (!row.override()) {
                                row.name("Corporates");
                            }
                        }
                    });

                    thisType().basicSurplusSecuredLiabilitiesTypes().forEach(function (type) {
                        thisType().basicSurplusSecuredLiabilitiesTypes[secLiabTypesMap[type.securedLiabilitiesType().name()]] = type;

                        //Note: Generic CollateralTypes now have a fieldName defined, so just use that
                        type.basicSurplusCollateralTypes().forEach(function (row) {
                            type[row.collateralType().fieldName()] = row;
                        });

                        //TODO HACK Force override on all types not in the BASIS data pull -- TODO maybe specify a field for this in generic type
                        assignDataPullField(type);
                    });

                    var subTypesToDel = [];
                    if (thisType().leftS1OvntFunds().length == 0) {
                        newType = addLeftS1OvntFunds();
                        newType.override(false);
                        newType.fieldName("leftS1OvntFundsSrtTrmInv");
                    }
                    thisType().leftS1OvntFunds().forEach(function (row) {
                        if (row.fieldName())
                            thisType().leftS1OvntFunds[row.fieldName()] = row;
                    });

                    if (thisType().leftS1SecCols().length == 0) {
                        newType = addLeftS1SecCols();
                        newType.override(false);
                        newType.fieldName("leftS1SecColsFHLB");
                        newType = addLeftS1SecCols();
                        newType.override(false);
                        newType.fieldName("leftS1SecColsOtherSec");
                        newType = addLeftS1SecCols();
                        newType.override(false);
                        newType.fieldName("leftS1SecColsWholeRepos");
                        newType = addLeftS1SecCols();
                        newType.override(false);
                        newType.fieldName("leftS1SecColsRetRepos");
                        newType = addLeftS1SecCols();
                        newType.override(false);
                        newType.fieldName("leftS1SecColsMuniDep");
                        newType = addLeftS1SecCols();
                        newType.override(false);
                        newType.fieldName("leftS1SecColsOther");
                    }
                    thisType().leftS1SecCols().forEach(function (row) {
                        if (row.fieldName())
                            thisType().leftS1SecCols[row.fieldName()] = row;
                    });

                    if (thisType().leftS1LiqAssets().length == 0) {
                        newType = addLeftS1LiqAssets(true);
                        newType.override(false);
                        newType.fieldName("leftS1LiqAssetsColSecPos");
                        newType = addLeftS1LiqAssets(false);
                        newType.override(false);
                        newType.fieldName("leftS1LiqAssetsGovLoans");
                        newType.include(false);
                        newType = addLeftS1LiqAssets(true);
                        newType.override(false);
                        newType.fieldName("leftS1LiqAssetsCashflow");
                        newType = addLeftS1LiqAssets(true);
                        newType.override(false);
                        newType.fieldName("leftS1LiqAssetsOther");
                    }
                    thisType().leftS1LiqAssets().forEach(function (row) {
                        if (row.fieldName())
                            thisType().leftS1LiqAssets[row.fieldName()] = row;
                    });

                    if (thisType().midS1Miscs().length == 0) {
                        newType = addMidS1Miscs();
                        newType.override(false);
                        newType.fieldName("midS1MiscsTotalAsset");
                    }
                    thisType().midS1Miscs().forEach(function (row) {
                        if (row.fieldName()) {
                            thisType().midS1Miscs[row.fieldName()] = row;

                            //Patch out midS1MiscsTotalDeposit
                            if (row.fieldName() == "midS1MiscsTotalDeposit")
                                subTypesToDel.push(row);
                        }
                    });

                    if (thisType().rightS1OtherLiqs().length == 0) {
                        newType = addRightS1OtherLiqs();
                        newType.override(false);
                        newType.fieldName("rightS1OtherLiqsCorpSec");
                        newType = addRightS1OtherLiqs();
                        newType.override(false);
                        newType.fieldName("rightS1OtherLiqsMuniSec");
                        newType = addRightS1OtherLiqs();
                        newType.override(false);
                        newType.fieldName("rightS1OtherLiqsEqSec");
                        newType = addRightS1OtherLiqs();
                        newType.override(false);
                        newType.fieldName("rightS1OtherLiqsOther");
                    }
                    thisType().rightS1OtherLiqs().forEach(function (row) {
                        if (row.fieldName())
                            thisType().rightS1OtherLiqs[row.fieldName()] = row;
                    });

                    if (thisType().rightS3UnsecBors().length == 0) {
                        newType = addRightS3UnsecBors();
                        newType.override(false);
                        newType.fieldName("rightS3UnsecBorsFedFunds");
                    }
                    thisType().rightS3UnsecBors().forEach(function (row) {
                        if (row.fieldName())
                            thisType().rightS3UnsecBors[row.fieldName()] = row;
                    });

                    if (thisType().rightS2SecBors().length == 0) {
                        newType = addRightS2SecBors();
                        newType.override(false);
                        newType.fieldName("rightS2SecBorsFedBIC");
                        newType = addRightS2SecBors();
                        newType.override(false);
                        newType.fieldName("rightS2SecBorsOther");
                    }
                    thisType().rightS2SecBors().forEach(function (row) {
                        if (row.fieldName())
                            thisType().rightS2SecBors[row.fieldName()] = row;
                    });

                if (thisType().rightS4UnrealGL().length == 0) {
                    newType = addRightS4UnrealGL();
                    newType.override(false);
                    newType.fieldName("rightS4BaseGL");
                    newType.title("Base");
                    newType = addRightS4UnrealGL();
                    newType.override(false);
                    newType.fieldName("rightS4OtherScenGL");
                    newType.title("Shock Up 100BP");
                    newType = addRightS4UnrealGL();
                    newType.override(false);
                    newType.fieldName("rightS4Diff");
                    newType.title("Difference");
                }
                thisType().rightS4UnrealGL().forEach(function (row) {
                    if (row.fieldName())
                        thisType().rightS4UnrealGL[row.fieldName()] = row;
                });

                    calcUnrealized(false, "rightS4BaseGL");
                    calcUnrealized(false, "rightS4OtherScenGL")

                    subs.push(thisType().rightS4UnrealGL["rightS4BaseGL"].override.subscribe(function (newValue) {

                        calcUnrealized(false, "rightS4BaseGL");
                    }));

                    subs.push(thisType().rightS4UnrealGL["rightS4OtherScenGL"].override.subscribe(function (newValue) {

                        calcUnrealized(false, "rightS4OtherScenGL");
                    }));

                    if (thisType().rightS5BorCaps().length == 0) {
                        newType = addRightS5BorCaps();
                        newType.override(false);
                        newType.fieldName("rightS5BorCapsMaxBorCap");
                        newType = addRightS5BorCaps();
                        newType.override(false);
                        newType.fieldName("rightS5BorCapsCurOutBor");
                    }
                    thisType().rightS5BorCaps().forEach(function (row) {
                        if (row.fieldName())
                            thisType().rightS5BorCaps[row.fieldName()] = row;
                    });

                    if (thisType().rightS6Footnotes().length == 0) {
                        newType = addRightS6Footnotes();
                        newType.override(false);
                        newType.fieldName("rightS6FootnotesReqPledging");
                    }
                    thisType().rightS6Footnotes().forEach(function (row) {
                        if (row.fieldName()) {
                            thisType().rightS6Footnotes[row.fieldName()] = row;

                            //Patch out rightS6FootnotesLiquidity
                            if (row.fieldName() == "rightS6FootnotesLiquidity")
                                subTypesToDel.push(row);
                        }
                    });

                    //Remove anything that needs to be patched out
                    subTypesToDel.forEach(function (row) {
                        removeType(row, undefined, true);
                    });
                    isSaving(false);
                                   
            });
        };

        var compositionComplete = function () {
            doSomeWork();
            doLotsOfWork();

            return save(undefined, undefined, true).then(function () {
                return true;
            });
        };

        var updateAsOfDateOffsets = function (newValue, lastModified) {

            //Guard against firing while cancelling changes
            if (isSaving())
                return;

            return globalContext.getAsOfDateOffsets(asOfDateOffsets, newValue);
        };

        var getBasicSurplusData = function (newValue, lastModified, skipCalc) {
            var self = this;
            //Guard against firing while cancelling changes
            if (isSaving())
                return;


            var bsData = ko.observableArray();

   
            return globalContext.getBasicSurplusAdminData(
                thisType().institutionDatabaseName(),
                thisType().asOfDateOffset(),
                thisType().simulationTypeId(),
                thisType().scenarioTypeId(),
                bsData
            ).then(function () {
                bsData().forEach(function (row) {
                    basicSurplusData[row.fieldName] = row;
                });
                if (!skipCalc) {
                    doLotsOfWork(); //Need this in here again as some time may have passed from getting the data back
                }

            });
        };

        //Helpers
        var sub = function (thing, func) {
          
            subs.push((func)
                ? thing.subscribe(function (newValue) {
                    func(newValue, thing);
                })
                : thing.subscribe(function (newValue) {
                    doRelativelyInsaneAmountOfWork(newValue, thing);
                })
            );
        };

        var subAll = function (thing, func) {
            for (var k in thing) {
                if (thing[k] && thing[k].subscribe) {
                    sub(thing[k], func);
                }

                if (thing.forEach) {
                    thing.forEach(function (row) {
                        for (var k in row)
                            if (row[k] && row[k].subscribe)
                                sub(row[k], func);
                    });
                }
            }
        };

        var getField = function (startObject, fields, rawReturn, valIfNull) {
            var testVar = startObject;

            fields.forEach(function (f) {
                //May need to unwrap intermediate observables - if this may be the case, explicitly check just to be sure
                if (testVar) {
                    testVar = ((typeof testVar == 'function' && testVar()[f]) ? testVar() : testVar)[f];
                }                    
            });

            //Optionally return unwrapped or with custom valIfNull if we didn't find something valid
            return (testVar)
                ? ((rawReturn) ? testVar : testVar())
                : ((valIfNull) ? valIfNull : 0);
        };

        var assignDataPullField = function (type) {
            switch (type.securedLiabilitiesType().name()) {
                case 'FHLB':
                    type.dataPullField = "collateralFHLBOutstanding";
                    break;
                case 'Fed Discount/Other Secured':
                    type.dataPullField = "collateralFedDiscountOtherSecuredOutstanding";
                    break;
                case 'Retail Repos/Sweeps':
                    type.dataPullField = "collateralRetailReposSweepsOutstanding";
                    break;
                case 'Wholesale Repos':
                    type.dataPullField = "collateralWholesaleReposOutstanding";
                    break;
                default:
                    type.dataPullField = null;
                    type.override(true); //Shouldn't be needed, but enforce it just in case for old types
            }
        };
        var sumRow = function (row, cols) {
            var ret = 0;
            cols.forEach(function (col) {
                ret += row[col[0]]();
            });
            return ret;
        };
        var sumCollateralField = function (fName, field) {
            
            var ret = 0;
            thisType().basicSurplusSecuredLiabilitiesTypes().forEach(function (type) {
                type.basicSurplusCollateralTypes().forEach(function (row) {
                    switch (row.collateralType().fieldName()) {
                        case fName:
                            ret += row[field]();
                    }
                });
            });
            return ret;
        };
        var findCollateralValue = function (fName) {
            var ret = 0;
            thisType().collateralTypes().forEach(function (type) {
                switch (type.fieldName()) {
                    case fName:
                        ret = type.collateralValue();
                }
            });
            return ret;
        };

        //Heavy Lifting
        var doSomeWork = function (newValue, lastModified) {
            var self = this;
            //Guard against firing more than once per update or during initial activation
            if (isSaving())
                return;
            isSaving(true);

            thisType().collateralTypes().forEach(function (row) {
                if (!row.override())
                    row.name(collateralTypeNameMap[row.fieldName()]);
            });

            isSaving(false);
        };

        var doLotsOfWork = function (newValue, lastModified) {
            console.log('doing lots of work');
            //Note: This calls doRelativelyInsaneAmountOfWork at the end of the function, so no need to subscribe both
            //Guard against firing more than once per update or during initial activation
            if (isSaving())
                return;
            isSaving(true);

            //Bottom Screen
            var denom = 0;
            var rpr = 0;
            thisType().basicSurplusSecuredLiabilitiesTypes().forEach(function (type) {
                if (!type.override())
                    type.outstanding(getField(basicSurplusData, [type.dataPullField, "data"], true));
                type.pledgingRequired(type.outstanding() * type.pledgingFactor() / 100.0);
                rpr = type.pledgingRequired();
                type.basicSurplusCollateralTypes().forEach(function (row) {
                    row.pledgingRequired(rpr);
                    if (lastModified == row.netCollateralValue) {
                        denom = row.amountPledgedMktVal();
                        row.collateralValuePercentage((denom == 0) ? 0 : (row.netCollateralValue() / denom) * 100.0);
                    }
                    else
                        row.netCollateralValue(row.amountPledgedMktVal() * row.collateralValuePercentage() / 100.0);
                    row.remainingPledgingRequired(row.pledgingRequired() - ((row.maxCollateralUsed() <= 0 || row.netCollateralValue() < row.maxCollateralUsed()) ? row.netCollateralValue() : row.maxCollateralUsed()));
                    if (row.remainingPledgingRequired() <= 0)
                        row.remainingPledgingRequired(0);
                    denom = row.collateralValuePercentage() / 100.0;
                    row.excessCollateralMktVal((denom == 0) ? 0 : (row.netCollateralValue() - ((row.maxCollateralUsed() <= 0 || row.netCollateralValue() < row.maxCollateralUsed() || row.pledgingRequired() < row.maxCollateralUsed())
                        ? row.pledgingRequired() : row.maxCollateralUsed())) / denom);
                    if (row.excessCollateralMktVal() <= 0)
                        row.excessCollateralMktVal(0);
                    row.overCollateralizedBasicSurplus(row.excessCollateralMktVal() * row.collateralType().collateralValue() / 100.0);
                    rpr = row.remainingPledgingRequired();
                });
            });

            //Top Screen
            var field = '';
            var cols = [['treasuryAgencyBonds', 'Treasuries & Agencies'],
                ['mBSCMOAgency', 'Agency MBS/CMO'],
                ['mBSCMOPvt', 'Non-Agency MBS/CMO'],
            ['corporateSecurities', 'Corporates'],
            ['municipalSecurities', 'Municipals'],
            ['equitySecurities', 'Equities'],
            ['otherInvestments', 'Other Investments']];
            thisType().basicSurplusMarketValues().forEach(function (row) {
                switch (row.name()) {
                    //case 'Market Value':
                    //    break;
                    case 'Amount Pledged':
                        field = 'amountPledgedMktVal';
                        cols.forEach(function (col) {
                            row[col[0]](sumCollateralField(col[0], field));
                        });
                        break;
                    case 'Available/Unencumbered':
                        cols.forEach(function (col) {
                            row[col[0]](thisType().basicSurplusMarketValues.marketValue[col[0]]() - thisType().basicSurplusMarketValues.amountPledged[col[0]]());
                        });
                        break;
                    case 'Available/Unencumbered for BS':
                        cols.forEach(function (col) {
                            row[col[0]](thisType().basicSurplusMarketValues.availableUnencumbered[col[0]]() * findCollateralValue(col[0]) / 100.0);
                        });
                        break;
                    case 'Excess Collateral':
                        field = 'excessCollateralMktVal';
                        cols.forEach(function (col) {
                            row[col[0]](sumCollateralField(col[0], field));
                        });
                        break;
                    case 'Over Collateralized BS':
                        field = 'overCollateralizedBasicSurplus';
                        cols.forEach(function (col) {
                            row[col[0]](sumCollateralField(col[0], field));
                        });
                }
                row.total(sumRow(row, cols));
            });

            isSaving(false);

            //This needs to update the rest of the Basic Surplus Admin values
            doRelativelyInsaneAmountOfWork(newValue, lastModified);
        };

        var doRelativelyInsaneAmountOfWork = function (newValue, lastModified) {
            console.log('DUN DUN DUN');
            var self = this;
            //Guard against firing more than once per update or during initial activation
            if (isSaving())
                return;
            isSaving(true);

            //Vars
            var totals = [];
            var field = "";
            var newType;
            var title = "";
            var percent = 0;
            var amount = 0;

            //Left
            //Section 1
            if (!thisType().leftS1HeaderOverride()) {
                //thisType().leftS1HeaderTitle("I. LIQUID ASSETS");
                thisType().leftS1HeaderTitle("Tier 1 Basic Surplus");
            }

            thisType().leftS1OvntFunds().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "leftS1OvntFundsSrtTrmInv":
                            row.title("Overnight Funds Sold & Short-Term Investments (avg. balance, if wide daily fluctuations)");
                            row.balance(getField(basicSurplusData, ["leftS1OvntFundsSrtTrmInv", "data"], true));
                            break;
                    }
                }
            });

            if (!thisType().leftS1SecColOverride()) {
                thisType().leftS1SecColTitle("Security Collateral");
            }

            //TODO Collateral Value percentages? (CollateralType CollateralValue?)

            //reset collateral type names if not overridden
            thisType().collateralTypes().forEach(function (row) {

                if (!row.override()) {
                    row.name(collateralTypeNameMap[row.fieldName()]);
                }
            });

            if (!thisType().leftS1SecColTotalSecMktValOverride()) {
                //thisType().leftS1SecColTotalSecMktValTitle("Total Market Value of Securities");
                thisType().leftS1SecColTotalSecMktValTitle("Market Value");
                thisType().leftS1SecColTotalSecMktValUSTAgency(getField(thisType().basicSurplusMarketValues, ["marketValue", "treasuryAgencyBonds"]));
                thisType().leftS1SecColTotalSecMktValMBSAgency(getField(thisType().basicSurplusMarketValues, ["marketValue", "mBSCMOAgency"]));
                thisType().leftS1SecColTotalSecMktValMBSPvt(getField(thisType().basicSurplusMarketValues, ["marketValue", "mBSCMOPvt"]));
            }
            if (!thisType().leftS1SecColLessSecPledgedOverride()) {
                thisType().leftS1SecColLessSecPledgedTitle("Less Securities Pledged to:");
            }

            totals = [0, 0, 0];
            totals[0] += thisType().leftS1SecColTotalSecMktValUSTAgency();
            totals[1] += thisType().leftS1SecColTotalSecMktValMBSAgency();
            totals[2] += thisType().leftS1SecColTotalSecMktValMBSPvt();
            thisType().leftS1SecCols().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "leftS1SecColsFHLB":
                            field = "fHLB";
                            row.title("FHLB");
                            break;
                        case "leftS1SecColsOtherSec":
                            field = "fedDiscountOtherSecured";
                            row.title("Fed Discount/Other Secured");
                            break;
                        case "leftS1SecColsWholeRepos":
                            field = "wholesaleRepos";
                            row.title("Wholesale Repos");
                            break;
                        case "leftS1SecColsRetRepos":
                            field = "retailReposSweeps";
                            row.title("Retail Repos/Sweeps");
                            break;
                        case "leftS1SecColsMuniDep":
                            field = "municipalDeposits";
                            row.title("Municipal Deposits");
                            break;
                        case "leftS1SecColsOther":
                            field = "other";
                            row.title("Other");
                            break;
                    }
                    row.uSTAgency(getField(thisType().basicSurplusSecuredLiabilitiesTypes, [field, "treasuryAgencyBonds", "amountPledgedMktVal"]) * -1);
                    row.mBSAgency(getField(thisType().basicSurplusSecuredLiabilitiesTypes, [field, "mBSCMOAgency", "amountPledgedMktVal"]) * -1);
                    row.mBSPvt(getField(thisType().basicSurplusSecuredLiabilitiesTypes, [field, "mBSCMOPvt", "amountPledgedMktVal"]) * -1);
                }
                totals[0] += row.uSTAgency();
                totals[1] += row.mBSAgency();
                totals[2] += row.mBSPvt();
            });

            //TODO this and other totaling logic will probably move to ReportView
            if (!thisType().leftS1SecColTotalSecColOverride())
                thisType().leftS1SecColTotalSecColTitle("Available / Unencumbered Security Collateral");
            thisType().leftS1SecColTotalSecColUSTAgency(totals[0] * thisType().collateralTypes.treasuryAgencyBonds.collateralValue() / 100.0);
            thisType().leftS1SecColTotalSecColMBSAgency(totals[1] * thisType().collateralTypes.mBSCMOAgency.collateralValue() / 100.0);
            thisType().leftS1SecColTotalSecColMBSPvt(totals[2] * thisType().collateralTypes.mBSCMOPvt.collateralValue() / 100.0);

            if (!thisType().leftS1BlankTotalOverride())
                thisType().leftS1BlankTotalTitle(""); //This should be Intentionally left blank - Total of Unencumbered Securities
            thisType().leftS1BlankTotalAmount(thisType().leftS1SecColTotalSecColUSTAgency() + thisType().leftS1SecColTotalSecColMBSAgency() + thisType().leftS1SecColTotalSecColMBSPvt());

            totals = [0];
            thisType().leftS1LiqAssets().forEach(function (row) {

                switch (row.fieldName()) {
                    case "leftS1LiqAssetsColSecPos":
                        if (!row.override()) {
                            row.title("Over Collateralized Securities Pledging Position");
                            row.amount(getField(thisType().basicSurplusMarketValues, ["overCollateralizedBS", "total"]));
                        }

                        break;
                    case "leftS1LiqAssetsGovLoans":
                        if (!row.override()) {
                            row.title("Government Guaranteed Loans");
                            percent = row.percent();
                            amount = row.amount();
                            var fieldData = getField(basicSurplusData, ["leftS1LiqAssetsGovLoans", "data"], true);
                            if (lastModified == row.amount) {
                                if (fieldData == 0) {
                                    row.percent(0);
                                } else {
                                    row.percent(amount / fieldData);
                                }
                                //row.percent(((getField(basicSurplusData, ["leftS1LiqAssetsGovLoans", "data"], true) == 0) ? 0 : amount / getField(basicSurplusData, ["leftS1LiqAssetsGovLoans", "data"], true)) * 100.0);
                            } else {
                                //TODO else if (percent > 0)?
                                row.amount(getField(basicSurplusData, ["leftS1LiqAssetsGovLoans", "data"], true) * percent / 100.0);
                            }
                        }
                        

                        break;
                    case "leftS1LiqAssetsCashflow":
                        if (!row.override()) {
                            row.title("Cash flow (< 30 Days) from Securities not listed above");
                            row.amount(getField(basicSurplusData, ["leftS1LiqAssetsCashflow", "data"], true));
                        }

                        break;
                    case "leftS1LiqAssetsOther":
                        if (!row.override()) {
                            row.title("Other Liquid Assets (Int Bearing Deposits, MM Mutual Funds, etc.)");
                            row.amount(getField(basicSurplusData, ["leftS1LiqAssetsOther", "data"], true));
                        }
                        break;
                }
                totals[0] += row.amount(); //TODO should we really be totaling these different fields together with OvntFunds?
            });

            thisType().leftS1OvntFunds().forEach(function (row) {
                totals[0] += row.balance();
            });
            if (!thisType().leftS1LiqAssetTotalOverride())
                //thisType().leftS1LiqAssetTotalTitle("TOTAL LIQUID ASSETS");
                thisType().leftS1LiqAssetTotalTitle("Total Liquid Assets");
            thisType().leftS1LiqAssetTotalAmount(totals[0] + thisType().leftS1BlankTotalAmount());

            //Section 2
            if (!thisType().leftS2HeaderOverride()) {
                //thisType().leftS2HeaderTitle("II. SHORT TERM / POTENTIALLY VOLATILE LIABILITIES & COVERAGE");
                thisType().leftS2HeaderTitle("Volatile Liabilities Coverage:");

            }

            if (!thisType().leftS2MatUnsecLiabOverride()) {
                thisType().leftS2MatUnsecLiabTitle("Maturing Unsecured Liabilities (< 30 Days)");
                thisType().leftS2MatUnsecLiabAmount(getField(basicSurplusData, ["leftS2MatUnsecLiabAmount", "data"], true));
            }
            if (!thisType().leftS2DepCovOverride()) {
                thisType().leftS2DepCovTitle("Deposit Coverages");
            }
            //string leftS2DepCovOption

            //Note: From Middle Section 1, formerly midS1MiscsTotalDeposit
            if (!thisType().midS1TotalDepositOverride()) {
                thisType().midS1TotalDepositAmount(getField(basicSurplusData, ["midS1MiscsTotalDeposit", "data"], true));
            }

            if (!thisType().leftS2TotalDepOverride()) {
                percent = thisType().leftS2TotalDepPct();
                //title = percent + "% of Total Deposits";
                amount = thisType().midS1TotalDepositAmount();
                if (thisType().leftS2TotalDepExclBrokeredOneWay()
                    || thisType().leftS2TotalDepExclBrokeredRecip()
                    || thisType().leftS2TotalDepExclPublicDeposits()
                    || thisType().leftS2TotalDepExclNationalCDs()) {
                    var multiExlude = false;
                    title += " Excl.";
                    if (thisType().leftS2TotalDepExclBrokeredOneWay()) {
                        title += " Brokered/One Way";
                        amount -= getField(basicSurplusData, ["leftS2TotalDepAmountBrokered", "data"], true);
                        amount -= getField(basicSurplusData, ["leftS2TotalDepAmountBrokeredOneWay", "data"], true);
                        multiExlude = true;
                    }
                    if (thisType().leftS2TotalDepExclBrokeredRecip()) {
                        if (multiExlude)
                            title += ",";
                        title += " Brokered Recip.";
                        amount -= getField(basicSurplusData, ["leftS2TotalDepAmountBrokeredRecip", "data"], true);
                        multiExlude = true;
                    }
                    if (thisType().leftS2TotalDepExclPublicDeposits()) {
                        if (multiExlude)
                            title += ",";
                        title += " Public Deposits";
                        amount -= getField(basicSurplusData, ["leftS2TotalDepAmountPublicDeposits", "data"], true);
                        multiExlude = true;
                    }
                    if (thisType().leftS2TotalDepExclNationalCDs()) {
                        if (multiExlude)
                            title += ",";
                        title += " National CD";
                        amount -= getField(basicSurplusData, ["leftS2TotalDepAmountNationalCDs", "data"], true);
                    }
                }

                thisType().leftS2TotalDepAmount(amount * percent / 100.0);
                //get total assets amount
                for (var i = 0; i < thisType().midS1Miscs().length; i++) {
                    if (thisType().midS1Miscs()[i].fieldName() == "midS1MiscsTotalAsset") {
                        amount = thisType().midS1Miscs()[i].amount();
                    }
                }
                title = "Deposit Coverage (" + numeral((thisType().leftS2TotalDepAmount() / amount)).format("0%") + " of Assets/" + numeral(thisType().leftS2TotalDepPct()).format("0") + "% of Deposits)" + title;
                thisType().leftS2TotalDepTitle(title);
            }

            if (!thisType().leftS2RegCDMatOverride()) {
                //thisType().leftS2RegCDMatPct(0); //Note: Always entered
                percent = thisType().leftS2RegCDMatPct();
                thisType().leftS2RegCDMatTitle(percent + "% of CDs Maturing < 30 Days");
                amount = getField(basicSurplusData, ["leftS2RegCDMatAmount", "data"], true);
                if (thisType().leftS2VolLiabInclPublicCD())
                    amount += getField(basicSurplusData, ["leftS2RegCDMatAmountPublic", "data"], true);
                thisType().leftS2RegCDMatAmount(amount * percent / 100.0);
            }
            if (!thisType().leftS2JumboCDMatOverride()) {
                //thisType().leftS2JumboCDMatPct(0); //Note: Always entered
                percent = thisType().leftS2JumboCDMatPct();
                thisType().leftS2JumboCDMatTitle(percent + "% of Jumbo CDs Maturing < 30 Days");
                amount = getField(basicSurplusData, ["leftS2RegJumboCDMatAmount", "data"], true);
                if (thisType().leftS2VolLiabInclPublicJumboCD())
                    amount += getField(basicSurplusData, ["leftS2RegJumboCDMatAmountPublic", "data"], true);
                thisType().leftS2JumboCDMatAmount(amount * percent / 100.0);
            }
            if (!thisType().leftS2OtherDepOverride()) {
                //thisType().leftS2OtherDepPct(0); //Note: Always entered
                percent = thisType().leftS2OtherDepPct();
                thisType().leftS2OtherDepTitle(percent + "% of Other Deposits");
                amount = getField(basicSurplusData, ["leftS2OtherDepAmount", "data"], true);
                if (thisType().leftS2VolLiabInclPublicDDANOWMMDA())
                    amount += getField(basicSurplusData, ["leftS2OtherDepAmountPublic", "data"], true);
                thisType().leftS2OtherDepAmount(amount * percent / 100.0);
            }

            //Note: Not used if not entered
            if (!thisType().leftS2AddLiqResOverride()) {
                //thisType().leftS2AddLiqResTitle("Additional Liquidity Reserves");
                thisType().leftS2AddLiqResTitle("Other Coverages");
                //thisType().leftS2AddLiqResAmount(0); //Note: Always entered
            }

            totals = [0];
            if (thisType().leftS2DepCovOption() == "0")
                totals[0] += thisType().leftS2TotalDepAmount();
            else {
                totals[0] += thisType().leftS2RegCDMatAmount();
                totals[0] += thisType().leftS2JumboCDMatAmount();
                totals[0] += thisType().leftS2OtherDepAmount();
                thisType().leftS2VolLiabs().forEach(function (row) {
                    totals[0] += row.amount();
                });

                thisType().leftS2TotalDepPct(((thisType().midS1TotalDepositAmount() == 0) ? 0 : totals[0] / thisType().midS1TotalDepositAmount()) * 100.0);

                thisType().leftS2TotalDepAmount(totals[0]);
                //get total assets amount
                for (var i = 0; i < thisType().midS1Miscs().length; i++) {
                    if (thisType().midS1Miscs()[i].fieldName() == "midS1MiscsTotalAsset") {
                        amount = thisType().midS1Miscs()[i].amount();
                    }
                }
                title = "Deposit Coverage (" + numeral((thisType().leftS2TotalDepAmount() / amount)).format("0%") + " of Assets/" + numeral(thisType().leftS2TotalDepPct()).format("0") + "% of Deposits)";
                thisType().leftS2TotalDepTitle(title);
                //thisType().leftS2TotalDepTitle(thisType().leftS2TotalDepPct() + "% of Total Deposits");
            }
            totals[0] += thisType().leftS2MatUnsecLiabAmount();
            totals[0] += thisType().leftS2AddLiqResAmount();
            if (!thisType().leftS2VolLiabTotalOverride())
                thisType().leftS2VolLiabTotalTitle("TOTAL SHORT TERM & POTENTIALLY VOLATILE LIABILITIES AND COVERAGES");
            thisType().leftS2VolLiabTotalAmount(totals[0]);

            if (!thisType().leftS2SubHeaderOverride()) {
                //thisType().leftS2SubHeaderTitle("BASIC SURPLUS");
                thisType().leftS2SubHeaderTitle("Tier 1 Basic Surplus");
            }

            //Section 3
            if (!thisType().leftS3HeaderOverride()) {
                //thisType().leftS3HeaderTitle("III. QUALIFYING FHLB LOAN COLLATERAL");
                thisType().leftS3HeaderTitle("Tier 2 Basic Surplus");
            }

            if (!thisType().leftS3FamResReColOverride()) {
                thisType().leftS3FamResReColAmountPledged(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "resReLoans", "amountPledgedMktVal"]));
                thisType().leftS3FamResReColCollateralValue(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "resReLoans", "collateralValuePercentage"]));
                thisType().leftS3FamResReColNetCollateral(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "resReLoans", "netCollateralValue"]));
                thisType().leftS3FamResReColL360Collateral(thisType().leftS3FamResReColNetCollateral());
            }
            if (!thisType().leftS3HELOCOtherColOverride()) {
                thisType().leftS3HELOCOtherColAmountPledged(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "hELOCOtherLoans", "amountPledgedMktVal"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "constructionLoans", "amountPledgedMktVal"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "cILoans", "amountPledgedMktVal"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "agLoans", "amountPledgedMktVal"]) );
                thisType().leftS3HELOCOtherColCollateralValue(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "hELOCOtherLoans", "collateralValuePercentage"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "constructionLoans", "collateralValuePercentage"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "cILoans", "collateralValuePercentage"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "agLoans", "collateralValuePercentage"]) );
                thisType().leftS3HELOCOtherColNetCollateral(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "hELOCOtherLoans", "netCollateralValue"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "constructionLoans", "netCollateralValue"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "cILoans", "netCollateralValue"]) + getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "agLoans", "netCollateralValue"]));
                thisType().leftS3HELOCOtherColL360Collateral(thisType().leftS3HELOCOtherColNetCollateral());
            }
            if (!thisType().leftS3CREColOverride()) {
                thisType().leftS3CREColAmountPledged(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "cRELoans", "amountPledgedMktVal"]));
                thisType().leftS3CREColCollateralValue(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "cRELoans", "collateralValuePercentage"]));
                thisType().leftS3CREColNetCollateral(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "cRELoans", "netCollateralValue"]));
                thisType().leftS3CREColL360Collateral(thisType().leftS3CREColNetCollateral());
            }

            if (!thisType().leftS3MaxBorLineFHLBOverride()) {
                title = "A. Maximum Borrowing Line at FHLB";
                switch (thisType().leftS3MaxBorLineFHLBType()) {
                    case "Percent":
                        percent = thisType().leftS3MaxBorLineFHLBPct();
                        title += (percent == 0) ? " Not Available" : " (Up to " + percent + "% of Assets)";
                        thisType().leftS3MaxBorLineFHLBAmount(thisType().midS1Miscs.midS1MiscsTotalAsset.amount() * percent / 100.0);
                        break;
                    case "Dollar":
                        thisType().leftS3MaxBorLineFHLBPct(0);
                        if (thisType().leftS3MaxBorLineFHLBAmount() == 0)
                            title += " Not Available";
                        break;
                    case "None":
                        thisType().leftS3MaxBorLineFHLBPct(0);
                        title += " Not Available";
                        thisType().leftS3MaxBorLineFHLBAmount(0);
                        break;
                }
                thisType().leftS3MaxBorLineFHLBTitle(title);
            }
            if (!thisType().leftS3LoanColFHLBOverride()) {
                //thisType().leftS3LoanColFHLBTitle("B. Qualifying Loan Collateral at the FHLB (net of haircut)");
                thisType().leftS3LoanColFHLBTitle("B. Loan Collateral at the FHLB (net of haircut)");
                amount = thisType().leftS3FamResReColNetCollateral() + thisType().leftS3HELOCOtherColNetCollateral() + thisType().leftS3CREColNetCollateral();
                thisType().leftS3LoanCols().forEach(function (row) {
                    amount += row.netCollateral();
                });
                thisType().leftS3LoanColFHLBAmount(amount);
                thisType().leftS3LoanColFHLBL360(amount);
            }
            if (!thisType().leftS3ExcessLoanColOverride()) {
                thisType().leftS3ExcessLoanColTitle("C. Excess Loan Collateral (if A < B)");
                thisType().leftS3ExcessLoanColAmount(thisType().leftS3LoanColFHLBAmount() - thisType().leftS3MaxBorLineFHLBAmount());
                if (thisType().leftS3MaxBorLineFHLBAmount() == 0 || thisType().leftS3ExcessLoanColAmount() < 0)
                    thisType().leftS3ExcessLoanColAmount(0);
            }

            if (!thisType().leftS3MaxBorCapOverride()) {
                thisType().leftS3MaxBorCapTitle("Maximum Borrowing Capacity (Lesser of A or B)");
                thisType().leftS3MaxBorCapAmount((thisType().leftS3MaxBorLineFHLBAmount() == 0 || thisType().leftS3LoanColFHLBAmount() < thisType().leftS3MaxBorLineFHLBAmount()) ? thisType().leftS3LoanColFHLBAmount() : thisType().leftS3MaxBorLineFHLBAmount());
            }
            if (!thisType().leftS3ColEncOverride()) {
                thisType().leftS3ColEncTitle("Collateral Currently Encumbered by Outstanding Advances/Letters of Credit");
                thisType().leftS3ColEncAmount(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["fHLB", "outstanding"])); //TODO may need fallback to BASIS data directly if this isn't present but that would be a pain and also inconsistent so maybe not
            }

            totals = [0];
            thisType().leftS3Loans().forEach(function (row) {
                totals[0] += row.amount();
            });
            if (!thisType().leftS3LoanTotalOverride())
                //thisType().leftS3LoanTotalTitle("REMAINING FHLB LOAN BASED BORROWING CAPACITY");
                thisType().leftS3LoanTotalTitle("Remaining FHLB Loan Based Borrowing Capacity");
            thisType().leftS3LoanTotalAmount(thisType().leftS3MaxBorCapAmount() - thisType().leftS3ColEncAmount() - totals[0]);
            if (thisType().leftS3LoanTotalAmount() < 0)
                thisType().leftS3LoanTotalAmount(0);

            if (!thisType().leftS3SubHeaderOverride()) {
                //thisType().leftS3SubHeaderTitle("BASIC SURPLUS W/ FHLB");
                thisType().leftS3SubHeaderTitle("Tier 2 Basic Surplus");
            }

            //Section 4
            if (!thisType().leftS4HeaderOverride()) {
                //thisType().leftS4HeaderTitle("IV. BROKERED DEPOSIT ACCESS");
                thisType().leftS4HeaderTitle("Tier 3 Basic Surplus");
            }

            if (!thisType().leftS4BrokDepCapOverride()) {
                var policyLimit = !thisType().leftS4BrokDepPolicyLimit();
                title = "Maximum Board Authorized Brokered Deposit Capacity";
                switch (thisType().leftS4BrokDepType()) {
                    case "Assets":
                        title += " (";
                        if (policyLimit) {
                            percent = getField(basicSurplusData, ["brokAssetPolicyLimit", "data"], true);

                            if (percent == -999)
                                percent = 0;
                            title += "Policy Limit of ";
                            thisType().leftS4BrokDepCapPct(percent); //Note: Always entered otherwise
                        }
                        else
                            percent = thisType().leftS4BrokDepCapPct();
                        title += percent + "% of Assets)";
                        if (percent == 0) {
                            thisType().leftS4BrokDepCapAmount(0);
                        }
                        else {
                            thisType().leftS4BrokDepCapAmount(getField(basicSurplusData, ["midS1MiscsTotalAsset", "data"], true) * percent / 100.0);
                        }
                       
                        break;
                    case "Deposits":
                        title += " (";
                        if (policyLimit) {
                            percent = getField(basicSurplusData, ["brokDepositPolicyLimit", "data"], true);
                            if (percent == -999) {
                                percent = 0;
                            }           
                            title += "Policy Limit of ";
                            thisType().leftS4BrokDepCapPct(percent); //Note: Always entered otherwise
                        }
                        else {
                            percent = thisType().leftS4BrokDepCapPct();
                        }
                        title += percent + "% of Deposits)";
                        if (percent == 0) {
                            thisType().leftS4BrokDepCapAmount(0);
                        }
                        else {
                            thisType().leftS4BrokDepCapAmount(thisType().midS1TotalDepositAmount() * percent / 100.0);
                        }
                        
                        break;
                    case "Dollar":
                        thisType().leftS4BrokDepCapPct(0);
                        if (policyLimit) {
                            amount = getField(basicSurplusData, ["brokMaxPolicyLimit", "data"], true);
                            if (amount == -999)
                                amount = 0;
                            title += " (per Policy)";
                            thisType().leftS4BrokDepCapAmount(amount); //Note: Always entered otherwise
                        }
                        break;
                    case "None":
                        thisType().leftS4BrokDepCapPct(0);
                        title += " Not Available";
                        thisType().leftS4BrokDepCapAmount(0);
                        thisType().leftS4BrokDepTotalAmount(0);

                        break;
                }
                thisType().leftS4BrokDepCapTitle(title);
            }
            if (!thisType().leftS4BrokDepBalOverride()) {
                title = "Current Brokered Deposit Balances";
                amount = getField(basicSurplusData, ["leftS2TotalDepAmountBrokered", "data"], true);
                if (!thisType().leftS4BrokDepBalExclBrokeredOneWay())
                    amount += getField(basicSurplusData, ["leftS2TotalDepAmountBrokeredOneWay", "data"], true);
                if (!thisType().leftS4BrokDepBalExclBrokeredRecip())
                    amount += getField(basicSurplusData, ["leftS2TotalDepAmountBrokeredRecip", "data"], true);
                if (thisType().leftS4BrokDepBalInclNationalCDs())
                    amount += getField(basicSurplusData, ["leftS2TotalDepAmountNationalCDs", "data"], true);
                thisType().leftS4BrokDepBalTitle(title);
                thisType().leftS4BrokDepBalAmount(amount);
            }

            if (!thisType().leftS4BrokDepTotalOverride()) {
                if (thisType().leftS4BrokDepType() == "None") {
                    thisType().leftS4BrokDepTotalAmount(0);
                }
                else {
                    //Loop through any custom ones to subtract as well
                    var amt = 0;
                    for (var i = 0; i < thisType().leftS4BrokDeps().length; i++) {
                        amt += thisType().leftS4BrokDeps()[i].amount();
                    }
                    amt = thisType().leftS4BrokDepCapAmount() - thisType().leftS4BrokDepBalAmount() - amt;
                    if (amt >= 0) {
                        thisType().leftS4BrokDepTotalAmount(amt);
                    } else {
                        thisType().leftS4BrokDepTotalAmount(0);
                    }
                    //thisType().leftS4BrokDepTotalAmount(thisType().leftS4BrokDepCapAmount() - thisType().leftS4BrokDepBalAmount() - amt);
                }
                thisType().leftS4BrokDepTotalTitle("Remaining Capacity to Utilize Brokered Deposits");

            }

            if (!thisType().leftS4SubHeaderOverride()) {
                //thisType().leftS4SubHeaderTitle("BASIC SURPLUS W/ FHLB & BROKERED DEPOSITS");
                thisType().leftS4SubHeaderTitle("Tier 3 Basic Surplus");
            }

            //Middle
            thisType().midS1Miscs().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "midS1MiscsTotalAsset":
                            row.title("Total Assets");
                            row.amount(getField(basicSurplusData, ["midS1MiscsTotalAsset", "data"], true));
                            break;
                    }
                }
            });
            //Note: midS1TotalDeposit logic in Left Section 2, formerly midS1MiscsTotalDeposit

            thisType().leftS1LiqAssetPercent(((thisType().midS1Miscs.midS1MiscsTotalAsset.amount() == 0) ? 0 : thisType().leftS1LiqAssetTotalAmount() / thisType().midS1Miscs.midS1MiscsTotalAsset.amount()) * 100.0);

            thisType().midS1BasicSurplusAmount(thisType().leftS1LiqAssetTotalAmount() - thisType().leftS2VolLiabTotalAmount());
            thisType().midS1BasicSurplusPercent(((thisType().midS1Miscs.midS1MiscsTotalAsset.amount() == 0) ? 0 : thisType().midS1BasicSurplusAmount() / thisType().midS1Miscs.midS1MiscsTotalAsset.amount()) * 100.0);
            thisType().midS1BasicSurplusFHLBAmount(thisType().midS1BasicSurplusAmount() + thisType().leftS3LoanTotalAmount());
            thisType().midS1BasicSurplusFHLBPercent(((thisType().midS1Miscs.midS1MiscsTotalAsset.amount() == 0) ? 0 : thisType().midS1BasicSurplusFHLBAmount() / thisType().midS1Miscs.midS1MiscsTotalAsset.amount()) * 100.0);
            thisType().midS1BasicSurplusBrokAmount(thisType().midS1BasicSurplusFHLBAmount() + thisType().leftS4BrokDepTotalAmount());
            thisType().midS1BasicSurplusBrokPercent(((thisType().midS1Miscs.midS1MiscsTotalAsset.amount() == 0) ? 0 : thisType().midS1BasicSurplusBrokAmount() / thisType().midS1Miscs.midS1MiscsTotalAsset.amount()) * 100.0);

            //Right
            //Section 1
            if (!thisType().rightS1HeaderOverride()) {
                thisType().rightS1HeaderTitle("Other Liquidity Items");
            }
            if (!thisType().rightS1SubHeaderOverride()) {
                thisType().rightS1SubHeaderTitle("Other Investments");
            }

            totals = [0, 0, 0];
            thisType().rightS1OtherLiqs().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "rightS1OtherLiqsCorpSec":
                            field = "corporateSecurities";
                            row.title("Corporates");
                            break;
                        case "rightS1OtherLiqsMuniSec":
                            field = "municipalSecurities";
                            row.title("Municipals");
                            break;
                        case "rightS1OtherLiqsEqSec":
                            field = "equitySecurities";
                            row.title("Equities");
                            break;
                        case "rightS1OtherLiqsOther":
                            field = "otherInvestments";
                            row.title("Other");
                            break;
                    }
                    row.marketValue(getField(thisType().basicSurplusMarketValues, ["marketValue", field]));
                    row.pledged(getField(thisType().basicSurplusMarketValues, ["amountPledged", field]));
                }
                row.available(row.marketValue() - row.pledged());
                totals[0] += row.marketValue();
                totals[1] += row.pledged();
                totals[2] += row.available();
            });

            if (!thisType().rightS1OtherLiqTotalOverride())
                thisType().rightS1OtherLiqTotalTitle("Total");
            thisType().rightS1OtherLiqTotalMarketValue(totals[0]);
            thisType().rightS1OtherLiqTotalPledged(totals[1]);
            thisType().rightS1OtherLiqTotalAvailable(totals[2]);

            //Section 2
            if (!thisType().rightS3HeaderOverride()) {
                thisType().rightS3HeaderTitle("Unsecured");
            }

            totals = [0, 0, 0];
            thisType().rightS2SecBors().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "rightS2SecBorsFedBIC":
                            row.title("Fed BIC Lines");
                            //row.line(0); //Note: Always entered
                            //row.outstanding(0);
                            break;
                        case "rightS2SecBorsOther":
                            row.title("Fed Discount / Other");
                            //row.line(0); //Note: Always entered
                            //row.outstanding(0);
                            break;
                    }
                }
                row.available(row.line() - row.outstanding());
                totals[0] += row.line();
                totals[1] += row.outstanding();
                totals[2] += row.available();
            });

            //Section 3
            if (!thisType().rightS2HeaderOverride()) {
                thisType().rightS2HeaderTitle("Borrowing Lines");
            }
            if (!thisType().rightS2SubHeaderOverride()) {
                thisType().rightS2SubHeaderTitle("Secured");
            }

            //totals = [0, 0, 0];
            thisType().rightS3UnsecBors().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "rightS3UnsecBorsFedFunds":
                            row.title("Fed Funds Lines");
                            //row.line(0); //Note: Always entered
                            row.outstanding(getField(basicSurplusData, ["rightS3UnsecBorsFedFunds", "data"], true));
                            break;
                    }
                }
                row.available(row.line() - row.outstanding());
                totals[0] += row.line();
                totals[1] += row.outstanding();
                totals[2] += row.available();
            });

            if (!thisType().rightS3UnsecBorTotalOverride())
                thisType().rightS3UnsecBorTotalTitle("Total");
            thisType().rightS3UnsecBorTotalLine(totals[0]);
            thisType().rightS3UnsecBorTotalOutstanding(totals[1]);
            thisType().rightS3UnsecBorTotalAvailable(totals[2]);



            if (!thisType().rightS2SecBorTotalOverride())
                thisType().rightS2SecBorTotalTitle("Total");
            thisType().rightS2SecBorTotalLine(totals[0]);
            thisType().rightS2SecBorTotalOutstanding(totals[1]);
            thisType().rightS2SecBorTotalAvailable(totals[2]);

            //Section 4
            if (thisType().rightS4HeaderOverride() == undefined || !thisType().rightS4HeaderOverride()) {
                //thisType().rightS4HeaderTitle("BASIC SURPLUS - EXCL. FED BIC & OTHER SECURED LINES");
                thisType().rightS4HeaderTitle("Unrealized Gain/Loss on Securities (AFS & HTM)");
            }
            if (!thisType().rightS4Column1Override()) {
                thisType().rightS4Column1Title("Total Portfolio");
            }
            if (!thisType().rightS4Column2Override()) {
                thisType().rightS4Column2Title("Tier 1 Basic Surplus Bonds");
            }
            if (!thisType().rightS4Column2Override()) {
                thisType().rightS4Column2Title("Tier 1 Basic Surplus Bonds");
            }
            thisType().rightS4Column3Title("% of Assets");

            var totAssets = thisType().midS1Miscs.midS1MiscsTotalAsset.amount();
            var base1, base2;
            var other1, other2;
            thisType().rightS4UnrealGL().forEach(function (row) {
                switch (row.fieldName()) {
                    case "rightS4BaseGL":
                        base1 = row.totalPortfolio();
                        base2 = row.tier1BasicSurplus();
                        if (totAssets == 0) {
                            row.percOfAssets(0);
                        } else {
                            row.percOfAssets(((Math.abs(row.tier1BasicSurplus()) / totAssets) * 100).toFixed(2));
                        }

                        break;
                    case "rightS4OtherScenGL":
                        other1 = row.totalPortfolio();
                        other2 = row.tier1BasicSurplus();
                        if (totAssets == 0) {
                            row.percOfAssets(0);
                        } else {
                            row.percOfAssets(((Math.abs(row.tier1BasicSurplus()) / totAssets) * 100).toFixed(2));
                        }
                        break;
                    case "rightS4Diff":
                        row.title("Difference");
                        //since forEach is technically unordered, set the values outside of this loop
                        //row.amount(thisType().midS1BasicSurplusBrokAmount() - amount);
                        //row.percent(((thisType().midS1Miscs.midS1MiscsTotalAsset.amount() == 0) ? 0 : thisType().midS1BasicSurplusBrokAmount() / thisType().midS1Miscs.midS1MiscsTotalAsset.amount()) * 100.0);
                        break;
                }
            });

            thisType().rightS4UnrealGL().forEach(function (row) {
                if (!row.override() && row.fieldName() == "rightS4Diff") {
                    row.totalPortfolio(other1 - base1);
                    row.tier1BasicSurplus(other2 - base2);
                }
                if (totAssets == 0) {
                    row.percOfAssets(0);
                } else {
                    row.percOfAssets(((Math.abs(row.tier1BasicSurplus()) / totAssets) * 100).toFixed(2));
                }
            });

            //Note: rightS4Footnote logic in footnotes section, formerly rightS6FootnotesLiquidity

            //Section 5
            if (!thisType().rightS5HeaderOverride()) {
                thisType().rightS5HeaderTitle("AVAILABLE BORROWING CAPACITY (PER POLICY)");
            }

            thisType().rightS5BorCaps().forEach(function (row) {
                if (!row.override()) {
                    switch (row.fieldName()) {
                        case "rightS5BorCapsMaxBorCap":
                            var policyLimit = !thisType().rightS5BorCapsPolicyLimit();
                            title = "Maximum Borrowing Capacity";
                            switch (row.type()) {
                                case "Assets":
                                    title += " (";
                                    if (policyLimit) {
                                        percent = getField(basicSurplusData, ["borAssetPolicyLimit", "data"], true);
                                        if (percent == -999)
                                            percent = 0;
                                        title += "Policy Limit of ";
                                        row.percent(percent); //Note: Always entered otherwise
                                    }
                                    else
                                        percent = row.percent();
                                    title += percent + "% of Assets)";
                                    row.amount(getField(basicSurplusData, ["midS1MiscsTotalAsset", "data"], true) * percent / 100.0);
                                    break;
                                case "Deposits":
                                    title += " (";
                                    if (policyLimit) {
                                        percent = getField(basicSurplusData, ["borDepositPolicyLimit", "data"], true);
                                        if (percent == -999)
                                            percent = 0;
                                        title += "Policy Limit of ";
                                        row.percent(percent); //Note: Always entered otherwise
                                    }
                                    else
                                        percent = row.percent();
                                    title += percent + "% of Deposits)";
                                    row.amount(thisType().midS1TotalDepositAmount() * percent / 100.0);
                                    break;
                                case "Dollar":
                                    row.percent(0);
                                    if (policyLimit) {
                                        amount = getField(basicSurplusData, ["borMaxPolicyLimit", "data"], true);
                                        if (amount == -999)
                                            amount = 0;
                                        title += " (per Policy)";
                                        row.amount(amount); //Note: Always entered otherwise
                                    }
                                    break;
                                case "None":
                                    row.percent(0);
                                    title += " Not Available";
                                    row.amount(0);
                                    break;
                            }
                            row.title(title);
                            break;
                        case "rightS5BorCapsCurOutBor":
                            row.title("Current Outstanding Borrowings (Excl. Sweeps/Retail Repos)");
                            row.amount(getField(basicSurplusData, ["rightS5BorCapsCurOutBorAll", "data"], true) - getField(basicSurplusData, ["rightS5BorCapsCurOutBorRetail", "data"], true));
                            break;
                    }
                }
            });

            thisType().rightS5AuthBorCapTitle((thisType().rightS5BorCaps.rightS5BorCapsMaxBorCap.type() == "None") ? "Maximum Borrowing Capacity Not Available" : "Available Policy Authorized Borrowing Capacity");
            //TODO maybe move this to Report View (like others hidden here)? Do other things need this?
            thisType().rightS5AuthBorCapAmount((thisType().rightS5BorCaps.rightS5BorCapsMaxBorCap.type() == "None") ? 0 : thisType().rightS5BorCaps.rightS5BorCapsMaxBorCap.amount() - thisType().rightS5BorCaps.rightS5BorCapsCurOutBor.amount());

            //Section 6
            if (!thisType().rightS6HeaderOverride()) {
                thisType().rightS6HeaderTitle("Notes");
            }

            if (!thisType().rightS6PubDepReqPledgingOverride()) {
                console.log(thisType().basicSurplusSecuredLiabilitiesTypes());
                console.log(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["municipalDeposits", "outstanding"], false, 0));
                thisType().rightS6PubDepReqPledgingTitle("Public Deposits Req. Pledging (in Millions)");
                thisType().rightS6PubDepReqPledgingCurrentQuarter(getField(thisType().basicSurplusSecuredLiabilitiesTypes, ["municipalDeposits", "outstanding"], false, 0) / 1000);
                thisType().rightS6PubDepReqPledgingPreviousQuarter(thisType().rightS6PubDepReqPledgingPreviousQuarter._initialValue); // ATO-605
            }

            thisType().rightS6Footnotes().forEach(function (row) {
                if (!row.override()) {
                    title = "";
                    switch (row.fieldName()) {
                        case "rightS6FootnotesReqPledging":
                            if (thisType().rightS6PubDepReqPledgingCurrentQuarter() > 0) {
                                title = "The total amount of public deposits requiring collateral as of " + profile().asOfDate + " is " + numeral(thisType().rightS6PubDepReqPledgingCurrentQuarter()).format('0,0') + " MM.";
                            }
                            else {
                                title = "There are no Municipal deposits requiring collateral.";
                            }
                               
                            break;
                    }
                    row.text(title);
                }

                isSaving(false);
            });
        }

        //Disposal
        var deactivate = function (isClose) {
            var self = this;
            subs.forEach(function (sub) {
                //TODO HACK subs should only be pushed ko.subscriptions, but sometimes numbers end up there!?
                if (sub.dispose)
                    sub.dispose();
            });
            subs = [];
        };

        var datacontext = {
            deactivate: deactivate,
            doRelativelyInsaneAmountOfWork: doRelativelyInsaneAmountOfWork,
            doLotsOfWork: doLotsOfWork,
            doSomeWork: doSomeWork,
            findCollateralValue: findCollateralValue,
            sumCollateralField: sumCollateralField,

            sumRow: sumRow,
            assignDataPullField: assignDataPullField,
            getField: getField,
            subAll: subAll,
            sub: sub,
            getBasicSurplusData: getBasicSurplusData,
            updateAsOfDateOffsets: updateAsOfDateOffsets,
            compositionComplete: compositionComplete,
            activate: activate,
            defaultMarketValues: defaultMarketValues,
            setMarketValue: setMarketValue,
            calcUnrealizedGainLoss: calcUnrealizedGainLoss,
            calcUnrealized: calcUnrealized,
            defaultSimulation: defaultSimulation,
            removeType: removeType,
            addRightS6Footnotes: addRightS6Footnotes,
            addRightS5BorCaps: addRightS5BorCaps,
            addRightS4UnrealGL: addRightS4UnrealGL,
            addRightS2SecBors: addRightS2SecBors,
            addRightS3UnsecBors: addRightS3UnsecBors,
            addRightS1OtherLiqs: addRightS1OtherLiqs,
            addMidS1Miscs: addMidS1Miscs,
            addLeftS4BrokDeps: addLeftS4BrokDeps,
            addLeftS3Loans: addLeftS3Loans,
            addLeftS3LoanCols: addLeftS3LoanCols,
            addLeftS2VolLiabs: addLeftS2VolLiabs,
            addLeftS1LiqAssets: addLeftS1LiqAssets,
            addLeftS1SecCols: addLeftS1SecCols,
            addLeftS1OvntFunds: addLeftS1OvntFunds,
            addCollateralType: addCollateralType,
            addMarketValues: addMarketValues,
            removeSecuredLiabilitiesType: removeSecuredLiabilitiesType,
            dontAddSecuredLiabilitiesType: dontAddSecuredLiabilitiesType,
            sortableAfterMove: sortableAfterMove,
            addSecuredLiabilitiesType: addSecuredLiabilitiesType,
            sortTypes: sortTypes,
            deleteType: deleteType,
            save: save,
            cancel: cancel,
            goToTab: goToTab,
            goBack: goBack,
            goToDetail: goToDetail,
            hasChanges: hasChanges,
            canSave: canSave,
            canDeactivate: canDeactivate,

            //Variables
            policyOffsets: policyOffsets,
            marketValues: marketValues,
            scenLookUp: scenLookUp,
            connectClass: connectClass,
            institutions: institutions,
            global: global,
            simulationTypes: simulationTypes,
            scenarioTypes: scenarioTypes,
            shockScenarioTypeNames: shockScenarioTypeNames,
            basicSurplusAdmins: basicSurplusAdmins,
            securedLiabilitiesTypes: securedLiabilitiesTypes,
            allTypes: allTypes,
            isSaving: isSaving,
            profile: profile,
            thisType: thisType,
            borTypes: borTypes,
            borCapsTypes: borCapsTypes,

            sortedCollateralTypes: sortedCollateralTypes,
            sortedMarketValues:  sortedMarketValues,
            sortedTypes:  sortedTypes,
            sortedLeftS1OvntFunds:  sortedLeftS1OvntFunds,
            sortedLeftS1SecCols:  sortedLeftS1SecCols,
            sortedLeftS1LiqAssets:  sortedLeftS1LiqAssets,
            sortedLeftS2VolLiabs:  sortedLeftS2VolLiabs,
            sortedLeftS3LoanCols: sortedLeftS3LoanCols,
            sortedLeftS3Loans: sortedLeftS3Loans,
            sortedLeftS4BrokDeps:  sortedLeftS4BrokDeps,
            sortedMidS1Miscs:  sortedMidS1Miscs,
            sortedRightS1OtherLiqs:  sortedRightS1OtherLiqs,
            sortedRightS3UnsecBors:  sortedRightS3UnsecBors,
            sortedRightS2SecBors: sortedRightS2SecBors,
            sortedRightS4UnrealGL:  sortedRightS4UnrealGL,
            sortedRightS5BorCaps:  sortedRightS5BorCaps,
            sortedRightS6Footnotes:  sortedRightS6Footnotes

        };

        return datacontext;
    });