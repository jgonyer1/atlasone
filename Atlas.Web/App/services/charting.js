﻿define(function (require) {

    // color palette 1 as defined in https://dcgsoftware.atlassian.net/wiki/spaces/AT/pages/76775464/Atlas+One+Style+Guidelines
    var chartColorsPalette1 = [
        'rgb(168,205,242)',
        'rgb(255,127,  0)',
        'rgb(  0, 32, 96)',
        'rgb( 89, 89, 89)',
        'rgb(112, 48,160)',
        'rgb(202,164,105)',
        'rgb(170,102,102)',
        'rgb( 35, 81, 18)',
        'rgb(  0,128,255)'
    ];

    // color palette 2 as defined in https://dcgsoftware.atlassian.net/wiki/spaces/AT/pages/76775464/Atlas+One+Style+Guidelines
    var chartColorsPalette2 = [
        'rgb(115,175,173)',
        'rgb( 64, 99,122)',
        'rgb(217,217,  0)',
        'rgb(139,101, 91)',
        'rgb(166,140, 38)',
        'rgb(  0,  0,  0)',
        'rgb(  0, 128, 255)',
        'rgb( 30,123, 61)',
        'rgb(255, 51,  1)',
        'rgb(224,178,182)'
    ];

    // Expanded - Combines background and accompanying text colors for color palette 2 to prevent contrast issues 
    var chartColorsTextPalette2= [
        '#ffffff',
        '#ffffff',
        '#000000',
        '#ffffff',
        '#ffffff',
        '#ffffff',
        '#ffffff',
        '#ffffff',
        '#ffffff',
        '#ffffff',
    ];

    var negativeValueRed = 'rgb(255,0,0)';

    var createEveChartOptions = function (title, categories, seriesData) {
        return {
            credits: { enabled: false },
            title: {
                text: title,
                style:
                {
                    fontSize: "12px"
                }
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: categories,
                labels:
                {
                    align: "left",
               
                }
            },
            yAxis: {
                title: {
                    text: ""
                }
            },
            plotOptions: {
                column: {
                    pointPadding: -.15,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        color: '#444646',
                        formatter: function () {
                            return '<span style="font-size:12;font-weight:bold;">' + Math.floor(this.y / 1000000) + 'M</span>';
                        }
                    }
                },
                series: {
                    animation: false
                }
            },
            series: [
                {
                    name: title,
                    type: "column",
                    color: '#428BCA',
                    data: seriesData
                }
            ]

        }

    };

    var createEvePolicyChartOptions = function (yAxisFormatter, categories, series, min, max, formatter, eveType, toolTipFormatter) {
        return {
            credits: { enabled: false },
            title: {
                text: ''
            },
            legend: {
                enabled: true,
                symbolRadius: 0,
                padding: 0,
                margin: 0
            },
            xAxis: {
                categories: categories,
                minorTickLength: 0,
                tickLength: 0
            },
            yAxis: {
                title: {
                    text: ""
                },
                labels: {
                    formatter: yAxisFormatter
                }
            },
            tooltip: {
                formatter: toolTipFormatter
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        useHTML: true,
                        formatter: formatter,
                        crop: false,
                        overflow: 'none'
                    }
                },
                series: {
                    animation: false
                }

            },
            series: series

        };


    };

    var createBalanceSheetMixPie = function (asOfDate, seriesData, colors) {
        return {
            chart: {
                type: 'pie',
                animation: false
            },
            colors: chartColorsPalette2,
            credits: { enabled: false },
            height: 300,
            title: {
                text: moment(asOfDate).format('l'),
                align: 'center',
                verticalAlign: 'bottom',
                margin: '3',
                style:
                {
                    fontSize: "10px",
                    fontWeight: "bold"
                }
            },
            plotOptions: {
                series: {
                    animation: false,

                }
            },
            legend: {
                enabled: false,
            },
            plotOptions: {
                pie: {
                    size: 230,
                    animation: false,
                    dataLabels: {
                        enabled: false,
                        distance: 5,
                        style: {
                            width: '10px',
                            fontWeight: 'bold'
                        }
                    }
                }
            },
            tooltip: {
                valueDecimals: 2,
                valueSuffix: '%'
            },
            series: [
                {
                    name: "Assets",
                    type: "pie",
                    animation: false,
                    data: seriesData
                }
            ]
        };

    };


    var cashflowScaling = function (max, min) {
        var maxDigits, scaleBase, calcMax, scaleMax, scaleMin, scaleUnit;
        var retObj = {};

        if (min >= 0) {
            maxDigits = max.toString().substr(0, max.toString().indexOf('.')).length;
            scaleBase = 4 * Math.pow(10, maxDigits - 2);
            calcMax = Math.ceil(max / scaleBase) * scaleBase;
            scaleMax = calcMax;
            scaleMin = 0;


            if (max + (scaleBase / 8) > calcMax) {
                scaleMax = calcMax + scaleBase;
            }
            scaleUnit = scaleMax / 4;

            retObj = {
                scaleMax: scaleMax,
                scaleMin: scaleMin,
                scaleUnit: scaleUnit
            };
        } else {
            maxDigits = Math.abs(max).toString().substr(0, Math.abs(max).toString().indexOf('.')).length;
            minDigits = Math.abs(min).toString().substr(0, Math.abs(min).toString().indexOf('.')).length;

            scaleBase = 4 * Math.pow(10, maxDigits - 3);
            var minScaleBase = 4 * Math.pow(10, minDigits - 3);

            calcMax = Math.ceil(max / scaleBase) * scaleBase;
            var calcMin = (Math.ceil(Math.abs(min / minScaleBase)) * -1) * minScaleBase;
            var minMaxDiff = (calcMax - calcMin) / 4;
            var minMaxDig = minMaxDiff.toString().substr(0, minMaxDiff.toString().indexOf('.')).length;
            if (minMaxDiff.toString().indexOf('.') == -1) {
                minMaxDig = minMaxDiff.toString().length;
            }

            var minMaxScaleBase = 4 * Math.pow(10, minMaxDig - 3);
            scaleUnit = Math.ceil(minMaxDiff / minMaxScaleBase) * minMaxScaleBase;
            scaleMin = (Math.ceil(Math.abs(calcMin / scaleUnit)) * -1) * scaleUnit;
            scaleMax = scaleMin + (scaleUnit * (Math.ceil(Math.abs(calcMin / scaleUnit)) + 3));

            retObj = {
                scaleMax: scaleMax,
                scaleMin: scaleMin,
                scaleUnit: scaleUnit
            };
        }

        return retObj;
    };

    //scaling for Capital Analysius 
    var capanalysisScaling = function (max, min) {
        var maxDigits, scaleBase, calcMax, scaleMax, scaleMin, scaleUnit;
        var retObj = {};

        if (min >= 0) {
            maxDigits = max.toString().substr(0, max.toString().indexOf('.')).length;
            scaleBase = 4 * Math.pow(10, maxDigits - 2);
            calcMax = Math.ceil(max / scaleBase) * scaleBase;
            scaleMax = calcMax;
            scaleMin = 0;


            if (max + (scaleBase / 8) > calcMax) {
                scaleMax = calcMax + scaleBase;
            }
            scaleUnit = scaleMax / 4;

            retObj = {
                scaleMax: scaleMax,
                scaleMin: scaleMin,
                scaleUnit: scaleUnit
            };
        } else {
            maxDigits = Math.abs(max).toString().substr(0, Math.abs(max).toString().indexOf('.')).length;
            minDigits = Math.abs(min).toString().substr(0, Math.abs(min).toString().indexOf('.')).length;

            scaleBase = 4 * Math.pow(10, maxDigits - 3);
            var minScaleBase = 4 * Math.pow(10, minDigits - 3);

            calcMax = Math.ceil(max / scaleBase) * scaleBase;
            var calcMin = (Math.ceil(Math.abs(min / minScaleBase)) * -1) * minScaleBase;
            var minMaxDiff = (calcMax - calcMin) / 4;
            var minMaxDig = minMaxDiff.toString().substr(0, minMaxDiff.toString().indexOf('.')).length;
            if (minMaxDiff.toString().indexOf('.') == -1) {
                minMaxDig = minMaxDiff.toString().length;
            }

            var minMaxScaleBase = 4 * Math.pow(10, minMaxDig - 3);
            scaleUnit = Math.ceil(minMaxDiff / minMaxScaleBase) * minMaxScaleBase;
            scaleMin = (Math.ceil(Math.abs(calcMin / scaleUnit)) * -1) * scaleUnit;
            scaleMax = scaleMin + (scaleUnit * (Math.ceil(Math.abs(calcMin / scaleUnit)) + 3));

            retObj = {
                scaleMax: scaleMax,
                scaleMin: scaleMin,
                scaleUnit: scaleUnit
            };
        }

        return retObj;
    };


    var netCashflowSummaryChart = function (name, cats, series, scenCount, min, max, scaleUnit) {
        return cashflowSummaryChart(name, cats, series, scenCount, min, max, scaleUnit, false)
    };

    var cashflowSummaryChart = function (name, cats, series, scenCount, min, max, scaleUnit, showLegend) {
        var legend = showLegend == undefined ? true : false;
        return {
            chart: {
                type: 'column'
            },
            title: {
                useHTML: true,
                text: "<span class='cashflow-title'>" + name + "</span>"
            },
            legend: {
                enabled: legend,
                borderWidth: 0,
                useHTML: true,
                labelFormatter: function () {
                    return "<span style='font-weight: bold; color: rgb(96,96,96)'>" + this.name + "</span>";
                }
            },
            xAxis: {
                categories: cats,
                labels: {
                    style: { 'fontWeight': 'bold' }
                },
                plotBands: [{
                    color: '#e9e9e9',
                    label: {
                        text: "YEAR 1",
                        style: {
                            color: '#777777'
                        },
                    },
                    from: -1,
                    to: scenCount - 0.5
                }, {
                    color: '#ffffff',
                    label: {
                        text: "YEAR 2",
                        style: {
                            color: '#777777'
                        },
                    },
                    from: scenCount - 0.5,
                    to: (scenCount * 2) - 0.5
                }, {
                    color: '#e9e9e9',
                    label: {
                        text: "YEAR 3",
                        style: {
                            color: '#777777'
                        },
                    },
                    from: (scenCount * 2) - 0.5,
                    to: (scenCount * 3) - 0.5
                },

                {
                    color: '#ffffff',
                    label: {
                        text: "YEAR 4",
                        style: {
                            color: '#777777'
                        },
                    },
                    from: (scenCount * 3) - 0.5,
                    to: (scenCount * 4) - 0.5
                }, {
                    color: '#e9e9e9',
                    label: {
                        text: "YEAR 5",
                        style: {
                            color: '#777777'
                        },
                    },
                    from: (scenCount * 4) - 0.5,
                    to: (scenCount * 5) - 0.5
                },
                {
                    color: '#ffffff',
                    label: {
                        text: ">YEAR 5",
                        style: {
                            color: '#777777'
                        },
                    },
                    from: (scenCount * 5) - 0.5,
                    to: (scenCount * 6)
                }],
            },
            yAxis: {
                title: '',
                min: min,
                max: max,
                tickInterval: scaleUnit,
                labels: {
                    useHTML: true,
                    formatter: function () {
                        var ret = "<span class='y-axis-label'>" + numeral(this.value / 1000).format('(0,0)') + "</span>";
                        if (this.value < 0) {
                            ret = "<span class='neg-val y-axis-label'>" + numeral(this.value / 1000).format('(0,0)') + "</span>";
                        }
                        return ret
                    }
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                },
                series: {
                    animation: false
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + ':</b> ' + numeral(this.y / 1000).format('0,0');
                }
            },
            series: series
        };

    };

    var cashflowChart = function (name, cats, series, min, max, scaleUnit) {
        return {
            chart: {
                type: 'column',
                animation: false
            },
            title: {
                useHTML: true,
                text: "<span class='cashflow-title'>" + name + "</span>"
            },
            legend: {
                borderWidth: 0,
                useHTML: true,
                labelFormatter: function () {
                    return "<span style='font-weight: bold; color: rgb(96,96,96)'>" + this.name + "</span>";
                }
            },
            xAxis: {
                categories: cats,
                labels: {
                    style: { 'fontWeight': 'bold' }
                }
            },
            yAxis: {
                title: '',
                min: min,
                max: max,
                tickInterval: scaleUnit,
                labels: {
                    useHTML: true,
                    formatter: function () {
                        var ret = "<span class='y-axis-label'>" + numeral(this.value / 1000).format('(0,0)') + "</span>";
                        if (this.value < 0) {
                            ret = "<span class='neg-val y-axis-label'>" + numeral(this.value / 1000).format('(0,0)') + "</span>";
                        }
                        return ret
                    }
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    animation: false
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.point.thisLabel + '</b> ' + numeral(this.point.thisVal / 1000).format('0,0');
                }
            },
            series: series
        }

    }

    //headerIdPair is an array with two id's in it. One is the id of the place holder center column header to be padded out. The second is the actual header that we should grab the height from
    //this whole thing only works because the axes of the reports have been calculated and forced to be the same
    //DO NOT INCLUDE HASH SYMBOL WITH THE IDS PASSED IN
    var moveSharedAxis = function (headerIdPair, middleSVGId, middleColId) {
        if ($("#" + headerIdPair[0]) && $("#" + headerIdPair[1])) {
            $("#" + headerIdPair[0]).css({ 'margin': "0" }).outerHeight($("#" + headerIdPair[1]).outerHeight(true));
        }
        var hcAxes = document.getElementsByClassName("highcharts-axis-labels highcharts-yaxis-labels");
        if (hcAxes.length > 0) {
            var axis = hcAxes[0].cloneNode(true);

            var svg = document.getElementById(middleSVGId);
            $(svg).append($(axis));
            var parWidth = $("#" + middleColId).width();
            document.getElementById(middleSVGId).style.width = parWidth;
            for (var t = 0; t < axis.childNodes.length; t++) {
                var textNode = axis.childNodes[t];
                var textNodeOffsetCoord = $(textNode).offset();
                var tWidth = textNode.getBBox().width;
                textNode.x.baseVal[0].value = (parWidth / 2) + (tWidth / 2);
            }
        }
    };

    //DO NOT INCLUDE HASH SYMBOL IN IDS
    var redrawSharedAxisChart = function (chartContainerId, chartAxisId) {
        $('#' + chartContainerId).highcharts().update({
            id: chartAxisId,
            yAxis: {
                labels: {
                    enabled: false
                }
            }
        }, true);
    };


    var sharedAxisChart = function (name, isQuarterly, series, units, yAxisId) {

        //we need to do this override because cannot figure out what to do with min, max and an interval
        var tickPositions = [];
        var temp = units.min;
        while (temp <= units.max) {
            tickPositions.push(temp);
            temp += units.inc;
        }
        return {
            chart: {
                animation: false,
                plotBorderWidth: 1,
                spacingLeft: 1,
                spacingRight: 1,
                //marginLeft: -45
            },
            title: {
                text: ''
            },
            credits: { enabled: false },
            plotOptions: {
                series: {
                    animation: false,
                    events: {
                        legendItemClick: function () {
                            return false;
                        }
                    }
                },
                line: {
                    marker: {
                        height: 4,
                        width: 4,
                        radius: 4,
                        lineWidth: 1
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    var s = ''
                    for (var i = 0; i < this.points.length; i++) {
                        var point = this.points[i];
                        if (i == 0) {
                            s += moment(point.point.date).format('MMM-YY');
                        }

                        var val = numeral(point.y / 1000).format('0,0');
                        //Build Tooltip string
                        s += '<br />';
                        s += '<span style="color:' + point.point.series.color + ';font-weight: bold;">' + point.series.name + ': </span>';
                        s += '<span>' + val + '</span>';

                    }

                    return s;
                },
                shared: true
            },
            xAxis: isQuarterly ? quarterlyXaxis() : MonthlyXaxis(),
            yAxis:
            {
                id: yAxisId,
                labels: {
                    formatter: function () {
                        return numeral(this.value / 1000).format("0,0");
                    }
                },
                title: { text: "" },
                // min: min,
                // max: max,
                gridLineWidth: 1,
                zIndex: 1,
                tickLength: 0,
                tickWidth: 1,
                tickPosition: 'outside',
                max: units.max,
                min: units.min,
                tickInterval: units.inc,
                tickPositions: tickPositions,
                visible: true
                //tickAmount: (Math.abs(units.min) + units.max) / units.inc
                //tickAmount: 6
            },
            series: series
        };

    }

    var simulationCompareChart = function (name, isQuarterly, series, units, yAxisId) {

        //we need to do this override because cannot figure out what to do with min, max and an interval
        var tickPositions = [];
        var temp = units.min;
        while (temp <= units.max) {
            tickPositions.push(temp);
            temp += units.inc;
        }
        return {
            chart: {
                animation: false,
                plotBorderWidth: 1,
            },
            title: {
                text: ''
            },
            credits: { enabled: false },
            plotOptions: {
                series: {
                    animation: false,
                    events: {
                        legendItemClick: function () {
                            return false;
                        }
                    }
                },
                line: {
                    marker: {
                        height: 4,
                        width: 4,
                        radius: 4,
                        lineWidth: 1
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    var s = '' 
                    for (var i = 0; i < this.points.length; i++) {
                        var point = this.points[i];
                        if (i == 0) {
                            s += moment(point.point.date).format('MMM-YY');
                        }
                     
                        var val = numeral(point.y / 1000).format('0,0');
                        //Build Tooltip string
                        s += '<br />';
                        s += '<span style="color:' + point.point.series.color + ';font-weight: bold;">' + point.series.name + ': </span>';
                        s += '<span>' + val + '</span>';
                       
                    }

                    return s;
                },
                shared: true
            },
            xAxis: isQuarterly ? quarterlyXaxis() : MonthlyXaxis(),
            yAxis:
            {
                id: yAxisId,
                labels: {
                    formatter: function () {
                        return numeral(this.value / 1000).format("0,0");
                    }
                },
                title: { text: "" },
                // min: min,
                // max: max,
                gridLineWidth: 1,
                zIndex: 1,
                tickLength: 0,
                tickWidth: 1,
                tickPosition: 'outside',
                max: units.max,
                min: units.min,
                tickInterval: units.inc,
                tickPositions: tickPositions,
                visible: true
                //tickAmount: (Math.abs(units.min) + units.max) / units.inc
                //tickAmount: 6
            },
            series: series
        };

    }

    var interestRatePolicyGuidelinesChart = function (title, categories, series) {
        return {
            title: {
                text: title
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            chart: {
                //TODO color taken from template, any way to reference template's value directly?
                borderColor: '#ddd',
                borderWidth: 1
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    enableMouseTracking: false
                },
                series: {
                    animation: false
                }
            },
            series: series
        }
    }

    var scenarioSeriesColor = function (scenario) {
        var color = "";
        switch (scenario) {
            case "Down 300BP":
            case "Down 200BP":
            case "Down 100BP":
            case "Down 400BP":
            case "Assets":
                color = "#B71213";
                break;
            case "Up 100BP":
            case "Up 200BP":
            case "Up 300BP":
            case "Up 400BP":
            case "Liabilities":
                color = "#81BC00";
                break;
            case "Up 400BP 24M":
            case "Up 100BP 24M":
            case "Up 200BP 24M":
            case "Up 300BP 24M":
            case "Net":
                color = "#282634";
                break;
            case "Flat Up 200BP":
            case "Flat Up 400BP":
            case "Flat Up 500BP":
            case "Delay Flat Up 500BP":
            case "Flat Up 100BP":
            case "Flat Up 300BP":
                color = "#611427";
                break;
            case "Steep Down 100BP":
            case "Steep Down 200BP":
            case "Steep Down 300BP":
            case "Steep Down 400BP":
                color = "#EC2E48";
                break;
            case "Steep Up 200BP":
            case "Steep Up 100BP":
            case "Steep Up 300BP":
            case "Steep Up 400BP":
                color = "#8A704C";
                break;
            case "Shock Down 300BP":
            case "Shock Down 200BP":
            case "Shock Down 100BP":
            case "Shock Down 400BP":
                color = "#fa5b0f";
                break;
            case "Shock Up 100BP":
            case "Shock Up 200BP":
            case "Shock Up 300BP":
            case "Shock Up 400BP":
                color = "#464646";
                break;
            case "-400BP":
            case "-300BP":
            case "-200BP":
            case "-150BP":
            case "-100BP":
            case "-50BP":
            case "0 Shock":
            case "+50BP":
            case "+100BP":
            case "+150BP":
            case "+200BP":
            case "+300BP":
            case "+400BP":
                color = "#262626";
                break;
            case "Yield Curve Twist":
                color = "#553285";
                break;
            default:
                color = "#0069AA";
                break;
        }

        return color;
    }

    var scenarioProperName = function (scenario) {
        return scenario.charAt(0).toUpperCase() + scenario.slice(1);
    }

    var scenarioIconStagger = function (series) {


        var trueCounter = 0;
        for (var z = 0; z < series.length; z++)
        {

            if (series[z].type == undefined || series[z].type.toUpperCase() == "LINE") {
                var modPos = 0;
                if (trueCounter <= 3) {
                    modPos = trueCounter;
                }
                else {
                    modPos = trueCounter % 3;
                }


                for (var x = 0; x < series[z].data.length; x++) {
                    var showMarker = false;
                    if (x <= 3) {
                        if (x == modPos) {
                            showMarker = true;
                        }
                    }
                    else {
                        var rem = x % 4;
                        if (rem == modPos) {
                            showMarker = true;
                        }
                    }

                    if (showMarker) {

                        series[z].data[x].marker = this.scenarioIdLookUp(series[z].name).marker;
                    }
                    else {
                        series[z].data[x].marker = { symbol: 'circle' };
                    }

                }
                trueCounter += 1;
            }

        }


        return series;
    }

    var scenarioIdLookUp = function (scenario) {
        scenario = scenarioProperName(scenario);
        var scens = {};
        scens['Base Scenario'] = { scenarioId: 1, marker: { symbol: "url(Content/images/markers/16/base-16.png)" }, seriesColor: "#A8CDF2", labelClass: "baseButton", rowClass: "baseHighlight" };
        scens['Base'] = { scenarioId: 1, marker: { symbol: "url(Content/images/markers/16/base-16.png)" }, seriesColor: "#A8CDF2", labelClass: "baseButton", arrowColor: "#a8cdf2", rowClass: "baseHighlight" };
        scens['Down 300BP'] = { scenarioId: 2, marker: { symbol: "url(Content/images/markers/16/down-3-navy-circle-16.png)" }, seriesColor: "#002060", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "downButton" };
        scens['Down 200BP'] = { scenarioId: 3, marker: { symbol: "url(Content/images/markers/16/down-2-navy-circle-16.png)" }, seriesColor: "#002060", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "downButton" };
        scens['Down 100BP'] = { scenarioId: 4, marker: { symbol: "url(Content/images/markers/16/down-1-navy-circle-16.png)" }, seriesColor: "#002060", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "downButton" };
        scens['Up 100BP'] = { scenarioId: 5, marker: { symbol: "url(Content/images/markers/16/up-1-orange-circle-16.png)" }, seriesColor: "#FF7F00", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "upButton" };
        scens['Up 200BP'] = { scenarioId: 6, marker: { symbol: "url(Content/images/markers/16/up-2-orange-circle-16.png)" }, seriesColor: "#FF7F00", arrowColor: "#ffffff", arrowDirection: "right",  labelClass: "upButton" };
        scens['Up 300BP'] = { scenarioId: 7, marker: { symbol: "url(Content/images/markers/16/up-3-orange-circle-16.png)" }, seriesColor: "#FF7F00", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "upButton" };
        scens['Up 400BP'] = { scenarioId: 8, marker: { symbol: "url(Content/images/markers/16/up-4-orange-circle-16.png)" }, seriesColor: "#FF7F00", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "upButton" };
        scens['Up 400BP (24 Months)'] = { scenarioId: 9, marker: { symbol: "url(Content/images/markers/16/up200bp24m-4-slate-diamond-16.png)" }, seriesColor: "#7030A0", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "up40024mButton" };
        scens['Flat Up 200BP'] = { scenarioId: 10, marker: { symbol: "url(Content/images/markers/16/flat-up200bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Flat Up 400BP'] = { scenarioId: 11, marker: { symbol: "url(Content/images/markers/16/flat-up400bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Flat Up 500BP'] = { scenarioId: 12, marker: { symbol: "url(Content/images/markers/16/flat-up500bp-24m-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Delay Flat Up 500BP'] = { scenarioId: 13, marker: { symbol: "url(Content/images/markers/16/delay-flat-up500bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Steep Down 100BP'] = { scenarioId: 14, marker: { symbol: "url(Content/images/markers/16/steep-down-100-tan-square-16.png)" }, seriesColor: "#CAA469", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "steepDownButton" };
        scens['Steep Down 200BP'] = { scenarioId: 15, marker: { symbol: "url(Content/images/markers/16/steep-down-200-tan-square-16.png)" }, seriesColor: "#CAA469", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "steepDownButton" };
        scens['Steepening Down 200BP'] = { scenarioId: 15, marker: { symbol: "url(Content/images/markers/16/steep-down-200-tan-square-16.png)" }, seriesColor: "#CAA469", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "steepDownButton" };
        scens['Steep Up 200BP'] = { scenarioId: 16, marker: { symbol: "url(Content/images/markers/16/steep-up200bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Shock Down 300BP'] = { scenarioId: 17, marker: { symbol: "url(Content/images/markers/16/shock-down-3-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['Shock Down 200BP'] = { scenarioId: 18, marker: { symbol: "url(Content/images/markers/16/shock-down-2-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['Shock Down 100BP'] = { scenarioId: 19, marker: { symbol: "url(Content/images/markers/16/shock-down-1-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['Shock Up 100BP'] = { scenarioId: 20, marker: { symbol: "url(Content/images/markers/16/shock-up-1-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['Shock Up 200BP'] = { scenarioId: 21, marker: { symbol: "url(Content/images/markers/16/shock-up-2-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['Shock Up 300BP'] = { scenarioId: 22, marker: { symbol: "url(Content/images/markers/16/shock-up-3-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['Shock Up 400BP'] = { scenarioId: 23, marker: { symbol: "url(Content/images/markers/16/shock-up-4-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['-400BP'] = { scenarioId: 24, marker: { symbol: "url(Content/images/markers/16/shock-down-4-black-triangle.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['-300BP'] = { scenarioId: 25, marker: { symbol: "url(Content/images/markers/16/shock-down-3-black-triangle.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['-200BP'] = { scenarioId: 26, marker: { symbol: "url(Content/images/markers/16/shock-down-2-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['-150BP'] = { scenarioId: 27, marker: { symbol: "url(Content/images/markers/16/shock-down-1-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['-100BP'] = { scenarioId: 28, marker: { symbol: "url(Content/images/markers/16/shock-down-1-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['-50BP'] = { scenarioId: 29, marker: { symbol: "url(Content/images/markers/16/shock-down-1-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "shockDownButton" };
        scens['0 Shock'] = { scenarioId: 30, marker: { symbol: "url(Content/images/markers/16/0-shock-black-square-16.png)" }, seriesColor: "#595959", arrowColor: "#000000", arrowDirection: "left", labelClass: "shockUpButton" };
        scens['+50BP'] = { scenarioId: 31, marker: { symbol: "url(Content/images/markers/16/shock-up-1-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#000000", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['+100BP'] = { scenarioId: 32, marker: { symbol: "url(Content/images/markers/16/shock-up-1-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['+150BP'] = { scenarioId: 33, marker: { symbol: "url(Content/images/markers/16/shock-up-1-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['+200BP'] = { scenarioId: 34, marker: { symbol: "url(Content/images/markers/16/shock-up-2-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right",labelClass: "shockUpButton" };
        scens['+300BP'] = { scenarioId: 35, marker: { symbol: "url(Content/images/markers/16/shock-up-3-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['+400BP'] = { scenarioId: 36, marker: { symbol: "url(Content/images/markers/16/shock-up-4-black-triangle-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockUpButton" };
        scens['Yield Curve Twist'] = { scenarioId: 37, marker: { symbol: "url(Content/images/markers/16/yield-curve-twist-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Down 400BP'] = { scenarioId: 38, marker: { symbol: "url(Content/images/markers/16/down-4-navy-circle-16.png)" }, seriesColor: "#002060", arrowColor: "#002060", arrowDirection: "left", labelClass: "downButton" };
        scens['Up 100BP 24M'] = { scenarioId: 39, marker: { symbol: "url(Content/images/markers/16/up100bp24m-1-slate-diamond-16.png)" }, seriesColor: "#7030A0", arrowColor: "#ffffff", arrowDirection: "right",  labelClass: "up40024mButton" };
        scens['Up 200BP 24M'] = { scenarioId: 40, marker: { symbol: "url(Content/images/markers/16/up200bp24m-2-slate-diamond-16.png)" }, seriesColor: "#7030A0", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "up40024mButton" };
        scens['Up 300BP 24M'] = { scenarioId: 41, marker: { symbol: "url(Content/images/markers/16/up300bp24m-3-slate-diamond-16.png)" }, seriesColor: "#7030A0", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "up40024mButton" };
        scens['Up 400BP 24M'] = { scenarioId: 41, marker: { symbol: "url(Content/images/markers/16/up200bp24m-4-slate-diamond-16.png)" }, seriesColor: "#7030A0", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "up40024mButton" };
        scens['Flat Up 100BP'] = { scenarioId: 42, marker: { symbol: "url(Content/images/markers/16/flat-up100bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Flat Up 300BP'] = { scenarioId: 43, marker: { symbol: "url(Content/images/markers/16/flat-up300bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Steep Down 300BP'] = { scenarioId: 44, marker: { symbol: "url(Content/images/markers/16/steep-down-300-tan-square-16.png)" }, seriesColor: "#CAA469", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "steepDownButton" };
        scens['Steep Down 400BP'] = { scenarioId: 45, marker: { symbol: "url(Content/images/markers/16/steep-down-400-tan-square-16.png)" }, seriesColor: "#CAA469", arrowColor: "#ffffff", arrowDirection: "left", labelClass: "steepDownButton" };
        scens['Steep Up 100BP'] = { scenarioId: 46, marker: { symbol: "url(Content/images/markers/16/steep-up100bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Steep Up 300BP'] = { scenarioId: 47, marker: { symbol: "url(Content/images/markers/16/steep-up300bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Steep Up 400BP'] = { scenarioId: 48, marker: { symbol: "url(Content/images/markers/16/steep-up400bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Shock Down 400BP'] = { scenarioId: 49, marker: { symbol: "url(Content/images/markers/16/shock-down-4-black-triangle-16.png)" }, seriesColor: "#000000", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "shockDownButton" };
        scens['Up 200BP Flat'] = { scenarioId: 50, marker: { symbol: "url(Content/images/markers/16/flat-up200bp-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Up 400BP Flat (24 Months)'] = { scenarioId: 51, marker: { symbol: "url(Content/images/markers/16/flat-up400bp24m-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Up 500BP Flat (24 Months - Delayed)'] = { scenarioId: 52, marker: { symbol: "url(Content/images/markers/16/delayed-flat-up500bp24m-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        scens['Up 500BP Flat (24 Months)'] = { scenarioId: 53, marker: { symbol: "url(Content/images/markers/16/flat-up500bp24m-gray-square-16.png)" }, seriesColor: "#595959", arrowColor: "#ffffff", arrowDirection: "right", labelClass: "flatupButton" };
        return scens[scenario];
        };

    var createHighChartOptions = function (name, isQuarterly, chartViewData, highChartOptionsObservable) {

        var min = calculateYAxisMinValue(chartViewData.min, chartViewData.max);
        var max = calculateYAxisMaxValue(chartViewData.min, chartViewData.max);
        highChartOptionsObservable(
            {
                chart: {},
                title: {
                    text: name,
                    align: 'center',
                    x: 30
                },
                credits: { enabled: false },
                plotOptions: { series: { animation: false } },
                xAxis: isQuarterly ? quarterlyXaxis() : MonthlyXaxis(),
                yAxis:
                {
                    title: { text: "Net Interest Income ($000)" },
                    min: min,
                    max: max,
                    gridLineWidth: 0,
                    tickLength: 5,
                    tickWidth: 1,
                    tickPosition: 'outside'
                },
                series: chartViewData.seriesJsonObjects
            }
        );
    };

    var createLiabilityChart = function (series, title, min, max) {
        // var data = [3.0, 6.0, 9.0, 12.0, 24.0, 36.0, 48.0, 60.0];
        return {
            credits: { enabled: false },
            title: {
                text: title,
            },
            subtitle: {
            },
            plotOptions: {
                series: {
                    animation: false,
                    connectNulls: true
                },
                line: { connectNulls: true }

            },
            xAxis: {
                tickInterval: 3,
                labels: {
                    formatter: function () {
                        if (this.value == 48 || this.value == 36 || this.value == 24 || this.value == 12 || this.value == 9 || this.value == 6 || this.value == 3 || this.value == 60) {
                            return this.value;
                        }
                    }
                },
                title: {
                    text: "Months"
                }
            },
            yAxis: {
                title: {
                    text: "Rates"
                },
                min: min,
                max: max
            },
            series: series
        };



    }

    var staggerIcons = function (series) {

    }

    var createLiquidityChart = function (series) {
        return {
            credits: { enabled: false },
            title: {
                text: '',
            },
            subtitle: {
            },
            
            plotOptions: {
                series: {
                    animation: false,
                }
            },
            xAxis: {
                categories: ['Q1Y1', 'Q2Y1', 'Q3Y1', 'Q4Y1', 'Q1Y2', 'Q2Y2', 'Q3Y2', 'Q4Y2']
            },
            series: series
        };
    }

    var mRound = function (val, mroundVal) {
        return Math.round(val / mroundVal, 0) * mroundVal;
    };

    var standardChartScaling = function (maxValue) {
        maxValue = maxValue / 1000;
        var minRange = 0;
        var maxRange = 0;
        var roundVal = 0;
        var otherVal = 0;
        var majorUnit = 0;

        if (maxValue <= 1000) {
            roundVal = 250;
            otherVal = 200;
        }
        else if (maxValue <= 100000) {
            roundVal = 2500;
            otherVal = 2000;
        }
        else if (maxValue <= 1000000) {
            roundVal = 25000;
            otherVal = 5000;
        }
        else {
            roundVal = 250000;
            otherVal = 50000;
        }

        var maxRound = mRound(maxValue, roundVal);

        if (maxRound < maxValue + otherVal) {
            maxRange = maxRound + roundVal;
        }
        else {
            maxRange = maxRound;
        }

        majorUnit = maxRange / 5;

        console.log(maxRange);
        console.log(minRange);
        console.log(majorUnit);
        return {
            scaleMax: maxRange,
            scaleMin: minRange,
            majorUnit: majorUnit
        };
    };

    var createCapitalAnalysisChart = function (series, cats, maxVal) {
        var scaleOptions = standardChartScaling(maxVal);
        var height = [200];
        var tickPositions = [];
        var temp = 0;
        if (maxVal < 27.5) {
            scaleOptions.scaleMin = 0;
            scaleOptions.majorUnit = 5;
            temp = Math.round(maxVal / 5) * 5 - maxVal;
            if (temp < 2.5) {
                scaleOptions.scaleMax = Math.round(maxVal / 5) * 5 + 5;
            } else {
                scaleOptions.scaleMax = Math.round(maxVal / 5) * 5;
            }

        } else {
            scaleOptions.scaleMin = 0;
            scaleOptions.majorUnit = 10;
            temp = Math.round(maxVal / 10) * 10 - maxVal;
            if (temp < 5) {
                scaleOptions.scaleMax = Math.round(maxVal / 10) * 10 + 10;
            } else {
                scaleOptions.scaleMax = Math.round(maxVal / 10) * 10;
            }
        }


        var temp = scaleOptions.scaleMin;
        while (temp <= scaleOptions.scaleMax) {
            tickPositions.push(temp);
            temp += scaleOptions.majorUnit;
        }


        return {
            chart: {
                type: 'column'
            },
            credits: { enabled: false },
            title: {
                text: "",
            },
            subtitle: {
            },
            plotOptions: {
                series: {
                    animation: false,
                }
            },
            xAxis: {
                categories: cats
            },
            yAxis: {
                tickInterval: '5',
                tickLength: '5',
                //min: 0,
                //max: 27.5,
                max: scaleOptions.scaleMax,
                min: scaleOptions.scaleMin,
                tickInterval: scaleOptions.majorUnit,
                tickPositions: tickPositions,
                title: {
                    text: ''
                 }
            },

            legend: {
                enabled: true,
                verticalAlign: 'bottom',
                padding: 0,
                align: 'right'
            },

            series: series
        };
    }

    
    var createTimeDepositChart = function (data, tooltipFormat) {
        var retObj = {
            chart: {
                type: 'waterfall',
                animation: false
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            plotOptions: {
                series: {
                      dashStyle: 'solid',
                }
            },
            xAxis: {
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    enabled: false
                }

            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    if (tooltipFormat == 1) {
                        return '<b>' + numeral(this.y / 1000).format('0,0') + ' USD</b>'
                    } else if (tooltipFormat == 2) {
                        return '<b>' + numeral(this.y).format('0.00') + '%</b>'
                    } else {
                        return '<b>' + numeral(this.y).format('0,0') + '</b>'
                    }
                }
            },
            series: [{
                data: data
            }]
        };


        return retObj;

    }

    //var simCompareChartMinMax = function (min, max) {
    //    var divisor = 50;
    //    var scaleUnit = Math.round((((max - min) / 4.0) / divisor) * divisor);
    //    var calcMin = Math.round(((min / divisor) * divisor) - scaleUnit);
    //    var calcMax = (scaleUnit * 6) + calcMin;

    //    return {
    //        min: (calcMin < 0 && min >= 0) ? 0 : calcMin,
    //        max: calcMax,
    //        inc: scaleUnit
    //    }
    //}
    var simCompareChartMinMax = function (min, max) {
        var tempMin = Math.round(min / 1000);
        var tempMax = Math.round(max / 1000);
        var divisor = 25;

        //var scaleUnit = Math.round(((max - min) / 3.0) / divisor) * divisor;
        var scaleUnit = Math.round(((tempMax - tempMin) / 3.0) / divisor) * divisor;
        if (scaleUnit < 25) {
            scaleUnit = 25;
        }

        var temp = Math.ceil(tempMin / divisor) * divisor - scaleUnit;
        //var calcMax = Math.ceil(max / divisor) * divisor + scaleUnit;
        //var calcMin = Math.floor(min / divisor) * divisor - scaleUnit;
        //var calcMax = Math.ceil(tempMax / divisor) * divisor + scaleUnit;
        var calcMin = temp;//Math.floor(tempMin / divisor) * divisor - scaleUnit;
        if (min > 0 && temp < 0) {
            calcMin = 0
        }

        var calcMax = calcMin + (scaleUnit * 5);

        scaleUnit = scaleUnit * 1000;
        calcMax = calcMax * 1000;
        calcMin = calcMin * 1000;

        return {
            min: calcMin,//(calcMin < 0 && min > 0) ? 0 : calcMin,
            max: calcMax,
            inc: scaleUnit
        }
    }


    return {
        createHighChartOptions: createHighChartOptions,
        createEveChartOptions: createEveChartOptions,
        createEvePolicyChartOptions: createEvePolicyChartOptions,
        createBalanceSheetMixPie: createBalanceSheetMixPie,
        simulationCompareChart: simulationCompareChart,
        moveSharedAxis: moveSharedAxis,
        redrawSharedAxisChart: redrawSharedAxisChart,
        sharedAxisChart: sharedAxisChart,
        interestRatePolicyGuidelinesChart: interestRatePolicyGuidelinesChart,
        scenarioSeriesColor: scenarioSeriesColor,
        scenarioProperName: scenarioProperName,
        scenarioIdLookUp: scenarioIdLookUp,
        cashflowScaling: cashflowScaling,
        netCashflowSummaryChart: netCashflowSummaryChart,
        cashflowChart: cashflowChart,
        cashflowSummaryChart: cashflowSummaryChart,
        createLiabilityChart: createLiabilityChart,
        createLiquidityChart: createLiquidityChart,
        createTimeDepositChart: createTimeDepositChart,
        createCapitalAnalysisChart: createCapitalAnalysisChart,
        simCompareChartMinMax: simCompareChartMinMax,
        chartColorsPalette1: chartColorsPalette1,
        chartColorsPalette2: chartColorsPalette2,
        chartColorsTextPalette2: chartColorsTextPalette2,
        negativeValueRed: negativeValueRed,
        scenarioIconStagger: scenarioIconStagger
    };


    function calculateYAxisMinValue(minIncomeValue, maxIncomeValue) {
        var calcMin = (Math.floor(minIncomeValue / 25) * 25) - getYAxisPaddingAmount(minIncomeValue, maxIncomeValue);
        if (minIncomeValue >= 0 && calcMin < 0) calcMin = 0;
        return calcMin;
    }

    function calculateYAxisMaxValue(minIncomeValue, maxIncomeValue) {
        return (Math.ceil(maxIncomeValue / 25) * 25) + getYAxisPaddingAmount(minIncomeValue, maxIncomeValue);
    }

    function getYAxisPaddingAmount(minIncomeValue, maxIncomeValue) {
        var paddingAmount = (((maxIncomeValue - minIncomeValue) / 4) / 25) * 25;
        return paddingAmount <= 25 ? 25 : paddingAmount;
    }

    function quarterlyXaxis() {
        return {
            plotLines: [{
                width: 1,
                zIndex: 1,
                value: 3.5,
                color: 'rgb(230, 230, 230)'
            },
            {
                width: 1,
                zIndex: 1,
                value: 7.5,
                color: 'rgb(230, 230, 230)'
            },
            {
                width: 1,
                zIndex: 1,
                value: 11.5,
                color: 'rgb(230, 230, 230)'
            },
            {
                width: 1,
                zIndex: 1,
                value: 15.5,
                color: 'rgb(230, 230, 230)'
            }
           
            ],
            plotBands: [{
                color: '#ffffff',
                label: {
                    text: "YEAR 1",
                    style: {
                        color: '#000'
                    },
                },
                from: -1,
                to: 3.5
            }, {
                color: '#ffffff',
                label: {
                    text: "YEAR 2",
                    style: {
                        color: '#000'
                    },
                },
                from: 3.5,
                to: 7.5
            }, {
                color: '#ffffff',
                label: {
                    text: "YEAR 3",
                    style: {
                        color: '#000'
                    },
                },
                from: 7.5,
                to: 11.5
            },

            {
                color: '#ffffff',
                label: {
                    text: "YEAR 4",
                    style: {
                        color: '#000'
                    },
                },
                from: 11.5,
                to: 15.5
            }, {
                color: '#ffffff',
                label: {
                    text: "YEAR 5",
                    style: {
                        color: '#000'
                    },
                },
                from: 15.5,
                to: 19.5
            }],
            categories: ["Q1", "Q2", "Q3", "Q4", "Q1", "Q2", "Q3", "Q4", "Q1", "Q2", "Q3", "Q4", "Q1", "Q2", "Q3", "Q4", "Q1", "Q2", "Q3", "Q4"],
            tickLength: 0,
            labels: {
                useHTML: true,
                formatter: function () {
                    if (this.value === 'Q4') {
                        return '<div class="lines" style="color: green">' + this.value + '</div>';
                    }
                    else {
                        return this.value;
                    }
                }
            }
        }
    };

    function MonthlyXaxis() {
        return {
            plotLines: [{
                width: 1,
                zIndex: 1,
                value: 11.5,
                color: 'rgb(230, 230, 230)'
            }],
            plotBands: [{
                color: '#ffffff',
                label: {
                    text: "YEAR 1",
                    style: {
                        color: '#000'
                    },
                },
                from: -1,
                to: 11.5
            }, {
                color: '#ffffff',
                label: {
                    text: "YEAR 2",
                    style: {
                        color: '#000'
                    },
                },
                from: 11.5,
                to: 23.5
            }],
            categories: ["M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12"],
            tickLength: 0,
            labels: {
                useHTML: true,
                formatter: function () {
                    if (this.value === 'Q4') {
                        return '<div class="lines">' + this.value + '</div>';
                    } else {
                        return this.value;
                    }
                }
            }
        }
    };

    function monthlyXaxis() {
        return [
            {
                name: "Year 1",
                categories: ["M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12"]
            }, {
                name: "Year 2",
                categories: ["M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12"]
            }
        ];
    }
});