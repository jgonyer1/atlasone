﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var cashflowVm = function (cashflow) {
        var self = this;
        var curPage = "CashFlowReportConfig";
        this.cashflow = cashflow;

        this.subs = [];

        this.cancel = function () {
        };

        //Period Compares Block
        this.sortedSimulations = ko.observableArray();

        this.sortCashflowSimulations = function () {
            console.log(this);




            self.sortedSimulations(self.cashflow().cashflowSimulationScenario().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
        };

       // this.cashflow().cashflowSimulationScenario().sort(function (a, b) { return a.priority() - b.priority(); });

        this.removeCashflowSimulationScenario = function (simScen) {
            var msg = 'Delete Simulation/Scenario/DateOffset  "' + simScen.priority() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    simScen.entityAspect.setDeleted();
                    cashflow().cashflowSimulationScenario().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortCashflowSimulations();

                    if (cashflow().cashflowSimulationScenario().length < 3) {
                        $('#addSimulationScenario').removeClass('disabled');
                    }
                }
            }

        };

        this.sortableAfterMove = function (arg) {
            //first we need to find existing row based off of priority so we can correctly adjust the compare scen
            var oldIndex = 0;
            for (var x = 0; x < self.cashflow().cashflowSimulationScenario().length; x++) {
                if (self.cashflow().compare() == self.cashflow().cashflowSimulationScenario()[x].priority()) {
                    oldIndex = x;
                }
            }

            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });

            //find item and set it
            self.cashflow().compare(self.cashflow().cashflowSimulationScenario()[oldIndex].priority());
        };

        this.addSimulationscenario = function () {
            console.log('callling this turd');
            var newOffset = globalContext.createCashflowSimulationScenario();
            newOffset.priority(cashflow().cashflowSimulationScenario().length);
            newOffset.institutionDatabaseName(self.profile().databaseName);
       
            if (self.cashflow().cashflowSimulationScenario().length == 0) {
                self.cashflow().compare(0);
            }

            //AsOfDateOffset Handling (Custom FundingMatrix Multi-Institution Template)
            newOffset.asOfDateOffsets = ko.observableArray();
            newOffset.policyOffsets = ko.observableArray();

            newOffset.simulationTypes = ko.observableArray();
            newOffset.scenarioTypes = ko.observableArray();


            globalContext.getPolicyOffsets(newOffset.policyOffsets, newOffset.institutionDatabaseName()).then(function () {
                globalContext.getAsOfDateOffsets(newOffset.asOfDateOffsets, newOffset.institutionDatabaseName()).then(function () {
                    globalContext.getAvailableSimulations(newOffset.simulationTypes, newOffset.institutionDatabaseName(), newOffset.asOfDateOffset()).then(function () {
                        globalContext.getAvailableSimulationScenarios(newOffset.scenarioTypes, newOffset.institutionDatabaseName(), newOffset.asOfDateOffset(), newOffset.simulationTypeId(), false).then(function () {
                            self.defaultSimulation(newOffset);
                            newOffset.instSub = newOffset.institutionDatabaseName.subscribe(function (newValue) {
                                instChange(newOffset);
                            }, newOffset);

                            newOffset.offsetSub = newOffset.asOfDateOffset.subscribe(function (newValue) {
                                self.refreshItemSim(newOffset);
                                self.defaultSimulation(newOffset);
                            }, newOffset);


                            newOffset.simSub = newOffset.simulationTypeId.subscribe(function (newValue) {
                                self.refreshItemScen(this);
                            }, newOffset);


                            newOffset.ovSub = newOffset.overrideSimulationId.subscribe(function (newValue) {
                                if (!newValue) {
                                    self.refreshItemSim(newOffset);
                                    self.defaultSimulation(newOffset);
                                }
                            });


                            self.cashflow().cashflowSimulationScenario().push(newOffset);
                            self.sortCashflowSimulations();
                        });
                    });
                });
            });


        };

        

        this.institutions = ko.observableArray();

        this.global = global;


        this.definedGroupings = ko.observableArray();

        this.reportSelections = ko.observableArray([{ id: 1, name: 'Investment' }, { id: 2, name: 'Loan' }, { id: 3, name: 'Funding Maturities' }]);

        this.compositionComplete = function () {
            console.log('cino asf');
        }

        this.defaultSimulation = function (fmOffset) {

            if (!fmOffset.overrideSimulationId()) {
                var aod = fmOffset.asOfDateOffsets()[fmOffset.asOfDateOffset()].asOfDateDescript;

                if (fmOffset.policyOffsets()[0][aod] != undefined) {
                    fmOffset.simulationTypeId(fmOffset.policyOffsets()[0][aod][0].niiId);
                } else {
                    fmOffset.simulationTypeId(1);
                    app.showMessage("No Policies found for " + aod + ", defaulting to Base Simulation", "Error", ["OK"]);
                }
            }
            return true;
        };

        this.refreshItemScen = function (item) {
            globalContext.getAvailableSimulationScenarios(item.scenarioTypes, item.institutionDatabaseName(), item.asOfDateOffset(), item.simulationTypeId(), false)
        }

        this.refreshItemSim = function (item) {
            globalContext.getAvailableSimulations(item.simulationTypes, item.institutionDatabaseName(),item.asOfDateOffset()).then(function () {
                self.refreshItemScen(item);
            });
        }

        var instChange = function (offset) {
            globalContext.getAsOfDateOffsets(offset.asOfDateOffsets, offset.institutionDatabaseName()).then(function () {
                globalContext.getPolicyOffsets(offset.policyOffsets, offset.institutionDatabaseName()).then(function () {
                    self.refreshItemSim(offset);
                    self.defaultSimulation(offset);
                });
            });
        };

        this.profile = ko.observable();

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);
           // globalContext.clearCache();
            global.loadingReport(true);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                     return globalContext.getUserProfile(self.profile).then(function () {

                         

                         //loop through offsets and set up simulationTypes array
                         var offsetsProcessed = 0
                         self.cashflow().cashflowSimulationScenario().forEach(function (item, index, arr) {

                             item.asOfDateOffsets = ko.observableArray();
                             item.policyOffsets = ko.observableArray();
                             item.simulationTypes = ko.observableArray();
                             item.scenarioTypes = ko.observableArray();

                             globalContext.getPolicyOffsets(item.policyOffsets, item.institutionDatabaseName()).then(function () {
                                 globalContext.getAsOfDateOffsets(item.asOfDateOffsets, item.institutionDatabaseName()).then(function () {
                                     globalContext.getAvailableSimulations(item.simulationTypes, item.institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                                         globalContext.getAvailableSimulationScenarios(item.scenarioTypes, item.institutionDatabaseName(), item.asOfDateOffset(), item.simulationTypeId(), false).then(function () {
                                             self.sortCashflowSimulations();
                                             item.offsetSub = item.asOfDateOffset.subscribe(function (newValue) {
                                                 self.refreshItemSim(this);
                                             }, item);

                                             item.override = item.overrideSimulationId.subscribe(function (newValue) {
                                                 self.refreshItemSim(item);

                                             }, item);

                                             item.simSub = item.simulationTypeId.subscribe(function (newValue) {
                                                 self.resetScensAvailableScens(item);

                                             }, item);

                                             self.defaultSimulation(item);
                                        
                                             offsetsProcessed++;

                                             //when done this just move ona nd setorrest chagnes function
                                             if (offsetsProcessed == arr.length) {

                                                 self.sortCashflowSimulations();
                                                 if (self.cashflow().id() > 0) {
                                                     globalContext.saveChangesQuiet();
                                                 }
                                                 global.loadingReport(false);
                                             }
                                         });
                                        });
                                 });
                             });
                   
                        });



                      /*  for (var z = 0; z < self.cashflow().cashflowSimulationScenario().length; z++) {
                            self.cashflow().cashflowSimulationScenario()[z].asOfDateOffsets = ko.observableArray();
                            self.cashflow().cashflowSimulationScenario()[z].policyOffsets = ko.observableArray();
                            self.cashflow().cashflowSimulationScenario()[z].simulationTypes = ko.observableArray();
                            self.cashflow().cashflowSimulationScenario()[z].scenarioTypes = ko.observableArray();

                            self.loadEachCashflow(self.cashflow().cashflowSimulationScenario()[z], z);
                        }*/
                    }); //blank instName will just use profile institution
                });
            });
        };


        this.detached = function (isClose) {
            console.log('disposing outside');
            for (var z = 0; z < self.cashflow().cashflowSimulationScenario().length; z++) {
                try {
                    self.cashflow().cashflowSimulationScenario()[z].instSub.dispose();
                    self.cashflow().cashflowSimulationScenario()[z].ovSub.dispose();
                    self.cashflow().cashflowSimulationScenario()[z].simSub.dispose();

                    self.cashflow().cashflowSimulationScenario()[z].offsetSub.dispose();
                    
                }
                catch (err) {

                }



            }
        };

        return this;
    };

    return cashflowVm;
});
