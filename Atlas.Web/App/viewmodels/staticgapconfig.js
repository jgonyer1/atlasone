﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var staticGapVm = function (staticGap) {
        var self = this;
        this.staticGap = staticGap;
        this.modelSetup = ko.observable();
        this.subs = [];
        this.cancel = function () {
        };

        this.group = ko.observable();
        this.definedGroup = ko.observable();

        this.institutions = ko.observableArray();

        this.global = global;
        this.leftAsOfDateOffsets = ko.observableArray();
        this.rightAsOfDateOffsets = ko.observableArray();

        this.profile = ko.observableArray();

        this.definedGroupings = ko.observableArray();

        this.leftPolicyOffsets = ko.observableArray();
        this.rightPolicyOffsets = ko.observableArray();


        this.leftSimulationTypes = ko.observableArray();
        this.leftScenarioTypes = ko.observableArray();

        this.rightSimulationTypes = ko.observableArray();
        this.rightScenarioTypes = ko.observableArray();

        this.enableSummaryDropdown = ko.computed(function () {
            var intFormat = parseInt(self.staticGap().reportFormat());
            return intFormat > 2;
        });

        this.changeSummaryDropdown = function () {
            var selectedValue = document.getElementById("summary_format_dropdown").options[document.getElementById("summary_format_dropdown").selectedIndex].value;
            self.staticGap().reportFormat(selectedValue);
        };

        this.summaryFormatChange = function () {
            if ($("#summary_placeholder_radio").prop("checked")) {
                self.staticGap().reportFormat("3");
                document.getElementById("summary_format_dropdown").selectedIndex = 0;
            } else {
                //document.getElementById("summary_format_dropdown").selectedIndex = -1;
            }
        };

        this.updateViewOption = function () {
            staticGap().viewOption(self.definedGroup());
            return true;
        }

        this.defaultDefinedGroup = function () {
            staticGap().viewOption(self.definedGroupings()[0].id);
            return true;
        }

        this.disableGroups = function () {
            self.group(false);
            return true;
        }

        this.reportFormats = ko.observableArray([
            { title: "Schedule J", value: "0", disable: false },
            { title: "Schedule H", value: "1", disable: false },
            { title: "Schedule H + 121-180Mths & > 180Mths", value: "2", disable: false },
            { title: "Summary", value: "3", disable: false }
        ]);

        //multi group select array that gets set after we pull down defined groupings
        this.viewOptions = ko.observableArray();

        this.setOptionDisable = function (option, item) {
            ko.applyBindingsToNode(option, { disable: item.disable, css: {'def-group-opt': item.defGroupClass} }, item);
        }

        this.defaultLeftSimulationScenario = function () {
            //Based off off set find corresponding policy
            var aod = self.leftAsOfDateOffsets()[self.staticGap().leftAsOfDateOffset()].asOfDateDescript;
            if (!self.staticGap().leftOverrideSimulationId()) {
                if (self.leftPolicyOffsets()[0][aod] && self.leftPolicyOffsets()[0][aod][0] != undefined) {
                    self.staticGap().leftSimulationTypeId(self.leftPolicyOffsets()[0][aod][0].niiId);
                }                
            }
            if (!self.staticGap().leftOverrideScenarioId()) {
                if (self.leftPolicyOffsets()[0][aod] && self.leftPolicyOffsets()[0][aod][0] != undefined) {
                    var arr = self.leftScenarioTypes().filter(function (obj) {
                        return obj.id == "1"
                    });
                    if (self.leftPolicyOffsets()[0][aod][0].bsSimulation == self.leftPolicyOffsets()[0][aod][0].niiId) {
                        self.staticGap().leftScenarioTypeId(self.leftPolicyOffsets()[0][aod][0].bsScenario);
                    } else if (arr.length > 0) {
                        self.staticGap().leftScenarioTypeId("1");
                    }else {
                        self.staticGap().leftScenarioTypeId(self.leftScenarioTypes()[0].id());
                    }
                }
            }
        }
        this.defaultRightSimulationScenario = function () {
            var aod = self.rightAsOfDateOffsets()[self.staticGap().rightAsOfDateOffset()].asOfDateDescript;
            //Loop through offsets and find the as ofdate that we want
            if (!self.staticGap().rightOverrideSimulationId()) {
                
                if ( self.rightPolicyOffsets()[0][aod] && self.rightPolicyOffsets()[0][aod][0]) {
                    self.staticGap().rightSimulationTypeId(self.rightPolicyOffsets()[0][aod][0].niiId);
                }
            }
            if (!self.staticGap().rightOverrideScenarioId()) {
                if (self.rightPolicyOffsets()[0][aod] && self.rightPolicyOffsets()[0][aod][0]) {
                    var arr = self.rightScenarioTypes().filter(function (obj) {
                        return obj.id == "1"
                    });
                    if (self.rightPolicyOffsets()[0][aod][0].bsSimulation == self.rightPolicyOffsets()[0][aod][0].niiId) {
                        self.staticGap().rightScenarioTypeId(self.rightPolicyOffsets()[0][aod][0].bsScenario);
                    } else if (arr.length > 0) {
                        self.staticGap().rightScenarioTypeId("1");
                    } else {
                        self.staticGap().rightScenarioTypeId(self.rightScenarioTypes()[0].id());
                    }
                }
            }
        }

        this.compositionComplete = function () {
            if (self.staticGap().id() > 0) {
                globalContext.saveChangesQuiet();
            }
        };

        this.refreshLeftScen = function () {
            globalContext.getAvailableSimulationScenarios(self.leftScenarioTypes, self.staticGap().leftInstitutionDatabaseName(), self.staticGap().leftAsOfDateOffset(), self.staticGap().leftSimulationTypeId(), false)
        }

        this.refreshLeftSim = function () {
            globalContext.getAvailableSimulations(self.leftSimulationTypes, self.staticGap().leftInstitutionDatabaseName(), self.staticGap().leftAsOfDateOffset()).then(function () {
                self.refreshLeftScen();
            });
        }

        this.refreshRightScen = function () {
            globalContext.getAvailableSimulationScenarios(self.rightScenarioTypes, self.staticGap().rightInstitutionDatabaseName(), self.staticGap().rightAsOfDateOffset(), self.staticGap().rightSimulationTypeId(), false)
        }

        this.refreshRightSim = function () {
            globalContext.getAvailableSimulations(self.rightSimulationTypes, self.staticGap().rightInstitutionDatabaseName(), self.staticGap().rightAsOfDateOffset()).then(function () {
                self.refreshRightScen();
            });
        }

        this.activate = function (id) {
            global.loadingReport(true);
            return globalContext.getAvailableSimulations(self.leftSimulationTypes, self.staticGap().leftInstitutionDatabaseName(), self.staticGap().leftAsOfDateOffset()).then(function () {
                return globalContext.getAvailableSimulationScenarios(self.leftScenarioTypes, self.staticGap().leftInstitutionDatabaseName(), self.staticGap().leftAsOfDateOffset(), self.staticGap().leftSimulationTypeId(), false).then(function () {
                    return globalContext.getAvailableSimulations(self.rightSimulationTypes, self.staticGap().rightInstitutionDatabaseName(), self.staticGap().rightAsOfDateOffset()).then(function () {
                        return globalContext.getAvailableSimulationScenarios(self.rightScenarioTypes, self.staticGap().rightInstitutionDatabaseName(), self.staticGap().rightAsOfDateOffset(), self.staticGap().rightSimulationTypeId(), false).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                            return globalContext.getModelSetup(self.modelSetup).then(function () {

                                    //tax equiv syncing
                                    if (!self.staticGap().overrideTaxEquiv()) {
                                        self.staticGap().taxEqivalentYields(self.modelSetup().reportTaxEquivalentYield());
                                        if (self.staticGap().id() > 0) {
                                            globalContext.saveChangesQuiet();
                                        }
                                    }
           

                                    self.subs.push(self.staticGap().overrideTaxEquiv.subscribe(function (newValue) {
                                        if (!newValue) {
                                            self.staticGap().taxEqivalentYields(self.modelSetup().reportTaxEquivalentYield());
                                        }
                                    }));

                                    self.definedGroup();
                                    var arr = [];
                                    arr.push({
                                        name: "Standard View", options: [
                                            { name: "Sub Accounts", id: "0" },
                                            { name: "Classifications", id: "1" },
                                            { name: "Account Type", id: "2" },
                                        ]
                                    });
                                    arr.push({ name: "Defined Groupings", options: self.definedGroupings() });
                                    self.viewOptions(global.getGroupedSelectOptions(arr));

                                    if (staticGap().viewOption().indexOf('dg_') > -1) {
                                        self.group(true);
                                        self.definedGroup(staticGap().viewOption());
                                    }
                                    else {
                                        self.group(false);
                                    }

                                    //AsOfDateOffset Handling (Double-Institution Template)
                                    self.subs.push(self.staticGap().leftInstitutionDatabaseName.subscribe(function (newValue) {
                                        return Q.all([
                                            globalContext.getAsOfDateOffsets(self.leftAsOfDateOffsets, newValue),
                                            globalContext.getPolicyOffsets(self.leftPolicyOffsets, newValue)
                                        ]).then(function () {
                                            self.refreshLeftSim();
                                            self.defaultLeftSimulationScenario();
                                        });
                                    }));
                                    self.subs.push(self.staticGap().rightInstitutionDatabaseName.subscribe(function (newValue) {
                                        return Q.all([
                                            globalContext.getAsOfDateOffsets(self.rightAsOfDateOffsets, newValue),
                                            globalContext.getPolicyOffsets(self.rightPolicyOffsets, newValue)
                                        ]).then(function () {
                                            self.refreshRightSim();
                                            self.defaultRightSimulationScenario();
                                        });
                                    }));

                                //Subscriptions for date off set changing
                                    self.subs.push(self.staticGap().leftAsOfDateOffset.subscribe(function (newValue) {
                                        self.refreshLeftSim();
                                        self.defaultLeftSimulationScenario();
                                    }));
                                    self.subs.push(self.staticGap().rightAsOfDateOffset.subscribe(function (newValue) {
                                        self.refreshRightSim();
                                        self.defaultRightSimulationScenario();
                                    }));

                                    self.subs.push(self.staticGap().leftOverrideSimulationId.subscribe(function (newValue) {
                                        self.refreshLeftScen();
                                        self.defaultLeftSimulationScenario();
                                    }));
                                    self.subs.push(self.staticGap().leftOverrideScenarioId.subscribe(function (newValue) {
                                   
                                        self.defaultLeftSimulationScenario();
                                    }));


                                    self.subs.push(self.staticGap().rightOverrideSimulationId.subscribe(function (newValue) {
                                        self.refreshRightScen();
                                        self.defaultRightSimulationScenario();
                                    }));
                                    self.subs.push(self.staticGap().rightOverrideScenarioId.subscribe(function (newValue) {
                                        self.defaultRightSimulationScenario();
                                    }));

                                    self.subs.push(self.staticGap().leftSimulationTypeId.subscribe(function (newValue) {
                                        self.refreshLeftScen();
                                        self.defaultLeftSimulationScenario();
                                    }));
                                    self.subs.push(self.staticGap().rightSimulationTypeId.subscribe(function (newValue) {
                                        self.refreshRightScen();
                                        self.defaultRightSimulationScenario();
                                    }));
             

                                return Q.all([
                                    globalContext.getAsOfDateOffsets(self.leftAsOfDateOffsets, self.staticGap().leftInstitutionDatabaseName()),
                                    globalContext.getAsOfDateOffsets(self.rightAsOfDateOffsets, self.staticGap().rightInstitutionDatabaseName()),
                                    globalContext.getPolicyOffsets(self.leftPolicyOffsets, self.staticGap().leftInstitutionDatabaseName()),
                                    globalContext.getPolicyOffsets(self.rightPolicyOffsets, self.staticGap().rightInstitutionDatabaseName())
                                ]).then(function () {
                                    //Left Simulation Sync
                                 
                                    self.defaultLeftSimulationScenario();
                                    self.defaultRightSimulationScenario();
                                    global.loadingReport(false);
                                 });
                            });
                        });
                    });
                });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Double-Institution Template)
            //self.leftInstSub.dispose();
            //self.rightInstSub.dispose();
            for (var i = 0; i < self.subs.length; i++) {
                self.subs[i].dispose();
            }
        };

        return this;
    };

    return staticGapVm;
});
