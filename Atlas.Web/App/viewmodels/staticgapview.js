﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');

    var staticGapVm = function (id, reportView) {
        var self = this;
        this.staticGapViewData = ko.observable();
        this.leftData;
        this.leftScenarioName;
        this.rightScenarioName;
        this.leftDate;
        this.rightDate;
        this.leftCellPos;
        this.rightCellPos;
        this.profile = ko.observable();

        function GenerateTableHeader(tableId, cells) {
            var headRow1 = '<tr id="' + tableId + 'HeaderRow1" style="border-bottom: 1px solid #ffffff;"><th></th><th colspan="2" style="text-align: center !important; border-bottom: 3px solid #E9E9E9;">Total</th>';
            var headRow2 = '<tr><th style="text-align: right !important; border-bottomstyle="text-align: right!important;": 1px solid #ffffff !important;"></th><th style="text-align: right!important;">Volume</th><th>Rate</th>';

            for (var i = 0; i < Object.keys(cells).length; i++) {
                if (self.staticGapViewData().schedule != "3" || Object.keys(cells)[i] != "One Day") {
                    headRow1 += '<th colspan="2" style="text-align: center !important; border-bottom: 3px solid #E9E9E9;"></th>';
                    headRow2 += '<th style="text-align: right !important;">Volume</th><th style="text-align: right !important;">Rate</th>';
                }
            }

            headRow1 += '</tr>';
            headRow2 += '</tr>';
            $('#' + tableId + ' thead').append(headRow1);
            $('#' + tableId + ' thead').append(headRow2);

            for (var c in cells) {
                if (self.staticGapViewData().schedule != "3" || c != "One Day") {
                    $($('#' + tableId + 'HeaderRow1').children('th')[cells[c].headerCell]).text(c);
                }
            }
        }

        function TableTitle(inst, instDB, simulation, scenario, asOfDate) {
            if (self.profile().databaseName == instDB) {
                return simulation + ' Simulation as of  ' + asOfDate + ', ' + scenario + ' Scenario';
            } else {
                return inst + '-' + simulation + ' Simulation as of  ' + asOfDate + ', ' + scenario + ' Scenario';
            }
        }

        var getSecRateComponent = function (rate, bal) {
            return rate * (Math.round((bal + 0.00001) * 100) / 100);
        };

        var secRateComponentUnitTest = function () {
            var result = getSecRateComponent(0.5, 100);

            if (global.isTesting && result != 50) {
                document.getElementById("staticgap-view").innerHTML = "SECTION RATE COMPONENT UNIT TEST FAILED" + document.getElementById("staticgap-view").innerHTML;
            }
        };

        var getTotRateComponent = function (r, b) {
            return (r / (Math.round((b + 0.00001) * 100) / 100)) * (Math.round((b + 0.00001) * 100) / 100);
        }

        var totRateComponentUnitTest = function () {
            var result = getTotRateComponent(0.5, 100);

            if (global.isTesting && result != 0.5) {
                document.getElementById("staticgap-view").innerHTML = "TOTAL RATE COMPONENT UNIT TEST FAILED" + document.getElementById("staticgap-view").innerHTML;
            }
        };

        this.compositionComplete = function () {
            secRateComponentUnitTest();
            totRateComponentUnitTest();
            var tables = [{ tbl: 'leftTable', cells: self.leftCellPos }, { tbl: 'rightTable', cells: self.rightCellPos }];
            var bucketValLength = 0;

            if (self.staticGapViewData().schedule == "3") {
                //for summary we need to tell the report which cell the One Day values should go into but we don't want to give their own cell
                bucketValLength = Object.keys(self.leftCellPos).length * 2;
            } else {
                bucketValLength = Object.keys(self.leftCellPos).length * 2 + 2;
            }

            for (var k = 0; k < tables.length; k++) {
                var tbl = tables[k];

                if (self.staticGapViewData().viewOption != '2' && k == 1)
                    continue;

                if (k== 0){
                    $('#' + tbl.tbl + 'Title').text(TableTitle(self.staticGapViewData().leftInstName, self.staticGapViewData().leftInstDBName, self.staticGapViewData().leftSimulation, self.staticGapViewData().leftScenario, self.staticGapViewData().leftDate));
                }
                else{
                    $('#' + tbl.tbl + 'Title').text(TableTitle(self.staticGapViewData().rightInstName, self.staticGapViewData().rightInstDBName, self.staticGapViewData().rightSimulation, self.staticGapViewData().rightScenario, self.staticGapViewData().rightDate));
                }

                //After HTML is rendered so we can access and manipulate
                GenerateTableHeader(tbl.tbl, tbl.cells);

                // Draw Headers Because number of columns can change
                //Name Field To Grab
                var name = '';
                var secondName = 'classificationName'
                switch (self.staticGapViewData().viewOption) {
                    case '0':
                        name = 'categoryName';
                        break;
                    case '1':
                        name = 'accountName';
                        break;
                    case '2':
                        name = 'accountName';
                        break;
                    default:
                        name = 'name';
                        break;
                }

                //Set Up Groupings
                var tblData = {
                    assets: {
                        cats: {},
                        bucketVals: new Array(bucketValLength)
                    },
                    liabs: {
                        cats: {},
                        bucketVals: new Array(bucketValLength)
                    }
                };

                //Default Bucket Bals
                for (var y = 0; y < tblData.assets.bucketVals.length; y++) {
                    tblData.assets.bucketVals[y] = 0;
                    tblData.liabs.bucketVals[y] = 0;
                }

                for (var z = 0; z < self.staticGapViewData()[tbl.tbl].length; z++) {
                    var rec = self.staticGapViewData()[tbl.tbl][z];
                    var type = 'liabs'

                    if (rec.isAsset) {
                        type = 'assets'
                    };

                    if (tblData[type].cats[rec[name]] == undefined) {
                        tblData[type].cats[rec[name]] = {
                            bucketVals: new Array(bucketValLength) //Double Cell Positions And Add two For total Columns 
                        };

                        if (self.staticGapViewData().viewOption == '1') {
                            tblData[type].cats[rec[name]].cats = {}
                        }

                        for (var y = 0; y < bucketValLength; y++) {
                            tblData[type].cats[rec[name]].bucketVals[y] = 0;
                        }
                    }

                    if (self.staticGapViewData().viewOption == '1') {
                        if ((tblData[type].cats[rec[name]].cats[rec['classificationName']] == undefined)) {
                            tblData[type].cats[rec[name]].cats[rec['classificationName']] = {
                                bucketVals: new Array(bucketValLength) //Double Cell Positions And Add two For total Columns 
                            };

                            for (var y = 0; y < bucketValLength; y++) {
                                tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[y] = 0;
                            }
                        }
                    }

                    //Set Balance and Rate
                    tblData[type].cats[rec[name]].bucketVals[tbl.cells[rec.monthGrouping].cell - 1] += rec.balance;
                    tblData[type].cats[rec[name]].bucketVals[tbl.cells[rec.monthGrouping].cell] += (rec.rate * rec.balance);
                    
                    if (self.staticGapViewData().viewOption == '1') {
                        tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[tbl.cells[rec.monthGrouping].cell - 1] = rec.balance;
                        tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[tbl.cells[rec.monthGrouping].cell] = rec.rate;
                    }

                    //Total for Grand Total
                    tblData[type].bucketVals[tbl.cells[rec.monthGrouping].cell - 1] += rec.balance;
                    tblData[type].bucketVals[tbl.cells[rec.monthGrouping].cell] += (rec.rate * rec.balance);

                    if ((tbl.cells[rec.monthGrouping].cell == 5 && self.staticGapViewData().schedule != "3") || (tbl.cells[rec.monthGrouping].cell == 3 && self.staticGapViewData().schedule == "3")) {
                        //Set Balance And Rate
                        tblData[type].cats[rec[name]].bucketVals[self.leftCellPos['One Day'].cell - 1] += rec.oneDayBal;
                        tblData[type].cats[rec[name]].bucketVals[self.leftCellPos['One Day'].cell] += (rec.oneDayRate * rec.oneDayBal);

                        if (self.staticGapViewData().viewOption == '1') {
                            if (self.staticGapViewData().schedule == "3") {
                                //for summary schedule, one day and month 1 have the same cell position, need to be added together.
                                tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[tbl.cells['One Day'].cell - 1] += rec.oneDayBal;
                                tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[tbl.cells['One Day'].cell] += rec.oneDayRate;
                            } else {
                                tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[tbl.cells['One Day'].cell - 1] = rec.oneDayBal;
                                tblData[type].cats[rec[name]].cats[rec['classificationName']].bucketVals[tbl.cells['One Day'].cell] = rec.oneDayRate;
                            }
                        }

                        //Total For Grand Total
                        tblData[type].bucketVals[tbl.cells['One Day'].cell - 1] += rec.oneDayBal;
                        tblData[type].bucketVals[tbl.cells['One Day'].cell] += (rec.oneDayRate * rec.oneDayBal);
                        //tblData[type].bucketVals[self.leftCellPos['One Day'].cell - 1] += rec.oneDayBal;
                        //tblData[type].bucketVals[self.leftCellPos['One Day'].cell] += (rec.oneDayRate * rec.oneDayBal);
                    } 
                }

                var cellCount = (Object.keys(tbl.cells).length * 2) + 3;
                var totBal = 0;
                var totRate = 0;
                var tmpRow = '';
                var row = '';

                for (var x = 0; x < 2; x++) {
                    var type = 'assets';
                    var dispName = 'Assets'

                    if (x == 1) {
                        type = 'liabs';
                        dispName = 'Liabilities';
                    }

                    $('#' + tbl.tbl + ' tbody').append('<tr class="spacerRow ' + (dispName == 'Liabilities' ? 'page-breakBlock' : '') + '"><td colspan="' + cellCount + '"></td></tr>');
                    $('#' + tbl.tbl + ' tbody').append('<tr class="' + (dispName == 'Liabilities' ? 'page-break' : '') + '" ><td class="name grandLevel" colspan="' + cellCount + '"> ' + (dispName == 'Liabilities' ? '' : '')  + dispName + '</td></tr>');

                    for (var cat in tblData[type].cats) {
                        //Write Out Account Header
                        if (self.staticGapViewData().viewOption == '1') {
                            $('#' + tbl.tbl + ' tbody').append('<tr><td class="name secondLevel" colspan="' + cellCount + '"> ' + cat + '</td></tr>');
                        }

                        var secondRow;
                        var secTmpRow;
                        var secTotBal;
                        var secTotRate;
                        var clCounter = 1;
                        var zeroLineItem = true;

                        for (var cl in tblData[type].cats[cat].cats) {
                            secTotBal = 0;
                            secTotRate = 0;
                            secTmpRow = '';
                            zeroLineItem = true;
                            var textUnderline = '';

                            if (clCounter == Object.keys(tblData[type].cats[cat].cats).length) {
                                //textUnderline = 'underline';
                            }

                            secondRow = '<tr><td class="name indent">' + cl + '</td>';

                            for (var i = 2; i < tblData[type].cats[cat].cats[cl].bucketVals.length; i++) { //Skip Totals Doing it Another Way now!
                                if (tblData[type].cats[cat].cats[cl].bucketVals[i] == '-' || tblData[type].cats[cat].cats[cl].bucketVals[i] == 0) {
                                    secTmpRow += '<td class="' + (i % 2 > 0 ? 'borderRight' : '') + ' ' + textUnderline + '">-</td>'
                                }
                                else {
                                    if (i % 2 > 0) {
                                        secTotRate += getSecRateComponent(tblData[type].cats[cat].cats[cl].bucketVals[i], tblData[type].cats[cat].cats[cl].bucketVals[i - 1]);//(tblData[type].cats[cat].cats[cl].bucketVals[i] * (Math.round((tblData[type].cats[cat].cats[cl].bucketVals[i - 1] + 0.00001) * 100)/ 100))
                                        secTmpRow += '<td class="borderRight ' + textUnderline + '">' + numeral(tblData[type].cats[cat].cats[cl].bucketVals[i]).format('(0.00)') + '</td>'
                                    }
                                    else {
                                        if (Math.round((tblData[type].cats[cat].cats[cl].bucketVals[i] + 0.00001) * 100) / 100 != 0) {
                                            zeroLineItem = false;
                                        }
                                        console.log("secTotBal: " + secTotBal);
                                        console.log("Thing Added: " + Math.round((tblData[type].cats[cat].cats[cl].bucketVals[i] + 0.00001) * 100) / 100);
                                        secTotBal += Math.round((tblData[type].cats[cat].cats[cl].bucketVals[i] + 0.00001) * 100) / 100;
                                        secTmpRow += '<td class="' + textUnderline + '">' + numeral(tblData[type].cats[cat].cats[cl].bucketVals[i] / 1000).format('(0,0)') + '</td>'
                                    }
                                }
                            }
                            secTotBal = Math.round((secTotBal + 0.00001) * 100) / 100;
                            tblData[type].cats[cat].cats[cl].bucketVals[0] = secTotBal;
                            tblData[type].cats[cat].cats[cl].bucketVals[1] = secTotRate / secTotBal;
                            if (secTotBal != 0) {
                                secondRow += '<td>' + numeral(secTotBal / 1000).format('(0,0)') + '</td>';
                                secondRow += '<td>' + numeral(secTotRate / secTotBal).format('0.0)') + '</td>';
                            } else {
                                secondRow += '<td>-</td>';
                                secondRow += '<td>-</td>';
                            }
                            
                            
                            if (secTotBal != 0) {
                                
                            }
                            else {
                                
                            }
                            
                            secondRow += secTmpRow;

                            if (!zeroLineItem) {
                                $('#' + tbl.tbl + ' tbody').append(secondRow + '</tr>');
                            }
                            
                            clCounter += 1;
                        }

                        if (self.staticGapViewData().viewOption == '1') {
                            $('#' + tbl.tbl + ' tbody').append('<tr class="spacerRow"><td colspan="' + cellCount + '"></td></tr>');
                            row = '<tr class="accountTotalRow"><td class="name">Total ' + cat + '</td>';
                        }
                        else {
                            row = '<tr><td class="name">' + cat + '</td>';
                        }

                        tmpRow = '';
                        totBal = 0;
                        totRate = 0;
                        zeroLineItem = true;

                        for (var i = 2; i < tblData[type].cats[cat].bucketVals.length; i++) { //Skip Totals Doing it Another Way now!
                            if (tblData[type].cats[cat].bucketVals[i] == '-' || tblData[type].cats[cat].bucketVals[i] == 0) {
                                tmpRow += '<td class=' + (i % 2 > 0 ? 'borderRight' : '') + '>-</td>'
                            }
                            else if (i % 2 > 0) {
                                totRate += getTotRateComponent(tblData[type].cats[cat].bucketVals[i], tblData[type].cats[cat].bucketVals[i - 1]);//((tblData[type].cats[cat].bucketVals[i] / (Math.round((tblData[type].cats[cat].bucketVals[i - 1] + 0.00001) * 100) / 100)) * (Math.round((tblData[type].cats[cat].bucketVals[i - 1] + 0.00001) * 100) / 100));
                                tmpRow += '<td class="borderRight">' + numeral(tblData[type].cats[cat].bucketVals[i] / (Math.round((tblData[type].cats[cat].bucketVals[i - 1] + 0.00001) * 100) / 100)).format('(0.00)') + '</td>'
                            }
                            else {
                                let addBal = Math.round((tblData[type].cats[cat].bucketVals[i] + 0.00001) * 100) / 100;
                                zeroLineItem = addBal == 0;

                                totBal += addBal; //tblData[type].cats[cat].bucketVals[i];
                                tmpRow += '<td>' + numeral(tblData[type].cats[cat].bucketVals[i] / 1000).format('(0,0)') + '</td>'
                            }
                        }

                        tblData[type].cats[cat].bucketVals[0] = totBal;
                        tblData[type].cats[cat].bucketVals[1] = totRate / totBal;

                        let vol = numeral(totBal / 1000).format('(0,0)');

                        row += `<td>${vol}</td>`;

                        if (parseInt(vol, 10) === 0) {
                            // ATO-525, if volume is effectively 0, rate must be as well
                            // otherwise you can get gigantic numbers when dividing by tiny numbers
                            row += '<td>0</td>';
                        }
                        else if (totBal !== 0) {
                            row += `<td>${numeral(totRate / totBal).format('0.0)')}</td>`;
                        }
                        else {
                            row += '<td>-</td>';
                        }
                        
                        row += tmpRow;
                        row += '</tr>';

                        if (self.staticGapViewData().viewOption == '1' && ! zeroLineItem) {
                            $('#' + tbl.tbl + ' tbody').append(row);
                            $('#' + tbl.tbl + ' tbody').append('<tr class="spacerRow"><td colspan="' + cellCount + '"></td></tr>');
                        }
                        else if (! zeroLineItem) {
                            $('#' + tbl.tbl + ' tbody').append(row);
                        }
                    }

                    //Add Grand Total For Assets/Liabilities
                    totBal = 0;
                    totRate = 0;
                    tmpRow = '';
                    row = '<tr class="grandTotalRow"><td class="name">Total ' + dispName + '</td>';

                    for (var i = 2; i < tblData[type].bucketVals.length; i++) { //Skip Totals Doing it Another Way now!
                        if (tblData[type].bucketVals[i] == '-' || tblData[type].bucketVals[i] == 0) {
                            tmpRow += '<td class=' + (i % 2 > 0 ? 'borderRight' : '') + '>-</td>'
                        }
                        else if (i % 2 > 0) {
                            totRate += (tblData[type].bucketVals[i] / tblData[type].bucketVals[i - 1]) * tblData[type].bucketVals[i - 1];
                            tmpRow += '<td class="borderRight">' + numeral(tblData[type].bucketVals[i] / tblData[type].bucketVals[i - 1]).format('0.00') + '</td>'
                            tblData[type].bucketVals[i] = (tblData[type].bucketVals[i] / tblData[type].bucketVals[i - 1]);
                        }
                        else {
                            totBal += Math.round((tblData[type].bucketVals[i] + 0.00001) * 100) / 100;
                            tmpRow += '<td>' + numeral(tblData[type].bucketVals[i] / 1000).format('(0,0)') + '</td>'
                        }
                    }

                    tblData[type].bucketVals[0] = totBal;
                    tblData[type].bucketVals[1] = totRate / totBal;
                    row += '<td>' + numeral(totBal / 1000).format('(0,0)') + '</td>';
                    row += '<td>' + numeral(totRate / totBal).format('0.0)') + '</td>';
                    row += tmpRow;
                    $('#' + tbl.tbl + ' tbody').append(row);
                }
    
                if (self.staticGapViewData().gapRatios) {
                    //Now That Regular Data Is Done Display Calculated Gap Rows
                    $('#' + tbl.tbl + ' tbody').append('<tr class="spacerRow"><td colspan="' + cellCount + '"></td></tr>');
                    var gapRow = '<tr style="text-transform: uppercase; font-weight: bold; border-top: 1px solid #e9e9e9;"><td class="name"> Gap (and Rate Spread)</td><td>-</td><td>-</td>';
                    var cumGapRow = '<tr style="text-transform: uppercase; font-weight: bold;"><td class="name">Cumulative Gap (Spread)</td><td>-</td><td>-</td>';
                    var rsaRow = '<tr style="text-transform: uppercase; font-weight: bold;"><td class="name">RSA/RSL</td><td>-</td><td>-</td>';
                    var cumRSARow = '<tr style="text-transform: uppercase; font-weight: bold;"><td class="name">Cumulative RSA/RSL (Spread)</td><td>-</td><td>-</td>';
                    var gapPercRow = '<tr style="text-transform: uppercase; font-weight: bold;"><td class="name">Gap/Total Assets</td><td>-</td><td>-</td>';
                    var cumGapPercRow = '<tr style="text-transform: uppercase; font-weight: bold; border-bottom: 1px solid #e9e9e9;"><td class="name">Cum. Gap/Total Assets</td><td>-</td><td>-</td>';

                    var gapBalDiff = 0;
                    var gapRateDiff = 0;
                    var totalAssetVol = 0;
                    var totalLiabVol = 0;
                    var totalAssetRate = 0;
                    var totalLiabRate = 0;
                    var totalGap = 0;

                    for (var i = 3; i < tblData.assets.bucketVals.length; i += 2) {
                        //Gap Difference Balance             
                        gapBalDiff = tblData.assets.bucketVals[i - 1] - tblData.liabs.bucketVals[i - 1];
                        totalGap += gapBalDiff;
                        //Gap Difference Rate
                        gapRateDiff = tblData.assets.bucketVals[i] - tblData.liabs.bucketVals[i];

                        totalAssetVol += tblData.assets.bucketVals[i - 1];
                        totalLiabVol += tblData.liabs.bucketVals[i - 1];
                        totalLiabRate += tblData.liabs.bucketVals[i - 1] * tblData.liabs.bucketVals[i];
                        totalAssetRate += tblData.assets.bucketVals[i - 1] * tblData.assets.bucketVals[i];

                        gapRow += '<td>' + numeral(gapBalDiff / 1000).format('(0,0)') + '</td><td>' + numeral(gapRateDiff).format('(0.00)') + '</td>';
                        cumGapRow += '<td>' + numeral((totalAssetVol - totalLiabVol) / 1000).format('(0,0)') + '</td><td>' + numeral((totalAssetRate / totalAssetVol) - (totalLiabRate / totalLiabVol)).format('(0.00)') + '</td>';

                        if (tblData.liabs.bucketVals[i - 1] != 0) {
                            rsaRow += '<td>-</td><td>' + numeral(tblData.assets.bucketVals[i - 1] / tblData.liabs.bucketVals[i - 1]).format('(0.00)') + '</td>';
                        }
                        else {
                            rsaRow += '<td>-</td><td>-</td>';
                        }

                        if (totalLiabVol != 0) {
                            cumRSARow += '<td>-</td><td>' + numeral(totalAssetVol / totalLiabVol).format('(0.00)') + '</td>';
                        }
                        else {
                            cumRSARow += '<td>-</td><td>-</td>';
                        }

                        if (tblData.assets.bucketVals[0] != 0) {
                            gapPercRow += '<td>-</td><td>' + numeral(gapBalDiff / tblData.assets.bucketVals[0]).format('(0.00)') + '</td>';
                            cumGapPercRow += '<td>-</td><td>' + numeral(totalGap / tblData.assets.bucketVals[0]).format('(0.00)') + '</td>';
                        }
                        else {
                            gapPercRow += '<td>-</td><td>-</td>';
                            cumGapPercRow += '<td>-</td><td>-</td>';
                        }
                    }

                    $('#' + tbl.tbl + ' tbody').append(gapRow + '</tr>');
                    $('#' + tbl.tbl + ' tbody').append(cumGapRow + '</tr>');
                    $('#' + tbl.tbl + ' tbody').append('<tr class="spacerRow"><td colspan="' + cellCount + '"></td></tr>');
                    $('#' + tbl.tbl + ' tbody').append(rsaRow + '</tr>');
                    $('#' + tbl.tbl + ' tbody').append(cumRSARow + '</tr>');
                    $('#' + tbl.tbl + ' tbody').append('<tr class="spacerRow"><td colspan="' + cellCount + '"></td></tr>');
                    $('#' + tbl.tbl + ' tbody').append(gapPercRow + '</tr>');
                    $('#' + tbl.tbl + ' tbody').append(cumGapPercRow + '</tr>');
                }
            }
            
           // $('#staticgap-view').height($('#staticgap-view').height());
            //global.correctPDFTable('staticgap-view');
            global.loadingReport(false);
            if (typeof hiqPdfInfo != "undefined") {
                hiqPdfConverter.startConversion();
                $('#reportLoaded').text('1');
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getUserProfile(self.profile).then(function(){
                return globalContext.getStaticGapViewDataById(id, self.staticGapViewData).then(function () {
                    globalContext.reportErrors(self.staticGapViewData().errors, self.staticGapViewData().warnings);

                    // only wire up the print handlers if the view option is not 2 or a custom grouping
                    var numericViewOption = parseInt(self.staticGapViewData().viewOption, 10);

                    if (numericViewOption == 0 || numericViewOption == 1) {
                        window.onbeforeprint = () => { reportView.prepareTableForPrint($('#leftTable'), 2, 'page-break', true); };
                        window.onafterprint = reportView.restoreTableFromPrint;
                    }

                    var leftDate = moment(self.staticGapViewData().leftDate).add(1, 'day');
                    var rightDate = moment(self.staticGapViewData().rightDate).add(1, 'day');

                    var leftCellPos = {
                        'One Day': { cell: 3, headerCell: 2 },
                        '4-6 Months': { cell: 11, headerCell: 6 },
                        '7-12 Months': { cell: 13, headerCell: 7 },
                        //'13-24 Months': { cell: 15, headerCell: 8 },
                        //'25-36 Months': { cell: 17, headerCell: 9 },
                        //'37-60 Months': { cell: 19, headerCell: 10}                    
                    };

                    var rightCellPos = {
                        'One Day': { cell: 3, headerCell: 2 },
                        '4-6 Months': { cell: 11, headerCell: 6 },
                        '7-12 Months': { cell: 13, headerCell: 7 },
                        //'13-24 Months': { cell: 15, headerCell: 8 },
                        //'25-36 Months': { cell: 17, headerCell: 9 },
                        //'37-60 Months': { cell: 19, headerCell: 10 }
                    }

                    switch (self.staticGapViewData().schedule) {
                        case '0':
                            leftCellPos['13-24 Months'] = { cell: 15, headerCell: 8 };
                            leftCellPos['25-36 Months'] = { cell: 17, headerCell: 9 };
                            leftCellPos['37-60 Months'] = { cell: 19, headerCell: 10 };
                            leftCellPos['Over 60 Months'] = { cell: 21, headerCell: 11 };

                            rightCellPos['13-24 Months'] = { cell: 15, headerCell: 8 };
                            rightCellPos['25-36 Months'] = { cell: 17, headerCell: 9 };
                            rightCellPos['37-60 Months'] = { cell: 19, headerCell: 10 };
                            rightCellPos['Over 60 Months'] = { cell: 21, headerCell: 11 };
                            break;

                        case '1':
                            leftCellPos['13-36 Months'] = { cell: 15, headerCell: 8 };
                            leftCellPos['37-60 Months'] = { cell: 17, headerCell: 9 };
                            leftCellPos['61-120 Months'] = { cell: 19, headerCell: 10 };
                            leftCellPos['Over 120 Months'] = { cell: 21, headerCell: 11 };

                            rightCellPos['13-36 Months'] = { cell: 15, headerCell: 8 };
                            rightCellPos['37-60 Months'] = { cell: 17, headerCell: 9 };
                            rightCellPos['61-120 Months'] = { cell: 19, headerCell: 10 };
                            rightCellPos['Over 120 Months'] = { cell: 21, headerCell: 11 };

                            break;

                        case '2':
                            leftCellPos['13-36 Months'] = { cell: 15, headerCell: 8 };
                            leftCellPos['37-60 Months'] = { cell: 17, headerCell: 9 };
                            leftCellPos['61-120 Months'] = { cell: 19, headerCell: 10 };
                            leftCellPos['121-180 Months'] = { cell: 21, headerCell: 11 };
                            leftCellPos['Over 180 Months'] = { cell: 23, headerCell: 12 };

                            rightCellPos['13-36 Months'] = { cell: 15, headerCell: 8 };
                            rightCellPos['37-60 Months'] = { cell: 17, headerCell: 9 };
                            rightCellPos['61-120 Months'] = { cell: 19, headerCell: 10 };
                            rightCellPos['121-180 Months'] = { cell: 21, headerCell: 11 };
                            rightCellPos['Over 180 Months'] = { cell: 23, headerCell: 12 };

                            break;

                        case '3':
                            leftCellPos = {
                                'One Day': { cell: 3, headerCell: 2 },
                                '4-6 Months': { cell: 9, headerCell: 5 },
                                '7-12 Months': { cell: 11, headerCell: 6 },
                                '13-24 Months': { cell: 13, headerCell: 7 },
                                '25-60 Months': { cell: 15, headerCell: 8 },
                                'Over 60 Months': { cell: 17, headerCell: 9 },
                            };

                            rightCellPos = {
                                'One Day': { cell: 3, headerCell: 2 },
                                '4-6 Months': { cell: 9, headerCell: 5 },
                                '7-12 Months': { cell: 11, headerCell: 6 },
                                '13-24 Months': { cell: 13, headerCell: 7 },
                                '25-60 Months': { cell: 15, headerCell: 8 },
                                'Over 60 Months': { cell: 17, headerCell: 9 },
                            };

                            break;
                    }

                    if (self.staticGapViewData().schedule != "3") {
                        leftCellPos[leftDate.add(2, 'month').format('MMM-YY')] = { cell: 9, headerCell: 5 };
                        leftCellPos[leftDate.add(-1, 'month').format('MMM-YY')] = { cell: 7, headerCell: 4 };
                        leftCellPos[leftDate.add(-1, 'month').format('MMM-YY')] = { cell: 5, headerCell: 3 };

                        rightCellPos[rightDate.add(2, 'month').format('MMM-YY')] = { cell: 9, headerCell: 5 };
                        rightCellPos[rightDate.add(-1, 'month').format('MMM-YY')] = { cell: 7, headerCell: 4 };
                        rightCellPos[rightDate.add(-1, 'month').format('MMM-YY')] = { cell: 5, headerCell: 3 };
                    } else {
                        leftCellPos[leftDate.add(2, 'month').format('MMM-YY')] = { cell: 7, headerCell: 4 };
                        leftCellPos[leftDate.add(-1, 'month').format('MMM-YY')] = { cell: 5, headerCell: 3 };
                        leftCellPos[leftDate.add(-1, 'month').format('MMM-YY')] = { cell: 3, headerCell: 2 };

                        rightCellPos[leftDate.add(2, 'month').format('MMM-YY')] = { cell: 7, headerCell: 4 };
                        rightCellPos[leftDate.add(-1, 'month').format('MMM-YY')] = { cell: 5, headerCell: 3 };
                        rightCellPos[leftDate.add(-1, 'month').format('MMM-YY')] = { cell: 3, headerCell: 2 };
                    }

                    self.leftCellPos = leftCellPos;
                    self.rightCellPos = rightCellPos;
                });
            });
        };
    };

    return staticGapVm;
});
