﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var sumResultsVm = function (id) {
        var self = this;
        this.sumResults = ko.observable();

        this.compositionComplete = function () {
            global.loadingReport(false);
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
                $('#reportLoaded').text('1');
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getSummaryOfResultsViewDataById(id, self.sumResults).then(function () {
                var sumResults = self.sumResults();

                if (!sumResults) return;

                globalContext.reportErrors(sumResults.errors, sumResults.warnings);
            });
        };
    };

    return sumResultsVm;

});
