﻿/// <reference path="reportview.js" />
define([
    'services/globalcontext',
    'plugins/router',
    'viewmodels/onechartview',
    'viewmodels/twochartview',
    'viewmodels/simcompareview',
    'viewmodels/eveview',
    'viewmodels/documentview',
    'viewmodels/balancesheetcompareview',
    'viewmodels/balancesheetmixview',
    'viewmodels/basicsurplusreportview',
    'viewmodels/interestratepolicyguidelinesview',
    'viewmodels/fundingmatrixview',
    'viewmodels/staticgapview',
    'viewmodels/corefundingutilizationview',
    'viewmodels/simulationsummaryview',
    'viewmodels/yieldcurveshiftview',
    'viewmodels/coverpageview',
    'viewmodels/loancapfloorview',
    'viewmodels/cashflowreportview',
    'viewmodels/netcashflowview',
    'viewmodels/summaryrateview',
    'viewmodels/asc825balancesheetview',
    'viewmodels/prepaymentdetailsview',
    'viewmodels/asc825datasourceview',
    'viewmodels/asc825discountrateview',
    'viewmodels/asc825worksheetview',
    'viewmodels/asc825marketvalueview',
    'viewmodels/ratechangematrixview',
    'viewmodels/investmentportfoliovaluationview',
    'viewmodels/investmentdetailview',
    'viewmodels/investmenterrorview',
    'viewmodels/historicalbalancesheetniiview',
    'viewmodels/asc825assumptionmethodsview',
    'viewmodels/evenevassumptionsview',
    'viewmodels/assumptionmethodsview',
    'viewmodels/liabilitypricinganalysisview',
    'viewmodels/liquidityprojectionview',
    'viewmodels/detailedsimulationassumptionview',
    'viewmodels/summaryofresultsview',
    'viewmodels/timedepositmigrationview',
    'viewmodels/capitalanalysisview',
    'viewmodels/sectionpageview',
    'viewmodels/lookbackreportview',
    'viewmodels/marginalcostoffundsview',
    'viewmodels/executiverisksummaryview',
    'viewmodels/inventoryliquidityresourcesview',
    'services/global',
    'durandal/app'
],
    function (
        globalContext,
        router,
        OneChartView,
        TwoChartView,
        SimCompareView,
        EveView,
        DocmumentView,
        BalanceSheetCompareView,
        BalanceSheetMixView,
        BasicSurplusReportView,
        InterestRatePolicyGuidelinesView,
        FundingMatrixView,
        StaticGapView,
        CoreFundingUtilizationView,
        SimulationSummaryView,
        YieldCurveShiftView,
        CoverPageView,
        LoanCapFloorView,
        CashflowReportView,
        NetCashflowView,
        SummaryRateView,
        ASC825BalanceSheetView,
        PrepaymentDetailsView,
        ASC825DataSourceView,
        ASC285DiscountRateView,
        ASC285WorksheetView,
        ASC285MarketValueView,
        RateChangeMatrixView,
        InvestmentPortfolioValuationView,
        InvestmentDetailView,
        InvestmentErrorView,
        HistoricalBalanceSheetNiiView,
        ASC825AssumptionMethodsView,
        EveNevAssumptionsView,
        AssumptionMethodsView,
        LiabilityPricingAnalysisView,
        LiquidityProjectionView,
        DetailedSimulationAssumptionView,
        SummaryOfResultsView,
        TimeDepositMigrationView,
        CapitalAnalysisView,
        SectionPageView,
        LookbackReportView,
        MarginalCostOfFundsView,
        ExecutiveRiskSummaryView,
        InventoryLiquidityResourcesView,
        global,
        app
    ) {
        var vm = {};
        var thisReport = ko.observable();
        var thisReportView = ko.observable();
        var nextReport = ko.observable();
        var previousReport = ko.observable();
        var reportVm = ko.observable();
        var profile = ko.observable();
        var oldLabels;
        var hasRunLabels = false;


        function realignLabels(serie) {
            var innerPadding = 4;
            var chart = serie.chart;
            var borderWidth = chart.options.plotOptions.bar.borderWidth;

            $.each(serie.points, function (j, point) {
                if (!point.dataLabel) return true;

                var yCoord = point.dataLabel.y;
                var color = '#ffffff';

                if (point.shapeArgs.height <= point.dataLabel.width + (innerPadding * 2)) {
                    yCoord += (point.shapeArgs.width / 2) + point.dataLabel.height / 2;
                    color = 'rgb(96,96,96)';
                }
                point.dataLabel.attr({
                    y: yCoord
                }).css({
                    color: color,
                    fontSize: '13px'
                });
            });

            var n = 0;
        }

        function activate(id) {
            thisReportView(null);
            oldLabels = Highcharts.Series.prototype.drawDataLabels;

            return globalContext.getUserProfile(profile).then(function () {
                return globalContext.getReportById(parseInt(id), thisReport).fin(function () {
                    console.log(thisReport());
                    return Q.all([
                        globalContext.getNextReport(thisReport, nextReport),
                        globalContext.getPreviousReport(thisReport, previousReport),
                        getViewVm(thisReport)
                    ]).then(function () {
                        try {
                            if (thisReport().shortName() == 'InventoryLiquidityResources' || thisReport().shortName() == 'AssumptionMethods') {
                                $('#footnoteContainer').hide();
                            }
                            else {
                                $('#footnoteContainer').show();
                            }
                        }
                        catch (err) {
                            
                        }
                        });
                });
            });
        }

        function compositionComplete() {
            //Hide footnotes here!
            if (thisReport().shortName() == 'InventoryLiquidityResources' || thisReport().shortName() == 'AssumptionMethods') {
                $('#footnoteContainer').hide();
            }
            else {
                $('#footnoteContainer').show();
            }
        }


    function getViewVm(report) {
        //set the highcharts datalabel function to move the labels for corefunding
        if (thisReport().shortName() === "CoreFundingUtilization") {
            Highcharts.Series.prototype.drawDataLabels = (function (func) {
                return function () {
                    func.apply(this, arguments);
                    if ((this.options.dataLabels.enabled || this._hasPointLabels) && !hasRunLabels) {
                        realignLabels(this);
                        hasRunLabels = false;
                    }
                };
            }(Highcharts.Series.prototype.drawDataLabels));
        }

        console.log(thisReport().shortName());
        if (thisReport().shortName() === "OneChart") {
            thisReportView(new OneChartView(report().id()));
        }
        else if (thisReport().shortName() === "TwoChart") {
            thisReportView(new TwoChartView(report().id()));
        }
        else if (thisReport().shortName() === "SimCompare") {
            thisReportView(new SimCompareView(report().id()));
        }
        else if (thisReport().shortName() === "Eve") {
            thisReportView(new EveView(report().id()));
        }
        else if (thisReport().shortName() === "Document") {
            thisReportView(new DocmumentView(report().id()));
        }
        else if (thisReport().shortName() === "Document") {
            thisReportView(new DocmumentView(report().id()));
        }
        else if (thisReport().shortName() === "BalanceSheetCompare") {
            thisReportView(new BalanceSheetCompareView(report().id(), vm));
        }
        else if (thisReport().shortName() === "BalanceSheetMix") {
            thisReportView(new BalanceSheetMixView(report().id()));
        }
        else if (thisReport().shortName() === "BasicSurplusReport") {
            thisReportView(new BasicSurplusReportView(report().id()));
        }
        else if (thisReport().shortName() === "InterestRatePolicyGuidelines") {
            thisReportView(new InterestRatePolicyGuidelinesView(report().id()));
        }
        else if (thisReport().shortName() === "StaticGap") {
            thisReportView(new StaticGapView(report().id(), vm));
        }
        else if (thisReport().shortName() === "FundingMatrix") {
            thisReportView(new FundingMatrixView(report().id()));
        }
        else if (thisReport().shortName() === "TimeDepositMigration") {
            thisReportView(new TimeDepositMigrationView(report().id()));
        }
        else if (thisReport().shortName() === "CoreFundingUtilization") {
            thisReportView(new CoreFundingUtilizationView(report().id()));
        }
        else if (thisReport().shortName() === "YieldCurveShift") {
            thisReportView(new YieldCurveShiftView(report().id()));
        }
        else if (thisReport().shortName() === "SimulationSummary") {
            thisReportView(new SimulationSummaryView(report().id()));
        }
        else if (thisReport().shortName() === "CoverPage") {
            thisReportView(new CoverPageView(report().id()));
        }
        else if (thisReport().shortName() === "LoanCapFloor") {
            thisReportView(new LoanCapFloorView(report().id(), vm));
        }
        else if (thisReport().shortName() === "CashflowReport") {
            thisReportView(new CashflowReportView(report().id()));
        }
        else if (thisReport().shortName() === "NetCashflow") {
            thisReportView(new NetCashflowView(report().id()));
        }
        else if (thisReport().shortName() === "SummaryRate") {
            thisReportView(new SummaryRateView(report().id()));
        }
        else if (thisReport().shortName() === "ASC825BalanceSheet") {
            thisReportView(new ASC825BalanceSheetView(report().id()));
        }
        else if (thisReport().shortName() === "PrepaymentDetails") {
            thisReportView(new PrepaymentDetailsView(report().id()));
        }
        else if (thisReport().shortName() === "ASC825DataSource") {
            thisReportView(new ASC825DataSourceView(report().id()));
        }
        else if (thisReport().shortName() === "ASC825DiscountRate") {
            thisReportView(new ASC285DiscountRateView(report().id()));
        }
        else if (thisReport().shortName() === "ASC825Worksheet") {
            thisReportView(new ASC285WorksheetView(report().id()));
        }
        else if (thisReport().shortName() === "ASC825MarketValue") {
            thisReportView(new ASC285MarketValueView(report().id()));
        }
        else if (thisReport().shortName() === "RateChangeMatrix") {
            thisReportView(new RateChangeMatrixView(report().id()));
        }
        else if (thisReport().shortName() === "InvestmentPortfolioValuation") {
            thisReportView(new InvestmentPortfolioValuationView(report().id()));
        }
        else if (thisReport().shortName() === "InvestmentDetail") {
            thisReportView(new InvestmentDetailView(report().id()));
        }
        else if (thisReport().shortName() === "InvestmentError") {
            thisReportView(new InvestmentErrorView(report().id()));
        }
        else if (thisReport().shortName() === "HistoricalBalanceSheetNII") {
            thisReportView(new HistoricalBalanceSheetNiiView(report().id()));
        }
        else if (thisReport().shortName() === "ASC825AssumptionMethods") {
            thisReportView(new ASC825AssumptionMethodsView(report().id()));       
        }
        else if (thisReport().shortName() === "EveNevAssumptions") {
            thisReportView(new EveNevAssumptionsView(report().id()));
        }
        else if (thisReport().shortName() === "AssumptionMethods") {
            thisReportView(new AssumptionMethodsView(report().id(), thisReport));
        }
        else if (thisReport().shortName() === "LiabilityPricingAnalysis") {
            thisReportView(new LiabilityPricingAnalysisView(report().id()));
        }
        else if (thisReport().shortName() === "LiquidityProjection") {
            thisReportView(new LiquidityProjectionView(report().id()));
        }    
        else if (thisReport().shortName() === "DetailedSimulationAssumption") {
            thisReportView(new DetailedSimulationAssumptionView(report().id()));
        }
        else if (thisReport().shortName() === "SummaryOfResults") {
            thisReportView(new SummaryOfResultsView(report().id()));       
        }
        else if (thisReport().shortName() === "CapitalAnalysis") {
            thisReportView(new CapitalAnalysisView(report().id()));
        }
        else if (thisReport().shortName() === "SectionPage") {
            thisReportView(new SectionPageView(report().id(), report().name()));
        }
        else if (thisReport().shortName() === "LookbackReport") {
            thisReportView(new LookbackReportView(report().id()));
        }
        else if (thisReport().shortName() === "MarginalCostOfFunds") {
            thisReportView(new MarginalCostOfFundsView(report().id()));
        }
        else if (thisReport().shortName() === "ExecutiveRiskSummary") {
            thisReportView(new ExecutiveRiskSummaryView(report().id()));
        }
        else if (thisReport().shortName() == 'InventoryLiquidityResources') {
            //This is sturcuted like the config becuase all data resides in config(kinda)(its complicated)
            var ers = ko.observable();
            return globalContext.getInventoryLiquidityResourcesById(thisReport().id(), ers).then(function () {
                thisReportView(new InventoryLiquidityResourcesView(ers, thisReport));
                return;
            });
        }
    }

    function prepareTableForPrint(tableEl, sizingHeaderRowIndex, pageBreakClass, breakHeaderOut) {
        function safeClone(el, shallow) {
            return $(el.cloneNode(shallow)).removeAttr('id');
        }

        var sourceTable = $(tableEl);
        var pageBreakSections = $('.' + pageBreakClass, sourceTable);

        if (!pageBreakSections.length) return;

        sourceTable.css({ 'table-layout': 'fixed' });
        var sourceHeaders = $('thead > tr:nth-child(' + sizingHeaderRowIndex + ') > th', sourceTable);
        var sourceHeaderWidths = [];

        sourceHeaders.each((idx, th) => {
            var width = $(th).width();
            console.log(width);
            sourceHeaderWidths.push(width);
            // set the header column widths on the original table
            $(th).css({ width: width + 'px' }).attr('data-print-style', 'true')
        });

        var currentTable = null;

        function cloneTable(hasPageBreak) {
            var result = $('<table data-print-generated="true" id="secondTable"></table>');
            result.attr('class', sourceTable.attr('class'));
            result.css({ 'table-layout': 'fixed' });

            if (hasPageBreak) {
                result.css({ 'page-break-before': 'always' });
            }

            return result;
        }

        // build new table(s)
        // this is done in two phases due to a bug with puppeteer/chromium
        // the first table contains only the header rows, the second contains only the body rows
        for (var i = 0; i <= pageBreakSections.length; i++) {
            if (currentTable != null) {
                sourceTable.parent().append(currentTable);
                currentTable = null;
            }

            if (i == pageBreakSections.length) break;

            currentTable = cloneTable(true);

            // deep clone the table header
            currentTable.append(safeClone($('thead', sourceTable)[0], true));

            // set the widths of the header columns
            $('thead > tr:nth-child(' + sizingHeaderRowIndex + ') > th', currentTable).each((idx, th) => {
                $(th).css({ width: sourceHeaderWidths[idx] + 'px' });
            });

            let firstTH = $('thead > tr:eq(0) > th:eq(0)', currentTable);

            sourceTable.parent().append(currentTable);

            // when breakHeaderOut is true, a new table must be created for the tbody
            if (breakHeaderOut) {
                currentTable = cloneTable();
            }

            // shallow clone the table body
            var tbody = safeClone($('tbody:nth-child(1)')[0], false);

            currentTable.append(tbody);

            if (breakHeaderOut) {
                let ltr = $('<tr class="spacerRow"></tr>');

                // table-layout: fixed only looks at the widths of the first row, so we need to shim a blank row in first
                for (let j = 0; j < sourceHeaderWidths.length; j++) {

                    ltr.append($('<td></td>').css({ width: (sourceHeaderWidths[j]) + 'px' }));
                }

                tbody.append(ltr);
            }

            // clone the rows in between and including page sections
            $(pageBreakSections[i]).nextUntil('.' + pageBreakClass).andSelf().each((idx, tr) => {
                tbody.append(safeClone(tr, true));
                $(tr).attr('data-print-hidden', true).hide();
            });


            if (!breakHeaderOut) continue;

            // todo jdk - this needs work

            firstTH.width($('tbody > tr:eq(0) > td:eq(0)', currentTable).outerWidth() + 'px');


            var r = -1;
            console.log(sourceHeaderWidths);
            while (row = currentTable[0].rows[r++]) {
                console.log('IN HEEERE');
                var c = -1;
                while (cell = row.cells[c++]) {
                    cell.style.width = sourceHeaderWidths[c] + 'px';
                }
            }
        }




        return sourceHeaderWidths;
    }

    function restoreTableFromPrint() {
        $('*[data-print-hidden=true]').removeAttr('data-print-hidden').show();
        $('*[data-print-generated=true').remove();
        $('*[data-print-style=true').removeAttr('style');
    }

    var deactivatePrintHandlers = function () {
        if (window.onafterprint) {
            window.onafterprint();
        }

        window.onbeforeprint = null;
        window.onafterprint = null;
    }

    var deactivate = function () {
        Highcharts.Series.prototype.drawDataLabels = oldLabels;
        deactivatePrintHandlers();
    };

    function next() {
        deactivatePrintHandlers();
        activate(nextReport().repId);
    }

    function prev() {
        deactivatePrintHandlers();
        activate(previousReport().repId);
    }

    function goToReportConfig() {
         router.navigate('#/reportconfig/' + thisReport().id());
    }

    var goToPdf = function () {
        globalContext.startReportPdfProcess(thisReport().id());
        global.pdfInProgress(true);
        global.pdfType('report');
        global.pdfReportId(thisReport().id());
        global.pdfPackage(thisReport().packageId());
        app.trigger('application:pdfStart');
    };

    var goToPackageConfig = function () {
        router.navigate("#/packageconfig/" + thisReport().packageId());
    };

    vm.title = 'Report View';
    vm.thisReport = thisReport;
    vm.thisReportView = thisReportView;
    vm.nextReport = nextReport;
    vm.previousReport = previousReport;
    vm.reportVm = reportVm;
    vm.activate = activate;
    vm.deactivate = deactivate;
    vm.compositionComplete = compositionComplete;
    vm.next = next;
    vm.prev = prev;
    vm.goToReportConfig = goToReportConfig;
    vm.goToPdf = goToPdf;
    vm.goToPackageConfig = goToPackageConfig;
    vm.profile = profile;
    vm.prepareTableForPrint = prepareTableForPrint;
    vm.restoreTableFromPrint = restoreTableFromPrint;
    vm.global = global;

    return vm;
});