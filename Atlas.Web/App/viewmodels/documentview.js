﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ctor = function (id) {
        var self = this;
        this.documentId = id;
        this.document = ko.observable();

        this.computedHTML = ko.computed(() => {
            if (!self.document()) return '';

            var source = $(self.document().htmlSource());

            if (source.length > 1) {
                // wrap legacy document sources
                source = $(`<div>${self.document().htmlSource()}</div>`);
            }

            var result = $('div[data-orientation=both]', source);

            if (result.length > 0) return result.html();

            result = $(`div[data-orientation=${self.document().orientation()}]`, source);

            if (result.length > 0) return result.html();

            return source.html() || '';
        });
    };

    ctor.prototype.compositionComplete = function () {
        if (this.document().orientation() == 'Portrait') {
            $('#documentContainer').attr('data-pdf-orientation', 'portrait');
        }

        global.loadingReport(false);
        $('#reportLoaded').text('1');

        //Export To Pdf
        if (typeof hiqPdfInfo == "undefined") {
            globalContext.logEvent("Viewed", "DocumentView"); 
        }
        else {
            hiqPdfConverter.startConversion();
        }
    };

    ctor.prototype.activate = function () {
         
        global.loadingReport(true);
        var self = this;
        return globalContext.getDocumentViewById(self.documentId, self.document)
            .then(function () {
            });
    };

    return ctor;
});
