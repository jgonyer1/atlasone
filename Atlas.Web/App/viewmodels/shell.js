﻿define([
    'plugins/router',
    'plugins/dialog',
    'durandal/app',
    'services/globalcontext',
    'services/model',
    'services/global',
    'plugins/history'
],
function (router, dialog, app, globalContext, model, global, history) {
    var version = ko.observable();

    return {
        router: router,
        version: version,
        model: model,
        global: global,
        app: app,
        compositionComplete: function () {
            $("#shell_wrapper").click(function (e) {
                var id = e.target.id;
                var urlFrag = history.getFragment();
                if (urlFrag.length == 0) {
                    urlFrag = "landing";
                }
                if (id == null || id == undefined || id.length == 0){
                    id = e.target.getAttribute("data-log-id");
                }
                if (id == null || id == undefined || id.length == 0) {
                    //do nothing
                } else {
                    globalContext.logEvent("Clicked element " + id, urlFrag);
                    //console.log("SHELL CLICK GRAB TARGET: " + id + ": Page? " + history.getHash() + " ; fragment? " + history.getFragment());
                }
                //console.log("SHELL CLICK GRAB TARGET: " + e.target.id + ": Page? " + history.getHash() + " ; fragment? " + history.getFragment());
            });
            function pdfPoll() {
                globalContext.getPDFStatus(global.pdfPackage(), global.pdfStatus).then(function () {
                    var status = global.pdfStatus();
                    if (! status) return;

                    if (status.isSuccess && status.reportName) {
                        $('#exportStatus').text(`Exporting ${status.reportName}`);
                    }
                    else {
                        $('#exportStatus').text(status.message);
                    }

                    if (status.reportIndex) {
                        $('#pdfExportProgress').show();
                        $('#pdfExportProgress > .progress-bar').css({ 'width': `${status.reportIndex * 100 / status.totalReportCount}%` });
                    }
                    else {
                        $('#pdfExportProgress').hide();
                    }
                });
            }

            //This interval will poll server every second for status of PDF export
            var pdfInterval;// = setInterval(function () { pdfPoll() }, 1000);

            //Fires When user changes settings
            app.on('application:pdfStart').then(function () {
                $('#exportStatus').text('Contacting Server');
                $("#pdfExporter").modal('show')     
                $('#pdfExportProgress').hide();
                pdfInterval = setInterval(function () { pdfPoll() }, 1000);
            });

            app.on('application:pdfFailed').then(function (error) {
                clearInterval(pdfInterval);
                $("#pdfExporter").modal('hide')

                if (error.reportName == 'undefined' || error.reportName === undefined) { error.reportName = null; }
                if (error.message == 'undefined' || error.message === undefined) { error.message = null; }

                dialog.showMessage(`PDF export failed because ${`<b>${error.reportName}</b>` || 'one or more reports'} could not be rendered.<br/><br/><span class="error">${error.message || ''}</span>`, 'PDF Export Failed', ['OK']);
            });

            app.on('application:pdfFinished').then(function (finalStatus) {
                clearInterval(pdfInterval);
                $("#pdfExporter").modal('hide')
                var url = "";
                if (global.pdfType() == 'package') {
                    url = "package/pdf/" + global.pdfPackage();
                }
                else {
                    url = "report/pdf/" + global.pdfReportId();
                }

                //Add redirect to load up package pdf
               
              //  router.navigate('packageconfig/' + global.pdfPackage() + '?pdf=' + Math.random());
                window.open(url, '_blank');
            });
        },
        activate: function () {
          //  globalContext.getCodeVersion(version);

            Highcharts.setOptions({
                chart: {
                    style: {
                        fontWeight: 'bold',
                        fontSize: '10px',
                        fontFamily: "'Open Sans', sans-serif"
                    }

                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        animation: false
                    }
                },
                yAxis: {
                    labels: {
                        style: {
                            color: '#000',
                            fontWeight: 'bold',
                            fontSize: '10px',
                            fontFamily: "'Open Sans', sans-serif"
                        }
                    }
                },
                xAxis: {
                    labels: {
                        style: {
                            color: '#000',
                            fontWeight: 'bold',
                            fontSize: '10px',
                            fontFamily: "'Open Sans', sans-serif"
                        }
                    }
                },
                legend: {
                    itemStyle: {
                        fontSize: '10px',
                        font: "bold 10px 'Open Sans', sans-serif"
                    },
                    symbolRadius: 40
                },
                lang: {
                    numericSymbols: ['K', 'M', 'B', 'T', 'P', 'E'],
                    thousandsSep: '' // default is space
                }
            });

            // ATO-637 - prevent exit if there are unsaved changes
            // add event listener is prolematic here
            window.onbeforeunload = () => {
                if (!globalContext.hasChanges()) return; // must be undefined/void
                return "There are unsaved changes."; // this message will get ignored by most browsers
            };

            dialog.MessageBox.setDefaults({ style: {width: "auto"}});

            return router
                .makeRelative({ moduleId: 'viewmodels' })
                .map([
                    { route: '', moduleId: 'landing', title: 'landing', nav: false },
                    { route: 'htmltoimage', moduleId: 'htmltoimage', title: 'htmltoimage', nav: false },
                    { route: 'about', moduleId: 'about', title: 'About', nav: false },
                    { route: 'packages', moduleId: 'packages', title: 'packages', nav: false },
                    { route: 'packageconfig/:id', moduleId: 'packageconfig', title: 'packageconfig', nav: false },
                    { route: 'packagecopy/:id', moduleId: 'packagecopy', title: 'packagecopy', nav: false },
                    { route: 'onechartconfig/:id', moduleId: 'onechartconfig', title: 'onechartconfig', nav: false },
                    { route: 'onechartadd', moduleId: 'onechartadd', title: 'onechartadd', nav: false },
                    { route: 'onechartview/:id', moduleId: 'onechartview', title: 'onechartview', nav: false },
                    { route: 'twochartconfig/:id', moduleId: 'twochartconfig', title: 'twochartconfig', nav: false },
                    { route: 'twochartadd', moduleId: 'twochartadd', title: 'twochartadd', nav: false },
                    { route: 'twochartview/:id', moduleId: 'twochartview', title: 'twochartview', nav: false },
                    { route: 'simcompareconfig/:id', moduleId: 'simcompareconfig', title: 'simcompareconfig', nav: false },
                    { route: 'simcompareadd', moduleId: 'simcompareadd', title: 'simcompareadd', nav: false },
                    { route: 'loancapfloorconfig/:id', moduleId: 'loancapfloorconfig', title: 'loancapfloorconfig', nav: false },
                    { route: 'loancapflooradd', moduleId: 'loancapflooradd', title: 'loancapflooradd', nav: false },
                    { route: 'simcompareview/:id', moduleId: 'simcompareview', title: 'simcompareview', nav: false },
                    { route: 'eveconfig/:id', moduleId: 'eveconfig', title: 'eveconfig', nav: false },
                    { route: 'eveadd', moduleId: 'eveadd', title: 'eveadd', nav: false },
                    { route: 'eveview/:id', moduleId: 'eveview', title: 'eveview', nav: false },
                    { route: 'documentconfig/:id', moduleId: 'documentconfig', title: 'documentconfig', nav: false },
                    { route: 'documentadd', moduleId: 'documentadd', title: 'documentadd', nav: false },
                    { route: 'documentview/:id', moduleId: 'documentview', title: 'documentview', nav: false },
                    { route: 'reportview/:id', moduleId: 'reportview', title: 'reportview', nav: false },
                    { route: 'reportconfig/:id', moduleId: 'reportconfig', title: 'reportconfig', nav: false },
                    { route: 'reportadd', moduleId: 'reportadd', title: 'reportconfig', nav: false },
                    { route: 'files/:id', moduleId: 'files', title: 'files', nav: false },

                    //Basic Surplus Stuff
                    { route: 'basicsurplusadmin/:id', moduleId: 'basicsurplusadmin', title: 'basicsurplusadmin', nav: false },
                    { route: 'basicsurplustypeconfig/:id', moduleId: 'basicsurplustypeconfig', title: 'basicsurplustypeconfig', nav: false },

                    //Eve Config junk
                    { route: 'eveadminconfig/:id', moduleId: 'eveadminconfig', title: 'eveadminconfig', nav: false },

                    //Defined Groupings Stuff
                    { route: 'definedgroupingsconfig/:id', moduleId: 'definedgroupingsconfig', title: 'definedgroupingsconfig', nav: false },
                    { route: 'definedgroupinggroupadd/:id', moduleId: 'definedgroupinggroupadd', title: 'definedgroupingroupadd', nav: false },
                    { route: 'definedgroupinggroupconfig/:id', moduleId: 'definedgroupinggroupconfig', title: 'definedgroupingroupconfig', nav: false },

                    //Look Back Stuff
                    { route: 'lookbackcustomlayoutconfig/:id', moduleId: 'lookbackcustomlayoutconfig', title: 'lookbackcustomlayoutconfig', nav: false },
                    { route: 'lookbackconfig/:id', moduleId: 'lookbackconfig', title: 'lookbackconfig', nav: false },
                ]).buildNavigationModel()
                .mapUnknownRoutes()
                .activate();
        }
    };
});
