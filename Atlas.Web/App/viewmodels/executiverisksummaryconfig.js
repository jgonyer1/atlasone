﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
    function (globalContext, global, app) {
        var ca = ko.observable();
        var vm = function (ers) {
            var self = this;
            this.ers = ers;

            function onChangesCanceled() {
                setOrResetChanges(true);
            }
            var curPage = "ExecRiskSummaryConfig";
            //observables and observableArrays
            this.institutions = ko.observableArray();
            this.simulationTypes = ko.observableArray();
            this.basicSurplusAdminOffsets = ko.observableArray();
            this.policyOffsets = ko.observableArray();
            this.scenarioTypes = ko.observableArray();
            this.modelOffsets = ko.observableArray();

            this.currentRiskAss = ko.observableArray();

            this.sortedHistoricalOffSets = ko.observableArray();
            this.sortedMasters = ko.observableArray();

            this.liqParentSections = ko.observableArray(["Basic Surplus", "Other Liquidity Measures", "Custom"]);

            this.riskAssesmentOptions = ko.observableArray([{ name: "None", value: 0 }, { name: "Low", value: 1 }, { name: "Low-Moderate", value: 2 }, { name: "Moderate", value: 3 }, { name: "Moderate-High", value: 4 }, { name: "High", value: 5 }]);

            this.allSectionTypes = ko.observable();

            this.asOfDateOffsets = ko.observableArray();

            this.sortMasters = function (l) {
                this.sortedMasters(this.sortedMasters().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            // Liquidity Section
            this.leftLiqSections = ko.observableArray();

            this.rightLiqSections = ko.observableArray();

            this.sortedRightLiqSections = ko.observableArray();

            this.sortedLeftLiqSections = ko.observableArray();

            this.addLeftLiquidity = function () {
                self.addLiqSection('Left');
            };

            this.addRightLiquidity = function () {
                self.addLiqSection('Right');
            };

            this.addLiqSection = function (side) {
                var liqSection = globalContext.createExecRiskSummaryLiqSection();
                liqSection.reportSide(side);

                if (side == "Right") {
                    liqSection.priority(self.rightLiqSections().length);
                }
                else {
                    liqSection.priority(self.leftLiqSections().length);
                }

                // before I add a section add property of sortable details
                liqSection.sortedDetails = ko.observableArray();

                globalContext.logEvent("Added" + side + " liq section to ERS " + self.ers().id(), curPage);

                self.ers().executiveRiskSummaryLiquiditySections.push(liqSection);

                self.reseperatreAndSortLiq();
            };

            this.reseperatreAndSortLiq = function () {
                self.rightLiqSections(self.ers().executiveRiskSummaryLiquiditySections().filter(function (item, arr, index) { return item.reportSide() == "Right" }));
                self.leftLiqSections(self.ers().executiveRiskSummaryLiquiditySections().filter(function (item, arr, index) { return item.reportSide() == "Left" }));

                self.sortedRightLiqSections(self.rightLiqSections().sort(function (a, b) { return a.priority() - b.priority(); }));
                self.sortedLeftLiqSections(self.leftLiqSections().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.removeLiqSectionDet = function () {
                var msg = 'Delete ' + this.name() + ' Detail?';
                var det = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        globalContext.logEvent("Removed liq section detail" + this.name() + " from ERS " + self.ers().id(), curPage);
                        det.entityAspect.setDeleted();

                        //Find parent and resort it
                        var parent = self.ers().executiveRiskSummaryLiquiditySections().filter(function (item, arr, index) { return item.id() == det.executiveRiskSummaryLiquiditySectionId() })[0]

                        // reindex based off of one that has been removed
                        parent.executiveRiskSummaryLiquiditySectionDets().forEach(function (d, index) {
                            d.priority(index);
                        });

                        self.sortLiqDetails(parent);
                    }
                }
            };

            this.removeLiqSection = function () {
                var msg = 'Delete ' + this.reportSectionTypeName() + ' Section?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption !== 'Yes') return;
                    globalContext.logEvent("Removed liq section " + this.reportSectionTypeName() + " from ERS " + self.ers().id(), curPage);
                    var detsLen = item.executiveRiskSummaryLiquiditySectionDets().length;

                    for (var d = detsLen - 1; d >= 0; d--) {
                        item.executiveRiskSummaryLiquiditySectionDets()[d].entityAspect.setDeleted();
                    }

                    item.entityAspect.setDeleted();
                    self.reseperatreAndSortLiq();

                    // reindex based off of one that has been removed
                    self.rightLiqSections().forEach(function (d, index) {
                        d.priority(index);
                    });

                    self.leftLiqSections().forEach(function (d, index) {
                        d.priority(index);
                    });
                }
            };

            this.addLiqSectionDet = function () {
                var newLiqSectionDet = globalContext.createExecutiveRiskSummaryLiquiditySectionDet();

                // TODO GET PARENT ITEM AND COUNT for now just set to 1000
                newLiqSectionDet.priority(this.executiveRiskSummaryLiquiditySectionDets().length);
                globalContext.logEvent("Added liq section detail to ERS " + self.ers().id(), curPage);
                this.executiveRiskSummaryLiquiditySectionDets.push(newLiqSectionDet);
                self.sortLiqDetails(this);
                self.resetRisks();
            }

            this.sortLiqDetails = function (l) {
                l.sortedDetails(l.executiveRiskSummaryLiquiditySectionDets().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            // IRR Section
            this.leftIRRSections = ko.observableArray();

            this.rightIRRSections = ko.observableArray();

            this.sortedRightIRRSections = ko.observableArray();

            this.sortedLeftIRRSections = ko.observableArray();

            this.reseperatreAndSortIRR = function () {
                self.rightIRRSections(self.ers().executiveSummaryIRRSections().filter(function (item, arr, index) { return item.reportSide() == "Right" }));
                self.leftIRRSections(self.ers().executiveSummaryIRRSections().filter(function (item, arr, index) { return item.reportSide() == "Left" }));

                self.sortedRightIRRSections(self.rightIRRSections().sort(function (a, b) { return a.priority() - b.priority(); }));
                self.sortedLeftIRRSections(self.leftIRRSections().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.sortIRRDetails = function (l) {
                console.log(l.executiveRiskSummaryIRRSectionDets().length);
                l.sortedDetails(l.executiveRiskSummaryIRRSectionDets().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            this.sortIRRDetailScenarios = function (l) {
                l.sortedDetails(l.executiveSummaryIRRScenarios().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            this.setAvailableIRRDetails = function (l) {
                switch (l.name().toLowerCase()) {
                    case "earnings at risk ramp scenarios":
                    case "earnings at risk shock scenarios":
                        l.availableDetails(self.allSectionTypes().irrLevel2.filter(function (item, arr, index) { return item.name == 'Year 1 NII % ∆ from Year 1 Base' || item.name == "Year 2 NII % ∆ from Year 1 Base" || item.name == "Year 2 NII % ∆ from Year 2 Base" || item.name == "24 Month % ∆ from Base" }));
                        break;
                    case "core funding utilization":
                        l.availableDetails(self.allSectionTypes().irrLevel2.filter(function (item, arr, index) { return item.name == "Core Funding Utilization" }));
                        break;
                    case "economic value of equity shock scenarios":
                        l.availableDetails(self.allSectionTypes().irrLevel2.filter(function (item, arr, index) {
                            //return item.name == 'Post Shock EVE/NEV Ratio' || item.name == "EVE/NEV Ratio BP ∆ from 0 Shock" || item.name == "EVE/NEV % ∆ from 0 Shock"
                            return item.name == 'Post Shock EVE Ratio' || item.name == "EVE Ratio BP ∆ from 0 Shock" || item.name == "EVE % ∆ from 0 Shock";
                        }));
                        break;
                    case "net economic value shock scenarios":
                        l.availableDetails(self.allSectionTypes().irrLevel2.filter(function (item, arr, index) {
                            //return item.name == 'Post Shock EVE/NEV Ratio' || item.name == "EVE/NEV Ratio BP ∆ from 0 Shock" || item.name == "EVE/NEV % ∆ from 0 Shock"
                            return item.name == 'Post Shock NEV Ratio' || item.name == "NEV Ratio BP ∆ from 0 Shock" || item.name == "NEV % ∆ from 0 Shock";
                        }));
                        break;
                }
            }

            this.setAvailableScenarios = function (l, editMode) {
                // reindex based off of one that has been removed
                // for (var i = l.executiveSummaryIRRScenarios().length - 1; i >= 0; i--) {
                // l.executiveSummaryIRRScenarios()[i].entityAspect.setDeleted();
                // }
                // l.executiveSummaryIRRScenarios().forEach(function (d, index) {
                // d.priority(index);
                // });
                // self.sortIRRDetailScenarios(l);

                switch (l.name()) {
                    case "Year 1 NII % ∆ from Year 1 Base":
                    case "Year 2 NII % ∆ from Year 1 Base":
                    case "Year 2 NII % ∆ from Year 2 Base":
                    case "24 Month % ∆ from Base":
                    case "Core Funding Utilization":
                        newScens = self.scenarioTypes().filter(function (item, arr, index) { return item.isEve() == 0 });
                        break;
                    case "Post Shock EVE Ratio":
                    case "Post Shock NEV Ratio":
                    case "EVE Ratio BP ∆ from 0 Shock":
                    case "NEV Ratio BP ∆ from 0 Shock":
                    case "EVE % ∆ from 0 Shock":
                    case "NEV % ∆ from 0 Shock":
                        newScens = self.scenarioTypes().filter(function (item, arr, index) { return item.isEve() == 1 });
                        break;
                    default:
                        newScens = self.scenarioTypes().filter(function (item, arr, index) { return item.isEve() == 0 });
                        break;
                }
                console.log(`executiveRiskSummaryConfig.setAvailableScenarios('${l.name()}', ${editMode})`);
                console.log(newScens);
                l.availableScenarios(newScens);

                this.reseperatreAndSortIRR();
            }

            this.addLeftIRRSection = function () {
                self.addIRRSection('Left');
            };

            this.addRightIRRSection = function () {
                self.addIRRSection('Right');
            };

            this.irrSectionChange = function (det, event) {
                self.setAvailableIRRDetails(det);
            }

            this.addIRRSection = function (side) {
                var irrSec = globalContext.createExecRiskSummaryIRRSection();
                irrSec.reportSide(side);

                irrSec.name("Earnings at Risk Ramp Scenarios");
                irrSec.availableDetails = ko.observableArray();
                if (side == "Right") {
                    irrSec.priority(self.rightIRRSections().length);
                }
                else {
                    irrSec.priority(self.leftIRRSections().length);
                }
                self.setAvailableIRRDetails(irrSec);

                // before I add a section add property of sortable details
                irrSec.sortedDetails = ko.observableArray();
                globalContext.logEvent("Added " + side + " IRR sec to ERS " + self.ers().id(), curPage);
                self.ers().executiveSummaryIRRSections.push(irrSec);

                self.reseperatreAndSortIRR();
            };

            this.removeIRRSection = function () {

                var msg = 'Delete ' + this.name() + ' Section?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        globalContext.logEvent("Removed IRR sec from ERS " + self.ers().id(), curPage);
                        var detsLen = item.executiveRiskSummaryIRRSectionDets().length;

                        // loop through and delete all details
                        for (var d = detsLen - 1; d >= 0; d--) {
                            // loop through and delete all scenarios
                            var scenLen = item.executiveRiskSummaryIRRSectionDets()[d].executiveSummaryIRRScenarios().length;
                            for (var z = scenLen - 1; z >= 0; z--) {
                                item.executiveRiskSummaryIRRSectionDets()[d].executiveSummaryIRRScenarios()[z].entityAspect.setDeleted();
                            }

                            item.executiveRiskSummaryIRRSectionDets()[d].entityAspect.setDeleted();
                        }

                        item.entityAspect.setDeleted();
                        self.reseperatreAndSortIRR();

                        // reindex based off of one that has been removed
                        self.rightIRRSections().forEach(function (d, index) {
                            d.priority(index);
                        });

                        self.leftIRRSections().forEach(function (d, index) {
                            d.priority(index);
                        });
                    }
                }
            };

            this.irrSectionDetChange = function (det, event) {
                self.setAvailableScenarios(det, true);
            }


            this.addIRRSectionDet = function () {
                var det = globalContext.createExecutiveRiskSummaryIRRSectionDet();

                det.priority(this.executiveRiskSummaryIRRSectionDets().length);

                det.name(this.availableDetails()[0].name);

                // before I add a section add property of sortable details
                det.sortedDetails = ko.observableArray();
                det.availableScenarios = ko.observableArray();
                globalContext.logEvent("Added IRR Detail to ERS " + self.ers().id(), curPage);
                this.executiveRiskSummaryIRRSectionDets.push(det);
                self.setAvailableScenarios(det, true);

                self.sortIRRDetails(this);
            };

            this.removeIRRSectionDet = function () {
                var msg = 'Delete ' + this.name() + ' Section?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption !== 'Yes') return;
                    globalContext.logEvent("Removed IRR sec detail from ERS " + self.ers().id(), curPage);
                    var detsLen = item.executiveSummaryIRRScenarios().length;

                    // loop through and delete all details
                    for (var d = detsLen - 1; d >= 0; d--) {
                        // loop through and delete all scenerios
                        item.executiveSummaryIRRScenarios()[d].entityAspect.setDeleted();

                        item.executiveSummaryIRRScenarios().forEach(function (d, index) {
                            d.priority(index);
                        });

                        self.sortIRRDetailScenarios(item);
                    }

                    // reindex based off of one that has been removed
                    var parent = self.ers().executiveSummaryIRRSections().filter(function (par, arr, index) { return par.id() == item.executiveRiskSummaryIRRSectionId() })[0]

                    item.entityAspect.setDeleted();

                    parent.executiveRiskSummaryIRRSectionDets().forEach(function (d, index) {
                        d.priority(index);
                    });

                    self.sortIRRDetails(parent);
                }
            };

            this.addIRRSectionDetScenario = function () {
                var scen = globalContext.createExecRiskSummaryIRRScenario();

                scen.priority(this.executiveSummaryIRRScenarios().length);
                globalContext.logEvent("Added IRR sec detail scen to ERS " + self.ers().id(), curPage);
                this.executiveSummaryIRRScenarios.push(scen);
                self.sortIRRDetailScenarios(this);
                self.resetRisks();
            };

            this.removeIRRSectionScenario = function () {
                var msg = 'Delete Scenario?';
                var det = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption !== 'Yes') return;
                    globalContext.logEvent("Removed IRR sec scen from ERS " + self.ers().id(), curPage);
                    //Find parent and resort it 
                    var parent = det.executiveRiskSummaryIRRDet();
                    det.entityAspect.setDeleted();
                    //re indes based off of one that has been removed
                    parent.executiveSummaryIRRScenarios().forEach(function (d, index) {
                        d.priority(index);
                    });

                    self.sortIRRDetailScenarios(parent);
                }
            }

            //Capital Section

            this.leftCapSections = ko.observableArray();
            this.rightCapSections = ko.observableArray();
            this.sortedRightCapSections = ko.observableArray();
            this.sortedLeftCapSections = ko.observableArray();

            this.reseperatreAndSortCapital = function () {
                self.rightCapSections(self.ers().executiveRiskSummaryCapitalSection().filter(function (item, arr, index) { return item.reportSide() == "Right" }));
                self.leftCapSections(self.ers().executiveRiskSummaryCapitalSection().filter(function (item, arr, index) { return item.reportSide() == "Left" }));

                self.sortedRightCapSections(self.rightCapSections().sort(function (a, b) { return a.priority() - b.priority(); }));
                self.sortedLeftCapSections(self.leftCapSections().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.addLeftCapSection = function () {
                self.addCapSection('Left');
            };

            this.addRightCapSection = function () {
                self.addCapSection('Right');
            };

            this.addCapSection = function (side) {
                var capSec = globalContext.createExecutiveRiskSummaryCapitalSection();
                capSec.reportSide(side);

                if (side == "Right") {
                    capSec.priority(self.rightCapSections().length);
                }
                else {
                    capSec.priority(self.leftCapSections().length);
                }
                globalContext.logEvent("Added Capital sec to ERS " + self.ers().id(), curPage);
                self.ers().executiveRiskSummaryCapitalSection.push(capSec);

                self.reseperatreAndSortCapital();
                self.resetRisks();
            };

            this.removeCapSection = function () {

                var msg = 'Delete ' + this.name() + ' Section?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption !== 'Yes') return;
                    globalContext.logEvent("Removed Capital sec from ERS " + self.ers().id(), curPage);
                    item.entityAspect.setDeleted();
                    self.reseperatreAndSortCapital();

                    // reindex based off of one that has been removed
                    self.rightCapSections().forEach(function (d, index) {
                        d.priority(index);
                    });

                    self.leftCapSections().forEach(function (d, index) {
                        d.priority(index);
                    });
                }
            };

            this.sortableAfterMove = function (arg) {
                arg.sourceParent().forEach(function (obj, index) {
                    obj.priority(index);
                });
            };

            this.sortableAfterMoveMaster = function (arg) {
                arg.sourceParent().forEach(function (obj, index) {
                    obj.priority(index);
                });

                // set order fields for master section
                for (var i = 0; i < self.sortedMasters().length; i++) {
                    switch (self.sortedMasters()[i].name()) {
                        case "Liq":
                            console.log('setting liq ' + i);
                            self.ers().liqOrder(i);
                            break
                        case "Cap":
                            console.log('setting cap ' + i);
                            self.ers().capOrder(i);
                            break
                        case "IRR":
                            console.log('setting IRR ' + i);
                            self.ers().iRROrder(i);
                            break;
                    }
                }
            };

            this.sortHistoricalDateOffSets = function () {
                self.sortedHistoricalOffSets(self.ers().executiveRiskSummaryDateOffsets().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.ers().executiveRiskSummaryDateOffsets().sort(function (a, b) { return a.priority() - b.priority(); });

            this.removeHistoricalDateOffSet = function (offSet) {

                var msg = 'Delete offset "' + offSet.offSet() + '" ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption !== 'Yes') return;

                    offSet.entityAspect.setDeleted();

                    self.ers().executiveRiskSummaryDateOffsets().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });

                    self.sortHistoricalDateOffSets();
                }
            };

            this.addHistoricalDateOffSet = function () {
                var newCoreFundOffSet = globalContext.createExecutiveRiskSummaryDateOffset();

                newCoreFundOffSet.offSet(0);
                newCoreFundOffSet.priority(self.ers().executiveRiskSummaryDateOffsets().length);

                newCoreFundOffSet.nIISimulationTypeId(1);
                newCoreFundOffSet.eveSimulationTypeId(2);

                newCoreFundOffSet.simulationTypes = ko.observableArray();

                //Push on after we compelted ajax call
                self.ers().executiveRiskSummaryDateOffsets().push(newCoreFundOffSet);

                return globalContext.getAvailableSimulations(newCoreFundOffSet.simulationTypes, self.ers().institutionDatabaseName(), newCoreFundOffSet.offSet()).then(function () {

                    newCoreFundOffSet.offset = newCoreFundOffSet.offSet.subscribe(function (newValue) {
                        self.refreshItemSim(this);
                    }, newCoreFundOffSet);

                    newCoreFundOffSet.override = newCoreFundOffSet.overrideBasicSurplus.subscribe(function (newValue) {
                        globalContext.logEvent("ERSDateOffset overrideBasicSurplus changed to " + newValue, curPage);
                        self.refreshItemSim(newCoreFundOffSet);

                    }, newCoreFundOffSet);

                    newCoreFundOffSet.niiSim = newCoreFundOffSet.nIISimulationTypeId.subscribe(function (newValue) {
                        self.resetScensAvailableScens(newCoreFundOffSet);

                    }, newCoreFundOffSet);

                    newCoreFundOffSet.eveSim = newCoreFundOffSet.eveSimulationTypeId.subscribe(function (newValue) {
                        self.resetScensAvailableScens(newCoreFundOffSet);

                    }, newCoreFundOffSet);




                    self.updateOffsetFields(newCoreFundOffSet);

                    self.sortHistoricalDateOffSets();
                    self.resetScensAvailableScens();
                    return true;
                });



            };

            this.updateOffsetFields = function (offSet) {
                // find offset among policies and see what we can find
                if (self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']] != undefined) {
                    offSet.nIISimulationTypeId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']][0]["niiId"]));
                    offSet.eveSimulationTypeId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']][0]["eveId"]));
                    offSet.basicSurplusAdminId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']][0]["bsId"]));
                }

                if (self.modelOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']] != undefined) {
                    offSet.taxEquivIncome(JSON.parse(self.modelOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']][0]["taxIncome"].toLowerCase()));
                    offSet.taxEquivYield(JSON.parse(self.modelOffsets()[0][self.asOfDateOffsets()[offSet.offSet()]['asOfDateDescript']][0]["taxYield"].toLowerCase()));
                }
            }

            this.offsetChange = function () {
                self.updateOffsetFields(this);
            }

            this.restoreDefaultCheck = function () {
                if (!this.overrideBasicSurplus()) {
                    self.updateOffsetFields(this);
                }

                return true;
            }

            this.resetOnInstChange = function () {
                for (var i = 0; i < self.ers().executiveRiskSummaryDateOffsets().length; i++) {
                    self.ers().executiveRiskSummaryDateOffsets()[i].overrideBasicSurplus(false);
                    self.updateOffsetFields(self.ers().executiveRiskSummaryDateOffsets()[i]);
                }
            }

            this.resetWait = function () {
                var self = this;
                setTimeout(function () { self.resetRisks() }, 150);
                return true;
            }

            this.resetRisks = function () {
                if (!self.ers().liquidityRiskAssesmentOverride()) {
                    self.ers().liquidityRiskAssesment(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[self.ers().executiveRiskSummaryDateOffsets()[0].offSet()]['asOfDateDescript']][0].liquidityRiskAssessment));
                }
                if (!self.ers().interestRateRiskAssesmentOverride()) {
                    self.ers().interestRateRiskAssesment(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[self.ers().executiveRiskSummaryDateOffsets()[0].offSet()]['asOfDateDescript']][0].nIIRiskAssessment));
                }
                if (!self.ers().capitalRiskAssesmentOverride()) {
                    self.ers().capitalRiskAssesment(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[self.ers().executiveRiskSummaryDateOffsets()[0].offSet()]['asOfDateDescript']][0].capitalRiskAssessment));
                }

                if (self.currentRiskAss().irrTable == undefined) {
                    return true;
                }

                // loop through all section each report and perform look up

                // Liq Sections
                for (var s = 0; s < self.ers().executiveRiskSummaryLiquiditySections().length; s++) {
                    for (var i = 0; i < self.ers().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets().length; i++) {
                        if (!self.ers().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets()[i].overrideRiskAssesment()) {

                            var foundVal = self.currentRiskAss().liqTable.filter(function (item, arr, index) { return item.name == self.ers().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets()[i].name() });

                            if (foundVal.length != 0) {
                                self.ers().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets()[i].riskAssesment(foundVal[0].ra);
                            }
                        }
                    }
                }

                // IRR Sections
                for (var s = 0; s < self.ers().executiveSummaryIRRSections().length; s++) {
                    for (var i = 0; i < self.ers().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets().length; i++) {
                        for (var k = 0; k < self.ers().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios().length; k++) {

                            if (!self.ers().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios()[k].overrideRiskAssesment()) {
                                // map names since they are different on policies and here
                                var parentLookupName = "";
                                switch (self.ers().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].name()) {
                                    case "Core Funding Utilization":
                                        parentLookupName = "Core Funding Utilization (Max.)";
                                        break;
                                    case "Year 1 NII % ∆ from Year 1 Base":
                                        parentLookupName = "Year 1 NII % Change from Year 1 Base";
                                        break;
                                    case "Year 2 NII % ∆ from Year 1 Base":
                                        parentLookupName = "Year 2 NII % Change from Year 1 Base";
                                        break;
                                    case "Year 2 NII % ∆ from Year 2 Base":
                                        parentLookupName = "Year 2 NII % Change from Year 2 Base";
                                        break;
                                    case "24 Month % ∆ from Base":
                                        parentLookupName = "24 Month % Change From Base";
                                        break;
                                    case "Post Shock EVE Ratio":
                                        parentLookupName = "Post-Shock EVE/NEV Ratio (EVE/EVA)";
                                        break;
                                    case "EVE Ratio BP ∆ from 0 Shock":
                                    case "NEV Ratio BP ∆ from 0 Shock":
                                        parentLookupName = "BP Change in EVE/NEV Ratio (EVE/EVA) from 0 Shock";
                                        break;
                                    case "EVE % ∆ from 0 Shock":
                                    case "NEV % ∆ from 0 Shock":
                                        parentLookupName = "EVE/NEV % Change from 0 Shock";
                                        break;
                                    default:
                                        parentLookupName = "";
                                        break;
                                }


                                var foundVal = self.currentRiskAss().irrTable.filter(function (item, arr, index) { return item.name == parentLookupName && item.scenId == self.ers().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios()[k].scenarioTypeId() });
                                if (foundVal.length != 0) {
                                    self.ers().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios()[k].riskAssesment(foundVal[0].ra);
                                }

                            }


                        }
                    }
                }

                // Capital Section
                for (var s = 0; s < self.ers().executiveRiskSummaryCapitalSection().length; s++) {
                    if (!self.ers().executiveRiskSummaryCapitalSection()[s].overrideRiskAssesment()) {

                        var foundVal = self.currentRiskAss().capTable.filter(function (item, arr, index) { return item.name == self.ers().executiveRiskSummaryCapitalSection()[s].name() });

                        if (foundVal.length != 0) {
                            self.ers().executiveRiskSummaryCapitalSection()[s].riskAssesment(foundVal[0].ra);
                        }
                    }
                }

                return true;
            }

            function setOrResetChanges(isReset) {

                try {
                    self.sortedHistoricalOffSets(self.ers().executiveRiskSummaryDateOffsets());

                    // loop through all IRR sections and sort
                    for (var i = 0; i < self.ers().executiveSummaryIRRSections().length; i++) {
                        var currentIRRSection = self.ers().executiveSummaryIRRSections()[i];

                        if (!isReset) {
                            currentIRRSection.availableDetails = ko.observableArray();
                            currentIRRSection.sortedDetails = ko.observableArray();
                        }

                        self.setAvailableIRRDetails(currentIRRSection);
                        self.sortIRRDetails(currentIRRSection);


                        for (var z = 0; z < currentIRRSection.executiveRiskSummaryIRRSectionDets().length; z++) {
                            var currentIRRDetail = currentIRRSection.executiveRiskSummaryIRRSectionDets()[z];

                            if (!isReset) {
                                currentIRRDetail.availableScenarios = ko.observableArray();
                                currentIRRDetail.sortedDetails = ko.observableArray();
                            }

                            self.sortIRRDetailScenarios(currentIRRDetail);
                            self.setAvailableScenarios(currentIRRDetail, false);

                            // var obj = self.ers().executiveSummaryIRRSections()[i].executiveRiskSummaryIRRSectionDets()[z];
                            // self.ers().executiveSummaryIRRSections()[i].executiveRiskSummaryIRRSectionDets()[z].name.subscribe(function (newValue) {
                            // self.setAvailableScenarios(this);
                            // }, self.ers().executiveSummaryIRRSections()[i].executiveRiskSummaryIRRSectionDets()[z]);
                        }
                    }

                    // loop through all liquidity sections and sort deatils for each one
                    for (var i = 0; i < self.ers().executiveRiskSummaryLiquiditySections().length; i++) {
                        var currentLiquiditySection = self.ers().executiveRiskSummaryLiquiditySections()[i];

                        if (!isReset) {
                            currentLiquiditySection.sortedDetails = ko.observableArray();
                        }

                        self.sortLiqDetails(currentLiquiditySection);
                    }

                    // loop through historical off sets and if override is false make sure they are inline with policies
                    for (var i = 0; i < self.ers().executiveRiskSummaryDateOffsets().length; i++) {
                        if (!self.ers().executiveRiskSummaryDateOffsets()[i].overrideBasicSurplus()) {
                            self.updateOffsetFields(self.ers().executiveRiskSummaryDateOffsets()[i]);
                        }
                    }

                    self.reseperatreAndSortLiq();
                    self.reseperatreAndSortIRR();
                    self.reseperatreAndSortCapital();
                    self.sortHistoricalDateOffSets();
                }
                catch (err) {
                    alert(err);
                }

            }



            this.refreshItemSim = function (item) {
                globalContext.getAvailableSimulations(item.simulationTypes, self.ers().institutionDatabaseName(), item.offSet()).then(function () {
                    self.resetScensAvailableScens();
                });
            }

            this.resetScensAvailableScens = function () {
                self.refreshScens().then(function () {
                    setOrResetChanges(true);

                });
            }

            this.refreshScens = function () {

                //loop through all offsets and get sim type sand opffets into array and pass into function to get distinct scenarios across all of them
                var sims = [];
                var offsets = [];
                var eveSims = [];
                self.ers().executiveRiskSummaryDateOffsets().forEach(function (item, index, arr) {

                    sims.push(item.nIISimulationTypeId());
                    eveSims.push(item.eveSimulationTypeId());
                    offsets.push(item.offSet());

                });

                return globalContext.getAvailableSimulationScenariosMultiple(self.scenarioTypes, self.ers().institutionDatabaseName(), offsets.join(), sims.join(), eveSims.join());
            }

            //this.compositionComplete = function () {
            //    if (self.ers().id() > 0) {
            //        globalContext.saveChangesQuiet();
            //    }
            //};

            var isInActivate = false;

            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage);  
                if (isInActivate) return;

                isInActivate = true;

                app.on('application:cancelChanges', onChangesCanceled);

                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return self.refreshScens().then(function () {
                        return globalContext.getModelOffsets(self.modelOffsets, self.ers().institutionDatabaseName()).then(function () {
                            return globalContext.getExecRiskSummarySections(self.allSectionTypes, self.ers().institutionDatabaseName()).then(function () {
                                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.ers().institutionDatabaseName()).then(function () {
                                    return globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, self.ers().institutionDatabaseName()).then(function () {
                                        return globalContext.getPolicyOffsets(self.policyOffsets, self.ers().institutionDatabaseName()).then(function () {
                                            return globalContext.getCurrentRiskAss((self.ers().executiveRiskSummaryDateOffsets().length == 0 ? 0 : self.ers().executiveRiskSummaryDateOffsets()[0].offSet()), self.ers().institutionDatabaseName(), self.currentRiskAss).then(function () {
                                                self.instSub = self.ers().institutionDatabaseName.subscribe(function (newValue) {
                                                    return Q.all([
                                                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue),
                                                        globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, newValue),
                                                        globalContext.getPolicyOffsets(self.policyOffsets, newValue),
                                                        globalContext.getExecRiskSummarySections(self.allSectionTypes, newValue),
                                                        globalContext.getModelOffsets(self.modelOffsets, newValue)

                                                    ]).then(function () {
                                                        // go fetch current risk assessments
                                                        globalContext.getCurrentRiskAss(self.ers().executiveRiskSummaryDateOffsets()[0].offSet(), newValue, self.currentRiskAss).then(function () {
                                                            // call function to build hash of risk assesments and then sets them if needed
                                                            self.resetRisks();
                                                            //globalContext.saveChangesQuiet();
                                                        });
                                                    });
                                                });

                                                //var ersOverrides = ["liquidityRiskAssesmentOverride", "interestRateRiskAssesmentOverride", "capitalRiskAssesmentOverride"];

                                                self.liqOverride = self.ers().liquidityRiskAssesmentOverride.subscribe(function (newValue) {
                                                    globalContext.logEvent("ERS LiquidityRiskAssesmentOverride changed to " + newValue, curPage);
                                                    self.resetRisks();
                                                });

                                                self.intRateOverride = self.ers().interestRateRiskAssesmentOverride.subscribe(function (newValue) {
                                                    globalContext.logEvent("ERS InterestRateRiskAssesmentOverride changed to " + newValue, curPage);
                                                    self.resetRisks();
                                                });

                                                self.capOverride = self.ers().capitalRiskAssesmentOverride.subscribe(function (newValue) {
                                                    globalContext.logEvent("ERS CapitalRiskAssesmentOverridechanged to " + newValue, curPage);
                                                    self.resetRisks();
                                                });

                                                if (self.ers().id() < 0) {
                                                    var val = self.addHistoricalDateOffSet();
                                                }


                                                self.resetRisks();
                                                // loop through all IRR sections and sort
                                                for (var i = 0; i < self.ers().executiveSummaryIRRSections().length; i++) {
                                                    var currentIRRSection = self.ers().executiveSummaryIRRSections()[i];

                                                    currentIRRSection.availableDetails = ko.observableArray();
                                                    currentIRRSection.sortedDetails = ko.observableArray();

                                                    for (var z = 0; z < currentIRRSection.executiveRiskSummaryIRRSectionDets().length; z++) {
                                                        var currentIRRDetail = currentIRRSection.executiveRiskSummaryIRRSectionDets()[z];

                                                        currentIRRDetail.availableScenarios = ko.observableArray();
                                                        currentIRRDetail.sortedDetails = ko.observableArray();
                                                    }
                                                }

                                                // loop through all liquidity sections and sort deatils for each one
                                                for (var i = 0; i < self.ers().executiveRiskSummaryLiquiditySections().length; i++) {
                                                    var currentLiquiditySection = self.ers().executiveRiskSummaryLiquiditySections()[i];

                                                    currentLiquiditySection.sortedDetails = ko.observableArray();
                                                }

                                                // loop through historical off sets and if override is false make sure they are inline with policies
                                                for (var i = 0; i < self.ers().executiveRiskSummaryDateOffsets().length; i++) {
                                                    if (!self.ers().executiveRiskSummaryDateOffsets()[i].overrideBasicSurplus()) {
                                                        self.updateOffsetFields(self.ers().executiveRiskSummaryDateOffsets()[i]);
                                                    }
                                                }

                                                //loop through offsets and set up simulationTypes array
                                                var offsetsProcessed = 0
                                                self.ers().executiveRiskSummaryDateOffsets().forEach(function (item, index, arr) {

                                                    if (self.ers().id() >= 0) {
                                                        item.simulationTypes = ko.observableArray();
                                                    }

                                                    globalContext.getAvailableSimulations(item.simulationTypes, self.ers().institutionDatabaseName(), item.offSet()).then(function () {
                                                        console.log(item.simulationTypes());
                                                        if (self.ers().id() >= 0) {
                                                            item.offset = item.offSet.subscribe(function (newValue) {
                                                                self.refreshItemSim(this);
                                                            }, item);

                                                            item.override = item.overrideBasicSurplus.subscribe(function (newValue) {
                                                                globalContext.logEvent("ERSDateOffset overrideBasicSurplus changed to " + newValue, curPage);
                                                                self.refreshItemSim(item);

                                                            }, item);

                                                            item.niiSim = item.nIISimulationTypeId.subscribe(function (newValue) {
                                                                self.resetScensAvailableScens(item);

                                                            }, item);

                                                            item.eveSim = item.eveSimulationTypeId.subscribe(function (newValue) {
                                                                self.resetScensAvailableScens(item);

                                                            }, item);
                                                        }

                                                        offsetsProcessed++;
                                                        //when done this just move ona nd setorrest chagnes function
                                                        if (offsetsProcessed == arr.length) {
                                                            //self.refreshScens().then(function () {
                                                            setOrResetChanges(true);
                                                            if (self.ers().id() >= 0) {
                                                                globalContext.saveChangesQuiet();
                                                            }

                                                            // });
                                                        }
                                                    });
                                                });

                                                // create sorted masters 
                                                self.sortedMasters.push({ name: ko.observable('dummy'), priority: ko.observable(0) });
                                                self.sortedMasters.push({ name: ko.observable('dummy'), priority: ko.observable(0) });
                                                self.sortedMasters.push({ name: ko.observable('dummy'), priority: ko.observable(0) });

                                                // if new set it in default ourder liq, irr, cap otherwise set to whatever they had
                                                if (self.ers().id() < 0) {
                                                    self.sortedMasters()[0].name("Liq");
                                                    self.sortedMasters()[0].priority(0);

                                                    self.sortedMasters()[1].name("IRR");
                                                    self.sortedMasters()[1].priority(1);

                                                    self.sortedMasters()[2].name("Cap");
                                                    self.sortedMasters()[2].priority(2);

                                                    self.ers().liqOrder(0);
                                                    self.ers().iRROrder(1);
                                                    self.ers().capOrder(2);
                                                }
                                                else {
                                                    self.sortedMasters()[self.ers().liqOrder()].name("Liq");
                                                    self.sortedMasters()[self.ers().liqOrder()].priority(self.ers().liqOrder());

                                                    self.sortedMasters()[self.ers().iRROrder()].name("IRR");
                                                    self.sortedMasters()[self.ers().iRROrder()].priority(self.ers().iRROrder());

                                                    self.sortedMasters()[self.ers().capOrder()].name("Cap");
                                                    self.sortedMasters()[self.ers().capOrder()].priority(self.ers().capOrder());
                                                }

                                                self.sortMasters();
                                            })
                                        });
                                    });
                                });
                            });
                        });
                    });
                }).fin(function () {
                    isInActivate = false;
                });
            };

            this.deactivate = function () {
                app.off('application:cancelChanges', onChangesCanceled);
            };

            this.detached = function (view, parent) {
                //AsOfDateOffset Handling (Custom FundingMatrix Multi-Institution Template)
                self.ers().executiveRiskSummaryDateOffsets().forEach(function (item, index, arr) {
                    item.offset.dispose();
                    item.override.dispose();
                    item.niiSim.dispose();
                    item.eveSim.dispose();
                });

                self.liqOverride.dispose();
                self.intRateOverride.dispose();
                self.capOverride.dispose();

                self.instSub.dispose();
            };

            return this;
        };

        return vm;
    });
