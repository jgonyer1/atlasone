﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var dsaVm = function (dsa) {
        var self = this;
        var curPage = "DetailedSimulationAssumptionsConfig";
        this.dsa = dsa;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();
        this.policyOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();
        this.scenarioTypes = ko.observableArray();

        this.subs = [];

        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.dsa().institutionDatabaseName(), self.dsa().asOfDateOffset(), self.dsa().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.dsa().institutionDatabaseName(), self.dsa().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }
        this.defaultSimulation = function () {
            var aod = self.asOfDateOffsets()[self.dsa().asOfDateOffset()].asOfDateDescript;
            if (!self.dsa().overrideSimulationType()) {
                if (self.policyOffsets()[0][aod] && self.policyOffsets()[0][aod][0] != undefined) {
                    self.dsa().simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                }
            }
        };
        this.defaultScenario = function () {
            var aod = self.asOfDateOffsets()[self.dsa().asOfDateOffset()].asOfDateDescript;
            if (!self.dsa().overrideScenarioType()) {
                if (self.policyOffsets()[0][aod] && self.policyOffsets()[0][aod][0] != undefined) {
                    self.dsa().scenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
                }
            }
        };

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            return Q.all([
                    globalContext.getAvailableBanks(self.institutions) //weird becuase we remove two fo the calls but for now this will do...this will do
            ]).then(function () {
                //AsOfDateOffset Handling (Single-Institution Template)

                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.dsa().institutionDatabaseName()).then(function () {
                    return globalContext.getAvailableSimulations(self.simulationTypes, self.dsa().institutionDatabaseName(), self.dsa().asOfDateOffset()).then(function () {
                        return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.dsa().institutionDatabaseName(), self.dsa().asOfDateOffset(), self.dsa().simulationTypeId(), false).then(function () {
                            return globalContext.getPolicyOffsets(self.policyOffsets, self.dsa().institutionDatabaseName()).then(function () {
                                self.defaultSimulation();
                                self.defaultScenario();
                                self.subs.push(self.dsa().institutionDatabaseName.subscribe(function (newValue) {
                                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                                    self.refreshSim();
                                }));

                                self.subs.push(self.dsa().asOfDateOffset.subscribe(function (newValue) {
                                    self.refreshSim();
                                }));


                                self.subs.push(self.dsa().simulationTypeId.subscribe(function (newValue) {
                                    self.refreshScen();
                                }));

                                self.subs.push(self.dsa().overrideSimulationType.subscribe(self.defaultSimulation));
                                self.subs.push(self.dsa().overrideScenarioType.subscribe(self.defaultScenario));
                                global.loadingReport(false);
                            }); 
                            
                        });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            //self.instSub.dispose();
            //self.dateSub.dispose();
            //self.simSub.dispose();
            for (var i = 0; i < self.subs.length; i++) {
                self.subs[i].dispose();
            }
        };

        return this;
    };

    return dsaVm;
});
