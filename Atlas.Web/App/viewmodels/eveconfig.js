﻿define([
    'services/globalcontext',
    'services/global',
    'viewmodels/simulationselectconfig',
    'durandal/app'
],
function (globalContext, global, simulationselectconfig, app) {
    var eveVm = function (eve) {
        var self = this;
        var curPage = "EveConfig";
        this.eve = eve;

        this.institutions = ko.observableArray();

        this.global = global;

        this.simulationTypes = ko.observableArray();

        this.definedGroupings = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.disablePolicyLimits = ko.observable(false);

        this.viewOptions = ko.observableArray([{ name: 'Account Type', value: '1', disable: false }]);
        this.policyOffsets = ko.observableArray();
        this.subs = [];

        this.reportFormatOptions = ko.observableArray();
        this.simulationselectconfig = new simulationselectconfig(self.eve().institutionDatabaseName, self.eve().asOfDateOffset, self.eve().simulationTypeId, self.eve().overrideSimulationId, true);

        this.sortedEveScenarioTypes = ko.observableArray(self.eve().eveScenarioTypes());
        this.setScenarioTypePriority = function () {
            self.sortedEveScenarioTypes().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.sortEveScenarioTypes = function () {
            self.sortedEveScenarioTypes(self.eve().eveScenarioTypes().sort(function (a, b) {
                return parseInt(a.scenarioType().name()) - parseInt(b.scenarioType().name());
            }));
        };

        this.eve().eveScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var newEveScenarioType = globalContext.createEveScenarioType();
            newEveScenarioType.scenarioTypeId(scenarioType.id);
            newEveScenarioType.priority(eve().eveScenarioTypes().length);
            //hack so sort works since it is not setting object
            //newEveScenarioType.scenarioType = ko.observable({ name: ko.observable(scenarioType.name) });
            eve().eveScenarioTypes().push(newEveScenarioType);
            self.sortEveScenarioTypes();
            self.setScenarioTypePriority();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };

        this.removeScenarioType = function (eveScenarioType) {

            var msg = 'Delete scenario ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    eveScenarioType.entityAspect.setDeleted();
                    eve().eveScenarioTypes().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortEveScenarioTypes();
                    self.setScenarioTypePriority();
                }
            }

        };

        this.cancel = function () {
            self.sortEveScenarioTypes();
        };
        self.sortEveScenarioTypes();
        self.setScenarioTypePriority();

        this.deactivate = function () {
            for (var s = 0; s < self.subs.length; s++) {
                self.subs[s].dispose();
            }
        };
        this.bankInfo = ko.observable();

        this.defaultSimulation = function () {
            //console.log(self.simulationselectconfig);
            if (!self.eve().overrideSimulationId()) {
                var aod = self.simulationselectconfig.asOfDateOffsets()[self.eve().asOfDateOffset()].asOfDateDescript;

                var currentPolicy = self.policyOffsets()[0][aod];

                if (! currentPolicy) return;

                self.eve().simulationTypeId(currentPolicy[0].eveId);
            }
        }

        this.togglePolicyLimits = function () {
            try {
                var aod = self.simulationselectconfig.asOfDateOffsets()[self.eve().asOfDateOffset()].asOfDateDescript;


                if (self.policyOffsets()[0][aod][0].eveId != self.eve().simulationTypeId()) {
                    self.eve().policyLimits(false);
                    self.disablePolicyLimits(true);
                }
                else if (self.eve().reportFormat() == '2') {
                    self.eve().policyLimits(false);
                    self.disablePolicyLimits(true);
                }
                else {
                    self.disablePolicyLimits(false);
                }
            }
            catch (err) {

            }    
        }

        this.compositionComplete = function () {
            self.defaultSimulation();
         
            if (self.eve().id() > 0) {
               // self.togglePolicyLimits();
                globalContext.saveChangesQuiet();
            }
        }

        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.eve().institutionDatabaseName(), self.eve().asOfDateOffset(), self.eve().simulationTypeId(), true)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationselectconfig.simulationTypes, self.eve().institutionDatabaseName(), self.eve().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }


        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.eve().institutionDatabaseName(), self.eve().asOfDateOffset(), self.eve().simulationTypeId(), true).then(function () {
                return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                    return globalContext.getPolicyOffsets(self.policyOffsets, self.eve().institutionDatabaseName()).then(function () {
                        return globalContext.getBankInfo(self.bankInfo, null).then(function () {
                            if (eve().eveScenarioTypes().length == 0) {
                                self.addScenarioType({id: 30});
                            }

                            if (self.bankInfo().creditUnion.instType == "CU") {
                                self.reportFormatOptions([
                                    { name: "Standard", value: "0" },
                                    { name: "Include Risk Summary Grid", value: "1" },
                                    { name: "NCUA Supervisory NEV Test", value: "2" }
                                ]);
                            } else {
                                self.reportFormatOptions([
                                    { name: "Standard", value: "0" },
                                    { name: "Include Risk Summary Grid", value: "1" }
                                ]);
                            }
      
                            self.subs.push(self.eve().asOfDateOffset.subscribe(function () {
                                self.refreshSim();
                                self.defaultSimulation();
                            }));
                            self.subs.push(self.eve().overrideSimulationId.subscribe(function (newValue) {
                                self.refreshSim();
                                self.defaultSimulation();
                            }));
                            self.subs.push(self.eve().institutionDatabaseName.subscribe(function (newValue) {
                                globalContext.getPolicyOffsets(self.policyOffsets, newValue).then(function () {
                                    self.refreshSim();
                                    self.defaultSimulation();
                                });
                            }));

                            //If ncua stops them from inckluding policies
                            self.subs.push(self.eve().simulationTypeId.subscribe(function (newValue) {
                                self.refreshScen();
                              //  self.togglePolicyLimits();
 
                            }));

                            //On simulation change if the simulatiotype if != the policies simulation type id uncheck policy limits and disable
                            self.subs.push(self.eve().reportFormat.subscribe(function (newValue) {
                              //  self.togglePolicyLimits();

                            }));


                            self.availableScenarioTypes = ko.computed(function () {
                                var scenarioTypeIdsInUse = self.eve().eveScenarioTypes().map(function (eveScenarioType) {
                                    return eveScenarioType.scenarioTypeId();
                                });
                                var result = [];
                                self.scenarioTypes().forEach(function (scenarioType) {
                                    result.push({
                                        id: scenarioType.id(),
                                        name: scenarioType.name(),
                                        inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                    });
                                });
                                return result;
                            });

                            var arr = [];
                            arr.push({
                                name: "Standard View", options: [{ name: "Account Type", id: "1" }]
                            });
                            arr.push({ name: "Defined Groupings", options: self.definedGroupings() });
                            self.viewOptions(global.getGroupedSelectOptions(arr));
                            global.loadingReport(false);
                        });
                    });
                });
            });
        };



        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        return this;
    };

    return eveVm;
});
