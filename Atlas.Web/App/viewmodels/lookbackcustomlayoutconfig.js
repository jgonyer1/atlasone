﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/system',
    'durandal/app',
    'services/logger',
    'services/model',
    'viewmodels/lookbackrowitemcollectionconfig',
    'services/lookbackservice',
    'viewmodels/navigateawaymodal'
],
    function (globalContext, router, system, app, logger, model, rowItemCollection, lbService, naModal) {
        var vm = function () {
            var self = this;
            var curPage = "LookbacCustomLayoutConfig";
            this.layout = ko.observable();
            this.isSaving = ko.observable(false);

            //these will get populated in the activate function
            this.assetRowItems;
            this.liabRowItems;

            this.allAssetClassifications = ko.observableArray([]);
            this.allLiabClassifications = ko.observableArray([]);
            this.reportTypes = ["Section Header", "Sub", "Master"];

            this.addAssetRowItem = function () {
                addRowItem(true);
            };
            this.addLiabilityRowItem = function () {
                addRowItem(false);
            };

            //Navigation Helpers
            this.hasChanges = ko.computed(function () {
                var changes = globalContext.getChanges();
                return globalContext.hasChanges();
            });

            this.canSave = ko.computed(function () {
                return self.hasChanges();// && !self.isSaving();
            });

            this.canDeactivate = function () {
                if (! self.hasChanges()) return true;

                return naModal.show(
                    function () { globalContext.cancelChanges(); },
                    function () { self.save(); }
                );
            };
            //Navigation
            this.goBack = function () {
                router.navigateBack();
            };

            this.cancel = function () {
                self.isSaving(true);
                globalContext.cancelChanges();
                self.isSaving(false);
            };

            this.save = function () {
                self.isSaving(true);
                return globalContext.saveChanges().fin(function () {
                    var allLookbacksWithThisLayout = ko.observableArray();
                    globalContext.getLookbacksByLayoutId(self.layout().id(), allLookbacksWithThisLayout).then(function () {
                        var promises = [];
                        for (var lb = 0; lb < allLookbacksWithThisLayout().length; lb++) {
                            promises.push(lbService.createDataForLookbackGroup(ko.observable(allLookbacksWithThisLayout()[lb]), true, ko.observable({}), true));
                        }
                        Q.all(promises).fin(function () {
                            globalContext.saveChangesQuiet().then(function () {
                                self.isSaving(false);
                            });
                        });

                        
                    });
                });

                function complete() {
                    self.isSaving(false);
                }
            };
            this.sortableAfterMove = function (arg) {
                arg.sourceParent().forEach(function (chartScenarioType, index) {
                    chartScenarioType.priority(index);
                });
            };
            //this.canSave = ko.pureComputed(function () {
            //    return self.hasChanges() && !self.isSaving();
            //});

            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage); 
                return globalContext.getLBCustomLayoutById(id, self.layout).then(function () {
                    return globalContext.getClassifications(1, self.allAssetClassifications).then(function () {
                        return globalContext.getClassifications(0, self.allLiabClassifications).then(function () {
                            self.assetRowItems = new rowItemCollection(true, self, self.layout().rowItems, self.allAssetClassifications);
                            self.liabRowItems = new rowItemCollection(false, self, self.layout().rowItems, self.allLiabClassifications);
                        });
                    });
                });
            }
            
        };    

        
   
        return vm;
    });
