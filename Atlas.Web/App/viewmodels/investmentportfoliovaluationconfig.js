﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var ivpVm = function (ivp) {
        var self = this;
        var curPage = "InvestmentPortfolioValuationConfig";
        this.ivp = ivp;

        function onChangesCanceled() {
            self.sortScenarioTypes();
        }

        this.cancel = function () {
        };

        this.scenarioTypes = [
        { ddwName: '-400', name: 'Instantaneous Shock Down 400BP' },
        { ddwName: '-300', name: 'Instantaneous Shock Down 300BP' },
        { ddwName: '-200', name: 'Instantaneous Shock Down 200BP' },
        { ddwName: '-100', name: 'Instantaneous Shock Down 100BP' },
        { ddwName: 'Base', name: 'Instantaneous Shock 0BP' },
        { ddwName: '100', name: 'Instantaneous Shock Up 100BP' },
        { ddwName: '200', name: 'Instantaneous Shock Up 200BP' },
        { ddwName: '300', name: 'Instantaneous Shock Up 300BP' },
        { ddwName: '400', name: 'Instantaneous Shock Up 400BP' }
        ];
        //Scenario Block For Sorting, Adding, and Deleting From Scenario List
        this.sortedScenarioTypes = ko.observableArray(self.ivp().scenarios());

        this.sortScenarioTypes = function () {
            self.sortedScenarioTypes(self.ivp().scenarios().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.ivp().scenarios().sort(function (a, b) { return a.priority() - b.priority(); });


        this.addScenarioType = function (scenarioType) {
            var newScenario = globalContext.createInvestmentPortfolioValuationScenario();
            newScenario.dDWName(scenarioType.ddwName);
            newScenario.name(scenarioType.name);
            newScenario.priority(ivp().scenarios().length);

            ivp().scenarios().push(newScenario);
            self.sortScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };

        this.removeScenarioType = function (scenario) {

            var msg = 'Delete scenario "' + scenario.name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    scenario.entityAspect.setDeleted();
                    ivp().scenarios().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortScenarioTypes();

                }
            }

        };

        self.sortScenarioTypes();

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.institutions = ko.observableArray();

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return globalContext.getAvailableBanks(this.institutions).then(function () {
                self.availableScenarioTypes = ko.computed(function () {
                    var scenarioTypeIdsInUse = self.ivp().scenarios().map(function (scenario) {
                        return scenario.name();
                    });

                    var result = [];
                    self.scenarioTypes.forEach(function (scenarioType) {
                        result.push({
                            name: scenarioType.name,
                            inUse: scenarioTypeIdsInUse.indexOf(scenarioType.name) != -1,
                            ddwName: scenarioType.ddwName
                        });
                    });
                    return result;
                });

                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub = self.ivp().institutionDatabaseName.subscribe(function (newValue) {
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                });
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.ivp().institutionDatabaseName()).then(function () {
                    global.loadingReport(false);
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return ivpVm;
});
