﻿define(['viewmodels/chartconfig', 'services/globalContext', 'services/global'],
    
    function (chartconfig, globalContext, global) {
        var oneChartConfigVm = function (oneChart) {
            if (oneChart().niiOption() != "nii" && oneChart().niiOption() != "ni") {
                oneChart().niiOption("nii");
            }
            var that = this;
            this.oneChart = oneChart;
            this.modelSetup = ko.observable();
            this.overrideSub = null;
            this.chartVm = ko.observable(new chartconfig(oneChart().chart, oneChart().niiOption, { showPercChange: true, showComparatives: true, showInst: true, callSave: true, canAddSimulations: true, side: "left" }));
            this.cancel = function () {
                that.chartVm().cancel();
            };
            this.compositionComplete = function () {
                
                
            };
            this.activate = function () {
                global.loadingReport(true);
                return globalContext.getModelSetup(that.modelSetup).fin(function () {

                    //tax equiv sync
                    if (!oneChart().overrideTaxEquiv()) {
                        console.log("ONE CHART SET TAX EQUIV TO MODEL SETUP VAL");
                        oneChart().taxEquivalentIncome(that.modelSetup().reportTaxEquivalent());
                        //save gets done in chartconfig
                        if (oneChart().id() > 0) {
                            console.log("ONE CHART SAVE CHANGES QUIET");
                            //globalContext.saveChangesQuiet();
                        }
                    }
                    that.overrideSub = oneChart().overrideTaxEquiv.subscribe(function (newValue) {
                        if (!newValue) {
                            oneChart().taxEquivalentIncome(that.modelSetup().reportTaxEquivalent());
                        }
                    });
                    global.loadingReport(false);
                });
            }
        };
        this.deactivate = function () {
            that.overrideSub.dispose();
        };
        return oneChartConfigVm;
    });
