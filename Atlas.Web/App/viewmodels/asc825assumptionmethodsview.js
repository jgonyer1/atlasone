﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var ascVm = function (id) {
        var self = this;
        this.ascViewData = ko.observable();

        this.compositionComplete = function () {
            //
            global.loadingReport(false);
            //$('#lastSection').append('<span>height: ' + $('#lastSection').height() + '</span>');
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {

                $('#lastSection').css('min-height', '768px');
                $('#reportHolder').height($('#reportHolder').height());
                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            globalContext.logEvent("Viewed", "ASC825assumptionmethodsview");
            global.loadingReport(true);
            return globalContext.getASC825AssumptionMethodsViewDataById(id, self.ascViewData).then(function () {

            });
        };
    };

    return ascVm;

});
