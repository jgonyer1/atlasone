﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var yieldCurveShiftVm = function (yieldCurveShift) {
        var self = this;

        function onChangesCanceled() {
            self.sortScenarioTypes();
            self.sortHistoricDates();
        }

        this.yieldCurveShift = yieldCurveShift;

        this.cancel = function () {
        };

        //Scenario Block For Sorting, Adding, and Deleting From Scenario List
        this.sortedScenarioTypes = ko.observableArray();

        this.sortScenarioTypes = function () {
            self.sortedScenarioTypes(self.yieldCurveShift().yieldCurveShiftScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
            var x = 0;
        };

        this.yieldCurveShift().yieldCurveShiftScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });
        this.yieldCurveShift().yieldCurveShiftHistoricDates().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var yieldCurveScenarioType = globalContext.createYieldCurveShiftScenarioType();
            yieldCurveScenarioType.webRateScenarioTypeId(scenarioType.id);
            console.log(scenarioType);
            //yieldCurveScenarioType.webRateScenarioType(1);
            yieldCurveScenarioType.priority(yieldCurveShift().yieldCurveShiftScenarioTypes().length);
            yieldCurveShift().yieldCurveShiftScenarioTypes().push(yieldCurveScenarioType);
           // console.log(yieldCurveShift().yieldCurveShiftScenarioTypes()[yieldCurveShift().yieldCurveShiftScenarioTypes().length - 1].webRateScenarioType());
            self.sortScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };


        this.removeScenarioType = function () {

            var msg = 'Delete scenario "' + this.webRateScenarioType().name() + '" ?';
            var title = 'Confirm Delete';
            var det = this;
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {

                    //For somer eason had to hack this believe it has to do with default base and cannot be deleted
                    yieldCurveShift().yieldCurveShiftScenarioTypes.remove(function (scen) {
                        return scen.id() == det.id();
                    });


                    try {
                        det.entityAspect.setDeleted();
                    }
                    catch (err) {
                       // alert(err);
                    }

                    yieldCurveShift().yieldCurveShiftScenarioTypes().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortScenarioTypes();
                }
            }

        };


        self.sortScenarioTypes();



        //Period Compares Block

        this.sortedPeriods = ko.observableArray(self.yieldCurveShift().yieldCurveShiftPeriodCompares());
        this.sortedHistoricDates = ko.observableArray(self.yieldCurveShift().yieldCurveShiftHistoricDates())

      //  this.sortPeriodCompares = function () {
        //    self.sortedPeriods(self.yieldCurveShift().yieldCurveShiftPeriodCompares().sort(function (a, b) { return a.priority() - b.priority(); }));
       // };

        this.sortHistoricDates = function () {
            self.sortedHistoricDates(self.yieldCurveShift().yieldCurveShiftHistoricDates().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.updateHistoricDates = function () {
            self.sortedHistoricDates(self.yieldCurveShift().yieldCurveShiftHistoricDates());
        }


     //   this.yieldCurveShift().yieldCurveShiftPeriodCompares().sort(function (a, b) { return a.priority() - b.priority(); });

       /* this.removePeriodCompare = function (periodCompare) {

            var msg = 'Delete Period Compare "' + periodCompare.priority() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    periodCompare.entityAspect.setDeleted();
                    yieldCurveShift().yieldCurveShiftPeriodCompares().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortPeriodCompares();

                    if (yieldCurveShift().yieldCurveShiftPeriodCompares().length < 6) {
                        $('#addComparePeriod').removeClass('disabled');
                    }
                }
            }

        };*/

        this.removeHistoricDate = function (histCompare) {

            var msg = 'Delete Period Compare "' + histCompare.webRateAsOfDate() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    histCompare.entityAspect.setDeleted();

                    self.updateHistoricDates();

                    //if (yieldCurveShift().yieldCurveShiftPeriodCompares().length < 6) {
                     //   $('#addComparePeriod').removeClass('disabled');
                    //}
                }
            }

        };

        this.compareIndeces = [
            'CMT 1 Month',
            'CMT 3 Month',
            'CMT 6 Month',
            'CMT 1 Year',
            'CMT 2 Year',
            'CMT 3 Year',
            'CMT 5 Year',
            'CMT 7 Year',
            'CMT 10 Year',
            'CMT 20 Year',
            'CMT 30 Year',
            'FNMA 10Yr 60D',
            'FNMA 15Yr 60D',
            'FNMA 20Yr 60D',
            'FNMA 30Yr 60D',
            'FHLB Boston 1 Month',
            'FHLB Boston 3 Month',
            'FHLB Boston 6 Month',
            'FHLB Boston 1 Year',
            'FHLB Boston 2 Year',
            'FHLB Boston 3 Year',
            'FHLB Boston 4 Year',
            'FHLB Boston 5 Year',
            'FHLB Boston 6 Year',
            'FHLB Boston 7 Year',
            'FHLB Boston 8 Year',
            'FHLB Boston 9 Year',
            'FHLB Boston 10 Year',
            'FHLB Boston 15 Year',
            'FHLB Boston 20 Year'
        ];



   /*     this.addPeriodCompare = function (offSet) {
            var newComparePeriod = globalContext.createYieldCurveShiftPeriodCompare();
            newComparePeriod.indexA('CMT 1 Month');
            newComparePeriod.indexB('CMT 1 Month');
            newComparePeriod.priority(yieldCurveShift().yieldCurveShiftPeriodCompares().length);
            yieldCurveShift().yieldCurveShiftPeriodCompares().push(newComparePeriod);
            self.sortPeriodCompares();
            if (yieldCurveShift().yieldCurveShiftPeriodCompares().length == 6) {
                $('#addComparePeriod').addClass('disabled');
            }
        };*/

        this.addHistoricDate = function (offSet) {
            var newDate = globalContext.createYieldCurveShiftHistoricDate();
            newDate.priority(yieldCurveShift().yieldCurveShiftPeriodCompares().length);
            yieldCurveShift().yieldCurveShiftPeriodCompares().push(newDate);
            self.updateHistoricDates();
        };


       // self.sortPeriodCompares();
      //  self.sortHistoricDates();

        //To Do: Add A Call that Gets The Latest Dates in String Format From Web Rates
        this.webRatesDates = ko.observableArray();
        this.historicWebRates = ko.observableArray();

        this.fedRatesDates = ko.observableArray();

        this.indexes = ko.observableArray(['Treasury', 'LIBOR', 'FHLB']);

        this.advancedIndexes = ko.observableArray(['Atlanta', 'Boston', 'Chicago', 'Cincinnati', 'Dallas', 'Des Moines', 'Indianapolis', 'New York', 'Pittsburgh', 'San Francisco', 'Seattle', 'Topeka']);

        //To Do Pull These dates based off the the index and FHLB Advances district. Pull all dates in  the system for specified rate
        //this.historicDates = ko.observableArray(['9/30/2014', '8/31/2014', '7/31/2014', '6/30/2014']);

        this.periods = ko.observableArray([{ label: '3 Month', index: 0 }, { label: '6 Month', index: 1 }, { label: '1 Year', index: 2 }, { label: '2 Year', index: 3 }, { label: '3 Year', index: 4 }, { label: '4 Year', index: 5 }, { label: '5 Year', index: 6 }, { label: '7 Year', index: 7 }, { label: '10 Year', index: 8 }, { label: '15 Year', index: 9 }, { label: '20 Year', index: 10 }, { label: '30 Year', index: 11 }]);
        this.currentLiab = ko.observable();

        this.global = global;


        this.scenarioTypes = ko.observableArray();


        this.checkDefaults =function(){
            if (!this.yieldCurveShift().overrideDistrict()) {
                this.yieldCurveShift().fHLBAdvanceDistrict(this.currentLiab().fHLBDistrict());
            }

            if (!this.yieldCurveShift().overrideWebRateDate()) {
                this.yieldCurveShift().webRateAsOfDate(this.currentLiab().webRateAsOfDate());
            }
        }

        this.activate = function (id) {
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return globalContext.getWebRateScenarios(self.scenarioTypes).then(function () {
                return globalContext.getWebRateAsOfDate(self.webRatesDates).then(function () {
                    return globalContext.getFedRatesDates(self.fedRatesDates).then(function () {
                        return globalContext.getLiabilityPricing(self.currentLiab).then(function () {
                            //If length is > 0 must be an existing one so sort and silent save so the priority is updated
                            if (self.yieldCurveShift().yieldCurveShiftHistoricDates().length > 0) {
                                self.sortHistoricDates();
                                globalContext.saveChangesQuiet();
                            }


                            self.yieldCurveShift().overrideWebRateDate.subscribe(function (newValue) {
                                self.checkDefaults();
                            });

                            self.yieldCurveShift().overrideDistrict.subscribe(function (newValue) {
                                self.checkDefaults();
                            });

                           
                            self.historicWebRates(self.webRatesDates.slice(0));

                            self.historicWebRates().unshift({ asOfDate: "Current", description: "Current" });

                            self.availableScenarioTypes = ko.computed(function () {
                                var scenarioTypeIdsInUse = self.yieldCurveShift().yieldCurveShiftScenarioTypes().map(function (coreFundScenarioType) {
                                    return coreFundScenarioType.webRateScenarioTypeId();
                                });
                                var result = [];
                                self.scenarioTypes().forEach(function (scenarioType) {

                                    result.push({
                                        id: scenarioType.id(),
                                        name: scenarioType.name(),
                                        inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) !== -1
                                    });
                                });
                                return result;
                            });

                            self.checkDefaults();
                            if (self.yieldCurveShift().id() < 0) {
                               // self.addScenarioType(self.availableScenarioTypes().find(function (x) { return x.name === 'Base Scenario' }));
                            }
                            else {
                                globalContext.saveChangesQuiet();
                            }
                            
                            self.sortScenarioTypes();
                            global.loadingReport(false);
                       // self.compareDateCheck = ko.computed(function () {
                         //   return new Date(self.yieldCurveShift().compareStartDate()) > new Date(self.yieldCurveShift().compareEndDate());
                       // });


                        })

                    });
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);

            if (self.yieldCurveShift().id() < 0) {

            }
        };

        this.compositionComplete = function () {
            if (self.yieldCurveShift().id() < 0) {
                self.addScenarioType(self.availableScenarioTypes().find(function (x) { return x.name === 'Base Scenario' }));
            }
        }

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        return this;
    };

    return yieldCurveShiftVm;
});
