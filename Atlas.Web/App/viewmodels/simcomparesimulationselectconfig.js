﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var simCompareSimulationSelectVm = function (institutionDatabaseName, asOfDateOffset, simulationTypeIdToBindTo) {
        var self = this;
        var tempArr = [];

        //this.institutionDatabaseName = ko.observable(institutionDatabaseName);
        this.institutionDatabaseName = ko.computed({
            read: function () {
                return institutionDatabaseName();
            },
            write: function (value) {
                institutionDatabaseName(value);
                globalContext.getAsOfDateOffsets(self.asOfDateOffsets, value);
            }
        });

        this.simulationTypeId = ko.computed({
            read: function(){
                return simulationTypeIdToBindTo();
            },
            write: function (value) {
                simulationTypeIdToBindTo(value);
            }
        });
        this.asOfDateOffset = asOfDateOffset;  

        this.compositionComplete = function () {
            //self.sortSimCompareSimulationTypes();
        }

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.activate = function () {
            return globalContext.getSimulationTypes(self.simulationTypes).then(function () {
                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    self.availableSimulations = ko.computed(function () {
                        var simulationTypeIdsInUse = [[], []];
                        var result = [];

                        self.simulationTypes().forEach(function (simulationType) {
                            result.push({
                                id: simulationType.id(),
                                name: simulationType.name(),
                                //inUse: isSimulationTypeInUse(simulationTypeIdsInUse, simulationType.id())
                            });

                        });

                        return result;

                    });
                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.institutionDatabaseName()).then(function () { });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            //self.instSub.dispose();
        };

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (chartSimulationType, index) {
                chartSimulationType.priority(index);

            });
        };

        return this;
    };

    return simCompareSimulationSelectVm;
})