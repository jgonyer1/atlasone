﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var summaryRateVm = function (id) {
        var self = this;
        this.summaryRateViewData = ko.observable();

        this.compositionComplete = function () {   
            
            var tbl = self.summaryRateViewData().tableData;
            var scens = self.summaryRateViewData().scenario;
            var formatData = {};
            var headRow = '<tr><th>Indices</th><th>Curr. Rates</th>';

            for (var s = 0; s < scens.length; s++) {
                headRow += '<th>' + scens[s] + '</th>';
            }

            headRow += '</tr>';
          
            $('#rateHeader').append(headRow);

            for (var i = 0; i < tbl.length; i++) {
                var rec = tbl[i];

                if (formatData[rec.name] == undefined) {
                    formatData[rec.name] = [];

                    for (var c = 0; c < scens.length; c++) {
                        formatData[rec.name][c + 1] = '';
                    }
                }

                formatData[rec.name][0] = numeral(rec.curRate).format('0.00');
                formatData[rec.name][scens.indexOf(rec.scenName) + 1] = numeral(rec.bpMove).format() + ' (' + rec.monthStart + ' - ' + rec.monthEnd + ') ' + numeral(rec.endRate).format('0.00');
            }

            

            for (var id in formatData) {
                var row = '<tr><td>' + id + '</td>';

                for (var z = 0; z < formatData[id].length; z++) {
                    if (z != 0 && (formatData[id][z] == undefined || formatData[id][z] == '')) {
                        row += '<td>' + '0  (0 - 0) ' + formatData[id][0] + '</td>';
                    }
                    else {
                        row += '<td>' + formatData[id][z] + '</td>';
                    }
                }

                row += '</tr>';
                $('#rateBody').append(row);
            }

            //$("#summmaryrate-view").height($("#summmaryrate-view").height());
            global.loadingReport(false);
            //Export To Pdf
            if (typeof hiqPdfInfo != "undefined") {
                hiqPdfConverter.startConversion();
                $('#reportLoaded').text('1');
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getSummaryRateViewDataById(id, self.summaryRateViewData).then(function () {
                var viewData = self.summaryRateViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
            });
        };
    };

    return summaryRateVm;
});
