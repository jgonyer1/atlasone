﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var balanceSheetCompareVm = function (balanceSheetCompare) {
        var self = this;
        var curPage = "BalanceSheetCompare";
        //Report
        this.balanceSheetCompare = balanceSheetCompare;

        //Vars
        this.institutions = ko.observableArray();
        this.global = global;
        this.leftAsOfDateOffsets = ko.observableArray();
        this.rightAsOfDateOffsets = ko.observableArray();
        this.leftPolicyOffsets = ko.observableArray();
        this.rightPolicyOffsets = ko.observableArray();
        this.leftSimulationTypes = ko.observableArray();
        this.leftScenarioTypes = ko.observableArray();

        this.rightSimulationTypes = ko.observableArray();
        this.rightScenarioTypes = ko.observableArray();

        this.group = ko.observable();
        this.definedGroup = ko.observable();
        this.definedGroupings = ko.observableArray();

        this.modelSetup = ko.observable();
        //holds all of the subscriptions
        this.subs = [];

        //Navigation
        this.cancel = function () { };

        this.viewOptions = ko.observableArray();

        this.setOptionDisable = function (option, item) {
            ko.applyBindingsToNode(option, { disable: item.disable }, item);
        }

        this.defaultLeftStart_EndDate = function () {
            var o = self.balanceSheetCompare().leftAsOfDateOffset();
            self.balanceSheetCompare().leftStartDate(self.leftAsOfDateOffsets()[o].asOfDateDescript);
            self.balanceSheetCompare().leftEndDate(self.leftAsOfDateOffsets()[o].asOfDateDescript);
        };
        this.defaultRightStart_EndDate = function () {
            var ro = self.balanceSheetCompare().rightAsOfDateOffset();
            self.balanceSheetCompare().rightStartDate(self.rightAsOfDateOffsets()[ro].asOfDateDescript);
            self.balanceSheetCompare().rightEndDate(self.rightAsOfDateOffsets()[ro].asOfDateDescript);
        };

        this.refreshLeftScen = function () {
            globalContext.getAvailableSimulationScenarios(self.leftScenarioTypes, self.balanceSheetCompare().leftInstitutionDatabaseName(), self.balanceSheetCompare().leftAsOfDateOffset(), self.balanceSheetCompare().leftSimulationTypeId(), false)
        }

        this.refreshLeftSim = function () {
            globalContext.getAvailableSimulations(self.leftSimulationTypes, self.balanceSheetCompare().leftInstitutionDatabaseName(), self.balanceSheetCompare().leftAsOfDateOffset()).then(function () {
                self.refreshLeftScen();
            });
        }

        this.refreshRightScen = function () {
            globalContext.getAvailableSimulationScenarios(self.rightScenarioTypes, self.balanceSheetCompare().rightInstitutionDatabaseName(), self.balanceSheetCompare().rightAsOfDateOffset(), self.balanceSheetCompare().rightSimulationTypeId(), false)
        }

        this.refreshRightSim = function () {
            globalContext.getAvailableSimulations(self.rightSimulationTypes, self.balanceSheetCompare().rightInstitutionDatabaseName(), self.balanceSheetCompare().rightAsOfDateOffset()).then(function () {
                self.refreshRightScen();
            });
        }

        this.compositionComplete = function () {
            if (self.balanceSheetCompare().id() > -1) {
                globalContext.saveChangesQuiet();
            }
        };

        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getAvailableSimulations(self.leftSimulationTypes, self.balanceSheetCompare().leftInstitutionDatabaseName(), self.balanceSheetCompare().leftAsOfDateOffset()).then(function () {
                return globalContext.getAvailableSimulationScenarios(self.leftScenarioTypes, self.balanceSheetCompare().leftInstitutionDatabaseName(), self.balanceSheetCompare().leftAsOfDateOffset(), self.balanceSheetCompare().leftSimulationTypeId(), false).then(function () {
                    return globalContext.getAvailableSimulations(self.rightSimulationTypes, self.balanceSheetCompare().rightInstitutionDatabaseName(), self.balanceSheetCompare().rightAsOfDateOffset()).then(function () {
                        return globalContext.getAvailableSimulationScenarios(self.rightScenarioTypes, self.balanceSheetCompare().rightInstitutionDatabaseName(), self.balanceSheetCompare().rightAsOfDateOffset(), self.balanceSheetCompare().rightSimulationTypeId(), false).then(function () {


                            return globalContext.getAvailableBanks(self.institutions);
                        }).then(function () {
                            return globalContext.configDefinedGroupings(self.definedGroupings);
                        }).then(function () {
                            return globalContext.getModelSetup(self.modelSetup);
                        }).then(function () {

                            //tax equiv sync
                            if (!self.balanceSheetCompare().overrideTaxEquiv()) {
                                self.balanceSheetCompare().taxEqivalentYields(self.modelSetup().reportTaxEquivalentYield());
                                if (self.balanceSheetCompare().id() > 0) {
                                    globalContext.saveChangesQuiet();
                                }
                            }
                            self.subs.push(self.balanceSheetCompare().overrideTaxEquiv.subscribe(function (newValue) {
                                globalContext.logEvent("OverrideTaxEquiv set to " + newValue, curPage);
                                if (!newValue) {
                                    self.balanceSheetCompare().taxEqivalentYields(self.modelSetup().reportTaxEquivalentYield());
                                }
                            }));



                            var arr = [];
                            arr.push({
                                name: "Standard View", options: [
                                    { name: "Sub Accounts", id: "0" },
                                    { name: "Masters", id: "1" },
                                    { name: "Classifications", id: "2" },
                                    { name: "Account Type", id: "3" }
                                ]
                            });
                            arr.push({
                                name: "Defined Groupings", options: self.definedGroupings()
                            });
                            self.viewOptions(global.getGroupedSelectOptions(arr));




                            return Q.all([
                                globalContext.getAsOfDateOffsets(self.leftAsOfDateOffsets, self.balanceSheetCompare().leftInstitutionDatabaseName()),
                                globalContext.getAsOfDateOffsets(self.rightAsOfDateOffsets, self.balanceSheetCompare().rightInstitutionDatabaseName()),
                                globalContext.getPolicyOffsets(self.leftPolicyOffsets, self.balanceSheetCompare().leftInstitutionDatabaseName()),
                                globalContext.getPolicyOffsets(self.rightPolicyOffsets, self.balanceSheetCompare().rightInstitutionDatabaseName())
                            ]).then(function () {


                                //AsOfDateOffset Handling (Double-Institution Template)
                                self.subs.push(self.balanceSheetCompare().leftInstitutionDatabaseName.subscribe(function (newValue) {
                                    return Q.all([
                                        globalContext.getAsOfDateOffsets(self.leftAsOfDateOffsets, newValue),
                                        globalContext.getPolicyOffsets(self.leftPolicyOffsets, newValue)
                                    ]).then(function () {
                                        self.refreshLeftSim();
                                        self.defaultLeftSimulationScenario();
                                    });
                                }));
                                self.subs.push(self.balanceSheetCompare().rightInstitutionDatabaseName.subscribe(function (newValue) {
                                    return Q.all([
                                        globalContext.getAsOfDateOffsets(self.rightAsOfDateOffsets, newValue),
                                        globalContext.getPolicyOffsets(self.rightPolicyOffsets, newValue)
                                    ]).then(function () {
                                        self.refreshRightSim();
                                        self.defaultRightSimulationScenario();
                                    });
                                }));

                                self.subs.push(self.balanceSheetCompare().leftAsOfDateOffset.subscribe(function (newValue) {
                                    //self.balanceSheetCompare().leftStartDate(self.leftAsOfDateOffsets()[newValue].asOfDateDescript);
                                    //self.balanceSheetCompare().leftEndDate(self.leftAsOfDateOffsets()[newValue].asOfDateDescript);
                                    self.defaultLeftStart_EndDate();
                                    self.refreshLeftSim();
                                    self.defaultLeftSimulationScenario();
                                }));
                                self.subs.push(self.balanceSheetCompare().rightAsOfDateOffset.subscribe(function (newValue) {
                                    //self.balanceSheetCompare().rightStartDate(self.rightAsOfDateOffsets()[newValue].asOfDateDescript);
                                    //self.balanceSheetCompare().rightEndDate(self.rightAsOfDateOffsets()[newValue].asOfDateDescript);
                                    self.defaultRightStart_EndDate();
                                    self.refreshRightSim();
                                    self.defaultRightSimulationScenario();
                                }));

                                self.subs.push(self.balanceSheetCompare().leftOverrideSimulationId.subscribe(function (newValue) {
                                    globalContext.logEvent("LeftOverrideSimulationId set to " + newValue, curPage);
                                    self.defaultLeftSimulationScenario();
                                }));
                                self.subs.push(self.balanceSheetCompare().leftOverrideScenarioId.subscribe(function (newValue) {
                                    globalContext.logEvent("LeftOverrideScenarioId set to " + newValue, curPage);
                                    self.defaultLeftSimulationScenario();
                                }));


                                self.subs.push(self.balanceSheetCompare().rightOverrideSimulationId.subscribe(function (newValue) {
                                    globalContext.logEvent("RightOverrideSimulationId set to " + newValue, curPage);
                                    self.defaultRightSimulationScenario();
                                }));
                                self.subs.push(self.balanceSheetCompare().rightOverrideScenarioId.subscribe(function (newValue) {
                                    globalContext.logEvent("RightOverrideScenarioId set to " + newValue, curPage);
                                    self.defaultRightSimulationScenario();
                                }));

                                //Add subs to simulation left and right

                                self.subs.push(self.balanceSheetCompare().leftSimulationTypeId.subscribe(function (newValue) {
                                    self.refreshLeftScen();
                                }));
                                self.subs.push(self.balanceSheetCompare().rightSimulationTypeId.subscribe(function (newValue) {
                                    self.refreshRightScen();
                                }));




                                //DEfault them on loa
                                self.defaultLeftSimulationScenario();
                                self.defaultRightSimulationScenario();
                                //self.defaultLeftStart_EndDate();
                                //self.defaultRightStart_EndDate();

                                global.loadingReport(false);

                            });
                        });

                    });
                });
            });
        };



        this.defaultLeftSimulationScenario = function () {
            //Based off off set find corresponding policy
            var aod = self.leftAsOfDateOffsets()[self.balanceSheetCompare().leftAsOfDateOffset()].asOfDateDescript;
            if (!self.balanceSheetCompare().leftOverrideSimulationId()) {
                if (self.leftPolicyOffsets()[0][aod] && self.leftPolicyOffsets()[0][aod][0] != undefined) {
                    self.balanceSheetCompare().leftSimulationTypeId(self.leftPolicyOffsets()[0][aod][0].niiId);
                }
            }
            if (!self.balanceSheetCompare().leftOverrideScenarioId()) {
                if (self.leftPolicyOffsets()[0][aod] && self.leftPolicyOffsets()[0][aod][0] != undefined) {
                    var arr = self.leftScenarioTypes().filter(function (obj) {
                        return obj.id() == "1"
                    });
                    if (self.leftPolicyOffsets()[0][aod][0].bsSimulation == self.leftPolicyOffsets()[0][aod][0].niiId) {
                        self.balanceSheetCompare().leftScenarioTypeId(self.leftPolicyOffsets()[0][aod][0].bsScenario);
                    } else if (arr.length > 0) {
                        self.balanceSheetCompare().leftScenarioTypeId("1");
                    } else {
                        self.balanceSheetCompare().leftScenarioTypeId(self.leftScenarioTypes()[0].id);
                    }
                }
            }
        }
        this.defaultRightSimulationScenario = function () {
            var aod = self.rightAsOfDateOffsets()[self.balanceSheetCompare().rightAsOfDateOffset()].asOfDateDescript;
            //Loop through offsets and find the as ofdate that we want
            if (!self.balanceSheetCompare().rightOverrideSimulationId()) {

                if (self.rightPolicyOffsets()[0][aod] && self.rightPolicyOffsets()[0][aod][0]) {
                    self.balanceSheetCompare().rightSimulationTypeId(self.rightPolicyOffsets()[0][aod][0].niiId);
                }
            }
            if (!self.balanceSheetCompare().rightOverrideScenarioId()) {
                if (self.rightPolicyOffsets()[0][aod] && self.rightPolicyOffsets()[0][aod][0]) {
                    var arr = self.rightScenarioTypes().filter(function (obj) {
                        return obj.id() == "1"
                    });
                    if (self.rightPolicyOffsets()[0][aod][0].bsSimulation == self.rightPolicyOffsets()[0][aod][0].niiId) {
                        self.balanceSheetCompare().rightScenarioTypeId(self.rightPolicyOffsets()[0][aod][0].bsScenario);
                    } else if (arr.length > 0) {
                        self.balanceSheetCompare().rightScenarioTypeId("1");
                    } else {
                        self.balanceSheetCompare().rightScenarioTypeId(self.rightScenarioTypes()[0].id);
                    }
                }
            }
        }


        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Double-Institution Template)
            //self.leftInstSub.dispose();
            //self.rightInstSub.dispose();
            for (var i = 0; i < self.subs.length; i++) {
                self.subs[i].dispose();
            }
        };

        return this;
    };

    return balanceSheetCompareVm;
});
