﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app',
    'viewmodels/chartconfig'
],
function (globalContext, global, app, chartconfig) {
    var histNii = ko.observable();
    var historicalBalanceSheetNIIVm = function (historicalBalanceSheetNII) {
        var self = this;
        var curPage = "HistBalSheetNiiConfig";
        histNii = historicalBalanceSheetNII;

        this.subs = [];
        this.modelSetup = ko.observable();
        this.historicalBalanceSheetNII = historicalBalanceSheetNII;
        this.policyOffsets = ko.observableArray();

        this.graphSelection = ko.observableArray([{ id: 1, name: 'Scenario' }, { id: 2, name: 'Type' }]);

        function onChangesCanceled() {
            self.sortHistoricalNiiSimulations();
            self.sortHistoricalNiiScenarios();

            self.leftChartVM(recalcChart(false));
            self.rightChartVM(recalcChart(true));
        }

        this.cancel = function () {
        };

        //this.viewOptions = ko.observableArray([{ title: 'Account Type', value: '0', disable: false }]);
        this.viewOptions = ko.observableArray();
        this.niiGroupings = ko.observableArray();
        //this.niiGroupings = ko.observableArray([{ title: 'Account Type', value: '0', disable: false }]);
        this.setOptionDisable = function (option, item) {
            ko.applyBindingsToNode(option, { disable: item.disable }, item);
        }

        this.sortedLeftChartScenarioTypes = ko.observableArray();
        this.sortedRightChartScenarioTypes = ko.observableArray();

        this.sortedSimulations = ko.observableArray(self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations());

        this.defaultSimulation = function (histBalSim) {
            if (!histBalSim.overrideSimulation()) {
                var aod = self.asOfDateOffsets()[histBalSim.asOfDateOffset()].asOfDateDescript;
                if (self.policyOffsets()[0][aod] != undefined) {
                    histBalSim.simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                } else {
                    histBalSim.simulationTypeId(1);
                    app.showMessage("No Policies found for " + aod + ", defaulting to Base Simulation", "Error", ["OK"]);
                }

            }
        };

        this.defaultChartSimulation = function (chart) {
            if (!chart.overrideParentSimulationType() && chart.chartSimulationTypes().length > 0) {
                var aod = self.asOfDateOffsets()[chart.asOfDateOffset()].asOfDateDescript;
                if (self.policyOffsets()[0][aod] != undefined) {
                    chart.chartSimulationTypes()[0].simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                } else {
                    chart.simulationTypeId(1);
                    app.showMessage("No Policies found for " + aod + ", defaulting to Base Simulation", "Error", ["OK"]);
                }
            }
        };

        //Top Foot Notes Block
        this.sortedTopFootNotes = ko.observableArray(self.historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes());

        this.sortTopFootNotes = function () {
            self.sortedTopFootNotes(self.historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        //sort simulations
        this.sortHistoricalNiiSimulations = function () {
            self.sortedSimulations(self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.removeHistoricalNiiSimulation = function (sim) {

            var msg = 'Delete Simulation/DateOffset ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    sim.entityAspect.setDeleted();
                    self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortHistoricalNiiSimulations();
                }
            }

        };

        this.addSimulation = function () {
            var newSim = globalContext.createHistoricalBalanceSheetNIISimulation();
            newSim.simulationTypes = ko.observableArray();
            self.refreshItemSim(newSim);
            newSim.priority(self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().length);
            self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().push(newSim);
            self.sortHistoricalNiiSimulations();
            self.defaultSimulation(newSim);
            self.subs.push(newSim.asOfDateOffset.subscribe(function (newVal) {
                self.refreshItemSim(newSim);
                self.defaultSimulation(newSim);
            }, newSim));
            self.subs.push(newSim.overrideSimulation.subscribe(function (newVal) {
                globalContext.logEvent("HistBalSheetNiiSimulation overrideSimulation set to " + newValue, curPage);
                if (!newVal) {
                    self.defaultSimulation(this);
                }
            }, newSim));
        };

        //scenario types

        this.sortedScenarioTypes = ko.observableArray(self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios());

        //sort scenarios
        this.sortHistoricalNiiScenarios = function () {
            self.sortedScenarioTypes(self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios().sort(function (a, b) { return a.priority() - b.priority(); }));
        };


        this.removeScenarioType = function (scen) {

            var msg = 'Delete Scenario? ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    scen.entityAspect.setDeleted();
                    self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortHistoricalNiiScenarios();
                }
            }

        };

        this.addScenarioType = function (scenarioType) {
            var newScen = globalContext.createHistoricalBalanceSheetNIIScenario();
            newScen.scenarioTypeId(scenarioType.id);
            newScen.priority(self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios().length);

            if (self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios().length == 0) {
                self.historicalBalanceSheetNII().comparativeScenario(scenarioType.id);
            }

            self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios().push(newScen);
            self.sortHistoricalNiiScenarios();
        };
        //going to use this as a 'dontAddSimulationType' function for now
        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };

        this.sortLeftChartScenarioTypes = function () {
            var sorted = self.historicalBalanceSheetNII().leftChart().chartScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });
            self.sortedLeftChartScenarioTypes(sorted);
        };

        this.addLeftScenarioType = function (scenarioType) {
            var newChartScenarioType = globalContext.createChartScenarioType();
            newChartScenarioType.scenarioTypeId(scenarioType.id);
            newChartScenarioType.priority(self.historicalBalanceSheetNII().leftChart().chartScenarioTypes().length);
            self.historicalBalanceSheetNII().leftChart().chartScenarioTypes().push(newChartScenarioType);
            self.sortLeftChartScenarioTypes();
        };
        this.removeLeftScenarioType = function (scenarioType) {
            scenarioType.entityAspect.setDeleted();
            self.sortLeftChartScenarioTypes();
        };
        this.sortRightChartScenarioTypes = function () {
            var sorted = self.historicalBalanceSheetNII().rightChart().chartScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });
            self.sortedRightChartScenarioTypes(sorted);
        };
        this.addRightScenarioType = function (scenarioType) {
            var newChartScenarioType = globalContext.createChartScenarioType();
            newChartScenarioType.scenarioTypeId(scenarioType.id);
            newChartScenarioType.priority(self.historicalBalanceSheetNII().rightChart().chartScenarioTypes().length);
            self.historicalBalanceSheetNII().rightChart().chartScenarioTypes().push(newChartScenarioType);
            self.sortRightChartScenarioTypes();
        };
        this.removeRightScenarioType = function (scenarioType) {
            scenarioType.entityAspect.setDeleted();
            self.sortRightChartScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };

        function recalcChart(isRight) {
            return new chartconfig(
                histNii()[isRight ? "rightChart" : "leftChart"], ko.observable(),
                {
                    showPercChange: false,
                    showComparatives: false,
                    showInst: false,
                    callSave: !!isRight,
                    hideQuarterly: true,
                    canAddSimulations: false,
                    side: isRight ? "right" : "left"
                }
            );
        } 

        this.leftChartVM = ko.observable(recalcChart(false));
        this.rightChartVM = ko.observable(recalcChart(true));
        
        this.histBalSheetMonthlyClick = function () {
            self.historicalBalanceSheetNII().isQuarterly(false);
            self.historicalBalanceSheetNII().leftChart().isQuarterly(false);
            self.historicalBalanceSheetNII().rightChart().isQuarterly(false);
        };
        this.histBalSheetQuarterlyClick = function () {
            self.historicalBalanceSheetNII().isQuarterly(true);
            self.historicalBalanceSheetNII().rightChart().isQuarterly(true);
            self.historicalBalanceSheetNII().leftChart().isQuarterly(true);
        };

        this.scenarioConnectClass = "scenario-container";

        this.historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().sort(function (a, b) { return a.priority() - b.priority(); });

        this.removeTopFootNote = function (fn) {

            var msg = 'Delete Footnote ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    fn.entityAspect.setDeleted();
                    //Not Sure Why Binding was not pucking this up but this did the trick!
                    self.historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes.remove(fn);
                    self.historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortTopFootNotes();

                    //if (historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().length < 4) {
                    //   $('#addTopFootNoteButton').removeClass('disabled');
                    //}
                }
            }





        };

        this.addTopFootNote = function () {
            var tpn = globalContext.createHistoricBalanceSheetNIIBottomFootNote();
            tpn.priority(historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().length);
            self.historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().push(tpn);
            self.sortTopFootNotes();
            //if (historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes().length == 3) {
            //    $('#addTopFootNoteButton').addClass('disabled');
            //}

        };

        self.sortTopFootNotes();

        //Bottom Foot Notes Block
        this.sortedBottomFootNotes = ko.observableArray(self.historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes());

        this.sortBottomFootNotes = function () {
            self.sortedBottomFootNotes(self.historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().sort(function (a, b) { return a.priority() - b.priority(); });

        this.removeBottomFootNote = function (fn) {

            var msg = 'Delete Footnote ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    fn.entityAspect.setDeleted();

                    //Not Sure Why Binding was not pucking this up but this did the trick!
                    historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes.remove(fn);
                    historicalBalanceSheetNII().historicalBalanceSheetNIITopFootNotes().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortBottomFootNotes();

                    //if (historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes().length < 4) {
                    //$('#addBottomFootNoteButton').removeClass('disabled');
                    //}
                }
            }

        };

        this.addBottomFootNote = function () {
            var tpn = globalContext.createHistoricBalanceSheetNIITopFootNote();
            tpn.priority(historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes().length);
            self.historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes().push(tpn);
            self.sortBottomFootNotes();
            //if (historicalBalanceSheetNII().historicalBalanceSheetNIIBottomFootNotes.length == 3) {
            //    $('#addBottomFootNoteButton').addClass('disabled');
            //}

        };
        self.sortHistoricalNiiSimulations();
        self.sortHistoricalNiiScenarios();
        self.sortTopFootNotes();
        self.sortBottomFootNotes();


        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        //this.leftChartSortableAfterMove = function (arg) {
        //    arg.sourceParent().forEach(function (scenarioType, index) {
        //        scenarioType.priority(index);
        //    });
        //};
        //this.leftChartSortableAfterMove = function () { }

        //self.addSimulation();
        //self.addSimulation();

        this.balAssetGroup = ko.observable();
        this.balLiabGroup = ko.observable();
        this.niiAssetGroup = ko.observable();
        this.niiLiabGroup = ko.observable();

        this.balAssetDefinedGroup = ko.observable();
        this.balLiabDefinedGroup = ko.observable();
        this.niiAssetDefinedGroup = ko.observable();
        this.niiLiabDefinedGroup = ko.observable();

        this.definedGroupings = ko.observableArray();

        //functions to update the model's view options
        this.updateBalanceSheetAssetViewOption = function () {
            histNii().balanceSheetAssetViewOption(self.balAssetDefinedGroup());
            return true;
        }
        this.updateBalanceSheetLiabilityViewOption = function () {
            histNii().balanceSheetLiabilityViewOption(self.balLiabDefinedGroup());
            return true;
        }
        this.updateNiiAssetViewOption = function () {
            histNii().nIIAssetViewOption(self.niiAssetDefinedGroup());
            return true;
        }
        this.updateNiiLiabilityViewOption = function () {
            histNii().nIILiabilityViewOption(self.niiLiabDefinedGroup());
            return true;
        }

        //just returns the default defined group for a list
        this.defaultBalAssetDefinedGroup = function () {
            histNii().balanceSheetAssetViewOption(self.definedGroupings()[0].id);
            return true;
        }
        this.defaultBalLiabDefinedGroup = function () {
            histNii().balanceSheetLiabilityViewOption(self.definedGroupings()[0].id);
            return true;
        }
        this.defaultNiiAssetDefinedGroup = function () {
            histNii().nIIAssetViewOption(self.definedGroupings()[0].id);
            return true;
        }
        this.defaultNiiLiabDefinedGroup = function () {
            histNii().nIILiabilityViewOption(self.definedGroupings()[0].id);
            return true;
        }

        this.defaultTaxEquivalent = function () {
            if (!self.historicalBalanceSheetNII().overrideTaxEquiv()) {
                self.historicalBalanceSheetNII().taxEquivalent(self.modelSetup().reportTaxEquivalentYield());
            }
        };
        this.defaultTaxEquivIncome = function () {
            if (!self.historicalBalanceSheetNII().overrideChartsTaxEquivalent()) {
                self.historicalBalanceSheetNII().chartsTaxEquivalent(self.modelSetup().reportTaxEquivalent());
            }
        };

        //dsiable the respective defined groups
        this.disableBalAssetGroups = function () {
            this.balAssetGroup(false);
            return true;
        }
        this.disableBalLiabGroups = function () {
            this.balLiabGroup(false);
            return true;
        }
        this.disableNiiAssetGroups = function () {
            this.niiAssetGroup(false);
            return true;
        }
        this.disableNiiLiabGroups = function () {
            this.niiLiabGroup(false);
            return true;
        }

        this.simulationTypes = ko.observableArray();
        this.scenarioTypes = ko.observableArray();
        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();
        this.deactivate = function () {
            globalContext.logEvent("Viewed", curPage);
            app.off('application:cancelChanges', onChangesCanceled);

            if (self.subs) {
                for (var i = 0; i < self.subs.length; i++) {
                    self.subs[i].dispose();
                }
            }
        }

        this.compositionComplete = function () {

        };

        this.refreshItemSim = function (item) {
            console.log(item);
            globalContext.getAvailableSimulations(item.simulationTypes, self.historicalBalanceSheetNII().institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                console.log('got em');
                console.log(item.simulationTypes());
                self.resetScensAvailableScens();
            });
        }

        this.resetScensAvailableScens = function () {
            self.refreshScens().then(function () {
                console.log('setting them scenarios');
                console.log(self.scenarioTypes());

            });
        }

        this.refreshScens = function () {
            //loop through all offsets and get sim type sand opffets into array and pass into function to get distinct scenarios across all of them
            var sims = [];
            var offsets = [];
            var eveSims = [];
            self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().forEach(function (item, index, arr) {

                sims.push(item.simulationTypeId());
                eveSims.push(item.simulationTypeId());
                offsets.push(item.asOfDateOffset());

            });

            return globalContext.getAvailableSimulationScenariosMultiple(self.scenarioTypes, self.historicalBalanceSheetNII().institutionDatabaseName(), offsets.join(), sims.join(), eveSims.join());

        }

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);  
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                            return globalContext.getModelSetup(self.modelSetup).then(function () {
                                return globalContext.getPolicyOffsets(self.policyOffsets, self.historicalBalanceSheetNII().institutionDatabaseName()).then(function () {
                                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.historicalBalanceSheetNII().institutionDatabaseName()).then(function () {

                                        var arr = [];
                                        arr.push({
                                            name: "Standard View", options: [{ name: "Account Type", id: "0" }]
                                        });
                                        arr.push({ name: "Defined Groupings", options: self.definedGroupings() });
                                        self.viewOptions(global.getGroupedSelectOptions(arr));
                                        self.niiGroupings(global.getGroupedSelectOptions(arr));

                                        //for (var i = 0; i < self.definedGroupings().length; i++) {
                                        //    if (i == 0) {
                                        //        self.viewOptions().push({ title: "Defined Groupings", value: null, disable: true });
                                        //        self.niiGroupings().push({ title: "Defined Groupings", value: null, disable: true });
                                        //    }
                                        //    self.viewOptions().push({ title: self.definedGroupings()[i].name, value: self.definedGroupings()[i].id, disable: false })
                                        //    self.niiGroupings().push({ title: self.definedGroupings()[i].name, value: self.definedGroupings()[i].id, disable: false })
                                        //}


                                        //loop through offsets and set up simulationTypes array
                                        var offsetsProcessed = 0
                                        self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().forEach(function (item, index, arr) {

                                            item.simulationTypes = ko.observableArray();
                                            globalContext.getAvailableSimulations(item.simulationTypes, self.historicalBalanceSheetNII().institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                                                if (!item.overrideSimulation()) {
                                                    self.defaultSimulation(item);
                                                }
                                                self.subs.push(item.asOfDateOffset.subscribe(function (newVal) {
                                                    //update the simulation for the policy of the new asofdate offset
                                                    self.refreshItemSim(item);
                                                    self.defaultSimulation(item);

                                                }, item));

                                                self.subs.push(item.simulationTypeId.subscribe(function (newVal) {
                                                    self.refreshScens();
                                                }, item));


                                                self.subs.push(item.overrideSimulation.subscribe(function (newVal) {
                                                    globalContext.logEvent("HistBalSheetNiiSimulation " + item.id() + " overrideSimulation changed to " + newVal, curPage);
                                                    if (!newVal) {
                                                        self.refreshScens();
                                                        self.defaultSimulation(this);
                                                    }
                                                }, item));

                                                offsetsProcessed++;
                                                //when done this just move ona nd setorrest chagnes function
                                                if (offsetsProcessed == arr.length) {
                                                    //self.refreshScens().then(function () {
                                                    self.refreshScens(this);
                                                    // });
                                                    global.loadingReport(false);
                                                }
                                            });
                                        });

                                        self.defaultChartSimulation(self.historicalBalanceSheetNII().leftChart());
                                        self.defaultChartSimulation(self.historicalBalanceSheetNII().rightChart());
                                        self.availableScenarioTypes = ko.computed(function () {
                                            var scenarioTypeIdsInUse = self.historicalBalanceSheetNII().historicalBalanceSheetNIIScenarios().map(function (scenType) {
                                                return scenType.scenarioTypeId();
                                            });

                                            var scenarioTypeIdsInUseLeftChart = [];
                                            if (self.historicalBalanceSheetNII().leftChart()) {
                                                scenarioTypeIdsInUseLeftChart = self.historicalBalanceSheetNII().leftChart().chartScenarioTypes().map(function (scenType) {
                                                    return scenType.scenarioTypeId();
                                                });
                                            }
                                            var scenarioTypeIdsInUseRightChart = [];
                                            if (self.historicalBalanceSheetNII().rightChart()) {
                                                scenarioTypeIdsInUseRightChart = self.historicalBalanceSheetNII().rightChart().chartScenarioTypes().map(function (scenType) {
                                                    return scenType.scenarioTypeId();
                                                });
                                            }

                                            var result = [];
                                            self.scenarioTypes().forEach(function (scenarioType) {
                                                result.push({
                                                    id: scenarioType.id(),
                                                    name: scenarioType.name(),
                                                    inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1,
                                                    inUseLeft: scenarioTypeIdsInUseLeftChart.indexOf(scenarioType.id()) != -1,
                                                    inUseRight: scenarioTypeIdsInUseRightChart.indexOf(scenarioType.id()) != -1
                                                });
                                            });
                                            return result;
                                        });

                                        self.sortLeftChartScenarioTypes();
                                        self.sortRightChartScenarioTypes();

                                        //set the view model's defined grouping based on the view options
                                        if (self.historicalBalanceSheetNII().balanceSheetAssetViewOption().indexOf("dg_") > -1) {
                                            self.balAssetGroup(true);
                                            self.balAssetDefinedGroup(self.historicalBalanceSheetNII().balanceSheetAssetViewOption());
                                        } else {
                                            self.balAssetGroup(false);
                                        }

                                        if (self.historicalBalanceSheetNII().balanceSheetLiabilityViewOption().indexOf("dg_") > -1) {
                                            self.balLiabGroup(true);
                                            self.balLiabDefinedGroup(self.historicalBalanceSheetNII().balanceSheetLiabilityViewOption());
                                        } else {
                                            self.balLiabGroup(false);
                                        }

                                        if (self.historicalBalanceSheetNII().nIIAssetViewOption().indexOf("dg_") > -1) {
                                            self.niiAssetGroup(true);
                                            self.niiAssetDefinedGroup(self.historicalBalanceSheetNII().nIIAssetViewOption());
                                        } else {
                                            self.niiAssetGroup(false);
                                        }

                                        if (self.historicalBalanceSheetNII().nIILiabilityViewOption().indexOf("dg_") > -1) {
                                            self.niiLiabGroup(true);
                                            self.niiLiabDefinedGroup(self.historicalBalanceSheetNII().nIILiabilityViewOption());
                                        } else {
                                            self.niiLiabGroup(false);
                                        }

                                        self.subs.push(self.historicalBalanceSheetNII().institutionDatabaseName.subscribe(function (newValue) {
                                            globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.historicalBalanceSheetNII().institutionDatabaseName()).then(function () {
                                                globalContext.getPolicyOffsets(self.policyOffsets, self.historicalBalanceSheetNII().institutionDatabaseName()).then(function () {
                                                    //keep the chart inst db names synced up with the report db name
                                                    self.historicalBalanceSheetNII().leftChart().institutionDatabaseName(self.historicalBalanceSheetNII().institutionDatabaseName());
                                                    self.historicalBalanceSheetNII().rightChart().institutionDatabaseName(self.historicalBalanceSheetNII().institutionDatabaseName());
                                                    self.defaultChartSimulation(self.historicalBalanceSheetNII().leftChart());
                                                    self.defaultChartSimulation(self.historicalBalanceSheetNII().rightChart());
                                                    self.historicalBalanceSheetNII().historicalBalanceSheetNIISimulations().forEach(function (item, index, arr) {
                                                        if (!item.overrideSimulation()) {
                                                            self.defaultSimulation(item);
                                                        }
                                                    });
                                                });
                                            });

                                            
                                        }));

                                        self.defaultTaxEquivalent();
                                        self.defaultTaxEquivIncome();

                                        self.subs.push(self.historicalBalanceSheetNII().overrideTaxEquiv.subscribe(function (newValue) {
                                            globalContext.logEvent("OverrideTaxEquiv changed to " + newValue, curPage);
                                            self.defaultTaxEquivalent();
                                        }));


                                        self.subs.push(self.historicalBalanceSheetNII().overrideChartsTaxEquivalent.subscribe(function (newValue) {
                                            globalContext.logEvent("OverrideChartsTaxEquivalent changed to " + newValue, curPage);
                                            self.defaultTaxEquivIncome();
                                        }));
                                        self.subs.push(self.historicalBalanceSheetNII().leftChart().asOfDateOffset.subscribe(function () {
                                            self.defaultChartSimulation(self.historicalBalanceSheetNII().leftChart());
                                        }));
                                        self.subs.push(self.historicalBalanceSheetNII().rightChart().asOfDateOffset.subscribe(function () {
                                            self.defaultChartSimulation(self.historicalBalanceSheetNII().rightChart());
                                        }));

                                    }); //blank instName will just use profile institution
                                });                                
                            });
                    });
            });
        };

        return this;
    };

    return historicalBalanceSheetNIIVm;
});
