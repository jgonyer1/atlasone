﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var summaryRateVm = function (id) {
        var self = this;
        var curPage = "InvestmentPortfolioValuationView";
        this.ivpViewData = ko.observable();

        this.compositionComplete = function () {
          
            var tbl = self.ivpViewData().tblData;
            var scens = self.ivpViewData().scens;


            var formatData = {};

            for (var s = 0; s < scens.length; s++) {
                formatData[scens[s].ddwName] = {
                    currentPar: 0,
                    currentBook: 0,
                    marketValue: 0,
                    gain: 0,
                    name: scens[s].dispName,
                    isActive: false
                }
            }

           for (var i = 0; i < tbl.length; i++) {
               var rec = tbl[i];

               formatData[rec.scenario].currentPar = rec.parValue;
               formatData[rec.scenario].currentBook = rec.bookValue;
               formatData[rec.scenario].marketValue = rec.marketValue;
               formatData[rec.scenario].gain = rec.gainLoss;
               formatData[rec.scenario].isActive = true;
           }


           for (var s = 0; s < scens.length; s++) {
               if (formatData[scens[s].ddwName].isActive) {
                   var row = '<tr><td>' + scens[s].dispName + '</td>';
                   row += '<td>' + numeral(formatData[scens[s].ddwName].currentPar).format('0,0') + '</td>';
                   row += '<td>' + numeral(formatData[scens[s].ddwName].currentBook).format('0,0') + '</td>';
                   row += '<td>' + numeral(formatData[scens[s].ddwName].marketValue).format('0,0') + '</td>';
                   row += '<td>' + numeral(formatData[scens[s].ddwName].gain).format('(0,0)') + '</td>';
                   row += '</tr>';
                   $('#invBody').append(row);
               }

            }

           global.loadingReport(false);
           $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage); 
            }
            else {
                hiqPdfConverter.startConversion();
            }



        }



        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getInvestmentPortfolioValuationViewDataById(id, self.ivpViewData).then(function () {

            });
        };
    };

    return summaryRateVm;

});
