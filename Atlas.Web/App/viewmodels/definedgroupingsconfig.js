﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model'
],
function (globalContext, router, system, app, logger, model) {
    var definedGrouping = ko.observable();
    var isSaving = ko.observable(false);
    var curPage = "DefinedGroupingsConfig";
    function activate(id) {
        globalContext.logEvent("Viewed", curPage);  
        //Load All Of The Defined Groupings
        globalContext.clearCache();
        return globalContext.getDefinedGroupingById(id, definedGrouping).then(function () {
            sortGroups();
        });
    }

    var sortedGroups = ko.observableArray();

    var fixupGroupPriority = function () {
        sortedGroups().forEach(function (report, index) {
            report.priority(index);
        });
    };

    var groupCompare = function(a, b) {
        if (a.priority() > b.priority()) return 1; if (a.priority() < b.priority()) return -1; return 0;
    }

    var sortGroups = function () {
        sortedGroups(definedGrouping().definedGroupingGroups().sort(groupCompare));
    };

    var hasChanges = ko.computed(function () {
        return globalContext.hasChanges();
    });

    var cancel = function () {
        globalContext.cancelChanges();
        router.navigate('#/definedgroupings');
    };

    var canSave = ko.computed(function () {
        return hasChanges() && !isSaving();
    });

    var goToDefinedGroupings = function () {
        router.navigate('#/definedgroupings');
    };

    var addGroup = function () {
        router.navigate('#/definedgroupinggroupadd/' + definedGrouping().id() + "?groupPriority=" + definedGrouping().definedGroupingGroups().length);
    };

    var deleteDefinedGrouping = function () {
        globalContext.deleteDefinedGrouping(dg.id()).fin(function () {
            goToDefinedGroupings();
        });
    }

    var _deleteGroup = function (dg) {
        for (var i = dg.definedGroupingClassification().length - 1; i >= 0; i--) {
            dg.definedGroupingClassification()[i].entityAspect.setDeleted();
        }
        dg.entityAspect.setDeleted();

        globalContext.getUserName().then(userName => {
            definedGrouping().lastModifiedBy(userName);
            definedGrouping().lastModified(new Date());

            globalContext.saveChanges();
            sortGroups();
        }).fin(complete);

        function complete() {
            isSaving(false);
        }
    };

    var deleteGroup = function (dg) {
        var responses = ["OK", "Cancel"];
        app.showMessage("Do you want to delete group " + dg.name() + "?", "Alert", responses).then(function (r) {
            if (r == responses[0]) {
                _deleteGroup(dg);
            } else {
                return false;
            }
        });
    };

    var goToGroupConfig = function () {
        router.navigate('#/definedgroupinggroupconfig/' + this.id() );
    };

    var save = function () {
        return globalContext.getUserName()
            .then(userName => {
                definedGrouping().lastModifiedBy(userName);
                definedGrouping().lastModified(new Date());
            })
            .then(globalContext.saveChanges)
            .fin(complete);

        function complete() {
            isSaving(false);
        }
    };

    var vm = {
        definedGrouping: definedGrouping,
        goToDefinedGroupings: goToDefinedGroupings,
        activate: activate,
        save: save,
        canSave: canSave,
        cancel: cancel,
        hasChanges: hasChanges,
        sortedGroups: sortedGroups,
        fixupGroupPriority: fixupGroupPriority,
        deleteGroup: deleteGroup,
        goToGroupConfig: goToGroupConfig,
        addGroup: addGroup
    };

    return vm;
});
