﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var router = require('plugins/router');
    var files = ko.observableArray();
    var file = ko.observable();
    var reportId;
    var ckEditorNum;

    function activate(id, queryString) {
        if (queryString) {
            ckEditorNum = queryString.CKEditorFuncNum;
        }

        reportId = id;
        return globalContext.getFiles(files).then(function () {
            return true;
        }).fail(function (error) {
            console.log(error);
        });
    }

    function goToFileAdd() {
        router.navigate("#/fileadd");
    }

    function addToReport(data, event) {
        event.preventDefault();
        window.opener.CKEDITOR.tools.callFunction(ckEditorNum, '/breeze/global/ServeFile/' + data.id(), '');
        window.close();


        if (reportId)
            alert("add to report " + reportId);
    }

    function upload() {
        var file = this.file();
        name = file.name;
        size = file.size;
        type = file.type;

        if (file.name.length < 1) {

        }
        else if (file.size > 1000000) {
            alert("File is to big");
        }
        else if (file.type != 'image/png' && file.type != 'image/jpg' && !file.type != 'image/gif' && file.type != 'image/jpeg') {
            alert("File doesnt match png, jpg or gif");
        }
        else {
            var formData = new FormData();
            formData.append('file', file, file.name);
            formData.append('description', "this is a discription");
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 201) {
                    return globalContext.getFiles(files)
                }
            };
            xhr.open('POST', '/breeze/global/UploadFile', true);
            xhr.send(formData);
        }


    }


    return {
        activate: activate,
        goToFileAdd: goToFileAdd,
        addToReport: addToReport,
        files: files,
        file: file,
        upload: upload
    };
});