﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var liabVm = function (liab, rep) {
        var self = this;
        var curPage = "LiabilityPricingAnalysisConfig";
        this.liab = liab;

        this.thisReport = rep;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();
        this.allAsOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();
        this.scenarioTypes = ko.observableArray();

        this.compSimulationTypes = ko.observableArray();
        this.compScenarioTypes = ko.observableArray();

        this.pricing = ko.observable();
        this.policyOffsets = ko.observableArray();

        this.setDefaultSimScen = function () {
            var aod = self.asOfDateOffsets()[self.liab().asOfDateOffset()].asOfDateDescript;

            if (!self.liab().overrideSimulationTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.liab().simulationTypeId(self.policyOffsets()[0][aod][0].bsSimulation);
            }

            if (!self.liab().overrideScenarioTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.liab().scenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
            }
        }

        this.setDefaultCompSimScen = function () {
            var aod = self.asOfDateOffsets()[self.liab().comparativeDateOffset()].asOfDateDescript;
            if (!self.liab().overrideCompSimulationTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.liab().compSimulationTypeId(self.policyOffsets()[0][aod][0].bsSimulation);
            }

            if (!self.liab().overrideCompScenarioTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.liab().compScenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
            }
        }

        this.refreshLiabPricing = function () {
            globalContext.getInstLiabilityPricings(self.liab().asOfDateOffset(), self.liab().comparativeDateOffset(), self.liab().institutionDatabaseName(), self.pricing).then(function () {

            });
        }


        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.liab().institutionDatabaseName(), self.liab().asOfDateOffset(), self.liab().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.liab().institutionDatabaseName(), self.liab().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }

        this.refreshCompScen = function () {
            globalContext.getAvailableSimulationScenarios(self.compScenarioTypes, self.liab().institutionDatabaseName(), self.liab().comparativeDateOffset(), self.liab().compSimulationTypeId(), false)
        }

        this.refreshCompSim = function () {
            globalContext.getAvailableSimulations(self.compSimulationTypes, self.liab().institutionDatabaseName(), self.liab().comparativeDateOffset()).then(function () {
                self.refreshCompScen();
            });
        }



        this.mostRecentDate = ko.observable();

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            global.loadingReport(true);
            return globalContext.getInstLiabilityPricings(self.liab().asOfDateOffset(), self.liab().comparativeDateOffset(), self.liab().institutionDatabaseName(), self.pricing).then(function () {
                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return globalContext.getScenarioTypes(self.scenarioTypes).then(function () {
                        return globalContext.getAvailableSimulations(self.simulationTypes, self.liab().institutionDatabaseName(), self.liab().asOfDateOffset()).then(function () {
                            return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.liab().institutionDatabaseName(), self.liab().asOfDateOffset(), self.liab().simulationTypeId(), false).then(function () {
                                return globalContext.getAvailableSimulations(self.compSimulationTypes, self.liab().institutionDatabaseName(), self.liab().comparativeDateOffset()).then(function () {
                                    return globalContext.getAvailableSimulationScenarios(self.compScenarioTypes, self.liab().institutionDatabaseName(), self.liab().comparativeDateOffset(), self.liab().compSimulationTypeId(), false).then(function () {
                                        return globalContext.getPolicyOffsets(self.policyOffsets, self.liab().institutionDatabaseName()).then(function () {
                          
                                            //AsOfDateOffset Handling (Single-Institution Template)
                                            self.instSub = self.liab().institutionDatabaseName.subscribe(function (newValue) {
                                                globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                                                globalContext.getAsOfDateOffsetsCustomCount(self.allAsOfDateOffsets, newValue, 10000);
                                  
                                                globalContext.getPolicyOffsets(self.policyOffsets, self.liab().institutionDatabaseName()).then(function () {
                                                    self.refreshCompSim();
                                                    self.refreshSim();
                                                    self.setDefaultCompSimScen();
                                                    self.setDefaultSimScen();
                                                    self.refreshLiabPricing();
                                                });
                                            });

                                            //Date offset handling
                                            self.dateOffsetSub = self.liab().asOfDateOffset.subscribe(function (newValue) {
                                                self.refreshSim();
                                                self.setDefaultSimScen();
                                                self.refreshLiabPricing();
                                            });

                                            //Date offset handling
                                            self.compDateOffsetSub = self.liab().comparativeDateOffset.subscribe(function (newValue) {
                                                self.refreshCompSim();
                                                self.setDefaultCompSimScen();
                                                self.refreshLiabPricing();
                                            });


                                            self.compOverrideScenSub = self.liab().overrideCompScenarioTypeId.subscribe(function (newValue) {
                                                self.setDefaultCompSimScen();
                                            });

                                            self.compOverrideSimSub = self.liab().overrideCompSimulationTypeId.subscribe(function (newValue) {

                                                self.refreshCompScen();
                                                self.setDefaultCompSimScen();
                                            });

                                            self.compSimSub = self.liab().compSimulationTypeId.subscribe(function (newValue) {
                                                self.refreshCompScen();
                                            });


                                            self.simSub = self.liab().simulationTypeId.subscribe(function (newValue) {
                                                self.refreshScen();
                                            });

                                            self.overrideSimSub = self.liab().overrideSimulationTypeId.subscribe(function (newValue) {
                                                self.refreshScen();
                                                self.setDefaultSimScen();
                                            });

                                            self.overrideScenSub = self.liab().overrideScenarioTypeId.subscribe(function (newValue) {
                                                self.setDefaultSimScen();
                                            });
        
                                            return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.liab().institutionDatabaseName()).then(function () {
                                                return globalContext.getAsOfDateOffsetsCustomCount(self.allAsOfDateOffsets, self.liab().institutionDatabaseName(), 10000).then(function () {
                                                    self.setDefaultSimScen();
                                                    self.setDefaultCompSimScen();

                                                    if (self.liab().id() > 0) {
                                                        globalContext.saveChangesQuiet();
                                                    }
                                                    global.loadingReport(false);
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.dateOffsetSub.dispose();
            self.compDateOffsetSub.dispose();
            self.compOverrideSimSub.dispose();
            self.compOverrideScenSub.dispose();
            self.compSimSub.dispose();
            self.simSub.dispose();
            self.overrideSimSub.dispose();
            self.overrideScenSub.dispose();
        };


        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };
    };

    return liabVm;
});
