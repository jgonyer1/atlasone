﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var vm = function (id) {
        var self = this;
        this.dsa = ko.observable();
        var curPage = "DetailedSimulationAssumptionsView";

        this.compositionComplete = function () {
            numeral.zeroFormat('--');

            var firstAsset = true;
            var firstLiability = true;

            var previousType = '-1';
            var continueLeftBorder = false;
            var highlight = false;
            var outputPrevRecord = false;
            var alertme = false;
            for (var i = 0; i < self.dsa().tbl.length; i++) {
                var rec = self.dsa().tbl[i];

                if ((rec.rowType == '0' || rec.rowType == '1') && !((rec.balance != 0 && rec.balance != null) || (rec.newCashflowBalance != 0 && rec.newCashflowBalance != null))) {
                    continue;
                }

                var borderTop = false;
            
                if (previousType != '-1' && ((rec.rowType == '0' && previousType == '2'))){
                    borderTop = true;
                    continueLeftBorder = false;
                }

                //If just plain row always switch it no matter what
                //Always Flip if the row type is 0
                if (rec.rowType == '0' && ((rec.balance != 0 && rec.balance != null) || (rec.newCashflowBalance != 0 && rec.newCashflowBalance != null)) && outputPrevRecord) {
                    highlight = !highlight;
                }
                else if ((previousType == '0') && rec.rowType == '3') {
                    highlight = !highlight;
                }
                //If hitting return statement flip it
                else if (previousType == '1' && rec.rowType == '2') {
                    highlight = !highlight;
                }
                //If relationshop title flip it
                else if (rec.rowType == '3') {
                    highlight = !highlight;
                }
                //If first record after relationship title flip it
                else if (rec.rowType == '1' && previousType == '3') {
                    highlight = !highlight;
                }
                else if (rec.rowType == '3' && previousType == '2') {
                    highlight = !highlight;
                }


                if (rec.rowType == '3') {
                    borderTop = true;
                    continueLeftBorder = true;
                }

                if (rec.name == 'NMD migration / shift') {
                    debugger;
                }
                //added condition to make check for if first row in liabilities is a grouping and thereofre deos not have a isasset flag
                if (((rec.isAsset == 1 && firstAsset) || i == 0) || ((rec.isAsset == 0 && firstLiability) || (rec.isAsset == null && self.dsa().tbl[i + 1].isAsset == 0 && firstLiability))) {

                    var typeText = '';
                    if ((rec.isAsset == 1 && firstAsset) || i == 0) {
                        firstAsset = false;
                        typeText = 'Asset';
                    }

                    if ((rec.isAsset == 0 && firstLiability) || (rec.isAsset == null && self.dsa().tbl[i + 1].isAsset == 0 && firstLiability)) {
                        firstLiability = false;
                        typeText = 'Liability';
                    }
                    var typeHeaderRow = '<tr class="rowHeader"><td>' + typeText + '</td>';
                    typeHeaderRow += '<td class="borderLeftThick"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThick"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThick"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThick"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThick"></td>';
                    typeHeaderRow += '<td class="borderLeftThin"></td>';
                    typeHeaderRow += '<td class="borderLeftThick"></td>';
                    typeHeaderRow += '<td class="borderLeftThin borderRightThick"></td>';
                    typeHeaderRow += '</tr>';
                    $('#detailedBody').append(typeHeaderRow);
                }

                

                var row = '<tr class="rowType' + rec.rowType + ' ' + (borderTop ? 'rowBorderTop' : '') + ' ' + (highlight ? 'highlight' : '') + '">';
                //Row Type
                //0 == No Relat Cat
                //1 == Relat Cat
                //2 == Return
                //3 == Relat Title
                numeral.zeroFormat('--');
                row += '<td class="' + (continueLeftBorder ? 'borderLeftThickGrey' : '') + '">' + rec.name + '</td>';
                if (rec.rowType == '3') {
                    
                    row += '<td class="borderLeftThick"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThick"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThick"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThick"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThick"></td>';
                    row += '<td class="borderLeftThin"></td>';
                    row += '<td class="borderLeftThick"></td>';
                    row += '<td class="borderLeftThin borderRightThick"></td>';
                    row += '</tr>';
                    $('#detailedBody').append(row);
                    outputPrevRecord = true;
                    previousType = rec.rowType;
                    
                }
                else if (rec.rowType == '2') {

                    if (rec.name.indexOf('Total Cap') > -1 || rec.name.indexOf('Period Cap') > -1) {
                        row += '<td class="borderLeftThick">' + numeral(rec.balance / 1000).format('(0,0)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.rate).format('(0.00)') + ' </td>';
                        if (numeral(rec.balance).value() != 0) {
                            row += '<td class="borderLeftThin">' + rec.accrual + ' </td>';
                        }
                        else {
                            row += '<td class="borderLeftThin">--</td>';
                        }

                      
                    }
                    else {
                        row += '<td class="borderLeftThick">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                    }

                    if (rec.existingRepriceBalance != 0 && rec.existingRepriceBalance != '' && rec.name.indexOf('Itself') == -1) {
                        row += '<td class="borderLeftThick">' + (rec.repriceIndex == null ? '--' : rec.repriceIndex) + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.repriceOffset).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.repriceRate).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.repriceFreq).format('(0)') + ' </td>';

                        row += '<td class="borderLeftThin">' + numeral(rec.repriceChgCap).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.repriceChgFloor).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.repriceLifeCap).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.repriceLifeFloor).format('(0.00)') + ' </td>';
                    }
                    else {
                        row += '<td class="borderLeftThick">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';

                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                    }



                    if (rec.name.indexOf('Itself') > 0) {
                        // Three For New volume
                        row += '<td class="borderLeftThick">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                    }
                    else {
                        // Three For New volume
                        if (rec.newOffset != 0) {
                            row += '<td class="borderLeftThick">' + rec.newIndex + '</td>';
                            row += '<td class="borderLeftThin">' + numeral(rec.newOffset).format('(0.00)') + '</td>';
                            row += '<td class="borderLeftThin">' + numeral(rec.newRate).format('(0.00)') + '</td>';
                        }
                        else {
                            row += '<td class="borderLeftThick">--</td>';
                            row += '<td class="borderLeftThin">--</td>';
                            row += '<td class="borderLeftThin">--</td>';
                        }

                    }
                    if (rec.name.indexOf('Itself') > 0) {
                    //New Repricing
                    // if (rec.newRepricingBalance != 0 && rec.newRepricingBalance != null) {
                        row += '<td class="borderLeftThick">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThick">--</td>';
                        row += '<td class="borderLeftThin">--</td>';
                        row += '<td class="borderLeftThick">--</td>';
                        row += '<td class="borderLeftThin borderRightThick">--</td>';
                    }
                    else {
                        row += '<td class="borderLeftThick">' + (rec.newRepricingIndex == null ? '--' : rec.newRepricingIndex) + ' </td>';
                        row += '<td class="borderLeftThin">' + (rec.newRepricingIndex == null ? '--' : numeral(rec.newRepricingOffset).format('(0.00)')) + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepricingRate).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepricingFixedPer).format('(0)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepriceFreq).format('(0)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepriceChgCap).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepriceChgFloor).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepriceLifeCap).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.newRepriceLifeFloor).format('(0.00)') + ' </td>';

                        if (rec.newCashflowTerm == '0') {
                            row += '<td class="borderLeftThick">--</td>';
                        }
                        else {
                            row += '<td class="borderLeftThick">' + rec.newCashflowTerm + '</td>';
                        }
                       
                        row += '<td class="borderLeftThin">' + numeral(rec.newCashflowAmount).format('(0)') + '</td>';
                        if ((rec.balance != 0 && rec.balance != null)) {
                            row += '<td class="borderLeftThick">' + rec.preapyExisting + ' </td>';
                        }
                        else {
                            row += '<td class="borderLeftThick">--</td>';
                        }
                        row += '<td class="borderLeftThin  borderRightThick">' + rec.prepayNew + '</td>';
                    }

                    //Four for New Cashflow and Prepay Type
 

                    row += '</tr>';
                    $('#detailedBody').append(row);

                    outputPrevRecord = true;
                    previousType = rec.rowType;
                }
                else {
                    if ((rec.balance != 0 && rec.balance != null) || (rec.newCashflowBalance != 0 && rec.newCashflowBalance !=null)) {
                       // if (rec.balance != 0 ) {
                

                        row += '<td class="borderLeftThick">' + numeral(rec.balance / 1000).format('(0,0)') + ' </td>';
                        row += '<td class="borderLeftThin">' + numeral(rec.rate).format('(0.00)') + ' </td>';
                        row += '<td class="borderLeftThin">' + rec.accrual + ' </td>';

                            //Existing Reprice Section
                            if (rec.existingRepriceBalance != 0 && rec.existingRepriceBalance != '') {
                                row += '<td class="borderLeftThick">' + (rec.repriceIndex == null ? '--' : rec.repriceIndex) + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.repriceOffset).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.repriceRate).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.repriceFreq).format('(0)') + ' </td>';

                                row += '<td class="borderLeftThin">' + numeral(rec.repriceChgCap).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.repriceChgFloor).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.repriceLifeCap).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.repriceLifeFloor).format('(0.00)') + ' </td>';
                            }
                            else {
                                row += '<td class="borderLeftThick">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';

                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                            }

                            //New Volume Section
                            if (rec.newCashflowBalance != 0 && rec.newCashflowBalance != null) {
                                row += '<td class="borderLeftThick">' + (rec.newIndex == null ? '--' : rec.newIndex) + '</td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newOffset).format('(0.00)') + '</td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRate).format('(0.00)') + '</td>';


                                row += '<td class="borderLeftThick">' + (rec.newRepricingIndex == null ? '--' : rec.newRepricingIndex) + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRepricingOffset).format('(0.00)') + ' </td>';



                                row += '<td class="borderLeftThin">' + numeral(rec.newRepricingRate).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRepricingFixedPer).format('(0)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRepriceFreq).format('(0)') + ' </td>';

                                row += '<td class="borderLeftThin">' + numeral(rec.newRepriceChgCap).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRepriceChgFloor).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRepriceLifeCap).format('(0.00)') + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newRepriceLifeFloor).format('(0.00)') + ' </td>';

                                row += '<td class="borderLeftThick">' + rec.newCashflowTerm + ' </td>';
                                row += '<td class="borderLeftThin">' + numeral(rec.newCashflowAmount).format() + ' </td>';



                            }
                            else {
                                row += '<td class="borderLeftThick">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';

                                row += '<td class="borderLeftThick">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';

                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                                row += '<td class="borderLeftThin">--</td>';

                                row += '<td class="borderLeftThick">--</td>';
                                row += '<td class="borderLeftThin">--</td>';
                            }

                            if ((rec.balance != 0 && rec.balance != null)) {
                                row += '<td class="borderLeftThick">' + rec.preapyExisting + ' </td>';
                            }
                            else {
                                row += '<td class="borderLeftThick">--</td>';
                            }
                           
                            if (rec.newCashflowBalance != 0 && rec.newCashflowBalance != '') {
                                row += '<td class="borderLeftThin borderRightThick">' + rec.prepayNew + ' </td>';
                            }
                            else {
                                row += '<td class="borderLeftThin borderRightThick">--</td>';
                            }
                           
                        row += '</tr>';
                        $('#detailedBody').append(row);
                        outputPrevRecord = true;
                        previousType = rec.rowType;
                    }
                    else {
                        outputPrevRecord = false;
                    }

 
                }

               

            }


            //$('#reportHolder').height($('#reportHolder').height());
           // var h = globalContext.getMultiPageHeight($('.report-view').height(), $('#footnoteContainer').height());
           // $('#reportHolder').height($('#reportHolder').height()).css({
             //   'padding-bottom': '0px'
            //});
            //$('#reportHolder').height(h).css({
              //  'padding-bottom': '0px'
            //});

            $('#footnoteContainer').height($('#footnoteContainer').height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                
                //hiqPdfConverter.startConversion();
                setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);
            }
        }

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            return globalContext.getDetailedSimulationAssumptionViewDataById(id, self.dsa).then(function () {
                var dsa = self.dsa();

                if (!dsa) return;

                globalContext.reportErrors(dsa.errors, dsa.warnings);
            });
        };
    };

    return vm;

});
