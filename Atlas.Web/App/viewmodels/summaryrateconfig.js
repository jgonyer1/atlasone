﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var summaryRateVm = function (summaryRate) {
        var self = this;

        this.summaryRate = summaryRate;

        function onChangesCanceled() {
            self.sortScenarioTypes();
        }

        this.cancel = function () {
        };

        //Scenario Block For Sorting, Adding, and Deleting From Scenario List
        this.sortedScenarioTypes = ko.observableArray(self.summaryRate().summaryRateScenarios());

        this.sortScenarioTypes = function () {
            self.sortedScenarioTypes(self.summaryRate().summaryRateScenarios().sort(function (a, b) { return a.priority() - b.priority(); }));
        };


        this.summaryRate().summaryRateScenarios().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var newSummaryRateScenario = globalContext.createSummaryRateScenario();
            newSummaryRateScenario.scenarioTypeId(scenarioType.id);
            newSummaryRateScenario.priority(summaryRate().summaryRateScenarios().length);
            summaryRate().summaryRateScenarios().push(newSummaryRateScenario);
            self.sortScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };


        this.removeScenarioType = function (summaryRateScenario) {

            var msg = 'Delete scenario "' + summaryRateScenario.scenarioType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    summaryRateScenario.entityAspect.setDeleted();
                    summaryRate().summaryRateScenarios().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortScenarioTypes();

                }
            }

        };

        self.sortScenarioTypes();

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.institutions = ko.observableArray();


        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.summaryRate().institutionDatabaseName(), self.summaryRate().asOfDateOffset(), self.summaryRate().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.summaryRate().institutionDatabaseName(), self.summaryRate().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }

        this.activate = function (id) {
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return globalContext.getAvailableSimulations(self.simulationTypes, self.summaryRate().institutionDatabaseName(), self.summaryRate().asOfDateOffset()).then(function () {
                return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.summaryRate().institutionDatabaseName(), self.summaryRate().asOfDateOffset(), self.summaryRate().simulationTypeId(), false).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {

                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.summaryRate().institutionDatabaseName()).then(function () {

                            self.availableScenarioTypes = ko.computed(function () {
                                var scenarioTypeIdsInUse = self.summaryRate().summaryRateScenarios().map(function (scenario) {
                                    return scenario.scenarioTypeId();
                                });
                                var result = [];
                                self.scenarioTypes().forEach(function (scenarioType) {
                                    if (scenarioType.name != 'Base') {
                                        result.push({
                                            id: scenarioType.id(),
                                            name: scenarioType.name(),
                                            inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                        });
                                    }

                                });
                                return result;
                            });

                            //AsOfDateOffset Handling (Single-Institution Template)
                            self.instSub = self.summaryRate().institutionDatabaseName.subscribe(function (newValue) {
                                globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                                self.refreshSim();
                            });

                            self.aodSub = self.summaryRate().asOfDateOffset.subscribe(function (newValue) {
                                self.refreshSim();
                            });
                            self.simSub = self.summaryRate().simulationTypeId.subscribe(function (newValue) {
                                self.refreshScen();
                            });
                            global.loadingReport(false);

                        });
                    });
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.simSub.dispose();
        };

        return this;
    };

    return summaryRateVm;
});
