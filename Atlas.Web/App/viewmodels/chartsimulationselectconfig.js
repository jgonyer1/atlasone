﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var simulationSelectVm = function (chartObs, showInst, canAddSim, simulationTypes, side) {
        var self = this;
        this.side = side;
        this.subs = [];
        this.showInst = showInst;
        this.canAddSim = canAddSim;
        this.chart = chartObs;
        //this.institutionDatabaseName = institutionDatabaseName;
        //this.overrideSimulationTypeId = overrideSimulationTypeId;
        this.policyOffsets = ko.observable();
        //globalContext.getPolicyOffsets(self.policyOffsets, self.chart().institutionDatabaseName());

        this.institutions = ko.observableArray();

        this.asOfDateOffsets = ko.observableArray();
        //globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.chart().institutionDatabaseName());

        this.simulationTypes = simulationTypes;
        this.sortedSimulationTypes = ko.observableArray();

        this.addChartSimulationType = function () {
            if (self.policyOffsets() == undefined) {
                return;
            }
            var chartSimType = globalContext.createChartSimulationType();
            var aod = self.asOfDateOffsets()[0].asOfDateDescript;
            var simTypeId;
            
            if (self.policyOffsets() && self.policyOffsets()[0][aod] != undefined) {
                simTypeId = self.policyOffsets()[0][aod][0].niiId;
            } else {
                simTypeId = 1;//self.simulationTypes()[0].id;
            }
           
            chartSimType.priority(self.chart().chartSimulationTypes().length);
            chartSimType.asOfDateOffset(0);
            chartSimType.simulationTypes = ko.observableArray();
            chartSimType.simulationTypeId(simTypeId);
            globalContext.getAvailableSimulations(chartSimType.simulationTypes, self.chart().institutionDatabaseName(), chartSimType.asOfDateOffset()).then(function () {

            });
            self.chart().chartSimulationTypes().push(chartSimType);
            if (self.side == "left") {
                console.log('Tirggering LEft');
                app.trigger('application:addedLeftChartSimulation');
            }
            else {
                console.log('Triggering Right');
                app.trigger('application:addedRightChartSimulation');
            }
            

            self.sortChartSimulationTypes();
        };

        this.removeChartSimType = function (cSimType) {
            console.log(cSimType);
            var msg = "Delete Simulation Type?";
            var title = "Delete Simulation Type";
            var opts = ["Cancel", "Ok"];
            app.showMessage(msg, title, opts).then(function (r) {
                if (r == opts[1]) {
                    self._deleteChartSimType(cSimType);

                    if (self.side == "left") {
                        app.trigger('application:deleteLeftChartSimulation');
                    }
                    else {
                        app.trigger('application:deleteRightChartSimulation');
                    }

                }
            });
        };
        this._deleteChartSimType = function (cSimType) {
            //self.chart().chartSimulationTypes.remove(cSimType);
            cSimType.entityAspect.setDeleted();
            self.sortChartSimulationTypes();
        };

        this.enableSimulationDropdown = function (cSimType) {
            if (cSimType.priority() > 0) {
                return true;
            } else {
                return self.chart().overrideParentSimulationType();
            }
        };
        this.ovrdParentSim = function () {
            self.chart().chartSimulationTypes().forEach(function (item, indx, arr) {
                self.enableSimulationDropdown(item);
            });
            self.defaultParentSimulationType();
        };

        this.defaultParentSimulationType = function () {
            var simTypes = self.getSortedSimulationTypes();
            var aod;
            if (simTypes[0] != undefined && simTypes[0] != null) {
                if (!self.chart().overrideParentSimulationType()) {

                    aod = self.asOfDateOffsets()[simTypes[0].asOfDateOffset()].asOfDateDescript;
                    if (self.policyOffsets()[0][aod] != undefined) {
                        simTypes[0].simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                    }
                }
            }
        };

        this.fixSimulationTypePriority = function () {
            self.sortedSimulationTypes().forEach(function (item, idx, arr) {
                item.priority(idx);
            });
        };

        this.getSortedSimulationTypes = function () {
            var arr = self.chart().chartSimulationTypes().sort(function (a, b) {
                return a.priority() - b.priority();
            });
            return arr;
        };

        this.sortChartSimulationTypes = function () {
            self.sortedSimulationTypes(self.getSortedSimulationTypes());
        };

        this.activate = function () {

                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.chart().institutionDatabaseName()).then(function () {
                        return globalContext.getPolicyOffsets(self.policyOffsets, self.chart().institutionDatabaseName()).then(function () {
                            //if (self.chart().chartSimulationTypes().length == 0) {
                            //    self.addChartSimulationType();
                            //}
                            self.sortChartSimulationTypes();
                            //self.defaultParentSimulationType();
                            if (self.side == "left") {
                                app.trigger('application:readyToAddLeftDefaultSim');
                            } else {
                                app.trigger('application:readyToAddRightDefaultSim');
                            }
                            
                        });
                    });
                });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            //self.instSub.dispose();
        };
        this.deactivate = function () {
            for (var s = 0; s < self.subs.length; s++) {
                self.subs[s].dispose();
            }

        };

        return this;
    };

    return simulationSelectVm;
})