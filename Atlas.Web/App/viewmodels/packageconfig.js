﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model',
        'services/global',
        'viewmodels/navigateawaymodal'
],
function (globalContext, router, system, app, logger, model, global, naModal) {
    var thePackage = ko.observable();
    var packageTemplate = ko.observable();
    var sortedReports = ko.observableArray();
    var filter = ko.observable('');
    var isDeleting = ko.observable(false);
    var isSaving = ko.observable(false);
    var curPage = "PackageConfig";

    function reportCompare(a, b) {
        if (a.priority() > b.priority()) return 1; if (a.priority() < b.priority()) return -1; return 0;
    }

    function activate(id) {
        globalContext.logEvent("Viewed", curPage);
        $('#package').text('');
        $('#packageTemplate').text('');
        return globalContext.getPackageById(parseInt(id), thePackage).then(function () {
            sortReports();
            $('#package').text('Current Package: ' + thePackage().name());
            if (thePackage().sourcePackageId()) {
                globalContext.getAtlasTemplateById(thePackage().sourcePackageId(), packageTemplate).then(function () {
                    if (packageTemplate())
                        $('#packageTemplate').text('Current Package Template: ' + packageTemplate().name);
                });
            }
        });
    }

    function compositionComplete() {
        
    }


    function sortReports() {
        globalContext.logEvent("Sorted reports", curPage);
        sortedReports(thePackage().reports().sort(reportCompare));
    };

    var goToView = function (report) {
        globalContext.logEvent("Going to report view", curPage);
        if (report.shortName() == 'NIIRecon') {
            var url = "report/Excel/" + thisReport().id();
            window.open(url, '_blank');
        }
        else {
            router.navigate("#/reportview/" + report.id());
        }


    };

    var deleteReportingItem = function (rep) {
        var msg = 'Delete report "' + rep.name() + '" ?';
        var title = 'Confirm Delete';
        var packageId = rep.packageId();
        isDeleting(true);
        return app.showMessage(msg, title, ['No', 'Yes'])
            .then(confirmDelete);

        function confirmDelete(selectedOption) {
            if (selectedOption === 'Yes') {
                globalContext.logEvent("Deleted " + rep.entityAspect.entity.shortName() + " report", curPage);
                globalContext.deleteReport(rep.id()).fin(function () {
                    rep.entityAspect.setDetached(); //Set it to detached becuase of breeze cache since we deleted it on server

                    thePackage().reports.remove(rep);
                    var numberOfReports = thePackage().reports().length;
                    for (var i = 0; i < numberOfReports; i++) {
                        var aReport = thePackage().reports()[i];
                        aReport.priority(i);
                    }

                    fixupReportPriority();
                    sortReports();
                    globalContext.saveChanges();

                    function success() {

                    }

                    function failed(error) {
                        cancel();
                        var errorMsg = 'Error: ' + error.message;
                        logger.logError(
                            errorMsg, error, system.getModuleId(vm), true);
                    }

                    function finish() {
                        return selectedOption;
                    }

                });
            }
            isDeleting(false);
        }
    };



    var goToFirstReportView = function () {
        globalContext.logEvent("View first report", curPage);
        var firstReport = thePackage().reports()[0];

        router.navigate("#/reportview/" + firstReport.id());
    };
    var goToPdf = function () {
        if (!hasChanges()) {
            continuation();
            return;
        }

        return naModal.show(
            function () { cancel(); continuation(); },
            function () { save().then(continuation); }
        );

        function continuation() {
            globalContext.logEvent("Start pdf export", curPage);
            globalContext.startPdfProcess(thePackage().id());
            global.pdfInProgress(true);
            global.pdfType('package');
            global.pdfPackage(thePackage().id());
            app.trigger('application:pdfStart');
        }
    };

    var goBack = function () {
        globalContext.logEvent("Go Back Nav", curPage);
        router.navigate("#/packages/");
    };

    var hasChanges = ko.computed(function () {
        return globalContext.hasChanges();
    });

    var cancel = function () {
        globalContext.cancelChanges();
        sortReports();
    };

    var canSave = ko.computed(function () {
        return hasChanges() && !isSaving();
    });

    var save = function () {
        return globalContext.saveChanges().fin(complete);

        function complete() {
            isSaving(false);
        }
    };

    var canDeactivate = function () {
        if (!hasChanges()) return true;

        return naModal.show(
            function () { globalContext.cancelChanges(); },
            function () { save(); }
        );
    };

    var add = function (reportType) {
        router.navigate("#/reportadd?packageid=" + thePackage().id() + "&packagelength=" + thePackage().reports().length + "&reporttype=" + reportType.name);
    };

    var config = function (report) {
        router.navigate("#/reportconfig/" + report.id());
    };

    var deletePackage = function () {
        var msg = 'Delete package "' + thePackage().name() + '" ?';
        var title = 'Confirm Delete';
        isDeleting(true);
        return app.showMessage(msg, title, ['No', 'Yes'])
            .then(confirmDelete);

        function confirmDelete(selectedOption) {
            if (selectedOption === 'Yes') {
                globalContext.deletePackageById(thePackage().id()).fin(success).fail(failed).fin(finish);

                function success() {
                    router.navigate('#/packages/');
                }

                function failed(error) {
                    cancel();
                    var errorMsg = 'Error: ' + error.message;
                    logger.logError(
                        errorMsg, error, system.getModuleId(vm), true);
                }

                function finish() {
                    return selectedOption;
                }
            }

            isDeleting(false);
        }
    };

    var newDeletePackage = function () {
        var msg = 'Delete package "' + thePackage().name() + '" ?';
        var title = 'Confirm Delete';
        isDeleting(true);
        return app.showMessage(msg, title, ['No', 'Yes'])
            .then(confirmDelete);

        function confirmDelete(selectedOption) {
            if (selectedOption === 'Yes') {
                globalContext.logEvent("Deleted package " + thePackage().name(), curPage);
                $("#deleting_dialog").modal("show");
                globalContext.deleteReportingItemById("Package", thePackage().id(), "").fin(success).fail(failed).fin(finish);
                //globalContext.deletePackageById(thePackage().id()).fin(success).fail(failed).fin(finish);

                function success() {
                    router.navigate('#/packages/');
                }

                function failed(error) {
                    cancel();
                    var errorMsg = 'Error: ' + error.message;
                    logger.logError(
                        errorMsg, error, system.getModuleId(vm), true);
                }

                function finish() {
                    $("#deleting_dialog").modal("hide");
                    return selectedOption;
                }
            }

            isDeleting(false);
        }
    };


    var fixupReportPriority = function (arg) {
        //alert("hello");
        //arg.sourceParent().forEach(function (report, index) {
        //    report.priority(index);
        //    alert(index);
        //});
        sortedReports().forEach(function (report, index) {
            report.priority(index);
        });
    };


    function dontMove(arg, event, ui) {
        if (arg.sourceParent()[arg.sourceIndex].shortName() == 'CoverPage' || arg.sourceParent()[arg.targetIndex].shortName() == 'CoverPage') {
            arg.cancelDrop = true;
        }
        //alert(arg.sourceParent()[0].entityType.shortName);
        return false;
    };

    function reportNames() {
        var filtered = filter().toLowerCase().trim();

        if(! filtered)
            return model._reportSections;

        var results = [];

        for (var i = 0; i < model._reportSections.length; i++) {
            var sourceSection = model._reportSections[i];
            var targetSection = null;

            for (var j = 0; j < sourceSection.subSections.length; j++) {
                var sourceSubSection = sourceSection.subSections[j];
                var targetSubSection = null;

                for (var k = 0; k < sourceSubSection.reports.length; k++) {
                    var report = sourceSubSection.reports[k];

                    if (report.dispName.toLowerCase().indexOf(filtered) == -1)
                        continue;

                    if (targetSubSection !== null) {
                        targetSubSection.reports.push(report);
                        continue;
                    }

                    targetSubSection = {
                        name: sourceSubSection.name,
                        reports: [report]
                    };

                    if (targetSection === null) {
                        targetSection = {
                            name: sourceSection.name,
                            subSections: [targetSubSection]
                        }

                        results.push(targetSection);

                        continue;
                    }

                    targetSection.subSections.push(targetSubSection);
                }
            }
        }

        return results;
    }

    function clearFilter() {
        filter('');
    }

    function cancelEvent(data, e) {
        e.cancelBubble = true;
        if (e.stopPropagation) { e.stopPropagation();}
    }

    var vm = {
        activate: activate,
        compositionComplete: compositionComplete,
        title: 'Package Config',
        filter: filter,
        thePackage: thePackage,
        goToView: goToView,
        goToFirstReportView: goToFirstReportView,
        goToPdf: goToPdf,
        goBack: goBack,
        save: save,
        canDeactivate: canDeactivate,
        canSave: canSave,
        hasChanges: hasChanges,
        cancel: cancel,
        reportNames: ko.computed(reportNames),
        add: add,
        config: config,
        deletePackage: deletePackage,
        newDeletePackage: newDeletePackage,
        sortedReports: sortedReports,
        fixupReportPriority: fixupReportPriority,
        dontMove: dontMove,
        deleteReportingItem: deleteReportingItem,
        clearFilter: clearFilter,
        cancelEvent: cancelEvent        
    };

    return vm;

    //#region Internal Methods


    //#endregion

});
