﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model'
],


        function (globalContext, router, system, app, logger, model) {

        var thePackage = ko.observable();
        var reportNames = ko.observableArray(model.reportNames);
        var asOfDates = ko.observable();
        var packageName = ko.observable();
        var asOfDate = ko.observable();
        var profile = ko.observable();
        var newPackageId = ko.observable();

        function activate(id) {
            //Get As Of Date To Copy Package
            return globalContext.getUserProfile(profile).then(function () {
                return globalContext.getAsOfDates(asOfDates).then(function () {
                    asOfDate(asOfDates()[0].asOfDateDescript);
                    return globalContext.getPackageById(parseInt(id), thePackage).then(function () {
                    });
                });
            });

        }

        var cancel = function () {
            globalContext.cancelChanges();
            router.navigate('#/packages/');
        };

        var copy = function () {
            globalContext.copyPackage(thePackage().id(), asOfDate(), newPackageId).fin(function () {
                goToEditView(newPackageId());
            }


           );

            function goToEditView(id) {
                router.navigate('#/packageconfig/' + id);
            }

        };

        var newCopy = function () {
            globalContext.newCopyPackage(thePackage().id(), asOfDate(), newPackageId, profile().databaseName, profile().databaseName).fin(function () {
                goToEditView(newPackageId());
            });

            function goToEditView(id) {
                router.navigate('#/packageconfig/' + id);
            }
        };
        
        var add = function (reportType) {
            router.navigate("#/" + reportType.toLowerCase() + "add/" + thePackage().id());
        };
        
        var config = function (report) {
            router.navigate("#/" + report.entityType.shortName.toLowerCase() + "config/" + report.id());
        };
        

        var vm = {
            activate: activate,
            thePackage: thePackage,
            copy: copy,
            newCopy: newCopy,
            cancel: cancel,
            reportNames: reportNames,
            add: add,
            config: config,
            asOfDates: asOfDates,
            profile: profile,
            asOfDate: asOfDate


        };

        return vm;

        //#region Internal Methods

        
        //#endregion

    });
