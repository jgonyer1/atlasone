﻿const maxWidthOrHeight = 1500;

define(
    [],
    function () {
        function wipeRelatedFiles(fileID) {
            var result = new Promise(function (resolve, reject) {
                if (fileID <= 0) {
                    resolve();
                    return;
                }

                $.ajax({
                    async: true,
                    url: '/File/DeleteByOwner?ownerType=Report&ownerID=' + fileID,
                    type: 'post',
                    error: function (err) { reject(); },
                    success: function () { resolve(); }
                });
            });

            return result;
        }

        function savePDF(base64, name, contentType, ownerType, ownerID) {
            var chompIdx = base64.indexOf('base64,') + 7;
            base64 = base64.substring(chompIdx);

            return new Promise(function (resolve, reject) {
                $.ajax({
                    async: true,
                    url: '/File/Upload',
                    type: 'post',
                    data: "base64=" + encodeURIComponent(base64) +
                          "&ownerType=" + ownerType +
                          "&ownerID=" + ownerID +
                          "&name=" + encodeURIComponent(name || "") +
                          "&contentType=" + encodeURIComponent(contentType),
                    error: function (err) { reject(); },
                    success: function (fileSummaryInfo) { resolve(fileSummaryInfo); }
                });
            });
        }

        function saveWord(base64, name, ownerType, ownerID, orientation) {
            var chompIdx = base64.indexOf('base64,') + 7;
            base64 = base64.substring(chompIdx);

            return new Promise(function (resolve, reject) {
                $.ajax({
                    async: true,
                    url: '/File/UploadWord',
                    type: 'post',
                    data: "base64=" + encodeURIComponent(base64) +
                        "&ownerType=" + ownerType +
                        "&ownerID=" + ownerID +
                        "&name=" + encodeURIComponent(name || "") +
                        "&orientation=" + encodeURIComponent(orientation),
                    error: function (err) { reject(); },
                    success: function (fileSummaryInfo) { resolve(fileSummaryInfo); }
                });
            });
        }

        function getTemplates(fileSummaries) {
            var result = [];
            var completedCount = 0;

            return new Promise(function (resolve, reject) {
                for (var i = 0; i < fileSummaries.length; i++) {
                    var fileSummary = fileSummaries[i];

                    $.ajax({
                        async: true,
                        url: `/File/GetFileViewTemplateByID?fileID=${fileSummary.FileID}`,
                        type: 'get',
                        error: function (err) { reject(); },
                        success: (function (lidx) {
                            return function (template) {
                                result[lidx] = template;
                                completedCount++;

                                if (completedCount >= fileSummaries.length) {
                                    resolve(result.join("\r\n") + "</div>");
                                }
                            };
                        })(i)
                    });
                }
            });
        }

        function getTemplateByReport(report) {
            return new Promise(function (resolve, reject) {
                var cacheBuster= new Date().getTime();

                $.ajax({
                    async: true,
                    url: `/File/GetFileViewTemplateByReportID?reportID=${report.id()}&orientation=${report.orientation()}&cb=${cacheBuster}`,
                    type: 'get',
                    error: function (err) { reject(); },
                    success: function (template) {
                        resolve(`<div>${template}</div>`);
                    }
                });
            });
        }

        return function (document, vm, rootModel) {
            var self = this;

            this.document = document;
            this.defaults = vm.thisReport().defaults;
            this.bankName = vm.bankName;
            this.report = vm.thisReport();
            this.rootModel = rootModel;
            var curPage = "DocumentConfig";
            //globalContext.logEvent("Viewed", curPage);  
            this.report.onResetChanges = function () {
                return new Promise(function (resolve, reject) {
                    getTemplateByReport(self.report).then(
                        html => {
                            self.document().htmlSource(html);
                            resolve();
                        }
                    ).catch(err => { reject(err); });
                });
            };

            this.showHeader = ko.pureComputed({
                read: () => !this.report.hideHeader(),
                write: (value) => { this.report.hideHeader(!value); },
                owner: this
            });

            this.docHtmlSourceMinusIframe = ko.computed(() => {
                if (!self.report.contentsFromFile()) return '';

                var source = $(self.document().htmlSource());

                if (source.length > 1) {
                    // wrap legacy document sources
                    source = $(`<div>${self.document().htmlSource()}</div>`);
                }

                var result = $('div[data-orientation=both]', source);

                if (result.length > 0) return result.html();

                result = $(`div[data-orientation=${self.report.orientation()}]`, source);

                if (result.length > 0) return result.html();

                return source.html() || '';
            });

            this.restoreTextEdit = function () {
                wipeRelatedFiles(self.report.id())
                    .then(() => {
                        if (!self.report.orientation()) {
                            self.report.orientation('Landscape');
                        }

                        self.document().htmlSource('');
                        self.report.contentsFromFile(false);

                        $('input[type=file]').val('');

                        return vm.save('config')
                    });
            };

            if (this.report.id() <= 0) {
                // default to landscape
                this.report.orientation('Landscape');
            }

            this.handleError = function (msg) {
                msg = msg || 'Could not upload selected document.';
                toastr.error(msg, "", 2500);
                self.rootModel.isLoading(false);
            };

            this.uploadFile = function (file) {
                if (!file) return;

                self.rootModel.isLoading(true);

                var reader = new FileReader();

                switch (file.type) {
                    case 'text/html':
                        reader.onload = function (loadEvent) {
                            // jQuery automatically strips out body, but not necessarily everything in the head
                            var documentHTML = $(loadEvent.target.result).not('style').not('title').not('link').not('script');

                            var bodyHTML = $('<div></div>').append(documentHTML);

                            document().htmlSource(bodyHTML[0].outerHTML);

                            wipeRelatedFiles(self.report.id())
                                .then(() => {
                                    if (! self.report.orientation()) {
                                        self.report.orientation('Landscape');
                                    }

                                    self.report.contentsFromFile(false);

                                    return vm.save('config')
                                })
                                .then(() => { self.rootModel.isLoading(false); })
                                .catch(self.handleError);
                        };

                        reader.readAsText(file);

                        break;

                    default:
                        if (file.type != 'application/pdf' && (file.type.indexOf('application/') != 0 || file.type.indexOf('document') == -1)) {
                            self.handleError(`Unsupported file type ${file.type}`);
                            return;
                        }

                        var mustObtainID = self.report.id() <= 0;

                        var isPDF = file.type == 'application/pdf';

                        reader.onload = function (loadEvent) {
                            var base64 = loadEvent.target.result;

                            wipeRelatedFiles(self.report.id())
                                .then(() => {
                                    self.report.contentsFromFile(true);

                                    if (isPDF) {
                                        self.report.orientation(null);
                                    }
                                    else if (!self.report.orientation()) {
                                        self.report.orientation('Landscape');
                                    }

                                    function continuation() {
                                        return isPDF
                                            ? savePDF(base64, file.name, file.type, 'report', self.report.id())
                                            : saveWord(base64, file.name, 'report', self.report.id(), self.report.orientation())
                                    }

                                    if (mustObtainID) {
                                        return vm.save('none').then(continuation);
                                    }

                                    return continuation();
                                })
                                .then(fsi => {
                                    if (isPDF) {
                                        getTemplates([fsi]).then(html => {
                                            document().htmlSource(html);
                                            continuation();
                                        });
                                    }
                                    else {
                                        getTemplates(fsi).then(html => {
                                            document().htmlSource(html);
                                            continuation();
                                        });
                                    }

                                    function continuation() {
                                        if (mustObtainID) {
                                            vm.finalSave('config');
                                        }
                                        else {
                                            vm.save('none');
                                        }
                                    }
                                })
                                .then(() => { self.rootModel.isLoading(false); })
                                .catch(self.handleError);
                        };

                        reader.readAsDataURL(file);
                }
            };
        };
    }
);
