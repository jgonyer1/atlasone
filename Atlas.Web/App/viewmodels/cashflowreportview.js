﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var cashflowVm = function (id) {
        var self = this;
        var curPage = "CashFlowReportView";
        this.cashflowViewData = ko.observable();

        this.mRound = function (val, mroundVal) {
            return Math.round(val / mroundVal, 0) * mroundVal;
        };

        this.standardChartScaling = function (maxValue) {
            maxValue = maxValue / 1000;
            var minRange = 0;
            var maxRange = 0;
            var roundVal = 0;
            var otherVal = 0;
            var majorUnit = 0;

            if (maxValue <= 1000) {
                roundVal = 250;
                otherVal = 200;
            }
            else if (maxValue <= 100000) {
                roundVal = 2500;
                otherVal = 2000;
            }
            else if (maxValue <= 1000000) {
                roundVal = 25000;
                otherVal = 5000;
            }
            else{
                roundVal = 250000;
                otherVal = 50000;
            }

            var maxRound = self.mRound(maxValue, roundVal);

            if (maxRound < maxValue + otherVal) {
                maxRange = maxRound + roundVal;
            }
            else {
                maxRange = maxRound;
            }

            majorUnit = maxRange / 5;
            
            return {
                scaleMax: maxRange * 1000,
                scaleMin: minRange * 1000,
                majorUnit: majorUnit * 1000
            };
        };

        this.hasErrors = ko.observable(false);
        this.compositionComplete = function () {
          
            var simpleHash = {};

            //These are for populating the arrays of buckets set of for the line times for both charting and table
            var tableBucketLookUp = {
                'Q1Y1': 0,
                'Q2Y1': 1,
                'Q3Y1': 2,
                'Q4Y1': 3,
                'Q1Y2': 4,
                'Q2Y2': 5,
                'Q3Y2': 6,
                'Q4Y2': 7,
                'Y3': 8,
                'Y4': 9,
                'Y5': 10,
                '>Y5': 11,
            };

            var chartBucketLookUp = {
                'Q1Y1': 0,
                'Q2Y1': 0,
                'Q3Y1': 0,
                'Q4Y1': 0,
                'Q1Y2': 1,
                'Q2Y2': 1,
                'Q3Y2': 1,
                'Q4Y2': 1,
                'Y3': 2,
                'Y4': 3,
                'Y5': 4,
                '>Y5': 5,
            };

            var compScenName = self.cashflowViewData().scens[self.cashflowViewData().compareScen];
            var tableCompHash = "";

            //This will keep track of unique accoutns and what colors are assigned to them
            var uniqueAccounts = {};
            var uniqueCounter = 0;

            //this is for whenc alculatuing height of charts becuase they var based off of number of rows in table 30px per row
            var totalUniqueConter = 0;
            //Loop through each table and report it out
            for (var t = 0; t < self.cashflowViewData().tableData.length; t++) {
                var tableId = '#table' + t;
                var chartId = '#chart' + t;
                var hashLookUp = 'table' + t;

                //if this is the comparitiave flag it
                var compScen = false;
                if (t == self.cashflowViewData().compareScen) {
                    compScen = true;
                    tableCompHash = hashLookUp;
                }

                simpleHash[hashLookUp] = {

                    tableId: tableId,
                    chartId: chartId,
                    accounts: {},
                    compScen: compScen,
                    scenName: self.cashflowViewData().scens[t], 
                    numberOfAccounts: 0
                }



                //Now loop through and build hash table of data so that we can easily calculate total, and cumulative, etc
                for (var r in self.cashflowViewData().tableData[t]) {
                    var rec = self.cashflowViewData().tableData[t][r];
                    //Load rec everything should be fine as is just need to compare 

                    //Check if this is a new unique account and assign it a color! oh boy!
                    if (uniqueAccounts[rec.name] == undefined) {
                        uniqueAccounts[rec.name] = charting.chartColorsPalette2[uniqueCounter];

                        //increment unique account finder
                        uniqueCounter += 1;
                        totalUniqueConter += 1;
                        //reset it if we have gone too damn high
                        if (uniqueCounter > charting.chartColorsPalette2.length - 1) {
                            uniqueCounter = 0;
                        }
                    }

                    //first time seeing account create it and add to buckets
                    if (simpleHash[hashLookUp].accounts[rec.name] == undefined) {
                        simpleHash[hashLookUp].numberOfAccounts += 1;
                        simpleHash[hashLookUp].accounts[rec.name] = {
                            name: rec.name,
                            tableBuckets: self.tableBuckets(),
                            chartBuckets: self.chartBuckets(),
                            isTotal: false
                        };
                    }

                    //Now that account is created populate table values and chart values

                    //get idx for chart and table
                    var chartIdx = chartBucketLookUp[rec.qtrs];
                    var tableIdx = tableBucketLookUp[rec.qtrs];

                    //Set the values
                    simpleHash[hashLookUp].accounts[rec.name].tableBuckets[tableIdx] += numeral(rec.cashFlow).value();
                    simpleHash[hashLookUp].accounts[rec.name].chartBuckets[chartIdx] += numeral(rec.cashFlow).value();

                }


                //Now that we processed all accounts need to begin to add totaling rows
                simpleHash[hashLookUp].accounts['Total'] = {
                    name: 'Total',
                    tableBuckets: self.tableBuckets(),
                    chartBuckets: self.chartBuckets(),
                    isTotal: true
                };
                //Increment this important for charting cell row span
                simpleHash[hashLookUp].numberOfAccounts += 1;

                simpleHash[hashLookUp].accounts['Cumulative'] = {
                    name: 'Cumulative',
                    tableBuckets: self.tableBuckets(),
                    chartBuckets: self.chartBuckets(),
                    isTotal: true
                };

                //Increment this important for charting cell row span
                simpleHash[hashLookUp].numberOfAccounts += 1;

                //If not comp scen a
                if (!simpleHash[hashLookUp].compScen) {
                    simpleHash[hashLookUp].accounts['Difference'] = {
                        name: 'CML &#916; from  ' + compScenName,
                        tableBuckets: self.tableBuckets(),
                        chartBuckets: self.chartBuckets(),
                        isTotal: true
                    };
                    //Increment this important for charting cell row span
                    simpleHash[hashLookUp].numberOfAccounts += 1;

                }

                //Now that I added totaling stuff lets loop through each account that is not total and do the math!

                for (var a in simpleHash[hashLookUp].accounts) {
                    //If its total continue loop do not total the total!
                    if (simpleHash[hashLookUp].accounts[a].isTotal) {
                        continue;
                    }

                    //Perform basic totaling
                    for (var z = 0; z < simpleHash[hashLookUp].accounts[a].tableBuckets.length; z++){
                        simpleHash[hashLookUp].accounts['Total'].tableBuckets[z] += simpleHash[hashLookUp].accounts[a].tableBuckets[z];
                    }

                    //Perform basic totaling
                    for (var z = 0; z < simpleHash[hashLookUp].accounts[a].chartBuckets.length; z++) {
                        simpleHash[hashLookUp].accounts['Total'].chartBuckets[z] += simpleHash[hashLookUp].accounts[a].chartBuckets[z];
                    }
                }

                //Now loop through totals and figure out cumulative much easier now that i have totals
                for (var z = 0; z < simpleHash[hashLookUp].accounts['Total'].tableBuckets.length; z++){

                    //I think this is right for  cumulatibe looping!
                    for (var y = 0; y <= z; y++) {
                        simpleHash[hashLookUp].accounts['Cumulative'].tableBuckets[z] += simpleHash[hashLookUp].accounts['Total'].tableBuckets[y];
                    }
                   
                }
            }

            //Now that all my tables are build I still need to do some shit

            //Loop through everything and find out which one is the compartive do the comparative math 
            for (var t in simpleHash) {
                //if not compartivate do the math
                if (!simpleHash[t].compScen) {
                    for (var z = 0; z < simpleHash[t].accounts['Difference'].tableBuckets.length; z++) {
                        simpleHash[t].accounts['Difference'].tableBuckets[z] = simpleHash[t].accounts['Cumulative'].tableBuckets[z] - simpleHash[tableCompHash].accounts['Cumulative'].tableBuckets[z];
                    }
                }
            }


            var maxVal = 0;

            for (var s in simpleHash) {
                //if not total populate chart
                for (var a in simpleHash[s].accounts) {
                    var acc = simpleHash[s].accounts[a];

                    if (simpleHash[s].accounts[a].isTotal && a == 'Total') {

                        for (var z = 0; z < acc.chartBuckets.length; z++) {
                            if (maxVal < numeral(acc.chartBuckets[z]).value()) {
                                maxVal = numeral(acc.chartBuckets[z]).value();
                            }
                        }
                    }
                }
            }

            var scaleOptions = self.standardChartScaling(maxVal);

            //Now that I have all these hash tables build in right order loop through and dump them out!
            for (var s in simpleHash) {
                var cats = ["Y1", "Y2", "Y3", "Y4", "Y5", ">Y5"];
                var series = [];

                //Build table headers
                $('#oneBigTable').append(self.buildTableHeaders(simpleHash[s].tableId, simpleHash[s].numberOfAccounts, simpleHash[s].scenName));
                var firstTot = true;
                //Loop through all accounts and output actual data!
                for (var a in simpleHash[s].accounts) {
                    var acc = simpleHash[s].accounts[a];

                    var totalClass = '';

                    //set total cloass row
                    if (simpleHash[s].accounts[a].isTotal) {
                        totalClass = 'totRow';
                    }
                    
                    //check to see if we need to do border on top
                    if (firstTot && simpleHash[s].accounts[a].isTotal) {
                        firstTot = false;
                        totalClass += " borderTopRow";
                    }

                    //if not total push it on to series and chart it
                    if (!simpleHash[s].accounts[a].isTotal) {
                        series.push({
                            name: simpleHash[s].accounts[a].name,
                            data: [],
                            color: uniqueAccounts[a]
                        });
                    }

                    var row = '<tr class="' + totalClass + '">';
                    
                    //Set account name and add color block
                    row += '<td class="assetLabel">' + acc.name + '</td>';
                    row += '<td><span class="colorBlock" style="background-color: ' + uniqueAccounts[a] + '">&nbsp;</span></td> ';

                    for (var z = 0; z < acc.tableBuckets.length; z++) {
                        var borderRightClass = '';
                        if (z == 3 || z == 7) {
                            borderRightClass = 'borderRight';
                        }
                        row += '<td class="' + borderRightClass + '">' + numeral(acc.tableBuckets[z] / 1000).format('0,0') + '</td>';
                    }

                    row += '</tr>';

                    //if not total populate chart
                    if (!simpleHash[s].accounts[a].isTotal) {
                        for (var z = 0; z < acc.chartBuckets.length; z++) {
                            series[series.length - 1].data.push(numeral(acc.chartBuckets[z]).value());
                        }
                    }

                    $(simpleHash[s].tableId).append(row);
                }

                //Now that we generated table lets generate the chart
                self.generateChart(cats, series, simpleHash[s].tableId + 'Chart', (27 * (totalUniqueConter + 3)), scaleOptions);

                //At end of each table add a spacer row just cause we want vertical spacing!
                $(simpleHash[s].tableId).append('<tr><td class="spacerRow" colspan="16">&nbsp;</td></td>');
            }

            $("#cashflow-view").height($("#cashflow-view").height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }


        }

        this.generateChart = function (cats, series, chartId, height, scaleOptions) {


            var tickPositions = [];

            var temp = scaleOptions.scaleMin;
            while (temp <= scaleOptions.scaleMax) {
                tickPositions.push(temp);
                temp += scaleOptions.majorUnit;
            }
            console.log(tickPositions);

            Highcharts.chart(chartId.substring(1), {
                chart: {
                    type: 'column',
                    height: height,
                    backgroundColor: 'rgb(252, 254, 235)',
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: cats
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                   // gridLineWidth: 1,
                    //tickLength: 0,
                    tickWidth: 1,
                    tickPosition: 'inside',
                    max: scaleOptions.scaleMax,
                    min: scaleOptions.scaleMin,
                    tickInterval: scaleOptions.majorUnit,
                    tickPositions: tickPositions
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                   // headerFormat: '<b>{point.x}</b><br/>',
                    //pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
                series: series
            });
        }

        this.tableBuckets = function() {
            return [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
        };


        this.chartBuckets = function() {
            return [
                0,
                0,
                0,
                0,
                0,
                0
            ];
        };

        this.buildTableHeaders = function (tbodyId, rowSpan, scenario) {

            var obj = charting.scenarioIdLookUp(scenario);
            var thead = "";
            //substring this to remove #
            thead += "<tbody id='" + tbodyId.substring(1) + "'>";
            thead += "<tr class='headRow'>";

            thead += "<td rowspan='" + (rowSpan + 1) + "' class='empty vertCell' style='background-color: " + obj.seriesColor + "' ><div><span class='fa fa-arrow-" + obj.arrowDirection + "' style='color: " + obj.arrowColor + "'></span>" + scenario + " Rate Scenario</div></td>";
            thead += "<td rowspan='" + (rowSpan + 1) + "' class='spacerCell empty'></td>";
            thead += "<td class='assetLabel'>ASSETS</td>";
            thead += "<td><span class='colorBlock'>&nbsp;</span></td> ";

            thead += "<td>Q1Y1</td>";
            thead += "<td>Q2Y1</td>";
            thead += "<td>Q3Y1</td>";
            thead += "<td class='borderRight'>Q4Y1</td>";
            //Secon year
            thead += "<td>Q1Y2</td>";
            thead += "<td>Q2Y2</td>";
            thead += "<td>Q3Y2</td>";
            thead += "<td class='borderRight'>Q4Y2</td>";
            //YEar buckets
            thead += "<td>Y3</td>";
            thead += "<td>Y4</td>";
            thead += "<td>Y5</td>";
            thead += "<td>>Y5</td>";
            //add one to row span to account for header row that we are creating now
            thead += "<td class='bigChartCell empty' rowspan='" + (rowSpan + 1) + "'><h1 class='atlasHeader grey no-margin'>Cash flow</h1><div id='" + tbodyId.substring(1) + "Chart' class='cashflowChart'></div></td>";
            thead += "</tr>";
            thead += '</tbody>';
            return thead;

        }

        this.buildEmptyRowStructure = function (id) {

            var struct = "<div class='row'>";
            struct += "<div class='col-md-8'>";
            struct += "<div class='vertScenarioButton'><span class='fa fa-arrow-down'></span></div>";
            struct += "<table id='table" + id + "' class='dcg-table'></table>";

            struct += "</div>";

            struct += "<div class='col-md-4'>";
            struct += "<div id='chart" + id + "'></div>";

            struct += "</div>";
            struct += "</div>";
            return struct;
        }

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getCashflowReportViewDataById(id, self.cashflowViewData).then(function () {
                var viewData = self.cashflowViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
                if (viewData.errors.length > 0) {
                    self.hasErrors(true);
                }
            });
        };
    };

    return cashflowVm;

});
