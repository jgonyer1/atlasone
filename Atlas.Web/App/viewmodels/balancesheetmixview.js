﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var balanceSheetMixVm = function (id) {
        var self = this;
        var curPage = "BalanceSheetMixView";;
        this.balanceSheetMix = ko.observable();
        this.balanceSheetMixViewData = ko.observable();
        this.assetCurrentPieHighChartOptions = ko.observable();
        this.assetPriorPieHighChartOptions = ko.observable();
        this.liabilityCurrentPieHighChartOptions = ko.observable();
        this.liabilityPriorPieHighChartOptions = ko.observable();
        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getBalanceSheetMixById(id, self.balanceSheetMix).then(function () {
                return globalContext.getBalanceSheetMixViewDataById(id, self.balanceSheetMixViewData).then(function () {
                    var viewData = self.balanceSheetMixViewData();

                    if (!viewData) return;

                    globalContext.reportErrors(viewData.errors, viewData.warnings);
                });
            });
        };

        this.generateTotRow = function (tblId, label, monthDiff, curBal, priorBal) {
            $('#' + tblId + ' tbody').append('<tr><td class="spacerCell" colspan="6"></td></tr>');
            var row = '<tr class="totalRow">';
            row += '<td class="borderLeft borderTop borderBottom borderRadiusLeft"><div style="width:180px">' + label + '</div></td>';
            row += '<td class="borderTop borderBottom"></td>';
            row += '<td class="borderTop borderBottom"></td>';
            row += '<td class="borderTop borderBottom"></td>';
            row += '<td class="borderTop borderBottom">' + numeral((curBal - priorBal) / 1000).format('0,0') + '</td>';

            if (priorBal != 0) {
                row += '<td  class="borderTop borderBottom borderRight borderRadiusRight">' + numeral((curBal - priorBal) / ((monthDiff / 12)) / priorBal).format('%0.0') + '</td>';
            }
            else {
                row += '<td class="borderTop borderBottom borderRight borderRadiusRight">0.0%</td>';
            }

            $('#' + tblId + ' tbody').append(row);
        }

        this.compositionComplete = function () {
            $('.asOfDateHeader').text(self.balanceSheetMixViewData().asOfDate);
            $('.priorDateHeader').text(self.balanceSheetMixViewData().offSetDate);
            //$('#assetTable tbody').empty();
            //$('#liabilityTable tbody').empty();;
            var tbData = self.balanceSheetMixViewData().tableData;
            var tblToUse = '';
            var monthDiff = Math.round(moment(self.balanceSheetMixViewData().asOfDate).diff(moment(self.balanceSheetMixViewData().offSetDate), 'months', true));

            var totCurBal = 0;
            var totPriorBal = 0;
            var tots = {
                assetCurBal: 0,
                assetPriorBal: 0,
                liabCurBal: 0,
                liabPriorBal: 0,
                parents: {}
            };
            for (var i = 0; i < tbData.length; i++) {
                var rec = tbData[i];

                if (rec.isAsset == 0) {
                    tots.liabCurBal += rec.curBalance;
                    tots.liabPriorBal += rec.priorBalance;
                }
                else {
                    tots.assetCurBal += rec.curBalance;
                    tots.assetPriorBal += rec.priorBalance;
                }

                if (tots.parents[rec.parentName] == undefined) {
                    tots.parents[rec.parentName] = {
                        curBal: 0,
                        priorBal: 0,
                        used: false,
                        isAsset: rec.isAsset
                    };
                }

                tots.parents[rec.parentName].curBal += numeral(rec.curBalance).value();
                tots.parents[rec.parentName].priorBal += numeral(rec.priorBalance).value();
            }

            var assetData = {
                curData: [],
                priorData: []
            }

            var liabData = {
                curData: [],
                priorData: []
            }

            var assetParentCounter = 0;
            var liabParentCounter = 0;
            var bgColor = '';
            var textColor = '';
            for (var i = 0; i < tbData.length; i++) {
                var rec = tbData[i];
                var row = '';
                if (!tots.parents[rec.parentName].used) {
                    row += '<tr class="parent-row">';

                    var totPriorBal = 0;
                    var totCurBal = 0;
                    if (rec.isAsset == 1) {
                        totCurBal = tots.assetCurBal;
                        totPriorBal = tots.assetPriorBal;
                        bgColor = charting.chartColorsPalette2[assetParentCounter];
                        textColor = charting.chartColorsTextPalette2[assetParentCounter];
                        assetParentCounter++;
                    }

                    else {
                        totCurBal = tots.liabCurBal;
                        totPriorBal = tots.liabPriorBal;
                        bgColor = charting.chartColorsPalette2[liabParentCounter];
                        textColor = charting.chartColorsTextPalette2[liabParentCounter];
                        liabParentCounter++;                  
                    }




                    row += '<td class="parentCell"><div class="color-key" style="background-color: ' + bgColor + '; color: ' + textColor + ';">' + rec.parentName + '</div></td>';
                    row += '<td class="text-center">' + numeral(tots.parents[rec.parentName].curBal / totCurBal).format('%0.0') + '</td>';
                    row += '<td class="text-center">' + numeral(tots.parents[rec.parentName].priorBal / totPriorBal).format('%0.0') + '</td>';
                    row += '<td class="text-center">' + numeral((tots.parents[rec.parentName].curBal / totCurBal) - (tots.parents[rec.parentName].priorBal / totPriorBal)).format('%0.0') + '</td>';
                    row += '<td class="text-center">' + numeral((tots.parents[rec.parentName].curBal - tots.parents[rec.parentName].priorBal) / 1000).format('(0,0)') + '</td>';

                    if (tots.parents[rec.parentName].priorBal > 0) {
                        row += '<td class="text-center">' + numeral((tots.parents[rec.parentName].curBal - tots.parents[rec.parentName].priorBal) / ((monthDiff / 12)) / tots.parents[rec.parentName].priorBal).format('%0.0') + '</td>';
                    }
                    else {
                        row += '<td class="text-center">-</td>';
                    }
                    

                    row += '</tr>';
                    //Set to True so we only Show Once
                    tots.parents[rec.parentName].used = true;
                    if (rec.isAsset == 1) {
                        tblToUse = 'assetTable';
                        assetData.curData.push([rec.parentName, (tots.parents[rec.parentName].curBal / totCurBal) * 100]);
                        assetData.priorData.push([rec.parentName, (tots.parents[rec.parentName].priorBal / totPriorBal) * 100]);
                    }
                    else {

                        tblToUse = 'liabilityTable';
                        liabData.curData.push([rec.parentName, (tots.parents[rec.parentName].curBal / totCurBal) * 100]);
                        liabData.priorData.push([rec.parentName, (tots.parents[rec.parentName].priorBal / totPriorBal) * 100]);
                    }


                    $('#' + tblToUse + ' tbody').append(row);
                }

                if (rec.name != rec.parentName) {
                    row = '<tr>';

                    row += '<td class="childCell"><div style="width:165px">' + rec.name + '</div></td>';
                    row += '<td class="text-center">' + numeral(rec.curBalance / totCurBal).format('%0.0') + '</td>';
                    row += '<td class="text-center">' + numeral(rec.priorBalance / totPriorBal).format('%0.0') + '</td>';
                    row += '<td class="text-center">' + numeral((rec.curBalance / totCurBal) - (rec.priorBalance / totPriorBal)).format('%0.0') + '</td>';
                    row += '<td class="text-center">' + numeral((rec.curBalance - rec.priorBalance) / 1000).format('(0,0)') + '</td>';

                    if (rec.curBalance == null) {
                        rec.curBalance = 0;
                    }

                    if (rec.priorBalance == null) {
                        rec.priorBalance = 0;
                    }

                    if (rec.priorBalance >  0) {             
                        row += '<td class="text-center">' + numeral((rec.curBalance - rec.priorBalance) / ((monthDiff / 12)) / rec.priorBalance).format('%0.0') + '</td>';
                    }
                    else {
                        row += '<td class="text-center">-</td>';
                    }
                
                    row += '</tr>';
                    $('#' + tblToUse + ' tbody').append(row);
                }

                //throw total rows on
                if (i == tbData.length - 1) {
                    self.generateTotRow('liabilityTable', 'TOTAL LIABILITIES & EQUITY', monthDiff, tots.liabCurBal, tots.liabPriorBal)
                    self.generateTotRow('assetTable', 'TOTAL ASSETS', monthDiff, tots.assetCurBal, tots.assetPriorBal)
                }
            }

            $('.asOfDateHeader').text(self.balanceSheetMixViewData().asOfDate);

            $('#assetCurrentPieChart').highcharts(charting.createBalanceSheetMixPie(self.balanceSheetMixViewData().asOfDate, assetData.curData, charting.chartColorsPalette2));
            $('#assetPriorPieChart').highcharts(charting.createBalanceSheetMixPie(self.balanceSheetMixViewData().offSetDate, assetData.priorData, charting.chartColorsPalette2));
            $('#liabilityCurrentPieChart').highcharts(charting.createBalanceSheetMixPie(self.balanceSheetMixViewData().asOfDate, liabData.curData, charting.chartColorsPalette2));
            $('#liabilityPriorPieChart').highcharts(charting.createBalanceSheetMixPie(self.balanceSheetMixViewData().offSetDate, liabData.priorData, charting.chartColorsPalette2));

            $('#balancesheetmix-view').height($('#balancesheetmix-view').height());
            global.loadingReport(false);


            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                setTimeout(function () { hiqPdfConverter.startConversion() }, 500);
                $('#reportLoaded').text('1');
            }

        };


    };

    return balanceSheetMixVm;

});
