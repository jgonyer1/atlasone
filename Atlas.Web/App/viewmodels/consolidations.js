﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model'],
    function (globalContext, router, system, app, logger, model) {
        var consolidations = ko.observable();
        var curPage = "Consolidations";
        function activate(routeData) {
            globalContext.logEvent("Viewed", curPage);
            return globalContext.getConsolidations(consolidations)
                .then(function () {
                    alert(consolidations()[0].name());
            });
        }
        //Teryu
        var vm = {
            activate: activate,
            title: 'Consolidations',
            consolidations: consolidations,
        };

        return vm;

    });
