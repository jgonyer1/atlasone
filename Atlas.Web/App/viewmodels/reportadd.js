﻿define(
    function (require) {
        var vm = {};

        var globalContext = require('services/globalcontext');
        var router = require('plugins/router');
        var system = require('durandal/system');
        var app = require('durandal/app');
        var logger = require('services/logger');
        var onechartconfig = require('viewmodels/onechartconfig');
        var twochartconfig = require('viewmodels/twochartconfig');
        var simcompareconfig = require('viewmodels/simcompareconfig');
        var eveconfig = require('viewmodels/eveconfig');
        var documentconfig = require('viewmodels/documentconfig');
        var BalanceSheetCompareConfig = require('viewmodels/balancesheetcompareconfig');
        var BalanceSheetMixConfig = require('viewmodels/balancesheetmixconfig');
        var basicsurplusreportconfig = require('viewmodels/basicsurplusreportconfig');
        var interestratepolicyguidelinesconfig = require('viewmodels/interestratepolicyguidelinesconfig');
        var loancapfloorconfig = require('viewmodels/loancapfloorconfig');
        var staticgapconfig = require('viewmodels/staticgapconfig');
        var fundingmatrixconfig = require('viewmodels/fundingmatrixconfig');
        var timedepositmigrationconfig = require('viewmodels/timedepositmigrationconfig');
        var corefundingutilizationconfig = require('viewmodels/corefundingutilizationconfig');
        var yieldcurveshiftconfig = require('viewmodels/yieldcurveshiftconfig');
        var historicalBalanceSheetNIIconfig = require('viewmodels/historicalbalancesheetniiconfig');
        var simulationsummaryconfig = require('viewmodels/simulationsummaryconfig');
        var coverpageconfig = require('viewmodels/coverpageconfig');
        var evenevassumptionsconfig = require('viewmodels/evenevassumptionsconfig');
        var cashflowreportconfig = require('viewmodels/cashflowreportconfig');
        var netcashflowconfig = require('viewmodels/netcashflowconfig');
        var summaryrateconfig = require('viewmodels/summaryrateconfig');
        var asc825balancesheetconfig = require('viewmodels/asc825balancesheetconfig');
        var prepaymentdetailsconfig = require('viewmodels/prepaymentdetailsconfig');
        var asc825datasourceconfig = require('viewmodels/asc825datasourceconfig');
        var asc825discountrateconfig = require('viewmodels/asc825discountrateconfig');
        var asc825worksheetconfig = require('viewmodels/asc825worksheetconfig');
        var asc825marketvalueconfig = require('viewmodels/asc825marketvalueconfig');
        var ratechangematrixconfig = require('viewmodels/ratechangematrixconfig');
        var investmentportfoliovaluationconfig = require('viewmodels/investmentportfoliovaluationconfig');
        var investmentdetailconfig = require('viewmodels/investmentdetailconfig');
        var investmenterrorconfig = require('viewmodels/investmenterrorconfig');
        var asc825assumptionmethodsconfig = require('viewmodels/asc825assumptionmethodsconfig');
        var assumptionmethodsconfig = require('viewmodels/assumptionmethodsconfig');
        var liabilitypricinganalysisconfig = require('viewmodels/liabilitypricinganalysisconfig');
        var liquidityprojectionconfig = require('viewmodels/liquidityprojectionconfig');
        var detailedsimulationassumptionconfig = require('viewmodels/detailedsimulationassumptionconfig');
        var summaryofresultsconfig = require('viewmodels/summaryofresultsconfig');
        var niireconconfig = require('viewmodels/niireconconfig');
        var capitalanalysisconfig = require('viewmodels/capitalanalysisconfig');
        var sectionpageconfig = require('viewmodels/sectionpageconfig');
        var lookbackreportconfig = require('viewmodels/lookbackreportconfig');
        var executiverisksummaryconfig = require('viewmodels/executiverisksummaryconfig');
        var marginalcostoffundsconfig = require('viewmodels/marginalcostoffundsconfig');
        var inventoryliquidityresourcesconfig = require('viewmodels/inventoryliquidityresourcesconfig');
        var model = require('services/model');
        
        var naModal = require('viewmodels/navigateawaymodal');

        var packageId;
        var thePackage = ko.observable();
        var packageLength;
        var reportType;
        var reportName = ko.observable();
        var thisReport = ko.observable();
        var specificReport = ko.observable();
        var isSaving = ko.observable(false);
        var showName = ko.observable(true);
        var thisConcreteReportVm = ko.observable();
        var sortedFootnotes = ko.observableArray();
        var sortedLeftSplitFootnotes = ko.observableArray();
        var sortedRightSplitFootnotes = ko.observableArray();
        var hasSplitFootnotes = ko.observable(false);
        var canAddFootnotes = ko.observable(true);
        var profile = ko.observable();
        var bankInfo = ko.observable();
        var curPage = "ReportAdd";


        function activate(queryString) {
            globalContext.logEvent("Viewed", curPage);
            return globalContext.getUserProfile(profile).then(function () {
                sortedFootnotes([]);
                packageId = queryString.packageid;
                return globalContext.getPackageById(packageId, thePackage).then(function () {
                    return globalContext.getBankInfo(bankInfo, profile).then(function () {
                        packageLength = queryString.packagelength;
                        reportType = queryString.reporttype;

                        for (var i = 0; i < model.reportNames.length; i++) {
                            if (model.reportNames[i].name == reportType) {
                                if (reportType == "Eve") {
                                    if (bankInfo().creditUnion.instType == "CU") {
                                        reportName("NEV");
                                    } else {
                                        reportName("EVE");
                                    }
                                    
                                } else {
                                    reportName(model.reportNames[i].dispName);
                                }
                                
                                break;
                            }
                        }
                        //Disable Split foot notes
                        canAddFootnotes(true);
                        hasSplitFootnotes(false);

                        thisReport(globalContext.createReport(packageId, packageLength, reportType, reportName()));
                        if (profile().databaseName.toUpperCase() == "DDW_ATLAS_TEMPLATES") {
                            thisReport().defaults(true);
                        }
                        specificReport = null;
                        specificReport = ko.observable();

                        if (reportType === "OneChart") {
                            specificReport(globalContext.createOneChart(packageId, packageLength, profile));
                            thisConcreteReportVm(new onechartconfig(specificReport));
                        } else if (reportType === "TwoChart") {
                            specificReport(globalContext.createTwoChart(packageId, packageLength, profile));
                            thisConcreteReportVm(new twochartconfig(specificReport));
                        }
                        else if (reportType === "SimCompare") {
                            specificReport(globalContext.createSimCompare(packageId, packageLength, profile));
                            thisConcreteReportVm(new simcompareconfig(specificReport));
                        }
                        else if (reportType === "Eve") {
                            specificReport(globalContext.createEve(packageId, packageLength, profile));
                            thisConcreteReportVm(new eveconfig(specificReport));
                        }
                        else if (reportType === "Document") {
                            specificReport(globalContext.createDocument(packageId, packageLength));
                            thisConcreteReportVm(new documentconfig(specificReport, vm, model));
                        }
                        else if (reportType === "BalanceSheetCompare") {
                            specificReport(globalContext.createBalanceSheetCompare(packageId, packageLength, profile));
                            thisConcreteReportVm(new BalanceSheetCompareConfig(specificReport));
                        }
                        else if (reportType === "BalanceSheetMix") {
                            specificReport(globalContext.createBalanceSheetMix(packageId, packageLength, profile));
                            thisConcreteReportVm(new BalanceSheetMixConfig(specificReport));
                        }
                        else if (reportType == 'BasicSurplusReport') {
                            specificReport(globalContext.createBasicSurplusReport(packageId, packageLength, profile));
                            thisConcreteReportVm(new basicsurplusreportconfig(specificReport));
                        }
                        else if (reportType == 'InterestRatePolicyGuidelines') {
                            specificReport(globalContext.createInterestRatePolicyGuidelines(packageId, packageLength, profile));
                            thisConcreteReportVm(new interestratepolicyguidelinesconfig(specificReport));
                        }
                        else if (reportType === "LoanCapFloor") {
                            showName(false);
                            hasSplitFootnotes(true);
                            canAddFootnotes(false);
                            specificReport(globalContext.createLoanCapFloor(packageId, packageLength, profile));
                            thisConcreteReportVm(new loancapfloorconfig(specificReport, sortedFootnotes, thisReport));
                        }
                        else if (reportType === "StaticGap") {
                            specificReport(globalContext.createStaticGap(profile));
                            thisConcreteReportVm(new staticgapconfig(specificReport));
                        }
                        else if (reportType === "FundingMatrix") {
                            specificReport(globalContext.createFundingMatrix(packageId, packageLength, profile));
                            thisConcreteReportVm(new fundingmatrixconfig(specificReport));
                        }
                        else if (reportType === "TimeDepositMigration") {
                            specificReport(globalContext.createTimeDepositMigration(packageId, packageLength, profile));
                            thisConcreteReportVm(new timedepositmigrationconfig(specificReport));
                        }
                        else if (reportType === "CoreFundingUtilization") {
                            specificReport(globalContext.createCoreFundingUtilization(packageId, packageLength, profile));
                            thisConcreteReportVm(new corefundingutilizationconfig(specificReport));
                        }
                        else if (reportType === "YieldCurveShift") {
                            specificReport(globalContext.createYieldCurveShift(packageId, packageLength));
                            thisConcreteReportVm(new yieldcurveshiftconfig(specificReport));
                        }
                        else if (reportType === "HistoricalBalanceSheetNII") {
                            specificReport(globalContext.createHistoricalBalanceSheetNII(packageId, packageLength, profile));;
                            thisConcreteReportVm(new historicalBalanceSheetNIIconfig(specificReport));
                        }
                        else if (reportType === "SimulationSummary") {
                            specificReport(globalContext.createSimulationSummary(packageId, packageLength, profile));
                            thisConcreteReportVm(new simulationsummaryconfig(specificReport));
                        }
                        else if (reportType == 'CoverPage') {
                            //specificReport(globalContext.createCoverPage(packageId, packageLength, thePackage().name(), profile));
                            //packageType will default to 0 naturally, so pass in the text for that type (ALCO Package)
                            specificReport(globalContext.createCoverPage(packageId, packageLength, "Asset/Liability Management Review", profile));
                            thisConcreteReportVm(new coverpageconfig(specificReport));
                        }
                        else if (reportType == 'SectionPage') {
                            specificReport(globalContext.createSectionPage());
                            thisConcreteReportVm(new sectionpageconfig(specificReport));
                        }
                        else if (reportType == 'EveNevAssumptions') {
                            specificReport(globalContext.createEveNevAssumptions(packageId, packageLength, profile));
                            thisConcreteReportVm(new evenevassumptionsconfig(specificReport));
                        }
                        else if (reportType == 'CashflowReport') {
                            specificReport(globalContext.createCashflowReport(packageId, packageLength, profile));
                            thisConcreteReportVm(new cashflowreportconfig(specificReport));
                        }
                        else if (reportType == 'NetCashflow') {
                            specificReport(globalContext.createNetCashflow(packageId, packageLength, profile));
                            thisConcreteReportVm(new netcashflowconfig(specificReport));
                        }
                        else if (reportType == 'SummaryRate') {
                            specificReport(globalContext.createSummaryRate(packageId, packageLength, profile));
                            thisConcreteReportVm(new summaryrateconfig(specificReport));
                        }
                        else if (reportType == 'ASC825BalanceSheet') {
                            specificReport(globalContext.createASC825BalanceSheet(packageId, packageLength, profile));
                            thisConcreteReportVm(new asc825balancesheetconfig(specificReport));
                        }
                        else if (reportType == 'PrepaymentDetails') {
                            specificReport(globalContext.createPrepaymentDetails(packageId, packageLength, profile));
                            thisConcreteReportVm(new prepaymentdetailsconfig(specificReport));
                        }
                        else if (reportType == 'ASC825DataSource') {
                            specificReport(globalContext.createASC825DataSource(packageId, packageLength, profile));
                            thisConcreteReportVm(new asc825datasourceconfig(specificReport));
                        }
                        else if (reportType == 'ASC825DiscountRate') {
                            specificReport(globalContext.createASC825DiscountRate(packageId, packageLength, profile));
                            thisConcreteReportVm(new asc825discountrateconfig(specificReport));
                        }
                        else if (reportType == 'ASC825Worksheet') {
                            specificReport(globalContext.createASC825Worksheet(packageId, packageLength, profile));
                            thisConcreteReportVm(new asc825worksheetconfig(specificReport));
                        }
                        else if (reportType == 'ASC825MarketValue') {
                            specificReport(globalContext.createASC825MarketValue(packageId, packageLength, profile));
                            thisConcreteReportVm(new asc825marketvalueconfig(specificReport));
                        }
                        else if (reportType == 'RateChangeMatrix') {
                            specificReport(globalContext.createRateChangeMatrix(packageId, packageLength, profile));
                            thisConcreteReportVm(new ratechangematrixconfig(specificReport));
                        }
                        else if (reportType == 'InvestmentPortfolioValuation') {
                            specificReport(globalContext.createInvestmentPortfolioValuation(packageId, packageLength, profile));
                            thisConcreteReportVm(new investmentportfoliovaluationconfig(specificReport));
                        }
                        else if (reportType == 'InvestmentDetail') {
                            specificReport(globalContext.createInvestmentDetail(packageId, packageLength, profile));
                            thisConcreteReportVm(new investmentdetailconfig(specificReport));
                        }
                        else if (reportType == 'InvestmentError') {
                            specificReport(globalContext.createInvestmentError(packageId, packageLength, profile));
                            thisConcreteReportVm(new investmenterrorconfig(specificReport));
                        }
                        else if (reportType == 'ASC825AssumptionMethods') {
                            specificReport(globalContext.createASC825AssumptionMethods(packageId, packageLength, profile));
                            thisConcreteReportVm(new asc825assumptionmethodsconfig(specificReport));
                        }
                        else if (reportType == 'AssumptionMethods') {
                            specificReport(globalContext.createAssumptionMethods(packageId, packageLength, profile));
                            thisConcreteReportVm(new assumptionmethodsconfig(specificReport));
                        }
                        else if (reportType == 'LiabilityPricingAnalysis') {
                            specificReport(globalContext.createLiabilityPricingAnalysis(packageId, packageLength, profile));
                            thisConcreteReportVm(new liabilitypricinganalysisconfig(specificReport, thisReport));
                        }
                        else if (reportType == 'LiquidityProjection') {
                            specificReport(globalContext.createLiquidityProjection(packageId, packageLength, profile));
                            thisConcreteReportVm(new liquidityprojectionconfig(specificReport, thisReport));
                        }
                        else if (reportType == 'DetailedSimulationAssumption') {
                            specificReport(globalContext.createDetailedSimulationAssumption(packageId, packageLength, profile));
                            thisConcreteReportVm(new detailedsimulationassumptionconfig(specificReport, thisReport));
                        }
                        else if (reportType == 'NIIRecon') {
                            var modelSetup = ko.observable();
                            canAddFootnotes(false);
                            globalContext.getModelSetup(modelSetup).fin(function () {
                                specificReport(globalContext.createNIIRecon(packageId, packageLength, profile, modelSetup));
                                thisConcreteReportVm(new niireconconfig(specificReport));
                            })
                        }
                        else if (reportType == 'SummaryOfResults') {
                            canAddFootnotes(false);
                            specificReport(globalContext.createSummaryOfResults(packageId, packageLength, profile));
                            thisConcreteReportVm(new summaryofresultsconfig(specificReport));
                        }
                        else if (reportType == 'CapitalAnalysis') {
                            specificReport(globalContext.createCapitalAnalysis(packageId, packageLength, profile));
                            thisConcreteReportVm(new capitalanalysisconfig(specificReport));
                        }
                        else if (reportType == 'LookbackReport') {
                            specificReport(globalContext.createLookbackReport(packageId, packageLength, profile));
                            thisConcreteReportVm(new lookbackreportconfig(specificReport));
                        }
                        else if (reportType == 'ExecutiveRiskSummary') {
                            specificReport(globalContext.createExecutiveRiskSummary(packageId, packageLength, profile));
                            thisConcreteReportVm(new executiverisksummaryconfig(specificReport));
                        }
                        else if (reportType == 'InventoryLiquidityResources') {
                            specificReport(globalContext.createInventoryLiquidityResources(packageId, packageLength, profile));
                            thisConcreteReportVm(new inventoryliquidityresourcesconfig(specificReport));
                        }
                        else if (reportType == 'MarginalCostOfFunds') {
                            specificReport(globalContext.createMarginalCostOfFunds(packageId, packageLength, profile));
                            thisConcreteReportVm(new marginalcostoffundsconfig(specificReport));
                        }
                    });   
                });
            });

        }
        function addFootnote() {
            var unlimitedFootnotes = ["AssumptionMethods", "InventoryLiquidityResources"];
            //if (thisReport().footnotes().length < 3) {
            if ((unlimitedFootnotes.indexOf(thisReport().shortName()) == -1 && thisReport().footnotes().length < 3) || unlimitedFootnotes.indexOf(thisReport().shortName()) > -1) {
                globalContext.logEvent("Added Footnote", curPage);
                var newFootnote = globalContext.createFootnote(thisReport().id(), thisReport().footnotes().length);
                newFootnote.priority(thisReport().footnotes().length);
                newFootnote.report(thisReport());
                sortFootnotes();
                $("#footnote_tb_" + newFootnote.id()).focus();
            } else {
                globalContext.logEvent("Tried to add footnote when at max", curPage);
                var msg = 'You may only add three footnotes to this report.';
                var title = 'Footnote Limit Reached';
                app.showMessage(msg, title, ['Ok']);
            }
        }
        function addSplitFootnote(side) {//0 = left 1 = right
            var tempFootnoteArr = thisReport().splitFootnotes().filter(function (footnote) { return footnote.side() == side });
            if (tempFootnoteArr.length < 3) {
                globalContext.logEvent("Added Split Footnote", curPage);
                var newSplitFootnote = globalContext.createSplitFootnote();
                newSplitFootnote.priority(tempFootnoteArr.length);
                newSplitFootnote.report(thisReport());
                newSplitFootnote.side(side);
                sortFootnotes();
            } else {
                globalContext.logEvent("Tried to add split footnote when at max", curPage);
                var msg = 'You may only add three footnotes to this report.';
                var title = 'Footnote Limit Reached';
                app.showMessage(msg, title, ['Ok']);
            }
        }


        function updateHardCodedFootnotes(textArr) {
            //delete all the footnotes and replace them with the the new/exisiting ones
            for (var i = 0; i < thisReport().footnotes().length; i++) {
                thisReport().footnotes()[i].entityAspect.setDeleted();
                var newFootnote = globalContext.createFootnote(thisReport().id(), thisReport().footnotes().length);
                newFootnote.priority(i + 1);
                newFootnote.report(thisReport());
                newFootnote.text(textArr[i]);
            }
            sortedFootnotes();
        }

        //$(document.ready(function () {
        //    alert($('.lcf-footnote-selects').length);
        //}));
        

        function removeFootnote(footnote) {

            var msg = 'Delete footnote "' + footnote.text() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    globalContext.logEvent("Deleted Footnote", curPage);
                    footnote.entityAspect.setDeleted();
                    if (!footnote.side) {
                        thisReport().footnotes().forEach(function (footnote, index) {
                            footnote.priority(index);
                        });
                    } else {
                        thisReport().splitFootnotes().filter(function (item) { return item.side() == footnote.side() }).forEach(function (thisfootnote, index) {
                            thisfootnote.priority(index);
                        });
                    }

                    sortFootnotes();
                }
            }

        };

        var sortFootnotes = function () {
            var sorted = thisReport().footnotes().sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            var lefts = thisReport().splitFootnotes().filter(function (item) { return item.side() == 0 });
            var leftSorted = lefts.sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            var rights = thisReport().splitFootnotes().filter(function (item) { return item.side() == 1 });
            var rightSorted = rights.sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            sortedFootnotes(sorted);
            sortedLeftSplitFootnotes(leftSorted);
            sortedRightSplitFootnotes(rightSorted);
        };

        function fixupFootnotePriority() {
            if (thisReport() && thisReport().footnotes()) {
                thisReport().footnotes().forEach(function (footnote, index) {
                    footnote.priority(index);
                });
            }
            if (thisReport() && thisReport().splitFootnotes()) {
                thisReport().splitFootnotes().filter(function (item) { return item.side() == 0 }).forEach(function (footnote, index) {
                    footnote.priority(index);
                });
                thisReport().splitFootnotes().filter(function (item) { return item.side() == 1 }).forEach(function (footnote, index) {
                    footnote.priority(index);
                });
            }

        };

        var hasChanges = ko.computed(function () {
            return globalContext.hasChanges();
        });

        var cancel = function () {
            globalContext.cancelChanges();
     
            router.navigate('#/packageconfig/' + packageId);
        };

        var canSave = ko.computed(function () {
            return hasChanges() && !isSaving();
        });

        var save = function (gotoView) {
            isSaving(true);
            globalContext.logEvent("Added report " + thisReport().name(), curPage);
            //Do not save it until I have id of actual report to link it.  This enforces the inhertitence without making it overly slow becuase of Entity Framework
            //May be the largest hack I ever wrote but this way we did not have to change crazy amount of things
            specificReport().entityAspect.setUnchanged();
            console.log('--------------' + thisReport().shortName());
            switch (thisReport().shortName()) {
                case "ASC825Worksheet":
                case "ASC825AssumptionMethods":
                case "ASC825BalanceSheet":
                case "ASC825DataSource":
                case "ASC825DiscountRate":
                case "AssumptionMethods":
                case "BalanceSheetCompare":
                case "BalanceSheetMix":
                case "BasicSurplusReport":
                case "DetailedSimulationAssumption":
                case "Document":
                case "EveNevAssumptions":
                case "InvestmentDetail":
                case "InvestmentError":
                //case "FundingMatrix":
                case "LiabilityPricingAnalysis":
                case "LoanCapFloor":
                case "StaticGap":
                case "SimulationSummary":
                case "TimeDepositMigration":
                case "CoverPage":
                case "OneChart":
                case "TwoChart":
                case "MarginalCostOfFunds":
                    break;
                case "InventoryLiquidityResources":
                    for (var s = 0; s < specificReport().inventoryLiquidityResourcesDateOffsets().length; s++) {
                        specificReport().inventoryLiquidityResourcesDateOffsets()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().inventoryLiquidityResourcesTier().length; s++) {
                        specificReport().inventoryLiquidityResourcesTier()[s].entityAspect.setUnchanged();
                        for (var i = 0; i < specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail().length; i++) {

                            specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail()[i].entityAspect.setUnchanged();
                            for (var y = 0; y < specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail()[i].inventoryLiquidityResourcesTierDetailCollection().length; y++) {
                                specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail()[i].inventoryLiquidityResourcesTierDetailCollection()[y].entityAspect.setUnchanged();
                            }
                        }
                    }


                    for (var s = 0; s < specificReport().inventoryLiquidityResourcesWholesaleFunding().length; s++) {
                        specificReport().inventoryLiquidityResourcesWholesaleFunding()[s].entityAspect.setUnchanged();
                    }

                    break;
                case "LiquidityProjection":
                    for (var s = 0; s < specificReport().liquidityProjectionDateOffsets().length; s++) {
                        specificReport().liquidityProjectionDateOffsets()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "CapitalAnalysis":
                    //Need to set all children parent id of report object
                    for (var s = 0; s < specificReport().historicalBufferOffsets().length; s++) {
                        specificReport().historicalBufferOffsets()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "CashflowReport":
                    for (var s = 0; s < specificReport().cashflowSimulationScenario().length; s++) {
                        specificReport().cashflowSimulationScenario()[s].entityAspect.setUnchanged();
                    }
                    break;
                    case "NetCashflow":
                    for (var s = 0; s < specificReport().netCashflowSimulationScenario().length; s++) {
                        specificReport().netCashflowSimulationScenario()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "CoreFundingUtilization":
                    for (var s = 0; s < specificReport().coreFundingUtilizationScenarioTypes().length; s++) {
                        specificReport().coreFundingUtilizationScenarioTypes()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().coreFundingUtilizationDateOffSets().length; s++) {
                        specificReport().coreFundingUtilizationDateOffSets()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "Eve":
                    for (var s = 0; s < specificReport().eveScenarioTypes().length; s++) {
                        specificReport().eveScenarioTypes()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "HistoricalBalanceSheetNII":
                    for (var s = 0; s < specificReport().historicalBalanceSheetNIISimulations().length; s++) {
                        specificReport().historicalBalanceSheetNIISimulations()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().historicalBalanceSheetNIIScenarios().length; s++) {
                        specificReport().historicalBalanceSheetNIIScenarios()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().historicalBalanceSheetNIITopFootNotes().length; s++) {
                        specificReport().historicalBalanceSheetNIITopFootNotes()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().historicalBalanceSheetNIIBottomFootNotes().length; s++) {
                        specificReport().historicalBalanceSheetNIIBottomFootNotes()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "InvestmentPortfolioValuation":
                    for (var s = 0; s < specificReport().scenarios().length; s++) {
                        specificReport().scenarios()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "NIIRecon":
                    for (var s = 0; s < specificReport().scenarioTypes().length; s++) {
                        specificReport().scenarioTypes()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "PrepaymentDetails":
                    for (var s = 0; s < specificReport().prePaymentDetailScenarioTypes().length; s++) {
                        specificReport().prePaymentDetailScenarioTypes()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "RateChangeMatrix":
                    for (var s = 0; s < specificReport().rateChangeMatrixScenarios().length; s++) {
                        specificReport().rateChangeMatrixScenarios()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "SimCompare":
                    for (var s = 0; s < specificReport().simCompareSimulationTypes().length; s++) {
                        specificReport().simCompareSimulationTypes()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().simCompareScenarioTypes().length; s++) {
                        specificReport().simCompareScenarioTypes()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "ASC825MarketValue":
                    for (var s = 0; s < specificReport().eveScenarioTypes().length; s++) {
                        specificReport().eveScenarioTypes()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "SummaryRate":
                    for (var s = 0; s < specificReport().summaryRateScenarios().length; s++) {
                        specificReport().summaryRateScenarios()[s].entityAspect.setUnchanged();
                    }

                    break;
                case "YieldCurveShift":
                    for (var s = 0; s < specificReport().yieldCurveShiftScenarioTypes().length; s++) {
                        specificReport().yieldCurveShiftScenarioTypes()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().yieldCurveShiftPeriodCompares().length; s++) {
                        specificReport().yieldCurveShiftPeriodCompares()[s].entityAspect.setUnchanged();
                    }
                    break;
                case "InterestRatePolicyGuidelines":
                    for (var s = 0; s < specificReport().dates().length; s++) {
                        specificReport().dates()[s].entityAspect.setUnchanged();
                    }

                    for (var s = 0; s < specificReport().nIIComparatives().length; s++) {

                        specificReport().nIIComparatives()[s].entityAspect.setUnchanged();
                        for (var z = 0; z < specificReport().nIIComparatives()[s].scenarioTypes().length; z++) {
                            specificReport().nIIComparatives()[s].scenarioTypes()[z].entityAspect.setUnchanged();
                            for (var x = 0; x < specificReport().nIIComparatives()[s].scenarioTypes()[z].scenarios().length; x++) {
                                specificReport().nIIComparatives()[s].scenarioTypes()[z].scenarios()[x].entityAspect.setUnchanged();
                            }
                        }
                    }

                    for (var y = 0; y < specificReport().eVEComparatives().length; y++) {
                        specificReport().eVEComparatives()[y].entityAspect.setUnchanged();
                        for (var i = 0; i < specificReport().eVEComparatives()[y].scenarios().length; i++) {
                            specificReport().eVEComparatives()[y].scenarios()[i].entityAspect.setUnchanged();
                        }                      
                    }

                    break;
                case "LookbackReport":
                    for (var s = 0; s < specificReport().sections().length; s++) {
                        specificReport().sections()[s].entityAspect.setUnchanged();
                        //alert(specificReport().sections()[s].lookbackTypes().length);
                        for (var i = 0; i < specificReport().sections()[s].lookbackTypes().length; i++) {
                            specificReport().sections()[s].lookbackTypes()[i].entityAspect.setUnchanged();
                        }
                    }

                    break;
                case "ExecutiveRiskSummary":

                    //Liq Sections
                    for (var s = 0; s < specificReport().executiveRiskSummaryLiquiditySections().length; s++) {
                        specificReport().executiveRiskSummaryLiquiditySections()[s].entityAspect.setUnchanged();
                        for (var i = 0; i < specificReport().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets().length; i++) {
                            specificReport().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets()[i].entityAspect.setUnchanged();
                        }
                    }

                    //IRR Sections
                    for (var s = 0; s < specificReport().executiveSummaryIRRSections().length; s++) {
                        specificReport().executiveSummaryIRRSections()[s].entityAspect.setUnchanged();
                        for (var i = 0; i < specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets().length; i++) {
                            specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].entityAspect.setUnchanged();
                            for (var k = 0; k < specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios().length; k++) {
                                specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios()[k].entityAspect.setUnchanged();
                            }
                        }
                    }

                    //Capital Section
                    for (var s = 0; s < specificReport().executiveRiskSummaryCapitalSection().length; s++) {
                        specificReport().executiveRiskSummaryCapitalSection()[s].entityAspect.setUnchanged();
                    }

                    //Date offset section
                    for (var s = 0; s < specificReport().executiveRiskSummaryDateOffsets().length; s++) {
                        specificReport().executiveRiskSummaryDateOffsets()[s].entityAspect.setUnchanged();
                    }

                    break;
                case "FundingMatrix":
                    for (var o = 0; o < specificReport().fundingMatrixOffsets().length; o++) {
                        specificReport().fundingMatrixOffsets()[o].entityAspect.setUnchanged();
                    }
                    break;
            }

            return globalContext.saveChanges().fin(function () {
                specificReport().entityAspect.setAdded();
                specificReport().id(thisReport().id());

                switch (thisReport().shortName()) {
                    case "ASC825Worksheet":
                    case "ASC825AssumptionMethods":
                    case "ASC825BalanceSheet":
                    case "ASC825DataSource":
                    case "ASC825DiscountRate":
                    case "AssumptionMethods":
                    case "BalanceSheetCompare":
                    case "BalanceSheetMix":
                    case "BasicSurplusReport":
                    case "DetailedSimulationAssumption":
                    case "Document":
                    case "EveNevAssumptions":
                    case "InvestmentDetail":
                    case "InvestmentError":
                    //case "FundingMatrix":
                    case "LiabilityPricingAnalysis":
                    case "LoanCapFloor":
                    case "StaticGap":
                    case "SimulationSummary":
                    case "TimeDepositMigration":
                    case "CoverPage":
                    case "OneChart":
                    case "TwoChart":
                    case "MarginalCostOfFunds":
                        break;
                    case "InventoryLiquidityResources":
                        console.log('adding back');
                        for (var s = 0; s < specificReport().inventoryLiquidityResourcesDateOffsets().length; s++) {
                            specificReport().inventoryLiquidityResourcesDateOffsets()[s].entityAspect.setAdded();
                        }

                        for (var s = 0; s < specificReport().inventoryLiquidityResourcesTier().length; s++) {
                            specificReport().inventoryLiquidityResourcesTier()[s].entityAspect.setAdded();
                            for (var i = 0; i < specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail().length; i++) {

                                specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail()[i].entityAspect.setAdded();
                                for (var y = 0; y < specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail()[i].inventoryLiquidityResourcesTierDetailCollection().length; y++) {
                                    specificReport().inventoryLiquidityResourcesTier()[s].inventoryLiquidityResourcesTierDetail()[i].inventoryLiquidityResourcesTierDetailCollection()[y].entityAspect.setAdded();
                                }
                            }
                        }


                        for (var s = 0; s < specificReport().inventoryLiquidityResourcesWholesaleFunding().length; s++) {
                            specificReport().inventoryLiquidityResourcesWholesaleFunding()[s].entityAspect.setAdded();
                        }


                        break;
                    case "LiquidityProjection":
                        for (var s = 0; s < specificReport().liquidityProjectionDateOffsets().length; s++) {
                            specificReport().liquidityProjectionDateOffsets()[s].liquidityProjectionId(thisReport().id());
                            specificReport().liquidityProjectionDateOffsets()[s].entityAspect.setAdded();
                        }
                        break;

                    case "CapitalAnalysis":
                        //Need to set all children parent id of report object
                        for (var s = 0; s < specificReport().historicalBufferOffsets().length; s++) {
                            specificReport().historicalBufferOffsets()[s].entityAspect.setAdded();
                            specificReport().historicalBufferOffsets()[s].capitalAnalysisId(thisReport().id());
                        }
                        break;

                    case "CashflowReport":
                        //remove subscriptions
                        //for (var z = 0; z < specificReport().cashflowSimulationScenario().length; z++) {

                        //    specificReport().cashflowSimulationScenario()[z].instSub.dispose();
                        //    specificReport().cashflowSimulationScenario()[z].aodSub.dispose();
                        //    specificReport().cashflowSimulationScenario()[z].ovSub.dispose();
                        //}

                        for (var s = 0; s < specificReport().cashflowSimulationScenario().length; s++) {
                            specificReport().cashflowSimulationScenario()[s].entityAspect.setAdded();
                            specificReport().cashflowSimulationScenario()[s].cashflowReportId(thisReport().id());
                        }
                        break;

                    case "NetCashflow":
                        for (var s = 0; s < specificReport().netCashflowSimulationScenario().length; s++) {
                            specificReport().netCashflowSimulationScenario()[s].entityAspect.setAdded();
                            specificReport().netCashflowSimulationScenario()[s].netCashflowId(thisReport().id());
                        }
                        break;

                    case "CoreFundingUtilization":
                        for (var s = 0; s < specificReport().coreFundingUtilizationScenarioTypes().length; s++) {
                            specificReport().coreFundingUtilizationScenarioTypes()[s].entityAspect.setAdded();
                            specificReport().coreFundingUtilizationScenarioTypes()[s].coreFundingUtilizationId(thisReport().id());
                        }

                        for (var s = 0; s < specificReport().coreFundingUtilizationDateOffSets().length; s++) {
                            specificReport().coreFundingUtilizationDateOffSets()[s].entityAspect.setAdded();
                            specificReport().coreFundingUtilizationDateOffSets()[s].coreFundingUtilizationId(thisReport().id());
                        }
                        break;

                    case "Eve":
                        for (var s = 0; s < specificReport().eveScenarioTypes().length; s++) {
                            specificReport().eveScenarioTypes()[s].entityAspect.setAdded();
                            specificReport().eveScenarioTypes()[s].eveId(thisReport().id());
                        }
                        break;

                    case "HistoricalBalanceSheetNII":
                        for (var s = 0; s < specificReport().historicalBalanceSheetNIISimulations().length; s++) {
                            specificReport().historicalBalanceSheetNIISimulations()[s].entityAspect.setAdded();
                            specificReport().historicalBalanceSheetNIISimulations()[s].historicalBalanceSheetNIIId(thisReport().id());
                        }

                        for (var s = 0; s < specificReport().historicalBalanceSheetNIIScenarios().length; s++) {
                            specificReport().historicalBalanceSheetNIIScenarios()[s].entityAspect.setAdded();
                            specificReport().historicalBalanceSheetNIIScenarios()[s].historicalBalanceSheetNIIId(thisReport().id());
                        }

                        for (var s = 0; s < specificReport().historicalBalanceSheetNIITopFootNotes().length; s++) {
                            specificReport().historicalBalanceSheetNIITopFootNotes()[s].entityAspect.setAdded();
                            specificReport().historicalBalanceSheetNIITopFootNotes()[s].historicalBalanceSheetNIIId(thisReport().id());
                        }

                        for (var s = 0; s < specificReport().historicalBalanceSheetNIIBottomFootNotes().length; s++) {
                            specificReport().historicalBalanceSheetNIIBottomFootNotes()[s].entityAspect.setAdded();
                            specificReport().historicalBalanceSheetNIIBottomFootNotes()[s].historicalBalanceSheetNIIId(thisReport().id());
                        }
                        break;

                    case "InvestmentPortfolioValuation":
                        for (var s = 0; s < specificReport().scenarios().length; s++) {
                            specificReport().scenarios()[s].entityAspect.setAdded();
                            specificReport().scenarios()[s].investmentPortfolioValuationId(thisReport().id());
                        }
                        break;

                    case "NIIRecon":
                        for (var s = 0; s < specificReport().scenarioTypes().length; s++) {
                            specificReport().scenarioTypes()[s].entityAspect.setAdded();
                            specificReport().scenarioTypes()[s].nIIReconId(thisReport().id());
                        }
                        break;

                    case "PrepaymentDetails":
                        for (var s = 0; s < specificReport().prePaymentDetailScenarioTypes().length; s++) {
                            specificReport().prePaymentDetailScenarioTypes()[s].entityAspect.setAdded();
                            specificReport().prePaymentDetailScenarioTypes()[s].prepaymentDetailsId(thisReport().id());
                        }
                        break;

                    case "RateChangeMatrix":
                        for (var s = 0; s < specificReport().rateChangeMatrixScenarios().length; s++) {
                            specificReport().rateChangeMatrixScenarios()[s].entityAspect.setAdded();
                            specificReport().rateChangeMatrixScenarios()[s].rateChangeMatrixId(thisReport().id());
                        }
                        break;

                    case "SimCompare":

                        //temp until I take these out of the model
                        //specificReport().leftSimulationTypeId(1);
                        //specificReport().rightSimulationTypeId(1);



                        for (var s = 0; s < specificReport().simCompareSimulationTypes().length; s++) {
                            specificReport().simCompareSimulationTypes()[s].entityAspect.setAdded();
                            specificReport().simCompareSimulationTypes()[s].simCompareId(thisReport().id());
                        }

                        for (var s = 0; s < specificReport().simCompareScenarioTypes().length; s++) {
                            specificReport().simCompareScenarioTypes()[s].entityAspect.setAdded();
                            specificReport().simCompareScenarioTypes()[s].simCompareId(thisReport().id());
                        }
                        break;

                    case "ASC825MarketValue":
                        for (var s = 0; s < specificReport().eveScenarioTypes().length; s++) {
                            specificReport().eveScenarioTypes()[s].entityAspect.setAdded();
                            specificReport().eveScenarioTypes()[s].aSC825MarketValueId(thisReport().id());
                        }
                        break;

                    case "SummaryRate":
                        for (var s = 0; s < specificReport().summaryRateScenarios().length; s++) {
                            specificReport().summaryRateScenarios()[s].entityAspect.setAdded();
                            specificReport().summaryRateScenarios()[s].summaryRateId(thisReport().id());
                        }

                        break;

                    case "YieldCurveShift":
                        for (var s = 0; s < specificReport().yieldCurveShiftScenarioTypes().length; s++) {
                            specificReport().yieldCurveShiftScenarioTypes()[s].entityAspect.setAdded();
                            specificReport().yieldCurveShiftScenarioTypes()[s].yieldCurveShiftId(thisReport().id());
                        }

                        for (var s = 0; s < specificReport().yieldCurveShiftPeriodCompares().length; s++) {
                            specificReport().yieldCurveShiftPeriodCompares()[s].entityAspect.setAdded();
                            specificReport().yieldCurveShiftPeriodCompares()[s].yieldCurveShiftId(thisReport().id());
                        }
                        break;

                    case "InterestRatePolicyGuidelines":
                        for (var s = 0; s < specificReport().dates().length; s++) {
                            specificReport().dates()[s].interestRatePolicyGuidelinesId(thisReport().id());
                            specificReport().dates()[s].entityAspect.setAdded();
                        }

                        for (var s = 0; s < specificReport().nIIComparatives().length; s++) {
                            specificReport().nIIComparatives()[s].interestRatePolicyGuidelinesId(thisReport().id());
                            specificReport().nIIComparatives()[s].entityAspect.setAdded();
                            for (var z = 0; z < specificReport().nIIComparatives()[s].scenarioTypes().length; z++) {
                                specificReport().nIIComparatives()[s].scenarioTypes()[z].entityAspect.setAdded();
                                for (var x = 0; x < specificReport().nIIComparatives()[s].scenarioTypes()[z].scenarios().length; x++) {
                                    specificReport().nIIComparatives()[s].scenarioTypes()[z].scenarios()[x].entityAspect.setAdded();
                                }
                            }
                        }

                        for (var s = 0; s < specificReport().eVEComparatives().length; s++) {
                            specificReport().eVEComparatives()[s].interestRatePolicyGuidelinesId(thisReport().id());
                            specificReport().eVEComparatives()[s].entityAspect.setAdded();
                            for (var i = 0; i < specificReport().eVEComparatives()[s].scenarios().length; i++) {
                                specificReport().eVEComparatives()[s].scenarios()[i].entityAspect.setAdded();
                            }
                        }
                        break;

                    case "LookbackReport":
                        for (var s = 0; s < specificReport().sections().length; s++) {
                            specificReport().sections()[s].lookbackReportId(thisReport().id());
                            specificReport().sections()[s].entityAspect.setAdded();
                            for (var i = 0; i < specificReport().sections()[s].lookbackTypes().length; i++) {
                                specificReport().sections()[s].lookbackTypes()[i].entityAspect.setAdded();
                            }
                        }

                        break;

                    case "ExecutiveRiskSummary":
                        //Liq Sections
                        for (var s = 0; s < specificReport().executiveRiskSummaryLiquiditySections().length; s++) {
                            specificReport().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryId(thisReport().id());
                            specificReport().executiveRiskSummaryLiquiditySections()[s].entityAspect.setAdded();
                            for (var i = 0; i < specificReport().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets().length; i++) {
                                specificReport().executiveRiskSummaryLiquiditySections()[s].executiveRiskSummaryLiquiditySectionDets()[i].entityAspect.setAdded();
                            }
                        }

                        //IRR Sections
                        for (var s = 0; s < specificReport().executiveSummaryIRRSections().length; s++) {
                            specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryId(thisReport().id());
                            specificReport().executiveSummaryIRRSections()[s].entityAspect.setAdded();
                            for (var i = 0; i < specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets().length; i++) {
                                specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].entityAspect.setAdded();
                                for (var k = 0; k < specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios().length; k++) {
                                    specificReport().executiveSummaryIRRSections()[s].executiveRiskSummaryIRRSectionDets()[i].executiveSummaryIRRScenarios()[k].entityAspect.setAdded();
                                }
                            }
                        }

                        //Capital Section
                        for (var s = 0; s < specificReport().executiveRiskSummaryCapitalSection().length; s++) {
                            specificReport().executiveRiskSummaryCapitalSection()[s].executiveRiskSummaryId(thisReport().id());
                            specificReport().executiveRiskSummaryCapitalSection()[s].entityAspect.setAdded();
                        }

                        //Date off set section

                        for (var s = 0; s < specificReport().executiveRiskSummaryDateOffsets().length; s++) {
                            specificReport().executiveRiskSummaryDateOffsets()[s].executiveRiskSummaryId(thisReport().id());
                            specificReport().executiveRiskSummaryDateOffsets()[s].entityAspect.setAdded();
                        }

                        break;

                    case "FundingMatrix":
                        for (var o = 0; o < specificReport().fundingMatrixOffsets().length; o++) {
                            specificReport().fundingMatrixOffsets()[o].fundingMatrixId(thisReport().id());
                            specificReport().fundingMatrixOffsets()[o].entityAspect.setAdded();

                        }
                        break;
                }

                finalSave(gotoView);
            });
        };

        var finalSave = function (gotoView) {
            var continuation;

            switch (gotoView) {
                case 'none':
                    continuation = function () { };
                    break;

                case 'config':
                    continuation = function () { router.navigate('#/reportconfig/' + vm.thisReport().id()); }
                    break;

                case 'view':
                    continuation = function () { router.navigate('#/reportview/' + vm.thisReport().id()); }
                    break;

                default:
                    var err = 'Goto View value of ' + gotoView + ' is not supported';
                    console.error(err);
                    throw err;
            }

            globalContext.saveChangesQuiet()
                .then(continuation)
                .fin(isSaving(false));
        };

        var canDeactivate = function () {
            if (!hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { save(); }
            );
        };

        this.deactivate = function (isClose) {
            console.log('disposing this bull shit');
         /*   thisReport().forEach(function (sub) {
                //TODO HACK subs should only be pushed ko.subscriptions, but sometimes numbers end up there!?
                if (sub.dispose)
                    sub.dispose();
            });*/
          //  specificReport().deactivate(isClose);

            /*specificReport().forEach(function (sub) {
                //TODO HACK subs should only be pushed ko.subscriptions, but sometimes numbers end up there!?
                if (sub.dispose)
                    sub.dispose();
            });*/
            
        };
        
        vm.activate = activate;
        vm.save = save;
        vm.finalSave = finalSave;
        vm.saveThenGoToConfig = function () { return save('config'); };
        vm.saveThenGoToView = function () { return save('view') };
        vm.canDeactivate = canDeactivate;
        vm.deactivate = deactivate;
        vm.showName = showName;
        vm.canSave = canSave;
        vm.hasChanges = hasChanges;
        vm.hasSplitFootnotes = hasSplitFootnotes;
        vm.canAddFootnotes = canAddFootnotes;
        vm.cancel = cancel;
        vm.thePackage = thePackage;
        vm.thisReport = thisReport;
        vm.thisConcreteReportVm = thisConcreteReportVm;
        vm.addFootnote = addFootnote;
        vm.addSplitFootnote = addSplitFootnote;
        vm.removeFootnote = removeFootnote;
        vm.sortedFootnotes = sortedFootnotes;
        vm.sortedLeftSplitFootnotes = sortedLeftSplitFootnotes;
        vm.sortedRightSplitFootnotes = sortedRightSplitFootnotes;
        vm.fixupFootnotePriority = fixupFootnotePriority;
        vm.profile = profile;
        vm.reportName = reportName

        return vm;
    });
