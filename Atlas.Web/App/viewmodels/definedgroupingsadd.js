﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model'
],
function (globalContext, router, system, app, logger, model) {        
    var definedGrouping = ko.observable();
    var isSaving = ko.observable(false);
    var curPage = "DefinedGroupingsAdd";
    function activate(queryString) {
        globalContext.logEvent("Viewed", curPage);  
        definedGrouping(globalContext.createDefinedGrouping());
    }

    var hasChanges = ko.computed(function () {
        return globalContext.hasChanges();
    });

    var cancel = function () {
        globalContext.cancelChanges();
        router.navigate('#/definedgroupings/');
    };

    var canSave = ko.computed(function () {
        return hasChanges() && !isSaving();
    });

    var save = function () {
        return globalContext.getUserName().then(userName => {
                definedGrouping().lastModifiedBy(userName);
                definedGrouping().lastModified(new Date());
            })
            .then(globalContext.saveChanges)
            .then(goToEditView)
            .fin(complete);

        function goToEditView(result) {
            router.navigate('#/definedgroupingsconfig/' + definedGrouping().id());
        }

        function complete() {
            isSaving(false);
        }
    };

    var vm = {
        activate: activate,
        title: 'New Defined Grouping',
        definedGrouping: definedGrouping,
        save: save,
        canSave: canSave,
        hasChanges: hasChanges,
        cancel: cancel
    };

    return vm;
});
