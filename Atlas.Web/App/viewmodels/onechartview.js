﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var system = require('durandal/system');
    var ctor = function (id) {

        this.charting = charting;

        this.oneChartId = id;
        this.oneChart = ko.observable();
        this.oneChartViewData = ko.observable();
        this.highChartOptions = ko.observable();
        this.percChangeLabel = ko.observable();
        this.yr2ColLabel = ko.observable("YEAR 2");
        this.formatNumber = function (num, numType) {
            //numType is0 for dollar, 1 for percent
            var ret = 0;
            if (num == null) {
                ret = "";
            } else if (numType == 0) {
                ret = Math.round((num / 1000));
                ret = numeral(ret).format("0,0");
            } else if (numType == 1) {
                ret = numeral(num).format("0.00%");
            } else if (numType == 2) {
                ret = numeral(num).format("0.0") + "%";
            }
            return ret;
        };
    }

    ctor.prototype.activate = function () {
        global.loadingReport(true);
        var self = this;

        //the order of these gets is important. In getOneChartViewDataById the override defaults get set. So after that, when we grab the Chart object all the correct overridable fields should be set
        return globalContext.getOneChartViewDataById(self.oneChartId, self.oneChartViewData).then(function () {
            return globalContext.getOneChartById(self.oneChartId, self.oneChart).then(function () {
                switch (self.oneChart().chart().percChangeComparator()) {
                    case "year2Year1":
                        self.percChangeLabel("Yr 2 % Change From Yr 1 " + self.oneChartViewData().compareScenName);
                        break;
                    case "year2Year2":
                        self.percChangeLabel("Yr 2 % Change From Yr 2 " + self.oneChartViewData().compareScenName);
                        break;
                    case "24Month":
                        self.yr2ColLabel("24 Month Cumulative");
                        self.percChangeLabel("24 Month Cumulative % Change From " + self.oneChartViewData().compareScenName);
                        break;
                }
                if (self.oneChartViewData().errors.length > 0 || self.oneChartViewData().warnings.length > 0) {
                    globalContext.reportErrors(self.oneChartViewData().errors, self.oneChartViewData().warnings);
                    return true;
                }
                return true;
            })
        })
            
    }



    ctor.prototype.compositionComplete = function () {

        var self = this;
        var groupEveryYear = 12;

        if (self.oneChart().chart().isQuarterly()) {
            groupEveryYear = 4;
        }

        var scenLookUp = {};
        var tblData = [];
        for (var i = 0; i < self.oneChart().chart().chartScenarioTypes().length; i++) {

            tblData[self.oneChart().chart().chartScenarioTypes()[i].priority()] = {
                name: self.oneChart().chart().chartScenarioTypes()[i].scenarioType().name(),
                years: []
            }

            scenLookUp[self.oneChart().chart().chartScenarioTypes()[i].scenarioType().name()] = self.oneChart().chart().chartScenarioTypes()[i].priority();

        }

        var series = [];
        var seriesLookUp = {};
        var minVal = 100000000;
        var maxVal = 0;




        for (var z = 0; z < tblData.length; z++) {


            console.log(tblData[z]);

            var scenProper = charting.scenarioProperName(tblData[z].name);
            var base = false;
            if (tblData[z].name.toUpperCase() == 'BASE') {
                base = true;
            }

          
            if (tblData[z].name.toUpperCase() == 'BASE') {
                base = true;
                marker: { };
            }

            series.push({
                name: scenProper,
                type: (base ? 'column' : 'line'),
                data: [],
                pointStart: 0,
                pointPadding: -.2,
                color: charting.scenarioIdLookUp(scenProper).seriesColor,
                marker: charting.scenarioIdLookUp(scenProper).marker,
                zIndex: (base ? 0 : 1)
            });
            seriesLookUp[tblData[z].name] = z;
        }

        for (var scen in self.oneChartViewData().chartData) {
            var scenProper = charting.scenarioProperName(scen);

            console.log("")

            var monthCounter = 0;
            var year = 1;
            var total = 0;
            for (var i = 0; i < self.oneChartViewData().chartData[scen].length; i++) {
                monthCounter += 1;
                total += self.oneChartViewData().chartData[scen][parseFloat(i)];
                series[seriesLookUp[scenProper]].data.push({ y: self.oneChartViewData().chartData[scen][parseFloat(i)].value, date: self.oneChartViewData().chartData[scen][parseFloat(i)].month });

                if (minVal > series[seriesLookUp[scenProper]].data[series[seriesLookUp[scenProper]].data.length - 1].y) {
                    minVal = series[seriesLookUp[scenProper]].data[series[seriesLookUp[scenProper]].data.length - 1].y;
                }

                if (maxVal < series[seriesLookUp[scenProper]].data[series[seriesLookUp[scenProper]].data.length - 1].y) {
                    maxVal = series[seriesLookUp[scenProper]].data[series[seriesLookUp[scenProper]].data.length - 1].y;
                }

                if (monthCounter == groupEveryYear || i == self.oneChartViewData().chartData[scen].length - 1) {
                    tblData[scenLookUp[scenProper]].years.push(total);
                    total = 0;
                    monthCounter = 0;
                    year += 1;
                }

            }

        }


        series = charting.scenarioIconStagger(series);
        console.log(series);

        //log the percent change data for now until the UI work is doneg
        console.table(self.oneChartViewData().percentChangeData);

        var units = charting.simCompareChartMinMax(minVal, maxVal);

        $('#oneChart').highcharts(charting.simulationCompareChart(self.oneChart().chart().name(), self.oneChart().chart().isQuarterly(), series, units));

        //Write Table Headers
        var headerRow = '<tr><th></th>';
        for (var z = 0; z < tblData.length; z++) {
            headerRow += '<th>' + tblData[z].name + '</th>';
        }
        headerRow += '</tr>';
        //Add To The Three Tables
        //$('#oneChartTable thead').append(headerRow);

        //Loop Through now and add row for each scenario
        for (var x = 0; x < tblData[0].years.length; x++) {
            var leftRow = '<tr><td>Year ' + (x + 1).toString() + '</td>';
            for (var z = 0; z < tblData.length; z++) {
                leftRow += '<td>' + numeral(tblData[z].years[x] / 1000).format() + '</td>';
            }
            leftRow += '</tr>';
            //$('#oneChartTable tbody').append(leftRow);
        }

        //$("#onechart-view").height($("#onechart-view").height());
        global.loadingReport(false);
        $('#reportLoaded').text('1');
        //Export To Pdf
        if (typeof hiqPdfInfo == "undefined") {
        }
        else {
            setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);
        }

    };


    return ctor;
});
