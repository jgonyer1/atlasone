﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var tdVm = function (id) {
        var self = this;
        this.td = ko.observable();

        this.compositionComplete = function () {


            // Set our headers that are super dynamc becuase atlas one is super dynamic therefore headers must be super dynamic!

            //Current date and prior date
            $('#currentDate').html('<span class="greyBubble">' + moment(self.td().asOfDate).format("MMM YY") + '</span>');
            $('#priorDate').html('<span class="greyBubble">' + moment(self.td().priorDate).format("MMM YY") + '</span>');

            //now need to do some date math to get correct month for project maturing
            $('#matProjecting').html('<span class="greyBubble">Maturity Projected <br/>' + moment(self.td().priorDate).add(1, 'months').format('MMM YY') + '-' + moment(self.td().asOfDate).format("MMM YY") + "</span>");

            //Need to do more match for cds maturing
            $('#cdMaturing').html('<span class="greyBubble">CDs Maturing <br/>' + moment(self.td().cdMatStartDate).format('MMM YY') + '-' + moment(self.td().cdMatEndDate).format("MMM YY") + "</span>");

            if (self.td().viewBy == "1") {
                //if sub accounts this is just the dump of the data

                $('#migTable').removeClass('dcg-table');
                $('#migTable').addClass('report-std-table');

                var migTable = document.getElementById("migTable");
                var migBody = "<tbody>";

                //Loop through the accounts and just dump them out for now

                for (var z = 0; z < self.td().table.length; z++) {
                    var rec = self.td().table[z];
                    //set this so we can use same report row generator
                    rec.isTotal = false;

                    migBody += this.buildTableRow(rec, false, true);
                }


                migBody += '</tbody>';

                migTable.innerHTML += migBody;


            }
            else {


                //if not sub accounts completly different layout, this one is much fancier and has prettier colors!
                var migTable = document.getElementById("migTable");
                var migBody = "<tbody>";
                var hashTable = {};

                for (var z = 0; z < self.td().table.length; z++) {
                    var rec = self.td().table[z];

                    //if first time seeing grouping set it up!
                    if (hashTable[rec.groupingName] == undefined) {
                        hashTable[rec.groupingName] = {
                            name: rec.groupingName,
                            termBuckets: [],
                            visible: false,
                            isTotal: false
                        }
                    }

                    //Push ont erm bucket and values
                    hashTable[rec.groupingName].termBuckets.push({
                        name: rec.termBucketName,
                        isTotal: false,
                        priorBal: numeral(rec.priorBal).value(),
                        priorRate: numeral(rec.priorRate).value(),
                        priorMatBal: numeral(rec.priorMatBal).value(),
                        priorMatRate: numeral(rec.priorMatRate).value(),
                        currentBal: numeral(rec.currentBal).value(),
                        currentRate: numeral(rec.currentRate).value(),
                        currentMatBal: numeral(rec.currentMatBal).value(),
                        currentMatRate: numeral(rec.currentMatRate).value()
                    });

                    //if any one of these is true means we show the grouping
                    if (numeral(rec.priorBal).value() != 0 || numeral(rec.currentBal).value() != 0) {
                        hashTable[rec.groupingName].visible = true;
                    }
                }

                //Now that has table is build loop through and properly fix other labels if we decide to as well as add total row

                //Push on grand total for hashtable
                hashTable['Total'] = {
                    name: 'Total',
                    visible: true,
                    isTotal: true,
                    priorBal: 0,
                    priorRate: 0,
                    priorMatBal: 0,
                    priorMatRate: 0,
                    currentBal: 0,
                    currentRate: 0,
                    currentMatBal: 0,
                    currentMatRate: 0
                }

                for (var g in hashTable) {
                  
                    //if grand total do not do this shit
                    if (hashTable[g].isTotal) {
                        continue;
                    }

                    //Push on total Row

                    hashTable[g].termBuckets.push({
                        name: 'Total',
                        isTotal: true,
                        priorBal: 0,
                        priorRate: 0,
                        priorMatBal: 0,
                        priorMatRate: 0,
                        currentBal: 0,
                        currentRate: 0,
                        currentMatBal: 0,
                        currentMatRate: 0

                    });

                    //Just becuase i am lazy and want it in a var
                    var totalIdx = hashTable[g].termBuckets.length - 1;

                    //Loop through the term buckets and calculate totals
                    for (var c = 0; c < hashTable[g].termBuckets.length - 1; c++) {

                        //prior totaling
                        hashTable[g].termBuckets[totalIdx].priorBal += hashTable[g].termBuckets[c].priorBal;
                        hashTable[g].termBuckets[totalIdx].priorRate += hashTable[g].termBuckets[c].priorBal * hashTable[g].termBuckets[c].priorRate;

                        //currentTotaling
                        hashTable[g].termBuckets[totalIdx].currentBal += hashTable[g].termBuckets[c].currentBal;
                        hashTable[g].termBuckets[totalIdx].currentRate += hashTable[g].termBuckets[c].currentBal * hashTable[g].termBuckets[c].currentRate;

                        //priorMat totaling
                        hashTable[g].termBuckets[totalIdx].priorMatBal += hashTable[g].termBuckets[c].priorMatBal;
                        hashTable[g].termBuckets[totalIdx].priorMatRate += hashTable[g].termBuckets[c].priorMatBal * hashTable[g].termBuckets[c].priorMatRate;

                        //currentMat totaling
                        hashTable[g].termBuckets[totalIdx].currentMatBal += hashTable[g].termBuckets[c].currentMatBal;
                        hashTable[g].termBuckets[totalIdx].currentMatRate += hashTable[g].termBuckets[c].currentMatBal * hashTable[g].termBuckets[c].currentMatRate;

                        //Do it all now for the grand total
                        hashTable['Total'].priorBal += hashTable[g].termBuckets[c].priorBal;
                        hashTable['Total'].priorRate += hashTable[g].termBuckets[c].priorBal * hashTable[g].termBuckets[c].priorRate;

                        //currentTotaling
                        hashTable['Total'].currentBal += hashTable[g].termBuckets[c].currentBal;
                        hashTable['Total'].currentRate += hashTable[g].termBuckets[c].currentBal * hashTable[g].termBuckets[c].currentRate;

                        //priorMat totaling
                        hashTable['Total'].priorMatBal += hashTable[g].termBuckets[c].priorMatBal;
                        hashTable['Total'].priorMatRate += hashTable[g].termBuckets[c].priorMatBal * hashTable[g].termBuckets[c].priorMatRate;

                        //currentMat totaling
                        hashTable['Total'].currentMatBal += hashTable[g].termBuckets[c].currentMatBal;
                        hashTable['Total'].currentMatRate += hashTable[g].termBuckets[c].currentMatBal * hashTable[g].termBuckets[c].currentMatRate;

                    }

                    //Now that I am done doing the total calculate the WAVG rates
                    hashTable[g].termBuckets[totalIdx].priorRate = hashTable[g].termBuckets[totalIdx].priorRate / hashTable[g].termBuckets[totalIdx].priorBal;
                    hashTable[g].termBuckets[totalIdx].currentRate = hashTable[g].termBuckets[totalIdx].currentRate / hashTable[g].termBuckets[totalIdx].currentBal;
                    hashTable[g].termBuckets[totalIdx].priorMatRate = hashTable[g].termBuckets[totalIdx].priorMatRate / hashTable[g].termBuckets[totalIdx].priorMatBal;
                    hashTable[g].termBuckets[totalIdx].currentMatRate = hashTable[g].termBuckets[totalIdx].currentMatRate / hashTable[g].termBuckets[totalIdx].currentMatBal;



                }

                //WAVG Grand Total
                hashTable['Total'].priorRate = hashTable['Total'].priorRate / hashTable['Total'].priorBal;
                hashTable['Total'].currentRate = hashTable['Total'].currentRate / hashTable['Total'].currentBal;
                hashTable['Total'].priorMatRate = hashTable['Total'].priorMatRate / hashTable['Total'].priorMatBal;
                hashTable['Total'].currentMatRate = hashTable['Total'].currentMatRate / hashTable['Total'].currentMatBal;



                //Now that totaling is done lets loop through and dump output

                for (var g in hashTable) {
                    if (hashTable[g].isTotal || !hashTable[g].visible) {
                        continue;
                    }

                    //First spit out header row for classificaiton/grouping
                    migBody += '<tr><td colspan="1" class="breakoutCell">' + hashTable[g].name + '</td>';
                    migBody += '<td class="spacerCell"></td>'
                    migBody += '<td colspan="4" class="shadeGreen"></td>'
                    migBody += '<td colspan="2" class="shadeBlue"></td>'
                    migBody += '<td class="spacerCell"></td>'
                    migBody += '<td colspan="2" class="shadeGreen"></td>'
                    migBody += '<td colspan="1" class="shadeBlue"></td>';
                    migBody += '<td colspan="1" class="shadeGreen"></td>';
                    migBody += '<td class="spacerCell"></td>'
                    migBody += '<td colspan="2" class="shadeYellow"></td>'
                    migBody+= '</tr > ';
                    console.log('adding riow');
                    //Now loop through term buckets and dump out data
                    for (var c = 0; c < hashTable[g].termBuckets.length; c++) {
                        migBody += this.buildTableRow(hashTable[g].termBuckets[c], false, false);
                    }
                }

                migBody += this.buildTableRow(hashTable['Total'], true, false);

                migBody += '</tbody>';

                migTable.innerHTML += migBody;
            }
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }


        }

        this.buildTableRow = function (obj, superTotal, subAccounts) {
            var row = "";

            var totClass = '';
            if (obj.isTotal) {
                totClass = 'italicRow';
            }
            if (superTotal) {
                totClass = 'grandTotal';
            }

            if (subAccounts) {
                switch (obj.cat_Type) {
                    case 7:
                        totClass = 'total-row-higher';
                        break;
                    case 6:
                        totClass = 'total-row-grand';
                        break;
                    case 5:
                        totClass = 'total-row-master';
                        break;
                    case 4:
                        totClass = 'total-row-sub';
                        break;
                    case 3:
                        totClass = 'total-row-int3';
                        break;
                    case 2:
                        totClass = 'total-row-int2';
                        break;
                    case 1:
                        totClass = 'total-row-int1';
                        break;
                } 

            }

            //DO not process if both are zeros and on sub accounts or id the total deposits number 4 cat type
            if ((subAccounts && numeral(obj.priorBal).value() == 0 && numeral(obj.currentBal).value() == 0) || (obj.name == 'Other' && numeral(obj.priorBal).value() == 0 && numeral(obj.currentBal).value() == 0) || (obj.cat_Type == 4)) {
                return "";
            }


            if (obj.name == 'Total Time Deposits') {
                obj.name = 'Total';
            }

            row += '<tr class="' + totClass + '">';
            row += '<td>' + obj.name + '</td>';
            row += '<td class="spacerCell"></td>'
            row += '<td class="shadeGreen">' + numeral(obj.currentBal / 1000).format('0,0') + '</td>';
            row += '<td class="shadeGreen">' + numeral(obj.currentRate).format('0.00') + '</td>';
            row += '<td class="shadeGreen">' + numeral(obj.priorBal / 1000).format('0,0') + '</td>';
            row += '<td class="shadeGreen">' + numeral(obj.priorRate).format('0.00') + '</td>';
            var moneyChange = (obj.currentBal - obj.priorBal);
            row += '<td class="shadeBlue">' + numeral((moneyChange) / 1000).format('0,0') + '</td>';
            row += '<td class="shadeBlue">' + numeral((obj.currentRate - obj.priorRate)).format('0.00') + '</td>';

            row += '<td class="spacerCell"></td>';

            row += '<td class="shadeGreen">' + numeral(obj.priorMatBal / 1000).format('0,0') + '</td>';
            row += '<td class="shadeGreen">' + numeral(obj.priorMatRate).format('0.00') + '</td>';

            var runOff = 0;
            if (moneyChange < 0 && obj.priorMatBal != 0) {
                runOff = (moneyChange / obj.priorMatBal) * -1;
            }


            if (obj.currentBal == 0 && obj.priorBal != 0) {
                runOff = 1;
            }
            else if (obj.currentBal != 0 && obj.priorBal == 0) {
                runOff = 0;
            }

            if (runOff < 0) {
                runOff = 0;
            }

            if (runOff > 1) {
                runOff = 1;
            }

            var retained = 1 - runOff;

            if (obj.currentBal == 0 && obj.priorBal == 0) {
                row += '<td class="shadeBlue">' + numeral(0).format('0%') + '</td>';
                row += '<td class="shadeGreen">' + numeral(0).format('0%') + '</td>';
            }
            else {
                row += '<td class="shadeBlue">' + numeral(retained).format('0%') + '</td>';
                row += '<td class="shadeGreen">' + numeral(runOff).format('0%') + '</td>';
            }

            row += '<td class="spacerCell"></td>';


            row += '<td class="shadeYellow">' + numeral(obj.currentMatBal / 1000).format('0,0') + '</td>';
            row += '<td class="shadeYellow">' + numeral(obj.currentMatRate).format('0.00') + '</td>';

            row += '</tr>';


            if (subAccounts) {
                switch (obj.cat_Type) {
                    case 7:
                    case 2:
                    case 3:
                    case 4:
                        row += '<tr class="spacer"><td colspan="1"></td>';
                        row += '<td class="spacerCell"></td>';
                        row += '<td colspan="4" class="shadeGreen"></td>';
                        row += '<td colspan="2" class="shadeBlue"></td>';
                        row += '<td class="spacerCell"></td>';
                        row += '<td colspan="2" class="shadeGreen"></td>';
                        row += '<td colspan="1" class="shadeBlue"></td>';
                        row += '<td colspan="1" class="shadeGreen"></td>';
                        row += '<td class="spacerCell"></td>';
                        row += '<td colspan="2" class="shadeYellow"></td>';
                        row += '</tr > ';
                        break;
                   default:
                   break;
                } 

            }

            return row;
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getTimeDepositMigrationViewDataById(id, self.td).then(function () {
              //  globalContext.reportErrors(self.td().errors, self.td().warnings);
            });
        };
    };

    return tdVm;

});
