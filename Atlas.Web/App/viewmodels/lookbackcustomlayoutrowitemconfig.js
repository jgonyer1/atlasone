﻿define([
    'services/globalcontext',
    'durandal/app'],
    function (globalContext, app) {

        var rowItemVm = function (parentCollection, rowItem, allClassifications) {
            var self = this;
            var curPage = "LookbackCustomLayoutRowItemConfig";
            globalContext.logEvent("Viewed", curPage); 
            var tempArr = [];
            this._rowItem = rowItem;
            this.availableClassifications = ko.computed(function () {
                var classificationIdsInUse = self._rowItem.lBCustomLayoutClassifications().map(function (classification) {
                    return classification.acc_Type() + "_" + classification.rbcTier() + "_"+classification.isAsset()
                });

                var result = [];
                var accTypeName = "";
                allClassifications().forEach(function (c) {
                    var inUseElseWhere = false;
                    parentCollection().forEach(function (rowI) {
                        rowI.lBCustomLayoutClassifications().forEach(function (parentC) {
                            if (rowI.id() != self._rowItem.id() && parentC.acc_Type() == c.accountTypeId() && parentC.rbcTier() == c.rbcTier() && parentC.isAsset() == c.isAsset())
                                inUseElseWhere = true;
                        });
                    });
                    result.push({
                        id: c.accountTypeId() + '_' + c.rbcTier() + '_' + c.isAsset,
                        name: c.name(),
                        inUse: ko.observable(classificationIdsInUse.indexOf(c.accountTypeId() + '_' + c.rbcTier() + '_' + c.isAsset()) != -1),
                        inUseElseWhere: inUseElseWhere,
                        cssClass: '',
                        isAsset: c.isAsset()
                    });
                });
                return result;
            });

            //classification manipulation
            this.changeClassificationStatus = function (classification, parentContext) {
                if (!classification.inUseElseWhere) {
                    if (!classification.inUse()) {
                        addClassification(classification, parentContext);
                    } else {
                        removeClassification(classification, parentContext);
                    }
                }
            }

            var addClassification = function (classification, parentContext) {
                var newClassification = globalContext.createLBCustomClassification(classification);
                self._rowItem.lBCustomLayoutClassifications().push(newClassification);
                //layoutVm.save();

            };
            var removeClassification = function (classification, parentContext) {
                var thisClassification;
                self._rowItem.lBCustomLayoutClassifications().forEach(function (customClass) {
                    if (customClass.acc_Type() == parseInt(classification.id.split('_')[0]) && customClass.rbcTier() == parseInt(classification.id.split('_')[1])) {
                        thisClassification = customClass;
                    }
                });
                if(thisClassification)
                    thisClassification.entityAspect.setDeleted();
            };
        };

        return rowItemVm;
    })