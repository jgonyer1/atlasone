﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var liqVm = function (liq, rep) {
        var self = this;
        var curPage = "LiquidityProjectionConfig";
        this.liq = liq;

        this.thisReport = rep;

        function onChangesCanceled() {
            self.sortHistoricalDateOffSets();
        }

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.reportTypes = ko.observableArray(['90 Day', '180 Day']);

        this.basicSurplusAdmins = ko.observableArray();
        this.basicSurplusAdminsOffset = ko.observableArray();
        this.sortedHistoricalOffSets = ko.observableArray();
        self.definedGroupings = ko.observableArray();
        this.basicSurplusAdminOffsets = ko.observableArray();
        this.policyOffsets = ko.observableArray();
        this.subs = [];

        this.definedGroupings = ko.observable();
        this.projectionOptions = ko.observableArray([{title: "90 Day", value: "90Day"}, {title: "180 Day", value: "180Day"}]);
        this.viewOptions = ko.observableArray();
        this.compositionComplete = function () {
            console.log('calluing this one');
            if (self.liq().id() > -1) {
                globalContext.saveChangesQuiet();
            } else {
                if (self.basicSurplusAdminOffsets().length == 0) {
                    toastr.warning("No prior Basic Surplus found. Please create one before proceeding.", "Warning", { timeOut: 1500, extendedTimeOut: 1500 });
                }
            }
        };


        this.sortHistoricalDateOffSets = function () {
            var arr = self.liq().liquidityProjectionDateOffsets().sort(function (a, b) { return a.priority() - b.priority(); });
            self.sortedHistoricalOffSets(arr);
        };

        this.removeHistoricalDateOffSet = function (offSet) {

            var msg = 'Delete offset "' + offSet.curOffSet() + '" - '+ offSet.prevOffSet() +' ?';
            var title = 'Confirm Delete';
            if (self.liq().liquidityProjectionDateOffsets().length == 1) {
                msg = "You must have at least one offset in this report."
                return app.showMessage(msg, "Alert", ["OK"]);
            } else {
                return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        self._deleteOffSet(offSet);
                    }
                }
            }
        };
        this._deleteOffSet = function (offSet) {
            offSet.entityAspect.setDeleted();
            self.liq().liquidityProjectionDateOffsets().forEach(function (anEveScenarioType, index) {
                anEveScenarioType.priority(index);
            });
            self.sortHistoricalDateOffSets();
        };

        this.addHistoricalDateOffSet = function () {
            var newCoreFundOffSet = globalContext.createLiquidityProjectionDateOffset();

            newCoreFundOffSet.curOffSet(self.liq().liquidityProjectionDateOffsets().length);
            newCoreFundOffSet.prevOffSet(self.liq().liquidityProjectionDateOffsets().length + 1);
            newCoreFundOffSet.priority(self.liq().liquidityProjectionDateOffsets().length);
            newCoreFundOffSet.curNIISimulationTypeId(1);
            newCoreFundOffSet.prevNIISimulationTypeId(1);


            newCoreFundOffSet.curSimulationTypes = ko.observableArray();
            newCoreFundOffSet.curScenarioTypes = ko.observableArray();
            newCoreFundOffSet.prevSimulationTypes = ko.observableArray();
            newCoreFundOffSet.prevScenarioTypes = ko.observableArray();


            globalContext.getAvailableSimulations(newCoreFundOffSet.curSimulationTypes, self.liq().institutionDatabaseName(), newCoreFundOffSet.curOffSet()).then(function () {
                globalContext.getAvailableSimulationScenarios(newCoreFundOffSet.curScenarioTypes, self.liq().institutionDatabaseName(), newCoreFundOffSet.curOffSet(), newCoreFundOffSet.curNIISimulationTypeId(), false).then(function () {
                    globalContext.getAvailableSimulations(newCoreFundOffSet.prevSimulationTypes, self.liq().institutionDatabaseName(), newCoreFundOffSet.prevOffSet()).then(function () {
                        globalContext.getAvailableSimulationScenarios(newCoreFundOffSet.prevScenarioTypes, self.liq().institutionDatabaseName(), newCoreFundOffSet.prevOffSet(), newCoreFundOffSet.prevNIISimulationTypeId(), false).then(function () {
                            //just give it the first scenario id, or 1 which should be base scenario
                            if (newCoreFundOffSet.curScenarioTypes()[0] != undefined) {
                                newCoreFundOffSet.curScenarioTypeId(self.basicSurplusAdminOffsets()[self.asOfDateOffsets()[newCoreFundOffSet.curOffSet()]["asOfDateDescript"]]);
                            } else {
                                newCoreFundOffSet.curScenarioTypeId("1");
                               
                            }

                            if (newCoreFundOffSet.prevScenarioTypes()[0] != undefined) {
                                newCoreFundOffSet.prevScenarioTypeId(self.basicSurplusAdminOffsets()[self.asOfDateOffsets()[newCoreFundOffSet.prevOffSet()]["asOfDateDescript"]]);
                            } else {
                                newCoreFundOffSet.prevScenarioTypeId("1");

                            }

                            try {
                                newCoreFundOffSet.curOffsetSub = newCoreFundOffSet.curOffSet.subscribe(function (newValue) {
                                    self.refreshItemSim(this);
                                }, newCoreFundOffSet);

                                newCoreFundOffSet.prevOffsetSub = newCoreFundOffSet.prevOffSet.subscribe(function (newValue) {
                                    self.refreshPrevItemSim(this);
                                }, newCoreFundOffSet);


                               newCoreFundOffSet.overrideSub = newCoreFundOffSet.override.subscribe(function (newValue) {
                                    self.refreshItemSim(newCoreFundOffSet);
                                    self.refreshPrevItemSim(newCoreFundOffSet);
                                }, newCoreFundOffSet);

                                newCoreFundOffSet.curSim = newCoreFundOffSet.curNIISimulationTypeId.subscribe(function (newValue) {
                                    self.refreshItemScen(newCoreFundOffSet);

                               }, newCoreFundOffSet);

                                newCoreFundOffSet.prevSimulation = newCoreFundOffSet.prevNIISimulationTypeId.subscribe(function (newValue) {
                                    self.refreshPrevItemScen(newCoreFundOffSet);

                                }, newCoreFundOffSet);
                                
                            }
                            catch (err) {
                                alert(err);
                            }


                            self.liq().liquidityProjectionDateOffsets().push(newCoreFundOffSet);

                            self.updateOffsetFields(newCoreFundOffSet);
                            self.sortHistoricalDateOffSets();

                        });
                    });
                });
            });



        };

        this.offsetChange = function () {
            self.updateOffsetFields(this);
        }

        this.updateOffsetFields = function (offSet) {

            if (self.policyOffsets()[0][self.asOfDateOffsets()[offSet.curOffSet()]['asOfDateDescript']] == undefined) {
                self._deleteOffSet(offSet);
                app.showMessage("Cannot find Policies for offset " + self.asOfDateOffsets()[offSet.curOffSet()]['asOfDateDescript'] + ". Failed to load offset.", "Error", ["OK"]);
            } else if (self.policyOffsets()[0][self.asOfDateOffsets()[offSet.prevOffSet()]['asOfDateDescript']] == undefined) {
                self._deleteOffSet(offSet);
                app.showMessage("Cannot find Policies for offset " + self.asOfDateOffsets()[offSet.prevOffSet()]['asOfDateDescript'] + ". Failed to load offset.", "Error", ["OK"]);
            } else if(!offSet.override()) {
                offSet.curNIISimulationTypeId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.curOffSet()]['asOfDateDescript']][0]["niiId"]));
                offSet.prevNIISimulationTypeId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.prevOffSet()]['asOfDateDescript']][0]["niiId"]));
                offSet.curBasicSurplusAdminId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.curOffSet()]['asOfDateDescript']][0]["bsId"]));
                offSet.prevBasicSurplusAdminId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.prevOffSet()]['asOfDateDescript']][0]["bsId"]));
            }
        }

        var overrideFunction = function (newValue) {
            if (!newValue) {
                self.updateOffsetFields(this);
            }
        };



        this.refreshItemScen = function (item) {
            globalContext.getAvailableSimulationScenarios(item.curScenarioTypes, self.liq().institutionDatabaseName(), item.curOffSet(), item.curNIISimulationTypeId(), false)
        }

        this.refreshItemSim = function (item) {
            globalContext.getAvailableSimulations(item.curSimulationTypes, self.liq().institutionDatabaseName(), item.curOffSet()).then(function () {
                self.refreshItemScen(item);
            });
        }

        this.refreshPrevItemScen = function (item) {
            globalContext.getAvailableSimulationScenarios(item.prevScenarioTypes, self.liq().institutionDatabaseName(), item.prevOffSet(), item.prevNIISimulationTypeId(), false)
        }

        this.refreshPrevItemSim = function (item) {
            globalContext.getAvailableSimulations(item.prevSimulationTypes, self.liq().institutionDatabaseName(), item.prevOffSet()).then(function () {
                self.refreshPrevItemScen(item);
            });
        }

        this.loadOffsetSimAndScen = function () {
            var offsetsProcessed = 0
            self.liq().liquidityProjectionDateOffsets().forEach(function (item, index, arr) {
                item.curSimulationTypes = ko.observableArray();
                item.curScenarioTypes = ko.observableArray();
                item.prevSimulationTypes = ko.observableArray();
                item.prevScenarioTypes = ko.observableArray();

                globalContext.getAvailableSimulations(item.curSimulationTypes, self.liq().institutionDatabaseName(), item.curOffSet()).then(function () {
                    globalContext.getAvailableSimulationScenarios(item.curScenarioTypes, self.liq().institutionDatabaseName(), item.curOffSet(), item.curNIISimulationTypeId(), false).then(function () {
                        globalContext.getAvailableSimulations(item.prevSimulationTypes, self.liq().institutionDatabaseName(), item.prevOffSet()).then(function () {
                            globalContext.getAvailableSimulationScenarios(item.prevScenarioTypes, self.liq().institutionDatabaseName(), item.prevOffSet(), item.prevNIISimulationTypeId(), false).then(function () {

                                item.curOffsetSub = item.curOffSet.subscribe(function (newValue) {
                                    self.refreshItemSim(this);
                                }, item);

                                item.prevOffsetSub = item.prevOffSet.subscribe(function (newValue) {
                                    self.refreshPrevItemSim(this);
                                }, item);


                                item.overrideSub = item.override.subscribe(function (newValue) {
                                    self.refreshItemSim(item);
                                    self.refreshPrevItemSim(item);
                                }, item);

                                item.curSim = item.curNIISimulationTypeId.subscribe(function (newValue) {
                                    self.refreshItemScen(item);

                                }, item);

                                item.prevSim = item.prevNIISimulationTypeId.subscribe(function (newValue) {
                                    self.refreshPrevItemScen(item);

                                }, item);

                                offsetsProcessed++;
                                //when done this just move ona nd setorrest chagnes function
                                if (offsetsProcessed == arr.length) {
                                    //self.refreshScens().then(function () {
                                    self.sortHistoricalDateOffSets();
                                    for (var i = 0; i < self.liq().liquidityProjectionDateOffsets().length; i++) {
                                        self.updateOffsetFields(self.liq().liquidityProjectionDateOffsets()[i]);
                                    }
                                }
                            });
                        });
                    });
                });
            });
        };

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return Q.all([
                    globalContext.getAvailableBanks(self.institutions),
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.liq().institutionDatabaseName()),
                    globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, self.liq().institutionDatabaseName()),
                    globalContext.getPolicyOffsets(self.policyOffsets, self.liq().institutionDatabaseName()),
                    globalContext.configDefinedGroupings(self.definedGroupings)
            ]).then(function () {
                self.loadOffsetSimAndScen();
                self.sortHistoricalDateOffSets();
                if (self.liq().liquidityProjectionDateOffsets().length == 0) {
                    self.addHistoricalDateOffSet();
                }

                var arr = [];
                arr.push({
                    name: "Standard View", options: [
                        { name: "Account Type", id: "0" }
                    ]
                });
                arr.push({
                    name: "Defined Groupings", options: self.definedGroupings()
                });
                self.viewOptions(global.getGroupedSelectOptions(arr));

                self.liq().liquidityProjectionDateOffsets().forEach(function (offset) {
                    self.subs.push(offset.override.subscribe(overrideFunction, offset));
                });

                self.instSub = self.liq().institutionDatabaseName.subscribe(function (newValue) {
                    Q.all([
                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue),
                        globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, newValue),
                        globalContext.getPolicyOffsets(self.policyOffsets, self.liq().institutionDatabaseName())
                    ]).then(function () {
                        self.loadOffsetSimAndScen();
                    });
                });

                self.liq().liquidityProjectionDateOffsets().forEach(function (offset) {
                    self.updateOffsetFields(offset);
                });

                global.loadingReport(false);
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return liqVm;
});
