﻿define([
    'services/globalcontext'
],
    function (globalContext) {

        var htmlToImg = function () {

            this.cancel = function () {
            };
            this.htmlCode = ko.observable();
            var MAP = {
                '&': '&amp;',
                '<': '%3C',
                '>': '%3E',
                '"': '&quot;',
                "'": '%27',
                '=': '%3D',
                ' ': '%20',
                '/': '%2F'
            };

            function escapeHTML(s, forAttribute) {
                return s.replace(forAttribute ? /[&<>'"/]/g : /[&<>/]/g, function (c) {
                    return MAP[c];
                });
            }
            this.goToImage = function () {
                
                var url = "report/imageFromHTML";
                window.open(url, '_blank');
            };
        };

        htmlToImg.prototype.activate = function (id) {
        };

        return htmlToImg;

    });
