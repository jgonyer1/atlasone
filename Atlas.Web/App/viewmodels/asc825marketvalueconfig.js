﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var ascVm = function (asc) {
        var self = this;

        this.asc = asc;

        function onChangesCanceled() {
            self.sortScenarioTypes();
        }

        this.cancel = function () {
        };

        //Scenario Block For Sorting, Adding, and Deleting From Scenario List
        this.sortedScenarioTypes = ko.observableArray(self.asc().eveScenarioTypes());

        this.sortScenarioTypes = function () {
            self.sortedScenarioTypes(self.asc().eveScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.asc().eveScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var newASCScenario = globalContext.createASC825MarketValueScenario();
            newASCScenario.scenarioTypeId(scenarioType.id);
            newASCScenario.priority(asc().eveScenarioTypes().length);
            asc().eveScenarioTypes().push(newASCScenario);
            self.sortScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };


        this.removeScenarioType = function (ascScenario) {

            var msg = 'Delete scenario "' + ascScenario.scenarioType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    ascScenario.entityAspect.setDeleted();
                    asc().eveScenarioTypes().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortScenarioTypes();

                }
            }

        };

        //self.sortScenarioTypes();

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.reportTypes = ko.observableArray([{ id: 1, name: 'Market Price' }, { id: 2, name: 'Market Value' }]);

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.institutions = ko.observableArray();


        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset(), self.asc().simulationTypeId(), true)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }


        this.activate = function (id) {
            globalContext.logEvent("Viewed", "ASC825marketvalueconfig");
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return globalContext.getEveScenarioTypes(self.scenarioTypes).then(function () {
                return globalContext.getSimulationTypes(self.simulationTypes).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        self.availableScenarioTypes = ko.computed(function () {
                            var scenarioTypeIdsInUse = self.asc().eveScenarioTypes().map(function (scenario) {
                                return scenario.scenarioTypeId();
                            });
                            var result = [];
                            self.scenarioTypes().forEach(function (scenarioType) {
                                if (scenarioType.name != 'Base') {
                                    result.push({
                                        id: scenarioType.id,
                                        name: scenarioType.name,
                                        inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id) != -1
                                    });
                                }

                            });
                            return result;
                        });

                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.asc().institutionDatabaseName()).then(function () {

                            //AsOfDateOffset Handling (Single-Institution Template)
                            self.instSub = self.asc().institutionDatabaseName.subscribe(function (newValue) {
                                globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                            });

                            self.aodSub = self.asc().asOfDateOffset.subscribe(function (newValue) {
                                self.refreshSim();
                            });

                            self.simSub = self.asc().simulationTypeId.subscribe(function (newValue) {
                                self.refreshScen();
                            });


                            global.loadingReport(false);
                        });
                    });
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.simSub.dispose();
        };

        return this;
    };

    return ascVm;
});
