﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id) {
        var self = this;
        var curPage = "EveAssumptionsView";
        this.eveViewData = ko.observable();

        this.compositionComplete = function () {

            $('#reportHolder').height($('#reportHolder').height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                $('#lastSection').css('min-height', '768px');
                $('#lastSection').addClass('lastSection');
                
                $('.last-prg').addClass('last-on-page');
                $('.first-h').addClass('first-on-page');
                

                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            return globalContext.getEveNevAssumptionsViewDataById(id, self.eveViewData).then(function () {

            });
        };
    };

    return ascVm;

});
