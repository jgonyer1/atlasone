﻿define(function (require) {
    var vm = {};

    var globalContext = require('services/globalcontext');
    var router = require('plugins/router');
    var system = require('durandal/system');
    var app = require('durandal/app');
    var logger = require('services/logger');
    var onechartconfig = require('viewmodels/onechartconfig');
    var twochartconfig = require('viewmodels/twochartconfig');
    var simcompareconfig = require('viewmodels/simcompareconfig');
    var eveconfig = require('viewmodels/eveconfig');
    var documentconfig = require('viewmodels/documentconfig');
    var BalanceSheetCompareConfig = require('viewmodels/balancesheetcompareconfig');
    var BalanceSheetMixConfig = require('viewmodels/balancesheetmixconfig');
    var basicsurplusreportconfig = require('viewmodels/basicsurplusreportconfig');
    var interestratepolicyguidelinesconfig = require('viewmodels/interestratepolicyguidelinesconfig');
    var loancapfloorconfig = require('viewmodels/loancapfloorconfig');
    var staticgapconfig = require('viewmodels/staticgapconfig');
    var fundingmatrixconfig = require('viewmodels/fundingmatrixconfig');
    var timedepositmigrationconfig = require('viewmodels/timedepositmigrationconfig');
    var corefundingutilizationconfig = require('viewmodels/corefundingutilizationconfig');
    var yieldcurveshiftconfig = require('viewmodels/yieldcurveshiftconfig');
    var historicalbalancesheetniiconfig = require('viewmodels/historicalbalancesheetniiconfig');
    var simulationsummaryconfig = require('viewmodels/simulationsummaryconfig');
    var coverpageconfig = require('viewmodels/coverpageconfig');
    var evenevassumptionsconfig = require('viewmodels/evenevassumptionsconfig');
    var cashflowreportconfig = require('viewmodels/cashflowreportconfig');
    var netcashflowconfig = require('viewmodels/netcashflowconfig');
    var summaryrateconfig = require('viewmodels/summaryrateconfig');
    var asc825balancesheetconfig = require('viewmodels/asc825balancesheetconfig');
    var prepaymentdetailsconfig = require('viewmodels/prepaymentdetailsconfig');
    var asc825datasourceconfig = require('viewmodels/asc825datasourceconfig');
    var asc825discountrateconfig = require('viewmodels/asc825discountrateconfig');
    var asc825worksheetconfig = require('viewmodels/asc825worksheetconfig');
    var asc825marketvalueconfig = require('viewmodels/asc825marketvalueconfig');
    var ratechangematrixconfig = require('viewmodels/ratechangematrixconfig');
    var investmentportfoliovaluationconfig = require('viewmodels/investmentportfoliovaluationconfig');
    var investmentdetailconfig = require('viewmodels/investmentdetailconfig');
    var investmenterrorconfig = require('viewmodels/investmenterrorconfig');
    var asc825assumptionmethodsconfig = require('viewmodels/asc825assumptionmethodsconfig');
    var assumptionmethodsconfig = require('viewmodels/assumptionmethodsconfig');
    var liabilitypricinganalysisconfig = require('viewmodels/liabilitypricinganalysisconfig');
    var liquidityprojectionconfig = require('viewmodels/liquidityprojectionconfig');
    var detailedsimulationassumptionconfig = require('viewmodels/detailedsimulationassumptionconfig');
    var niireconconfig = require('viewmodels/niireconconfig');
    var summaryofresultsconfig = require('viewmodels/summaryofresultsconfig');
    var capitalanalysisconfig = require('viewmodels/capitalanalysisconfig');
    var sectionpageconfig = require('viewmodels/sectionpageconfig');
    var lookbackreportconfig = require('viewmodels/lookbackreportconfig');
    var executiverisksummaryconfig = require('viewmodels/executiverisksummaryconfig');
    var marginalcostoffundsconfig = require('viewmodels/marginalcostoffundsconfig');
    var inventoryliquidityresourcesconfig = require('viewmodels/inventoryliquidityresourcesconfig');

    var model = require('services/model');
    var global = require('services/global');
    var naModal = require('viewmodels/navigateawaymodal');

    var thisReport = ko.observable();
    var thisReportConfigVm = ko.observable();
    var nextReport = ko.observable();
    var previousReport = ko.observable();
    var sortedFootnotes = ko.observableArray();
    var sortedLeftSplitFootnotes = ko.observableArray();
    var sortedRightSplitFootnotes = ko.observableArray();
    var isSaving = ko.observable(false);
    var isDeleting = ko.observable(false);
    var showName = ko.observable(true);
    var hasSplitFootnotes = ko.observable(false);
    var canAddFootnotes = ko.observable(true);
    var profile = ko.observable();
    var reportName = ko.observable();
    var curPage = "ReportConfig";
    var subs = [];

    function activate(id) {
        app.on('application:changeReportTitle').then(function () {
            thisReport().name(global.updatedTitle);
        });

        return globalContext.getUserProfile(profile).then(function () {
            return globalContext.getReportById(parseInt(id), thisReport).then(function () {
                subs.push(thisReport().staticDates.subscribe(function (newVal) {
                    globalContext.logEvent("Toggled report id " + thisReport().id() + " static dates to " + newVal, curPage);
                }));
                sortFootnotes();
                vm.defaultProt = ko.protectedObservable(thisReport().defaults(), thisReport().defaults);
                vm.bankName = profile().databaseName;
                return Q.all([
                   // globalContext.getPackageById(thisReport().packageId()),
                    globalContext.getNextReport(thisReport, nextReport),
                    globalContext.getPreviousReport(thisReport, previousReport),
                    getConfigVm(thisReport)
                ]);
            });
        });
    }


    function sortFootnotes() {
        var sorted = thisReport().footnotes().sort(function (a, b) {
            if (a.priority() === b.priority()) return 0;
            return a.priority() > b.priority() ? 1 : -1;
        });

        var leftSorted = thisReport().splitFootnotes().filter(function (item) { return item.side() == 0 }).sort(function (a, b) {
            if (a.priority() === b.priority()) return 0;
            return a.priority() > b.priority() ? 1 : -1;
        });
        var rightSorted = thisReport().splitFootnotes().filter(function (item) { return item.side() == 1 }).sort(function (a, b) {
            if (a.priority() === b.priority()) return 0;
            return a.priority() > b.priority() ? 1 : -1;
        });

        sortedFootnotes(sorted);

        sortedLeftSplitFootnotes(leftSorted);
        sortedRightSplitFootnotes(rightSorted);
    };


    function getConfigVm(report) {

        for (var i = 0; i < model.reportNames.length; i++) {
            if (model.reportNames[i].name == thisReport().shortName()) {
                reportName(model.reportNames[i].dispName);
                break;
            }
        }
        canAddFootnotes(true);
        hasSplitFootnotes(false);
        if (thisReport().shortName() === "OneChart") {
            var oneChart = ko.observable();
            return globalContext.getOneChartById(report().id(), oneChart).then(function () {
                thisReportConfigVm(new onechartconfig(oneChart));
            });
        }
        else if (thisReport().shortName() === "TwoChart") {
            var twoChart = ko.observable();
            return globalContext.getTwoChartById(thisReport().id(), twoChart).then(function () {
                thisReportConfigVm(new twochartconfig(twoChart));
                return;
            });
        }
        else if (thisReport().shortName() === "SimCompare") {
            var simCompare = ko.observable();
            return globalContext.getSimCompareById(thisReport().id(), simCompare).then(function () {
                thisReportConfigVm(new simcompareconfig(simCompare));
                return;
            });
        }
        else if (thisReport().shortName() === "Eve") {
            var eve = ko.observable();
            return globalContext.getEveById(thisReport().id(), eve).then(function () {
                thisReportConfigVm(new eveconfig(eve));
                return;
            });
        }
        else if (thisReport().shortName() === "Document") {
            var document = ko.observable();
            return globalContext.getDocumentById(thisReport().id(), document).then(function () {
                thisReportConfigVm(new documentconfig(document, vm, model));
                return;
            });
        }
        else if (thisReport().shortName() === "BalanceSheetCompare") {
            var balanceSheetCompare = ko.observable();
            return globalContext.getBalanceSheetCompareById(thisReport().id(), balanceSheetCompare).then(function () {
                thisReportConfigVm(new BalanceSheetCompareConfig(balanceSheetCompare));
                return;
            });
        }
        else if (thisReport().shortName() === "BalanceSheetMix") {
            var balanceSheetMix = ko.observable();
            return globalContext.getBalanceSheetMixById(thisReport().id(), balanceSheetMix).then(function () {
                thisReportConfigVm(new BalanceSheetMixConfig(balanceSheetMix));
                return;
            });
        }
        else if (thisReport().shortName() === "BasicSurplusReport") {
            var basicSurplusReport = ko.observable();
            return globalContext.getBasicSurplusReportById(thisReport().id(), basicSurplusReport).then(function () {
                thisReportConfigVm(new basicsurplusreportconfig(basicSurplusReport));
                return;
            });
        }
        else if (thisReport().shortName() === "InterestRatePolicyGuidelines") {
            var interestRatePolicyGuidelines = ko.observable();
            return globalContext.getInterestRatePolicyGuidelinesById(thisReport().id(), interestRatePolicyGuidelines).then(function () {
                thisReportConfigVm(new interestratepolicyguidelinesconfig(interestRatePolicyGuidelines));
                return;
            });
        }
        else if (thisReport().shortName() === "LoanCapFloor") {
            var lcf = ko.observable();
            showName(false);
            hasSplitFootnotes(true);
            canAddFootnotes(false);
            return globalContext.getLoanCapFloorById(thisReport().id(), lcf).then(function () {
                thisReportConfigVm(new loancapfloorconfig(lcf, sortedFootnotes, thisReport));
                return;
            });
        }
        else if (thisReport().shortName() === "StaticGap") {
            var staticGap = ko.observable();
            return globalContext.getStaticGapById(thisReport().id(), staticGap).then(function () {
                thisReportConfigVm(new staticgapconfig(staticGap));
                return;
            });
        }
        else if (thisReport().shortName() === "SectionPage") {
            var sectionPage = ko.observable();
            return globalContext.getSectionPageById(thisReport().id(), sectionPage).then(function () {
                thisReportConfigVm(new sectionpageconfig(sectionPage));
                return;
            });
        }
        else if (thisReport().shortName() === "FundingMatrix") {
            var fundingMatrix = ko.observable();
            return globalContext.getFundingMatrixById(thisReport().id(), fundingMatrix).then(function () {
                thisReportConfigVm(new fundingmatrixconfig(fundingMatrix));
                return;
            });
        }
        else if (thisReport().shortName() === "TimeDepositMigration") {
            var timeDep = ko.observable();
            return globalContext.getTimeDepositMigrationById(thisReport().id(), timeDep).then(function () {
                thisReportConfigVm(new timedepositmigrationconfig(timeDep));
                return;
            });
        }
        else if (thisReport().shortName() === "CoreFundingUtilization") {
            var coreFund = ko.observable();
            return globalContext.getCoreFundingUtilizationById(thisReport().id(), coreFund).then(function () {
                thisReportConfigVm(new corefundingutilizationconfig(coreFund));
                return;
            });
        }
        else if (thisReport().shortName() === "YieldCurveShift") {
            var yieldCurve = ko.observable();
            return globalContext.getYieldCurveShiftById(thisReport().id(), yieldCurve).then(function () {
                thisReportConfigVm(new yieldcurveshiftconfig(yieldCurve));
                return;
            });
        }
        else if (thisReport().shortName() === "HistoricalBalanceSheetNII") {
            var histBal = ko.observable();
            return globalContext.getHistoricalBalanceSheetNIIById(thisReport().id(), histBal).then(function () {
                thisReportConfigVm(new historicalbalancesheetniiconfig(histBal));
                return;
            });
        }
        else if (thisReport().shortName() === "SimulationSummary") {
            var simSum = ko.observable();
            return globalContext.getSimulationSummaryById(thisReport().id(), simSum).then(function () {
                thisReportConfigVm(new simulationsummaryconfig(simSum));
                return;
            });
        }
        else if (thisReport().shortName() === "CoverPage") {
            var cv = ko.observable();
            return globalContext.getCoverPageById(thisReport().id(), cv).then(function () {
                thisReportConfigVm(new coverpageconfig(cv));
                return;
            });
        }
        else if (thisReport().shortName() === "EveNevAssumptions") {
            var eve = ko.observable();
            return globalContext.getEveNevAssumptionsById(thisReport().id(), eve).then(function () {
                thisReportConfigVm(new evenevassumptionsconfig(eve));
                return;
            });
        }
        else if (thisReport().shortName() === "CashflowReport") {
            var cashflow = ko.observable();
            return globalContext.getCashflowReportById(thisReport().id(), cashflow).fin(function () {
                thisReportConfigVm(new cashflowreportconfig(cashflow));
                return;
            });
        }
        else if (thisReport().shortName() === "NetCashflow") {
            var cashflow = ko.observable();
            return globalContext.getNetCashflowById(thisReport().id(), cashflow).then(function () {
                thisReportConfigVm(new netcashflowconfig(cashflow));
                return;
            });
        }
        else if (thisReport().shortName() === "SummaryRate") {
            var sr = ko.observable();
            return globalContext.getSummaryRateById(thisReport().id(), sr).then(function () {
                thisReportConfigVm(new summaryrateconfig(sr));
                return;
            });
        }
        else if (thisReport().shortName() === "ASC825BalanceSheet") {
            var as = ko.observable();
            return globalContext.getASC825BalanceSheetById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new asc825balancesheetconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "PrepaymentDetails") {
            var pd = ko.observable();
            return globalContext.getPrepaymentDetailsById(thisReport().id(), pd).then(function () {
                var pdThing = pd();
                thisReportConfigVm(new prepaymentdetailsconfig(pd));
                return;
            });
        }
        else if (thisReport().shortName() === "ASC825DataSource") {
            var as = ko.observable();
            return globalContext.getASC825DataSourceById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new asc825datasourceconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "ASC825DiscountRate") {
            var as = ko.observable();
            return globalContext.getASC825DiscountRateById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new asc825discountrateconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "ASC825Worksheet") {
            var as = ko.observable();
            return globalContext.getASC825WorksheetById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new asc825worksheetconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "ASC825MarketValue") {
            var as = ko.observable();
            return globalContext.getASC825MarketValueById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new asc825marketvalueconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "RateChangeMatrix") {
            var as = ko.observable();
            return globalContext.getRateChangeMatrixById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new ratechangematrixconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "InvestmentPortfolioValuation") {
            var as = ko.observable();
            return globalContext.getInvestmentPortfolioValuationById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new investmentportfoliovaluationconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "InvestmentDetail") {
            var as = ko.observable();
            return globalContext.getInvestmentDetailById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new investmentdetailconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "InvestmentError") {
            var as = ko.observable();
            return globalContext.getInvestmentErrorById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new investmenterrorconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "ASC825AssumptionMethods") {
            var as = ko.observable();
            return globalContext.getASC825AssumptionMethodsById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new asc825assumptionmethodsconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "AssumptionMethods") {
            var as = ko.observable();
            return globalContext.getAssumptionMethodsById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new assumptionmethodsconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "LiabilityPricingAnalysis") {
            var as = ko.observable();
            return globalContext.getLiabilityPricingAnalysisById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new liabilitypricinganalysisconfig(as, thisReport));
                return;
            });
        }
        else if (thisReport().shortName() === "LiquidityProjection") {
            var as = ko.observable();
            return globalContext.getLiquidityProjectionById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new liquidityprojectionconfig(as, thisReport));
                return;
            });
        }
        else if (thisReport().shortName() === "DetailedSimulationAssumption") {
            var as = ko.observable();
            return globalContext.getDetailedSimulationAssumptionById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new detailedsimulationassumptionconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "NIIRecon") {
            var as = ko.observable();
            canAddFootnotes(false);
            return globalContext.getNIIReconById(thisReport().id(), as).then(function () {
                thisReportConfigVm(new niireconconfig(as));
                return;
            });
        }
        else if (thisReport().shortName() === "SummaryOfResults") {
            var sr = ko.observable();
            return globalContext.getSummaryOfResultsById(thisReport().id(), sr).then(function () {
                thisReportConfigVm(new summaryofresultsconfig(sr));
                return;
            });
        }
        else if (thisReport().shortName() === "CapitalAnalysis") {
            var sr = ko.observable();
            return globalContext.getCapitalAnalysisById(thisReport().id(), sr).then(function () {
                thisReportConfigVm(new capitalanalysisconfig(sr));
                return;
            });
        }
        else if (thisReport().shortName() === "LookbackReport") {
            var lb = ko.observable();
            return globalContext.getLookbackReportById(thisReport().id(), lb).then(function () {
                thisReportConfigVm(new lookbackreportconfig(lb));
                return;
            });
        }
        else if (thisReport().shortName() === "ExecutiveRiskSummary") {
            canAddFootnotes(true);
            var ers = ko.observable();
            return globalContext.getExecutiveRiskSummaryById(thisReport().id(), ers).then(function () {
                thisReportConfigVm(new executiverisksummaryconfig(ers));
                return;
            });
        }
        else if (thisReport().shortName() === "InventoryLiquidityResources") {
            canAddFootnotes(true);
            var ers = ko.observable();
            return globalContext.getInventoryLiquidityResourcesById(thisReport().id(), ers).then(function () {
                thisReportConfigVm(new inventoryliquidityresourcesconfig(ers));
                return;
            });
        }
        else if (thisReport().shortName() === "MarginalCostOfFunds") {
            var sr = ko.observable();
            return globalContext.getMarginalCostOfFundsById(thisReport().id(), sr).then(function () {
                thisReportConfigVm(new marginalcostoffundsconfig(sr));
                return;
            });
        }

    }

    this.deactivate = function (isClose) {
        app.off('application:changeReportTitle');
        thisReport().forEach(function (sub) {
            //TODO HACK subs should only be pushed ko.subscriptions, but sometimes numbers end up there!?
            if (sub.dispose)
                sub.dispose();
        });
        for (var s = 0; s < subs.length; s++) {
            subs[s].dispose();
        }

    };


    function addFootnote() {
        var unlimitedFootnotes = ["AssumptionMethods", "InventoryLiquidityResources"];
        if ((unlimitedFootnotes.indexOf(thisReport().shortName()) == -1 && thisReport().footnotes().length < 3) || unlimitedFootnotes.indexOf(thisReport().shortName()) > -1) {
            var newFootnote = globalContext.createFootnote(thisReport().id(), thisReport().footnotes().length);
            newFootnote.priority(thisReport().footnotes().length);
            newFootnote.report(thisReport());
            sortFootnotes();
            $("#footnote_tb_" + newFootnote.id()).focus();
        } else {
            var msg = 'You may only add three footnotes to this report.';
            var title = 'Footnote Limit Reached';
            app.showMessage(msg, title, ['Ok']);
        }
    }
    function addSplitFootnote(side) {//0 = left 1 = right

        var tempFootnoteArr = thisReport().splitFootnotes().filter(function (footnote) { return footnote.side() == side });
        if (tempFootnoteArr.length < 3) {
            var newSplitFootnote = globalContext.createSplitFootnote();
            newSplitFootnote.priority(tempFootnoteArr.length);
            newSplitFootnote.report(thisReport());
            newSplitFootnote.side(side);
            sortFootnotes();
        } else {
            var msg = 'You may only add three footnotes to this report.';
            var title = 'Footnote Limit Reached';
            app.showMessage(msg, title, ['Ok']);
        }
    }


    function removeFootnote(footnote) {

        var msg = 'Delete footnote "' + footnote.text() + '" ?';
        var title = 'Confirm Delete';
        return app.showMessage(msg, title, ['No', 'Yes'])
            .then(confirmDelete);

        function confirmDelete(selectedOption) {
            if (selectedOption === 'Yes') {
                footnote.entityAspect.setDeleted();
                if (!footnote.side) {
                    thisReport().footnotes().forEach(function (footnote, index) {
                        footnote.priority(index);
                    });
                } else {
                    thisReport().splitFootnotes().filter(function (item) { return item.side() == footnote.side()}).forEach(function (thisfootnote, index) {
                        thisfootnote.priority(index);
                    });
                }
                
                sortFootnotes();
            }
        }

    };

    function next() {
        router.navigate('#/reportconfig/' + nextReport().repId);
    }

    function prev() {
        router.navigate('#/reportconfig/' + previousReport().repId);
    }

    var goToView = function () {
        router.navigate("#/reportview/" + thisReport().id());
    };

    var goToPdf = function () {

     

        function pdfIt() {
            if (thisReport().shortName() == 'NIIRecon') {
                var url = "report/Excel/" + thisReport().id();
                var errors = ko.observable();
                //window.open(url, '_blank');
                globalContext.checkNiiSimulation(thisReport().id(), errors).then(function () {
                    if (errors().errors.length > 0 || errors().warnings.length > 0) {
                        globalContext.reportErrors(errors().errors, errors().warnings);
                    } else {
                        window.open(url, '_blank');
                    }
                });

            }
            else {

                //Do the cool modal type thing


                globalContext.startReportPdfProcess(thisReport().id());
                global.pdfInProgress(true);
                global.pdfType('report');
                global.pdfReportId(thisReport().id());
                global.pdfPackage(thisReport().packageId());
                app.trigger('application:pdfStart');


                //var url = "report/pdf/" + thisReport().id();
                //window.open(url, '_blank');


            }
        }


        if (!hasChanges()) {
            pdfIt();
            return true;
        }


        return naModal.show(
            function () {
                globalContext.cancelChanges();

                pdfIt();
            },
            function () {
                globalContext.saveChanges()
                    .then(pdfIt).fin(complete);
                function complete() {
                    isSaving(false);
                }
            }
        );

       

    };

    var goToImage = function () {
        if (false) {
            var url = "report/Excel/" + thisReport().id();
            window.open(url, '_blank');
        }
        else {
            var url = "report/image/" + thisReport().id();
            window.open(url, '_blank');
        }

    };

    var goToPackageConfig = function () {
        router.navigate("#/packageconfig/" + thisReport().packageId());
    };

    var hasChanges = ko.computed(function () {
        return globalContext.hasChanges();
    });

    var cancel = function () {
        globalContext.cancelChanges();
        globalContext.getFootnotesByReportId(thisReport().id()).then(sortFootnotes);
        thisReportConfigVm().cancel && thisReportConfigVm().cancel();
    };

    var resetDefaults = function (value) {
        if (!thisReport().defaults()) {
            var options = ["Cancel", "Ok"];
            app
                .showMessage("Are you sure you want to reset this configuration to template defaults? All work will be lost.", "Reset To Defaults", options)
                .then(function (r) {
                    if (r != options[1]) {
                        vm.defaultProt.reset();
                        return true;
                    }

                    $("#default_dialog").modal("show");
                    vm.defaultProt.commit();

                    if (thisReport().onResetChanges) {
                        thisReport().onResetChanges().then(continuation);
                    }
                    else {
                        continuation();
                    }

                    function continuation() {
                        globalContext.logEvent("Restoring report id " + thisReport().id(), curPage);
                        globalContext.saveChanges().then(function () {
                            globalContext.defaultReport(thisReport().id()).then(function () {
                                globalContext.clearCache();
                                activate(thisReport().id());
                                $("#default_dialog").modal("hide");
                            });
                        });
                    }
                });
        } else {
            vm.defaultProt.commit();
            globalContext.logEvent("Unlocking report id " + thisReport().id() + " from defaults", curPage);

            if (thisReport().onResetChanges) {
                thisReport().onResetChanges();
            }
        }
    };

    var canSave = ko.computed(function () {
        //console.log("REPORT CONFIG CAN SAVE: ");
        //console.log(globalContext.getChanges());
        return hasChanges() && !isSaving();
    });

    var save = function () {
        globalContext.saveChanges()
            .then(goToEditView).fin(complete);

        function goToEditView() {
            //router.replaceLocation('#/oneincomegraphdetail/' + oneIncomeGraph().id());
        }

        function complete() {
            isSaving(false);
        }
    };

    var saveThenGoToView = function () {
        globalContext.saveChanges()
            .then(goToView)
            .fin(complete);

        function complete() {
            isSaving(false);
        }
    };

    var canDeactivate = function () {
        if (!hasChanges()) return true;

        return naModal.show(
            function () { globalContext.cancelChanges(); },
            function () { save(); }
        );
    };
    
    var deleteReport = function () {
        var msg = 'Delete report "' + thisReport().name() + '" ?';
        var title = 'Confirm Delete';
        var packageId = thisReport().packageId();
        isDeleting(true);
        return app.showMessage(msg, title, ['No', 'Yes'])
            .then(confirmDelete);
   
        function confirmDelete(selectedOption) {
            if (selectedOption === 'Yes') {
                //globalContext.deleteReportById(thisReport().id()).fin(function () {
                //globalContext.deleteReportingItemById("Report", thisReport().id(), "Package").fin(function () {
                globalContext.deleteReport(thisReport().id()).fin(function () {
                    thisReport().entityAspect.setDetached(); //Set it to detached becuase of breeze cache since we deleted it on server
                    var reports = ko.observableArray();
                    var thePackage = ko.observable();
                    return globalContext.getPackageById(packageId, thePackage).fin(function () {
                        globalContext.getReportsByPackageId(packageId, reports)
                            .fin(function () {
                                var numberOfReports = thePackage().reports().length;
                                for (var i = 0; i < numberOfReports; i++) {
                                    var aReport = thePackage().reports()[i];
                                    aReport.priority(i);
                                }
                                globalContext.saveChanges()
                                    .then(success)
                                    .fail(failed)
                                    .fin(function () {
                                        router.navigate('#/packageconfig/' + packageId)
                                    });
                            });
                    });

                    function success() {
                        
                    }

                    function failed(error) {
                        cancel();
                        var errorMsg = 'Error: ' + error.message;
                        logger.logError(
                            errorMsg, error, system.getModuleId(vm), true);
                    }

                    function finish() {
                        return selectedOption;
                    }

                });

            }
            isDeleting(false);
        }

    };

    function fixupFootnotePriority() {
        if (thisReport() && thisReport().footnotes()) {
            thisReport().footnotes().forEach(function (footnote, index) {
                footnote.priority(index);
            });
        }
        if (thisReport() && thisReport().splitFootnotes()) {
            thisReport().splitFootnotes().filter(function (item) { return item.side() == 0 }).forEach(function (footnote, index) {
                footnote.priority(index);
            });
            thisReport().splitFootnotes().filter(function (item) { return item.side() == 1 }).forEach(function (footnote, index) {
                footnote.priority(index);
            });
        }

    };

    vm.title = 'Report Config';
    vm.thisReport = thisReport;
    vm.nextReport = nextReport;
    vm.previousReport = previousReport;
    vm.activate = activate;
    vm.next = next;
    vm.prev = prev;
    vm.goToView = goToView;
    vm.goToPdf = goToPdf;
    vm.goToImage = goToImage;
    vm.goToPackageConfig = goToPackageConfig;
    vm.save = save;
    vm.saveThenGoToView = saveThenGoToView;
    vm.canDeactivate = canDeactivate;
    vm.canSave = canSave;
    vm.canAddFootnotes = canAddFootnotes;
    vm.hasChanges = hasChanges;
    vm.hasSplitFootnotes = hasSplitFootnotes;
    vm.cancel = cancel;
    vm.showName = showName;
    vm.deleteReport = deleteReport;
    vm.thisConcreteReportVm = thisReportConfigVm;
    vm.addFootnote = addFootnote;
    vm.addSplitFootnote = addSplitFootnote;
    vm.removeFootnote = removeFootnote;
    vm.sortedFootnotes = sortedFootnotes;
    vm.sortedLeftSplitFootnotes = sortedLeftSplitFootnotes;
    vm.sortedRightSplitFootnotes = sortedRightSplitFootnotes;
    vm.fixupFootnotePriority = fixupFootnotePriority;
    vm.profile = profile;
    vm.resetDefaults = resetDefaults;
    vm.reportName = reportName;
    vm.global = global;

    return vm;
});
