﻿define(['services/globalcontext',
    'plugins/router',
    'durandal/system',
    'durandal/app',
    'services/logger',
    'services/model'
],


    function (globalContext, router, system, app, logger, model) {
        var thePackage = ko.observable();
        var reportNames = ko.observableArray(model.reportNames);
        var isSaving = ko.observable(false);
        var profile = ko.observable();
        var isTemplateBank = ko.observable();
        var atlasTemplates = ko.observable();
        var copyTemplate = ko.observable();
        var sourceTemplateId = ko.observable();
        var newPackageId = ko.observable();
        var curPage = "PackageAdd";

        copyTemplate(true);


        function activate() {
            return globalContext.getUserProfile(profile).then(function () {
                return globalContext.getAtlasTemplates(atlasTemplates).then(function () {
                    //Decides Wheter it should hide or show templates to copy from
                    globalContext.logEvent("Viewed", curPage);
                    if (profile().databaseName == 'DDW_Atlas_Templates') {
                        isTemplateBank(false);
                    }
                    else {
                        isTemplateBank(true);
                    }

                    if (atlasTemplates().length > 0) {
                        sourceTemplateId(atlasTemplates()[0].id);
                    }

                    thePackage(globalContext.createPackage());
                    thePackage().asOfDate(moment.utc(profile().asOfDate).format('YYYY-MM-DD'));
                    thePackage().modifiedBy(profile().userName.substring(0, profile().userName.indexOf('@')));

                });

            });
        }

        var hasChanges = ko.computed(function () {
            return globalContext.hasChanges();
        });

        var cancel = function () {
            globalContext.cancelChanges();
            router.navigate('#/packages/');
        };

        var canSave = ko.computed(function () {
            return hasChanges() && !isSaving();
        });

        var save = function () {
            globalContext.logEvent("Added new package", curPage);

            if (copyTemplate() && isTemplateBank()) {
                //globalContext.saveChanges().fin(
                //   function () {
                //       globalContext.copyPackageTemplate(thePackage().id(), sourceTemplateId()).fin(goToEditView);

                //   })
                $("#create_pkg_dialog").modal("show");
                globalContext.cancelChanges();
                globalContext.newCopyPackage(sourceTemplateId(), true, "", newPackageId).fin(function () {
                    $("#create_pkg_dialog").modal("hide");
                    if (newPackageId == "-1") {
                        globalContext.logEvent("Tried to add pkg before having BS", curPage);
                        return app.showMessage("Please set up a Basic Surplus before adding Packages", "Error", ["Ok"]);
                    } else {
                        goToEditView(newPackageId());
                    }

                });
            }
            else {
                globalContext.saveChanges().then(function () {
                    goToEditView(thePackage().id())
                }).fin(complete);
            }


            function goToEditView(pkgId) {
                isSaving(false);
                router.navigate('#/packageconfig/' + pkgId);
            }

            function complete() {
                isSaving(false);
            }
        };

        var add = function (reportType) {
            router.navigate("#/" + reportType.toLowerCase() + "add/" + thePackage().id());
        };

        var config = function (report) {
            router.navigate("#/" + report.entityType.shortName.toLowerCase() + "config/" + report.id());
        };


        var vm = {
            activate: activate,
            title: 'New Package',
            thePackage: thePackage,
            save: save,
            canSave: canSave,
            hasChanges: hasChanges,
            cancel: cancel,
            reportNames: reportNames,
            add: add,
            config: config,
            atlasTemplates: atlasTemplates,
            isTemplateBank: isTemplateBank,
            copyTemplate: copyTemplate,
            sourceTemplateId: sourceTemplateId,
            profile: profile

        };

        return vm;

        //#region Internal Methods


        //#endregion

    });
