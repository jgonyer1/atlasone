﻿define(['plugins/dialog', 'knockout'], function (dialog, ko) {
    var InputModal = function () {
        this.input = ko.observable('');
    };

    InputModal.prototype.cancel = function () {
        this.noDeactivateValidation = true;
        dialog.close(this, null);
    }

    InputModal.prototype.ok = function () {
        dialog.close(this, this.input());
    };

    InputModal.prototype.canDeactivate = function () {
        if (this.noDeactivateValidation || ! this.model.validation)
            return true;

        var validationResult = this.model.validation(this.input());

        if (validationResult.success)
            return true;

        if (validationResult.message)
            dialog.showMessage(validationResult.message, validationResult.title || '', ['OK']);

        return false;
    };

    InputModal.show = function (model) {
        var instance = new InputModal();
        instance.model = model;
        return dialog.show(instance);
    };

    return InputModal;
});