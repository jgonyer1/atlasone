﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');

    var ctor = function (id) {
        this.simCompareId = id;
        this.simCompare = ko.observable();
        this.simCompareViewData = ko.observable();
        this.leftHighChartOptions = ko.observable();
        this.rightHighChartOptions = ko.observable();
        this.compScenName = ko.observable();
        this.showLeftPolicies = ko.observable();
        this.showRightPolicies = ko.observable();
        this.yr2ColLabel = ko.observable("Y2");
        this.charting = charting;
        this.formatNumber = function (num, numType) {
            //numType is0 for dollar, 1 for percent
            var ret = 0;
            if (num == null) {
                ret = "";
            } else if (numType == 0) {
                ret = Math.round((num / 1000));
                ret = numeral(ret).format("0,0");
            } else if (numType == 1) {
                ret = numeral(num).format("0.00%");
            } else if (numType == 2) {
                ret = numeral(num).format("0.0") + "%";
            }
            return ret;
        };
        this.percChangeComparatorTitle = function () {
            var ret = "";
            switch (this.simCompare().percChangeComparator()) {
                case "year2Year1":
                    ret = "Yr 2 % Change from Yr 1 " + this.compScenName();
                    break;
                case "year2Year2":
                    ret = "Yr 2 % Change from Yr 2 " + this.compScenName();
                    break;
                case "24Month":
                    this.yr2ColLabel("24M       CML");
                    ret = "24M % Change from " + this.compScenName();
                    break;
            }
            return ret;
        };
    };

    ctor.prototype.compositionComplete = function () {
        //if (ctor.simCompareViewData().errors.length > 0) {
        //    return true;
        //}
        var self = this;

        var groupEveryYear = 12;

        if (self.simCompare().isQuarterly()) {
            groupEveryYear = 4;
        }

        var tblData = {
            'left': [],
            'right': []
        };
        var compareScenIndex = -1;

        var scenLookUp = {};
        for (var i = 0; i < self.simCompare().simCompareScenarioTypes().length; i++) {
            tblData['left'][self.simCompare().simCompareScenarioTypes()[i].priority()] = {
                name: self.simCompare().simCompareScenarioTypes()[i].scenarioType().name(),
                years: []
            }
            tblData['right'][self.simCompare().simCompareScenarioTypes()[i].priority()] = {
                name: self.simCompare().simCompareScenarioTypes()[i].scenarioType().name(),
                years: []
            }
            scenLookUp[self.simCompare().simCompareScenarioTypes()[i].scenarioType().name()] = self.simCompare().simCompareScenarioTypes()[i].priority();

            if (self.simCompare().simCompareScenarioTypes()[i].scenarioType().id() == self.simCompare().comparativeScenario()) {
                compareScenIndex = self.simCompare().simCompareScenarioTypes()[i].priority();
            }

        }


        var minVal = 100000000;
        var maxVal = 0;

        var seriesSide = {};

        //Do Each Side
        for (var side in self.simCompareViewData()) {
            if (side != "left" && side != "right") {
                continue;
            }
            seriesSide[side] = {};
            //Do Each Scenario
            var series = [];
            var seriesLookUp = {};
            //Push series on in order
            for (var z = 0; z < tblData['left'].length; z++) {

                var scenProper = charting.scenarioProperName(tblData['left'][z].name);
                var base = false;
                if (tblData['left'][z].name.toUpperCase() == 'BASE') {
                    base = true;
                }

                var marker = {
                    symbol: "url(../Content/images/AtlasGraphIcons/" + charting.scenarioIdLookUp(scenProper) + ".png)"
                }
                if (tblData['left'][z].name.toUpperCase() == 'BASE') {
                    base = true;
                    marker: { };
                }

                series.push({
                    name: scenProper,
                    type: (base ? 'column' : 'line'),
                    data: [],
                    color: charting.scenarioSeriesColor(scenProper),
                    pointStart: 0,
                    pointPadding: -.2,
                    color: charting.scenarioIdLookUp(scenProper).seriesColor,
                    marker: charting.scenarioIdLookUp(scenProper).marker,
                    zIndex: (base ? 0 : 1)
                });
                seriesLookUp[tblData['left'][z].name] = z;
            }


            for (var scen in self.simCompareViewData()[side]) {
                var scenProper = charting.scenarioProperName(scen);
                var monthCounter = 0;
                var year = 1;
                var total = 0;

                for (var i = 0; i < self.simCompareViewData()[side][scen].length; i++) {
                    monthCounter += 1;
                    total += self.simCompareViewData()[side][scen][parseFloat(i)].value;

                    series[seriesLookUp[scenProper]].data.push({ y: numeral(self.simCompareViewData()[side][scen][parseFloat(i)].value).value(), date: self.simCompareViewData()[side][scen][parseFloat(i)].month });


                    if (minVal > self.simCompareViewData()[side][scen][parseFloat(i)].value) {
                        minVal = self.simCompareViewData()[side][scen][parseFloat(i)].value;
                    }

                    if (maxVal < self.simCompareViewData()[side][scen][parseFloat(i)].value) {
                        maxVal = self.simCompareViewData()[side][scen][parseFloat(i)].value;
                    }



                    if (monthCounter == groupEveryYear || i == self.simCompareViewData()[side][scen].length - 1) {
                        tblData[side][scenLookUp[scenProper]].years.push(total);
                        total = 0;
                        monthCounter = 0;
                        year += 1;
                    }

                }


            }
            series = charting.scenarioIconStagger(series);
            seriesSide[side].series = series;
            seriesSide[side].isQuarterly = self.simCompare().isQuarterly()
            seriesSide[side].name = (side == 'left' ? self.simCompare().leftGraphName() : self.simCompare().rightGraphName());

        }

        var units = charting.simCompareChartMinMax(minVal, maxVal);
        for (var s in seriesSide) {
            //var chartObj = JSON.stringify( charting.simulationCompareChart(seriesSide[s].name, seriesSide[s].isQuarterly, seriesSide[s].series, units));
            //$('#' + s + 'Chart').highcharts(charting.simulationCompareChart(seriesSide[s].name, seriesSide[s].isQuarterly, seriesSide[s].series, units));
            $('#' + s + 'Chart').highcharts(charting.sharedAxisChart(seriesSide[s].name, seriesSide[s].isQuarterly, seriesSide[s].series, units, s + "y_axis"));
            if (s == "left") {
                charting.moveSharedAxis(["middleTitle", s + "ChartTitle"], "middle_svg", "middle_axis_col");
            }
            charting.redrawSharedAxisChart(s + 'Chart', s + "y_axis");
        }



        //Write Table Headers
        var headerRow = '<tr><th></th>';
        for (var z = 0; z < tblData['left'].length; z++) {
            headerRow += '<th>' + tblData['left'][z].name + '</th>';
        }
        headerRow += '</tr>';
        //Add To The Three Tables
        $('#leftTable thead').append(headerRow);
        $('#rightTable thead').append(headerRow);
        $('#differenceTable thead').append(headerRow);


        var leftTwoYearCml = '<tr class="two-yr-cml-row"><td><span class="details-frmt-trigger"><nobr>2 Y (CML)</nobr></span></td>';
        var leftTwoYearCmlNII = '<tr><td>Net Interest Inc';
        var leftTwoYearCmlChg = '<tr><td>Net Interest Inc ($ Change)';
        var leftTwoYearCmlPerc = '<tr><td>Net Interest Inc (% Change)';
        var rightTwoYearCml = leftTwoYearCml;
        var rightTwoYearCmlNII = leftTwoYearCmlNII;
        var rightTwoYearCmlChg = leftTwoYearCmlChg;
        var rightTwoYearCmlPerc = leftTwoYearCmlPerc;

        //Loop Through now and add row for each scenario
        for (var x = 0; x < tblData['left'][0].years.length; x++) {
            var leftRow = '<tr><td><span class="details-frmt-trigger">Y ' + (x + 1).toString() + '</span></td>';
            var leftCmpNiiRow = '<tr><td>Net Interest Inc</td>';
            var leftNIIChg = ['<tr><td>Net Interest Inc ($ Change)</td>', '<tr><td>Net Interest Inc ($ Change)</td>'];
            var leftNIIPerc = ['<tr><td>Net Interest Inc (% Change)</td>', '<tr><td>Net Interest Inc (% Change)</td>'];

            var rightRow = leftRow;
            var rightCmpNiiRow = leftCmpNiiRow;
            var rightNIIChg = ['<tr><td>Net Interest Inc ($ Change)</td>', '<tr><td>Net Interest Inc ($ Change)</td>'];
            var rightNIIPerc = ['<tr><td>Net Interest Inc (% Change)</td>', '<tr><td>Net Interest Inc (% Change)</td>'];

            var diffRow = leftRow;

            for (var z = 0; z < tblData['left'].length; z++) {

                var leftNIIVal = numeral(tblData['left'][z].years[x] / 1000).format();
                var leftNIIChgVal = numeral((tblData['left'][z].years[x] - tblData['left'][compareScenIndex].years[x]) / 1000).format('0,0');
                var leftNIIPercVal = numeral((tblData['left'][z].years[x] - tblData['left'][compareScenIndex].years[x]) / tblData['left'][compareScenIndex].years[x]).format('%0.00');
                var rightNIIVal = numeral(tblData['right'][z].years[x] / 1000).format();
                var rightNIIChgVal = numeral((tblData['right'][z].years[x] - tblData['right'][compareScenIndex].years[x]) / 1000).format('0,0');
                var rightNIIPercVal = numeral((tblData['right'][z].years[x] - tblData['right'][compareScenIndex].years[x]) / tblData['right'][compareScenIndex].years[x]).format('%0.0');

                //blank it out if we're on the comparative scenario
                if (z == compareScenIndex) {
                    leftNIIChgVal = '';
                    leftNIIPercVal = '';
                    rightNIIChgVal = '';
                    rightNIIPercVal = '';
                }

                //only need to get the two year cumulative once
                if (x == 0) {
                    var leftTwoYearCmlNII_raw = (tblData['left'][z].years[0] + tblData['left'][z].years[1]);
                    var leftTwoYearCmlChg_raw = leftTwoYearCmlNII_raw - (tblData['left'][compareScenIndex].years[0] + tblData['left'][compareScenIndex].years[1]);
                    var leftTwoYearCmlPerc_raw = leftTwoYearCmlChg_raw / (tblData['left'][compareScenIndex].years[0] + tblData['left'][compareScenIndex].years[1]);
                    var rightTwoYearCmlNII_raw = (tblData['right'][z].years[0] + tblData['right'][z].years[1]);
                    var rightTwoYearCmlChg_raw = rightTwoYearCmlNII_raw - (tblData['right'][compareScenIndex].years[0] + tblData['right'][compareScenIndex].years[1]);
                    var rightTwoYearCmlPerc_raw = rightTwoYearCmlChg_raw / (tblData['right'][compareScenIndex].years[0] + tblData['right'][compareScenIndex].years[1]);

                    leftTwoYearCml += '<td></td>';
                    rightTwoYearCml += '<td></td>';

                    leftTwoYearCmlNII += '<td>' + numeral(leftTwoYearCmlNII_raw / 1000).format('0,0') + '</td>';
                    rightTwoYearCmlNII += '<td>' + numeral(rightTwoYearCmlNII_raw / 1000).format('0,0') + '</td>';

                    if (z == compareScenIndex) {
                        leftTwoYearCmlChg += '<td></td>';
                        leftTwoYearCmlPerc += '<td></td>';
                        rightTwoYearCmlChg += '<td></td>';
                        rightTwoYearCmlPerc += '<td></td>';
                    } else {
                        leftTwoYearCmlChg += '<td>' + numeral(leftTwoYearCmlChg_raw / 1000).format('0,0') + '</td>';
                        leftTwoYearCmlPerc += '<td>' + numeral(leftTwoYearCmlPerc_raw).format('%0.00') + '</td>';
                        rightTwoYearCmlChg += '<td>' + numeral(rightTwoYearCmlChg_raw / 1000).format('0,0') + '</td>';
                        rightTwoYearCmlPerc += '<td>' + numeral(rightTwoYearCmlPerc_raw).format('%0.00') + '</td>';
                    }
                }



                //row data gets shifted down if details are checked
                if (!self.simCompare().details()) {
                    leftRow += '<td>' + leftNIIVal + '</td>';
                    rightRow += '<td>' + rightNIIVal + '</td>';
                } else {
                    leftRow += '<td></td>';
                    rightRow += '<td></td>';
                }

                //always populate these, only append them on if details is checked
                leftCmpNiiRow += '<td>' + leftNIIVal + '</td>';
                rightCmpNiiRow += '<td>' + rightNIIVal + '</td>';

                leftNIIChg[x] += '<td>' + leftNIIChgVal + '</td>';
                leftNIIPerc[x] += '<td>' + leftNIIPercVal + '</td>';

                rightNIIChg[x] += '<td>' + rightNIIChgVal + '</td>';
                rightNIIPerc[x] += '<td>' + rightNIIPercVal + '</td>';
                diffRow += '<td>' + numeral((tblData['left'][z].years[x] - tblData['right'][z].years[x]) / 1000).format() + '</td>';
            }
            leftRow += '</tr>';
            leftCmpNiiRow += '</tr>';
            leftNIIChg[x] += '</tr>';
            leftTwoYearCml += '</tr>';
            leftTwoYearCmlNII += '</tr>';
            leftTwoYearCmlChg += '</tr>';
            leftTwoYearCmlPerc += '</tr>';
            leftNIIPerc[x] += '</tr>';
            rightRow += '</tr>';
            rightCmpNiiRow += '</tr>';
            rightNIIChg[x] += '</tr>';
            rightTwoYearCml += '</tr>';
            rightTwoYearCmlNII += '</tr>';
            rightTwoYearCmlChg += '</tr>';
            rightTwoYearCmlPerc += '</tr>';
            rightNIIPerc[x] += '</tr>';
            diffRow += '</tr>';
            $('#leftTable tbody').append(leftRow);
            $('#rightTable tbody').append(rightRow);

            if (self.simCompare().details()) {



                $('#leftTable tbody').append(leftCmpNiiRow);
                $('#leftTable tbody').append(leftNIIChg[x]);
                $('#leftTable tbody').append(leftNIIPerc[x]);
                $('#rightTable tbody').append(rightCmpNiiRow);
                $('#rightTable tbody').append(rightNIIChg[x]);
                $('#rightTable tbody').append(rightNIIPerc[x]);

            }

            $('#differenceTable tbody').append(diffRow);
        }

        if (self.simCompare().details()) {
            $('#leftTable').addClass('details-trigger');
            $('#rightTable').addClass('details-trigger');
            $('#leftTable tbody').append(leftTwoYearCml);
            $('#leftTable tbody').append(leftTwoYearCmlNII);
            $('#leftTable tbody').append(leftTwoYearCmlChg);
            $('#leftTable tbody').append(leftTwoYearCmlPerc);
            $('#rightTable tbody').append(rightTwoYearCml);
            $('#rightTable tbody').append(rightTwoYearCmlNII);
            $('#rightTable tbody').append(rightTwoYearCmlChg);
            $('#rightTable tbody').append(rightTwoYearCmlPerc);
        }

        global.loadingReport(false);
        $('#reportLoaded').text('1');
        //Export To Pdf
        if (typeof hiqPdfInfo == "undefined") {
        }
        else {
            setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);

        }
    }


    ctor.prototype.activate = function () {
        global.loadingReport(true);
        var self = this;

        return globalContext.getSimCompareViewDataById(self.simCompareId, self.simCompareViewData).then(function () {
            return globalContext.getSimCompareById(self.simCompareId, self.simCompare).then(function () {
                globalContext.reportErrors(self.simCompareViewData().errors, self.simCompareViewData().warnings);
                self.compScenName(self.simCompareViewData().compareScenarioName);
                self.showLeftPolicies(self.simCompareViewData().leftSimulationId.toString() == self.simCompareViewData().leftPolicyNiiSimulationId);
                self.showRightPolicies(self.simCompareViewData().rightSimulationId.toString() == self.simCompareViewData().rightPolicyNiiSimulationId);
            });
        }).fail(function (error) {
            console.log(error);
        });
    };

    return ctor;
});
