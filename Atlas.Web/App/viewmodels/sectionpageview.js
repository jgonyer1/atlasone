﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var sectionPageVm = function (id, name) {
        var self = this;
        this.sectionPage = ko.observable();
        this.sectionName = ko.observable(name);

        this.compositionComplete = function () {
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }
        };

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getSectionPageById(id, self.sectionPage).then(function () {


            });
        };
    };

    return sectionPageVm;

});
