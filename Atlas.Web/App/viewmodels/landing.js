﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/system',
    'durandal/app',
    'services/logger',
    'services/model',
    'services/global'
],
    function (globalContext, router, system, app, logger, model, global) {
        var landingVm = function () {
            var self = this;
            this.isLoading = ko.observable(false);

            this.profile = ko.observableArray();
            this.availableBanks = ko.observableArray();
            this.asOfDates = ko.observableArray();
            this.status = ko.observableArray();
            this.userName = ko.observableArray();

            var curPage = "Landing";
            

            this.cancel = function () {
            };

            function SetDatabaseAsOfDateLabel() {
                //$('#database').text('Current Bank: ' + $("#atlasDatabaseSelector option:selected").text());
                //$('#asOfDate').text('Current As of Date: ' + $("#atlasAsOfDateSelector option:selected").text());
                $('#package').text(''); //Just to clear it when leaving a package
                $('#packageTemplate').text(''); //Just to clear it when leaving a package
            }

            //Captures the database chaning so we can update the profile and pull new asOfDates
            this.databaseChanged = function () {
                self.isLoading(true);
                globalContext.metaDataCheck(self.goodBank, self.profile().databaseName).then(function () {
                    self.isLoading(false);
                    if (!self.isLoading() && self.goodBank()) {
                        //Update available asOfDates, lock status, Atlas Templates options, etc.
                        let event = "Chose bank from dropdown - " + self.profile().databaseName;
                        globalContext.logEvent(event, curPage);
                        return self.updateDatabaseInfo().then(function () {
                            //Update current asOfDate to most recent if available
                            if (self.asOfDates().length > 0) {
                                atlasAsOfDateSelector.value = self.asOfDates()[0].asOfDateDescript;

                                //These can get out of sync due to auto-setting the asOfDateSelector above, so make sure we update profile if so
                                //Unfortunately we can't just set profile atlasAsOfDate directly as it is not an observable and won't reflect in HTML
                                if (atlasAsOfDateSelector.value != self.profile().asOfDate) {
                                    self.profile().asOfDate = atlasAsOfDateSelector.value;
                                    return self.asOfDateChanged();
                                }
                            }
                            SetDatabaseAsOfDateLabel();
                        });
                    } else if (!self.goodBank()) {
                        let event = "Chose bank with out of date model - " + self.profile().databaseName;
                        globalContext.logEvent(event, curPage);
                        $("#bad_bank_modal").modal("show");
                    }
                });
                

            }

            //If as of date changes just update profile object
            this.asOfDateChanged = function () {
                if (!self.isLoading()) {
                    var testSimScenario = ko.observable();


                    //Check to see if there is at least base simulatino and base scenario for this asofdate
                    if (self.profile().asOfDate != undefined) {
                        return globalContext.updateProfile(self.profile().databaseName, self.profile().asOfDate, self.profile).then(function () {
                            return globalContext.getSimulationScenarioInfo(self.profile().asOfDate, 0, 1, 1, testSimScenario).then(function () {
                                if (testSimScenario().asOfDate == "" && testSimScenario().scenario == "" && testSimScenario().simulation == "") {
                                    let event = "No base sim base scen for bank and aod - " + self.profile().databaseName + "; " + self.profile().asOfDate;
                                    globalContext.logEvent(event, curPage);
                                    document.getElementById("atlas_aod_selector_fg").classList.add("has-error");
                                    document.getElementById("atlasAsOfDateSelector").setAttribute("title", "Cannot find Base Simulation or Base Scenario for this As Of Date");
                                } else {
                                    document.getElementById("atlas_aod_selector_fg").classList.remove("has-error");
                                    document.getElementById("atlasAsOfDateSelector").setAttribute("title", "");
                                }
                                SetDatabaseAsOfDateLabel();
                            });
                        });
                    } else {
                        document.getElementById("atlas_aod_selector_fg").classList.add("has-error");
                        document.getElementById("atlasAsOfDateSelector").setAttribute("title", "Cannot find Base Simulation or Base Scenario for this As Of Date");
                    }



                }

            }

            //Handles when User Clicks Log In
            this.toggleDatabase = function () {
                var locking = !global.navBarLock();
                if (!locking) {
                    globalContext.setDatabaseStatus(locking, self.status).then(function () {
                        let event = "Logged out of bank - " + self.profile().databaseName;
                        globalContext.logEvent(event, curPage);
                        //context Check
                        global.navBarLock(locking);
                    });
                } else {
                    globalContext.metaDataCheck(self.goodBank, self.profile().databaseName).then(function () {
                        if (!self.goodBank() && locking) {
                            let event = "Chose bank with out of date model - " + self.profile().databaseName;
                            globalContext.logEvent(event, curPage);
                            $("#bad_bank_modal").modal("show");
                        } else {
                            globalContext.setDatabaseStatus(locking, self.status).then(function () {
                                //context Check
                                let event = "Logged into bank";
                                globalContext.logEvent(event, curPage);
                                global.navBarLock(locking);
                            });
                        }
                    });
                }
            };

            this.global = global;


            //Update information (locked status, nav options, etc.) based on current database
            this.updateDatabaseInfo = function () {

                // if (!this.isLoading) {
                return globalContext.updateProfile(self.profile().databaseName, self.profile().asOfDate, self.profile).then(function () {
                    return globalContext.getAsOfDates(self.asOfDates);
                }).then(function () {
                    return globalContext.getDatabaseStatus(self.status);
                }).then(function () {
                    //Set NavBar options for Atlas Templates
                    global.navBarShowAtlasTemplatesOptions(self.profile().databaseName == 'DDW_Atlas_Templates');

                    //Just in case we locked a previous bank and scroll past it, make us unlock it
                    global.navBarLock(self.status().atlas_lock == 'Atlas_' + self.userName());

                    //Clear EntityManager cache - see definition for comments on why we do this
                    globalContext.clearCache();
                });
                //  }

            }

            this.activate = function () {
                console.log('activating');
                self.isLoading(true);
                this.goodBank = ko.observable();
                
                //Load Profile Object
                return Q.all([
                    globalContext.getUserProfile(self.profile),
                    globalContext.getAvailableBanks(self.availableBanks)
                ]).then(function () {
                    return globalContext.metaDataCheck(self.goodBank, self.profile().databaseName).then(function () {
                        this.canLogIn = ko.pureComputed(function () {
                            var isGood = (self.status().atlas_lock == 'Not Locked' || global.navBarLock()) && self.goodBank() && !self.isLoading();
                            return isGood;
                        });
                        if (!self.goodBank()) {
                            $("#bad_bank_modal").modal("show");
                        }
                        if (self.profile().userName.indexOf('@') >= 0) {
                            self.userName(self.profile().userName.substring(0, self.profile().userName.indexOf('@')));
                        }
                        else {
                            self.userName(self.profile().userName);
                        }
                        let event = "Viewed";
                        globalContext.logEvent(event, curPage);
                        return self.updateDatabaseInfo();
                    });
                });
            };

            this.compositionComplete = function () {
                SetDatabaseAsOfDateLabel();
                //make sure whatever is inititally selected on the bank dropdown actually gets set to the atlas database
                var dbSelect = document.getElementById("atlasDatabaseSelector");
                var selectedDBName = dbSelect.options[dbSelect.selectedIndex].value;
                if (self.profile().databaseName != selectedDBName) {

                    self.profile().databaseName = selectedDBName;
                    self.databaseChanged();
                }
                self.isLoading(false);
                this.updateDatabaseInfo();
            };

            return this;
        };

        return landingVm;
    });
