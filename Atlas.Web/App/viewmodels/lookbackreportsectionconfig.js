﻿define([
    'services/globalcontext',
    'durandal/app'
],
    function (globalContext, app) {
        var lb = ko.observable();

        var vm = function (section, allLookbackTypes, instNameObs) {
            var self = this;
            this.section = section;
            this.lookbackTypes = ko.observableArray(section.lookbackTypes());
            this.cancel = function () {
            };
           
            self.section.lookbackTypes().forEach(function (lbType) {
                lbType.asOfDateOffset.subscribe(function () {
                    globalContext.getLBLookbacksByOffset(lbType.asOfDateOffset(), instNameObs, lbType.availLookbacks);
                });
                lbType.availLookbacks = ko.observableArray();
                globalContext.getLBLookbacksByOffset(lbType.asOfDateOffset(), instNameObs, lbType.availLookbacks);

                lbType.availLookbacks.subscribe(function () {
                    if (lbType.availLookbacks().length == 0) {
                        lbType.asOfDateOffset(0);
                        toastr.warning("No Lookbacks found for that date offset");
                    }
                });

                
            });

            //list of all the lookbacks
            this.availableLookbacks = ko.observableArray();
            this.sortedLookbackTypes = ko.observableArray();


            this.addLookbackToSection = function () {
                var newLookbackType = globalContext.createLookbackType();
                newLookbackType.asOfDateOffset(0);
                newLookbackType.priority(self.section.lookbackTypes().length);
                newLookbackType.asOfDateOffset.subscribe(function () {
                    globalContext.getLBLookbacksByOffset(newLookbackType.asOfDateOffset(), instNameObs, newLookbackType.availLookbacks);
                });
                newLookbackType.availLookbacks = ko.observableArray();
                globalContext.getLBLookbacksByOffset(newLookbackType.asOfDateOffset(), instNameObs, newLookbackType.availLookbacks);
                newLookbackType.availLookbacks.subscribe(function () {
                    if (newLookbackType.availLookbacks().length == 0) {
                        newLookbackType.asOfDateOffset(0);
                        toastr.warning("No Lookbacks found for that date offset");
                    }
                });
                globalContext.getLBLookbacksByOffset(newLookbackType.asOfDateOffset(), instNameObs(), newLookbackType.availLookbacks);
                self.section.lookbackTypes().push(newLookbackType);
                self.sortLookbackTypes(section);
            };

            this.removeSection = function (section) {
                var msg = 'Delete scenario "' + section().name() + '" ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        section.entityAspect.setDeleted();
                        self.sections().forEach(function (type, index) {
                            type.priority(index);
                        });
                        self.sortSections();
                    }
                }
            };

            this.removeLookbackType = function (lookbackType) {

                var msg = 'Delete lookback ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        lookbackType.entityAspect.setDeleted();
                        self.section().lookbackTypes().forEach(function (type, index) {
                            type.priority(index);
                        });
                        self.sortLookbackTypes();
                    }
                }

            };

            this.dontAddSection = function (data, event) {
                event.stopPropagation();
            };


            this.removeSection = function (section) {

                var msg = 'Delete scenario "' + section().name() + '" ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        section.entityAspect.setDeleted();
                        self.lb().sections().forEach(function (aSection, index) {
                            aSection.priority(index);
                        });
                        self.sortSections();
                    }
                }

            };
            this.sortLookbackTypes = function () {
                self.sortedLookbackTypes(self.section.lookbackTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
            };


            this.activate = function (id) {
                //add the avail lookback list to existing lookbacktypes in the section
                
            };

        };


        return vm;

    });
