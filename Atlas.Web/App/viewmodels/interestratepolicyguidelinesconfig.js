﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var interestRatePolicyGuidelinesVm = function (interestRatePolicyGuidelines) {
        var self = this;
        var curPage = "InterestRatePolicyGuidelinesConfig";

        //Report
        this.interestRatePolicyGuidelines = interestRatePolicyGuidelines;

        //Vars
        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();
        this.nIIScenarioTypes = ko.observableArray();
        this.eVEScenarioTypes = ko.observableArray();
        this.nIIComparativeTypes = ko.observableArray([
            { key: "year1Year1", name: "Year 1 to Year 1 Base" },
            { key: "year2Year1", name: "Year 2 to Year 1 Base" },
            { key: "year2Year2", name: "Year 2 to Year 2 Base" },
            { key: "24Month", name: "24 Month % Change from Base" }
        ]);
        this.nIIScenarioTypeLabels = ko.observableArray([
            { key: "core", name: "Core" },
            { key: "alternative", name: "Alternative" },
            { key: "shock", name: "Shock" },
            { key: "custom", name: "Custom" },
            { key: "none", name: "None" }
        ]);
        this.eVEComparativeTypes = ko.observableArray([
            { key: "postShock", name: "Post-Shock EVE/NEV Ratio" },
            { key: "bpChange", name: "BP Change in EVE/NEV Ratio" },
            { key: "eveChange", name: "EVE/NEV % Change from 0 Shock" }
        ]);

        //View
        this.connectClassDate = "date-container";
        this.connectClassNIIComparative = "niicomp-container";
        this.connectClassNIIScenarioType = "niiscentype-container";
        this.connectClassNIIScenario = "niiscen-container";
        this.connectClassEVEComparative = "evecomp-container";
        this.connectClassEVEScenario = "evescen-container";

        //Navigation
        this.cancel = function () { };

        //Sorting
        this.sortTypes = function () {
            self.sortedDates(self.sortedDates().sort(function (a, b) {
                return a.priority() - b.priority();
            }));

            self.sortedNIIComparatives(self.sortedNIIComparatives().sort(function (a, b) {
                return a.priority() - b.priority();
            }));

            self.sortedEVEComparatives(self.sortedEVEComparatives().sort(function (a, b) {
                return a.priority() - b.priority();
            }));

            self.sortedNIIComparatives().forEach(function (type) {
                type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                    return a.priority() - b.priority();
                }));
                type.sortedTypes().forEach(function (type) {
                    type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                        return a.priority() - b.priority();
                    }));
                });
            });

            self.sortedEVEComparatives().forEach(function (type) {
                type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                    return a.priority() - b.priority();
                }));
            });
        };

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (type, index) {
                type.priority(index);
            });
        };

        //Add / Remove
        this.addDate = function () {
            var type = globalContext.createInterestRatePolicyGuidelinesDate();
            type.interestRatePolicyGuidelinesId(self.interestRatePolicyGuidelines().id());
            type.priority(self.sortedDates().length);
            //self.subAll(type);
            self.interestRatePolicyGuidelines().dates.push(type);
            self.sortTypes();
            return type;
        };
        this.addNIIComparative = function () {
            var type = globalContext.createInterestRatePolicyGuidelinesNIIComparative();
            type.interestRatePolicyGuidelinesId(self.interestRatePolicyGuidelines().id());
            type.priority(self.sortedNIIComparatives().length);
            //self.subAll(type);
            self.interestRatePolicyGuidelines().nIIComparatives.push(type);
            type.sortedTypes = ko.observableArray(type.scenarioTypes());
            type.addType = function () {
                var subType = globalContext.createInterestRatePolicyGuidelinesNIIScenarioType();
                subType.interestRatePolicyGuidelinesNIIComparativeId(type.id());
                subType.priority(type.sortedTypes().length);
                //self.subAll(subType);
                type.scenarioTypes.push(subType);
                subType.sortedTypes = ko.observableArray(subType.scenarios());
                subType.addType = function () {
                    var subSubType = globalContext.createInterestRatePolicyGuidelinesNIIScenario();
                    subSubType.interestRatePolicyGuidelinesScenarioTypeId(subType.id());
                    subSubType.priority(subType.sortedTypes().length);
                    //self.subAll(subType);
                    subType.scenarios.push(subSubType);
                    self.sortTypes();
                    return subSubType;
                };
                self.sortTypes();
                return subType;
            };
            self.sortTypes();
            return type;
        };
        this.addEVEComparative = function () {
            var type = globalContext.createInterestRatePolicyGuidelinesEVEComparative();
            type.interestRatePolicyGuidelinesId(self.interestRatePolicyGuidelines().id());
            type.priority(self.sortedNIIComparatives().length);
            //self.subAll(type);
            self.interestRatePolicyGuidelines().eVEComparatives.push(type);
            type.sortedTypes = ko.observableArray(type.scenarios());
            type.addType = function () {
                var subType = globalContext.createInterestRatePolicyGuidelinesEVEScenario();
                subType.interestRatePolicyGuidelinesEVEComparativeId(type.id());
                subType.priority(type.sortedTypes().length);
                //self.subAll(subType);
                type.scenarios.push(subType);
                self.sortTypes();
                return subType;
            };
            self.sortTypes();
            return type;
        };

        this.removeType = function (type) {
            var msg = 'Delete row?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    //For some reason we have to first manually delete the subtypes
                    //And for some reason we must build a subTypesToDel to do this
                    var typesToDel = [];
                    if (type.sortedTypes) {
                        type.sortedTypes().forEach(function (type) {
                            if (type.sortedTypes) {
                                type.sortedTypes().forEach(function (type) {
                                    typesToDel.push(type);
                                });
                            }
                            typesToDel.push(type);
                        });
                    }
                    typesToDel.push(type);
                    typesToDel.forEach(function (type) {
                        type.entityAspect.setDeleted();
                    });

                    //We don't know which one we're deleting (yet), so sort all of them
                    self.interestRatePolicyGuidelines().dates().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    self.interestRatePolicyGuidelines().nIIComparatives().forEach(function (aType, index) {
                        aType.priority(index);
                        aType.scenarioTypes().forEach(function (aType, index) {
                            aType.priority(index);
                            aType.scenarios().forEach(function (aType, index) {
                                aType.priority(index);
                            });
                        });
                    });
                    self.interestRatePolicyGuidelines().eVEComparatives().forEach(function (aType, index) {
                        aType.priority(index);
                        aType.scenarios().forEach(function (aType, index) {
                            aType.priority(index);
                        });
                    });
                    self.sortTypes();
                }
                return selectedOption;
            }
        };



        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            return globalContext.getScenarioTypes(self.nIIScenarioTypes).then(function () {
                return globalContext.getEveScenarioTypes(self.eVEScenarioTypes);
            }).then(function () {
                return globalContext.getSimulationTypes(self.simulationTypes);
            }).then(function () {
                return globalContext.getAvailableBanks(self.institutions);
            }).then(function () {
                //Sorting
                self.sortedDates = ko.observableArray(interestRatePolicyGuidelines().dates());
                self.sortedNIIComparatives = ko.observableArray(interestRatePolicyGuidelines().nIIComparatives());
                self.sortedEVEComparatives = ko.observableArray(interestRatePolicyGuidelines().eVEComparatives());
                self.interestRatePolicyGuidelines().nIIComparatives().forEach(function (type) {
                    type.sortedTypes = ko.observableArray(type.scenarioTypes());
                    type.addType = function () {
                        var subType = globalContext.createInterestRatePolicyGuidelinesNIIScenarioType();
                        subType.interestRatePolicyGuidelinesNIIComparativeId(type.id());
                        subType.priority(type.sortedTypes().length);
                        //self.subAll(subType);
                        type.scenarioTypes.push(subType);
                        subType.sortedTypes = ko.observableArray(subType.scenarios());
                        subType.addType = function () {
                            var subSubType = globalContext.createInterestRatePolicyGuidelinesNIIScenario();
                            subSubType.interestRatePolicyGuidelinesScenarioTypeId(subType.id());
                            subSubType.priority(subType.sortedTypes().length);
                            //self.subAll(subType);
                            subType.scenarios.push(subSubType);
                            self.sortTypes();
                            return subSubType;
                        };
                        self.sortTypes();
                        return subType;
                    };
                    type.scenarioTypes().forEach(function (type) {
                        type.sortedTypes = ko.observableArray(type.scenarios());
                        type.addType = function () {
                            var subType = globalContext.createInterestRatePolicyGuidelinesNIIScenario();
                            subType.interestRatePolicyGuidelinesScenarioTypeId(type.id());
                            subType.priority(type.sortedTypes().length);
                            //self.subAll(subType);
                            type.scenarios.push(subType);
                            self.sortTypes();
                            return subType;
                        };
                    });
                });
                self.interestRatePolicyGuidelines().eVEComparatives().forEach(function (type) {
                    type.sortedTypes = ko.observableArray(type.scenarios());
                    type.addType = function () {
                        var subType = globalContext.createInterestRatePolicyGuidelinesEVEScenario();
                        subType.interestRatePolicyGuidelinesEVEComparativeId(type.id());
                        subType.priority(type.sortedTypes().length);
                        //self.subAll(subType);
                        type.scenarios.push(subType);
                        self.sortTypes();
                        return subType;
                    };
                });
                self.sortTypes();

                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub = self.interestRatePolicyGuidelines().institutionDatabaseName.subscribe(function (newValue) {
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                });
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.interestRatePolicyGuidelines().institutionDatabaseName()).then(function () {
                    global.loadingReport(false);

                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return interestRatePolicyGuidelinesVm;
});
