﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var fundingMatrixVm = function (id) {
        var self = this;
        var curPage = "FundingMatrixView";
        this.fundingMatrixViewData = ko.observable();
        this.data;
        this.leftScenarioName;
        this.rightScenarioName;
        this.leftDate;
        this.rightDate;
        var balScale = 1000000;


        this.compositionComplete = function () {

            if (self.data.length < 3) {
                $('#liabSource1').remove();
            }
            for (var i = 0; i < 4; i++) {
                var tbl = '#fundMatrix' + i;
                var $leftMatrix = $(tbl), leftTable = $leftMatrix[0];
                if (i > self.data.length - 1) {
                    $leftMatrix.remove();
                } else {

                    var r = 3;
                    var c = 1;

                    leftTable.rows[0].cells[2].innerHTML = self.data[i].scenarioName;

                    //HEre need to add dynamic colors based off of scenaior
                    //going to do this css style beacuse otherwise it would get complicated
                    var scenColor = charting.scenarioIdLookUp(self.data[i].origScen).seriesColor;
                    $(tbl + ' td.outside-border').css("background-color", scenColor);

                    $(tbl + ' > tbody > tr:last-child > td').css("border-bottom", "2px solid " + scenColor);
                    $(tbl + ' > tbody > tr > td:last-of-type').css("border-right", "2px solid " + scenColor);
                    $(tbl + ' > tbody > tr:last-child > td:last-of-type').css("border-bottom-right-radius", "10px");

                    $(tbl + '> tbody > tr > td.vert-cell').css("border-bottom", "2px solid " + scenColor);

                    //Set Date Headers
                    for (var m in self.data[i].assetData) {
                        if (r != 2) {
                            leftTable.rows[1].cells[c].innerHTML = m;
                            leftTable.rows[r].cells[0].innerHTML = m;
                        }
                        c += 1;
                        r += 1;

                    }

                    //Asset Data Totals Start at 2
                    c = 2;
                    for (var m in self.data[i].assetData) {
                        leftTable.rows[2].cells[c].innerHTML = "<div>" + numeral(self.data[i].assetData[m].balance / balScale).format() + '<br>' + numeral(self.data[i].assetData[m].rate).format('(0.00)') + "</div>";
                        leftTable.rows[2].cells[c].setAttribute('data-balance', self.data[i].assetData[m].balance);
                        leftTable.rows[2].cells[c].setAttribute('data-rate', self.data[i].assetData[m].rate);

                        c++;
                    }

                    //Liability Data 
                    r = 3;
                    for (var m in self.data[i].liabData) {
                        leftTable.rows[r].cells[1].innerHTML = "<div>" + numeral(self.data[i].liabData[m].balance / balScale).format() + '<br>' + numeral(self.data[i].liabData[m].rate).format('(0.00)') + "</div>";
                        leftTable.rows[r].cells[1].setAttribute('data-balance', self.data[i].liabData[m].balance);
                        leftTable.rows[r].cells[1].setAttribute('data-rate', self.data[i].liabData[m].rate);

                        r += 1;
                    }

                    //Put into Arrays and reverse so I can start at one Day
                    var assetArr = Object.keys(self.data[i].assetData);
                    var liabArr = Object.keys(self.data[i].liabData);

                    var remainderAsset = false;
                    var remainderBalance = 0;
                    var balRow = 3;
                    var balCol = 2;
                    var lastUsedAsset = 0;
                    var lastUsedLiab = 0;
                    var assetBal = self.data[i].assetData['Over 60<br>Months'].balance;
                    var liabBal = self.data[i].liabData['Over 60<br>Months'].balance;
                    var assetRate = self.data[i].assetData['Over 60<br>Months'].balance;
                    var liabRate = self.data[i].liabData['Over 60<br>Months'].balance;

                    while (balRow < 10 && balCol <= 8) {

                        var rateDiff = 0;
                        if (assetBal > liabBal) {
                            assetBal = assetBal - liabBal;
                            rateDiff = self.data[i].assetData[assetArr[lastUsedAsset]].rate - self.data[i].liabData[liabArr[lastUsedLiab]].rate;
                            leftTable.rows[balRow].cells[balCol].innerHTML = "<div>" + numeral(liabBal / balScale).format() + '<br>' + numeral(rateDiff).format('(0.00)') + "</div>";
                            leftTable.rows[balRow].cells[balCol].setAttribute('data-balance', liabBal);
                            leftTable.rows[balRow].cells[balCol].setAttribute('data-rate', rateDiff);

                            lastUsedLiab += 1;
                            if (lastUsedLiab <= 6) {
                                liabBal = self.data[i].liabData[liabArr[lastUsedLiab]].balance;
                            }

                            balRow += 1;
                        }
                        else {
                            liabBal = liabBal - assetBal;

                            rateDiff = self.data[i].assetData[assetArr[lastUsedAsset]].rate - self.data[i].liabData[liabArr[lastUsedLiab]].rate;
                            leftTable.rows[balRow].cells[balCol].innerHTML = "<div>" + numeral(assetBal / balScale).format('(0,0)') + '<br>' + numeral(rateDiff).format('(0.00)') + "</div>";
                            leftTable.rows[balRow].cells[balCol].setAttribute('data-balance', assetBal);
                            leftTable.rows[balRow].cells[balCol].setAttribute('data-rate', rateDiff);

                            lastUsedAsset += 1;
                            if (lastUsedAsset <= 6) {
                                assetBal = self.data[i].assetData[liabArr[lastUsedAsset]].balance;
                            }

                            balCol += 1;
                        }

                    }

                    //Totals By Row
                    balRow = 2;
                    balCol = 2;
                    for (var r = balRow; r <= 10; r += 1) {
                        var runningBal = 0;
                        var runningRate = 0;
                        for (var c = balCol; c <= 9; c++) {
                            var bal = numeral().unformat(leftTable.rows[r].cells[c].getAttribute('data-balance'));
                            var rate = numeral().unformat(leftTable.rows[r].cells[c].getAttribute('data-rate'));
                            runningBal += bal;
                            runningRate += rate * bal;
                        }
                        leftTable.rows[r].cells[9].innerHTML = "<div>" + numeral(runningBal / balScale).format('(0,0)') + '<br>' + numeral(runningRate / runningBal).format('(0.00)') + "</div>";
                        leftTable.rows[r].cells[9].setAttribute('data-balance', runningBal);
                        leftTable.rows[r].cells[9].setAttribute('data-rate', (runningRate / runningBal));
                    }


                    //Totals By Column
                    balRow = 3;
                    balCol = 1;
                    var totalRunningBal = 0;
                    var totalRunningRate = 0;
                    for (var c = balCol; c <= 9; c++) {
                        var runningBal = 0;
                        var runningRate = 0;

                        for (var r = balRow; r < 11; r += 1) {
                            var bal = numeral().unformat(leftTable.rows[r].cells[c].getAttribute('data-balance'));
                            var rate = numeral().unformat(leftTable.rows[r].cells[c].getAttribute('data-rate'));
                            runningBal += bal;
                            runningRate += rate * bal;
                            if (c > 1) {
                                totalRunningBal += bal;
                                totalRunningRate += rate * bal;
                            }

                        }
                        leftTable.rows[10].cells[c].innerHTML = "<div>" + numeral(runningBal / balScale).format('(0,0)') + '<br>' + numeral(runningRate / runningBal).format('(0.00)') + "</div>";
                        leftTable.rows[10].cells[c].setAttribute('data-balance', runningBal);
                        leftTable.rows[10].cells[c].setAttribute('data-rate', (runningRate / runningBal));

                    }

                }

                var fundMatrixCells = $('.fund-matrix-table td.circle');
                fundMatrixCells.each(function (index, item) {
                    if ($(item).text().length > 0) {
                        $(item).addClass("full");
                    }
                });


                // Return today's date and time
                var currentTime = new Date()

                $('.curYear').text(currentTime.getFullYear())

                global.loadingReport(false);
                if (typeof hiqPdfInfo == "undefined") {
                    globalContext.logEvent("Viewed", curPage);  
                }
                else {
                    $('.report-wrapper').css('min-height', '8.2in');
                    $('.report-view').css('padding-bottom', '0');
                    //$('.report-view').height($('.report-view').height());
                    $("#fm_view").height($("#fm_view").height());
                    $('#reportLoaded').text('1');
                    hiqPdfConverter.startConversion();
                    //setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);
                }


            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getFundingMatrixViewDataById(id, self.fundingMatrixViewData).then(function () {
                globalContext.reportErrors(self.fundingMatrixViewData().errors, self.fundingMatrixViewData().warnings);
                var data = [];
                for (var d = 0; d < self.fundingMatrixViewData().dates.length; d++) {
                    var curDate = moment(self.fundingMatrixViewData().dates[d]).add(1, 'day');
                    var scenLabel = self.fundingMatrixViewData().scenNames[d];


                    if (self.fundingMatrixViewData().scenNames[d].toUpperCase().indexOf("DOWN") > -1) {
                        scenLabel = '<span class="fa fa-arrow-down"></span>&nbsp;' + scenLabel;
                    } else if (self.fundingMatrixViewData().scenNames[d].toUpperCase().indexOf("UP") > -1) {
                        scenLabel = '<span class="fa fa-arrow-up"></span>&nbsp;' + scenLabel;
                    }


                    if (moment(self.fundingMatrixViewData().packageAOD).format('MM/DD/YYYY').valueOf() != moment(self.fundingMatrixViewData().dates[d]).format('MM/DD/YYYY').valueOf()) {
                        scenLabel += ' ' + moment(self.fundingMatrixViewData().dates[d]).format('MM/DD/YYYY').valueOf();
                    }
                    if (self.fundingMatrixViewData().simNames[d].toLowerCase().indexOf('base') == -1) {
                        scenLabel += ' ' + self.fundingMatrixViewData().simNames[d];
                    }
                    data.push({
                        assetData: {
                            'Over 60<br>Months': { balance: 0, rate: 0 },
                            '37-60<br>Months': { balance: 0, rate: 0 },
                            '25-36<br>Months': { balance: 0, rate: 0 },
                            '13-24<br>Months': { balance: 0, rate: 0 },
                            '7-12<br>Months': { balance: 0, rate: 0 },
                            '4-6<br>Months': { balance: 0, rate: 0 },
                            '0-3<br>Months': { balance: 0, rate: 0 }
                        },
                        liabData: {
                            'Over 60<br>Months': { balance: 0, rate: 0 },
                            '37-60<br>Months': { balance: 0, rate: 0 },
                            '25-36<br>Months': { balance: 0, rate: 0 },
                            '13-24<br>Months': { balance: 0, rate: 0 },
                            '7-12<br>Months': { balance: 0, rate: 0 },
                            '4-6<br>Months': { balance: 0, rate: 0 },
                            '0-3<br>Months': { balance: 0, rate: 0 }
                        },
                        scenarioName: scenLabel,
                        origScen: self.fundingMatrixViewData().scenNames[d]
                    });

                    //Populate the dth matrix
                    var dtCount = 0;
                    for (var dr in self.fundingMatrixViewData().dataTables[d]) {

                        var ad = 'liabData';

                        if (self.fundingMatrixViewData().dataTables[d][dr].isAsset) {
                            ad = 'assetData';
                        }

                        if (data[d][ad][self.fundingMatrixViewData().dataTables[d][dr].monthGrouping] == undefined) {
                            data[d][ad][self.fundingMatrixViewData().dataTables[d][dr].monthGrouping] = { balance: 0, rate: 0 };
                        }


                        data[d][ad][self.fundingMatrixViewData().dataTables[d][dr].monthGrouping].balance = self.fundingMatrixViewData().dataTables[d][dr].balance;
                        data[d][ad][self.fundingMatrixViewData().dataTables[d][dr].monthGrouping].rate = self.fundingMatrixViewData().dataTables[d][dr].rate;
                        dtCount++;
                    }
                }

                self.data = data;


            });
        };
    };

    return fundingMatrixVm;

});