﻿define([
    'services/globalcontext',
    'services/global'
],
    function (globalContext, global) {
        var basicSurplusReportVm = function (basicSurplusReport) {
            var self = this;
            var curPage = "BasicSurlusReportConfig";
            this.institutions = ko.observableArray();
            this.basicSurplusAdminOffsets = ko.observableArray();
            this.asOfDateOffsets = ko.observableArray();
            this.policyOffsets = ko.observableArray();
            //Report
            this.basicSurplusReport = basicSurplusReport;

            //Vars
            this.basicSurplusAdmins = ko.observableArray();

            //Navigation
            this.cancel = function () { };

            this.updateBasicSurplus = function () {

                if (!self.basicSurplusReport().overrideBasicSurplus()) {
                    //find offset among policies and see what we can find
                    if (self.policyOffsets()[0][self.asOfDateOffsets()[self.basicSurplusReport().asOfDateOffset()]['asOfDateDescript']] != undefined) {
                        self.basicSurplusReport().basicSurplusAdminId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[self.basicSurplusReport().asOfDateOffset()]['asOfDateDescript']][0]["bsId"]));
                    }
                }
            }

            //Data Handling
            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage);
                global.loadingReport(true);
                return Q.all([
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.basicSurplusReport().institutionDatabaseName()),
                    globalContext.getAvailableBanks(self.institutions),
                    globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, self.basicSurplusReport().institutionDatabaseName()),
                    globalContext.getPolicyOffsets(self.policyOffsets, self.basicSurplusReport().institutionDatabaseName())
                ]).then(function () {
                    self.updateBasicSurplus();

                    if (self.basicSurplusReport().id() > 0) {
                        globalContext.saveChangesQuiet();
                    }
                    //add subscriptiosjn for as of date off set, override checkbox, inst
                    self.ovrSub = self.basicSurplusReport().overrideBasicSurplus.subscribe(function (newValue) {
                        self.updateBasicSurplus();
                    });

                    self.dateSub = self.basicSurplusReport().asOfDateOffset.subscribe(function (newValue) {
                        self.updateBasicSurplus();
                    });

                    self.instSub = self.basicSurplusReport().institutionDatabaseName.subscribe(function (newValue) {
                        return Q.all([
                            globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.basicSurplusReport().institutionDatabaseName()),
                            globalContext.getAvailableBanks(self.institutions),
                            globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, self.basicSurplusReport().institutionDatabaseName()),
                            globalContext.getPolicyOffsets(self.policyOffsets, self.basicSurplusReport().institutionDatabaseName())

                        ]).then(function () {
                            self.updateBasicSurplus();
                        });
                    });
                    global.loadingReport(false);

                });
            };

            this.detached = function (isClose) {
                self.dateSub.dispose();
                self.ovrSub.dispose();
                self.instSub.dispose();
            };

            return this;
        };

        return basicSurplusReportVm;
    });
