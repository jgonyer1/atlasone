﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var coreFundingUtilizationVm = function (id) {
        var self = this;
        var curPage = "CoreFundingtUtilizationView";
        this.cfuViewData = ko.observable();
        this.tableRows = '';
        this.chartValues = [];
        this.orderedCats = [];
        function templateScenarioCats() {
            var obj = {};
            if (self.cfuViewData().orderTable.length == 0) {
                obj = {
                    'Investments': { volume: 0, isAsset: true, excess: 0 },
                    'Loans': { volume: 0, isAsset: true, excess: 0 },
                    'Other': { volume: 0, isAsset: true, excess: 0 },
                    'Off Balance Sheet': { volume: 0, isAsset: true, excess: 0 },
                };
            } else {
                for (var i = 0; i < self.cfuViewData().orderTable.length; i++) {
                    for (var j = 0; j < self.cfuViewData().orderTable.length; j++) {
                        if (self.cfuViewData().orderTable[j].priority == i) {
                            obj[self.cfuViewData().orderTable[i].name] = { volume: 0, isAsset: true, excess: 0 };
                        }
                    }

                }
            }

            obj['Equity'] = { volume: 0, isAsset: false, excess: 0 };
            obj['Other Liabilities'] = { volume: 0, isAsset: false, excess: 0 };
            obj['DDA'] = { volume: 0, isAsset: false, excess: 0 };
            obj['NOW'] = { volume: 0, isAsset: false, excess: 0 };
            obj['Savings'] = { volume: 0, isAsset: false, excess: 0 };
            obj['Other Deposits'] = { volume: 0, isAsset: false, excess: 0 };
            obj['Borrowings'] = { volume: 0, isAsset: false, excess: 0 };


            return obj;
        }

        function createHashTable(topTable, historicalTable) {
            var o = {};
            var topTableAods = [];
            for (var r in topTable) {
                var record = topTable[r],
                    aod = record.asOfDate,
                    scenarioName = record.scenarioName,
                    category = record.name,
                    balance = record.balance;

                if (topTableAods.indexOf(aod) == -1) {
                    topTableAods.push(aod);
                }
                if (scenarioName.indexOf('Base') > -1) {
                    scenarioName = 'Base';
                }

                if (category.indexOf('Liability') > -1) {
                    category = 'Other Liabilities';
                }
                if (self.cfuViewData().assetDetails.indexOf('dg') == -1 && (category.indexOf('Investment') > -1 || category.indexOf('Loan') > -1)) {
                    category += 's';
                }

                if (self.orderedCats.indexOf(category) == -1) {
                    self.orderedCats.push(category);
                }

                if (o[aod] == undefined) {
                    o[aod] = {};
                }
                if (o[aod][scenarioName] == undefined) {
                    o[aod][scenarioName] = {};
                    //o[aod][scenarioName] = new templateScenarioCats();
                }
                if (o[aod][scenarioName][category] == undefined) {
                    o[aod][scenarioName][category] = { volume: 0, isAsset: record.isAsset, excess: 0 }
                }

                o[aod][scenarioName][category].volume += balance;
            }
            for (var r in historicalTable) {
                var record = historicalTable[r],
                    aod = record.asOfDate,
                    scenarioName = record.scenarioName,
                    category = record.name,
                    balance = record.balance;

                if (scenarioName.indexOf('Base') > -1) {
                    scenarioName = 'Base';
                }
                if (category.indexOf('Liability') > -1) {
                    category = 'Other Liabilities';
                }
                if (self.cfuViewData().assetDetails.indexOf('dg') == -1 && (category.indexOf('Investment') > -1 || category.indexOf('Loan') > -1)) {
                    category += 's';
                }

                //we only want to do this if the topTable does not have this aod
                if (topTableAods.indexOf(aod) == -1) {
                    if (o[aod] == undefined) {
                        o[aod] = {};
                    }
                    if (o[aod][scenarioName] == undefined) {
                        o[aod][scenarioName] = {};
                        //o[aod][scenarioName] = new templateScenarioCats();
                    }
                    if (o[aod][scenarioName][category] == undefined) {
                        o[aod][scenarioName][category] = { volume: 0, isAsset: record.isAsset, excess: 0 };
                    }
                    o[aod][scenarioName][category].volume += balance;
                }

            }
            return o;
        }

        function getAssets(hashTable) {
            var assets = {};
            for (var aod in hashTable) {
                if (assets[aod] == undefined) {
                    assets[aod] = {};
                }
                for (var scenario in hashTable[aod]) {
                    assets[aod][scenario] = { totalAssets: 0 };
                    for (var cat in hashTable[aod][scenario]) {

                        if (hashTable[aod][scenario][cat].isAsset) {
                            if (assets[aod][scenario][cat] == undefined) {
                                assets[aod][scenario][cat] = 0;
                            }
                            assets[aod][scenario][cat] += hashTable[aod][scenario][cat].volume;
                            assets[aod][scenario].totalAssets += hashTable[aod][scenario][cat].volume;
                        }
                    }
                }
            }
            return assets;
        }
        function getLiabilities(hashTable) {
            var liabs = {};
            for (var aod in hashTable) {
                if (liabs[aod] == undefined) {
                    liabs[aod] = {};
                }
                for (var scenario in hashTable[aod]) {
                    if (liabs[aod][scenario] == undefined) {
                        liabs[aod][scenario] = { totalLiabilities: 0 };
                    }
                    for (var cat in hashTable[aod][scenario]) {
                        if (!hashTable[aod][scenario][cat].isAsset) {
                            if (liabs[aod][scenario][cat] == undefined) {
                                liabs[aod][scenario][cat] = 0;
                            }
                            liabs[aod][scenario][cat] += hashTable[aod][scenario][cat].volume;
                            liabs[aod][scenario].totalLiabilities += hashTable[aod][scenario][cat].volume;
                        }
                    }

                }
            }
            return liabs;
        }

        function bubbleChartObj() {
            this.chart = {
                type: 'bubble',
                zoomType: 'xy'
            };
            this.title = {
                text:
                ''
            };
            this.xAxis = {
                categories: [],
                labels: {
                    style: {
                        'font-weight': 'bold'
                    }
                }
            };
            this.plotOptions = {
                series: {
                    animation: false
                },
                bubble: {
                    dataLabels: {
                        enabled: false
                    },
                    minSize: 30,
                    maxSize: 30
                }
            };
            this.legend = {
                labelFormatter: function () {
                    return '<span style="font-weight: bold; vertical-align: top; top: -2px; color: ' + this.color + ';">' + this.name + '</span>'
                },
                style: {
                    'top': '-2px'
                },
                borderWidth: 0,
                borderRadius: 0
            };
            this.series = [];
            this.tooltip = {
                formatter: function () {
                    return '<span style="color: ' + this.series.color + '">' + this.point.series.name + '</span><br>' + this.point.category + ': ' + this.y + '%';
                }
            };
        }

        function barChartObj(yMin, yMax) {
            return {
                chart: {
                    type: 'bar'
                },
                plotOptions: {
                    series: {
                        animation: false,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<span>' + numeral(this.y / 1000).format('0,0') + '</span>';
                            },
                            crop: false,
                            overflow: 'none'
                        },
                        borderColor: 'rgb(96,96,96)',
                        borderWidth: 2
                    }
                },
                title: { text: '' },
                xAxis: {
                    categories: [],
                    lineColor: '#FFFFFF',
                    lineWidth: 0,
                    gridLineColor: 'transparent',
                    minorGridLineWidth: 0,
                    minorTickColor: '#FFFFFF',
                    minorTickLength: 0,
                    tickLength: 0,
                    tickAmount: 0,
                    labels: {
                        useHTML: true,
                        enabled: true,
                        style: {
                            'font-weight': 'bold'
                        }
                    }

                },
                yAxis: {
                    min: yMin,
                    startOnTick: false,
                    max: yMax,
                    //tickAmount: 3,
                    tickColor: '#ffffff',
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    title: {
                        text: ''
                    },
                    labels: {
                        enabled: false
                    }
                },
                legend: {
                    reversed: true,
                    labelFormatter: function () {
                        return '<span style="font-weight: bold; color: ' + this.color + ';">' + this.name + '</span>'
                    },
                    borderWidth: 0,
                    borderRadius: 0
                },
                tooltip: {
                    formatter: function () {
                        return this.series.name + '<br>' + numeral(this.y / 1000).format('0,0');
                    }
                },
                series: []
            };

        }

        function createTables(hashTable, assets, liabilities, orderedScenarios, policyLimits, historicalGraphing) {
            var topTable = '';
            var historicalTable = '';
            var aods = self.cfuViewData().orderedDates//Object.keys(hashTable);
            var colorClass = '';
            var headerFlag = true;
            var assetRows = '';
            var liabRows = '';
            var cat = "";
            var riskAssesmentColors = ['#FFFFFF',
            '#009933', '#2BAA2B',
            '#80CC1A', '#D5EE09',
            '#FFFF00', '#FFEA00',
            '#FFC000', '#FF7F00',
            '#FF4000', '#FF0000',
            '#FFFFFF'];

            var riskFontColors = ['rgb(96,96,96)',
            '#FFFFFF', '#FFFFFF', '#FFFFFF',
            'rgb(96,96,96)', 'rgb(96,96,96)', 'rgb(96,96,96)', 'rgb(96,96,96)',
            '#FFFFFF', '#FFFFFF', '#FFFFFF'];

            var cumExcess = {};
            for (var a = 0; a < Object.keys(hashTable).length; a++) {
                var aod = Object.keys(hashTable)[a];//asOfDates[a];
                cumExcess[aod] = {};
                for (var s = 0; s < orderedScenarios.length; s++) {
                    var scen = orderedScenarios[s];
                    var loweredScen = scen.substring(0, 1).toLowerCase() + scen.substring(1);
                    if (assets[aod][scen]) {
                        cumExcess[aod][scen] = assets[aod][scen].totalAssets * -1;
                    }
                    
                    
                }
            }

            var chartHolderCells = [
                { attributes: { 'class': 'left-chart-label' }, text: '<div>%&nbsp;UTILIZATION</div>' }
            ];

            var headerCells = [
                { attributes: {}, text: '' }
            ];
            var totalAssetsCells = [
                { attributes: { "class": "bold-text tot-assets" }, text: 'Assets > 60 Months' }
            ];

            var altHeaderCells = [
                { attributes: { 'class': 'strong-text' }, text: 'Core Liabilities ' }
            ];
            var altHeaderCells2 = [
                { attributes: { 'class': 'tot-assets bold-text' }, text: 'Core Funding' }
            ];
            var totalLiabsCells = [
                { attributes: { 'class': 'bold-text tot-assets' }, text: 'Total Core Funding' }
            ];
            //var utilizationCells = [
            //    { attributes: { }, text: '' }
            //];
            var bottomTableheaderRow_top = [
                            { attributes: {}, text: '' }
            ];

            var bottomTableheaderRow = [
                { attributes: {}, text: '' }
            ];

            //create the top table for the main as of date
            aod = self.cfuViewData().mainAsOfDate;
            var catCount = 0;
            var isZeroLineItem = true;
            for (var c = 0; c < self.orderedCats.length; c++) {
                isZeroLineItem = true;
                cat = self.orderedCats[c];
                cellCollection = [];
                var labelFlag = true;
                for (var j = 0; j < orderedScenarios.length; j++) {
                    var scenario = orderedScenarios[j];
                    scenarioColor = charting.scenarioIdLookUp(scenario).seriesColor;
                    if (j < orderedScenarios.length - 1) {
                        addSpacerCell = true;
                    } else {
                        addSpacerCell = false;
                    }
                    //colorClass = j % 2 == 0 ? 'blue-bg' : 'green-bg';
                    colorClass = '';
                    var excess = 0;

                    if (hashTable[aod][scenario] != undefined && !hashTable[aod][scenario][cat].isAsset) {
                        //keep track of the cumulative excess for the scenario
                        var excess = cumExcess[aod][scenario] + hashTable[aod][scenario][cat].volume;
                        cumExcess[aod][scenario] = excess;
                    }

                    //build row
                    //if (numeral(numeral(hashTable[aod][scenario][cat].volume / 1000).format('0,0')).value() != 0) {
                    if (true) {
                        isZeroLineItem = false;
                        if (labelFlag) {
                            if (hashTable[aod][scenario][cat].isAsset) {
                                cellCollection.push({
                                    attributes: { 'class': 'left-indent' }, text: cat
                                });
                            } else {
                                cellCollection.push({
                                    attributes: { 'class': 'left-indent' }, text: cat
                                });
                            }

                            labelFlag = false;
                        }

                        //if last category, need underline on Vol number
                        //if (catCount == Object.keys(hashTable[aod][orderedScenarios[0]]).length - 1) {

                        cellCollection.push({
                            attributes: {
                                //'class': colorClass + ' text-center ' + (j == 0 ? '' : 'vol-cell') + (cat.indexOf('Other Deposits') > -1 ? ' other-dep' : ''),
                                'class': colorClass + ' text-center ' + (j == 0 ? '' : 'vol-cell') + (catCount == Object.keys(hashTable[aod][orderedScenarios[0]]).length - 1 ? ' other-dep' : ''),
                                'style': 'border-left: 2px solid ' + scenarioColor + ';' + (hashTable[aod][scenario][cat].isAsset ? 'border-right: 2px solid' + scenarioColor + '; text-align: center;' : ''),
                                'colspan': (hashTable[aod][scenario][cat].isAsset ? '3' : '1')
                            }, text: numeral(hashTable[aod][scenario][cat].volume / 1000).format('0,0')
                        });

                        //if (catCount == Object.keys(hashTable[aod][orderedScenarios[0]]).length - 1) {
                        if (!hashTable[aod][scenario][cat].isAsset) {
                            cellCollection.push(
                                {
                                    attributes: {
                                        'class': colorClass + ' text-center' + (j == 1 ? '' : ' exc-cell'), 'colspan': '2'
                                        , 'style': 'border-right: 2px solid ' + scenarioColor,
                                        //'spanBorderStyle': (cat.indexOf('Other Deposits') > -1 ? 'border: 2px solid ' + scenarioColor : '') + '; padding-left: 10px; padding-right: 10px;'
                                        'spanBorderStyle': (catCount == Object.keys(hashTable[aod][orderedScenarios[0]]).length - 1 ? 'border: 2px solid ' + scenarioColor : '') + '; padding-left: 10px; padding-right: 10px;'

                                    },
                                    text: (excess > 0 ? numeral(excess / 1000).format('0,0') : ''),

                                }
                            );
                        }

                        if (addSpacerCell) {
                            cellCollection.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }
                    }
                    

                    //add to the header row, alt header, and util rows
                    if (headerFlag) {
                        chartHolderCells.push({
                            attributes: { 'colspan': '3', 'class': 'chart-holder-' + orderedScenarios.length, 'id': 'chart_holder_' + j }, 'text': ''
                        });

                        if (addSpacerCell) {
                            chartHolderCells.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }

                        if (scenario.toUpperCase().indexOf("DOWN") > -1) {
                            headerCells.push(
                                {
                                    attributes: {
                                        'colspan': '3', 'class': colorClass + ' text-center bottom-scenario-header bold-text',
                                        'style': 'background-color: ' + scenarioColor + '; border-left: 2px solid ' + scenarioColor + '; border-right: 2px solid ' + scenarioColor
                                    },
                                    text: "<span class='fa fa-arrow-down'></span> " + scenario
                                }
                            );
                        } else if (scenario.toUpperCase().indexOf("UP") > -1) {
                            headerCells.push(
                                {
                                    attributes: {
                                        'colspan': '3', 'class': colorClass + ' text-center bottom-scenario-header bold-text',
                                        'style': 'background-color: ' + scenarioColor + '; border-left: 2px solid ' + scenarioColor + '; border-right: 2px solid ' + scenarioColor
                                    },
                                    text: "<span class='fa fa-arrow-up'></span> " + scenario
                                }
                            );
                        } else {
                            headerCells.push(
                                {
                                    attributes: {
                                        'colspan': '3', 'class': colorClass + ' text-center bottom-scenario-header bold-text',
                                        'style': 'background-color: ' + scenarioColor + '; border-left: 2px solid ' + scenarioColor + '; border-right: 2px solid ' + scenarioColor
                                    },
                                    text: scenario
                                }
                            );
                        }


                        if (addSpacerCell) {
                            headerCells.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }

                        totalAssetsCells.push(
                            {
                                attributes: {
                                    'class': colorClass + ' text-center bold-text tot-assets', 'colspan': '3',
                                    'style': 'border-left: 2px solid ' + scenarioColor + '; border-right: 2px solid ' + scenarioColor
                                }, text: numeral(assets[aod][scenario].totalAssets / 1000).format('0,0')
                            }
                        );

                        if (addSpacerCell) {
                            totalAssetsCells.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }

                        self.chartValues.push([
                            {
                                color: charting.scenarioIdLookUp(scenario).seriesColor,
                                y: assets[aod][scenario] != undefined && liabilities[aod][scenario] ? Math.round((assets[aod][scenario].totalAssets / liabilities[aod][scenario].totalLiabilities) * 100) : 0,
                                dataLabels: {
                                    distance: -40,
                                    inside: true
                                }
                            },
                            {
                                color: "#ffffff",
                                y: assets[aod][scenario] != undefined && liabilities[aod][scenario] ?  (100 - Math.round((assets[aod][scenario].totalAssets / liabilities[aod][scenario].totalLiabilities) * 100)) : 0,
                                dataLabels: {
                                    enabled: false
                                }
                            }

                        ]);

                        altHeaderCells.push(
                            { attributes: { 'class': colorClass }, text: '' }
                        );
                        altHeaderCells.push(
                            { attributes: { 'class': ' text-center' }, text: 'Cumulative' }
                        );

                        if (addSpacerCell) {
                            altHeaderCells.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }

                        altHeaderCells2.push(
                            { attributes: { 'class': colorClass + ' alt-header text-center bold-text', 'style': 'border-left: 2px solid ' + scenarioColor }, text: 'Volume' }
                        );
                        altHeaderCells2.push(
                            { attributes: { 'class': colorClass + ' alt-header text-center bold-text exc-cell', 'colspan': '2', 'style': 'border-right: 2px solid ' + scenarioColor }, text: 'Excess' }
                        );

                        if (addSpacerCell) {
                            altHeaderCells2.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }

                        totalLiabsCells.push({
                            attributes: { 'class': colorClass + ' text-center bold-text tot-assets', 'style': 'text-align: right; border-left: 2px solid ' + scenarioColor }, text: liabilities[aod][scenario] != undefined ? numeral(liabilities[aod][scenario].totalLiabilities / 1000).format('0,0') : ""
                        });
                        totalLiabsCells.push(
                            { attributes: { 'class': colorClass }, text: '' }
                        );
                        totalLiabsCells.push(
                            { attributes: { 'class': colorClass, 'style': 'border-right: 2px solid ' + scenarioColor }, text: '' }
                        );

                        if (addSpacerCell) {
                            totalLiabsCells.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }

                    }

                }
                if (headerFlag) {
                    topTable += row(chartHolderCells);
                    topTable += '<td style="padding-top: 45px" colspan="' + (1 + orderedScenarios.length * 3 + orderedScenarios.length - 1) + '"></td>'
                    topTable += row(headerCells);
                    assetRows += row(totalAssetsCells);
                }

                if (hashTable[aod][orderedScenarios[0]][cat].isAsset) {
                    var assetDetailNum = parseInt(self.cfuViewData().assetDetails.replace("dg_"));
                    if ((self.cfuViewData().assetDetails == '1' || self.cfuViewData().assetDetails.indexOf("dg") > -1) && assets[aod][orderedScenarios[0]][cat] != 0) {
                        assetRows += row(cellCollection);
                    }
                //} else if(!isZeroLineItem) {
                } else if (true) {
                    liabRows += row(cellCollection, true);
                }
                catCount++;
                headerFlag = false;

            }
            topTable += assetRows;
            //topTable += row(altHeaderCells);
            topTable += row(altHeaderCells2);
            topTable += liabRows;
            topTable += row(totalLiabsCells);

            //historical table
            //for (var i = 0; i < Object.keys(hashTable).length; i++) {
            for (var i = 0; i < aods.length; i++) {
                var aod = aods[i];//aods[i];
                var polAOD = moment(aod).format('M/DD/YYYY') + ' 12:00:00 AM';
                var cellCollection = [];
                catCount = 0;
                var addSpacerCell = true;
                var scenarioColor = "";

                colorClass = '';


                cellCollection = [{
                    attributes: { 'class': colorClass + " bold-text" }, text: moment(aod).format('MMM-YY')
                }];
                var secondCellCollection = [{
                    attributes: { 'colspan': '2', 'class': colorClass + ' left-indent' }, text: 'Assets >60M'
                }];


                for (var s = 0; s < orderedScenarios.length; s++) {
                    var scenario = orderedScenarios[s];
                    scenarioColor = charting.scenarioIdLookUp(scenario).seriesColor;
                    var loweredScenario = scenario.substring(0, 1).toLowerCase() + scenario.substring(1);
                    var assetVal = assets[aod][scenario] != undefined ? assets[aod][scenario].totalAssets : 0;
                    var liabsVal = liabilities[aod][scenario] != undefined ? liabilities[aod][scenario].totalLiabilities : 0;
                    var util = assetVal / liabsVal;
                    var curAssets = assets[aods[0]][scenario].totalAssets;
                    var curUtil = liabilities[aods[0]][scenario] != undefined ?( curAssets / liabilities[aods[0]][scenario].totalLiabilities) : 0;
                    var changeAssets = curAssets - assetVal;
                    var changeUtil = curUtil - util;
                    var riskColor = '';
                    var riskFontColor = ''
                    var classStr = colorClass + ' text-center';

                    if (self.showPolicies() && policyLimits[loweredScenario] != undefined && policyLimits[loweredScenario][polAOD] != undefined && policyLimits[loweredScenario][polAOD][0].riskAssessment != undefined && policyLimits[loweredScenario][polAOD][0].riskAssessment > -900) {
                        riskColor = riskAssesmentColors[policyLimits[loweredScenario] && policyLimits[loweredScenario][polAOD] && policyLimits[loweredScenario][polAOD][0].riskAssessment];
                        riskFontColor = riskFontColors[policyLimits[loweredScenario] && policyLimits[loweredScenario][polAOD] && policyLimits[loweredScenario][polAOD][0].riskAssessment];
                    }

                    var classStr = colorClass + ' text-center';

                    //attributes: { 'class': colorClass + ' right-aligned' }, text: numeral(assets[aod][scenario].totalAssets / 1000).format('0,0')

                    //historic table total assets

                    //if we're on the last asofdate we need to put a bottom border on too
                    //if (i == Object.keys(hashTable).length - 1) {
                    if (i == aods.length - 1) {
                        cellCollection.push({
                            attributes: { 'class': classStr, 'style': 'border-left: 2px solid ' + scenarioColor + '; border-bottom: 2px solid ' + scenarioColor + '; border-bottom-left-radius: 8px' }, text: assets[aod][scenario] != undefined ? numeral(assets[aod][scenario].totalAssets / 1000).format('0,0') : ""
                        });
                    } else {
                        cellCollection.push({
                            attributes: { 'class': classStr, 'style': 'border-left: 2px solid ' + scenarioColor }, text: assets[aod][scenario] != undefined ? numeral(assets[aod][scenario].totalAssets / 1000).format('0,0'): ""
                        });
                    }

                    //if (i == Object.keys(hashTable).length - 1) {
                    if (i == aods.length - 1) {
                        //historical core funding
                        cellCollection.push({
                            attributes: { 'style': 'border-bottom: 2px solid ' + scenarioColor }, text: liabilities[aod][scenario] != undefined ? numeral(liabsVal / 1000).format('0,0'): ""
                        });
                    } else {
                        //historical core funding
                        cellCollection.push({
                            attributes: {}, text: liabilities[aod][scenario] != undefined ? numeral(liabsVal / 1000).format('0,0'): ""
                        });
                    }

                    //if (i == Object.keys(hashTable).length - 1) {
                    if (i == aods.length - 1) {
                        //historical perc of utilization
                        cellCollection.push({
                            attributes: { 'class': classStr + (riskColor.length > 0 ? ' risk-assess' : ''), 'style': 'border-right: 2px solid ' + scenarioColor + '; border-bottom: 2px solid ' + scenarioColor + '; border-bottom-right-radius: 8px' },
                            text: assets[aod][scenario] != undefined && liabilities[aod][scenario] != undefined ? numeral(util).format('0%'): ""
                        });
                    } else {
                        //historical perc of utilization
                        cellCollection.push({
                            attributes: { 'class': classStr + (riskColor.length > 0 ? ' risk-assess' : ''), 'style': 'border-right: 2px solid ' + scenarioColor }, text: assets[aod][scenario] != undefined && liabilities[aod][scenario] != undefined ? numeral(util).format('0%') : ""
                        });
                    }

                    if (s < orderedScenarios.length - 1) {
                        cellCollection.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                    }

                    secondCellCollection.push({
                        attributes: { 'class': classStr }, text: numeral(assetVal / 1000).format('0,0')
                    });
                    secondCellCollection.push({
                        attributes: { 'class': classStr }, text: numeral(changeAssets / 1000).format('0,0')
                    });

                    if (s < orderedScenarios.length - 1) {
                        secondCellCollection.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                    }

                    if (i == 0) {
                        bottomTableheaderRow.push({
                            attributes: { 'class': 'bold-text thick-bottom-border alt-header text-center', 'style': 'vertical-align: bottom;border-left: 2px solid' + scenarioColor + '; padding: 1px 0px 0px 5px;' }, text: '<div class="bot-tbl-head" style="padding-right: 5px;">Assets >60M</div>'
                        });
                        bottomTableheaderRow.push({
                            attributes: { 'class': 'bold-text thick-bottom-border alt-header text-center', 'style': 'padding: 0;' }, text: '<div class="bot-tbl-head" style="padding-right: 5px;">Core</br>Funding</div>'
                        });
                        bottomTableheaderRow.push({
                            attributes: { 'class': 'bold-text thick-bottom-border alt-header text-center', 'style': 'vertical-align:bottom; border-right: 2px solid ' + scenarioColor + '; padding: 1px 5px 0px 0px ' }, text: '<div class="bot-tbl-head" >% Util</div>'
                        });
                        if (s < orderedScenarios.length - 1) {
                            bottomTableheaderRow.push({ attributes: { 'class': 'spacer-cell' }, 'text': '' });
                        }
                    }
                }
                if (i == 0) {
                    //topTable += row(bottomTableheaderRow_top);
                    topTable += row(bottomTableheaderRow);
                }
                topTable += row(cellCollection);
            }

            return topTable;

        }



        function createChartObjects(assetsObj, liabsObj, orderedScenarios, aod, policyLimits) {
            var scenLen = orderedScenarios.length;

            var polAOD = moment(aod).format('M/DD/YYYY') + ' 12:00:00 AM';

            var maxLiabs = 0;
            var min = 0;
            var max = 0;

            for (var scen in liabsObj[aod]) {
                var loweredScen = scen.substring(0, 1).toLowerCase() + scen.substring(1);
                var liabVal;
                if (self.showPolicies() && policyLimits[loweredScen] != undefined && policyLimits[loweredScen][polAOD] != undefined && policyLimits[loweredScen][polAOD][0].policyLimit != undefined && policyLimits[loweredScen][polAOD][0].policyLimit > -900) {
                    liabVal = liabsObj[aod][scen].totalLiabilities * (policyLimits[loweredScen][polAOD][0].policyLimit / 100);
                } else {
                    liabVal = liabsObj[aod][scen].totalLiabilities;
                }
                if (liabVal > maxLiabs) {
                    maxLiabs = liabVal;
                }
                if (liabVal < min) {
                    min = liabVal;
                }
                if (maxLiabs > max) {
                    max = maxLiabs
                }
                if (min == 0) {
                    min = max * -0.009
                }
            }

            var chrt = barChartObj(min, maxLiabs);


            var chartCategories = [];
            var assetBars = {
                name: "Assets > 60 Months",
                data: [],
                //color: charting.chartColors.blue
            };
            var remCFBars = {
                name: "Remaining Core Funding",
                data: [],
                //color: charting.chartColors.green
            };

            for (var s = 0; s < scenLen; s++) {
                var scenario = orderedScenarios[s];
                var loweredScen = scenario.substring(0, 1).toLowerCase() + scenario.substring(1);
                var remCF;
                if (self.showPolicies() && policyLimits[loweredScen] != undefined && policyLimits[loweredScen][polAOD] != undefined && policyLimits[loweredScen][polAOD][0].policyLimit != undefined && policyLimits[loweredScen][polAOD][0].policyLimit > -900) {
                    remCF = ((liabsObj[aod][scenario]['totalLiabilities'] * (policyLimits[loweredScen][polAOD][0].policyLimit / 100)) - assetsObj[aod][scenario]['totalAssets']);
                } else {
                    remCF = liabsObj[aod][scenario]['totalLiabilities'] - assetsObj[aod][scenario]['totalAssets'];
                }

                if (remCF < min) {
                    min = remCF;
                }
                if (remCF > max) {
                    max = remCF;
                }
                if (assetsObj[aod][scenario].totalAssets < min) {
                    min = assetsObj[aod][scenario].totalAssets;
                }
                if (assetsObj[aod][scenario].totalAssets > max) {
                    max = assetsObj[aod][scenario].totalAssets;
                }



                chartCategories.push(scenario);

                remCFBars.data.push(remCF);
                assetBars.data.push(assetsObj[aod][scenario].totalAssets);

            }
            if (min < 0) {
                chrt.yAxis.min = min + (min * 0.04);
            }
            chrt.yAxis.min = min + (min * 0.04);
            chrt.yAxis.max = max;

            chrt.xAxis.categories = chartCategories;

            chrt.series.push(remCFBars);
            chrt.series.push(assetBars);



            return chrt;

        }

        //cell takes a collection of attributes and the text to put in the cell
        function cell(attributes, text) {
            var c = "<td ";
            var attrs = [];
            var needsSpan = false;
            var isOtherDep = false;
            var isAltHeader = false;
            var isOtherDepExcess = false; //WOAHHHHHHH WATCH OUT
            var borderSpanStyle = "style='";
            for (var atr in attributes) {
                var value = attributes[atr];
                if (value.indexOf("alt-header") > -1 || value.indexOf("other-dep") > -1 || atr == "spanBorderStyle") {
                    needsSpan = true;

                    if (value.indexOf("alt-header") > -1) {
                        isAltHeader = true;
                    }
                    if (value.indexOf("other-dep") > -1) {
                        isOtherDep = true;
                    }
                    if (atr == "spanBorderStyle" && value.indexOf("border") >= 0) {
                        isOtherDepExcess = true;
                      
                    }
                    if (atr == "spanBorderStyle") {
                        borderSpanStyle += value;
                    }
                   

                }
                attrs.push(atr.replace("_", "") + ' = "' + value + '"');
            }

            c += attrs.join(" ");
            c += ">";

            if (text == 'Excess') {
                borderSpanStyle += "padding-right: 10px;"
            }
            borderSpanStyle += "'";
            if (needsSpan) {
                c += "<span " + borderSpanStyle + " class='";

                if (isAltHeader) {
                    c += 'alt-header bigfont';
                }
                if (isOtherDep) {
                    c += ' other-dep'
                }

                if (isOtherDepExcess) {
                    c += 'bigfont bold-text';
                }
              

                c += "'>";
            }

            c += text;

            if (isAltHeader) {
                c += "</span>";
            }

            c += "</td>";

            return c;
        }

        //row takes array of cells, cell is an object containing properties attribute and text
        function row(cells, isExcessRow) {
            var r = "<tr>";
            if (isExcessRow) {
                r = "<tr class='excess-row'>"
            }
            cells.forEach(function (element, index, array) {
                r += cell(element.attributes, element.text);
            }, this);

            r += "</tr>";

            return r;
        }
        var innerPadding = 4;
        function realignLabels(serie) {
            var chart = serie.chart;
            var borderWidth = chart.options.plotOptions.bar.borderWidth;

            $.each(serie.points, function (j, point) {
                if (!point.dataLabel) return true;

                var yCoord = point.dataLabel.y;
                var color = '#ffffff';

                if (point.shapeArgs.height <= point.dataLabel.width + (innerPadding * 2)) {
                    yCoord += (point.shapeArgs.width / 2) + point.dataLabel.height / 2;
                    color = 'rgb(96,96,96)';
                }
                point.dataLabel.attr({
                    y: yCoord
                }).css({
                    color: color,
                    //fontSize: '13px'
                });
            });
        }
        this.oldDataLabels;
        this.compositionComplete = function () {
            if (self.cfuViewData().errors.length == 0 && self.cfuViewData().warnings.length == 0) {
                var chartRow = '<tr><td colspan="2"></td></tr>',
                    scenariolen = self.cfuViewData().scenarios.length,
                    spacerNum = 0;
                var chartTitleIcon;
                var scenarioChartData;
                var scenarioNameLowerFirst;
                var aod = self.cfuViewData().mainAsOfDate;
                var polAOD = moment(aod).format('M/DD/YYYY') + ' 12:00:00 AM';
                var policyLabel = "";
                document.getElementById("test_table_body").innerHTML = self.testTable;
                for (var i = 0; i < self.chartValues.length; i++) {
                    scenarioChartData = charting.scenarioIdLookUp(self.cfuViewData().scenarios[i]);
                    scenarioNameLowerFirst = self.cfuViewData().scenarios[i].substring(0, 1).toLowerCase() + self.cfuViewData().scenarios[i].substring(1, self.cfuViewData().scenarios[i].length);


                    if (self.cfuViewData().scenarios[i].toUpperCase().indexOf("BASE") > -1) {
                        chartTitleIcon = "";
                    } else {
                        chartTitleIcon = "<span style='color: " + scenarioChartData.seriesColor + "' class='fa fa-arrow-";
                        if (self.cfuViewData().scenarios[i].toUpperCase().indexOf("UP") > -1) {
                            chartTitleIcon += "up'></span>";
                        } else if (self.cfuViewData().scenarios[i].toUpperCase().indexOf("DOWN") > -1) {
                            chartTitleIcon += "down'></span>";
                        } else {
                            chartTitleIcon = "";
                        }
                    }

                    var chObj = {
                        chart: {
                            backgroundColor: "transparent",
                            type: "pie",
                            marginBottom: 50
                        },
                        title: {
                            enabled: true,
                            style: { "color": "#000000" },
                            useHTML: true,
                            text: chartTitleIcon + " " + self.cfuViewData().scenarios[i].toUpperCase(),
                            y: 230
                        },
                        plotOptions: {
                            pie: {
                                enabled: true,
                                borderColor: "#333333",
                                allowPointSelect: false,
                                cursor: 'pointer',
                                size: "180px",

                                dataLabels: {
                                    inside: true,
                                    enabled: true,
                                    borderWidth: 0,
                                    style: {
                                        textOutline: 0
                                    },
                                    useHTML: true,
                                    //format: '{point.percentage:.0f} %'
                                    formatter: function () {
                                        return "<span style='font-size: 20px;'>" + numeral(this.y).format("0") + "%</span>"
                                    },
                                    crop: false
                                }
                            }
                        },
                        series: [{
                            colorByPoint: true,
                            data: self.chartValues[i]
                        }]
                    };
                    //var chart = $("#chart_holder_" + i).highcharts(chObj);
                    //console.log(JSON.stringify(chObj));
                    Highcharts.chart("chart_holder_" + i, chObj, function (chart) {

                        if (self.cfuViewData().showPolicies) {
                            var policyLimit = self.cfuViewData().policyLimits[scenarioNameLowerFirst][polAOD][0].policyLimit;
                            if (policyLimit != -999) {
                                var endAngle = 360 * (policyLimit / 100);
                                var endRad = endAngle * (Math.PI / 180);

                                //the way svg interprets rotation is different from highcharts start angle. In svg 0 degrees is straight out right from center
                                var polLabelAngle = (endAngle % 360) - 90;
                                if (endAngle > 180 && endAngle < 360) {
                                    polLabelAngle = ((endAngle - 180) % 360) - 90;
                                }
                                policyLabel = "POLICY = " + policyLimit + "%";

                                var cos = Math.cos(endRad);
                                var sin = Math.sin(endRad);

                                var radius = 90;

                                var centerX = chart.series[0].center[0] + chart.marginRight;
                                var centerY = chart.series[0].center[1] + chart.marginBottom;

                                var endX = Math.round((centerX + (radius * sin)) * 10) / 10;
                                var endY = Math.round((centerY - (radius * cos)) * 10) / 10;


                                //the initital coords will be ON the line
                                //later we offset them perpendicular to the policy line
                                var initTextStartX = centerX + ((radius * .15) * sin);
                                var textStartX;
                                var initTextStartY = centerY - ((radius * .15) * cos);
                                var textStartY;


                                if (endAngle > 180 && endAngle < 360) {
                                    initTextStartX = endX + ((radius * .15) * Math.sin((endAngle - 180) * (Math.PI / 180)));
                                    initTextStartY = endY - ((radius * .15) * Math.cos((endAngle - 180) * (Math.PI / 180)));
                                }

                                var lineSlope;// = (centerY - endY) / (centerX - endX);
                                var perpSlope;
                                if ((centerX - endX) == 0) {
                                    lineSlope = 0;
                                } else {
                                    lineSlope = (centerY - endY) / (centerX - endX);
                                }

                                if (lineSlope == 0) {
                                    perpSlope = 1;
                                } else {
                                    perpSlope = -1 / lineSlope;
                                }


                                if ((endAngle > 90 && endAngle <= 180) || endAngle > 270 && endAngle < 360) {
                                    textStartX = initTextStartX + 5 / (Math.sqrt(1 + Math.pow(perpSlope, 2)));
                                } else {
                                    textStartX = initTextStartX - 5 / (Math.sqrt(1 + Math.pow(perpSlope, 2)));
                                }
                                textStartY = perpSlope * (textStartX - initTextStartX) + initTextStartY;


                                var textCssObj = {
                                    fontSize: "10px",
                                    color: "#cb4154"
                                };
                                //if (policyLimit > (chart.series[0].yData[0] * 1.25) || policyLimit == 0) {
                                //    textCssObj.color = "#ff0000;";
                                //} else {
                                //    textCssObj.color = "#ff0000";
                                //}

                                chart.renderer.path(['M', centerX, centerY, 'L', endX, endY, 'z']).attr({ stroke: '#ff0000', 'stroke-width': 2, zIndex: 10 }).add();
                                chart.renderer.text(policyLabel, textStartX, textStartY).attr({ rotation: polLabelAngle, zIndex: 10 }).css(textCssObj).add();
                            }
                        }
                    });

                }
                var colChart = $('#chart_container').highcharts();
                $('#bubChart').highcharts(self.bubbleChart);
                global.loadingReport(false);
                $('#reportLoaded').text('1');
                if (typeof hiqPdfInfo == "undefined") {
                }
                else {
                    $('div.report-view').css('padding-bottom', '0');
                    $('#testTable').css('line-height', '1.25');
                    $("#corefunding-view").height($("#corefunding-view").height());
                    hiqPdfConverter.startConversion();
                }
            }
            
        }



        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getCoreFundingUtilizationViewDataById(id, self.cfuViewData).then(function () {
                globalContext.reportErrors(self.cfuViewData().errors, self.cfuViewData().warnings);
                if (self.cfuViewData().errors.length == 0 && self.cfuViewData().warnings.length == 0) {
                    self.showPolicies = ko.observable(self.cfuViewData().showPolicies);
                    var hashTable = createHashTable(self.cfuViewData().topTable, self.cfuViewData().historicalTable, self.cfuViewData().scenarios);
                    var assets = getAssets(hashTable);
                    var liabs = getLiabilities(hashTable);
                    var rows = '';
                    var modifiedHistoricalGraph = {};
                    for (var date in self.cfuViewData().historicalGraphing) {
                        modifiedHistoricalGraph[moment(date).format("MM-DD-YYYY")] = self.cfuViewData().historicalGraphing[date];
                    }

                    self.tableRows = rows;
                    self.testTable = createTables(hashTable, assets, liabs, self.cfuViewData().scenarios, self.cfuViewData().policyLimits, modifiedHistoricalGraph);
                    if (self.cfuViewData().topTable.length > 0) {
                        self.chartObjects = createChartObjects(assets, liabs, self.cfuViewData().scenarios, self.cfuViewData().topTable[0].asOfDate, self.cfuViewData().policyLimits);
                    }
                }
                

                var n = 0;

            });
        };
    };

    return coreFundingUtilizationVm;

});
