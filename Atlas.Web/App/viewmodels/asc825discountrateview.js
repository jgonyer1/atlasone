﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id) {
        var self = this;
        this.ascViewData = ko.observable();

        this.compositionComplete = function () {
            
            var tbl = self.ascViewData().tableData;
            var monthLabels = {};
            var lblTbl = self.ascViewData().monthLabels;

            for (var i = 0; i < lblTbl.length; i++) {
                var rec = lblTbl[i];
                monthLabels[rec.month] = i;

                $('#ascHead').append('<th>' + rec.label +  '</th>');
            }

            var idxs = {};

            //Build JSON Object before we output Data
            for (var i = 0; i < tbl.length; i++) {
                var rec = tbl[i];
                if (idxs[rec.name] == undefined) {
                    idxs[rec.name] = {
                        rates: new Array(lblTbl.length),
                        offset: new Array(lblTbl.length),
                        specialType: 0,
                        disc: 0
                    }
                }

                idxs[rec.name].rates[monthLabels[rec.month]] = rec.rate;
                idxs[rec.name].offset[monthLabels[rec.month]] = rec.offset;
                idxs[rec.name].specialType = rec.specialType;
                idxs[rec.name].disc = rec.type;

            }



            var firstInt = true;
            var firstDisc = true;


            for (var id in idxs) {
                if (firstInt) {
                    firstInt = false;
                    $('#ascBody').append('<tr><td>Interest</td><td colspan = "' + lblTbl.length + '"</td></tr>');
                };



                if (numeral(idxs[id].disc).value() == 1) {
                    if (firstDisc) {
                        firstDisc = false;
                        $('#ascBody').append('<tr><td>Discount</td><td colspan = "' + lblTbl.length + '"</td></tr>');
                    }
                }

                var rateRow = '<tr><td> ' + id + '</td>';
                for (var z = 0; z < idxs[id].rates.length; z++) {
                    rateRow += '<td>' + numeral(idxs[id].rates[z]).format('0.00') + '</td>';

                }
                rateRow += '</tr>';


                $('#ascBody').append(rateRow);

                if (numeral(idxs[id].specialType).value() != 0) {
                    var offSetRow = '<tr><td>Offset</td>';
                    for (var z = 0; z < idxs[id].offset.length; z++) {
                        if (numeral(idxs[id].offset[z]).value() != 0) {
                            offSetRow += '<td>' + numeral(idxs[id].offset[z]).format('0.00') + '</td>';
                        }
                        else {
                            offSetRow += '<td>--</td>';
                        }
                       

                    }
                    offSetRow += '</tr>';
                    $('#ascBody').append(offSetRow);

                }

            }

            $("#discountrate-view").height($("#discountrate-view").height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }

        }



        this.activate = function () {
            globalContext.logEvent("Viewed", "ASC825discountrateview");
            global.loadingReport(true);
            return globalContext.getASC825DiscountRateViewDataById(id, self.ascViewData).then(function () {
                var viewData = self.ascViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
            });
        };
    };

    return ascVm;

});
