﻿define([
    'services/globalcontext',
    'durandal/app',
    'viewmodels/lookbackcustomlayoutrowitemconfig'
],
    function (globalContext, app, rowItemConfig) {

        var rowItemCollectionVm = function (isAsset, layoutVm, layoutRowItemCollection, allClassifications) {            
            var self = this;
            var tempArr = [];

            this.layoutVm = layoutVm;

            //create a viewmodel for each row item, we need some functionality that's not necessary to save in the root model
            var sorted = layoutRowItemCollection().filter(function (i) { return i.isAsset() == isAsset;}).sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            var sortedStuff = ko.observableArray(sorted);
            this.sortedRowItems = ko.observableArray();
            
            sortedStuff().forEach(function (rowItem) {
                var vm = new rowItemConfig(layoutRowItemCollection, rowItem, allClassifications);
                tempArr.push(vm);
            });
            this.sortedRowItems(tempArr);
            tempArr = [];

            //row item manipulation 
            this.addRowItem = function () {
                var rowItem = globalContext.createLBCustomLayoutRowItem(isAsset, rowItem);
                rowItem.priority(layoutRowItemCollection().filter(function (i) { return i.isAsset() == isAsset}).length);
                //rowItem.lBCustomLayout(layout());
                var rowItemVM = new rowItemConfig(layoutRowItemCollection, rowItem, allClassifications);
                layoutRowItemCollection().push(rowItem);
                self.sortedRowItems.push(rowItemVM);

                sortRowItems();
            }

            this.removeRowItem = function (rowItem) {

                var msg = 'Delete row item "' + rowItem._rowItem.title() + '" ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        var classificationsToDel = [];
                        rowItem._rowItem.lBCustomLayoutClassifications().forEach(function (cl) {
                            classificationsToDel.push(cl);
                        });
                        for (var c = 0; c < classificationsToDel.length; c++) {
                            classificationsToDel[c].entityAspect.setDeleted();
                        }
                        rowItem._rowItem.entityAspect.setDeleted();
                        layoutRowItemCollection().forEach(function (item, index) {
                            item.priority(index);
                        });
                        sortRowItems();
                    }
                }

            };

            this.changeRowType = function (rItem) {
                if (rItem._rowItem.rowType() == "Section Header") {
                    rItem._rowItem.lBCustomLayoutClassifications.removeAll();
                }
            };

            var sortRowItems = function () {
                var sorted = self.sortedRowItems().filter(function (i) {
                    return i._rowItem.entityAspect.entityState.name != "Deleted" && i._rowItem.entityAspect.entityState.name != "Detached";
                });
                sorted = sorted.sort(function (a, b) {
                    if (a._rowItem.priority() === b._rowItem.priority()) return 0;
                    return a._rowItem.priority() > b._rowItem.priority() ? 1 : -1;
                });
                self.sortedRowItems(sorted);
            };

            //function to run after sort
            this.sortableBeforeMove = function (arg) {
                var rowItem = arg.item;
                var rowType = rowItem._rowItem.rowType();
                if (arg.targetIndex > 0 && rowType == "Sub" && arg.targetParent()[arg.targetIndex - 1]._rowItem.rowType() == "Master") {
                    var msg = "Subs cannot be placed beneath Masters";
                    app.showMessage(msg, '', ['OK']);
                    arg.cancelDrop = true;
                }
            };

            this.sortableAfterMove = function () {
                self.sortedRowItems().forEach(function (r, index) {
                    r._rowItem.priority(index);
                });
            };


            //sortRowItems();
        };


        return rowItemCollectionVm;
    })