﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var cashflowVm = function (id) {
        var self = this;
        this.cashflowViewData = ko.observable();


        function BucketToAddTo(grouping) {
            //REturns Yearly Buckets, then detail Buckets
            switch (grouping) {
                case 'Q1Y1':
                    return [0, 0];
                    break;
                case 'Q2Y1':
                    return [0, 1];
                    break;
                case 'Q3Y1':
                    return [0, 2];
                    break;
                case 'Q4Y1':
                    return [0, 3];
                    break;
                case 'Q1Y2':
                    return [1, 4];
                    break;
                case 'Q2Y2':
                    return [1, 5];
                    break;
                case 'Q3Y2':
                    return [1, 6];
                    break;
                case 'Q4Y2':
                    return [1, 7];
                    break;
                case 'Y3':
                    return [2, 8];
                    break;
                case 'Y4':
                    return [3, 9];
                    break;
                case 'Y5':
                    return [4, 10];
                    break;
                case '>Y5':
                    return [5, 11];
                    break;
            }


        }

        this.compositionComplete = function () {
            if (self.cashflowViewData().errors.length > 0) {
                return true;
            }
            var yearBuckets = { Asset: {}, Liability: {}, Net: {} };

            if (self.cashflowViewData().scens.length == 0) {
                toastr.error("No scenarios found.", "", 2500);
            } else {
                //Build Buckets
                for (var i = 0; i < self.cashflowViewData().scens.length; i++) {
                    var scen = i + self.cashflowViewData().scens[i];
                    yearBuckets.Asset[scen] = {
                        yearBuckets: [0, 0, 0, 0, 0, 0],
                        cumulativeBuckets: [0, 0, 0, 0, 0],
                        extendBuckets: {},
                        trueScenName: self.cashflowViewData().scens[i],
                        scaling: {}
                    }
                    yearBuckets.Liability[scen] = {
                        yearBuckets: [0, 0, 0, 0, 0, 0],
                        cumulativeBuckets: [0, 0, 0, 0, 0],
                        extendBuckets: {},
                        trueScenName: self.cashflowViewData().scens[i],
                        scaling: {}
                    }
                    yearBuckets.Net[scen] = {
                        yearBuckets: [0, 0, 0, 0, 0, 0],
                        cumulativeBuckets: [0, 0, 0, 0, 0],
                        extendBuckets: {},
                        trueScenName: self.cashflowViewData().scens[i],
                        scaling: {}
                    }
                }

                //Put all values in correct buckets
                var simpleName = true;
                for (var i = 0; i < self.cashflowViewData().tableData.length; i++) {
                    var tbl = self.cashflowViewData().tableData[i];
                    var scen = i + self.cashflowViewData().scens[i];


                    //if not the compare scen check if they match up


                    for (var c in tbl) {
                        var rec = tbl[c];
                        var al, alFactor;
                        if (rec.isAsset) {
                            al = "Asset";
                            alFactor = 1;
                        } else {
                            al = "Liability";
                            alFactor = -1;
                        }

                        var idxs = BucketToAddTo(rec.qtrs);
                        yearBuckets[al][scen].yearBuckets[idxs[0]] += rec.cashFlow;
                        if (idxs[0] <= 4) {
                            //Populate cumulative data as well
                            for (var z = idxs[0]; z <= 4; z++) {
                                yearBuckets[al][scen].cumulativeBuckets[z] += rec.cashFlow;
                            }
                        }

                        if (yearBuckets[al][scen].extendBuckets[rec.name] == undefined) {
                            yearBuckets[al][scen].extendBuckets[rec.name] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                        }

                        yearBuckets[al][scen].extendBuckets[rec.name][idxs[1]] += rec.cashFlow;

                        //add to the Net
                        yearBuckets.Net[scen].yearBuckets[idxs[0]] += rec.cashFlow * alFactor;
                    }
                }



                //Build Difference 
                var cats = [];

                for (var x = 0; x < 6; x++) {
                    for (var s = 0; s < self.cashflowViewData().scens.length; s++) {
                        cats.push(self.cashflowViewData().scens[s]);
                    }
                }

                var aSumSeries = [];
                var lSumSeries = [];
                var nSumSeries = [{
                    color: charting.chartColors.red,
                    name: 'Net Cash Flow',
                    data: []
                }];


                var bSum = 0;
                var assetSummaryColorOrder = ['orange', 'blue', 'indigo', 'red', 'green', 'purple', 'yellow'];
                var liabSummaryColorOrder = ['green', 'purple', 'yellow', 'red', 'blue', 'orange', 'indigo'];
                var maxDigits = 0;
                var minDigits = 0;
                var maxCashflow = Number.MIN_VALUE;
                var minNetCashflow = Number.MAX_VALUE;
                //Do Summary Data
                for (var a = 0; a < 2; a++) {

                    var sFlag = true;
                    var al = ['Asset', 'Liability'][a];



                    //loop through all the defined grouping extended buckets
                    var dgCount = 0;
                    for (var dgName in yearBuckets[al]["0" + self.cashflowViewData().scens[0]].extendBuckets) {
                        var colorStr = '';

                        if (dgCount < assetSummaryColorOrder.length) {
                            if (a == 0) {
                                colorStr = charting.chartColors[assetSummaryColorOrder[dgCount]];
                            } else {
                                colorStr = charting.chartColors[liabSummaryColorOrder[dgCount]];
                            }

                        }
                        if (a == 0) {
                            aSumSeries.push({
                                color: colorStr,
                                name: dgName,
                                data: []
                            });
                        } else {
                            lSumSeries.push({
                                color: colorStr,
                                name: dgName,
                                data: []
                            });
                        }


                        //loop through the extend buckets to get each year group
                        for (var y = 0; y < 6; y++) {

                            var extIdxs = [];

                            switch (y) {
                                case 0:
                                    extIdxs = [0, 4];
                                    break;
                                case 1:
                                    extIdxs = [4, 8];
                                    break;
                                case 2:
                                    extIdxs = [8, 9];
                                    break;
                                case 3:
                                    extIdxs = [9, 10];
                                    break;
                                case 4:
                                    extIdxs = [10, 11];
                                    break;
                                case 5:
                                    extIdxs = [11, 12];
                                    break;
                            }
                            //loop through each scenario and add the year bucket values in scenario order
                            self.cashflowViewData().scens.forEach(function (curVal, indx, arr) {
                                var ybScenKey = indx + "" + curVal;
                                var ybsum = 0;

                                //while we're in here, get the overall maxes and mins for scaling
                                yearBuckets[al][ybScenKey].yearBuckets.forEach(function (curVal, idx, arr) {
                                    if (curVal > maxCashflow) {
                                        maxCashflow = curVal;
                                    }
                                });

                                for (var e = extIdxs[0]; e < extIdxs[1]; e++) {
                                    ybsum += yearBuckets[al][ybScenKey].extendBuckets[dgName][e];
                                }
                                if (a == 0) {
                                    aSumSeries[dgCount].data.push(ybsum);
                                } else {
                                    lSumSeries[dgCount].data.push(ybsum);
                                }
                            });
                        }
                        dgCount++;
                    }
                    yearBuckets[al].scaling = charting.cashflowScaling(maxCashflow, 0);
                    maxCashflow = Number.MIN_VALUE;
                }

                //handle the Net hash separately
                for (var y = 0; y < 6; y++) {
                    for (var s in yearBuckets.Net) {
                        if (yearBuckets.Net[s].yearBuckets[y] > maxCashflow) {
                            maxCashflow = yearBuckets.Net[s].yearBuckets[y];
                        }
                        if (yearBuckets.Net[s].yearBuckets[y] < minNetCashflow) {
                            minNetCashflow = yearBuckets.Net[s].yearBuckets[y];
                        }
                        nSumSeries[0].data.push(yearBuckets.Net[s].yearBuckets[y]);
                    }
                }

                yearBuckets.Net.scaling = charting.cashflowScaling(maxCashflow, minNetCashflow);

                $('#assetChart').highcharts(charting.cashflowSummaryChart('Asset Cash Flows', cats, aSumSeries, self.cashflowViewData().scens.length, yearBuckets.Asset.scaling.scaleMin, yearBuckets.Asset.scaling.scaleMax, yearBuckets.Asset.scaling.scaleUnit));
                $('#liabChart').highcharts(charting.cashflowSummaryChart('Liability Cash Flows', cats, lSumSeries, self.cashflowViewData().scens.length, yearBuckets.Liability.scaling.scaleMin, yearBuckets.Liability.scaling.scaleMax, yearBuckets.Liability.scaling.scaleUnit));
                $('#netChart').highcharts(charting.netCashflowSummaryChart('Net Cash Flows', cats, nSumSeries, self.cashflowViewData().scens.length, yearBuckets.Net.scaling.scaleMin, yearBuckets.Net.scaling.scaleMax, yearBuckets.Net.scaling.scaleUnit));

                $("#cashflow-view").height($("#cashflow-view").height());
            }

            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                $('.report-view').css('padding-bottom', '0');
                hiqPdfConverter.startConversion();
            }


        }



        this.activate = function () {
            return globalContext.getNetCashflowViewDataById(id, self.cashflowViewData).then(function () {
                var viewData = self.cashflowViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
            });
        };
    };

    return cashflowVm;

});
