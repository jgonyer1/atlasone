﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/app'
],
function (globalContext, router, app) {
    var vm = function () {
        var self = this;
        var curPage = "L360Push";
        //Vars
        this.simulationTypes = ko.observableArray();
        this.scenarioTypes = ko.observableArray();
        this.basicSurplusAdmins = ko.observableArray();
        this.simulationTypeId = ko.observable();
        this.scenarioTypeId = ko.observable();
        this.basicSurplusAdminId = ko.observable();
        this.isPushing = ko.observable(false);

        //Navigation Helpers
        this.canDeactivate = function () {
            if (self.isPushing()) {
                var msg = 'Do you want to leave and cancel?';
                return app.showMessage(msg, 'Navigate Away', ['No', 'Yes'])
                    .then(function (selectedOption) {
                        if (selectedOption === 'Yes') {
                            self.isPushing(false);
                        }
                        return selectedOption;
                    });
            }
            return true;
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        this.doPush = function () {
            self.isPushing(true);
            $("#l360_push_dialog").modal("show");
            //var ret = ko.observable();
            return globalContext.l360Push(self.simulationTypeId, self.scenarioTypeId, self.basicSurplusAdminId/*, ret*/).then(function () {
                self.isPushing(false);
                $("#l360_push_dialog").modal("hide");
            });
        };

        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            //Pre-Setup
            return Q.all([
                globalContext.getScenarioTypes(self.scenarioTypes),
                globalContext.getSimulationTypes(self.simulationTypes),
                globalContext.getBasicSurplusAdmins(self.basicSurplusAdmins)
            ]);
        };

        return this;
    };

    return vm;
});
