﻿define(['viewmodels/chartconfig', 'services/globalContext', 'services/global'],
    function (chartconfig, globalContext, global) {
        var twoChartConfigVm = function (twoChart) {
            if (twoChart().niiOption() != "nii" && twoChart().niiOption() != "ni") {
                twoChart().niiOption("nii");
            }
            var that = this;
            this.twoChart = twoChart;
            this.modelSetup = ko.observable();
            this.overrideSub = null;

            //NOTE the chart config for two chart does not update the niiOption, just putting it throught becuase it gets handled in the chartConfig for OneChart
            this.leftChartVm = ko.observable(new chartconfig(twoChart().leftChart, twoChart().niiOption, { showPercChange: true, showComparatives: true, showInst: true, callSave: false, hideQuarterly: true, canAddSimulations: true, side: 'left' }));
            this.rightChartVm = ko.observable(new chartconfig(twoChart().rightChart,twoChart().niiOption, { showPercChange: true, showComparatives: true, showInst: true, callSave: true, hideQuarterly: true, canAddSimulations: true, side: 'right' }));
            this.cancel = function () {
                that.leftChartVm().cancel();
                that.rightChartVm().cancel();
            };
            this.activate = function () {
                global.loadingReport(true);
                return globalContext.getModelSetup(that.modelSetup).then(function () {

                    //tax equiv sync
                    if (!that.twoChart().overrideTaxEquiv()) {
                        that.twoChart().taxEquivalentIncome(that.modelSetup().reportTaxEquivalent());
                        if (twoChart().id() > 0) {
                            globalContext.saveChangesQuiet();
                        }
                    }
                    that.overrideSub = that.twoChart().overrideTaxEquiv.subscribe(function (newValue) {
                        if (!newValue) {
                            that.twoChart().taxEquivalentIncome(that.modelSetup().reportTaxEquivalent());
                        }
                    });
                    global.loadingReport(false);
                });
            };
            this.deactivate = function () {
                that.overrideSub.dispose();
            };
        };
        return twoChartConfigVm;
    });
