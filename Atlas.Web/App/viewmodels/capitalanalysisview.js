﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var capVm = function (id) {
        var self = this;
        var curPage = "CapitalAnalysisView";
        this.cap = ko.observable();
        this.bankInfo = ko.observable();

        this.mRound = function (val, mroundVal) {
            return Math.round(val / mroundVal, 0) * mroundVal;
        };

        this.standardChartScaling = function (maxValue) {
            maxValue = maxValue / 1000;
            var minRange = 0;
            var maxRange = 0;
            var roundVal = 0;
            var otherVal = 0;
            var majorUnit = 0;

            if (maxValue <= 1000) {
                roundVal = 250;
                otherVal = 200;
            }
            else if (maxValue <= 100000) {
                roundVal = 2500;
                otherVal = 2000;
            }
            else if (maxValue <= 1000000) {
                roundVal = 25000;
                otherVal = 5000;
            }
            else {
                roundVal = 250000;
                otherVal = 50000;
            }

            var maxRound = self.mRound(maxValue, roundVal);

            if (maxRound < maxValue + otherVal) {
                maxRange = maxRound + roundVal;
            }
            else {
                maxRange = maxRound;
            }

            majorUnit = maxRange / 5;

            console.log(maxRange);
            console.log(minRange);
            console.log(majorUnit);
            return {
                scaleMax: maxRange,
                scaleMin: minRange,
                majorUnit: majorUnit
            };
        };

        this.compositionComplete = function () {

            //Loop through outtable and put records in there
            var results = self.cap();
            var chartIds = [];
            var chartHolderClasses = [];
            var headerIds = [];
            var cht;
            var totalColspan = 3;
            for (var i = 0; i < results.outTable.length; i++) {
                var rec = results.outTable[i];
                var row = "<tr>";
                row += "<td style='width:27% !important; font-weight:bold !important;'>" + rec.name + "</td>";

                if (rec.name == "TOTAL ASSETS FOR LEVERAGE RATIO" || rec.name == "TOTAL RISK WEIGHTED ASSETS" || rec.name == "Total Assets For Net Worth Ratio") {
                    row += "<td></td>";
                    row += "<td>" + numeral(rec.amount).format("0,0.0") + "</td>";

                    if (results.tableIncPol) {
                        totalColspan += 3;
                        row += "<td class='offset'></td>";
                        row += "<td ></td>";
                        row += "<td ></td>";
                    }

                    if (results.tableIncWell) {
                        totalColspan += 3;
                        row += "<td class='offset'></td>";
                        row += "<td></td>";
                        row += "<td></td>";
                    }

                }
                else {
                    row += "<td>" + numeral(rec.ratio).format("0.00%") + "</td>";
                    row += "<td>" + numeral(rec.amount).format("0.0") + "</td>";

                    if (results.tableIncPol) {
                        totalColspan += 3;
                        if (rec.policylimits != "-9.99") {
                            row += "<td class='offset'>" + numeral(rec.policylimits).format("0.00%") + "</td>";
                            row += "<td>" + numeral(rec.polCapital).format("+0,0.0") + "</td>";
                            row += "<td>" + numeral(rec.polAssets).format("+0,0.0") + "</td>";
                        } else {
                            row += "<td class='offset'></td>";
                            row += "<td></td>";
                            row += "<td></td>";
                        }
                        
                    }
                    
                    if (results.tableIncWell) {
                        totalColspan += 3;
                        if (rec.wellCapLimits != "-9.99") {
                            row += "<td class='offset'>" + numeral(rec.wellCapLimits).format("0.00%") + "</td>";
                            row += "<td>" + numeral(rec.wcCapital).format("+0,0.0") + "</td>";
                            row += "<td>" + numeral(rec.wcAssets).format("+0,0.0") + "</td>";
                        } else {
                            row += "<td class='offset'></td>";
                            row += "<td></td>";
                            row += "<td></td>";
                        }
                        
                    }
                }
                row += "</tr>";

                $("#polTable tbody").append(row);
            }
            row = "<tfoot>";
            row += "<tr>";
            row += "<td colspan='"+totalColspan+"' style='text-align: right'>All dollar amounts are shown in millions</td>";
            row += "<tr>";
            if (self.bankInfo().creditUnion.instType == "BK" && (self.cap().tableIncPol || self.cap().tableIncWell)) {
                row += "<tr>";
                row += "<td colspan='" + totalColspan + "' style='text-align: right'>*Risk Based asset cushion assumes 100% risk weighting</td>";
                row += "<tr>";
            }
            row += "</tfoot>"
            $("#polTable").append(row);
            
            //figure out how many charts we need to put on the report
            //and in what configuration
            var chartCount = 0;
            var wrapperHtml = "";
            for (var i = 0; i < results.charts.length; i++) {
                cht = results.charts[i];
                if (cht.chartName != "None") {
                    chartCount++;
                }
            }
            switch (chartCount) {
                case 0:
                    break;
                case 1:
                    headerIds = ["topHeader"];
                    chartIds = ["topChart"];
                    chartHolderClasses = ["col-sm-12"];
                    wrapperHtml = "<div class='row'><div class='col-sm-12'><h2 class='atlasHeader grey' id='"+ headerIds[0] +"'></h2><div id='"+ chartIds[0] +"'></div></div></div>";
                    break;
                case 2:
                    headerIds = ["topHeader", "bottomHeader"];
                    chartIds = ["topChart", "bottomChart"];
                    chartHolderClasses = ["col-sm-12", "col-sm-12"];
                    wrapperHtml = "<div class='row'><div class='col-sm-12'><h2 class='atlasHeader grey' id='"+ headerIds[0] +"'></h2><div id='"+ chartIds[0] +"'></div></div></div>";
                    wrapperHtml += "<div class='row'><div class='col-sm-12'><h2 class='atlasHeader grey' id='" + headerIds[1] + "'></h2><div id='" + chartIds[1] + "'></div></div></div>";
                    break;
                case 3:
                    headerIds = ["topLeftHeader", "topRightHeader", "bottomLeftHeader"];
                    chartIds = ["topLeftChart", "topRightChart", "bottomLeftChart"];
                    chartHolderClasses = ["col-sm-6", "col-sm-6", "col-sm-6"];
                    wrapperHtml = "<div class='row'><div class='col-sm-6'><h2 class='atlasHeader grey' class='atlasHeader grey' id='" + headerIds[0] + "'></h2><div id='" + chartIds[0] +"'></div></div><div class='col-sm-6'><h2 class='atlasHeader grey' id='"+ headerIds[1] +"'></h2><div id='"+ chartIds[1] +"'></div></div></div>";
                    wrapperHtml += "<div class='row'><div class='col-sm-6'><h2 class='atlasHeader grey' id='"+ headerIds[2] +"'></h2><div id='"+ chartIds[2] +"'></div></div></div>";
                    break;
                case 4:
                    headerIds = ["topLeftHeader", "topRightHeader", "bottomLeftHeader", "bottomRightHeader"];
                    chartIds = ["topLeftChart", "topRightChart", "bottomLeftChart", "bottomRightChart"];
                    chartHolderClasses = ["col-sm-6", "col-sm-6", "col-sm-6", "col-sm-6"];
                    wrapperHtml = "<div class='row'><div class='col-sm-6'><h2 class='atlasHeader grey' id='" + headerIds[0] + "'></h2><div id='" + chartIds[0] + "'></div></div><div class='col-sm-6'><h2 class='atlasHeader grey' id='" + headerIds[1] + "'></h2><div id='" + chartIds[1] + "'></div></div></div>";
                    wrapperHtml += "<div class='row'><div class='col-sm-6'><h2 class='atlasHeader grey' id='" + headerIds[2] + "'></h2><div id='" + chartIds[2] + "'></div></div><div class='col-sm-6'><h2 class='atlasHeader grey' id='" + headerIds[3] + "'></h2><div id='" + chartIds[3] + "'></div></div></div>";
                    break;
            }
            document.getElementById("chart_holder").innerHTML += wrapperHtml;
            
            var chartTracker = 0;
            //Generate chart objects
            var maxVal;
            for (var i = 0; i < results.charts.length; i++) {
                cht = results.charts[i];
                for (var z = 0; z < cht.chartValues.length; z++) {
                    var rec = cht.chartValues[z];
                    
                    if (maxVal == undefined) {
                        maxVal = numeral(rec.policyLimit).value() * 100;
                    }

                    if (maxVal < numeral(rec.policyLimit).value() * 100) {
                        maxVal = numeral(rec.policyLimit).value() * 100;
                    }
                    if (maxVal < numeral(rec.wellCap).value() * 100) {
                        maxVal = numeral(rec.wellCap).value() * 100;
                    }
                    if (maxVal < numeral(rec.currRatio).value() * 100) {
                        maxVal = numeral(rec.currRatio).value() * 100;
                    }
                }
            }
            for (var i = 0; i < results.charts.length; i++) {
                cht = results.charts[i];
                var chtId = "";
                var headerId = "";
                //Set dom ids based off of id off chart
                if (cht.chartName != 'None') {
                    headerId = headerIds[chartTracker];
                    chtId = chartIds[chartTracker];
                    document.getElementById(headerId).innerHTML += cht.chartName;
                    //$(headerId).text(cht.chartName);
                }

                //sereis arrays
                var series = [
                    {
                        type: 'scatter',
                        zIndex: '2',
                        name: 'POLICY',
                        marker: {
                            symbol: 'url(Content/images/policymarker-yellow.png)',
                            width: 50,
                            height: 5
                        },
                        data: []
                    },
                    {
                        type: 'scatter',
                        zIndex: '1',
                        name: 'WELL-CAPITALIZED',
                        marker: {
                            symbol: 'url(Content/images/wellcapitalized-red.png)',
                            width: 50,
                            height: 5
                        },
                        data: []
                    },
                    {
                        type: 'column',
                        color: '#40637a',
                        name: 'CURRENT',
                        showInLegend: false, 
                        dataLabels: {
                            format: '{point.y:.2f}',
                            enabled: true,
                            align: 'center',
                            color: '#000000',
                            shadow: false,
                            x: 0,
                            y:5,
                            style: {
                                "fontSize": "12px", "textShadow": "0px", "fontWeight": "bold" },
                        },
                        data: [],
                        zoneAxis: 'y',

                    
                    }

                ];

                for (var z = 0; z < cht.chartValues.length; z++){
                    var rec = cht.chartValues[z];
                    series[0].data.push(numeral(rec.policyLimit).value()* 100);
                    series[1].data.push(numeral(rec.wellCap).value()* 100);
                    //series[2].data.push(numeral(rec.currRatio).value() * 100);
                    if (z == cht.chartValues.length - 1) {
                        series[2].data.push({
                            y: numeral(rec.currRatio).value() * 100,
                            color: '#73afad'
                        })
                    } else {
                        series[2].data.push({
                            y: numeral(rec.currRatio).value() * 100,
                            color: '#40637a'
                        })
                    }

                }

                if (!results.chartIncPol) {
                    series.splice(0, 1);
                }

                if (!results.chartIncWell) {
                    if (!results.chartIncPol) {
                        series.splice(0, 1);
                    }
                    else {
                        series.splice(1, 1);
                    }
                   
                }

                //Now lets chart the thing
                if (cht.chartName != "None") {
                    $("#"+chtId).highcharts(charting.createCapitalAnalysisChart(series, cht.categories, maxVal));
                    chartTracker++;
                }
                


            }

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                $('.report-view').css('padding-bottom', '0px');
                //$('.report-wrapper').height(406);
                $("#capital-analysis-view").height(406);
                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getCapitalAnalysisViewDataById(id, self.cap).then(function () {
                return globalContext.getBankInfo(self.bankInfo, null);
            });
        };
    };

    return capVm;

});