﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/app',
    'viewmodels/navigateawaymodal'
],
function (globalContext, router, app, naModal) {
    var vm = function () {
        var self = this;

        //Vars
        this.modelsetup = ko.observable();


        this.mortPrepayType = ko.observableArray(['BK', 'Client Provided']);
        this.otherPrepayType = ko.observableArray(['Client Provided', 'Not Client Provided']);
        this.investmentType = ko.observableArray(['Yield Book', 'Other']);
        this.taxEquiv = ko.observableArray([{ name: 'Yes', value: true }, { name: 'No', value: false }]);
        this.offSheet = ko.observableArray([{ name: 'Yes', value: true }, { name: 'No', value: false }]);
        this.avgLifeAssumption = ko.observableArray(['5', '7.5', 'Other']);

        this.isSaving = ko.observable(false);
        //Navigation Helpers
        this.hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges();
        });

        this.canSave = ko.pureComputed(function () {
            return self.hasChanges() && !self.isSaving();
        });

        this.canDeactivate = function () {
            if (! self.hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { self.save().then(function () { return true; }); }
            );
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        this.cancel = function () {
            self.isSaving(true);
            globalContext.cancelChanges();
            self.isSaving(false);
        };

        this.UpdateAverageLife = function () {
            if (self.modelsetup().avgLifeAssumption() == 'Other') {
                self.modelsetup().avgLifeYears(0);
                self.modelsetup().avgLifeMessage('Average lives on non-maturity deposits are based upon a mm/dd/yyyy DCG deposit study. The aggregate average life based upon the current deposit mix is ##.## years. Please refer to the Deposit Study Tear Sheet for additional information. ');
            }
            else {
                self.modelsetup().avgLifeYears(parseFloat(self.modelsetup().avgLifeAssumption()));
                self.modelsetup().avgLifeMessage('This assessment assumed a ' + self.modelsetup().avgLifeAssumption() + ' year estimated life on the core deposit base.');
            }
        };

        this.save = function () {
            self.isSaving(true);
            return globalContext.saveChanges().fin(complete);

            function complete() {
                self.isSaving(false);
            }
        };

        //Data Handling
        this.activate = function (id) {
            //Get Eve Config
            return globalContext.getModelSetup(self.modelsetup).then(function () {

            });
        };

        return this;
    };

    return vm;
});
