﻿define(['durandal/app'], function (app) {
    var NavigateAwayModal = function () {};

    NavigateAwayModal.show = function (onDiscard, onSave, message) {
        return app.showMessage(message || 'There are unsaved changes, do you want to leave and discard changes?', 'Navigate Away', ['Cancel', 'Discard Changes', 'Save and Exit'])
            .then(function (selectedOption) {
                if (selectedOption === 'Discard Changes') {
                    onDiscard && onDiscard();
                    return true;
                }

                else if (selectedOption === 'Save and Exit') {
                    onSave && onSave();
                    return true;
                }

                return false;
            });
    };

    return NavigateAwayModal;
});