﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var pdVm = function (id) {
        var self = this;
        self.pdViewData = ko.observable();


        function createTableHeaders(lblTables) {
            var tbls = [];
            var monthLables = {};
            var firstpass = true;
            var tblCounter = 0;

            for (var tbl in lblTables) {
                //open the table and tbody here, close them in the createTable function
                var tblStr = "<h2>" + self.pdViewData().scenarios[tblCounter] + "</h2><table class='report-std-table simulation-summary-table'><tr><th></th>";
                var thisTable = lblTables[tbl];
                
                for (var i = 0; i < thisTable.length; i++) {
                    var record = thisTable[i];
                    if (firstpass) {
                        monthLables[record.month] = i;
                    }
                    
                    tblStr += "<th class='underline bold-text'>" + record.label + "</th>";
                }
                firstpass = false;
                tblStr += "</tr></thead><tbody>";
                tbls.push(tblStr);
                tblCounter++;
            }

            return { tables: tbls, monthLables: monthLables };
        }

        function createTable(scenDataTables, lblTables) {
            var tableHeaderObj = createTableHeaders(lblTables);
            var tables = tableHeaderObj.tables;
            var monthLabels = tableHeaderObj.monthLables;
            var idxs = {};
            var scenLen = scenDataTables.length;
            var assetLiabStr= "";

            //build json objects for data
            for (var i = 0; i < scenLen; i++) {
                var scenName = self.pdViewData().scenarios[i];
                idxs[scenName] = {};
                var dtLen = scenDataTables[i].length;
                for (var j = 0; j < dtLen; j++) {
                    var rec = scenDataTables[i][j];
                    if (idxs[scenName][rec.name] == undefined) {
                        idxs[scenName][rec.name] = {
                            categoryName: rec.name,
                            rowLabel: rec.nEString + " " + rec.pCType + " " + rec.calc,
                            speeds: new Array(lblTables[i].length),
                            isAssetStr: rec.sequence.substring(0,1)
                        };
                    }
                    idxs[scenName][rec.name].speeds[monthLabels[rec.month]] = rec.endBal;
                }
            }
            var scenarioCounter = 0;
            for (var scenarioName in idxs) {
                var tableData = idxs[scenarioName];
                for (var row in tableData) {
                    var rowData = tableData[row];
                    var nonZero = false;
                    if (rowData.isAsset) {

                    }
                    var rowStr = "<tr class='body-lt-blue'><td colspan='" + (rowData.speeds.length + 1) + "'>" + row + "</td></tr>";
                    if (assetLiabStr != rowData.isAssetStr) {
                        assetLiabStr = rowData.isAssetStr;
                        rowStr = "<tr><td class='bold-text underline' colspan='" + (rowData.speeds.length + 1) + "'>" + (assetLiabStr == "A" ? "ASSETS" : "LIABILITIES") + "</td></tr>" + rowStr;
                    }
                    rowStr += "<tr><td class='indent'>" + rowData.rowLabel + "</td>";
                    for (var cell in monthLabels) {
                        var str = "--";
                        var rawVal = parseFloat(rowData.speeds[monthLabels[cell]]);
                        var thiIsNotZero = !isNaN(rawVal) && rawVal != 0.00;
                        nonZero = nonZero || (!isNaN(rawVal) && rawVal != 0.00);
                        if (rowData.speeds[monthLabels[cell]] != undefined || thiIsNotZero) {
                            str = numeral(rowData.speeds[monthLabels[cell]]).format("0.00");
                        } else {
                            str = "--";
                        }
                        rowStr += "<td>"+ str +"</td>";
                    }
                    rowStr += "</tr>";
                    if (!nonZero) {
                        rowStr = "";
                    }
                    tables[scenarioCounter] += rowStr;
                }
                //close the tbody and table tags
                tables[scenarioCounter] += "</tbody></table>";
                scenarioCounter++;                
            }
            return tables.join("");
        }

        this.compositionComplete = function () {
            $("#table_container").html(self.coverPage + self.table);

            $('#prepaymentdetails-view').height($('#prepaymentdetails-view').height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }
            //$('.report-title').html(reportTitles[self.pdViewData().reportSelection]);
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getPrepaymentDetailsViewDataById(id, self.pdViewData).then(function () {
                globalContext.reportErrors(self.pdViewData().errors, self.pdViewData().warnings);
                var tempArr = [];
                var tempSubArr = [];
                var coverPage = "<div class='prepayment-details-coverpage'><h1>EVE/NEV Prepayment Details</h1>";

                //build cover page
                for (var scen in self.pdViewData().scenarios) {
                    if (self.pdViewData().scenarios[scen])
                        coverPage += "<h3>" + self.pdViewData().scenarios[scen] + " Scenario</h3>";
                }
                //end cover page
                coverPage += "</div>";
                self.coverPage = coverPage

                //should be as many of these entries as number of scenarios
                for (var tbl in self.pdViewData().scenarioDataTables) {
                    var thisTbl = self.pdViewData().scenarioDataTables[tbl];
                    if (thisTbl) {
                        for (var recs in thisTbl) {
                            var thisRec = thisTbl[recs];
                            tempSubArr.push(thisRec);
                        }
                        //once we've pushed a whole table, push that table into the whole collection
                        tempArr.push(tempSubArr);
                        tempSubArr = [];
                    }
                    
                }
                self.scenarioDataTablesArr = tempArr;
                tempSubArr = [];
                tempArr = [];

                for (var tbl in self.pdViewData().lblTables) {
                    var thisTbl = self.pdViewData().lblTables[tbl];
                    if (thisTbl) {
                        for (var recs in thisTbl) {
                            var thisRec = thisTbl[recs];
                            tempSubArr.push(thisRec);
                        }
                        //once we've pushed a whole table, push that table into the whole collection
                        tempArr.push(tempSubArr);
                        tempSubArr = [];
                    }
                    
                }
                self.lblTablesArr = tempArr;

                //if (self.lblTablesArr.length == 0 || self.scenarioDataTablesArr.length == 0) {
                //    toastr.warning("No data found.");
                //}

                self.table = createTable(self.scenarioDataTablesArr, self.lblTablesArr);
            });
        };
    };

    return pdVm;

});
