﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var coverPageVm = function (id) {
        var self = this;
        var curPage = "CoverPageView";
        this.coverPage = ko.observable();
        this.instName = ko.observable();
        this.profile = ko.observable();
        this.policy = ko.observable();
        this.compositionComplete = function () {
            
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }
        };

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getCoverPageById(id, self.coverPage).then(function () {
                return globalContext.getUserProfile(self.profile).then(function () {
                    return globalContext.getRegulatoryBankName(self.coverPage().institutionDatabaseName, self.instName).then(function () {
                         return globalContext.getPolicy(self.policy);
                    });
                });
            });
        };
    };

    return coverPageVm;

});
