﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var simSumVm = function (id) {
        var self = this;
        self.simSumViewData = ko.observable();

        function GenerateTableHeader(tableId, cells) {


        }

        function createTableHeaders(rawData, monthly) {
            var rowStr = "<thead><tr class='header-drk-blue total-row'>";
            for (var cell in rawData[0]) {
                if (cell.indexOf("name") > -1) {
                    rowStr += "<th></th>";
                }
                else if (Date.parse(cell)) {
                    rowStr += "<th class='underline'>" + moment(cell).format("MMM-YY") + ( monthly == "0" ? "" : " Q " ) + "</th>";
                }
            }
            rowStr += "</tr>";
            rowStr += "</thead><tbody>";
            return rowStr;
        }

        function createTable(rawData, reportSel, monthly) {
            var tableRows = createTableHeaders(rawData, monthly)
                , accountName = ""
                , addAccntName = false
                , cellCount = 0
                ,rowCount = 0;

            //count the number of cells 
            for (var c in rawData[0]) {
                if (c.indexOf("name") > -1 || Date.parse(c)) {
                    cellCount++;
                }
            }


            for (var row in rawData) {
                var nonZero = false
                , rowClassStr = ""
                , catType = rawData[row]["cat_Type"];

                if (catType && catType.length > 0) {
                    catType = parseInt(catType);
                } else {
                    catType = -1;
                }

                if (rowCount % 2 == 1) {
                    //rowClassStr = "body-lt-blue";
                }

                
                if (catType > 1 && catType != 0 && catType != 1 && catType != 5) {
                    rowClassStr = "total-row body-drk-blue";
                }
                var rowStr = "<tr class='[ROW_CLASS]'>";
                if (reportSel == 2) {//prepayments report is a little weird
                    rowStr = rowStr.replace("[ROW_CLASS]", "total-row ");
                    rowClassStr = "";
                    rowStr += "<td class='italic' colspan='[CELL_COUNT]'>[NAME]</td></tr><tr>";
                } else {
                    rowStr = rowStr.replace("[ROW_CLASS]", rowClassStr);
                }

                
                for (var cell in rawData[row]) {
                    var classStr = ["nowrap"];
                    


                    var rawVal = parseFloat(rawData[row][cell]);
                    var val = parseFloat(rawData[row][cell]);
                    if (reportSel == 0 || reportSel == 3) {
                        val = val / 1000;
                    }
                    if (reportSel == 0 || reportSel == 3) {
                        val = numeral(val).format('(0,0)');
                    }
                    else { //I am assuming this is the rate format?s
                        val = numeral(val).format('(0.00)');
                    }

                    //that's all well and good, but if the raw value is zero we'll just dash it out
                    //if (rawVal == 0.00 || isNaN(rawVal)) {
                    //    val = '--';
                    //}

                    if (Date.parse(cell)) {//numerical value
                        nonZero = nonZero || (!isNaN(rawVal) &&  rawVal != 0.00);
                        if (rawVal == 0.00 || isNaN(rawVal)) {
                            val = "--";
                        }
                        rowStr += "<td";//>" + val + "</td>";
                        if (reportSel == 2) {
                            rowStr += " class='italic'";
                        }
                        rowStr += ">" + val + "</td>";
                    }
                    else if (cell.indexOf("name") > -1) {
                        if (reportSel == 2) {
                            rowStr += "<td class='italic indent'>" + rawData[row]["nEString"] + " " + rawData[row]["calc"] + " " + rawData[row]["pCType"] + "</td>";
                            rowStr = rowStr.replace("[NAME]", rawData[row][cell]);
                        }
                        else {
                            rowStr += "<td>" + rawData[row][cell] + "</td>";
                        }
                        //
                    }
                }
                rowStr += "</tr>";

                rowStr = rowStr.replace('[CELL_COUNT]', cellCount.toString());
                
                if (!nonZero) { rowStr = ''; }
                else { rowCount++;}
                tableRows += rowStr;
            }
            return tableRows + "</tbody>";
        }

        this.compositionComplete = function () {
            var reportClasses = [
                "balance-sheet"
                , "rate-yield"
                , "asset-liability"
                ,"interest-income"
            ];
            var monthlySizeMod = self.simSumViewData().monthly == "1" ? "-quarterly" : "-monthly";

            var reportTitles = [
                "Balance Sheet"
                , "Rate/Yield Analysis"
                , "Asset/Liability Prepayments"
                , "Interest Income/Expense"
            ];
            $("#main_table").addClass(reportClasses[self.simSumViewData().reportSelection]+monthlySizeMod);
            $("#main_table").html(self.table);
            $('.report-title').html(reportTitles[self.simSumViewData().reportSelection] + " " + self.simSumViewData().scenarioTypeName + " Scenario");

            $('#simulationsummary-view').height($('#simulationsummary-view').height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getSimulationSummaryViewDataById(id, self.simSumViewData).then(function () {
                globalContext.reportErrors(self.simSumViewData().errors, self.simSumViewData().warnings);
                self.table = createTable(self.simSumViewData().data, self.simSumViewData().reportSelection, self.simSumViewData().monthly);
            });
        };
    };

    return simSumVm;

});
