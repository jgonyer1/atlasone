﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var fundingMatrixVm = function (fundingMatrix) {
        var self = this;
        var curPage = "FundingMatrixConfig";
        this.fundingMatrix = fundingMatrix;

        function onChangesCanceled() {
            self.sortMatrixOffsets();
        }

        this.sortedMatrixOffsets = ko.observableArray()//ko.observableArray(self.fundingMatrix().fundingMatrixOffsets());
        this.modelSetup = ko.observable();
        this.overrideTaxEquivSub = null;
        this.sortMatrixOffsets = function () {
            self.sortedMatrixOffsets(self.fundingMatrix().fundingMatrixOffsets().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
            //console.log(self.sortedMatrixOffsets());
        };

        var instChange = function (offset) {
            globalContext.getAsOfDateOffsets(offset.asOfDateOffsets, offset.institutionDatabaseName()).then(function () {
                globalContext.getPolicyOffsets(offset.policyOffsets, offset.institutionDatabaseName()).then(function () {
                    self.defaultSimulation(offset);
                });
            });
        };

        this.addMatrixOffset = function (offset) {
            var newOffset = globalContext.createFundingMatrixOffset();
            newOffset.institutionDatabaseName(self.profile().databaseName);
            newOffset.priority(self.fundingMatrix().fundingMatrixOffsets().length);

            //AsOfDateOffset Handling (Custom FundingMatrix Multi-Institution Template)
            newOffset.asOfDateOffsets = ko.observableArray();
            newOffset.policyOffsets = ko.observableArray();
            newOffset.simulationTypes = ko.observableArray();
            newOffset.scenarioTypes = ko.observableArray();
            globalContext.getPolicyOffsets(newOffset.policyOffsets, newOffset.institutionDatabaseName()).then(function () {
                globalContext.getAsOfDateOffsets(newOffset.asOfDateOffsets, newOffset.institutionDatabaseName()).then(function () {
                    globalContext.getAvailableSimulations(newOffset.simulationTypes, newOffset.institutionDatabaseName(), newOffset.asOfDateOffset()).then(function () {
                        globalContext.getAvailableSimulationScenarios(newOffset.scenarioTypes, newOffset.institutionDatabaseName(), newOffset.asOfDateOffset(), newOffset.simulationTypeId(), false).then(function () {
                            self.defaultSimulation(newOffset);
                            newOffset.instSub = newOffset.institutionDatabaseName.subscribe(function (newValue) {
                                self.refreshItemSim(newOffset);
                                instChange(this);
                            }, newOffset);

                            newOffset.aodSub = newOffset.asOfDateOffset.subscribe(function (newValue) {
                                self.refreshItemSim(newOffset);
                                self.defaultSimulation(newOffset);
                            }, newOffset);

                            newOffset.ovSub = newOffset.overrideSimulation.subscribe(function (newValue) {
                                if (!newValue) {
                                    self.refreshItemScen(newOffset);
                                    self.defaultSimulation(newOffset);
                                }
                            });

                            newOffset.simSub = newOffset.simulationTypeId.subscribe(function (newValue) {
                                self.refreshItemScen(newOffset);
                                self.defaultSimulation(newOffset);
                            }, newOffset);


                            self.fundingMatrix().fundingMatrixOffsets().push(newOffset);
                            self.sortMatrixOffsets();
                        });
                    });
                });
            });
        };
        this.removeMatrixOffset = function (offset) {
            var msg = 'Delete scenario Funding Matrix Offset ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    offset.entityAspect.setDeleted();
                    self.afterSortMove();
                    self.sortMatrixOffsets();
                }
            }

            //offset.entityAspect.setDeleted();
            //self.sortedMatrixOffsets();
        };
        this.cancel = function () {
        };
        this.afterSortMove = function () {
            self.sortedMatrixOffsets().forEach(function (item, index, arr) {
                item.priority(index);
            });
            //self.sortedMatrixOffsets();
        };



        this.institutions = ko.observableArray();

        this.global = global;

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();



        this.refreshItemScen = function (item) {
            globalContext.getAvailableSimulationScenarios(item.scenarioTypes, item.institutionDatabaseName(), item.asOfDateOffset(), item.simulationTypeId(), false)
        }

        this.refreshItemSim = function (item) {
            globalContext.getAvailableSimulations(item.simulationTypes, item.institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                self.refreshItemScen(item);
            });
        }


        this.defaultSimulation = function (fmOffset) {
            if (!fmOffset.overrideSimulation()) {
                var aod = fmOffset.asOfDateOffsets()[fmOffset.asOfDateOffset()].asOfDateDescript;
                if (fmOffset.policyOffsets()[0][aod] != undefined) {
                    fmOffset.simulationTypeId(fmOffset.policyOffsets()[0][aod][0].niiId);
                } else {
                    fmOffset.simulationTypeId(1);
                    app.showMessage("No Policies found for " + aod + ", defaulting to Base Simulation", "Error", ["OK"]);
                }
            }
            return true;
        };

        this.profile = ko.observable();

        this.compositionComplete = function () {
            console.log("HIT COMP COMPLETE");
            if (self.fundingMatrix().id() > 0) {
                 globalContext.saveChangesQuiet();
            }
        };

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            app.on('application:cancelChanges', onChangesCanceled);

            return globalContext.getScenarioTypes(self.scenarioTypes).then(function () {
                return globalContext.getSimulationTypes(self.simulationTypes).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        return globalContext.getUserProfile(self.profile).then(function () {
                            return globalContext.getModelSetup(self.modelSetup).then(function () {
                                //self.sortMatrixOffsets();
                                //tax equivalent sync
                                if (!self.fundingMatrix().overrideTaxEquiv()) {
                                    self.fundingMatrix().taxEqivalentYields(self.modelSetup().reportTaxEquivalentYield());
                                    if (self.fundingMatrix().id() > 0) {
                                        globalContext.saveChangesQuiet();
                                    }
                                }
                                self.overrideTaxEquivSub = self.fundingMatrix().overrideTaxEquiv.subscribe(function (newValue) {
                                    if (!newValue) {
                                        self.fundingMatrix().taxEqivalentYields(self.modelSetup().reportTaxEquivalentYield());
                                    }
                                });
                                //AsOfDateOffset Handling (Custom FundingMatrix Multi-Institution Template)
                                var qAllQueue = [];
                                var offsetsProcessed = 0
                                self.fundingMatrix().fundingMatrixOffsets().forEach(function (item, index, arr) {
                                    item.asOfDateOffsets = ko.observableArray();
                                    item.policyOffsets = ko.observableArray();
                                    item.simulationTypes = ko.observableArray();
                                    item.scenarioTypes = ko.observableArray();

                                     globalContext.getAsOfDateOffsets(item.asOfDateOffsets, item.institutionDatabaseName()).fin(function () {
                                         globalContext.getPolicyOffsets(item.policyOffsets, item.institutionDatabaseName()).fin(function () {
                                             globalContext.getAvailableSimulations(item.simulationTypes, item.institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                                                 globalContext.getAvailableSimulationScenarios(item.scenarioTypes, item.institutionDatabaseName(), item.asOfDateOffset(), item.simulationTypeId(), false).then(function () {
                                                    self.defaultSimulation(item);
                                                    item.instSub = item.institutionDatabaseName.subscribe(function (newValue) {
                                                        self.refreshItemSim(this);
                                                        instChange(this);
                                                
                                                    }, item);
                                                    item.aodSub = item.asOfDateOffset.subscribe(function (newValue) {
                                                        self.refreshItemSim(item);
                                                        self.defaultSimulation(item);
                                                    }, item);
                                                    item.ovSub = item.overrideSimulation.subscribe(function (newValue) {
                                                        if (!newValue) {
                                                            self.refreshItemScen(item);
                                                            self.defaultSimulation(item);
                                                        }
                                                    }, item);

                                                    item.simSub = item.simulationTypeId.subscribe(function (newValue) {
                                                            self.refreshItemScen(item);
                                                            self.defaultSimulation(item);
                                                    }, item);


                                                    offsetsProcessed++;
                                                    if (offsetsProcessed == arr.length) {
                                                        self.sortMatrixOffsets();
                                                        global.loadingReport(false);
                                                    }
                                                 });
                                             });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Custom FundingMatrix Multi-Institution Template)
            self.fundingMatrix().fundingMatrixOffsets().forEach(function (item, index, arr) {
                item.instSub.dispose();
                item.aodSub.dispose();
                item.ovSub.dispose();
                item.simSub.dispose();
            });
            self.overrideTaxEquivSub.dispose();
        };

        return this;
    };

    return fundingMatrixVm;
});
