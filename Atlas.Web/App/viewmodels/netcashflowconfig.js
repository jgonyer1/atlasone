﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var netcashflowVm = function (netcashflow) {
        var self = this;

        this.netcashflow = netcashflow;

        this.cancel = function () {
        };


        //Period Compares Block

        this.sortedSimulations = ko.observableArray(self.netcashflow().netCashflowSimulationScenario());


        this.sortNetCashflowSimulations = function () {
            self.sortedSimulations(self.netcashflow().netCashflowSimulationScenario().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.netcashflow().netCashflowSimulationScenario().sort(function (a, b) { return a.priority() - b.priority(); });

        this.removeNetCashflowSimulationScenario = function (simScen) {

            var msg = 'Delete Simulation/Scenario/DateOffset  "' + simScen.priority() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    simScen.entityAspect.setDeleted();
                    netcashflow().netCashflowSimulationScenario().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortNetCashflowSimulations();

                    //if (cashflow().cashflowSimulationScenario().length < 3) {
                    //    $('#addSimulationScenario').removeClass('disabled');
                    //}
                }
            }

        };

        this.addSimulationscenario = function () {
            var newSimScen = globalContext.createNetCashflowSimulationScenario();
            newSimScen.priority(netcashflow().netCashflowSimulationScenario().length);
            netcashflow().netCashflowSimulationScenario().push(newSimScen);
            self.sortNetCashflowSimulations();
        };

        self.sortNetCashflowSimulations();

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.definedGroupings = ko.observableArray();

        this.reportSelections = ko.observableArray([{ id: 1, name: 'Investment' }, { id: 2, name: 'Loan' }, { id: 3, name: 'Funding Maturities' }]);

        this.activate = function (id) {
            return globalContext.getScenarioTypes(self.scenarioTypes).then(function () {
                return globalContext.getSimulationTypes(self.simulationTypes).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                            //AsOfDateOffset Handling (No-Institution Template)
                            return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, undefined).then(function () { }); //blank instName will just use profile institution
                        });
                    });
                });
            });
        };

        return this;
    };

    return netcashflowVm;
});
