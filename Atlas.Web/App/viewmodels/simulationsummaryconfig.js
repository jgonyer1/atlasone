﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var simSumVm = function (simSum) {
        var self = this;

        this.simSum = simSum;

        this.cancel = function () {
        };

        this.group = ko.observable();
        this.definedGroup = ko.observable();

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.reportOptions = ko.observableArray([
            { value: 0, name: "Balance Sheet" },
            { value: 1, name: "Rate/Yield Analysis" },
            { value: 2, name: "Asset/Liability Prepayments" },
            { value: 3, name: "Interest Income/Expense" }
        ]);

        this.viewOptions = ko.observable();
        //this.viewOptions = ko.observableArray([
        //    { title: "Sub Accounts", value: "0", disable: false },
        //    { title: "Masters", value: "1", disable: false },
        //    { title: "Classifications", value: "2", disable: false },
        //    { title: "Account Type", value: "3", disable: false }
        //]);

        this.setOptionDisable = function (option, item) {
            ko.applyBindingsToNode(option, { disable: item.disable }, item);
        }

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.profile = ko.observableArray();

        this.definedGroupings = ko.observableArray();


        this.updateViewOption = function () {
            sm().grouping(self.definedGroup());
            return true;
        }

        this.defaultDefinedGroup = function () {
            sm().grouping(self.definedGroupings()[0].id);
            return true;
        }

        this.disableGroups = function () {
            self.group(false);
            return true;
        }

        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.simSum().institutionDatabaseName(), self.simSum().asOfDateOffset(), self.simSum().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.simSum().institutionDatabaseName(), self.simSum().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }

        this.activate = function (id) {
            global.loadingReport(true);
            return globalContext.getAvailableSimulations(self.simulationTypes, self.simSum().institutionDatabaseName(), self.simSum().asOfDateOffset()).then(function () {
                return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.simSum().institutionDatabaseName(), self.simSum().asOfDateOffset(), self.simSum().simulationTypeId(), false).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                            //self.definedGroup();


                            return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.simSum().institutionDatabaseName()).then(function () {
                                console.log('Just got thest ones');

                                var arr = [];
                                arr.push({
                                    name: "Standard View", options: [
                                        { name: "Sub Accounts", id: "0" },
                                        { name: "Classifications", id: "2" },
                                        { name: "Account Type", id: "3" }
                                    ]
                                });
                                arr.push({
                                    name: "Defined Groupings", options: self.definedGroupings()
                                });
                                self.viewOptions(global.getGroupedSelectOptions(arr));


                                //for (var i = 0; i < self.definedGroupings().length; i++) {
                                //    if (i == 0) {
                                //        self.viewOptions().push({ title: "Defined Groupings", value: null, disable: true });
                                //    }
                                //    self.viewOptions().push({ title: self.definedGroupings()[i].name, value: self.definedGroupings()[i].id, disable: false });
                                //}
                                //if (simSum().grouping().indexOf('dg_') > -1) {
                                //    self.group(true);
                                //    self.definedGroup(simSum().grouping());
                                //}
                                //else {
                                //    self.group(false);
                                //}

                                //AsOfDateOffset Handling (Single-Institution Template)
                                self.instSub = self.simSum().institutionDatabaseName.subscribe(function (newValue) {
                                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue).then(function () {
                                        self.refreshSim();
                                    });
                                   
                                });

                                self.simSub = self.simSum().simulationTypeId.subscribe(function (newValue) {
                                    self.refreshScen();
                                });

                                self.aodSub = self.simSum().asOfDateOffset.subscribe(function (newValue) {
                                    self.refreshSim();
                                });

                                self.reportSelection = self.simSum().reportSelection.subscribe(function (newValue) {
                                    if (self.simSum().reportSelection() == 2) {
                                        self.simSum().grouping(0);
                                    }
                                });

                                global.loadingReport(false);

                            });
                        });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.simSub.dispose();
            self.reportSelection.dispose();
        };

        return this;
    };

    return simSumVm;
});
