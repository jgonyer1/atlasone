﻿define([
    'services/globalcontext',
    'services/global',
    'viewmodels/simcomparesimulationselectconfig',
    'durandal/app'
],
    function (globalContext, global, simcomparesimulationselectconfig, app) {
        var simCompareVm = function (simCompare) {
            
            var self = this;

            this.simCompare = simCompare;
            this.simCompareScenarioTypes = simCompare().simCompareScenarioTypes;
            this.modelSetup = ko.observable();
            this.leftPolicyOffsets = ko.observable();
            this.rightPolicyOffsets = ko.observable();
            this.leftAsOfDateOffsets = ko.observableArray();
            this.rightAsOfDateOffsets = ko.observableArray();

            this.getSortedSimulationTypes = function (sideNum) {
                var sideArr = self.simCompare().simCompareSimulationTypes().filter(function (s) { return s.side() == sideNum });
                sideArr = sideArr.sort(function (a, b) { return a.priority() - b.priority() });
                return sideArr;
            };

            //just do both sides
            this.sortSimulationTypes = function () {

                //fix up priorities first
                //var sideArr;
                //for (var i = 0; i < 2; i++) {
                //    sideArr = self.simCompare().simCompareSimulationTypes().filter(function (s) { return s.side() == i });
                //    sideArr.forEach(function (item, indx, arr) {
                //        item.priority(indx);
                //    });
                //}

                self.sortedLeftSimulationTypes(self.getSortedSimulationTypes(0));
                self.sortedRightSimulationTypes(self.getSortedSimulationTypes(1));

                self.sortedLeftSimulationTypes().forEach(function (item, indx, arr) {
                    item.priority(indx);
                });
                self.sortedRightSimulationTypes().forEach(function (item, indx, arr) {
                    item.priority(indx);
                });

            };

            this.addSimCompSimulationType = function (sideNum) {
                var simCompSimType = globalContext.createSimCompareSimulationType();
                var p = sideNum == 0 ? self.sortedLeftSimulationTypes().length : self.sortedRightSimulationTypes().length;

                simCompSimType.simulationTypes = ko.observableArray();

                globalContext.getAvailableSimulations(simCompSimType.simulationTypes, (sideNum == 0 ? self.simCompare().leftInstitutionDatabaseName() : self.simCompare().rightInstitutionDatabaseName()), simCompSimType.asOfDateOffset()).then(function () {
                    var aod;
                    if (sideNum == 0) {
                        aod = self.leftAsOfDateOffsets()[0].asOfDateDescript;
                        if (self.leftPolicyOffsets()[0][aod] != undefined) {
                            simCompSimType.simulationTypeId(self.leftPolicyOffsets()[0][aod][0].niiId);
                        } else {
                            simCompSimType.simulationTypeId(1);
                        }
                    } else {
                        aod = self.rightAsOfDateOffsets()[0].asOfDateDescript;
                        if (self.rightPolicyOffsets()[0][aod] != undefined) {
                            simCompSimType.simulationTypeId(self.rightPolicyOffsets()[0][aod][0].niiId);
                        } else {
                            simCompSimType.simulationTypeId(1);
                        }
                    }


                    self.subscriptions.push(simCompSimType.asOfDateOffset.subscribe(function (newVal) {
                        self.refreshItemSim(simCompSimType);
                    }, simCompSimType));

                    self.subscriptions.push(simCompSimType.simulationTypeId.subscribe(function (newVal) {
                        self.refreshScens();
                    }, simCompSimType));

                    simCompSimType.side(sideNum);
                    simCompSimType.priority(p);
                    simCompSimType.asOfDateOffset(0);
                    self.refreshItemSim(simCompSimType);
                    simCompare().simCompareSimulationTypes().push(simCompSimType);
                    self.sortSimulationTypes();
                });


            };

            this.removeSimCompSimulationType = function (simCompSimType) {
                var msg = "Delete Simulation Type?";
                var title = "Delete Simulation Type";
                var opts = ["Cancel", "Ok"];
                app.showMessage(msg, title, opts).then(function (r) {
                    if (r == opts[1]) {
                        self._deleteSimCompSimulationType(simCompSimType);
                    }
                });
            };
            this._deleteSimCompSimulationType = function (simCompSimType) {
                self.simCompare().simCompareSimulationTypes.remove(simCompSimType);
                simCompSimType.entityAspect.setDeleted();
                self.sortSimulationTypes();
            };

            this.enableSimulationDropdown = function (simCompSimType) {
                if (simCompSimType.priority() > 0) {
                    return true;
                } else if (simCompSimType.side() == 0) {
                    return self.simCompare().overrideLeftParentSimulationType();
                } else if (simCompSimType.side() == 1) {
                    return self.simCompare().overrideRightParentSimulationType();
                } else {
                    return true;
                }
            };


            this.sortedLeftSimulationTypes = ko.observableArray(self.getSortedSimulationTypes(0));
            this.sortedRightSimulationTypes = ko.observableArray(self.getSortedSimulationTypes(1));

            this.defaultLeftParentSimulationType = function () {
                var simTypes = self.getSortedSimulationTypes(0);
                var aod;
                if (simTypes[0] != undefined && simTypes[0] != null) {
                    if (!self.simCompare().overrideLeftParentSimulationType()) {

                        aod = self.leftAsOfDateOffsets()[simTypes[0].asOfDateOffset()].asOfDateDescript;
                        if (self.leftPolicyOffsets()[0][aod] != undefined) {
                            simTypes[0].simulationTypeId(self.leftPolicyOffsets()[0][aod][0].niiId);
                        }
                    }
                }
            };

            this.defaultRightParentSimulationType = function () {
                var simTypes = self.getSortedSimulationTypes(1);
                var aod;
                if (simTypes[0] != undefined && simTypes[0] != null) {
                    if (!self.simCompare().overrideRightParentSimulationType()) {
                        aod = self.rightAsOfDateOffsets()[simTypes[0].asOfDateOffset()].asOfDateDescript;
                        if (self.rightPolicyOffsets()[0][aod] != undefined) {
                            simTypes[0].simulationTypeId(self.rightPolicyOffsets()[0][aod][0].niiId);
                        }
                    }
                }
            };

            this.ovrdLeftParentSim = function () {
                var sideArr = self.simCompare().simCompareSimulationTypes().filter(function (s) { return s.side() == 0 });
                sideArr.forEach(function (item, indx, arr) {
                    self.enableSimulationDropdown(item);
                });
                self.defaultLeftParentSimulationType();
                self.refreshScens();
            };
            this.ovrdRightParentSim = function () {
                var sideArr = self.simCompare().simCompareSimulationTypes().filter(function (s) { return s.side() == 1 });
                sideArr.forEach(function (item, indx, arr) {
                    self.enableSimulationDropdown(item);
                });
                self.defaultRightParentSimulationType();
                self.refreshScens();
            };

            this.subscriptions = [];

            this.subscriptions.push(self.simCompare().comparativeScenario.subscribe(function () {

                //if base is not the comparative scenario
                //and if percent change w/ policies is selected
                if (self.simCompare().comparativeScenario() != "1" && self.simCompare().percentChange() == "2") {
                    self.simCompare().percentChange("1");
                }
            }));

            this.canShowDetails = ko.computed(function () {
                return !self.simCompare().isQuarterly();
            });

            //this.leftSimulationselectconfig = new simcomparesimulationselectconfig(
            //    this.simCompare().leftInstitutionDatabaseName,
            //    this.simCompare().leftAsOfDateOffset,
            //    simCompare().leftSimulationTypeId
            //    );

            //this.rightSimulationselectconfig = new simcomparesimulationselectconfig(
            //    this.simCompare().rightInstitutionDatabaseName,
            //    this.simCompare().rightAsOfDateOffset,
            //    simCompare().rightSimulationTypeId
            //    );

            this.scenarioConnectClass = "scenario-container";

            this.sortedSimCompareScenarioTypes = ko.observableArray(self.simCompareScenarioTypes());
            this.sortSimCompareScenarioTypes = function () {
                self.sortedSimCompareScenarioTypes(self.simCompareScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.addScenarioType = function (scenarioType) {
                var newSimCompareScenarioType = globalContext.createSimCompareScenarioType();
                newSimCompareScenarioType.scenarioTypeId(scenarioType.id);
                newSimCompareScenarioType.priority(simCompare().simCompareScenarioTypes().length);
                //newSimCompareScenarioType.scenarioType({name: ko.observable(scenarioType.name)});
                //newSimCompareScenarioType.scenarioTypeName = ko.observable(scenarioType.name);
                if (simCompare().simCompareScenarioTypes().length == 0) {
                    simCompare().comparativeScenario(scenarioType.id);
                }
                simCompare().simCompareScenarioTypes().push(newSimCompareScenarioType);
                self.sortSimCompareScenarioTypes();
            };

            //going to use this as a 'dontAddSimulationType' function for now
            this.dontAddScenarioType = function (data, event) {
                event.stopPropagation();
            };

            this.removeScenarioType = function (simCompareScenarioType) {
                var msg = 'Delete scenario ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        //self.simCompare().simCompareScenarioTypes.remove(simCompareScenarioType);
                        //delete simCompareScenarioType["scenarioType"];
                        //simCompareScenarioType.scenarioType(null);
                        simCompareScenarioType.entityAspect.setDeleted();
                        
                        self.simCompare().simCompareScenarioTypes().forEach(function (aSimCompareScenarioType, index) {
                            aSimCompareScenarioType.priority(index);
                        });
                        self.sortSimCompareScenarioTypes();
                    }
                }



            };

            //hopefully this will be a generic remove function
            this.removeFromLeftSimList = function (simCompareLeftSimulationType) {
                var msg = 'Delete simulation ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        simCompareLeftSimulationType.entityAspect.setDeleted();
                        self.simCompare().simCompareLeftSimulations().forEach(function (aSimCompareLeftSimulationType, index) {
                            aSimCompareLeftSimulationType.priority(index);
                        });
                        self.sortLeftSimCompareSimulationTypes();
                    }
                }
            };

            this.cancel = function () {
                self.sortSimCompareScenarioTypes();
            };

            self.sortSimCompareScenarioTypes();

            this.institutions = ko.observableArray();

            this.global = global;

            this.simulationTypes = ko.observableArray();


            this.scenarioTypes = ko.observableArray();

            this.percChangeOptions = [
                {
                    value: "0",
                    label: "None"
                },
                {
                    value: "1",
                    label: "% Change"
                },
                {
                    value: "2",
                    label: "% Change With Policies"
                }
            ];

            this.availablePercChangeOptions = ko.computed(function () {
                var retArr = [];
                retArr.push(self.percChangeOptions[0]);
                retArr.push(self.percChangeOptions[1]);
                if ((self.simCompare().comparativeScenario() == "1" || self.simCompare().comparativeScenario() == null) && self.simCompare().niiOption() == "nii") {
                    retArr.push(self.percChangeOptions[2]);
                }
                return retArr;
            });

            //order of events matters here, need to reset the compare dropdown before it gets disabled
            this.enablePercChangeCompareSel = ko.computed(function () {
                if (self.simCompare()) {
                    if (self.simCompare().percentChange() > 0) {
                        return true;
                    } else {
                        self.simCompare().percChangeComparator("year2Year1");
                        return false;
                    }

                }
            });

            var getSelectedSimulationName = function (simulationTypeId, side) {
                //side: 0 = left, 1 = right
                var s = 0;
                if (side == 0) {
                    for (s = 0; s < self.leftSimulationselectconfig.simulationTypes().length; s++) {
                        if (self.leftSimulationselectconfig.simulationTypes()[s].id() == simulationTypeId) {
                            return self.leftSimulationselectconfig.simulationTypes()[s].name();
                        }
                    }
                } else {
                    for (s = 0; s < self.rightSimulationselectconfig.simulationTypes().length; s++) {
                        if (self.rightSimulationselectconfig.simulationTypes()[s].id() == simulationTypeId) {
                            return self.rightSimulationselectconfig.simulationTypes()[s].name();
                        }
                    }
                }

            };
            var getAsOfDateOffsetLabel = function (offset, side) {
                //side: 0 = left, 1 = right
                if (side == 0) {
                    if (self.leftSimulationselectconfig.asOfDateOffsets().length > 0) {
                        return self.leftSimulationselectconfig.asOfDateOffsets()[offset].asOfDateDescript;
                    } else {
                        return "";
                    }
                } else {
                    if (self.rightSimulationselectconfig.asOfDateOffsets().length > 0) {
                        return self.rightSimulationselectconfig.asOfDateOffsets()[offset].asOfDateDescript;
                    } else {
                        return "";
                    }
                }


            };


            this.binding = function () {
                console.log(self.simCompare().simCompareSimulationTypes());
            };
            this.bindingComplete = function () {
                console.log(self.simCompare().simCompareSimulationTypes());
            };

            this.compositionComplete = function () {
                if (self.simCompare().id() > 0) {
                    globalContext.saveChangesQuiet();
                }
            }

            this.refreshItemSim = function (item) {
                globalContext.getAvailableSimulations(item.simulationTypes, (item.side() == 0 ? self.simCompare().leftInstitutionDatabaseName() : self.simCompare().rightInstitutionDatabaseName()), item.asOfDateOffset()).then(function () {
                    self.resetScensAvailableScens();
                });
            }

            this.resetScensAvailableScens = function () {
                self.refreshScens().then(function () {
                    console.log('setting them scenarios');
                    console.log(self.scenarioTypes());

                });
            }

            this.refreshScens = function () {
                //loop through all offsets and get sim type sand opffets into array and pass into function to get distinct scenarios across all of them
                var sims = [];
                var offsets = [];
                var eveSims = [];
                self.simCompare().simCompareSimulationTypes().forEach(function (item, index, arr) {

                    sims.push(item.simulationTypeId());
                    eveSims.push(item.simulationTypeId());
                    offsets.push(item.asOfDateOffset());

                });

                return globalContext.getAvailableSimulationScenariosMultipleInst(self.scenarioTypes, self.simCompare().leftInstitutionDatabaseName() + "," + self.simCompare().rightInstitutionDatabaseName(), offsets.join(), sims.join(), eveSims.join());

            }



            this.activate = function (id) {
                global.loadingReport(true);
                        return globalContext.getModelSetup(self.modelSetup).then(function () {
                            return globalContext.getPolicyOffsets(self.leftPolicyOffsets, self.simCompare().leftInstitutionDatabaseName()).then(function () {
                                return globalContext.getPolicyOffsets(self.rightPolicyOffsets, self.simCompare().rightInstitutionDatabaseName()).then(function () {
                                    return globalContext.getAsOfDateOffsets(self.leftAsOfDateOffsets, self.simCompare().leftInstitutionDatabaseName()).then(function () {
                                        return globalContext.getAsOfDateOffsets(self.rightAsOfDateOffsets, self.simCompare().rightInstitutionDatabaseName()).then(function () {
                                            return globalContext.getAvailableBanks(self.institutions).then(function () {
                                                //this should cover pre-existing simCompares
                                                if (self.simCompare().niiOption() == null || self.simCompare().niiOption() == "" || self.simCompare().niiOption() == undefined) {
                                                    self.simCompare().niiOption("nii");
                                                }
                                                if (self.simCompare().simCompareSimulationTypes().length == 0) {
                                                    self.addSimCompSimulationType(0);
                                                    self.addSimCompSimulationType(1);
                                                    self.sortSimulationTypes();
                                                }

                                                //Loop through and set simulatins as well as 
                                               


                                                //loop through offsets and set up simulationTypes array
                                                var offsetsProcessed = 0
                                                self.simCompare().simCompareSimulationTypes().forEach(function (item, index, arr) {

                                                    item.simulationTypes = ko.observableArray();
                                                    globalContext.getAvailableSimulations(item.simulationTypes, (item.side() == 0 ? self.simCompare().leftInstitutionDatabaseName() : self.simCompare().rightInstitutionDatabaseName()), item.asOfDateOffset()).then(function () {

                                                      
                                                        self.subscriptions.push(item.asOfDateOffset.subscribe(function (newVal) {
                                                            self.refreshItemSim(item);
                                                        }, item));

                                                        self.subscriptions.push(item.simulationTypeId.subscribe(function (newVal) {
                                                            self.refreshScens();
                                                        }, item));

                                                        offsetsProcessed++;
                                                        //when done this just move ona nd setorrest chagnes function
                                                        if (offsetsProcessed == arr.length) {
                                                            //self.refreshScens().then(function () {
                                                            self.refreshScens();
                                                            self.defaultLeftParentSimulationType();
                                                            self.defaultRightParentSimulationType();
                                                            // });
                                                            global.loadingReport(false);
                                                        }
                                                    });
                                                });



                                                //tax equivalent sync
                                                if (!self.simCompare().overrideTaxEquiv()) {
                                                    self.simCompare().taxEquivalentYields(self.modelSetup().reportTaxEquivalent());
                                                }

                                                self.subscriptions.push(self.simCompare().overrideLeftParentSimulationType.subscribe(self.ovrdLeftParentSim));
                                                self.subscriptions.push(self.simCompare().overrideRightParentSimulationType.subscribe(self.ovrdRightParentSim));

                                                self.subscriptions.push(self.simCompare().leftInstitutionDatabaseName.subscribe(function () {
                                                    globalContext.getPolicyOffsets(self.leftPolicyOffsets, self.simCompare().leftInstitutionDatabaseName()).then(function () {
                                                        self.defaultLeftParentSimulationType();
                                                        self.refreshScens();
                                                    });
                                                }));
                                                self.subscriptions.push(self.simCompare().rightInstitutionDatabaseName.subscribe(function () {
                                                    globalContext.getPolicyOffsets(self.rightPolicyOffsets, self.simCompare().rightInstitutionDatabaseName()).then(function () {
                                                        self.defaultRightParentSimulationType();
                                                        self.refreshScens();
                                                    });
                                                }));
                                                self.subscriptions.push(self.simCompare().overrideTaxEquiv.subscribe(function (newValue) {
                                                    if (!newValue) {
                                                        self.simCompare().taxEquivalentYields(self.modelSetup().reportTaxEquivalent());
                                                    }
                                                }));
                                                self.availableScenarioTypes = ko.computed(function () {
                                                    var scenarioTypeIdsInUse = self.simCompareScenarioTypes().map(function (simCompareScenarioType) {
                                                        return simCompareScenarioType.scenarioTypeId();
                                                    });

                                                    var result = [];
                                                    self.scenarioTypes().forEach(function (scenarioType) {
                                                        result.push({
                                                            id: scenarioType.id(),
                                                            name: scenarioType.name(),
                                                            inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                                        });
                                                    });
                                                    return result;
                                                });
                                                this.leftChartNameComp = ko.computed(function () {
                                                    var sideArr = self.getSortedSimulationTypes(0);
                                                    if (sideArr.length > 0 &&
                                                        !self.simCompare().leftNameOverride() &&
                                                        sideArr[0].simulationTypes != null &&
                                                        sideArr[0].simulationTypes != undefined &&
                                                        sideArr[0].simulationTypes() != null &&
                                                        sideArr[0].simulationTypes() != undefined &&
                                                        sideArr[0].simulationTypes().length > 0 &&
                                                        self.leftAsOfDateOffsets().length > 0) {
                                                        //var simulationTypeName = sideArr[0].simulationType().name();
                                                        var simulationTypeName = sideArr[0].simulationTypes().filter(function (s) { return s.id == sideArr[0].simulationTypeId() })[0].name;
                                                        var aodLabel = self.leftAsOfDateOffsets()[sideArr[0].asOfDateOffset()].asOfDateDescript;
                                                        var fullName = simulationTypeName + " Simulation as of " + aodLabel;
                                                        self.simCompare().leftGraphName(fullName);
                                                        //if (self.simCompare().leftGraphName() != fullName) {

                                                        //}

                                                    } else {
                                                        //self.simCompare().leftGraphName("");
                                                    }
                                                });
                                                this.rightChartNameComp = ko.computed(function () {
                                                    var sideArr = self.getSortedSimulationTypes(1);
                                                    if (sideArr.length > 0 &&
                                                        !self.simCompare().rightNameOverride() &&
                                                        sideArr[0].simulationTypes != null &&
                                                        sideArr[0].simulationTypes != undefined &&
                                                        sideArr[0].simulationTypes() != null &&
                                                        sideArr[0].simulationTypes() != null &&
                                                        sideArr[0].simulationTypes().length > 0 &&
                                                        self.rightAsOfDateOffsets().length > 0) {
                                                        //var simulationTypeName = sideArr[0].simulationType().name();
                                                        var simulationTypeName = sideArr[0].simulationTypes().filter(function (s) { return s.id == sideArr[0].simulationTypeId() })[0].name;
                                                        var aodLabel = self.rightAsOfDateOffsets()[sideArr[0].asOfDateOffset()].asOfDateDescript;
                                                        var fullName = simulationTypeName + " Simulation as of " + aodLabel;
                                                        self.simCompare().rightGraphName(fullName);
                                                        //if (self.simCompare().rightGraphName() != fullName) {

                                                        //}

                                                    } else {
                                                        //self.simCompare().rightGraphName(fullName);
                                                    }
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
            };
            self.deactivate = function () {
                for (var s = 0; s < self.subscriptions.length; s++) {
                    self.subscriptions[s].dispose();
                }
            };

            this.sortableAfterMove = function (arg) {
                arg.sourceParent().forEach(function (chartScenarioType, index) {
                    chartScenarioType.priority(index);
                });
            };

            return this;
        };

        return simCompareVm;
    });
