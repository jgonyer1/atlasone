﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var logger = require('services/logger');
    var global = require('services/global');
    var liqVm = function (id) {
        var self = this;
        var curPage = "LiquidityProjectionView";
        this.liq = ko.observable();
        this.topLeftTableSectionHeader = ko.pureComputed(function () {
            if (self.liq() && self.liq().liqProjSettings.projectionPeriod == "90Day") {
                return "90 Day Liquidity Forecast";
            } else {
                return "180 Day Liquidity Forecast";
            }
        });

        var chartObj = function (minMaxObj) {
            if (minMaxObj != undefined && (!minMaxObj.minVal || !minMaxObj.maxVal)) {
                logger.logError("minMax values are undefined");
            }

            return {
                chart: {
                    type: "column",
                    backgroundColor: '#FCFEEB'
                    },
                legend: {
                    enabled: true,
                    style: {
                        color: '#000000',
                        fontWeight: 'normal',
                        font: 'Open Sans',
                        marginBottom: 2,
                        fontSize: '7px'
                        },
                },
                colors: charting.chartColorsPalette2,
                title: {
                    text: null
                },
                yAxis: {
                    min: minMaxObj.minVal,
                    max: minMaxObj.maxVal,
                    title: {
                        text: null
                    },
                    labels: {
                        enabled: true,
                        useHTML: true
                    }
                },
                xAxis: {
                    categories: [],
                    labels: {
                        rotation: 0,
                        style: {
                            color: '#000000',
                            fontWeight: 'normal',
                            font: 'Open Sans',
                            fontSize: '8px'
                        },
                    }
                },
                series: []
            };
        };

        this.blankIfNull = function (value) {
            if (value == null) {
                return "";
            }
            else {
                return numeral(value).format("0,0");
            }
        };

        //takes an array of arrays
        var getMinMax = function (arrays) {
            var minVal = 1000000000, maxVal = -1000000000;
            var i = 0;
            var a = 0;

            for (a = 0; a < arrays.length; a++) {
                for (i = 0; i < arrays[a].length; i++) {
                    if (arrays[a][i] != undefined) {
                        if (arrays[a][i] > maxVal) {
                            maxVal = arrays[a][i];
                        }
                        if (arrays[a][i] < minVal) {
                            minVal = arrays[a][i];
                        }
                    }
                }
            }

            return { minVal: minVal, maxVal: maxVal }
        }

        this.tableFootnote = function () {
            var fn = "Excludes";
            var exclArr = [];
            if (!self.liq().liqProjSettings.includePublicDep) {
                exclArr.push("Public");
            }
            if (!self.liq().liqProjSettings.includeNationalDep) {
                exclArr.push("National");
            }
            if (!self.liq().liqProjSettings.includeBrokeredDep) {
                exclArr.push("Brokered");
            }
            if (!self.liq().liqProjSettings.includeBrokeredRecip) {
                exclArr.push("Brokered Reciprocal");
            }
            if (!self.liq().liqProjSettings.includeBrokeredOneWay) {
                exclArr.push("Brokered One-Way");
            }
            if (!self.liq().liqProjSettings.includeSecuredRetail) {
                exclArr.push("Retail Repos");
            }
            if (exclArr.length > 0) {
                if (exclArr.length > 1) {
                    exclArr[exclArr.length - 1] = "and " + exclArr[exclArr.length - 1];
                }
                return "*Excludes " + exclArr.join(", ");
            } else {
                return "";
            }
        };

        this.compositionComplete = function () {
            if (self.liq()) {
                var arrs = [self.liq().topRightLeftChartSeriesActuals.data,
                    self.liq().topRightRightChartSeriesActuals.data,
                    self.liq().topRightLeftChartSeriesForecast.data,
                    self.liq().topRightRightChartSeriesForecast.data
                ];
                var minMax = getMinMax(arrs);

                var topRightLeftChart = new chartObj(minMax);
                topRightLeftChart.series.push(self.liq().topRightLeftChartSeriesActuals);
                topRightLeftChart.series.push(self.liq().topRightLeftChartSeriesForecast);
                topRightLeftChart.xAxis.categories = self.liq().topRightChartCategories;
                
                var topRightRightChart = new chartObj(minMax);
                topRightRightChart.series.push(self.liq().topRightRightChartSeriesActuals);
                topRightRightChart.series.push(self.liq().topRightRightChartSeriesForecast)
                topRightRightChart.xAxis.categories = self.liq().topRightChartCategories;
                topRightRightChart.yAxis.labels.enabled = false;

                arrs = [
                    self.liq().middle_leftChartSeries1.data,
                    self.liq().middle_leftChartSeries2.data,
                    self.liq().middle_rightChartSeries1.data,
                    self.liq().middle_rightChartSeries2.data
                ];
                minMax = { minVal: null, maxVal: null };

                var middleLeftChart = new chartObj(minMax);
                middleLeftChart.series.push(self.liq().middle_leftChartSeries1);
                middleLeftChart.series.push(self.liq().middle_leftChartSeries2);
                middleLeftChart.xAxis.categories = self.liq().middleChartCategories;
                var middleRightChart = new chartObj(minMax);
                middleRightChart.series.push(self.liq().middle_rightChartSeries1);
                middleRightChart.series.push(self.liq().middle_rightChartSeries2);
                middleRightChart.xAxis.categories = self.liq().middleChartCategories;

                //get the rows the same height
                //use the top right table as the stnadard
                var height = $("#top_left_tbl_container").height();
                $("#topRight_leftChart").height(height - $("#top_right_left_chart_title").height());
                $("#topRight_rightChart").height(height - $("#top_right_right_chart_title").height());
                $("#middle_left_chart").height(height - $("#middle_section_title").height());
                $("#middle_right_chart").height(height - $("#middle_section_title").height());

                $("#topRight_leftChart").highcharts(topRightLeftChart);
                $("#topRight_rightChart").highcharts(topRightRightChart);
                $("#middle_left_chart").highcharts(middleLeftChart);
                $("#middle_right_chart").highcharts(middleRightChart);
            }

            global.loadingReport(false);
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage); 
            }
            else {
                $("#liq-proj-view").height($("#liq-proj-view").height());
                $('#reportLoaded').text('1');
                hiqPdfConverter.startConversion();
                //setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getLiquidityProjectionViewDataById(id, self.liq).then(function () {
                //globalContext.reportErrors(self.liq().errors, self.liq().warnings);
            });
        };
    };

    return liqVm;
});
