﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var balanceSheetMixVm = function (balanceSheetMix) {
        var self = this;
        var curPage = "BalanceSheetMixConfig";
        this.balanceSheetMix = balanceSheetMix;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();
        this.policyOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();
        this.priorSimulationTypes = ko.observableArray();

        this.defaultSimulation = function () {
            if (!self.balanceSheetMix().overrideSimulationId()) {
                var aod = self.asOfDateOffsets()[self.balanceSheetMix().asOfDateOffset()].asOfDateDescript;
                if (self.policyOffsets()[0][aod]) {
                    self.balanceSheetMix().simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                }
                
            }

            if (!self.balanceSheetMix().overridePriorSimulationId()) {
                //default to zero???
                if (self.asOfDateOffsets().length < self.balanceSheetMix().priorAsOfDateOffset()) {
                    self.balanceSheetMix().priorAsOfDateOffset(0);
                }
                var aod = self.asOfDateOffsets()[self.balanceSheetMix().priorAsOfDateOffset()].asOfDateDescript;
                if (self.policyOffsets()[0][aod]) {
                    self.balanceSheetMix().priorSimulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                }
                
            }
        }



        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.balanceSheetMix().institutionDatabaseName(), self.balanceSheetMix().asOfDateOffset()).then(function () {});
        }

        this.refreshPriorSim  = function () {
            globalContext.getAvailableSimulations(self.priorSimulationTypes, self.balanceSheetMix().institutionDatabaseName(), self.balanceSheetMix().priorAsOfDateOffset()).then(function () { });
        }




        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getAvailableSimulations(self.simulationTypes, self.balanceSheetMix().institutionDatabaseName(), self.balanceSheetMix().asOfDateOffset()).then(function () {
                return globalContext.getAvailableSimulations(self.priorSimulationTypes, self.balanceSheetMix().institutionDatabaseName(), self.balanceSheetMix().priorAsOfDateOffset()).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {


                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.balanceSheetMix().institutionDatabaseName()).then(function () {
                            return globalContext.getPolicyOffsets(self.policyOffsets, self.balanceSheetMix().institutionDatabaseName()).then(function () {
                                self.defaultSimulation();

                                //AsOfDateOffset Handling (Single-Institution Template)
                                self.instSub = self.balanceSheetMix().institutionDatabaseName.subscribe(function (newValue) {

                                    return Q.all([
                                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue),
                                        globalContext.getPolicyOffsets(self.policyOffsets, newValue)
                                    ]).then(function () {
                                        self.refreshPriorSim();
                                        refreshSim
                                        //Left Simulation Sync
                                        self.defaultSimulation();
                                    });
                                });

                                self.aodSub = self.balanceSheetMix().asOfDateOffset.subscribe(function (newValue) {
                                    self.refreshSim();
                                    self.defaultSimulation();
                                });

                                self.priorAodSub = self.balanceSheetMix().priorAsOfDateOffset.subscribe(function (newValue) {
                                    self.refreshPriorSim();
                                    self.defaultSimulation();
                                });

                                self.overrideSimSub = self.balanceSheetMix().overrideSimulationId.subscribe(function (newValue) {
                                    self.defaultSimulation();
                                });

                                self.priorOverrideSimSub = self.balanceSheetMix().overridePriorSimulationId.subscribe(function (newValue) {
                                    self.defaultSimulation();
                                });


                                if (self.balanceSheetMix().id() > 0) {
                                    globalContext.saveChangesQuiet();
                                }
                                global.loadingReport(false);
                            });
                        });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.overrideSimSub.dispose();
            self.priorAodSub.dispose();
            self.priorOverrideSimSub.dispose();
        };

        return this;
    };

    return balanceSheetMixVm;
});
