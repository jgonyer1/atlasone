﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/app',
        'services/logger',
        'services/model'
],


        function (globalContext, router, app, logger, model) {

        var asOfDates = ko.observable();
        var asOfDate = ko.observable();
        var profile = ko.observable();

        function activate(id) {
            //Get As Of Date To Copy Package
            return globalContext.getUserProfile(profile).then(function () {
                return globalContext.getAsOfDates(asOfDates).then(function () {
                    asOfDate(asOfDates()[0].asOfDateDescript);
                });
            });

        }

        var dateChange = function () {

        }

        var cancel = function () {
            globalContext.cancelChanges();
            router.navigate('#/packages/');
        };

        var roll = function () {
            model.isLoading(true);
            globalContext.rollDate(asOfDate()).fin(function () {
                model.isLoading(false);
                //Navigate to home and will now be in the date the rolled to.
                router.navigate('#');
            });
        };
        
        var vm = {
            activate: activate,
            roll: roll,
            cancel: cancel,
            asOfDates: asOfDates,
            profile: profile,
            asOfDate: asOfDate,
            dateChange: dateChange
        };

        return vm;

        //#region Internal Methods

        
        //#endregion

    });
