﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ivpVm = function (id) {
        var self = this;
        var curPage = "InvestmentErrorView";
        this.ivpViewData = ko.observable();

        this.compositionComplete = function () {
            if (self.ivpViewData().errors.length > 0 || self.ivpViewData().warnings.length > 0) {
                globalContext.reportErrors(self.ivpViewData().errors, self.ivpViewData().warnings);
            }
            var tbl = self.ivpViewData().tblData;
           

           for (var i = 0; i < tbl.length; i++) {
               var rec = tbl[i];
               var row = '<tr>';
               row += '<td>' + rec.cusip + '</td>';
               row += '<td>' + numeral(rec.parValue).format('0,0') + '</td>';
               row += '<td>' + numeral(rec.bookValue).format('0,0') + '</td>';
               row += '<td>' + numeral(rec.bookPrice).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.marketPrice).format('0.00') + '</td>';
               row += '<td>' + rec.basisIntent + '</td>';
               row += '<td>' + numeral(rec.yield).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.currentCoupon).format('0.00') + '</td>';           
               row += '<td>' + moment(rec.maturityDate).format('MM/DD/YYYY') + '</td>';
               row += '<td>' + rec.couponType + '</td>';
               row += '<td>' + rec.masterIndex + '</td>';
               row += '<td>' + numeral(rec.mastermargin).format('0.00') + '</td>';

               if (rec.masterDate != null) {
                   row += '<td>' + moment(rec.masterDate).format('MM/DD/YYYY') + '</td>';
               }
               else {
                   row += '<td>-</td>';
               }
             
               row += '<td>' + numeral(rec.masterResetFreq).format('0') + '</td>';
               row += '<td>' + numeral(rec.masterPerCap).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.masterLifeCap).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.masterLifeFloor).format('0.00') + '</td>';

               row += '</tr>';

               $('#invBody').append(row);
           }

           $('#report-view').height($('#report-view').height());
           global.loadingReport(false);
           $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage); 
            }
            else {
                hiqPdfConverter.startConversion();
            }



        }



        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getInvestmentErrorViewDataById(id, self.ivpViewData).then(function () {

            });
        };
    };

    return ivpVm;

});
