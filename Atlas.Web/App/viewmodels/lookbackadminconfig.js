﻿define([
    'services/globalcontext',
    'services/global',
    'plugins/router',
    'durandal/app',
    'services/logger',
    'viewmodels/navigateawaymodal',
    './inputmodal'
],
function (globalContext, global, router, app, logger, naModal, inputModal) {
    var vm = function () {
        var self = this;
        var curPage = "LookbackAdminConfig";
        this.profile = ko.observable();
        //Vars
        this.layouts = ko.observableArray();
        this.allLayouts = ko.observableArray();
        this.types = ko.observableArray();
        this.lookbacks = ko.observableArray();
        this.isSaving = ko.observable(false);
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.LBTypeOneId = 0;
        this.LB1Type;

        //View
        this.connectClass = "scenario-container";

        //Navigation Helpers
        this.hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges();
        });

        var offsetDateCheck = function (type, index, array) {
            return type.startDateOffset() > type.endDateOffset();
        };

        this.passesDateOffsetCheck = ko.computed(function () {
            return self.types().every(offsetDateCheck);
        });

        this.canSave = ko.pureComputed(function () {
            return self.hasChanges() && !self.isSaving() && self.passesDateOffsetCheck();
        });

        this.canDeactivate = function () {
            if (! self.hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { self.save(); }
            );
        };

        this.compositionComplete = function () {
            router.restoreNavigationParams();
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        this.goToLayoutConfig = function (layout) {
            router.setNavigationParams({ tabs: ['layouts'] });
            router.navigate("#/lookbackcustomlayoutconfig/" + layout.id());
        };

        this.goToLookbackConfig = function (lookback) {
            router.setNavigationParams({ tabs: ['lookbacks'] });
            router.navigate("#/lookbackconfig/" + lookback.id());
        };

        //We don't need a separate config screen for the types

        this.cancel = function () {
            self.isSaving(true);
            globalContext.cancelChanges();
            self.isSaving(false);

            //self.sortTypes();
        };

        this.save = function () {
            self.isSaving(true);

            //if we've changed something about a lookback custom type, go and update the startOffset and endOffset for each LBLookbackGroup that uses it
            var changes = globalContext.getChanges();
            var customTypeIdsChanged = [];
            var p = 0, l = 0;
            for (var p in changes) {
                if (changes[p].constructor.name == "LBCustomType__Atlas_Institution_Model") {
                    for (var l = 0; l < self.lookbacks().length; l++) {
                        if (self.lookbacks()[l].lookbackGroups()[0].lBCustomTypeId() == changes[p].id()) {
                            self.lookbacks()[l].lookbackGroups()[0].startDateOffset(changes[p].startDateOffset());
                            self.lookbacks()[l].lookbackGroups()[0].endDateOffset(changes[p].endDateOffset());
                        }
                    }
                }
            }
            
            return globalContext.saveChanges().fin(complete);

            function complete() {
                self.isSaving(false);
            }
        };

        //Add / Remove
        this.addLayout = function () {
            var newLayout = globalContext.createLBCustomLayout();
            newLayout.name("New Custom Layout");
            newLayout.userAdded(true);
            newLayout.priority(self.layouts().length);
            self.layouts.push(newLayout);
            //self.sortTypes();

            //Just automatically save and advance
            self.save().then(function () {
                self.goToLayoutConfig(newLayout);
            });

            return newLayout;
        };
        this.addType = function () {
            var newType = globalContext.createLBCustomType();
            newType.userAdded(true);
            newType.priority(self.types().length);
            self.types.push(newType);
            //self.save();
        };
        this.addLookback = function () {
            var title = "New Lookback";
            inputModal.show({
                title: title,
                message: 'Please choose a name for the new Lookback',
                hideCancel: false,
                validation: function (value) {
                    var fvalue = value.toLowerCase().trim();
                    var lookbacks = self.lookbacks();

                    for (var i = 0; i < lookbacks.length; i++) {
                        if (lookbacks[i].name().toLowerCase().trim() == fvalue) {
                            return {
                                success: false,
                                title: title,
                                message: 'There is a conflicting Lookback with name ' + value
                            };
                        }
                    }

                    return { success: true };
                }
            }).then(function (lbName) {
                if (lbName === null) {
                    self.cancel();
                    return;
                }

                var newLookback = globalContext.createLBLookback();
                var lbGroup = globalContext.createLBLookbackGroup();

                lbGroup.lBCustomTypeId(self.LB1Type.id());
                lbGroup.layoutId(self.allLayouts()[0].id());
                lbGroup.startDateOffset(self.LB1Type.startDateOffset());
                lbGroup.endDateOffset(self.LB1Type.endDateOffset());
                lbGroup.startDate(self.asOfDateOffsets()[lbGroup.startDateOffset()].asOfDateDescript);
                lbGroup.endDate(self.asOfDateOffsets()[lbGroup.endDateOffset()].asOfDateDescript);
                newLookback.priority(self.lookbacks().length);
                newLookback.name(lbName);
                newLookback.asOfDate(self.profile().asOfDate);
                newLookback.lookbackGroups().push(lbGroup);
                self.lookbacks.push(newLookback);
                //self.sortTypes();

                //Just automatically save and advance
                self.save().then(function () {
                    self.goToLookbackConfig(newLookback);
                });

                return newLookback;
            });
            
        };
        this.dontAddType = function (data, event) {
            event.stopPropagation();
        };
        this.deleteLookback = function (lb, showPrompt) {

            var msg = 'Delete Lookback "'+ lb.name() +'"?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    //globalContext.deleteLookback(lb.id());
                    for (var lbg = lb.lookbackGroups().length-1; lbg >= 0; lbg--) {
                        for (var lbd = lb.lookbackGroups()[lbg].lBLookbackData().length -1; lbd >= 0; lbd--) {
                            lb.lookbackGroups()[lbg].lBLookbackData()[lbd].entityAspect.setDeleted();
                        }
                        lb.lookbackGroups()[lbg].entityAspect.setDeleted()
                    }
                    lb.entityAspect.setDeleted();
                    //self.lookbacks(self.lookbacks().filter(function (item, index, arr) { return !(item.entityAspect.entityState == "Deleted" || item.entityAspect.entityState == "Detached") }));
                    
                    self.activate();
                    self.save();
                   
                }
            }
            
        };
        this.deleteLayout = function (layout) {

            var msg = 'Delete Layout "' + layout.name() + '"?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    //globalContext.deleteLayout(layout.id());
                    for (var ri = layout.rowItems().length - 1; ri >= 0; ri--) {
                        for (var cl = layout.rowItems()[ri].lBCustomLayoutClassifications().length - 1; cl >= 0; cl--){
                            layout.rowItems()[ri].lBCustomLayoutClassifications()[cl].entityAspect.setDeleted();
                        }
                        layout.rowItems()[ri].entityAspect.setDeleted();
                    }
                    
                    
                    layout.entityAspect.setDeleted();
                    //self.activate().then();
                    globalContext.saveChangesQuiet().then(function () {
                        self.activate();
                    });
                    //self.layouts(self.layouts().filter(function (item, index, arr) { return !(item.entityAspect.entityState == "Deleted" || item.entityAspect.entityState == "Detached") }));
                    //
                    //globalContext.saveChangeQuiet().fin(function () {
                    //    
                    //});
                    
                    

                }
            }
            
        }
        this.deleteType = function (obj) {

            var msg = 'Delete Layout "' + obj.name() + '"?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    obj.entityAspect.setDeleted();
                    self.types(self.types().filter(function (item, index, arr) { return !(item.entityAspect.entityState == "Deleted" || item.entityAspect.entityState == "Detached") }));

                    self.save();

                }
            }
            
        }
        this.layoutBeforeMove = function () {
            console.log("layout before move function");
        };
        this.layoutAfterMove = function () {
            for (var i = 0; i < self.layouts().length; i++) {
                self.layouts()[i].priority(i);
            }
        };
        this.typeBeforeMove = function () {
            console.log("type before move function");
        };
        this.typeAfterMove = function () {
            for (var i = 0; i < self.types().length; i++) {
                self.types()[i].priority(i);
            }
        };
        this.lookbackBeforeMove = function () {
            console.log("lookback before move function");
        };
        this.lookbackAfterMove = function () {
            for (var i = 0; i < self.lookbacks().length; i++) {
                self.lookbacks()[i].priority(i);
            }
        };
        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            //Basic Surplus
            return globalContext.getUserProfile(self.profile).then(function () {
                return globalContext.getLBCustomLayouts(self.allLayouts).then(function () {
                    return globalContext.getLBCustomTypes(self.types).then(function () {
                        return globalContext.getLBLookbacks(self.lookbacks, self.profile().asOfDate).then(function () {
                            self.layouts(self.allLayouts().filter(function (item, index, arr) { return item.userAdded(); }));
                            
                            //before we filter the lookback types, get the ID of Default Type LB1
                            var lb1Type = self.types().filter(function (i, ind, arr) {
                                return !i.userAdded() && i.name() == 'LB1';
                            });
                            if (lb1Type.length > 0) {
                                self.LB1Type = lb1Type[0];
                                self.LBTypeOneId = lb1Type[0].id();
                            }
                            //self.LBTypeOneId = lb1Type[0].id();
                            self.types(self.types().filter(function (item, index, arr) { return item.userAdded(); }));

                            //AsOfDateOffset Handling (No-Institution Template)
                            return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, undefined).then(function () { }); //blank instName will just use profile institution
                        });
                    });
                });
            });
        };

        return this;
    };

    return vm;
});
