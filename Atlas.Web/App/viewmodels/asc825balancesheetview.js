﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id) {
        var self = this;
        this.ascViewData = ko.observable();

        this.compositionComplete = function () {

            var tbl = self.ascViewData().tableData;

            for (var i = 0; i < tbl.length; i++) {
                var rec = tbl[i];
                if (numeral(rec.bookBalance).value() != 0 || numeral(rec.mVAmount).value() != 0) {
                    var row = '<tr>';

                    if (rec.cat_type != '0' && rec.cat_type != '1' && rec.cat_type != '5') {

                        var row = '<tr class="total-step">';
                    }

                    row += '<td>' + rec.name + '</td>';
                    if (rec.cat_type == '0' || rec.cat_type == '1' || rec.cat_type == '5') {
                        row += '<td>' + (rec.rate != 0 ? numeral(rec.rate).format('(0.00)') : '--') + '</td>';
                        row += '<td>' + (rec.mVFuncCost != 0 ? numeral(rec.mVFuncCost).format('(0.00)') : '--') + '</td>';
                        row += '<td>' + (rec.discount != 0 ? numeral(rec.discount).format('(0.00)') : '--') + '</td>';
                    }
                    else {
                        row += '<td></td>';
                        row += '<td></td>';
                        row += '<td></td>';
                    }

                    row += '<td>' + (rec.bookBalance != 0 ? numeral(rec.bookBalance / 1000).format('(0,0)') : '--') + '</td>';
                    row += '<td>' + (rec.parValue != 0 ? numeral(rec.parValue / 1000).format('(0,0)') : '--') + '</td>';
                    row += '<td>' + (rec.mVAmount != 0 ? numeral(rec.mVAmount / 1000).format('(0,0)') : '--') + '</td>';
                    //

                    //Becuase I am lazy
                    try {
                        if (rec.cat_type == '0' || rec.cat_type == '1' || rec.cat_type == '5') {
                            row += '<td>' + (rec.marketPrice != 0 ? numeral(rec.marketPrice).format('(0.00)') : '--') + '</td>';
                        }
                        else {
                            row += '<td>' + (rec.mVAmount != 0 ? numeral(rec.mVAmount / rec.parValue).format('(0.00%)') : '--') + '</td>';
                        }

                    }
                    catch (err) {
                        row += '<td>--</td>';
                    }

                    row += '<td>' + (rec.gain != 0 ? numeral(rec.gain / 1000).format('(0,0)') : '--') + '</td>';

                    if (rec.cat_type == '0' || rec.cat_type == '1' || rec.cat_type == '5') {
                        row += '<td>' + rec.discountIndex + '</td>';
                        if (numeral(rec.discountOffset).value() != 0) {
                            row += '<td>' + numeral(rec.discountOffset).format('(0)') + '</td>';
                        }
                        else {
                            row += '<td>--</td>';
                        }

                    }
                    else {
                        row += '<td></td>';
                        row += '<td></td>';
                    }

                    row += '</tr>';

                    $('#ascBody').append(row);
                }

            }

            global.loadingReport(false);

            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                $('.report-view').height($('.report-view').height());
                var h = globalContext.getMultiPageHeight($('.report-view').height(), $('#footnoteContainer').height());


                $('.report-view').height(h);
                //$('.report-wrapper').height(h);

                $('.report-view').css('padding-bottom', '0');
                $('#footnoteContainer').height($('#footnoteContainer').height());
                hiqPdfConverter.startConversion();
            }

        }

        this.activate = function () {
            globalContext.logEvent("Viewed", "ASC825balancesheetview");
            global.loadingReport(true);
            return globalContext.getASC825BalanceSheetViewDataById(id, self.ascViewData).then(function () {
                var viewData = self.ascViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
            });
        };
    };

    return ascVm;

});