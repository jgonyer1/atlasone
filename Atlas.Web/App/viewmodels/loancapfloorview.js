﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');

    var ctor = function (id, reportView) {
        this.lcfId = id;
        this.reportView = reportView;
        this.lcf = ko.observable();
        this.report = ko.observable();
        this.lcfViewData = ko.observable();
        this.highChartOptions = ko.observable();
        this.eveRatioHighChartOptions = ko.observable();
        this.evePcfzsHighChartOptions = ko.observable();
        this.eveRatioBpcfzsHighChartOptions = ko.observable();
        this.eveHighChartOptions = ko.observable();
        this.zeroShockIndex = ko.observable();
    };



    ctor.prototype.compositionComplete = function () {
        var self = this;
        var curPage = "LoanCapFloorView";
        if (self.lcfViewData().errors.length > 0 || self.lcfViewData().warnings.length > 0) {
            globalContext.reportErrors(self.lcfViewData().errors, self.lcfViewData().warnings);
        }
        //Set Titles
        $('#topTitle').text(self.lcfViewData().topTitle);
        $('#bottomTitle').text(self.lcfViewData().bottomTitle);
        $('#compTitle').text(self.lcfViewData().diffTitle);

        var bps = self.lcf().bpEndpoints().split(',');
        var breakOutsHeadersOutMoney = [];
        var breakOutsHeadersInMoney = [];
        for (var z = bps.length - 1; z >= 0; z--) {

            if (z == bps.length - 1) {
                breakOutsHeadersOutMoney.push('>' + bps[z] + ' BP');
                breakOutsHeadersInMoney.push('>' + bps[z] + ' BP');
            }
            else {
                breakOutsHeadersOutMoney.push('' + (parseInt(bps[z]) + 1) + '-' + bps[z + 1] + ' BP');
                breakOutsHeadersInMoney.push('' + bps[z + 1] + '-' + (parseInt(bps[z]) + 1) + ' BP');
            }
        }
        breakOutsHeadersOutMoney.push('1-' + bps[0] + ' BP');
        breakOutsHeadersInMoney.push('' + bps[0] + '-1 BP');


        var topObj = ProcessTable('top', self.lcfViewData().topTable, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, self.lcf().type());
        var bottomObj = ProcessTable('bottom', self.lcfViewData().bottomTable, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, self.lcf().type());

        var comp = {
            Floating: {
                indexes: {},
                indexArray: [],
                indexCount: 0,
                cols: new Array(16),
            },
            Adjustable: {
                indexes: {},
                indexArray: [],
                indexCount: 0,
                cols: new Array(16),
            }
        };


        //DEfault Totals Array
        comp['Floating'].cols[1] = 0;
        comp['Adjustable'].cols[1] = 0;
        for (var c = 2; c < 17; c++) {
            comp['Floating'].cols[c] = 0;
            comp['Adjustable'].cols[c] = 0;
        }

        comp['Floating'].cols[0] = 'Total';
        comp['Adjustable'].cols[0] = 'Total';

        //Loop through top first and populate indexes
        for (var fa in comp) {


            //loop through top obj fa
            for (var topIdx = 0; topIdx < topObj[fa].indexArray.length; topIdx++) {
                //Push onto comp fa array
                comp[fa].indexArray.push(topObj[fa].indexArray[topIdx]);
                comp[fa].indexCount += 1;
            }

            //Loop through bottom now but first need to make sure we are not pushing duplicatesin for loop
            for (var botIdx = 0; botIdx < bottomObj[fa].indexArray.length; botIdx++ ) {
                var foundIdx = comp[fa].indexArray.findIndex(obj => obj.indexName === bottomObj[fa].indexArray[botIdx].indexName);

                if (foundIdx < 0) {
                    comp[fa].indexArray.push(bottomObj[fa].indexArray[botIdx]);
                    comp[fa].indexCount += 1;
                }
            }


            //Now that we have all possible indexes loop through set and do look up on bottomObj and topObj

            for (var compIdx = 0; compIdx < comp[fa].indexArray.length; compIdx++) {
                var compRec = comp[fa].indexArray[compIdx];
                var noMatch = false;

                var topRec;
                var bottomRec;

                try {
                    topRec = topObj[fa].indexes[compRec.indexName];
                    bottomRec = bottomObj[fa].indexes[compRec.indexName];

                    if (topRec == undefined) {
                        topRec = {
                            cols: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                            indexName: compRec.indexName
                        };
                        noMatch = true;
                    }

                    if (bottomRec == undefined) {
                        bottomRec = {
                            cols: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                            indexName: compRec.indexName
                        }
                        noMatch = true;
                    }

                    //Now set values of comp record
                    for (var c = 1; c < compRec.cols.length; c++) {
                        topN = numeral(topRec.cols[c]).value();
                        bottomN = numeral(bottomRec.cols[c]).value();

                        //If rate variance check if one or the other is null if that is the case then its -- otherwise its the difference
                        if (c == 1 && noMatch) {
                            comp[fa].indexArray[compIdx].cols[c] = "--";
                        }
                        else {
                            comp[fa].indexArray[compIdx].cols[c] = numeral(topN - bottomN).value();
                            comp[fa].cols[c] += comp[fa].indexArray[compIdx].cols[c];
                        }
                    }

                }
                catch (ex) {

                }
  

            }



            //Sort index array now by sequence
            comp[fa].indexArray.sort(function (a, b) { return a.seq - b.seq });

        }

        //Do these defaults for reporting

        if (comp['Floating'].indexCount > 0) {
            comp['Floating'].indexes['Total'] = { cols: comp['Floating'].cols };
            comp['Floating'].indexes['Total'].cols[1] = "";
        }
        if (comp['Adjustable'].indexCount > 0) {
            comp['Adjustable'].indexes['Total'] = { cols: comp['Adjustable'].cols };
            comp['Adjustable'].indexes['Total'].cols[1] = "";
        }


        BuildTable('comp', comp, true, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, self.lcf().type());

        global.loadingReport(false);
        if (typeof hiqPdfInfo == "undefined") {
            globalContext.logEvent("Viewed", curPage); 
        }
        else {
            //global.correctPDFTable("loancapfloor-view");
            var h = $('#loancapfloor-view').height();
            if (h > 806) {
                $('#lastTable').parent().addClass('pageBreakBefore');
            }
            $('#reportLoaded').text('1');
            //hiqPdfConverter.startConversion();
            setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);
        }
    }

    function ProcessTable(position, tbl, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, cap) {
        var tp = {
            Floating: {
                indexes: {},
                indexCount: 0,
                cols: new Array(16),
                indexArray: []
            },
            Adjustable: {
                indexes: {},
                indexCount: 0,
                cols: new Array(16),
                indexArray: []
            }
        };

        tp['Floating'].cols[1] = '';
        tp['Adjustable'].cols[1] = '';

        //DEfault Totals Array
        for (var c = 2; c < 17; c++) {
            tp['Floating'].cols[c] = 0;
            tp['Adjustable'].cols[c] = 0;
        }

        tp['Floating'].cols[0] = 'Total';
        tp['Adjustable'].cols[0] = 'Total';
        for (var i = 0; i < tbl.length; i++) {
            var rec = tbl[i];
            if (tp[rec.floatAdjust].indexes[rec.indexName] == undefined) {


                tp[rec.floatAdjust].indexArray.push({
                    cols: new Array(16),
                    seq: rec.sequence,
                    indexName: rec.indexName
                });

                //Array Struff new way to swe can sort compare table
                var newIdx = tp[rec.floatAdjust].indexArray.length - 1;

                tp[rec.floatAdjust].indexes[rec.indexName] = {
                    cols: new Array(16),
                    seq: rec.sequence,
                    arrayIdx: newIdx
                }

                //Set array stuff
                tp[rec.floatAdjust].indexArray[newIdx].cols[0] = rec.indexName;
                tp[rec.floatAdjust].indexArray[newIdx].cols[1] = numeral(rec.rate).format('0.00');

                tp[rec.floatAdjust].indexArray[newIdx].cols[7] = 0;
                tp[rec.floatAdjust].indexArray[newIdx].cols[14] = 0;
                tp[rec.floatAdjust].indexArray[newIdx].cols[16] = 0;

                //Keep track of idnex count
                tp[rec.floatAdjust].indexCount += 1;

                //Old has table method
                tp[rec.floatAdjust].indexes[rec.indexName].cols[0] = rec.indexName;
                tp[rec.floatAdjust].indexes[rec.indexName].cols[1] = numeral(rec.rate).format('0.00');
                tp[rec.floatAdjust].indexes[rec.indexName].cols[7] = 0;
                tp[rec.floatAdjust].indexes[rec.indexName].cols[14] = 0;
                tp[rec.floatAdjust].indexes[rec.indexName].cols[16] = 0;
            }

            //load index to i can populate it
            var foundIdx = tp[rec.floatAdjust].indexes[rec.indexName].arrayIdx;

            if (rec.colNumber <= 5) {
                tp[rec.floatAdjust].indexes[rec.indexName].cols[7] += numeral(rec.balance).value();
                tp[rec.floatAdjust].indexArray[foundIdx].cols[7] += numeral(rec.balance).value();

                tp[rec.floatAdjust].cols[7] += rec.balance;
                // tp['Total'].cols[7] += rec.balance;
            }
            else if (rec.colNumber >= 8 && rec.colNumber <= 12) {
                tp[rec.floatAdjust].indexes[rec.indexName].cols[14] += numeral(rec.balance).value();
                tp[rec.floatAdjust].indexArray[foundIdx].cols[14] += numeral(rec.balance).value();

                tp[rec.floatAdjust].cols[14] += numeral(rec.balance).value();
                // tp['Total'].cols[14] += rec.balance;
            }

            tp[rec.floatAdjust].indexes[rec.indexName].cols[16] += numeral(rec.balance).value();

            tp[rec.floatAdjust].indexArray[foundIdx].cols[16] += numeral(rec.balance).value();

            tp[rec.floatAdjust].cols[16] += rec.balance;

            tp[rec.floatAdjust].indexes[rec.indexName].cols[rec.colNumber + 1] = numeral(rec.balance).value();

            tp[rec.floatAdjust].indexArray[foundIdx].cols[rec.colNumber + 1] = numeral(rec.balance).value();

            tp[rec.floatAdjust].cols[rec.colNumber + 1] += numeral(rec.balance).value();
            // tp['Total'].cols[rec.colNumber + 1] = rec.balance;
        }


        if (tp['Floating'].indexCount > 0) {
            tp['Floating'].indexes['Total'] = { cols: tp['Floating'].cols };
            tp['Floating'].indexes['Total'].cols[1] = "";
        }
        if (tp['Adjustable'].indexCount > 0) {
            tp['Adjustable'].indexes['Total'] = { cols: tp['Adjustable'].cols };
            tp['Adjustable'].indexes['Total'].cols[1] = "";
        }


        BuildTable(position, tp, false, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, cap);
        return tp;
    }

    function BuildTable(position, tp, comp, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, cap) {
        var origHeader = false;

        //if indexes have all zeros do not run

        for (var ty in tp) {
                if (tp[ty].indexCount > 0) {
                    //Write out header row
                    if (!origHeader) {
                        $('#' + position + 'Body').append(WriteHeaderRow(ty, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, true, cap, position));
                        origHeader = true;
                    } 
                    else {
                        $('#' + position + 'Body').append(WriteHeaderRow(ty, breakOutsHeadersOutMoney, breakOutsHeadersInMoney, false, cap, position));
                    }
                   

                    for (var i = 0; i < tp[ty].indexArray.length; i++){
                        var c = tp[ty].indexArray[i];

                        if (c == undefined) {
                            continue;
                        }

                        var balGreaterZero = false;
                        var row = '<tr class="' + (c.indexName == 'Total' ? 'totalIndex' : '') + '">';
                        for (var z = 0; z < tp[ty].indexArray[i].cols.length; z++) {
                            //Handles Rate Name and Index Rate

                            var val = tp[ty].indexArray[i].cols[z];
                            if (z == 0) {
                                row += "<td>" + val + "</td>";
                            }
                            else if(z == 1) {
                                if (val == "--" || val == "") {
                                    row += "<td>" + val + "</td>";
                                } else {
                                    
                                    row += '<td>' + numeral(val).format('0.00') + '</td>';
                                }
                                
                            }
                            else { //Handles the balances
                                var bgColor = 'grey';
                                if (i % 2 != 0) {
                                    bgColor = '';
                                }

                                if (val == 0 || val == undefined) {
                                    row += '<td class="' + bgColor + '">--</td>';
                                }
                                else {
                                    console.log(val);
                                    balGreaterZero = true;
                                    row += '<td class="' + bgColor + '">' + numeral(val / 1000).format('0,0') + '</td>';
                                }

                            }
                        }

                       
                            row += '</tr>';
                      
                       
                        if (balGreaterZero) {
                            $('#' + position + 'Body').append(row);
                        }
                    }

                    var totRow = '<tr class="totalIndex"><td>Total</td><td></td>';
                    for (var i = 0; i < tp[ty].cols.length; i++) {
                        //Handles Rate Name and Index Rate
                        var val = tp[ty].cols[i];
                        if (i >= 2) { //Handles the balances
                            var bgColor = 'grey';
                            if (i % 2 != 0) {
                                bgColor = '';
                            }
                            if (val == 0 || val == undefined) {
                                totRow += '<td class="' + bgColor + '">--</td>';
                            }
                            else {
                                totRow += '<td class="' + bgColor + '">' + numeral(val / 1000).format('0,0') + '</td>';
                            }


                        }
                    }
                    totRow += '</tr>';

                    $('#' + position + 'Body').append(totRow);


                }


            

        }
    }

    function WriteHeaderRow(label, bpsOut, bpsIn, showBps, cap, comp) {
        var text = (cap == 1 ? 'Cap' : 'Floor');

        var row = '<tr class="secondaryHeader">';
        row += '<td>' + label + '</td>';


        if (showBps) {
            if (comp == 'comp') {
                row += '<td class="b-all">Rate Var.</td>';
            }
            else {
                row += '<td class="b-all">Index Rate</td>';
            }
        }
        else {
            row += '<td class="b-all"></td>';
        }


        for (var i = 0; i < bpsIn.length; i++) {
            if (showBps) {
                row += '<td class="b-top b-bottom">' + bpsIn[i] + '</td>';
            }
            else {
                row += '<td class="b-top b-bottom"></td>';
            }
        }

        if (showBps) {
            row += '<td class="b-top b-bottom">Total</td>';
            row += '<td class="b-all">At ' + text + '</td>';
        }
        else {
            row += '<td class="b-top b-bottom"></td>';
            row += '<td class="b-all"></td>';
        }


        for (var i = bpsOut.length - 1; i >= 0; i--) {
            if (showBps) {
                row += '<td class="b-top b-bottom">' + bpsOut[i] + '</td>';
            }
            else {
                row += '<td class="b-top b-bottom"></td>';
            }
        }
        if (showBps) {
            row += '<td class="b-top b-bottom">Total</td>';
            row += '<td class="b-all">No ' + text + '</td>';
        }
        else {
            row += '<td class="b-top b-bottom"></td>';
            row += '<td class="b-all"></td>';
        }

        if (showBps) {
            row += '<td class="b-all">TOTAL</td>';
        }
        else {
            row += '<td class="b-all"></td>';
        }


        row += '</tr>';
        return row;
    }


    ctor.prototype.activate = function () {
        global.loadingReport(true);
        var self = this;
        //window.onbeforeprint = () => {
        //    $('#real_last_table').css("background-color", "red");
        //    self.reportView.prepareTableForPrint($('#real_last_table'), 1, 'page-break', true);
        //};
        //window.onafterprint = self.reportView.restoreTableFromPrint;

        return Q.all([
            globalContext.getReportById(self.lcfId, self.report),
            globalContext.getLoanCapFloorById(self.lcfId, self.lcf),
            globalContext.getLoanCapFloorViewDataById(self.lcfId, self.lcfViewData)
        ]);

    };


    return ctor;
});
