﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/system',
    'durandal/app',
    'services/logger',
    'services/model',
    'services/lookbackservice',
    'viewmodels/navigateawaymodal',
    'services/global'
],
    function (globalContext, router, system, app, logger, model, lookbackService, naModal, global) {
        var vm = function () {
            var self = this;
            var curPage = "LookbacConfig";
            //Vars
            this.isSaving = ko.observable(false);
            this.startDate = ko.observable();
            this.endDate = ko.observable();
            this.projHash = {};
            this.projIncExpHash = {};
            this.projAvgBalHash = {};
            this.origProjections = ko.observable();
            this.lookback = ko.observable();
            this.policyOffsets = ko.observable();
            this.modelSetup = ko.observable();
            this.profile = ko.observable();

            this.layouts = ko.observableArray();
            this.allLayouts = [];
            this.types = ko.observableArray();
            this.allTypes = [];
            this.simulationTypes = ko.observableArray();
            this.scenarioTypes = ko.observableArray();
            this.sortedLBLookbackData = ko.observableArray();

            //Navigation Helpers
            this.hasChanges = ko.pureComputed(function () {
                return globalContext.hasChanges();
            });

            this.canSave = ko.pureComputed(function () {
                return self.hasChanges() && !self.isSaving();
            });

            this.canDeactivate = function () {
                if (!self.hasChanges()) return true;

                return naModal.show(
                    function () { globalContext.cancelChanges(); },
                    function () { self.save(); }
                );
            };

            //Navigation
            this.goBack = function () {
                router.navigateBack();
            };

            this.cancel = function () {
                self.isSaving(true);
                globalContext.cancelChanges();
                self.isSaving(false);

                self.sortTypes();
            };

            this.save = function (data, event, quiet) {
                self.isSaving(true);
                return (quiet)
                    ? globalContext.saveChangesQuiet().fin(complete)
                    : globalContext.saveChanges().fin(complete);

                function complete() {
                    self.isSaving(false);
                }
            };

            //Sorting
            this.sortTypes = function () {
                self.sortedLBLookbackData(self.sortedLBLookbackData().sort(function (a, b) {
                    return a.priority() - b.priority();
                }));
            };
            this.sortData = function () {
                var arr = self.lookbackGroup.lBLookbackData().sort(function (a, b) { return a.priority() - b.priority(); });
                self.sortedLBLookbackData(arr);
            };


            this.populateProjectionObjs = function () {
                self.projIncExpHash = {};
                self.projAvgBalHash = {};
                self.projHash = ko.observable({});
                globalContext.getLookbackGroupDataProjections(
                    self.lookbackGroup.layoutId(), 
                    self.lookbackGroup.startDateOffset(), 
                    self.lookbackGroup.endDateOffset(), 
                    self.lookbackGroup.lBLookback().simulationTypeId(), 
                    self.lookbackGroup.lBLookback().scenarioTypeId(), 
                    self.lookbackGroup.taxEquivalent(),
                    moment.utc(self.lookbackGroup.lBLookback().asOfDate()).format("MM/DD/YYYY"),
                    self.origProjections).then(function(){
                        for (var p = 0; p < self.origProjections()[0].retTable.length; p++) {
                            var dRow = self.origProjections()[0].retTable[p];
                            if (self.projHash()[dRow.title] == undefined) {
                                self.projHash()[dRow.title] = {
                                    incExp: dRow.incExp,
                                    avgBal: dRow.avgBal
                                }
                            }
                            if (self.projIncExpHash[dRow.title] == undefined) {
                                self.projIncExpHash[dRow.title] = dRow.incExp;
                            }
                            if (self.projAvgBalHash[dRow.title] == undefined) {
                                self.projAvgBalHash[dRow.title] = dRow.avgBal;
                            }
                        }
                        return true;
                    });                  
            };
            this.defaultSimulation = function () {
                if (!self.lookback().overrideSimulationTypeId()) {
                    var aod = self.profile().asOfDate;
                    if (self.policyOffsets()[0][aod] != undefined) {
                        self.lookback().simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                    }
                }
            };
            this.defaultScenario = function () {
                if (!self.lookback().overrideScenarioTypeId()) {
                    var aod = self.profile().asOfDate;
                    if (self.policyOffsets()[0][aod] && self.policyOffsets()[0][aod][0] != undefined) {
                        var arr = self.scenarioTypes().filter(function (obj) {
                            return obj.id() == "1"
                        });
                        if (self.policyOffsets()[0][aod][0].bsSimulation == self.policyOffsets()[0][aod][0].niiId) {
                            self.lookback().scenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
                        } else if (arr.length > 0) {
                            self.lookback().scenarioTypeId("1");
                        } else {
                            self.lookback().scenarioTypeId(self.scenarioTypes()[0].id());
                        }
                    }
                }
            };
            this.defaultTaxEquiv = function () {
                if (!self.lookbackGroup.overrideTaxEquivalent()) {
                    var msTE = self.modelSetup().reportTaxEquivalent();

                    //stop it from it always "changing" the value. unecessary server calls
                    if (self.lookbackGroup.taxEquivalent() != msTE) {
                        self.lookbackGroup.taxEquivalent(msTE);
                    }
                    
                }
            };

            //Data Handling
            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage); 
                global.loadingReport(true);
                return globalContext.getLBLookbackById(id, self.lookback).then(function () {
                    self.lookbackGroupObs = ko.observable(self.lookback().lookbackGroups()[0]);
                    self.lookbackGroup = self.lookbackGroupObs();
                    return globalContext.getLBCustomLayouts(self.layouts).then(function () {
                        return globalContext.getLBCustomTypes(self.types).then(function () {
                            return globalContext.getScenarioTypes(self.scenarioTypes).then(function () {
                                return globalContext.getSimulationTypes(self.simulationTypes).then(function () {
                                    return globalContext.getPolicyOffsets(self.policyOffsets, "").then(function () {
                                        return globalContext.getModelSetup(self.modelSetup).then(function () {
                                            return globalContext.getUserProfile(self.profile).then(function () {
                                                self.subs = [];
                                                
                                                self.defaultSimulation();
                                                self.defaultScenario();
                                                self.defaultTaxEquiv();
                                                self.populateProjectionObjs();
                                                self.updateDataCrap(undefined, undefined, true);
                                                self.sub(self.lookback().overrideSimulationTypeId, self.defaultSimulation);
                                                self.sub(self.lookback().overrideScenarioTypeId, self.defaultScenario);
                                                self.sub(self.lookbackGroup.overrideTaxEquivalent, self.defaultTaxEquiv);
                                                self.sub(self.lookback().simulationTypeId);
                                                self.sub(self.lookback().scenarioTypeId);

                                                //we don't need to subscribe to everything in lookback group
                                                
                                                self.sub(self.lookbackGroup.layoutId, function () {
                                                    if(self.lookbackGroup.lBLookbackData().length == 0){
                                                        self.updateDataCrap();
                                                    }else if (self.lookbackGroup.entityAspect.originalValues.layoutId != undefined && !self.isSaving()) {
                                                        app.showMessage("Changing the layout will erase any adjustments and actuals entered. Do you want to continue?", "Warning", ["No", "Yes"]).then(function (r) {
                                                            if (r == "Yes") {
                                                                self.updateDataCrap();
                                                            } else {
                                                                self.isSaving(true);
                                                                globalContext.revertChangesToProperty(self.lookbackGroupObs, "layoutId");
                                                                self.isSaving(false);
                                                                return true;
                                                            }
                                                        });
                                                    }
                                                    
                                                });
                                                self.subs.push(self.lookbackGroup.rateVol.subscribe(function () {
                                                    lookbackService.updateTotalLines(self.lookbackGroup, self.lookbackGroup.yearFromStartDays(), self.lookbackGroup.startToEndDays());
                                                }));
                                                self.sub(self.lookbackGroup.lBCustomTypeId);
                                                self.sub(self.lookbackGroup.taxEquivalent);
                                                
                                                //Block calculations until cleared by end of activate
                                                self.isSaving(true);

                                                //Subscriptions
                                                
                                                //self.subAll(self.lookback())
                                                //self.subAll(self.lookbackGroup)
                                                
                                                self.sub(self.lookbackGroup.lBCustomTypeId, function (newValue) {
                                                    var type = self.types().filter(function (item) {
                                                        return item.id() == newValue;
                                                    });
                                                    if (type[0]) {
                                                        self.lookbackGroup.startDateOffset(type[0].startDateOffset());
                                                        self.lookbackGroup.endDateOffset(type[0].endDateOffset());
                                                    }
                                                    //self.populateProjectionObjs();
                                                    self.updateDataCrap(undefined, undefined);
                                                });

                                                //Sorting -- TODO HACK using sortedLBLookbackData directly does not seem to update UI properly
                                                self.sortData();
                                                self.sortTypes();
                                                //All activated and ready to go
                                                global.loadingReport(false);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            };

            this.compositionComplete = function () {
                //$("#pleaseWait").modal("show");
                //self.updateDataCrap(); //Don't need this for some reason...?
                //self.save(undefined, undefined, true);
                //globalContext.saveChangesQuiet();
            };

            //Helpers
            this.sub = function (thing, func) {
                self.subs.push((func)
                    ? thing.subscribe(function (newValue) {
                        func(newValue, thing);
                    })
                    : thing.subscribe(function (newValue) {
                        self.updateDataCrap(newValue, thing);
                    })
                );
            };
            this.subAll = function (thing, func) {
                for (var k in thing)
                    if (thing[k] && thing[k].subscribe)
                        self.sub(thing[k], func);
                if (thing.forEach) {
                    thing.forEach(function (row) {
                        for (var k in row)
                            if (row[k] && row[k].subscribe)
                                self.sub(row[k], func);
                    });
                }
            };

            //Heavy Lifting
            this.updateGrid = function (row) {
                var startToEndDays = self.lookbackGroup.startToEndDays();
                var yearFromStartDays = self.lookbackGroup.yearFromStartDays();
                var isAsset = row.isAsset();
                var projIntExp = row.proj_IncExp() + row.proj_IncExp_Adj();
                var title = row.title();
                var varToVol = 0, varToRate = 0, varVal = 0;
                //var projIncExp = self.projIncExpHash[title] + row.proj_IncExp_Adj();
                //var projAvgBal = self.projAvgBalHash[title] + row.proj_AvgBal_Adj();

                lookbackService.calcRowData(row, false, {}, self.projHash()[row.title()], yearFromStartDays, startToEndDays);
                lookbackService.updateTotalLines(self.lookbackGroup, yearFromStartDays, startToEndDays);
            };

            this.updateDataCrap = function (newValue, lastModified, saveWhenDone) {
                //Guard against firing more than once per update or during initial activation
                if (self.isSaving())
                    return;
                self.isSaving(true);
                //self.populateProjectionObjs();

                var pw = $("#pleaseWait");
                if (pw.length > 0 && pw.is(":hidden")) {
                    pw.modal("show");
                }

                lookbackService.createDataForLookbackGroup(self.lookback, false, self.projHash, false).then(function () {
                    $("#pleaseWait").modal("hide");
                    self.sortData();
                    if (saveWhenDone != null && saveWhenDone != undefined && saveWhenDone == true) {
                        globalContext.saveChangesQuiet().then(function () {
                            self.isSaving(false);
                        });
                    } else {
                        self.isSaving(false);
                    }
                    
                    
                });
            };

            //Disposal
            this.deactivate = function (isClose) {
                self.subs.forEach(function (sub) {
                    //TODO HACK subs should only be pushed ko.subscriptions, but sometimes numbers end up there!?
                    if (sub.dispose)
                        sub.dispose();
                });
                self.subs = [];
            };
        };
        return vm;
    });
