﻿define([
    'services/globalcontext',
    'services/global',
     'viewmodels/basesimulationlockedconfig',
     'durandal/app'
],
function (globalContext, global, basesimulationlockedconfig, app) {
    /*An extender to make the endpoint values update the source array
    In this case the parent 'array' is actually a comma delimited string*/
    ko.observable.fn.updatesParentArr = function (parentArr, index) {
        var obs = this;
        obs.val = ko.computed({
            read: function () {
                return parentArr().split(',')[index];
            },
            write: function (value) {
                var newVal = parentArr().split(',');
                newVal[index] = value;

                parentArr(newVal.join(','));
            }
        });

        return obs.val;
    };

    var loanCapFloorVm = function (loanCapFloor, sortedFootnotes, report) {
        var self = this;
        var curPage = "LoanCapFloorConfig";
        globalContext.logEvent("Viewed", curPage); 
        var tempArr = [];
        var breakoutLetterTags = ["A", "B", "C", "D", "E"]

        this.loanCapFloor = loanCapFloor;
        this.report = report;
        this.consolidations = ko.observableArray();
        this.global = global;
        //this.bpBreakoutEndpoints = loanCapFloor.bpBreakoutEndpoints;
        //this.bpBreakoutEndpoints([50,100,150,200]);
        this.types = ko.observableArray([
            { value: 1, text: "Caps" },
            { value: 2, text: "Floors" }
        ]);
        this.modes = ko.observableArray([
            { value: 1, text: "Report" },
            { value: 2, text: "Reconciliation" }
        ]);
        this.breakouts = ko.observableArray([
            { value: 1, text: "Default" },
            { value: 2, text: "Expanded" },
            { value: 3, text: "Custom" }
        ]);
        //this.breakoutEndpoints = ko.observableArray([50,100,150,200]);
        this.type = this.loanCapFloor().type;
        this.mode = this.loanCapFloor().mode;
        this.bpBreakout = this.loanCapFloor().bPBreakout;

        if (self.type() == 0) {
            self.type(1);
        }
        if (self.mode() == 0) {
            self.mode(1);
        }
        if (self.bpBreakout() == 0) {
            self.bpBreakout(1);
        }


        this.bpEndpoints = this.loanCapFloor().bpEndpoints;
        this.bpEndpointsObsArr = ko.observableArray();
        this.zeroObs = ko.observable(0);

        //add the individual observables to the object
        var len = self.loanCapFloor().bpEndpoints().split(',').length;
        for (i = 0; i < len; i++) {
            self['bpBreakoutEndpoint' + i] = ko.observable().updatesParentArr(self.loanCapFloor().bpEndpoints, i);
            self.bpEndpointsObsArr.push({ letterTag: breakoutLetterTags[i], startValue: (i == 0 ? self.zeroObs : self['bpBreakoutEndpoint' + (i - 1)]), value: self['bpBreakoutEndpoint' + i] });
        }
        var titles = ["Loan Cap Report",
            "Loan Cap Report - Expanded",
            "Loan Cap Report - Custom",
            "Loan Cap Reconciliation",
            "Loan Cap Reconciliation - Expanded",
            "Loan Cap Reconciliation - Custom",
            "Loan Floor Report",
            "Loan Floor Report - Expanded",
            "Loan Floor Report - Custom",
            "Loan Floor Reconciliation",
            "Loan Floor Reconciliation - Expanded",
            "Loan Floor Reconciliation - Custom"
        ];


        //report().name = ko.computed(function () {
        //    var index = ((self.type() - 1) * 6) + ((self.mode() - 1) * 3) + (self.bpBreakout() - 1);
        //    return titles[index];
        //});

        this.name = this.report().name;
        self.name = ko.computed(function () {
            var index = ((self.type() - 1) * 6) + ((self.mode() - 1) * 3) + (self.bpBreakout() - 1);
            return titles[index];
        });

        this.footnotes = self.report().footnotes;

        //loan cap floor always uses base simulation, siumulationTypeId set in the construction of basesimulationlockedconfig
     
        this.leftBaseSimulationLockedConfig = new basesimulationlockedconfig(this.loanCapFloor().topInstitutionDatabaseName, this.loanCapFloor().topAsOfDateOffset);
        this.rightBaseSimulationLockedConfig = new basesimulationlockedconfig(this.loanCapFloor().bottomInstitutionDatabaseName, this.loanCapFloor().bottomAsOfDateOffset);

        this.availableFirstFootnotes = [
            "Floating Loans have a repricing frequency of 1, 3, or 6 months and reprice within 6 moths of the model's as-of-date.",
            "Floating Loans have a repricing frequency of 1, 3, or 6 months."
        ];
        this.availableSecondFootnotes = [
            "Adjustable loans have repricing frequencues greater than 6 months and reprice within 12 months of the model's as-of-date.",
            "Adjustable loans have repricing frequencues greater than 6 months."
        ];
        this.availableThirdFootnotes = [
            "Loans with caps greater than 25% are assumed to have 'No Cap'. Non-Accrual loans are excluded from the report.",
            "Non-Accrual loans are excluded from the report."
        ];


        this.firstHardCodedFootnote = ko.computed(function () {
            return self.availableFirstFootnotes[self.mode() - 1];
        });

        this.secondHardCodedFootnote = ko.computed(function () {
            return self.availableSecondFootnotes[self.mode() - 1];
        });

        this.thirdHardCodedFootnote = ko.computed(function () {
            return self.availableThirdFootnotes[self.type() - 1];
        });

        var sortFootnotes = function () {
            var sorted = report().footnotes().sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });

            sortedFootnotes(sorted);
        };


        self.updateFootnotes = function () {
            var fn1 = '';
            var fn2 = '';
            var fn3 = '';
            if (self.mode() == 1 && self.type() == 2) {
                fn1 = 'Floating Loans have a repricing frequency 1, 3 or 6 months and reprice within 6 months of the model’s as-of-date.';
                fn2 = 'Adjustable loans have repricing frequencies greater than 6 months and reprice within 12 months of the model’s as-of-date.';
                fn3 = 'Non-Accrual loans are excluded from the report.';
            }
            else if (self.mode() == 2 && self.type() == 2) {
                fn1 = 'Floating Loans have a repricing frequency of 1, 3 or 6 months. ';
                fn2 = 'Adjustable loans have repricing frequencies greater than 6 months.';
                fn3 = 'Non-Accrual loans are excluded from the report.';
            }
            else if (self.mode() == 1 && self.type() == 1) {
                fn1 = 'Floating Loans have a repricing frequency 1, 3 or 6 months and reprice within 6 months of the model’s as-of-date.';
                fn2 = 'Adjustable loans have repricing frequencies greater than 6 months and reprice within 12 months of the model’s as-of-date.';
                fn3 = 'Loans with caps greater than 25% are assumed to have "No Cap". Non-Accrual loans are excluded from the report.';
            }
            else if (self.mode() == 2 && self.type() == 1) {
                fn1 = 'Floating Loans have a repricing frequency of 1, 3 or 6 months. ';
                fn2 = 'Adjustable loans have repricing frequencies greater than 6 months.';
                fn3 = 'Loans with caps greater than 25% are assumed to have "No Cap". Non-Accrual loans are excluded from the report.';
            }



            if (report().footnotes().length == 0) {
                var firstFootnote = globalContext.createFootnote();
                var secondFootnote = globalContext.createFootnote();
                var thirdFootnote = globalContext.createFootnote();
                firstFootnote.priority(1);
                secondFootnote.priority(2);
                thirdFootnote.priority(3);
                firstFootnote.report(report());
                secondFootnote.report(report());
                thirdFootnote.report(report());

                firstFootnote.text(fn1);
                secondFootnote.text(fn2);
                thirdFootnote.text(fn3);
            } else {
                report().footnotes()["0"].text(fn1);
                report().footnotes()["1"].text(fn2);
                report().footnotes()["2"].text(fn3);
            }





            sortFootnotes();
        };

        self.updateFootnotes();

        self.updateName = function () {
            var index = ((self.type() - 1) * 6) + ((self.mode() - 1) * 3) + (self.bpBreakout() - 1);
            var nameStr = titles[index];
            self.report().name(nameStr);
            //global.updatedTitle = nameStr;
            //app.trigger('application:changeReportTitle');
        };
        self.updateName();

        self.updateType = function () {
            self.updateFootnotes();
            self.updateName();

        };
        self.updateMode = function () {
            self.updateFootnotes();
            self.updateName();
        };

        self.deactivate = function () {

        };
        self.compositionComplete = function () {
            if (loanCapFloor().id() > -1) {
                globalContext.saveChangesQuiet();
            }
        };



        self.updateBreakouts = function (selected) {
            var breakoutSets = ["50,100,150,200", "100,200,300,400"],
                curMode = self.bpBreakout();
            if (curMode < 3) {
                var thisBreakoutSet = breakoutSets[curMode - 1].split(',');

                for (i = 0; i < 4; i++) {
                    var temp = self["bpBreakoutEndpoint" + (i)]();
                    self["bpBreakoutEndpoint" + (i)](thisBreakoutSet[i]);
                    temp = self["bpBreakoutEndpoint" + (i)]();
                }
            }

            self.updateName();
        };

        this.cancel = function () {
        };

        return this;
    };

    return loanCapFloorVm;
})
