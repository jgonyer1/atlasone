﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var timeDepositMigrationVm = function (timeDepositMigration) {
        var self = this;
        this.viewOptions

        this.timeDepositMigration = timeDepositMigration;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();


        this.priorSimulationTypes = ko.observableArray();

        this.priorScenarioTypes = ko.observableArray();

        this.policyOffsets = ko.observableArray();

        //multi group select array that gets set after we pull down defined groupings
        this.viewOptions = ko.observableArray();


        this.setDefaultSimScen = function () {
            var aod = self.asOfDateOffsets()[self.timeDepositMigration().asOfDateOffset()].asOfDateDescript;

            if (!self.timeDepositMigration().overrideSimulationTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.timeDepositMigration().simulationTypeId(self.policyOffsets()[0][aod][0].bsSimulation);
            }

            if (!self.timeDepositMigration().overrideScenarioTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.timeDepositMigration().scenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
            }
        }

        this.setDefaultPriorSimScen = function () {
            var aod = self.asOfDateOffsets()[self.timeDepositMigration().priorAsOfDateOffset()].asOfDateDescript;
            if (!self.timeDepositMigration().overridePriorSimulationTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.timeDepositMigration().priorSimulationTypeId(self.policyOffsets()[0][aod][0].bsSimulation);
            }

            if (!self.timeDepositMigration().overridePriorScenarioTypeId() && self.policyOffsets()[0][aod] != undefined) {
                self.timeDepositMigration().priorScenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
            }
        }

        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().asOfDateOffset(), self.timeDepositMigration().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }


        this.refreshPriorScen = function () {
            globalContext.getAvailableSimulationScenarios(self.priorScenarioTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().priorAsOfDateOffset(), self.timeDepositMigration().priorSimulationTypeId(), false)
        }

        this.refreshPriorSim = function () {
            globalContext.getAvailableSimulations(self.priorSimulationTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().priorAsOfDateOffset()).then(function () {
                self.refreshPriorScen();
            });
        }



        this.activate = function (id) {
            global.loadingReport(true);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.timeDepositMigration().institutionDatabaseName()).then(function () {
                        return globalContext.getPolicyOffsets(self.policyOffsets, self.timeDepositMigration().institutionDatabaseName()).then(function () {
                            return globalContext.getAvailableSimulations(self.simulationTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().asOfDateOffset()).then(function () {
                                return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().asOfDateOffset(), self.timeDepositMigration().simulationTypeId(), false).then(function () {
                                    return globalContext.getAvailableSimulationScenarios(self.priorScenarioTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().priorAsOfDateOffset(), self.timeDepositMigration().priorSimulationTypeId(), false).then(function (){
                                        return globalContext.getAvailableSimulations(self.priorSimulationTypes, self.timeDepositMigration().institutionDatabaseName(), self.timeDepositMigration().priorAsOfDateOffset()).then(function () {
                                    self.instSub = self.timeDepositMigration().institutionDatabaseName.subscribe(function (newValue) {
                                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);

                                        globalContext.getPolicyOffsets(self.policyOffsets, self.timeDepositMigration().institutionDatabaseName()).then(function () {
                                            self.refreshSim();
                                            self.refreshPriorSim();

                                            self.setDefaultPriorSimScen();
                                            self.setDefaultSimScen();

                                        });
                                    });

                                    //Date offset handling
                                    self.dateOffsetSub = self.timeDepositMigration().asOfDateOffset.subscribe(function (newValue) {
                                        self.refreshSim();
                                        self.setDefaultSimScen();
                                     
                                    });

                                    //Date offset handling
                                    self.priorDateOffsetSub = self.timeDepositMigration().priorAsOfDateOffset.subscribe(function (newValue) {
                                        self.refreshPriorSim();
                                        self.setDefaultPriorSimScen();
                                    });


                                    //Date offset handling
                                    self.simSub = self.timeDepositMigration().simulationTypeId.subscribe(function (newValue) {
                                        self.refreshScen();

                                    });

                                    //Date offset handling
                                    self.priorSimSub = self.timeDepositMigration().priorSimulationTypeId.subscribe(function (newValue) {
                                        self.refreshPriorScen();
                                    });


                                    self.priorOverrideScenSub = self.timeDepositMigration().overridePriorScenarioTypeId.subscribe(function (newValue) {
                                        self.setDefaultPriorSimScen();
                                    });

                                    self.priorOverrideSimSub = self.timeDepositMigration().overridePriorSimulationTypeId.subscribe(function (newValue) {
                                        self.refreshPriorScen();
                                        self.setDefaultPriorSimScen();
                                    });

                                    self.overrideSimSub = self.timeDepositMigration().overrideSimulationTypeId.subscribe(function (newValue) {
                                        self.refreshScen();
                                        self.setDefaultSimScen();
                                    });

                                    self.overrideScenSub = self.timeDepositMigration().overrideScenarioTypeId.subscribe(function (newValue) {
                                        self.setDefaultSimScen();
                                    });


                                    var arr = [];
                                    arr.push({
                                        name: "Standard View", options: [
                                            { name: "Default", id: "0" },
                                            { name: "Sub Accounts", id: "1" },
                                            { name: "Classifications", id: "2" },
                                        ]
                                    });
                                    self.viewOptions(global.getGroupedSelectOptions(arr));


                                    self.setDefaultSimScen();
                                    self.setDefaultPriorSimScen();

                                    if (self.timeDepositMigration().id() > 0) {
                                        globalContext.saveChangesQuiet();
                                    }
                                    global.loadingReport(false);

                                });
                                });
                                });
                            })
                    });
                });

            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.dateOffsetSub.dispose();
            self.priorDateOffsetSub.dispose();
            self.priorOverrideScenSub.dispose();
            self.priorOverrideSimSub.dispose();
            self.overrideSimSub.dispose();
            self.overrideScenSub.dispose();
            self.simSub.dispose();
            self.priorSimSub.dispose();
        };

        return this;
    };

    return timeDepositMigrationVm;
});
