﻿define([
    'services/globalcontext',
    'durandal/app',
    'services/global'
],
    function (globalContext, app, global) {
        var ignoreRefill = false;

        var mcfVm = function (mcf) {
            var self = this;
            var curPage = "MarginalCostOfFundsConfig";
            this.mcf = mcf;
            this._runoffs = ko.observableArray();
            this._rateChanges = ko.observableArray();

            function onChangesCanceled() {
                ignoreRefill = true;
                unsplitAndFill(self.mcf().runoffs(), self._runoffs, self.refillRunoffs);
                unsplitAndFill(self.mcf().rateChanges(), self._rateChanges, self.refillRCs);
                ignoreRefill = false;
            }

            this.changeTitle = ko.computed(function () {
                var str = "";
                if (self.mcf().isReduction()) {
                    str = "Reduction"
                } else {
                    str = "Increase";
                }
                return str;
            });

            this.refillRunoffs = function () {
                var a = [];
                self._runoffs().forEach(function (item, index, arr) {
                    a.push(item());
                });
                self.mcf().runoffs(a.join(','));
            }

            this.refillRCs = function () {
                var a = [];
                self._rateChanges().forEach(function (item, index, arr) {
                    a.push(item());
                });
                self.mcf().rateChanges(a.join(','));
            }

            // subscribe to the arrays, this only indicates when something is added or removed
            self._runoffs.subscribe(function () {
                if (ignoreRefill) return;
                self.refillRunoffs();
            });

            self._rateChanges.subscribe(function () {
                if (ignoreRefill) return;
                self.refillRCs();
            }); 

            // need to subscribe to each observable to know when each item updates
            function unsplitAndFill(source, target, subscription) {
                var replacements = [];

                if (source) {
                    var values = source.split(',');

                    for (var i = 0; i < values.length; i++) {
                        var obs = ko.observable(values[i]);
                        obs.subscribe(subscription);
                        replacements.push(obs);
                    }
                }

                target.removeAll();
                target(replacements);
            }

            unsplitAndFill(self.mcf().runoffs(), self._runoffs, self.refillRunoffs);
            unsplitAndFill(self.mcf().rateChanges(), self._rateChanges, self.refillRCs);
            
            this.addRunoff = function () {
                var o = ko.observable(0);
                o.subscribe(function (newValue) {
                    self.refillRunoffs();
                });
                o.extend({notify: 'always'});
                self._runoffs.push(o);
            };

            this.addRateChange = function () {
                var o = ko.observable(0);
                o.subscribe(function (newValue) {
                    self.refillRCs();
                });
                o.extend({ notify: 'always' });
                self._rateChanges.push(o);
            };

            this.removeRunoff = function (ro) {
                self._runoffs.splice(ro,1);
            };

            this.removeRateChange = function (ro) {
                self._rateChanges.splice(ro, 1);
            };


            this.cancel = function () {
            };

            this.compositionComplete = function () {
                if (mcf().id() > -1) {
                    globalContext.saveChangesQuiet();
                }
            };

            this.activate = function (id) {
                app.on('application:cancelChanges', onChangesCanceled);
            };

            this.deactivate = function () {
                app.off('application:cancelChanges', onChangesCanceled);
            };
        };

        return mcfVm;
    });
