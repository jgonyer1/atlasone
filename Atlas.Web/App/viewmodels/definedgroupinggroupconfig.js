﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/system',
    'durandal/app',
    'services/logger',
    'services/model',
    'viewmodels/navigateawaymodal'
],

        function (globalContext, router, system, app, logger, model, naModal) {
            var vm = function () {
                var self = this;
            };

            var definedGrouping = ko.observable();

            vm.prototype.updateClassifications = function () {
                var classificationsInUse = vm.prototype.group().definedGroupingClassification().map(function (cl) {
                    return cl.name();
                });

                var results = [];
                vm.prototype.classifications().forEach(function (scen) {
                    if (classificationsInUse.indexOf(scen.name()) == -1) {
                        results.push(scen);
                    }
                });

                vm.prototype.availableClassifications(results);
            };

            vm.prototype.availableClassifications = ko.observable();
            vm.prototype.group = ko.observableArray();
            vm.prototype.isSaving = ko.observable(false);
            vm.prototype.classifications = ko.observable();

            vm.prototype.activate = function (id) {
                globalContext.logEvent("Viewed", "DefinedGroupingGroupConfig");
                return globalContext.getDefinedGroupingGroupById(id, vm.prototype.group)
                    .then(function () {
                        return globalContext.getClassifications(vm.prototype.group().isAsset(), vm.prototype.classifications)
                            .then(function () { globalContext.getDefinedGroupingById(vm.prototype.group().definedGroupingId(), function (dg) { definedGrouping(dg); }); })
                            .then(function () { vm.prototype.updateClassifications(); })
                    });
            };

            vm.prototype.hasChanges = ko.computed(function () {
                return globalContext.hasChanges();
            });

            vm.prototype.cancel = function () {
                globalContext.cancelChanges();
                router.navigate('#/definedgroupings');
            };

            vm.prototype.canSave = ko.computed(function () {
                return vm.prototype.hasChanges() && !vm.prototype.isSaving();
            });

            vm.prototype.goToDefinedGroupingGroup = function (groupId) {
                var that = this;

                if (!vm.prototype.hasChanges()) {
                    continueNavigation();
                    return;
                }

                return naModal.show(
                    function () {
                        globalContext.cancelChanges();
                        continueNavigation();
                    },
                    function () {
                        that.save().then(continueNavigation);
                    }
                );

                function continueNavigation() {
                    router.navigate('#/definedgroupingsconfig/' + vm.prototype.group().definedGrouping().id());
                }
            };

            vm.prototype.addGroup = function () {
                router.navigate('#/definedgroupinggroupadd/' + vm.prototype.definedGrouping().id());
            };

            vm.prototype.deleteDefinedGrouping = function () {
                //Delete DefinedGroup
                vm.prototype.definedGrouping().entityAspect.setDeleted();

                globalContext.saveChanges();
                vm.prototype.goToDefinedGroupingGroup;
            };

            vm.prototype.deleteGroup = function () {
                var responses = ["OK", "Cancel"];
                app.showMessage("Do you want to delete group " + vm.prototype.group().name() + "?", "Alert", responses).then(function (r) {
                    if (r == responses[0]) {
                        vm.prototype._deleteGroup();
                    } else {
                        return false;
                    }
                });
            }

            vm.prototype._deleteGroup = function () {
                //for (var c = vm.prototype.group().definedGroupingClassification().length - 1; c >= 0; c--) {
                //    vm.prototype.group().definedGroupingClassification()[c].entityAspect.setDeleted();
                //}
                //vm.prototype.group().entityAspect.setDeleted();
                //vm.prototype.group().entityAspect.setEntityState(EntityState.Deleted);

                globalContext.getUserName().then(userName => {
                    definedGrouping().lastModifiedBy(userName);
                    definedGrouping().lastModified(new Date());

                    globalContext.deleteDefinedGroupingGroup(vm.prototype.group().id());
                    globalContext.saveChanges().then(function () {
                        vm.prototype.isSaving(false);
                        vm.prototype.goToDefinedGroupingGroup();
                    })                    
                });
            };

            vm.prototype.addClassification = function () {
                var newClass = globalContext.createDefinedGroupingClassification(vm.prototype.group().id(), this);
                vm.prototype.group().definedGroupingClassification().push(newClass);
                vm.prototype.updateClassifications();
            }

            vm.prototype.deleteClassification = function (dg) {
                var responses = ["OK", "Cancel"];
                app.showMessage("Do you want to delete classification " + dg.name() + "?", "Alert", responses).then(function (r) {
                    if (r == responses[0]) {
                        vm.prototype._deleteClassification(dg);
                    } else {
                        return false;
                    }
                });
            }

            vm.prototype._deleteClassification = function (dg) {
                var id = vm.prototype.group().definedGroupingClassification().indexOf(dg);

                vm.prototype.group().definedGroupingClassification()[id].entityAspect.setDeleted();

                globalContext.getUserName().then(userName => {
                    definedGrouping().lastModifiedBy(userName);
                    definedGrouping().lastModified(new Date());

                    globalContext.saveChanges()
                        .then(vm.prototype.updateClassifications)
                        .fin(complete);

                    function complete() {
                        vm.prototype.isSaving(false);
                    }
                });
            }

            vm.prototype.save = function () {
                globalContext.getUserName().then(userName => {
                    definedGrouping().lastModifiedBy(userName);
                    definedGrouping().lastModified(new Date());

                    return globalContext.saveChanges().fin(complete);

                    function complete() {
                        vm.prototype.isSaving(false);
                    }
                });
            };

            return vm;
        });
