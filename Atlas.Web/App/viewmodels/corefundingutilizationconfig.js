﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
    function (globalContext, global, app) {
        var cfu = ko.observable();

        var coreFundingUtilizationVm = function (coreFundingUtilization) {
            var self = this;
            var curPage = "CoreFundingUtilizationConfig";
            cfu = coreFundingUtilization;
            this.coreFundingUtilization = coreFundingUtilization;
            this.sortedScenarioTypes = ko.observableArray(self.coreFundingUtilization().coreFundingUtilizationScenarioTypes());
            this.sortedOffSets = ko.observableArray(self.coreFundingUtilization().coreFundingUtilizationDateOffSets());

            function onChangesCanceled() {
                self.sortScenarioTypes();
                self.sortDateOffSets();
            }

            this.cancel = function () {
            };

            //Scenario Block For Sorting, Adding, and Deleting From Scenario List
            this.sortScenarioTypes = function () {
                self.sortedScenarioTypes(self.coreFundingUtilization().coreFundingUtilizationScenarioTypes().sort(function (a, b) {
                    return a.priority() - b.priority();
                }));
            };

            self.scenarioTypesetAfterMove = function () {
                self.sortedScenarioTypes().forEach(function (item, index, arr) {
                    item.priority(index);
                });
            };

            this.viewOptions = ko.observableArray();

            this.coreFundingUtilization().coreFundingUtilizationScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });

            this.addScenarioType = function (scenarioType) {
                var newCoreFundScenarioType = globalContext.createCoreFundingUtilizationScenarioType();
                newCoreFundScenarioType.scenarioTypeId(scenarioType.id);
                newCoreFundScenarioType.priority(coreFundingUtilization().coreFundingUtilizationScenarioTypes().length);
                //newCoreFundScenarioType.scenarioType = ko.observable({ name: ko.observable(scenarioType.name) });
                coreFundingUtilization().coreFundingUtilizationScenarioTypes().push(newCoreFundScenarioType);
                self.sortScenarioTypes();
                if (coreFundingUtilization().coreFundingUtilizationScenarioTypes().length == 4) {
                    $('#addScenarioButton').addClass('disabled');
                }
            };

            this.dontAddScenarioType = function (data, event) {
                event.stopPropagation();
            };

            this.removeScenarioType = function (coreFundScenarioType) {

                var msg = 'Delete scenario?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        try {
                            coreFundScenarioType.entityAspect.setDeleted();
                            //coreFundingUtilization().coreFundingUtilizationScenarioTypes.remove(coreFundScenarioType);
                            coreFundingUtilization().coreFundingUtilizationScenarioTypes().forEach(function (anEveScenarioType, index) {
                                anEveScenarioType.priority(index);
                            });
                            self.sortScenarioTypes();

                            if (coreFundingUtilization().coreFundingUtilizationScenarioTypes().length < 4) {
                                $('#addScenarioButton').removeClass('disabled');
                            }
                        }
                        catch (e) {
                            console.log("Set Deleted Broke");
                            console.log(e);
                        }
                        //coreFundScenarioType.entityAspect.setDeleted();
                        
                    }
                }
            };

            self.sortScenarioTypes();

            //Date Offset Block

            self.dateOffsetAfterMove = function () {
                self.sortedOffSets().forEach(function (item, index, arr) {
                    item.priority(index);
                });
            };

            this.sortDateOffSets = function () {
                self.sortedOffSets(self.coreFundingUtilization().coreFundingUtilizationDateOffSets().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.coreFundingUtilization().coreFundingUtilizationDateOffSets().sort(function (a, b) { return a.priority() - b.priority(); });

            this.removeDateOffSet = function (coreFundOffSet) {
                var msg = 'Delete offset "' + coreFundOffSet.offSet() + '" ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        coreFundOffSet.entityAspect.setDeleted();
                        coreFundingUtilization().coreFundingUtilizationDateOffSets().forEach(function (anEveScenarioType, index) {
                            anEveScenarioType.priority(index);
                        });
                        self.sortDateOffSets();

                        if (coreFundingUtilization().coreFundingUtilizationDateOffSets().length < 4) {
                            $('#addDateOffsetButton').removeClass('disabled');
                        }
                    }
                }
            };

            this.addDateOffSet = function (offSet) {
                if (self.coreFundingUtilization().coreFundingUtilizationDateOffSets().length <= 5) {
                    var newCoreFundOffSet = globalContext.createCoreFundingUtilizationDateOffSet();
                    var nextOffset = 0;
                    if (coreFundingUtilization().coreFundingUtilizationDateOffSets().length > 0 && coreFundingUtilization().coreFundingUtilizationDateOffSets()[coreFundingUtilization().coreFundingUtilizationDateOffSets().length - 1].offSet() < 8) {
                        nextOffset = coreFundingUtilization().coreFundingUtilizationDateOffSets()[coreFundingUtilization().coreFundingUtilizationDateOffSets().length - 1].offSet() + 1;
                    }
                    newCoreFundOffSet.simulationTypes = ko.observableArray();
                    newCoreFundOffSet.priority(coreFundingUtilization().coreFundingUtilizationDateOffSets().length);
                    newCoreFundOffSet.offSet(nextOffset);

                    self.refreshItemSim(newCoreFundOffSet);
                    self.defaultOffsetSimulation(newCoreFundOffSet);
                    self.subs.push(newCoreFundOffSet.offSet.subscribe(function () {
                        self.refreshItemSim(this);
                        self.defaultOffsetSimulation(this);
                    }, newCoreFundOffSet));

                    self.subs.push(newCoreFundOffSet.overrideSimulationTypeId.subscribe(function (newVal) {
                        self.defaultOffsetSimulation(this);
                    }, newCoreFundOffSet));

                    coreFundingUtilization().coreFundingUtilizationDateOffSets().push(newCoreFundOffSet);
                    self.sortDateOffSets();
                }
            };

            

            this.updateViewOption = function () {
                cfu().assetDetails(self.definedGroup());
                return true;
            };

            this.updateAssetDetails = function () {
                cfu().assetDetails(self.definedGroup());
                return true;
            };

            this.defaultDefinedGroup = function () {
                cfu().assetDetails(self.definedGroupings()[0].id);
                return true;
            };

            this.defaultSimulation = function () {
                if (!self.coreFundingUtilization().overrideSimulation()) {
                    var aod = self.asOfDateOffsets()[self.coreFundingUtilization().asOfDateOffset()].asOfDateDescript;
                    if (self.policyOffsets()[0][aod] != undefined) {
                        self.coreFundingUtilization().simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                    } else {
                        self.coreFundingUtilization().simulationTypeId(1);
                        app.showMessage("No Policies found for " + aod + ", defaulting to Base Simulation", "Error", ["OK"]);
                    }
                }
            };

            this.defaultOffsetSimulation = function (cfuOffset) {
                if (!cfuOffset.overrideSimulationTypeId()) {
                    var aod = self.asOfDateOffsets()[cfuOffset.offSet()].asOfDateDescript;
                    if (self.policyOffsets()[0][aod] != undefined) {
                        cfuOffset.simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                    } else {
                        cfuOffset.simulationTypeId(1);
                        app.showMessage("No Policies found for " + aod + ", defaulting to Base Simulation", "Error", ["OK"]);
                    }
                }
                
            };

            this.disableGroups = function () {
                self.group(false);
                return true;
            }

            this.group = ko.observable();
            this.definedGroup = ko.observable();
            this.definedGroupings = ko.observableArray();
            this.institutions = ko.observableArray();
            this.global = global;
            this.asOfDateOffsets = ko.observableArray();
            this.policyOffsets = ko.observableArray();
            this.simulationTypes = ko.observableArray();
            this.scenarioTypes = ko.observableArray();
            this.subs = [];

            this.compositionComplete = function () {
                console.log('calluing this core fund');
                if (self.coreFundingUtilization().id() > -1) {
                    globalContext.saveChangesQuiet();
                }
            };



            this.refreshItemSim = function (item) {
                globalContext.getAvailableSimulations(item.simulationTypes, self.coreFundingUtilization().institutionDatabaseName(), item.offSet()).then(function () {

                });
            }

            this.refreshScen = function () {
                globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.coreFundingUtilization().institutionDatabaseName(), self.coreFundingUtilization().asOfDateOffset(), self.coreFundingUtilization().simulationTypeId(), false)
            }

            this.refreshSim = function () {
                globalContext.getAvailableSimulations(self.simulationTypes, self.coreFundingUtilization().institutionDatabaseName(), self.coreFundingUtilization().asOfDateOffset()).then(function () {
                    self.refreshScen();
                });
            }


            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage);
                app.on('application:cancelChanges', onChangesCanceled);
                global.loadingReport(true);
                return globalContext.getAvailableSimulations(self.simulationTypes, self.coreFundingUtilization().institutionDatabaseName(), self.coreFundingUtilization().asOfDateOffset()).then(function () {
                    return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.coreFundingUtilization().institutionDatabaseName(), self.coreFundingUtilization().asOfDateOffset(), self.coreFundingUtilization().simulationTypeId(), false).then(function () {
                        return globalContext.getAvailableBanks(self.institutions).then(function () {
                            return globalContext.configDefinedGroupings(self.definedGroupings).then(function () {
                                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.coreFundingUtilization().institutionDatabaseName()).then(function () {
                                    return globalContext.getPolicyOffsets(self.policyOffsets, self.coreFundingUtilization().institutionDatabaseName()).then(function () {
                                        self.defaultSimulation();
                                        self.availableScenarioTypes = ko.computed(function () {
                                            var scenarioTypeIdsInUse = self.coreFundingUtilization().coreFundingUtilizationScenarioTypes().map(function (coreFundScenarioType) {
                                                return coreFundScenarioType.scenarioTypeId();
                                            });
                                            var result = [];
                                            self.scenarioTypes().forEach(function (scenarioType) {
                                                result.push({
                                                    id: scenarioType.id(),
                                                    name: scenarioType.name(),
                                                    inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                                });
                                            });
                                            return result;
                                        });

                                        self.subs.push(self.coreFundingUtilization().asOfDateOffset.subscribe(function (newValue) {
                                            self.refreshSim();
                                            self.defaultSimulation();
                                        }));

                                        self.subs.push(self.coreFundingUtilization().overrideSimulation.subscribe(function (newValue) {
                                            //self.refreshScen();
                                            self.defaultSimulation();
                                        }));
                                        self.subs.push(self.coreFundingUtilization().simulationTypeId.subscribe(function (newValue) {
                                            self.refreshScen();
                                            //self.defaultSimulation();
                                        }));

                                        self.subs.push(self.coreFundingUtilization().institutionDatabaseName.subscribe(function (newValue) {
                                            globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue).then(function () {
                                                globalContext.getPolicyOffsets(self.policyOffsets, self.coreFundingUtilization().institutionDatabaseName()).then(function () {
                                                    self.refreshSim();
                                                    self.defaultSimulation();

                                                    self.coreFundingUtilization().coreFundingUtilizationDateOffSets().forEach(function (item, index, arr) {
                                                        self.defaultOffsetSimulation(item);
                                                    });
                                                });
                                            });
                                        }));

                                        var offsetPromises = [];
                                        var offsetsProcessed = 0;
                                        self.coreFundingUtilization().coreFundingUtilizationDateOffSets().forEach(function (item, index, arr) {
                                            item.simulationTypes = ko.observableArray();
                                            offsetPromises.push(globalContext.getAvailableSimulations(item.simulationTypes, self.coreFundingUtilization().institutionDatabaseName(), item.offSet()));
                                        });
                                        Q.all(offsetPromises).then(function () {
                                            self.coreFundingUtilization().coreFundingUtilizationDateOffSets().forEach(function (item, index, arr) {
                                                self.defaultOffsetSimulation(item);
                                                self.subs.push(item.offSet.subscribe(function (newValue) {
                                                    self.refreshItemSim(item);
                                                    self.defaultOffsetSimulation(item);
                                                }));
                                                self.subs.push(item.overrideSimulationTypeId.subscribe(function (newValue) {
                                                    self.refreshItemSim(item);
                                                    self.defaultOffsetSimulation(item);
                                                }));
                                                offsetsProcessed++;
                                                if (offsetsProcessed == arr.length) {
                                                    self.sortDateOffSets();
                                                }                                                
                                            });
                                            var arr = [];
                                            arr.push({
                                                name: "Standard View", options: [
                                                    { name: "Total Assets", id: "0" },
                                                    { name: "Account Type", id: "1" }
                                                ]
                                            });
                                            arr.push({
                                                name: "Defined Groupings", options: self.definedGroupings()
                                            });
                                            self.viewOptions(global.getGroupedSelectOptions(arr));
                                            self.numOffsetsToGraph = ko.computed(function () {
                                                var a = self.sortedOffSets().filter(function (item, index, arr) {
                                                    return item.graph();
                                                });
                                                return a.length;
                                            });

                                            self.definedGroup();

                                            if (cfu().assetDetails().indexOf('dg_') > -1) {
                                                self.group(true);
                                                self.definedGroup(cfu().assetDetails());
                                            }
                                            else {
                                                self.group(false);
                                            }
                                            global.loadingReport(false);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            };

            this.deactivate = function () {
                app.off('application:cancelChanges', onChangesCanceled);
            }

            this.detached = function (view, parent) {
                for (var s = 0; s < self.subs.length; s++) {
                    self.subs[s].dispose();
                }
                //self.instSub.dispose();
            };

            return this;
        };

        return coreFundingUtilizationVm;
    });
