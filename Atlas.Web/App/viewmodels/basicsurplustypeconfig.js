﻿define([
    'services/globalcontext',
    'services/global',
    'plugins/router',
    'durandal/app',
    'viewmodels/navigateawaymodal'
],
function (globalContext, global, router, app, naModal) {
    var vm = function () {
        var self = this;
        var curPage = "BasicSurplusTypeConfig";
        //Vars
        this.global = global;
        this.thisType = ko.observable();
        this.basicSurplusSecuredLiabilitiesTypes = ko.observableArray();
        this.collateralTypes = ko.observableArray();
        this.isSaving = ko.observable(false);

        //View
        this.connectClass = "scenario-container";

        //Navigation Helpers
        this.hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges();
        });

        this.canSave = ko.pureComputed(function () {
            return self.hasChanges() && !self.isSaving();
        });

        this.canDeactivate = function () {
            if (!self.hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { self.save().then(self.goBack); }
            );
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        this.cancel = function () {
            self.isSaving(true);
            globalContext.cancelChanges();
            self.isSaving(false);

            self.sortTypes();
        };

        this.save = function () {
            self.isSaving(true);
            return globalContext.saveChanges().fin(complete);

            function complete() {
                self.isSaving(false);
            }
        };

        this.deleteType = function () {
            var type = self.thisType();
            var msg = 'Delete "' + type.securedLiabilitiesType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    //For some reason we have to first manually delete the subtypes
                    //And for some reason we must build a subTypesToDel to do this
                    var subTypesToDel = [];
                    type.basicSurplusCollateralTypes().forEach(function (subType) {
                        subTypesToDel.push(subType);
                    });
                    subTypesToDel.forEach(function (subType) {
                        subType.entityAspect.setDeleted();
                    });

                    type.entityAspect.setDeleted();
                   /* self.basicSurplusSecuredLiabilitiesTypes().forEach(function (aType, index) {
                        aType.priority(index);
                    });*/

                    //Actually save
                    self.save().then(function () {
                        self.goBack();
                    });
                }
                return selectedOption;
            }
        };

        //Sorting
        this.sortTypes = function () {
            self.sortedTypes(self.sortedTypes().sort(function (a, b) {
                return a.priority() - b.priority();
            }));

            //Also update available types
            self.collateralTypes.notifySubscribers();
        };

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (type, index) {
                type.priority(index);
            });
        };

        //Add / Remove
        this.addType = function (type) {
            var newType = globalContext.createBasicSurplusCollateralType();
            newType.priority(self.sortedTypes().length);
            newType.basicSurplusSecuredLiabilitiesTypeId(self.thisType().id());
            newType.collateralTypeId(type.id);
            //newType.outstanding(0);
            //newType.pledgingFactor(0);
            //newType.pledgingRequired(0);
            //newType.amountPledgedMktVal(0);
            //newType.collateralValuePercentage(0);
            //newType.netCollateralValue(0);
            //newType.maxCollateralUsed(0);
            //newType.remainingPledgingRequired(0);
            //newType.excessCollateralMktVal(0);
            //newType.overCollateralizedBasicSurplus(0);
            self.thisType().basicSurplusCollateralTypes.push(newType);
            self.sortTypes();
        };
        this.dontAddType = function (data, event) {
            event.stopPropagation();
        };

        this.removeType = function (type) {
            var msg = 'Delete "' + type.collateralType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    type.entityAspect.setDeleted();
                    self.thisType().basicSurplusCollateralTypes().forEach(function (aType, index) {
                        aType.priority(index);
                    });
                    self.sortTypes();
                }
                return selectedOption;
            }
        };

        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);
            //Pre-Setup
            return globalContext.getBasicSurplusSecuredLiabilitiesTypeById(id, self.thisType).then(function () {
                return Q.all([
                    globalContext.getBasicSurplusSecuredLiabilitiesTypesByBasicSurplusAdminId(self.thisType().basicSurplusAdminId(), self.basicSurplusSecuredLiabilitiesTypes),
                    globalContext.getCollateralTypesByBasicSurplusAdminId(self.thisType().basicSurplusAdminId(), self.collateralTypes)
                ]);
            }).then(function () {
                //Available Types
                self.availableTypes = ko.computed(function () {
                    var typeIdsInUse = self.thisType().basicSurplusCollateralTypes().map(function (type) {
                        return type.collateralTypeId();
                    });
                    var result = [];
                    self.collateralTypes().forEach(function (type) {
                        result.push({
                            id: type.id(),
                            name: type.name(),
                            inUse: typeIdsInUse.indexOf(type.id()) != -1
                        });
                    });
                    return result;
                });

                //Sorting
                self.sortedTypes = ko.observableArray(self.thisType().basicSurplusCollateralTypes());
                self.sortTypes();
            });
        };

        return this;
    };

    return vm;
});
