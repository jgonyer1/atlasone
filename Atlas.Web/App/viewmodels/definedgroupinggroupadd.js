﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model'
],

        function (globalContext, router, system, app, logger, model) {
        
            var group = ko.observable();
            var definedGrouping = ko.observable();
            var isSaving = ko.observable(false);
            var profile = ko.observable();
            var curPage = "DefinedGroupingGroupAdd";

            function activate(id, queryString) {
                globalContext.logEvent("Viewed", curPage);
                return globalContext.getUserProfile(profile)
                    .then(function () { group(globalContext.createDefinedGroupingGroup(id, queryString.groupPriority)); })
                    .then(function () { globalContext.getDefinedGroupingById(id, function (dg) { definedGrouping(dg); }); })
                    ;
            }

            var hasChanges = ko.computed(function () {
                return globalContext.hasChanges();
            });

            var cancel = function () {
                globalContext.cancelChanges();
                router.navigate('#/definedgroupings/');
            };

            var canSave = ko.computed(function () {
                return hasChanges() && !isSaving();
            });

            var save = function () {
                globalContext.getUserName().then(userName => {
                    definedGrouping().lastModifiedBy(userName);
                    definedGrouping().lastModified(new Date());

                    globalContext.saveChanges().then(goToEditView).fin(complete);

                    function goToEditView() {
                        router.navigate('#/definedgroupinggroupconfig/' + group().id());
                    }

                    function complete() {
                        isSaving(false);
                    }
                });
            };

            var LiabilityGroup = function () {
                group().isAsset(0);
                return true;
            };

            var AssetGroup = function () {
                group().isAsset(1);
                return true;
            };

            var vm = {
                activate: activate,
                title: 'New Defined Grouping',
                group: group,
                save: save,
                canSave: canSave,
                hasChanges: hasChanges,
                cancel: cancel,
                profile: profile,
                assetGroup: AssetGroup,
                liabilityGroup: LiabilityGroup
            };

            return vm;
    });
