﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var simulationSelectVm = function (institutionDatabaseName, asOfDateOffset, simulationTypeId, overrideSimulationTypeId, showInst) {
        var self = this;
        this.showInst = showInst;
        this.institutionDatabaseName = institutionDatabaseName;
        this.overrideSimulationTypeId = overrideSimulationTypeId;
        this.policyOffsets = ko.observable();
        this.institutionDatabaseName.subscribe(function () {
            globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.institutionDatabaseName());
        });
        //this.institutionDatabaseName = ko.computed({
        //    read: function () {
        //        return institutionDatabaseName();
        //    },
        //    write: function (value) {
        //        institutionDatabaseName(value);
        //        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, value);
        //    }
        //});
        this.asOfDateOffset = asOfDateOffset;
        this.simulationTypeId = simulationTypeId;

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.activate = function () {
            return globalContext.getAvailableSimulations(self.simulationTypes, self.institutionDatabaseName(), self.asOfDateOffset()).then(function () {
                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.institutionDatabaseName()).then(function () {
                        //Subscribe to as of date off set and 


                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            //self.instSub.dispose();
        };

        return this;
    };

    return simulationSelectVm;
})