﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var yieldCurveShiftVm = function (id) {
        var self = this;
        var _charting = charting;
        this.yieldCurveShiftViewData = ko.observable();
        this.yieldCurveShift = ko.observable();

        function RateCodeBucket(rateName) {
            if (rateName.indexOf('1 Month') > -1 || rateName.indexOf('1mo') > -1) {
                return '1mo';
            }
            if (rateName.indexOf('3 Month') > -1 || rateName.indexOf('3mo') > -1) {
                return '3mo';
            }
            else if (rateName.indexOf(' 6 Month') > -1 || rateName.indexOf(' 6mo') > -1) {
                return '6mo';
            }
            else if (rateName.indexOf(' 12 Month') > -1 || rateName.indexOf(' 1yr') > -1 || rateName.indexOf('1 Year') > -1) {
                return '1yr';
            }
            else if (rateName.indexOf(' 2 Year') > -1 || rateName.indexOf(' 2yr') > -1) {
                return '2yr';
            }
            else if (rateName.indexOf(' 3 Year') > -1 || rateName.indexOf(' 3yr') > -1) {
                return '3yr';
            }
            else if (rateName.indexOf(' 4 Year') > -1 || rateName.indexOf(' 4yr') > -1) {
                return '4yr';
            }
            else if (rateName.indexOf(' 5 Year') > -1 || rateName.indexOf(' 5yr') > -1) {
                return '5yr';
            }
            else if (rateName.indexOf(' 7 Year') > -1 || rateName.indexOf('7yr') > -1) {
                return '7yr';
            }
            else if (rateName.indexOf('10 Year') > -1 || rateName.indexOf('10yr') > -1) {
                return '10yr';
            }
            else if (rateName.indexOf('15 Year') > -1 || rateName.indexOf('15yr') > -1) {
                return '15yr';
            }
            else if (rateName.indexOf('20 Year') > -1 || rateName.indexOf('20yr') > -1) {
                return '20yr';
            }
            else if (rateName.indexOf('30 Year') > -1 || rateName.indexOf('30yr') > -1) {
                return '30yr';
            }
            else {
                alert(rateName);
            }
        }

        this.compositionComplete = function () {

            /*
                Clear Values
            */

            $('#scenarioTableHeaderRow').empty();
            $('#historicalTableHeaderRow').empty();
            $('#historicalTable tbody').empty();
            $('#scenarioTable tbody').empty();

            //Add headers
            $('#historicalTableHeaderRow').append('<th class="text-left cellLabel"> Historical Rates</th>');
            $('#scenarioTableHeaderRow').append('<th class="cellLabel">Scenarios</th>');

            /*
                Add default scenarios
            */

            var data = {};

            //Initialize Rate Buckets
            var rateBuckets = [];
            rateBuckets.push({ code: '1mo', name: '1 Month' });
            rateBuckets.push({ code: '3mo', name: '3 Month' });
            rateBuckets.push({ code: '6mo', name: '6 Month' });
            rateBuckets.push({ code: '1yr', name: '1 Year' });
            rateBuckets.push({ code: '2yr', name: '2 Year' });
            rateBuckets.push({ code: '3yr', name: '3 Year' });
            rateBuckets.push({ code: '4yr', name: '4 Year' });
            rateBuckets.push({ code: '5yr', name: '5 Year' });
            rateBuckets.push({ code: '7yr', name: '7 Year' });
            rateBuckets.push({ code: '10yr', name: '10 Year' });
            rateBuckets.push({ code: '15yr', name: '15 Year' });
            rateBuckets.push({ code: '20yr', name: '20 Year' });
            rateBuckets.push({ code: '30yr', name: '30 Year' });

            for (var i = 0; i < self.yieldCurveShiftViewData().historicDates.length; i++) {
                console.log(moment(self.yieldCurveShiftViewData().historicDates[i]).format('MM/DD/YYYY'));
                data[moment(self.yieldCurveShiftViewData().historicDates[i]).format('MM/DD/YYYY')] = { isHistoric: true };
            }

            //Set Up Data Buckets One For Each Date Being Reported
            if (data[moment(self.yieldCurveShiftViewData().rateDate).format('MM/DD/YYYY')] == undefined) {
                data[moment(self.yieldCurveShiftViewData().rateDate).format('MM/DD/YYYY')] = {};
            }

            //First Push Scenarios On then Rate Buckets
            for (var dt in data) {
                for (var z = 0; z < self.yieldCurveShiftViewData().scens.length; z++) {
                    var scenName = self.yieldCurveShiftViewData().scens[z];
                    data[dt][scenName] = {};
                    for (var i = 0; i < rateBuckets.length; i++) {
                        data[dt][scenName][rateBuckets[i].code] = {
                            isActive: false,
                            name: rateBuckets[i].name,
                            rate: 0
                        }
                    }
                }
            }

            
            //Populate Data Tree
            for (var i = 0; i < self.yieldCurveShiftViewData().rateData.length; i++) {
                var rec = self.yieldCurveShiftViewData().rateData[i];
                try {
                    data[moment(rec.asOfDate).format('MM/DD/YYYY')][rec.scenName][RateCodeBucket(rec.rateName)].rate = rec.rate;
                    data[moment(rec.asOfDate).format('MM/DD/YYYY')][rec.scenName][RateCodeBucket(rec.rateName)].isActive = true;
                }
                catch (err) {
                    debugger;
                }
               
            }

            //Write HEaders For TAbles and Write out Scenarios for Web Rate As Of DAte And Generate Chart Series and Categories
            var cats = [];
            var series = [];
            var writeHeaders = true;
            var curRateDate = moment(self.yieldCurveShiftViewData().rateDate).format('MM/DD/YYYY');
            var scenarioMin = 0;

            for (var scenarioId in data[curRateDate]) {

                //Hack for flags ideal create property called scens on the data object.
                if (scenarioId == "isHistoric") {
                    continue;
                }

                var scenario = _charting.scenarioIdLookUp(scenarioId);

                series.push({
                    name: scenarioId.replace(" Scenario", ""),
                    data: [],
                    color: scenario.seriesColor,
                    marker: scenario.marker
                });
                //Generating headers for each tables and data for just scenario table
                var curRow = '<tr><td class="cellLabel"><span class="text-left scenarioButton ' + scenario.labelClass + ' ">' + scenarioId.replace(" Scenario", "") + '</span></td>';
                console.log('here');
                for (var rb in data[curRateDate][scenarioId]) {
                    if (data[curRateDate][scenarioId][rb].isActive) {

                        if (writeHeaders) {
                            console.log('why not headers');
                            //Add Headers To Tables
                            $('#scenarioTableHeaderRow').append('<th>' + data[curRateDate][scenarioId][rb].name + '</th>');
                            $('#historicalTableHeaderRow').append('<th>' + data[curRateDate][scenarioId][rb].name + '</th>');
                            cats.push(data[curRateDate][scenarioId][rb].name);
                        }
                        curRow += '<td>' + numeral(data[curRateDate][scenarioId][rb].rate).format('0.00') + '</td>';
                        series[series.length - 1].data.push({ y: data[curRateDate][scenarioId][rb].rate });

                        if (data[curRateDate][scenarioId][rb].rate < scenarioMin) {
                            scenarioMin = null;
                        }
                    }

                }
                $('#scenarioTable tbody').append(curRow);
                writeHeaders = false;
            }

            series = charting.scenarioIconStagger(series);
            /*-------------------------
            Generating data for historic table
            -----------------------------*/
            var historicMin = 0;
            var historicSeries = [];
            var histCounter = 1;
            console.log(data);
            for (var dt in data) {
                //Onmly do historic dates
                if (data[dt].isHistoric) {
                    historicSeries.push({
                        name: dt,
                        data: []
                    });
                    var histRow = '<tr><td class="cellLabel"><span class="text-left histRateButton histRateButton' + histCounter + '">' + dt + '</span></td>';
                    //Only do rate buckets that are in top report
                    for (var rb in data[curRateDate]['Base Scenario']) {
                        if (data[curRateDate]['Base Scenario'][rb].isActive) {
                            histRow += '<td>' + numeral(data[dt]['Base Scenario'][rb].rate).format('0.00') + '</td>';
                            historicSeries[historicSeries.length - 1].data.push(data[dt]['Base Scenario'][rb].rate);

                            if (numeral(data[dt]['Base Scenario'][rb].rate).value() < 0) {
                                historicMin = null;
                            }
                        }
                    }

                    //Add row to historic table
                    histRow += '</tr>';
                    $('#historicalTable tbody').append(histRow);
                    histCounter += 1;
                    if (histCounter > _charting.chartColorsPalette2.length) {
                        histCounter = 1;
                    }
                }
            }

            ////Do Compares

            var shiftTitle = ' Scenarios';
            var historicTitle = ' Historical Rates';

            if (self.yieldCurveShiftViewData().index == 'FHLB') {
                shiftTitle = self.yieldCurveShiftViewData().index + ' ' + self.yieldCurveShiftViewData().fhlb + shiftTitle;
                historicTitle = self.yieldCurveShiftViewData().index + ' ' + self.yieldCurveShiftViewData().fhlb + historicTitle;
            }
            else {
                shiftTitle = self.yieldCurveShiftViewData().index + shiftTitle;
                historicTitle = self.yieldCurveShiftViewData().index + historicTitle;
            }

            //set divider title
            $('#shiftTitle').text(shiftTitle + ' - ' + moment(self.yieldCurveShiftViewData().rateDate).format('MM/DD/YYYY'));
            $('#historicTitle').text(historicTitle);

            console.log(series);
            console.log(historicSeries);
            console.log(cats);
            console.log(scenarioMin);
            console.log(historicMin);
            this.chartSeriesY('#yieldShiftChart', series.reverse(), cats, scenarioMin);
            this.chartSeriesH('#historicChart', historicSeries, cats, historicMin);

            //Export To Pdf
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                //  $('#yieldShiftChart, #compareChart').css('height', '305px');
                //  $('.report-view').css('padding-bottom', '0px');
                hiqPdfConverter.startConversion();
               
            }


        }


        this.chartSeriesH = function (div, series, cats, min) {
            $(div).highcharts({
                chart: {
                    spacing: [10, 10, 0, 10],
                },
                credits: { enabled: false },
                title: {
                    text: ''
                },
                colors: _charting.chartColorsPalette2,
                xAxis: {
                    categories: cats,
                    labels: {
                        style: {
                            color: '#000',
                            fontWeight: 'bold',
                            fontSize: '10px'
                        }
                    }
                },
                plotOptions: {
                    series: {
                        animation: false,
                        marker: {
                            symbol: 'circle',
                        },
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return numeral(this.value).format('0.00');
                        },
                        style: {
                            color: '#000',
                            fontWeight: 'bold',
                            fontSize: '10px'
                        }
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    min: min
                },
                tooltip: {
                    formatter: function () {
                        var s = '' + this.x;
                        for (var i = 0; i < this.points.length; i++) {
                            var point = this.points[i];
                            var val = numeral(point.y).format('0.00');
                            //Build Tooltip string
                            s += '<br />';
                            s += '<span style="color:' + point.point.series.color + ';font-weight: bold;">' + point.series.name + ': </span>';
                            s += '<span>' + val + '</span>';
                        }

                        return s;
                    },
                    shared: true
                },
                series: series,
                legend: {
                    reversed: (div == '#historicChart' ? false : true),
                    margin: 0
                },
            });
        }

        this.chartSeriesY = function (div, series, cats, min) {
            $(div).highcharts({
                chart: {
                    spacing: [10, 10, 0, 10],
                },
                credits: { enabled: false },
                title: {
                    text: ''
                },

                xAxis: {
                    categories: cats,
                    labels: {
                        style: {
                            color: '#000',
                            fontWeight: 'bold',
                            fontSize: '10px'
                        }
                    }
                },
                plotOptions: {
                    series: {
                        animation: false,
                    }
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return numeral(this.value).format('0.00');
                        },
                        style: {
                            color: '#000',
                            fontWeight: 'bold',
                            fontSize: '10px'
                        }
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    min: min
                    
                },
                series: series,
                tooltip: {
                    formatter: function () {
                        var s = '' + this.x;
                        for (var i = this.points.length - 1; i >= 0 ; i--) {
                            var point = this.points[i];
                            var val = numeral(point.y).format('0.00');
                            //Build Tooltip string
                            s += '<br />';
                            s += '<span style="color:' + point.point.series.color + ';font-weight: bold;">' + point.series.name + ': </span>';
                            s += '<span>' + val + '</span>';
                        }

                        return s;
                    },
                    shared: true
                },
                legend: {
                    reversed: (div == '#yieldShiftChart' ? true : false),
                    margin: 0
                },
            });
        }


        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getYieldCurveShiftViewDataById(id, self.yieldCurveShiftViewData).then(function () {
                var n = 0;
            });
        };
    };

    return yieldCurveShiftVm;

});
