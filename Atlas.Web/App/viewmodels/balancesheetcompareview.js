﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');

    var balanceSheetCompareVm = function (id, reportView) {
        var self = this;

        this.balanceSheetCompareId = id;
        this.balanceSheetCompare = ko.observable();
        this.balanceSheetCompareViewData = ko.observable();
        var curPage = "BalanceSheetCompareView";
        this.compositionComplete = function () {
            //$('#balcompare-view').height($('#balcompare-view').height());

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            $('#balcompare-view').attr('data-pdf-orientation', 'portrait');

            if (typeof hiqPdfInfo != "undefined") {
                hiqPdfConverter.startConversion();
            }
        };

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            return globalContext.getBalanceSheetCompareById(self.balanceSheetCompareId, self.balanceSheetCompare).then(function () {
                return globalContext.getBalanceSheetCompareViewDataById(self.balanceSheetCompareId, self.balanceSheetCompareViewData).then(function () {
                    // only wire up the print handlers if the view option is 0 <= x < 3
                    var numericViewOption = parseInt(self.balanceSheetCompare().viewOption(), 10);

                    if (numericViewOption >= 0 && numericViewOption < 3) {
                        window.onbeforeprint = () => { reportView.prepareTableForPrint($('#balcompare-view > table:eq(0)'), 2, 'page-break', false); };
                        window.onafterprint = reportView.restoreTableFromPrint;
                    }

                    var viewData = self.balanceSheetCompareViewData();

                    if (!viewData) return;

                    globalContext.reportErrors(viewData.errors, viewData.warnings);
                });
            });
        };
    };

    return balanceSheetCompareVm;
});