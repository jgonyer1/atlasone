﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var eveVm = function (eve) {
        var self = this;
        var curPage = "EveAssumptionsConfig";
        this.eve = eve;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.modelsetup = ko.observable();
        this.depositTypes = ko.observableArray(['5', '7.5', 'Other']);
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);  
            global.loadingReport(true);
            return globalContext.getModelSetup(self.modelsetup).then(function () {
                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    if (!self.eve().override()) {
                        self.eve().depositType(self.modelsetup().avgLifeAssumption());
                        self.eve().depositNotes(self.modelsetup().avgLifeMessage());
                    }

                    //AsOfDateOffset Handling (Single-Institution Template)
                    self.instSub = self.eve().institutionDatabaseName.subscribe(function (newValue) {
                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                    });
                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.eve().institutionDatabaseName()).then(function () {
                        global.loadingReport(false);
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        this.overrideChange = function () {
            if (self.eve().override()) {
                self.eve().depositType(self.modelsetup().avgLifeAssumption());
                self.eve().depositNotes(self.modelsetup().avgLifeMessage());
            }
        }

        this.UpdateAverageLife = function () {
            if (self.eve().depositType() == 'Other') {
                self.eve().depositNotes('Average lives on non-maturity deposits are based upon a mm/dd/yyyy DCG deposit study. The aggregate average life based upon the current deposit mix is ##.## years. Please refer to the Deposit Study Tear Sheet for additional information. ');
            }
            else {
                self.eve().depositNotes('This assessment assumed a ' + self.eve().depositType() + ' year estimated life on the core deposit base.');
            }
        };
    };

    return eveVm;
});
