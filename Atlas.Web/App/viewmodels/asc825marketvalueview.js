﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id) {
        var self = this;
        this.ascViewData = ko.observable();

        this.compositionComplete = function () {
            
            var tbl = self.ascViewData().tblData;
            var scenLabels = {};
            var scens = self.ascViewData().scens;

            for (var i = 0; i < scens.length; i++) {
                var scen = scens[i];
                scenLabels[scen] = i;

                $('#ascHead').append('<th>' + scen +  '</th>');
            }

            //var idxs = {};

            //2 is Market Value
            if (self.ascViewData().reportType == "2") {
                $('#changeTitle').text('Book Value');
            }

            var cats = {};
            ////Build JSON Object before we output Data
            for (var i = 0; i < tbl.length; i++) {
                var rec = tbl[i];
                if (numeral(rec.parValue).value() != 0) {
                    
                    if (cats[rec.name] == undefined) {
                        cats[rec.name] = {
                            prices: new Array(scens.length),
                            marketValues: new Array(scens.length),
                            isAsset: numeral(rec.isAsset).value(),
                            parValue: rec.parValue,
                            bookValue: rec.bookValue,
                            discount: 0,
                            catType: rec.cat_type
                        }
                    }

                    if (rec.scenario == '0 Shock') {
                        cats[rec.name].discount = rec.discount
                    }

                    cats[rec.name].prices[scenLabels[rec.scenario]] = rec.marketPrice;
                    cats[rec.name].marketValues[scenLabels[rec.scenario]] = rec.mvAmount;
                }


            }

            var firstAsset = true;
            var firstLiab = true;

            for (var c in cats) {

                if (cats[c].isAsset == 1 && firstAsset) {
                    var assetRow = '<tr><td colspan="' + scens.length + 3 + '"><u>ASSET</u></td></tr>';
                    $('#ascBody').append(assetRow);
                    firstAsset = false;
                }

                if (cats[c].isAsset == 0 && firstLiab) {
                    var assetRow = '<tr><td colspan="' + scens.length + 3 + '"><u>LIABILITIES</u></td></tr>';
                    $('#ascBody').append(assetRow);
                    firstLiab = false;
                }

            

                //Grouping cat
                var groupingCat = false;
                if (numeral(cats[c].catType).value() != 0 && numeral(cats[c].catType).value() != 1 && numeral(cats[c].catType).value() != 5) {
                    groupingCat = true;
                }

                if (groupingCat) {
                    $('#ascBody').append('<tr><td class="spacerRow" colspan="' + scens.length + 3 + '"></td></tr>');
                }

                var row = '<tr class="' + (groupingCat ? 'groupRow' : '') + '">';
                if (cats[c].catType != 0 && cats[c].catType != 1 && cats[c].catType != 5) {
                    row = '<tr class="' + (groupingCat ? 'groupRow' : '') + ' total-step">';
                }
            
                                

                row += '<td>' + c + '</td>';
                if (numeral(cats[c].discount).value() != null) {
                    row += '<td>' + numeral(cats[c].discount).format('0.000') + '</td>';
                }
                else {
                    row += '<td>--</td>';
                }
                

                //2 is Market Value
                if (self.ascViewData().reportType == "2") {
                    if (numeral(cats[c].parValue).value() != null) {
                        row += '<td>' + numeral(cats[c].parValue / 1000).format('(0,0)') + '</td>';
                    }
                    else {
                        row += '<td>--</td>';
                    }
                }
                    //MarketPrice
                else {
                    if (numeral(cats[c].bookValue).value() != null) {
                        row += '<td>' + numeral(cats[c].bookValue / 1000).format('(0,0)') + '</td>';
                    }
                    else {
                        row += '<td>--</td>';
                    }
                }

                if (self.ascViewData().reportType == "2") {
                    for (var z = 0; z < cats[c].marketValues.length; z++) {

                        if (numeral(cats[c].marketValues[z]).value() != null) {
                            row += '<td>' + numeral(cats[c].marketValues[z] / 1000).format('0,0') + '</td>';
                        }
                        else {
                            row += '<td>--</td>';
                        }


                    }
                }
                else {
                    for (var z = 0; z < cats[c].prices.length; z++) {

                        if (numeral(cats[c].prices[z]).value() != null) {
                            row += '<td>' + numeral(cats[c].prices[z]).format('0.000') + '</td>';
                        }
                        else {
                            row += '<td>--</td>';
                        }


                    }
                }


                row += '</tr>';
                $('#ascBody').append(row);

                if (groupingCat) {
                    $('#ascBody').append('<tr><td class="spacerRow" colspan="' + scens.length + 3 + '"></td></tr>');
                }

            }

            $('#reportView').height($('#reportView').height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                
                hiqPdfConverter.startConversion();
            }

        }



        this.activate = function () {
            globalContext.logEvent("Viewed", "ASC825marketvalueview");
            global.loadingReport(true);
            return globalContext.getASC825MarketValueViewDataById(id, self.ascViewData).then(function () {
                var viewData = self.ascViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
            });
        };
    };

    return ascVm;

});
