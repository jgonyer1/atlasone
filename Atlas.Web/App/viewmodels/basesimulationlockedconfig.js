﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var basesimulationLockedVm = function (institutionDatabaseName, asOfDateOffset) {
        var self = this;
        var curPage = "BaseSimulationLockedConfig";
        this.institutionDatabaseName = ko.observable(institutionDatabaseName());
        this.asOfDateOffset = asOfDateOffset;
        this.simulationTypeId = ko.observable(1);

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.institutionDatabaseName()).then(function () {
                    //AsOfDateOffset Handling (Single-Institution Template)
                    self.instSub = self.institutionDatabaseName.subscribe(function (newValue) {
                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                    });

                });


                
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return basesimulationLockedVm;
})