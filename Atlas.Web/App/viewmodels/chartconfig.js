﻿define([
    'services/globalcontext',
    'services/global',
    'viewmodels/chartsimulationselectconfig',
    'durandal/app'
],
    function (globalContext, global, chartsimulationselectconfig, app) {

        var chartVm = function (chart, niiOptObs, displayOptions, compFunction) {
            if (chart()) {
                var self = this;
                var curPage = "ChartConfig";
                this.chart = chart;
                //this.chart.subscribe(function (n) { console.log("CHART CHAGNED TO THIS: " + n.toString())});
                this.niiOption = niiOptObs;
                this.consolidations = ko.observableArray();
                this.global = global;
                this.simulationTypes = ko.observableArray();
                this.scenarioTypes = ko.observableArray();
                this.subscriptions = [];
                this.hideQuarterly = ko.observable(false);
                this.canAddSimulations = ko.observable(true);
                this.policyOffsets = ko.observable();

                //the actual list of asofdates lives in the simulationselect config, this is just so I can have something to reference inside this config
                this.asOfDateOffsets = ko.observableArray();

                if (displayOptions.hideQuarterly != undefined) {
                    this.hideQuarterly(displayOptions.hideQuarterly);
                }
                if (displayOptions.canAddSimulations != undefined) {
                    this.canAddSimulations(displayOptions.canAddSimulations);
                }

                this.percChangeOptions = [
                    {
                        value: "0",
                        label: "None"
                    },
                    {
                        value: "1",
                        label: "% Change"
                    },
                    {
                        value: "2",
                        label: "% Change With Policies"
                    }
                ];

                this.availablePercChangeOptions = ko.computed(function () {
                    if (self.chart()) {
                        var retArr = [];
                        retArr.push(self.percChangeOptions[0]);
                        retArr.push(self.percChangeOptions[1]);
                        if ((self.chart().comparativeScenario() == "1" || self.chart().comparativeScenario() == null) && self.niiOption() == "nii") {
                            retArr.push(self.percChangeOptions[2]);
                        }
                        return retArr;
                    }
                });
                //capture thsi becuase of nested composes
                app.on('application:addedLeftChartSimulation').then(function () {
                    if (self.chart()) {
                        if (displayOptions.side == 'left') {
                            //Take last item in array abnd add subscriptuosna nd get simulations for it
                            var item = self.chart().chartSimulationTypes()[self.chart().chartSimulationTypes().length - 1];
                            item.simulationTypes = ko.observableArray();

                            globalContext.getAvailableSimulations(item.simulationTypes, self.chart().institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                                self.subscriptions.push(item.asOfDateOffset.subscribe(function (newValue) {
                                    self.refreshItemSim(this);
                                }, item));

                                self.subscriptions.push(item.simulationTypeId.subscribe(function (newValue) {
                                    self.refreshScens(item);

                                }, item));

                                self.refreshScens();
                            });
                        }
                    }
                });


                app.on('application:addedRightChartSimulation').then(function () {
                    if (self.chart()) {
                        if (displayOptions.side == 'right') {
                            //Take last item in array abnd add subscriptuosna nd get simulations for it
                            var item = self.chart().chartSimulationTypes()[self.chart().chartSimulationTypes().length - 1];
                            item.simulationTypes = ko.observableArray();

                            globalContext.getAvailableSimulations(item.simulationTypes, self.chart().institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                                self.subscriptions.push(item.asOfDateOffset.subscribe(function (newValue) {
                                    self.refreshItemSim(this);
                                }, item));

                                self.subscriptions.push(item.simulationTypeId.subscribe(function (newValue) {
                                    self.refreshScens(item);

                                }, item));

                                self.refreshScens();
                            });
                        }
                    }
                });

                this.chartsimulationselectconfig = new chartsimulationselectconfig(self.chart, displayOptions.showInst, displayOptions.canAddSimulations, self.simulationTypes, displayOptions.side);

                this.cancel = function () {
                    self.sortChartScenarioTypes();
                    self.chartsimulationselectconfig.sortChartSimulationTypes();
                };


                this.showPercChange = displayOptions.showPercChange;
                this.showComparatives = displayOptions.showComparatives;

                //this.sortedChartScenarioTypes = ko.observableArray(self.chart().chartScenarioTypes());
                this.sortedChartScenarioTypes = ko.observableArray();
                this.sortChartScenarioTypes = function () {
                    if (self.chart()) {
                        var sorted = self.chart().chartScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });
                        self.sortedChartScenarioTypes(sorted);
                    }

                };

                this.addScenarioType = function (scenarioType) {
                    var newChartScenarioType = globalContext.createChartScenarioType();
                    newChartScenarioType.scenarioTypeId(scenarioType.id);
                    newChartScenarioType.priority(self.chart().chartScenarioTypes().length);

                    if (self.chart().chartScenarioTypes().length == 0 && self.showComparatives) {
                        self.chart().comparativeScenario(scenarioType.id);
                    }

                    //newChartScenarioType.scenarioType = ko.observable({ name: ko.observable(scenarioType.name) });

                    self.chart().chartScenarioTypes().push(newChartScenarioType);
                    self.sortChartScenarioTypes();
                };

                this.dontAddScenarioType = function (data, event) {
                    event.stopPropagation();
                };

                this.removeSeries = function (series) {

                    var msg = 'Delete scenario ?';
                    var title = 'Confirm Delete';
                    return app.showMessage(msg, title, ['No', 'Yes'])
                        .then(confirmDelete);

                    function confirmDelete(selectedOption) {
                        if (selectedOption === 'Yes') {
                            series.entityAspect.setDeleted();
                            self.chart().chartScenarioTypes().forEach(function (chartScenarioType, index) {
                                chartScenarioType.priority(index);
                            });
                            self.sortChartScenarioTypes();


                        }
                    }
                };


                this.sortableAfterMove = function (arg) {
                    arg.sourceParent().forEach(function (chartScenarioType, index) {
                        chartScenarioType.priority(index);
                    });
                };

                //order of events matters here, need to reset the compare dropdown before it gets disabled
                this.enablePercChangeCompareSel = ko.computed(function () {
                    if (self.chart()) {
                        if (self.chart().percentChange() > 0) {
                            return true;
                        } else {
                            self.chart().percChangeComparator("year2Year1");
                            return false;
                        }
                    }
                });



                var getSelectedSimulationName = function (simulationTypeId) {
                    return self.chartsimulationselectconfig.sortedSimulationTypes()[0].simulationType().name();
                };

                var getAsOfDateOffsetLabel = function (offset) {
                    if (self.chartsimulationselectconfig.asOfDateOffsets().length > 0) {
                        return self.chartsimulationselectconfig.asOfDateOffsets()[offset].asOfDateDescript;
                    } else {
                        return "";
                    }

                };

                this.chartNameComp = ko.computed(function () {
                    //this is essentially a void method whose only purpose is to update the actual model value of chart.name
                    //self.chart().name("Cahrt Name");
                    if (self.chart() != null &&
                        !self.chart().nameOverride() &&
                        self.chartsimulationselectconfig.sortedSimulationTypes().length > 0 &&
                        self.chartsimulationselectconfig.asOfDateOffsets().length > 0 &&
                        self.chartsimulationselectconfig.sortedSimulationTypes()[0].simulationTypes != null &&
                        self.chartsimulationselectconfig.sortedSimulationTypes()[0].simulationTypes != undefined &&
                        self.chartsimulationselectconfig.sortedSimulationTypes()[0].simulationTypes().length > 0) {
                        //var simulationTypeName = getSelectedSimulationName(self.chart().chartSimulationTypes()[0].simulationType().name());
                        var simulationTypeName = self.chart().chartSimulationTypes()[0].simulationTypes().filter(function (s) {
                            return s.id == self.chart().chartSimulationTypes()[0].simulationTypeId();
                        })[0].name;
                        var aodLabel = getAsOfDateOffsetLabel(self.chart().chartSimulationTypes()[0].asOfDateOffset());
                        var fullName = simulationTypeName + " Simulation as of " + aodLabel;
                        if (self.chart().name() != fullName) {
                            self.chart().name(fullName);
                        }

                    } else {
                        return "";
                    }
                });

                this.compositionComplete = function () {
                    //console.log("CHART CONFIG");
                    //console.log(globalContext.getChanges());
                    if (displayOptions.callSave && chart().id() > 0 && globalContext.hasChanges()) {
                        globalContext.saveChangesQuiet().then(function () {

                        });
                    }
                };


                this.defaultSimulationTypeId = function () {
                    if (!self.chart().overrideParentSimulationType()) {
                        var aod = self.asOfDateOffsets()[self.chart().chartSimulationTypes()[0].asOfDateOffset()].asOfDateDescript;
                        if (self.policyOffsets()[0][aod] != undefined) {
                            self.chart().chartSimulationTypes()[0].simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                        }
                    }
                };



                this.refreshScen = function () {
                    //Need to spoogf call
                    var sims = [];
                    var offsets = [];
                    var eveSims = [];
                    self.chart().chartSimulationTypes().forEach(function (item, index, arr) {
                        item.simulationTypes = ko.observableArray();

                        sims.push(item.simulationTypeId());
                        eveSims.push(item.simulationTypeId()); //Double push on here wont harm anything and we can resuse function
                        offsets.push(self.chart().asOfDateOffset());

                    });

                    return globalContext.getAvailableSimulationScenariosMultiple(self.scenarioTypes, self.chart().institutionDatabaseName(), offsets.join(), sims.join(), eveSims.join());
                }


                this.refreshItemSim = function (item) {
                    globalContext.getAvailableSimulations(item.simulationTypes, self.chart().institutionDatabaseName(), item.asOfDateOffset()).then(function () {
                        self.refreshScens();
                    });
                }

                this.refreshScens = function () {


                    //Need to spoogf call
                    var sims = [];
                    var offsets = [];
                    var eveSims = [];
                    self.chart().chartSimulationTypes().forEach(function (item, index, arr) {

                        sims.push(item.simulationTypeId());
                        eveSims.push(item.simulationTypeId()); //Double push on here wont harm anything and we can resuse function
                        offsets.push(item.asOfDateOffset());

                    });
                    console.log(displayOptions.side);
                    console.log('GETTTINGGG THEEEMMMMM');
                    return globalContext.getAvailableSimulationScenariosMultiple(self.scenarioTypes, self.chart().institutionDatabaseName(), offsets.join(), sims.join(), eveSims.join()).then(function () { console.log(self.scenarioTypes()) });

                }

                self.activate = function () {
                    globalContext.logEvent("Viewed", curPage);
                    if (self.chart()) {
                        this.sortedChartScenarioTypes(self.chart().chartScenarioTypes());
                        self.sortChartScenarioTypes();
                    }

                    var defaultAdd = false;
                    if (self.chart() && self.chart().chartSimulationTypes().length == 0) {
                        defaultAdd = true;
                    }



                    app.on('application:deleteLeftChartSimulation').then(function () {
                        self.refreshScens();
                    });

                    app.on('application:deleteRightChartSimulation').then(function () {
                        self.refreshScens();
                    });

                    app.on('application:readyToAddLeftDefaultSim').then(function () {
                        if (self.chart() && self.chart().chartSimulationTypes().length == 0) {
                            self.chartsimulationselectconfig.addChartSimulationType();
                        }
                    });
                    app.on('application:readyToAddRightDefaultSim').then(function () {
                        if (self.chart() && self.chart().chartSimulationTypes().length == 0) {
                            self.chartsimulationselectconfig.addChartSimulationType();
                        }
                    });


                    return globalContext.getPolicyOffsets(self.policyOffsets, self.chart().institutionDatabaseName()).then(function () {
                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.chart().institutionDatabaseName()).then(function () {

                            //self.defaultSimulationTypeId();
                            self.subscriptions.push(self.chart().comparativeScenario.subscribe(function () {
                                //if base is not the comparative scenario
                                //and if percent change w/ policies is selected
                                if (self.chart() && self.chart().comparativeScenario() != "1" && self.chart().percentChange() == "2") {
                                    self.chart().percentChange("1");
                                }
                            }));

                            self.subscriptions.push(self.chart().overrideParentSimulationType.subscribe(function () {
                                self.defaultSimulationTypeId();
                            }));

                            if (!defaultAdd) {
                                var offsetsProcessed = 0
                                self.chart().chartSimulationTypes().forEach(function (item, index, arr) {
                                    item.simulationTypes = ko.observableArray();

                                    globalContext.getAvailableSimulations(item.simulationTypes, self.chart().institutionDatabaseName(), item.asOfDateOffset()).then(function () {

                                        self.subscriptions.push(item.asOfDateOffset.subscribe(function (newValue) {
                                            self.refreshItemSim(this);
                                        }, item));

                                        self.subscriptions.push(item.simulationTypeId.subscribe(function (newValue) {
                                            self.refreshScens(item);

                                        }, item));


                                        offsetsProcessed++;
                                        //when done this just move ona nd setorrest chagnes function
                                        if (offsetsProcessed == arr.length) {
                                            self.refreshScens();
                                            self.defaultSimulationTypeId();
                                        }

                                    });
                                });

                            }



                            self.subscriptions.push(self.chart().institutionDatabaseName.subscribe(function (newValue) {
                                return Q.all([
                                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue),
                                    globalContext.getPolicyOffsets(self.policyOffsets, newValue)
                                ]).then(function () {
                                    self.refreshSim();
                                    self.defaultSimulationTypeId();
                                });
                            }));

                            self.availableScenarioTypes = ko.computed(function () {
                                //Need this or ko.computed flips out on chart deletion
                                if (!self.chart())
                                    return [];

                                var scenarioTypeIdsInUse = self.chart().chartScenarioTypes().map(function (chartScenarioType) {
                                    return chartScenarioType.scenarioTypeId();
                                });
                                var result = [];
                                self.scenarioTypes().forEach(function (scenarioType) {
                                    result.push({
                                        id: scenarioType.id(),
                                        name: scenarioType.name(),
                                        inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                    });
                                });
                                return result;
                            });
                        });
                    });

                };
                self.deactivate = function () {
                    for (var s = 0; s < self.subscriptions.length; s++) {
                        self.subscriptions[s].dispose();
                    }


                    app.off('application:addedChartSImulation');
                    app.off('application:deleteChartSimulation');
                    app.off('application:addedLeftChartSimulation');
                    app.off('application:addedRightChartSimulation');
                };
            }
        };
        return chartVm;
    })