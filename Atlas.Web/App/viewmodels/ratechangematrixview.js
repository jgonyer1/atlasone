﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var summaryRateVm = function (id) {
        var self = this;
        this.summaryRateViewData = ko.observable();

        this.compositionComplete = function () {
            if (self.summaryRateViewData().errors.length > 0 || self.summaryRateViewData().warnings.length > 0) {
                var viewData = self.summaryRateViewData();

                if (!viewData) return true;

                globalContext.reportErrors(viewData.errors, viewData.warnings);

                return true;
            }

            var tbl = self.summaryRateViewData().tableData;
            var scens = self.summaryRateViewData().scenario;
            var formatData = {};
            var formatDataFloors = {};
            var headRow = '<tr><th>Interest Indices</th>';

            for (var s = 0; s < scens.length; s++) {
                headRow += '<th>' + scens[s] + '</th>';
            }

            headRow += '</tr>';

            $('#rateHeader').append(headRow);

            for (var i = 0; i < tbl.length; i++) {
                var rec = tbl[i];

                if (numeral(rec.type) == 0) {
                    if (formatData[rec.name] == undefined) {
                        formatData[rec.name] = [];

                        for (var c = 0; c < scens.length; c++) {
                            formatData[rec.name][c] = '';
                        }
                    }

                    formatData[rec.name][scens.indexOf(rec.scenName)] = numeral(rec.bpMove).format();
                }
                else {
                    if (formatDataFloors[rec.name] == undefined) {
                        formatDataFloors[rec.name] = {floor: rec.eveDiscountFloor}
                    }
                }
            }

            //Interest Indicies Data
            for (var id in formatData) {
                var row = '<tr><td class="tab-plus-bps">' + id + '</td>';

                for (var z = 0; z < formatData[id].length; z++) {
                    if (formatData[id][z] == '') {
                        row += '<td>0</td>';
                    }
                    else {
                        row += '<td>' + formatData[id][z] + '</td>';
                    }
                }

                row += '</tr>';
                $('#rateBody').append(row);
            }

            //Discounts
            $('#floorHeader').append('<tr><th>Discounts</th><th>Floors</th><th colspan="' + (scens.length - 1) + '"></th></tr>');

            for (var id in formatDataFloors) {
                var row = '<tr><td>' + id + '</td>';
                row += '<td>' + numeral(formatDataFloors[id].floor).format('0.00') + '</td>';
                row += '<td colspan="' + scens.length - 1 + '"></td>';
                row += '</tr>';
                $('#floorBody').append(row);
            }

            global.loadingReport(false);
         //   $("#ratechange-view").height($("#ratechange-view").height());
            $('#reportLoaded').text('1');

            //Export To Pdf
            if (typeof hiqPdfInfo != "undefined") {
                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getRateChangeMatrixViewDataById(id, self.summaryRateViewData).then(function () {

            });
        };
    };

    return summaryRateVm;
});
