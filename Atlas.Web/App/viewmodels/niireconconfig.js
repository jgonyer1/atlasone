﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var vm = function (nii) {
        var self = this;

        this.nii = nii;

        function onChangesCanceled() {
            self.sortScenarioTypes();
        }

        this.cancel = function () {
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.timePeriods = ko.observableArray(['90 Day', '360 Day']);

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.institutions = ko.observableArray();

        this.modelSetup = ko.observable();


        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.nii().institutionDatabaseName(), self.nii().asOfDateOffset(), self.nii().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.nii().institutionDatabaseName(), self.nii().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }

        this.activate = function (id) {
            app.on('application:cancelChanges', onChangesCanceled);

            return Q.all([
                globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.nii().institutionDatabaseName(), self.nii().asOfDateOffset(), self.nii().simulationTypeId(), false),
                globalContext.getAvailableSimulations(self.simulationTypes, self.nii().institutionDatabaseName(), self.nii().asOfDateOffset()),
                globalContext.getAvailableBanks(self.institutions),
                globalContext.getModelSetup(self.modelSetup)
            ]).then(function () {
                self.availableScenarioTypes = ko.computed(function () {
                    var scenarioTypeIdsInUse = self.nii().scenarioTypes().map(function (scenario) {
                        return scenario.scenarioTypeId();
                    });
                    var result = [];
                    self.scenarioTypes().forEach(function (scenarioType) {
                        result.push({
                            id: scenarioType.id(),
                            name: scenarioType.name(),
                            inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                        });
                    });
                    return result;
                });

                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub = self.nii().institutionDatabaseName.subscribe(function (newValue) {
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                    self.refreshSim();
                });

                self.aodSub = self.nii().asOfDateOffset.subscribe(function (newValue) {
                    self.refreshSim();
                });

                self.simSub = self.nii().simulationTypeId.subscribe(function (newValue) {
                    self.refreshScen();
                });
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.nii().institutionDatabaseName()).then(function () { });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.simSub.dispose();
            self.aodSub.dispose();
        };

        this.compositionComplete = function () {
            $('#footnotes').hide();
        };

        //Scenario Block For Sorting, Adding, and Deleting From Scenario List
        this.sortedScenarioTypes = ko.observableArray(self.nii().scenarioTypes());

        this.sortScenarioTypes = function () {
            self.sortedScenarioTypes(self.nii().scenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.nii().scenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var newScen = globalContext.createNIIReconScenario();
            newScen.scenarioTypeId(scenarioType.id);
            newScen.priority(nii().scenarioTypes().length);
            nii().scenarioTypes().push(newScen);
            self.sortScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };

        this.removeScenarioType = function (scen) {
            var msg = 'Delete scenario "' + scen.scenarioType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    scen.entityAspect.setDeleted();
                    nii().scenarioTypes().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortScenarioTypes();
                }
            }
        };

        self.sortScenarioTypes();

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.setDefaultTaxEquiv = function () {
            if (!self.nii().override()) {
                self.nii().taxEquiv(self.modelSetup().reportTaxEquivalent());
            }
            return true;
        }

        return this;
    };

    return vm;
});
