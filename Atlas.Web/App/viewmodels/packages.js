﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model',
        './inputmodal'],
    function (globalContext, router, system, app, logger, model, inputModal) {
        var packages = ko.observable();
        var basicSurplusAdmins = ko.observableArray();
        var firstReport = ko.observable();
        var hasBS = false;
        var curPage = "packages";
        function activate() {
            globalContext.logEvent("Viewed", curPage);
            $('#package').text('');
            $('#packageTemplate').text('');
            return globalContext.getPackages(packages).then(function () {
                return globalContext.getBasicSurplusAdmins(basicSurplusAdmins).then(function () {
                    if (basicSurplusAdmins().length > 0) {
                        hasBS = true;
                    }
                });
            });
        }

        var goToFirstReportView = function (thePackage) {
            globalContext.getFirstReportInPackage(thePackage.id(), firstReport).fin(function () {
                if (firstReport() != -99) {
                    globalContext.logEvent("First report view", curPage);
                    router.navigate("#/reportview/" + firstReport());
                } else {
                    var msg = "This package doesn't contain any reports.";
                    var title = 'No reports found';
                    app.showMessage(msg, title, ['Ok']);
                }
                
            });



        };

        var goToDetail = function (thePackage) {
            router.navigate("#/packageconfig/" + thePackage.id());
        };

        var add = function () {
            if (!hasBS) {
                globalContext.logEvent("Tried add pkg before having BS", curPage);
                return app.showMessage("Please set up a Basic Surplus before adding Packages", "Error", ["Ok"]);
            }
            router.navigate("#/packageadd/");
        };

        var copy = function (thePackage) {
            
            inputModal.show({
                title: "Copy Package",
                message: 'Please choose a name for the new package',
                hideCancel: false,
                validation: function (value) {
                    var fvalue = value.toLowerCase().trim();
                    var sortedPkgs = packages;

                    for (var i = 0; i < sortedPkgs.length; i++) {
                        if (sortedPkgs[i].name().toLowerCase().trim() == fvalue) {
                            return {
                                success: false,
                                title: title,
                                message: 'There is a conflicting package with name ' + value
                            };
                        }
                    }

                    return { success: true };
                }
            }).then(function (pkgName) {
                if (pkgName === null) {
                    return;
                }
                var newPackageId = ko.observable();
                globalContext.newCopyPackage(thePackage.id(), false, pkgName, newPackageId).fin(function () {
                    goToEditView(newPackageId());
                });
            });

            

            function goToEditView(id) {
                router.navigate('#/packageconfig/' + id);
            }

        };

        //var copy = function (thisPackage) {
        //    router.navigate("#/packagecopy/" + thisPackage.id());
        //};

        var vm = {
            activate: activate,
            title: 'Packages',
            packages: packages,
            goToFirstReportView: goToFirstReportView,
            goToDetail: goToDetail,
            add: add,
            copy: copy
        };

        return vm;

        //#region Internal Methods


        //#endregion

    });
