﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id) {
        var self = this;
        this.ascViewData = ko.observable();

        this.compositionComplete = function () {
            global.loadingReport(false);
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
                $('#reportLoaded').text('1');
            }
        }


        this.activate = function () {
            globalContext.logEvent("Viewed", "ASCc825datasourceview");
            global.loadingReport(true);
            return globalContext.getASC825DataSourceViewDataById(id, self.ascViewData).then(function () {

            });
        };
    };

    return ascVm;

});
