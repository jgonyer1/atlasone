﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app',
    'viewmodels/lookbackreportsectionconfig'
],
function (globalContext, global, app, lookbackreportsectionconfig) {
    var lb = ko.observable();

    var vm = function (lookbackreport) {
        var self = this;
        var curPage = "LookbackReportConfig";
        function onChangesCanceled() {
            self.sortSections();
        }

        this.lb = lookbackreport;
        this.profile = ko.observable();
        this.cancel = function () {
        };
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();
        this.layouts = ko.observableArray();
        this.subs = [];

        this.institutions = ko.observableArray();
        //list of all the lookbacks
        this.allLookbacks = ko.observableArray();
        this.firstAcivatePass = false;
        //list of sections, with some extras for UI
        this.sortedSections = ko.observableArray();

        this.getLookbacksForType = function (type) {
            var arr = ko.observableArray();
            var temp = [];
            if (type.availLookbacks == undefined) {
                type.availLookbacks = ko.observableArray();
            }
            globalContext.getLBLookbacksByOffset(type.asOfDateOffset(), self.lb().institutionDatabaseName, arr).then(function () {
                temp = arr().filter(function (item) {
                    return item.lookbackGroups()[0].layoutId() == type.lookbackReportSection().lBLookbackLayoutId();
                });
                if (temp.length == 0) {
                    temp.push({
                        name: "N/A",
                        id: -1
                    });
                }
                type.availLookbacks(temp);
                self.typesProcessed++;
                if (self.typesProcessed == self.totalNumTypes && self.firstAcivatePass) {
                    globalContext.saveChangesQuiet().then(function () {
                        global.loadingReport(false);
                        self.sortSections();
                        self.firstAcivatePass = false;
                    });
                }
            });
        };

        this.getLookbacksForAllTypesInSection = function (section) {
            section.lookbackTypes().forEach(function (lbt, idx, arr) {
                self.getLookbacksForType(lbt);
            });
        };

        self.lb().institutionDatabaseName.subscribe(function () {
            //when the inst is changed we need to loop through all the sections, all the lookbacks, and update the available lookbacks
            self.lb().sections().forEach(function (sec, index, arr) {
                var n = 0;
                sec.lookbackTypes().forEach(function (lbT, ind, lbTs) {
                    globalContext.getLBLookbacksByOffset(lbT.asOfDateOffset(), self.lb().institutionDatabaseName, lbT.availLookbacks);
                });
            });
        });

        this.addSection = function () {
            var newSection = globalContext.createLookbackReportSection();
            newSection.name('New Section ' + self.lb().sections().length);
            newSection.priority(self.lb().sections().length);
            newSection.sortedLookbackTypes = ko.observableArray(newSection.lookbackTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
            self.subs.push(newSection.lBLookbackLayoutId.subscribe(function () {
                self.getLookbacksForAllTypesInSection(newSection);
            }));
            //var newSectionVM = new lookbackreportsectionconfig(newSection, self.allLookbacks, self.lb().institutionDatabaseName);
            //self.sectionVms().push(newSectionVM);
            newSection.hasRateVolConflict = ko.pureComputed(function () {
                var ret = false;
                var curRV = false;
                newSection.lookbackTypes().forEach(function (lbt, idx, arr) {
                    var lbId = lbt.lBLookbackId();
                    var lb = lbt.availLookbacks().filter(function (lb) {
                        return lb.id() == lbId;
                    })[0];
                    var rv;
                    if (lb != undefined) {
                        rv = lb.lookbackGroups()[0].rateVol();
                        if (idx == 0) {
                            curRV = rv;
                        } else {
                            if (curRV != rv) {
                                ret = true;
                            }
                        }
                    }

                })
                return ret;
            });
            self.lb().sections().push(newSection);
            self.sortSections();
            return true;
        };

        this.removeSection = function (section) {

            var msg = 'Delete section ' + section.name() + '?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    for (var i = section.lookbackTypes().length - 1; i >= 0; i--) {
                        section.lookbackTypes()[i].entityAspect.setDeleted();
                    }
                    
                    
                    section.entityAspect.setDeleted();
                    self.lb().sections().forEach(function (aSection, index) {
                        aSection.priority(index);
                    });
                    self.sortSections();
                    //globalContext.saveChangesQuiet();
                }
            }

        };

        this.addLookbackToSection = function (section) {
            var newLookbackType = globalContext.createLookbackType();
            newLookbackType.asOfDateOffset(section.lookbackTypes().length);
            newLookbackType.priority(section.lookbackTypes().length);
            newLookbackType.asOfDateOffset.subscribe(function () {
                self.getLookbacksForType(newLookbackType);
            });
            newLookbackType.availLookbacks = ko.observableArray();
            self.getLookbacksForType(newLookbackType);
            //globalContext.getLBLookbacksByOffset(newLookbackType.asOfDateOffset(), self.lb().institutionDatabaseName, newLookbackType.availLookbacks);
            newLookbackType.availLookbacks.subscribe(function () {
                if (newLookbackType.availLookbacks().length == 0) {
                    newLookbackType.asOfDateOffset(0);
                    toastr.warning("No Lookbacks found for that date offset");
                }
            });

            section.lookbackTypes().push(newLookbackType);
            self.sortSections();
            //self.sortLookbackTypes(section);
            return true;
        };

        this.removeLookbackType = function (lookbackType) {

            var msg = 'Delete lookback ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                var parentSec = lookbackType.lookbackReportSection;
                if (selectedOption === 'Yes') {
                    lookbackType.entityAspect.setDeleted();
                    self.sortSections();
                }
            }

        };

        this.sortLookbackTypes = function (section) {
            section.sortedLookbackTypes(section.lookbackTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.dontAddSection = function (data, event) {
            event.stopPropagation();
        };

        this.sectionAfterMove = function (arg) {
            self.sortedSections().forEach(function (vm, index, arr) {
                vm.priority(index);
            });
            self.sortSections();
        };
        this.lbTypeAfterMove = function (arg) {
            self.lb().sections().forEach(function (vm, index, arr) {
                vm.lookbackTypes().forEach(function (lbt, ind, lbArr) {
                    lbt.priority(ind);
                });
            });
        };



        this.updateAvailLookbacks = function (lookbackType) {
            globalContext.getLBLookbacksByOffset(lookbackType.asOfDateOffset(), self.lb().institutionDatabaseName, lookbackType.availLookbacks);
            var n = 0;
        };


        this.sortSections = function () {
            var vms = ko.observableArray();
            var sortedVms = self.lb().sections().sort(function (a, b) {
                return a.priority() - b.priority();
            });
            self.sortedSections(sortedVms);
            self.lb().sections().forEach(function (sec, indx, arr) {
                self.sortLookbackTypes(sec);
            });
            return true;
        };

        this.compositionComplete = function () {
            if (globalContext.hasChanges() && self.lb().id() > 0) {
                globalContext.saveChangesQuiet();
            }
        };

        this.typesProcessed = 0;
        this.secsProcessed = 0;
        this.totalNumTypes = 0;

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return globalContext.getUserProfile(self.profile).then(function () {
                return globalContext.getLBLookbacks(self.allLookbacks, self.profile().asOfDate).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        return globalContext.getLBCustomLayouts(self.layouts).then(function () {
                            return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.lb().institutionDatabaseName()).then(function () {
                                self.firstAcivatePass = true;
                                //AsOfDateOffset Handling (Single-Institution Template)
                                self.instSub = self.lb().institutionDatabaseName.subscribe(function (newValue) {
                                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                                });

                                for (var s = 0; s < self.lb().sections().length; s++){
                                    let sc = self.lb().sections()[s];
                                    self.totalNumTypes += sc.lookbackTypes().length;
                                }
                                self.lb().sections().forEach(function (sec, index, secs) {
                                    self.subs.push(sec.lBLookbackLayoutId.subscribe(function () {
                                        self.getLookbacksForAllTypesInSection(sec);
                                    }));
                                    sec.sortedLookbackTypes = ko.observableArray(sec.lookbackTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
                                    sec.lookbackTypes().forEach(function (lbT, idx, lbTs) {
                                        lbT.availLookbacks = ko.observableArray();
                                        self.getLookbacksForType(lbT);
                                        self.subs.push(lbT.asOfDateOffset.subscribe(function () {
                                            self.getLookbacksForType(lbT);
                                        }));
                                        self.subs.push(lbT.availLookbacks.subscribe(function () {
                                            if (lbT.availLookbacks().length == 0) {
                                                lbT.asOfDateOffset(0);
                                                toastr.warning("No Lookbacks found for that date offset");
                                            }
                                        }));
                                        //self.typesProcessed++;
                                    });
                                    sec.hasRateVolConflict = ko.pureComputed(function () {
                                        var ret = false;
                                        var curRV = false;
                                        sec.lookbackTypes().forEach(function (lbt, idx, arr) {
                                            var lbId = lbt.lBLookbackId();
                                            var lb = lbt.availLookbacks().filter(function (lb) {
                                                return lb.id() == lbId;
                                               })[0];
                                            var rv;
                                            if (lb != undefined) {
                                                rv = lb.lookbackGroups()[0].rateVol();
                                                if (idx == 0) {
                                                    curRV = rv;
                                                } else {
                                                    if (curRV != rv) {
                                                        ret = true;
                                                    }
                                                }
                                            }

                                        })
                                        return ret;
                                    });
                                    //self.secsProcessed++;
                                    //if (self.secsProcessed == secs.length && self.typesProcessed == totalNumTypes) {
                                    //    
                                    //    if (self.lb().id() > 0) {
                                    //        globalContext.saveChangesQuiet();
                                    //    }
                                    //    global.loadingReport(false);
                                    //}
                                });
                                
                            });
                        });                        
                    });
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return vm;
});
