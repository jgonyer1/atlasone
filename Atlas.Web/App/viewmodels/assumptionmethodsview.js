﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id, rep) {
        var self = this;
        this.thisReport = rep;
        this.assViewData = ko.observable();

        this.compositionComplete = function () {
            
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }
        }


        this.compositionComplete = function () {
           
            for (var z = 0; z < self.thisReport().footnotes().length; z++) {
                $('#fn_list').append('<li>' + self.thisReport().footnotes()[z].text() + '</li>');
            }

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                //$('#lastSection').css('min-height', '755px');
                $('#reportHolder').height($('#reportHolder').height());
                $('#last-prg').addClass('last-on-page');
                $('#int-rate-header').addClass('first-on-page');
                //global.correctPDFTable("reportHolder");
                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            globalContext.logEvent("Viewed", "AssumptionMethodsView");
            global.loadingReport(true);
            return globalContext.getAssumptionMethodsViewDataById(id, self.assViewData).then(function () {
                var viewData = self.assViewData();

                if (!viewData) return;

                globalContext.reportErrors(viewData.errors, viewData.warnings);
            });
        };
    };

    return ascVm;

});
