﻿define([
    'services/globalcontext',
    'durandal/app',
    'services/global',
],
    function (globalContext, app, global) {

        var coverPageVm = function (coverPage) {
            var self = this;
            var curPage = "CoverPageConfig";
            this.coverPage = coverPage;
            this.institutions = ko.observableArray();
            this.bankInfo = ko.observable();
            this.policy = ko.observable();
            this.subs = [];
            this.defaultMeetingDate = function () {
                if (!self.coverPage().overrideMeetingDate()) {
                    self.coverPage().meetingDate(self.policy().meetingDate());
                }
            };
            
            this.cancel = function () {
            };

            this.compositionComplete = function () {
                if (self.coverPage().id() > -1) {
                    globalContext.saveChangesQuiet();
                }
            };

            this.packageTypeOptions = [
                {
                    value: 0, name: "ALCO Package", title: function () {
                        return "Asset/Liability Management Review";
                    }
                },
                {
                    value: 1, name: "EVE/NEV Package", title: function () {
                        if (self.bankInfo().creditUnion.instType == "BK") {
                            return "Summary of Economic Value of Equity Calculation and Assumptions";
                        } else {
                            return "Summary of Net Economic Value Calculation and Assumptions";
                        }
                    }
                },
                {
                    value: 2, name: "FASB ASC825 Package", title: function () {
                        return "Summary of FASB ASC 825 Methodologies & Assumptions";
                    }
                },
                {
                    value: 3, name: "Stress Test Package", title: function () {
                        return "Stress Test Analysis";
                    }
                },
                {
                    value: 4, name: "Assumptions Package", title: function () {
                        return "Summary of Simulation Assumptions";
                    }
                },
                {
                    value: 5, name: "Custom Package", title: function () {
                        return "Custom Package";
                    }
                },
            ];

            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage);
                global.loadingReport(true);
                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return globalContext.getPolicy(self.policy).then(function () {
                        return globalContext.getBankInfo(self.bankInfo, null).then(function () {
                            self.defaultMeetingDate();
                            self.subs.push(self.coverPage().overrideMeetingDate.subscribe(function () {
                                self.defaultMeetingDate();
                            }));
                            self.subs.push(self.coverPage().packageType.subscribe(function () {
                                self.coverPage().packageName(self.packageTypeOptions[self.coverPage().packageType()].title());
                            }));
                            global.loadingReport(false);
                        });
                    });
                });
            };
            this.deactivate = function () {
                for (var i = 0; i < self.subs.length; i++) {
                    self.subs[i].dispose();
                }
            };
        };


        return coverPageVm;

    });
