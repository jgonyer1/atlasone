﻿define([
    'services/globalcontext',
    'services/global',
    'viewmodels/simulationselectconfig',
    'durandal/app'
],
function (globalContext, global, simulationselectconfig, app) {
    var prepayDetVm = function (prepayDet) {
        var self = this;

        this.prepayDet = prepayDet;

        this.simulationselectconfig = new simulationselectconfig(self.prepayDet().institutionDatabaseName, self.prepayDet().asOfDateOffset, self.prepayDet().simulationTypeId, self.prepayDet().overrideSimulationTypeId, true);

        this.sortedPrepayDetScenarioTypes = ko.observableArray(self.prepayDet().prePaymentDetailScenarioTypes());

        this.sortPrepayDetScenarioTypes = function () {
            self.sortedPrepayDetScenarioTypes(self.prepayDet().prePaymentDetailScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.prepayDet().prePaymentDetailScenarioTypes().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var newprepayDetScenarioType = globalContext.createPrepyamentDetailsScenarioType();
            newprepayDetScenarioType.scenarioTypeId(scenarioType.id);
            newprepayDetScenarioType.priority(prepayDet().prePaymentDetailScenarioTypes().length);

            //newprepayDetScenarioType.scenarioType = ko.observable({ name: ko.observable(scenarioType.name) });

            prepayDet().prePaymentDetailScenarioTypes().push(newprepayDetScenarioType);
            self.sortPrepayDetScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };

        this.removeScenarioType = function (prepayDetScenarioType) {

            var msg = 'Delete scenario "' + prepayDetScenarioType.scenarioType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    prepayDetScenarioType.entityAspect.setDeleted();
                    prepayDet().prePaymentDetailScenarioTypes().forEach(function (anPrepayDetScenarioType, index) {
                        anPrepayDetScenarioType.priority(index);
                    });
                    self.sortPrepayDetScenarioTypes();
                }
            }

        };

        this.cancel = function () {
            self.sortPrepayDetScenarioTypes();
        };
        self.sortPrepayDetScenarioTypes();

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();



        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.prepayDet().institutionDatabaseName(), self.prepayDet().asOfDateOffset(), self.prepayDet().simulationTypeId(), true)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.prepayDet().institutionDatabaseName(), self.prepayDet().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }



        this.activate = function (id) {
            global.loadingReport(true);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                self.refreshSim();
                self.availableScenarioTypes = ko.computed(function () {
                    console.log('compluting');
                            var scenarioTypeIdsInUse = self.prepayDet().prePaymentDetailScenarioTypes().map(function (prePaymentDetailsScenarioType) {
                                return prePaymentDetailsScenarioType.scenarioTypeId();
                            });
                            var result = [];
                            self.scenarioTypes().forEach(function (scenarioType) {
                                result.push({
                                    id: scenarioType.id(),
                                    name: scenarioType.name(),
                                    inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                });
                            });
                            return result;
                        });

                       

                        //AsOfDateOffset Handling (Single-Institution Template)
                        self.instSub = self.prepayDet().institutionDatabaseName.subscribe(function (newValue) {
                            globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                            self.refreshSim();
                        });

                        self.aodSub = self.prepayDet().institutionDatabaseName.subscribe(function (newValue) {       
                            self.refreshSim();
                        });

                        self.simSub = self.prepayDet().simulationTypeId.subscribe(function (newValue) {
                            self.refreshScen();
                        });

                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.prepayDet().institutionDatabaseName()).then(function () {
                            global.loadingReport(false);
                        });
                    });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.simSub.dispose();
        };

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (prepaymentDetailsScenarioType, index) {
                prepaymentDetailsScenarioType.priority(index);
            });
        };

        return this;
    };

    return prepayDetVm;
});
