﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var liabVm = function (id) {
        var curPage = "LiabilityPricingAnalysisView";
        var self = this;
        this.liab = ko.observable();
        this.report = ko.observable();
        function RateCodeBucket(rateName) {
            if (rateName.indexOf('3 Month') > -1 || rateName.indexOf('3mo') > -1) {
                return '3 Month';
            }
            else if (rateName.indexOf('6 Month') > -1 || rateName.indexOf('6mo') > -1) {
                return '6 Month';
            }
            else if (rateName.indexOf('9 Month') > -1 || rateName.indexOf('9mo') > -1) {
                return '9 Month';
            }
            else if (rateName.indexOf('12 Month') > -1 || rateName.indexOf('1yr') > -1 || rateName.indexOf('1 Year') > -1) {
                return '12 Month';
            }
            else if (rateName.indexOf('2 Year') > -1 || rateName.indexOf('2yr') > -1) {
                return '24 Month';
            }
            else if (rateName.indexOf('3 Year') > -1 || rateName.indexOf('3yr') > -1) {
                return '36 Month';
            }
            else if (rateName.indexOf('4 Year') > -1 || rateName.indexOf('4yr') > -1) {
                return '48 Month';
            }
            else if (rateName.indexOf('5 Year') > -1 || rateName.indexOf('5yr') > -1) {
                return '60 Month';
            }
        }

        function GetTerm(term) {
            var strTerm = term.substring(0, term.indexOf(' '));
            return parseInt(strTerm);
        }

        this.compositionComplete = function () {
            var data = {};

            globalContext.reportErrors(self.liab().errors, self.liab().warnings);
            if (self.liab().errors.length > 0) {
                return true;
            }

            //Set FHLB Bucket Name 
            $('#fhlbDistrict').text('FHLB ' + self.liab().district);
            var marketRates = {};
            marketRates['Fed Funds Target'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['1 Month LIBOR'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['1 Month CMT'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['1 Month FHLB'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['3 Month LIBOR'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['3 Month CMT'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['3 Month FHLB'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['3 Month Brokered'] = {
                current: 0,
                prior: 0,
                chg: 0
            };
            marketRates['Money Fund(Taxable Act)'] = {
                current: 0,
                prior: 0,
                chg: 0
            };

            var marketRateLookUp = {};
            marketRateLookUp['(006) LIBOR 1 Month'] = '1 Month LIBOR';
            marketRateLookUp['(010) CMT 1 Month'] = '1 Month CMT';
            marketRateLookUp['(002) FedFunds'] = 'Fed Funds Target';
            marketRateLookUp['(007) LIBOR 3 Month'] = '3 Month LIBOR';
            marketRateLookUp['(011) CMT 3 Month'] = '3 Month CMT';
            marketRateLookUp["(156) Brokered CD's (FNC) 3 Month"] = '3 Month Brokered';
            marketRateLookUp['(175) IBC Data Money Fund'] = 'Money Fund(Taxable Act)';


            var nowRates = {};
            var savRates = {};
            var mmdaRates = {};

            //Market Rates
            for (var i = 0; i < self.liab().marketRates.length; i++) {
                var rec = self.liab().marketRates[i];
                var lookUpName = '';
                if (rec.name.indexOf('FHLB') > 0) {

                    if (rec.name.indexOf('1 Month') > 0) {
                        lookUpName = '1 Month FHLB';
                    }
                    else {
                        lookUpName = '3 Month FHLB';
                    }

                }
                else {
                    lookUpName = marketRateLookUp[rec.name];

                }
                marketRates[lookUpName].current = rec.curRate;
                marketRates[lookUpName].prior = rec.prRate;
                marketRates[lookUpName].chg = rec.rateDiff;
            }

            for (var c in marketRates) {
                var row = '<tr><td style="text-align: left">' + c + '</td>';
                row += '<td>' + numeral(marketRates[c].current).format('0.00') + '%</td>';
                row += '<td>' + numeral(marketRates[c].prior).format('0.00') + '%</td>';
                row += '<td class="chgCell">' + numeral(marketRates[c].chg).format('0.00') + '%</td>';
                row += '</tr>';
                $('#marketRates').append(row);
            }

            //Now, Savings, MMDA
            for (var i = 0; i < self.liab().nonMatRates.length; i++) {
                var rec = self.liab().nonMatRates[i];
                switch (rec.type) {
                    case 'NOW':
                    case 'Public NOW':
                    case 'Online NOW':
                    case 'Brokered NOW':
                        nowRates[rec.name] = {
                            current: rec.curRate,
                            prior: rec.prRate,
                            chg: rec.diff
                        }
                        break;
                    case 'DDA':
                    case 'Public DDA':
                    case 'Online DDA':
                    case 'Savings':
                    case 'Public Savings':
                    case 'Online Savings':
                        savRates[rec.name] = {
                            current: rec.curRate,
                            prior: rec.prRate,
                            chg: rec.diff
                        }
                        break;
                    case 'MMDA':
                    case 'Public MMDA':
                    case 'Online MMDA':
                    case 'Brokered Reciprocal MMDA':
                    case 'Brokered One-Way MMDA':
                    case 'Brokered MMDA':
                    case 'Other Non-Mat Deposit':
                        mmdaRates[rec.name] = {
                            current: rec.curRate,
                            prior: rec.prRate,
                            chg: rec.diff
                        }
                        break;
                }
            }


            //Now Table
            for (var c in nowRates) {
                var row = '<tr><td style="text-align: left">' + c + '</td>';
                row += '<td>' + numeral(nowRates[c].current).format('0.00') + '%</td>';
                row += '<td>' + numeral(nowRates[c].prior).format('0.00') + '%</td>';
                row += '<td class="chgCell">' + numeral(nowRates[c].chg).format('0.00') + '%</td>';
                row += '</tr>';
                $('#nowRates').append(row);
            }

            //Savings Table
            for (var c in savRates) {
                var row = '<tr><td style="text-align: left">' + c + '</td>';
                row += '<td>' + numeral(savRates[c].current).format('0.00') + '%</td>';
                row += '<td>' + numeral(savRates[c].prior).format('0.00') + '%</td>';
                row += '<td class="chgCell">' + numeral(savRates[c].chg).format('0.00') + '%</td>';
                row += '</tr>';
                $('#savRates').append(row);
            }

            //MMDA Table
            for (var c in mmdaRates) {
                var row = '<tr><td style="text-align: left">' + c + '</td>';
                row += '<td>' + numeral(mmdaRates[c].current).format('0.00') + '%</td>';
                row += '<td>' + numeral(mmdaRates[c].prior).format('0.00') + '%</td>';
                row += '<td class="chgCell">' + numeral(mmdaRates[c].chg).format('0.00') + '%</td>';
                row += '</tr>';
                $('#mmdaRates').append(row);
            }

            //Time deposit/Term Rates
            var timeDeps = {};

            for (var i = 0; i < self.liab().timeDepRates.length; i++) {
                var rec = self.liab().timeDepRates[i];
                timeDeps[rec.term] = {
                    current: rec.curRate,
                    prior: rec.prRate,
                    chg: rec.diff,
                    special: rec.special,
                    treasury: {
                        current: 0,
                        prior: 0,
                        chg: 0,
                        bank: 0
                    },
                    libor: {
                        current: 0,
                        prior: 0,
                        chg: 0,
                        bank: 0
                    },
                    fhlb: {
                        current: 0,
                        prior: 0,
                        chg: 0,
                        bank: 0
                    },
                    brokered: {
                        current: 0,
                        prior: 0,
                        chg: 0,
                        bank: 0
                    },

                };
            }

            for (var i = 0; i < self.liab().treasuryRates.length; i++) {
                var rec = self.liab().treasuryRates[i];
                var rtBucket = RateCodeBucket(rec.name);
                timeDeps[rtBucket].treasury.current = rec.curRate;
                timeDeps[rtBucket].treasury.prior = rec.prRate;
                timeDeps[rtBucket].treasury.chg = rec.rateDiff;
                timeDeps[rtBucket].treasury.bank = rec.curRate - timeDeps[rtBucket].current;
            }
            var newCur = (timeDeps['60 Month'].treasury.current + timeDeps['36 Month'].treasury.current) / 2;
            var newPr = (timeDeps['60 Month'].treasury.prior + timeDeps['36 Month'].treasury.prior) / 2;
            timeDeps['48 Month'].treasury.current = newCur;
            timeDeps['48 Month'].treasury.prior = newPr;
            timeDeps['48 Month'].treasury.chg = newCur - newPr;
            timeDeps['48 Month'].treasury.bank = newCur - timeDeps['48 Month'].current;

            newCur = (timeDeps['12 Month'].treasury.current + timeDeps['6 Month'].treasury.current) / 2;
            newPr = (timeDeps['12 Month'].treasury.prior + timeDeps['6 Month'].treasury.prior) / 2;
            timeDeps['9 Month'].treasury.current = newCur;
            timeDeps['9 Month'].treasury.prior = newPr;
            timeDeps['9 Month'].treasury.chg = newCur - newPr;
            timeDeps['9 Month'].treasury.bank = newCur - timeDeps['9 Month'].current;


            for (var i = 0; i < self.liab().brokeredRates.length; i++) {
                var rec = self.liab().brokeredRates[i];
                var rtBucket = RateCodeBucket(rec.name);
                timeDeps[rtBucket].brokered.current = rec.curRate;
                timeDeps[rtBucket].brokered.prior = rec.prRate;
                timeDeps[rtBucket].brokered.chg = rec.rateDiff;
                timeDeps[rtBucket].brokered.bank = rec.curRate - timeDeps[rtBucket].current;
            }

            for (var i = 0; i < self.liab().fhlbRates.length; i++) {
                var rec = self.liab().fhlbRates[i];
                var rtBucket = RateCodeBucket(rec.name);
                timeDeps[rtBucket].fhlb.current = rec.curRate;
                timeDeps[rtBucket].fhlb.prior = rec.prRate;
                timeDeps[rtBucket].fhlb.chg = rec.rateDiff;
                timeDeps[rtBucket].fhlb.bank = rec.curRate - timeDeps[rtBucket].current;
            }
            switch (self.liab().district) {
                case 'Chicago':
                case 'Des Moines':
                case 'New York':
                case 'San Francisco':
                case 'Seattle':
                    newCur = (timeDeps['12 Month'].fhlb.current + timeDeps['6 Month'].fhlb.current) / 2;
                    newPr = (timeDeps['12 Month'].fhlb.prior + timeDeps['6 Month'].fhlb.prior) / 2;
                    timeDeps['9 Month'].fhlb.current = newCur;
                    timeDeps['9 Month'].fhlb.prior = newPr;
                    timeDeps['9 Month'].fhlb.chg = newCur - newPr;
                    timeDeps['9 Month'].fhlb.bank = newCur - timeDeps['9 Month'].current;
                    break;

            }

            for (var i = 0; i < self.liab().liborRates.length; i++) {
                var rec = self.liab().liborRates[i];
                var rtBucket = RateCodeBucket(rec.name);
                timeDeps[rtBucket].libor.current = rec.curRate;
                timeDeps[rtBucket].libor.prior = rec.prRate;
                timeDeps[rtBucket].libor.chg = rec.rateDiff;
                timeDeps[rtBucket].libor.bank =  rec.curRate - timeDeps[rtBucket].current;
            }

            newCur = (timeDeps['12 Month'].libor.current + timeDeps['6 Month'].libor.current) / 2;
            newPr = (timeDeps['12 Month'].libor.prior + timeDeps['6 Month'].libor.prior) / 2;
            timeDeps['9 Month'].libor.current = newCur;
            timeDeps['9 Month'].libor.prior = newPr;
            timeDeps['9 Month'].libor.chg = newCur - newPr;
            timeDeps['9 Month'].libor.bank = newCur - timeDeps['9 Month'].current;

            var currentSeries = [{
                name: 'Time Deposit Rates as of ' + self.liab().depositDate,
                data: [],
                marker: {
                    enabled: false
                }
            }, {
                name: self.liab().rateCurve + ' Rates as of ' + self.liab().webDate,
                data: [],
                marker: {
                    enabled: false
                }
            }, {
                type: 'scatter',
                name: 'Special',
                data: [],
                showInLegend: false,
                marker: {
                    symbol: 'diamond'
                }
            }];
            var min = 0;
            var max = 0;
            for (var c in timeDeps) {
                var row = '<tr><td  style="text-align: left;">' + c + '</td>';
                row += '<td>' + numeral(timeDeps[c].current).format('0.00') + '%</td>';
                row += '<td>' + numeral(timeDeps[c].prior).format('0.00') + '%</td>';
                row += '<td class="chgCell">' + numeral(timeDeps[c].chg).format('0.00') + '%</td>';
                if (timeDeps[c].special == 1) {
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank"></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank"></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank"></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank"></td>';
                    row += '<td></td>';
                    row += '<td></td>';
                    row += '<td"></td>';
                }
                else {
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank">' + numeral(timeDeps[c].treasury.bank).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].treasury.current).format('0.00') + '%</td>';             
                    row += '<td>' + numeral(timeDeps[c].treasury.prior).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].treasury.chg).format('0.00') + '%</td>';
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank">' + numeral(timeDeps[c].libor.bank).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].libor.current).format('0.00') + '%</td>';        
                    row += '<td>' + numeral(timeDeps[c].libor.prior).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].libor.chg).format('0.00') + '%</td>';
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank">' + numeral(timeDeps[c].fhlb.bank).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].fhlb.current).format('0.00') + '%</td>';    
                    row += '<td>' + numeral(timeDeps[c].fhlb.prior).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].fhlb.chg).format('0.00') + '%</td>';
                    row += '<td class="fillerCell"></td>';
                    row += '<td class="vsBank">' + numeral(timeDeps[c].brokered.bank).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].brokered.current).format('0.00') + '%</td>';    
                    row += '<td>' + numeral(timeDeps[c].brokered.prior).format('0.00') + '%</td>';
                    row += '<td>' + numeral(timeDeps[c].brokered.chg).format('0.00') + '%</td>';
                }


               
                var t = GetTerm(c);
                if (timeDeps[c].special == 1) {
                    if (c.indexOf('Day') > 0) {
                        t = 3;
                    }
                    currentSeries[2].data.push({ x: t, y: timeDeps[c].current, special: timeDeps[c].special });
                }
                else {
                    currentSeries[0].data.push({ x: t, y: timeDeps[c].current, special: timeDeps[c].special });                
                }

                if (min > timeDeps[c].current) {
                    min = timeDeps[c].current;
                }

                if (max < timeDeps[c].current) {
                    max = timeDeps[c].current;
                }

                var rateCurveRate = 0;
                if (timeDeps[c].special != 1) {
                    switch(self.liab().rateCurve){
                        case 'LIBOR/Swap':
                            rateCurveRate = timeDeps[c].libor.current;
                            currentSeries[1].data.push({ x: t, y: timeDeps[c].libor.current, special: 0 });
                            break;
                        case 'Treasury':
                            rateCurveRate = timeDeps[c].treasury.current;
                            currentSeries[1].data.push({ x: t, y: timeDeps[c].treasury.current, special: 0 });
                            break;
                        case 'FHLB':
                            rateCurveRate = timeDeps[c].fhlb.current;
                            currentSeries[1].data.push({ x: t, y: timeDeps[c].fhlb.current, special: 0 });
                            break;
                        case 'Brokered CD':
                            rateCurveRate = timeDeps[c].brokered.current;
                            currentSeries[1].data.push({ x: t, y: timeDeps[c].brokered.current, special: 0 });
                            break;
                    }
                }

                if (min > rateCurveRate) {
                    min = rateCurveRate;
                }

                if (max < rateCurveRate) {
                    max = rateCurveRate;
                }

                row += '</tr>';
                $('#timeDep').append(row);
            }


            //Build out Deposit History Chart


            //First collect all unique as of dates becuyase data is never acurate and consistent!
            var aods = [];
            var aodFound = {};
            var histCats = [];
            for (var i = 0; i < self.liab().histDeposit.length; i++){
                var rec = self.liab().histDeposit[i];
                //if unporcessesd as of date add it to the list
                if (aodFound[rec.month] == undefined) {

                    aodFound[rec.month] = aods.length;
                    //now push it onto array
                    aods.push({
                        TDBal: 0,
                        NMDBal: 0,
                        TDRate: 0,
                        NMDRate: 0,
                        TotalBal: 0,
                        TotalRate: 0,
                        date: rec.month
                    });

                    histCats.push(moment(rec.month).format('MMM-YY'));
                }


                if (rec.acc_TypeOR == '3') {
                    aods[aodFound[rec.month]].NMDBal = numeral(rec.totBalance).value();
                    aods[aodFound[rec.month]].NMDRate = numeral(rec.weightRate).value();
                }
                else {
                    aods[aodFound[rec.month]].TDBal = numeral(rec.totBalance).value();
                    aods[aodFound[rec.month]].TDRate = numeral(rec.weightRate).value();
                }

            }

            var histSeries = [
                {
                    type: 'area',
                    name: 'NMD',
                    data: [],
                    color: 'rgb(115,175,173)'
                },
                {
                    type: 'area',
                    name: 'TD',
                    data: [],
                    color: 'rgb(64,99,122)'
                },
                {
                    type: 'line',
                    name: 'NMD Cost',
                    data: [],
                    color: 'rgb(0,0,0)',
                    yAxis: 1
                },
                {
                    type: 'line',
                    name: 'TD Cost',
                    data: [],
                    color: 'rgb(217,217,0)',
                    yAxis: 1
                },
                {
                    type: 'line',
                    name: 'Total Cost',
                    data: [],
                    color: 'rgb(139,101,91)',
                    yAxis: 1
                }

            ];


            //if including retail add asterik next to label for NMD
            if (self.liab().incRetail) {
                histSeries[0].name = histSeries[0].name + '*';
            }


            for (var z = 0; z < aods.length; z++) {
                var data = aods[z];

               //do each series item
                histSeries[0].data.push({ x: moment(data.date).valueOf(), y: data.NMDBal })
                histSeries[1].data.push({ x: moment(data.date).valueOf(), y: data.TDBal })
                histSeries[2].data.push({ x: moment(data.date).valueOf(), y: data.NMDRate })
                histSeries[3].data.push({ x: moment(data.date).valueOf(), y: data.TDRate })

                var totalBals = data.NMDBal + data.TDBal;
                var totalRates = (data.NMDBal * data.NMDRate) + (data.TDBal * data.TDRate);

                var totalRate = 0;
                if (totalBals > 0) {
                    totalRate = totalRates / totalBals;
                }


                histSeries[4].data.push({ x: moment(data.date).valueOf(),  y: totalRate })
            }

            $('#depHistory').highcharts({
                chart: {
                },
                credits: {
                    enabled: self.liab().incRetail,
                    text: '*NMD includes Retail Repos'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'datetime',
                    minPadding: 0,
                    maxPadding: 0,
                    labels: {
                        formatter: function () {
                            return moment(this.value).format('MMM-YY');
                        }
                    },
                },
                plotOptions: {
                    series: {
                        animation: false,
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    formatter: function () {
                        var s = '' + this.x;
                        for (var i = 0; i < this.points.length; i++) {
                            var point = this.points[i];
                            var val = numeral(point.y).format('0,0');

                            if (point.series.name.indexOf('Cost') >= 0) {
                                val = numeral(point.y).format('0.00') + '%';
                            }

                            //Build Tooltip string
                            s += '<br />';
                            s += '<span style="color:' + point.point.series.color + ';font-weight: bold;">' + point.series.name + ': </span>';
                            s += '<span>' + val + '</span>';
                        }

                        return s;
                    },
                    shared: true

                },
                yAxis: [{
                    title: {
                        text: ''
                    }
                },
                {
                    title: {
                        text: ''
                    },
                    labels: {
                        formatter: function () {
                            return numeral(this.value).format('0.00');
                        }
                    },

                    opposite: true
                }


                ],
                series: histSeries,
                legend: {
                    margin: 0
                },
            });

            (function (H) {
                H.seriesTypes.waterfall.prototype.getCrispPath = function () {

                    var data = this.data,
                        length = data.length,
                        lineWidth = this.graph.strokeWidth() + this.borderWidth,
                        normalizer = Math.round(lineWidth) % 2 / 2,
                        reversedXAxis = this.xAxis.reversed,
                        reversedYAxis = this.yAxis.reversed,
                        arrHeight = 2,
                        arrOffset = 13,
                        path = [],
                        prevArgs,
                        pointArgs,
                        i,
                        d;

                    for (i = 1; i < length; i++) {
                        pointArgs = data[i].shapeArgs;
                        prevArgs = data[i - 1].shapeArgs;

                        d = [
                            'M',
                            prevArgs.x + (reversedXAxis ? 0 : prevArgs.width),
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer,
                            'L',
                            pointArgs.x + (reversedXAxis ? prevArgs.width : 0) - arrOffset,
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer,
                            pointArgs.x + (reversedXAxis ? prevArgs.width : 0) - arrOffset,
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer - arrHeight,
                            pointArgs.x + (reversedXAxis ? prevArgs.width : 0) - 8,
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer + arrHeight / 2,
                            pointArgs.x + (reversedXAxis ? prevArgs.width : 0) - arrOffset,
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer + 2 * arrHeight,
                            pointArgs.x + (reversedXAxis ? prevArgs.width : 0) - arrOffset,
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer + arrHeight,
                            prevArgs.x + (reversedXAxis ? 0 : prevArgs.width),
                            prevArgs.y + data[i - 1].minPointLengthOffset + normalizer + arrHeight,
                            'z'
                        ];

                        if (
                            (data[i - 1].y < 0 && !reversedYAxis) ||
                            (data[i - 1].y > 0 && reversedYAxis)
                        ) {
                            d[2] += prevArgs.height - arrHeight;
                            d[5] += prevArgs.height - arrHeight;
                            d[7] += prevArgs.height - arrHeight;
                            d[9] += prevArgs.height - arrHeight;
                            d[11] += prevArgs.height - arrHeight;
                            d[13] += prevArgs.height - arrHeight;
                            d[15] += prevArgs.height - arrHeight;
                        }

                        path = path.concat(d);
                    }

                    return path;
                }

                H.seriesTypes.waterfall.prototype.drawGraph = function () {
                    H.Series.prototype.drawGraph.call(this);
                    this.graph.attr({
                        d: this.getCrispPath(),
                        fill: '#40637a',
                        stroke: '255, 255, 255, 0.1',
                        'stroke-width': -2
                    });
                }
            })(Highcharts);

            var waterfallSeries = [{
                data: [],
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    },
                    useHTML: true,
                    formatter: function () {
                        //console.log(this);
                        //awesome plus sign
                        if (this.point.name == 'NEW') {
                            return '+' + numeral(this.y / 1000).format('$0,0') + '<br/>' + numeral(this.point.rate).format('0.00') + '%';
                        }
                        else {
                            return numeral(this.y / 1000).format('$0,0') + '<br/>' + numeral(this.point.rate).format('0.00') + '%';
                        }
                       
                    },
                },
                pointPadding: 0

            }];

            for (var z = 0; z < self.liab().waterfall.length; z++) {
                var rec = self.liab().waterfall[z];
                waterfallSeries[0].data.push({
                    name: rec.label,
                    y: numeral(rec.balance).value() / 1000,
                    rate: numeral(rec.rate).value(),
                    color: (rec.isIntermediateSum == '1' || z == 0 ? 'rgb(64,99,122)' : 'rgb(204,204,204)'),
                    isIntermediateSum: (rec.isIntermediateSum == '1' ? true : false)
                });
            }

            $('#depWaterfall').highcharts({
                chart: {
                    type: 'waterfall'
                },
                credits: { enabled: false },
                title: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        pointWidth: 70
                    }
                },
                legend: {
                    enabled: false
                },
                series: waterfallSeries
            });

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage); 
            }
            else {
                
                hiqPdfConverter.startConversion();
            }

        }

        this.activate = function () {
            global.loadingReport(true);
            return Q.all([
                  // globalContext.getPackageById(thisReport().packageId()),
                   globalContext.getLiabilityPricingAnalysisViewDataById(id, self.liab),
                   globalContext.getReportById(id, self.report)
            ]);
        };
    };

    return liabVm;

});
