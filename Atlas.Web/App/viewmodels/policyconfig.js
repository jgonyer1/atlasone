﻿define([
    'services/globalcontext',
    'services/basicsurplusservice',
    'plugins/router',
    'durandal/app',
    'viewmodels/basicsurplusadmin',
    'viewmodels/navigateawaymodal'
],
function (globalContext, basicSurplusService, router, app, bsAdmin, naModal) {
    var vm = function () {
        var self = this;
        this.bsAdmin = bsAdmin;

        //Vars
        this.eve = ko.observable();
        this.simulationTypes = ko.observableArray();
        this.basicSurplusAdmins = ko.observableArray();
        this.historicEves = ko.observableArray();
        this.riskOptions = ko.observableArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        this.riskAssesmentOptions = ko.observableArray([{ name: "None", value: 0 },{ name: "Low", value: 1 }, { name: "Low-Moderate", value: 2 }, { name: "Moderate", value: 3 }, { name: "Moderate-High", value: 4 }, { name: "High", value: 5 }]);
        this.reportedScenarioOptions = ko.observableArray();
        this.profile = ko.observable();
        this.sortedLiqPolicies = ko.observableArray();
        this.bankInfo = ko.observable();
        this.modelSetup = ko.observable();
        this.goToAddUserDefinedPolicy = function () {
            router.navigate("#/addpolicy/");
        }
        this.avgLifeAssumption = ko.observableArray(['5', '7.5', 'Other']);
        this.formatTypes = ko.observableArray(['#', '%', '$']);

        this.isSaving = ko.observable(false);

        //Navigation Helpers
        this.hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges();
        });

        this.canSave = ko.pureComputed(function () {
            return self.hasChanges() && !self.isSaving();
        });

        this.canDeactivate = function () {
            if (! self.hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { self.save().then(() => true); }
            );
        };

        this.UpdateAverageLife = function () {
            if (self.modelSetup().avgLifeAssumption() == 'Other') {
                self.modelSetup().avgLifeYears(0);
                self.modelSetup().avgLifeMessage('Average lives on non-maturity deposits are based upon a mm/dd/yyyy DCG deposit study. The aggregate average life based upon the current deposit mix is ##.## years. Please refer to the Deposit Study Tear Sheet for additional information. ');
            }
            else {
                self.modelSetup().avgLifeYears(parseFloat(self.modelSetup().avgLifeAssumption()));
                self.modelSetup().avgLifeMessage('This assessment assumed a ' + self.modelSetup().avgLifeAssumption() + ' year estimated life on the core deposit base.');
            }
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        //From MasterAM at http://stackoverflow.com/a/17831442
        this.goToTab = function (tab) {
            $('.nav-tabs a[href="' + tab + '"]').tab('show');
        };

        this.cancel = function () {
            self.isSaving(true);
            globalContext.cancelChanges();
            self.isSaving(false);
        };

        function stall(ms) {
            return new Promise(function (resolve, reject) {
                window.setTimeout(resolve, ms);
            });
        }

        this.save = function () {
            self.isSaving(true);
            return globalContext.saveChanges().then(complete);

            function complete() {
                return globalContext.defaultBasicSurplus(moment(self.eve().asOfDate()).format('MM/DD/YYYY')).fin(function () {
                    if (!self.changedSim) {
                        self.isSaving(false);
                        globalContext.hasChanges(false);
                        return;
                    }

                    self.refreshData();

                    var allNonOverridden = self.basicSurplusAdmins().filter(bsa => !bsa.overrideSimulationTypeId());

                    if (!allNonOverridden.length) {
                        self.changedSim = false;
                        self.isSaving(false);
                        globalContext.hasChanges(false);
                        return;
                    }

                    $("#linkedSimulation").modal({ backdrop: 'static', show: true});

                    async function process() {
                        for (var i = 0; i < allNonOverridden.length; i++) {
                            await basicSurplusService.activate(allNonOverridden[i].id(), null, 'noClearCache');
                            await stall(50);
                            await basicSurplusService.compositionComplete();
                            await stall(50);
                        }
                    }

                    process().then(() => {
                        $("#linkedSimulation").modal('hide');
                        self.changedSim = false;
                        self.isSaving(false);
                    });
                });            
            }
        };

        this.updateNIIChildren = function (niiParent) {
            for (var i = 0; i < niiParent.nIIPolicies().length; i++) {
                niiParent.nIIPolicies()[i].policyLimit(niiParent.policyLimit());
            }
        }

        this.capitalCalculation = function () {
            var t1Cap = 0;
            var totalAssets = 0;
            var commonCap = 0;
            var totalRisk = 0;
            var totCap = 0;

            //only for credit unions
            var netWorth = 0, totAssetsNetWorthRatio = 0;

            for (var i = 0; i < self.eve().capitalPolicies().length; i++) {
                var cp = self.eve().capitalPolicies()[i];
                if (cp.shortName() == 'tier1CapitalBal') {
                    t1Cap = cp.currentRatio();
                }

                if (cp.shortName() == 'averageAssets') {
                    totalAssets = cp.currentRatio();
                }

                if (cp.shortName() == 'commonCap') {
                    commonCap = cp.currentRatio();
                }

                if (cp.shortName() == 'totalRisk') {
                    totalRisk = cp.currentRatio();
                }

                if (cp.shortName() == 'totalCapital') {
                    totCap = cp.currentRatio();
                }

                if (cp.shortName() == 'netWorth') {
                    netWorth = cp.currentRatio();
                }

                if (cp.shortName() == 'totAssetsNetWorthRatio') {
                    totAssetsNetWorthRatio = cp.currentRatio();
                }
            }

            for (var i = 0; i < self.eve().capitalPolicies().length; i++) {
                var cp = self.eve().capitalPolicies()[i];

                if (cp.shortName() == 'tier1Leverage') {
                    if (totalAssets != 0) {
                        cp.currentRatio(t1Cap / totalAssets);
                    } else {
                        cp.currentRatio(0);
                    }
                    continue;
                }

                if (cp.shortName() == 'tier1Common') {
                    if (totalRisk != 0) {
                        cp.currentRatio(commonCap / totalRisk);
                    } else {
                        cp.currentRatio(0);
                    }
                    continue;
                }

                if (cp.shortName() == 'tier1Capital') {
                    if (totalRisk != 0) {
                        cp.currentRatio(t1Cap / totalRisk);
                    } else {
                        cp.currentRatio(0);
                    }
                    continue;
                }

                if (cp.shortName() == 'totalCap') {
                    if (totalRisk != 0) {
                        cp.currentRatio(totCap / totalRisk);
                    } else {
                        cp.currentRatio(0);
                    }
                    continue;
                }

                //only for credit unions
                if (cp.shortName() == "netWorthRatio") {
                    if (totAssetsNetWorthRatio != 0) {
                        cp.currentRatio(netWorth / totAssetsNetWorthRatio);
                    } else {
                        cp.currentRatio(0);
                    }
                }
            }
        };

        this.refresh = function () {
           // self.save();
            globalContext.getHistoricPolicies(self.eve().id(), self.historicEves).then(function () {
                self.recalcHistory();
            });
            //router.navigate("#/policyconfig/");
        }

        this.recalcHistory = function () {
            //Add transient deltaName entries for tab names
            self.eve().nIIPolicyParents().forEach(function (row) {
                row.deltaName = ko.observable(row.name().replace(/change/i, "Δ"));
            });
            self.eve().evePolicyParents().forEach(function (row) {
                row.deltaName = ko.observable(row.name().replace(/change/i, "Δ"));
            });

            self.sortedLiqPolicies(self.eve().liquidityPolicies().sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            }));

            //Build this hash table to refrence them all becuase order was wrong
            for (var a = 0; a < self.historicEves().length; a++) {
                var repScen = 'N/A';

                for (var z = 0; z < self.reportedScenarioOptions().length; z++) {
                    if (self.reportedScenarioOptions()[z].id() == self.historicEves()[a].reportedEveScenarioId()) {
                        repScen = self.reportedScenarioOptions()[z].name();
                    }
                }

                function findRiskAssesmentLabel(val) {
                    var retVals = self.riskAssesmentOptions().filter(function (element) {
                        return element.value == val;
                    });

                    if (retVals.length > 0) {
                        return retVals[0].name;
                    }
                    else {
                        return "N/A";
                    }
                }

                //populate the values for general risk assesment and use
                self.historicHash[a] = {
                    liquidity: {},
                    nii: {},
                    coreFund: {},
                    eve: {},
                    capital: {},
                    other: {},
                    asOfDate: self.historicEves()[a].asOfDate(),
                    liquidityRiskAssessment: findRiskAssesmentLabel(self.historicEves()[a].liquidityRiskAssessment()),
                    nIIRiskAssessment: findRiskAssesmentLabel(self.historicEves()[a].nIIRiskAssessment()),
                    capitalRiskAssessment: findRiskAssesmentLabel(self.historicEves()[a].capitalRiskAssessment()),
                    reportedScenario: repScen
                }

                //Liquidy Hash
                for (var l = 0; l < self.historicEves()[a].liquidityPolicies().length; l++) {
                    self.historicHash[a].liquidity[self.historicEves()[a].liquidityPolicies()[l].name()] = {
                        currentRatio: self.historicEves()[a].liquidityPolicies()[l].currentRatio(),
                        riskAssesment: self.historicEves()[a].liquidityPolicies()[l].riskAssessment(),
                    }
                }

                //Nii Hash
                for (var np = 0; np < self.historicEves()[a].nIIPolicyParents().length; np++) {
                    self.historicHash[a].nii[self.historicEves()[a].nIIPolicyParents()[np].name()] = {
                        currentRatio: self.historicEves()[a].nIIPolicyParents()[np].currentRatio(),
                        riskAssesment: self.historicEves()[a].nIIPolicyParents()[np].riskAssessment(),
                        niis: {},
                    }

                    for (var n = 0; n < self.historicEves()[a].nIIPolicyParents()[np].nIIPolicies().length; n++) {
                        self.historicHash[a].nii[self.historicEves()[a].nIIPolicyParents()[np].name()].niis[self.historicEves()[a].nIIPolicyParents()[np].nIIPolicies()[n].scenarioType().name()] = {
                            currentRatio: self.historicEves()[a].nIIPolicyParents()[np].nIIPolicies()[n].currentRatio(),
                            riskAssesment: self.historicEves()[a].nIIPolicyParents()[np].nIIPolicies()[n].riskAssessment()
                        }
                    }

                    //Add Dummy Scenarios 
                    ////for (var z = 0; z < self.eve().nIIPolicyParents()[0].nIIPolicies().length; z++) {
                    ////    if (self.historicHash[a].nii[self.historicEves()[a].nIIPolicyParents()[np].name()].niis[self.eve().nIIPolicyParents()[0].nIIPolicies()[z].scenarioType().name()] == undefined) {
                    ////        self.historicHash[a].nii[self.historicEves()[a].nIIPolicyParents()[np].name()].niis[self.eve().nIIPolicyParents()[0].nIIPolicies()[z].scenarioType().name()] = {
                    ////            currentRatio: 0,
                    ////            riskAssesment: 0
                    ////        }
                    ////    }
                    ////}
                }

                //Core Fund
                for (var cp = 0; cp < self.historicEves()[a].coreFundParents().length; cp++) {
                    self.historicHash[a].coreFund[self.historicEves()[a].coreFundParents()[cp].name()] = {
                        currentRatio: self.historicEves()[a].coreFundParents()[cp].currentRatio(),
                        policyLimit: self.historicEves()[a].coreFundParents()[cp].policyLimit(),
                        cfs: {},
                    }

                    for (var c = 0; c < self.historicEves()[a].coreFundParents()[cp].coreFundPolicies().length; c++) {
                        self.historicHash[a].coreFund[self.historicEves()[a].coreFundParents()[cp].name()].cfs[self.historicEves()[a].coreFundParents()[cp].coreFundPolicies()[c].scenarioType().name()] = {
                            currentRatio: self.historicEves()[a].coreFundParents()[cp].coreFundPolicies()[c].currentRatio(),
                            policyLimit: self.historicEves()[a].coreFundParents()[cp].coreFundPolicies()[c].policyLimit(),
                            riskAssesment: self.historicEves()[a].coreFundParents()[cp].coreFundPolicies()[c].riskAssessment()
                        }
                    }
                }

                //Eve
                for (var ep = 0; ep < self.historicEves()[a].evePolicyParents().length; ep++) {
                    self.historicHash[a].eve[self.historicEves()[a].evePolicyParents()[ep].name()] = {
                        currentRatio: self.historicEves()[a].evePolicyParents()[ep].currentRatio(),
                        riskAssesment: self.historicEves()[a].evePolicyParents()[ep].riskAssessment(),
                        eves: {},
                    }

                    for (var c = 0; c < self.historicEves()[a].evePolicyParents()[ep].evePolicies().length; c++) {
                        self.historicHash[a].eve[self.historicEves()[a].evePolicyParents()[ep].name()].eves[self.historicEves()[a].evePolicyParents()[ep].evePolicies()[c].scenarioType().name()] = {
                            currentRatio: self.historicEves()[a].evePolicyParents()[ep].evePolicies()[c].currentRatio(),
                            riskAssesment: self.historicEves()[a].evePolicyParents()[ep].evePolicies()[c].riskAssessment()
                        }
                    }
                }

                //Capital
                for (var cap = 0; cap < self.historicEves()[a].capitalPolicies().length; cap++) {
                    self.historicHash[a].capital[self.historicEves()[a].capitalPolicies()[cap].name()] = {
                        currentRatio: self.historicEves()[a].capitalPolicies()[cap].currentRatio(),
                        riskAssesment: self.historicEves()[a].capitalPolicies()[cap].riskAssessment(),
                    }
                }

                //Other
                for (var o = 0; o < self.historicEves()[a].otherPolicies().length; o++) {
                    //self.historicHash[a].other[self.historicEves()[0].otherPolicies()[o].shortName()] = {
                    self.historicHash[a].other[self.historicEves()[a].otherPolicies()[o].shortName()] = {
                        currentRatio: self.historicEves()[a].otherPolicies()[o].currentRatio(),
                        riskAssesment: self.historicEves()[a].otherPolicies()[o].riskAssessment(),
                    }
                }
            }
        }

        this.historicHash = [];

        this.compositionComplete = function () {

            if (self.basicSurplusAdmins().length == 0) {
                globalContext.reportErrors([], ["You must have a Basic Surplus set up before saving Policies."]);
                globalContext.cancelChanges();
                router.navigate("#/");
            } else {
                globalContext.saveChangesQuiet();
            }

            self.addNavEvents();
        }

        this.addNavEvents = function () {
            //Navigation Events
            $("a[href='#niiAtRisk']").on('show.bs.tab', function (e) {
                self.goToTab('#niiPolicy0');
                console.log('Go To Tab');
            });
            $("a[href='#evenev']").on('show.bs.tab', function (e) {
                self.goToTab('#evePolicy-1');
            });
        }

        //Hanldes All Adding of Custom Policies for Other, Liquidity, and Capital

        this.addOtherPolicy = function () {
            var newPol = globalContext.createOtherPolicy();
            var sorted = self.eve().otherPolicies().sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            newPol.formatType('%');
            newPol.priority(sorted[sorted.length - 1].priority() + 1);
            newPol.policyLimit(-999);
            newPol.riskAssessment(1);
            newPol.currentRatio(-999);
            newPol.name('');
            newPol.shortName('custom' + newPol.priority());
            self.eve().otherPolicies().push(newPol);
        }

        this.addCapitalPolicy = function () {
            var newPol = globalContext.createCapitalPolicy();
            var sorted = self.eve().capitalPolicies().sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            newPol.formatType('%');
            newPol.priority(sorted[sorted.length - 1].priority() + 1);
            newPol.policyLimit(-999);
            newPol.riskAssessment(1);
            newPol.wellCap(-999);
            newPol.currentRatio(0);
            newPol.name('');
            newPol.shortName('custom' + newPol.priority());
            self.eve().capitalPolicies().push(newPol);
        }

        this.updateFormatType = function (data, sym) {
            sym.formatType(data);
        };

        this.addLiquidityPolicy = function (tp, ty) {
            var newPol = globalContext.createLiquidityPolicy();
            var shortPrefix = '';
            switch (tp) {
                case 1:
                    shortPrefix = 'bsmin';
                    break;
                case 2:
                    shortPrefix = 'borrow';
                    break;
                case 3:
                    shortPrefix = 'brokered';
                    break;
                case 4:
                    shortPrefix = 'whole';
                    break;
                case 5:
                    shortPrefix = 'fhlb';
                    break;
            }

            var filtered = self.eve().liquidityPolicies().filter(function (lp) { return lp.shortName().indexOf(shortPrefix) >= 0 });

            var sorted = filtered.sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            });
            newPol.formatType('%');
            newPol.priority(sorted[sorted.length - 1].priority() + 1);
            newPol.policyLimit(-999);
            newPol.riskAssessment(1);
            newPol.currentRatio(-999);
            newPol.wellCap(-999);
            newPol.name('');
            newPol.shortName('custom' + shortPrefix + newPol.priority());

            var adjustPrior = ko.observableArray();
            adjustPrior(self.eve().liquidityPolicies().filter(function (lp) { return lp.priority() >= sorted[sorted.length - 1].priority() + 1 }));

            self.eve().liquidityPolicies().forEach(function (entry) {
                if (entry.priority() >= sorted[sorted.length - 1].priority() + 1) {
                    entry.priority(entry.priority() + 1);
                }
            });

            self.eve().liquidityPolicies().push(newPol);

            self.sortedLiqPolicies(self.eve().liquidityPolicies().sort(function (a, b) {
                if (a.priority() === b.priority()) return 0;
                return a.priority() > b.priority() ? 1 : -1;
            }));
        }

        this.removePolicy = function (policy) {
            var msg = 'Do you want to remove this policy?';
            return app.showMessage(msg, 'Navigate Away', ['No', 'Yes'])
                .then(function (selectedOption) {
                    if (selectedOption === 'Yes') {
                        policy.entityAspect.setDeleted();

                        self.sortedLiqPolicies(self.eve().liquidityPolicies().sort(function (a, b) {
                            if (a.priority() === b.priority()) return 0;
                            return a.priority() > b.priority() ? 1 : -1;
                        }));
                    }
                    return selectedOption;
                });
        };

        this.changedSim = false;
        this.refreshData = function () {
            return Q.all([
                globalContext.getUserProfile(self.profile),
                globalContext.getEveScenarioTypes(self.reportedScenarioOptions),
                globalContext.getPolicy(self.eve),
                globalContext.getAvailableSimulations(self.simulationTypes, "", 0),        
                globalContext.getBasicSurplusAdmins(self.basicSurplusAdmins)
            ]).fin(function () {
                self.addNavEvents();

                self.policyChange = self.eve().nIISimulationTypeId.subscribe(function (newValue) {
                    self.changedSim = true;
                });

                self.eveChange = self.eve().eveSimulationTypeId.subscribe(function (newValue) {
                    self.changedSim = true;
                });

                self.bsChange = self.eve().basicSurplusAdminId.subscribe(function (newValue) {
                    self.changedSim = true;
                });

                return globalContext.getHistoricPolicies(self.eve().id(), self.historicEves).then(function () {
                    self.recalcHistory();
                });

            });
        }

        //Data Handling
        this.activate = function () {
            return globalContext.getModelSetup(self.modelSetup).then(function () {
                //Get Eve Config
                return self.refreshData();
            });
        };

        this.deactivate = async function () {
            await globalContext.clearCache();
        };

        this.detached = async function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.policyChange.dispose();

            self.eveChange.dispose();

            self.bsChange.dispose();
        };

        return this;
    };

    return vm;
});
