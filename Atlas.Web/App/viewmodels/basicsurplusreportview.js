﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/app',
    'services/basicsurplusservice',
    'services/charting',
    'services/global'
],
    function (globalContext, router, app, bService, charting, global) {
    var vm = function (id) {
        var self = this;
        var curPage = "BasicSurplusReportView";
        this.basicSurplusReportViewData = ko.observable();
        this.bService = bService;


        this.buildBlockHeader = function (title, body, tierLevel) {
            //tier level is used to get associated color
            var color = charting.chartColorsPalette2[tierLevel - 1];
            //Add header for tieq 1 basic suplus box

            var fontColor = '#fff';
            if (color == 'rgb(217,217,  0)') {
                fontColor = "#000";
            }
            tier1Body
            $(body).append('<tr><td colspan="7" style="background-color: ' + color + '; color: ' + fontColor + ' " class="tierHeader tierBorder borderLeft borderRight">' + title + '</td><td style="vertical-align: top; padding:0px; width: 130px;" id="' + body.replace("#", "") + 'RowSpanTop"></td></tr>');
        }

        this.buildLeftSpacerRow = function (body) {
            $(body).append("<tr><td colspan='7' class='spacerRow tierBorder borderLeft borderRight'></td></tr>");
        }

        this.addRowsOfItems = function(arr, body, checkInclude){
            for (var i = 0; i < arr.length; i++) {
                var rec = arr[i];

                //below is a hack to arress for one section that only one option has the include
                if (rec.include != undefined) {
                    if (rec.include() == false && (checkInclude || rec.fieldName() == "leftS1LiqAssetsGovLoans")) {
                        continue;
                    }
                }
                
                self.addSingleRow(rec.title(), (rec.balance == undefined ? rec.amount() : rec.balance()), "detailRow", body, "");

            }
            //returns the array length to properly update the big rowspan cell i need to create
            return arr.length;
        }

        this.addSingleRow = function(title, val, rowClass, body, overrideValue){
            var row = '<tr class="' + rowClass + '">';

            
            row += '<td colspan="5" class="labelsRightLeftTable tierBorder borderLeft ' + (rowClass == 'totalRow' ? 'boldCell' : '') + '">' + title + '</td>';
            if (val === "" || overrideValue != "") {
                row += '<td>' + overrideValue + '</td>';
            }
            else {
                if (val == 0) {
                    row += '<td class="extraPadLeft">' + 0 + '</td>';
                }
                else {
                    row += '<td class="extraPadLeft">' + numeral(val).format('(0,0)') + '</td>';
                }
                
            }
            row += '<td class="tierBorder borderRight"></td>';
            row += '</tr>';
            //Add row
            $(body).append(row);
        }

        this.buildFooterRow = function (title, amt, perc, tierLevel, tierBody) {

            //tier level is used to denote border color and shade of percenate cell
            var color = charting.chartColorsPalette2[tierLevel - 1];

            var fontColor = '#fff';
            if (color == 'rgb(217,217,  0)') {
                fontColor = "#000";
            }

            var row = '<tr class="footerTierRow">';
            row += '<td colspan="3" class="tierBorder borderBottom borderLeft"></td>';
            row += '<td class="tierBorder borderBottom borderLeft borderTop" colspan="2">' + title + '</td>';
            row += '<td class="tierBorder borderBottom borderTop">' + numeral(amt).format('0,0') + '</td>';


            row += '<td class="tierBorder borderBottom borderTop" style="padding: 0px;"><span style="padding-left:10px; padding-right: 10px; background-color: ' + color + '; color: ' + fontColor +';  border: 1px solid '+ color + '; display: block; float:right; width: 90%; ">' + numeral(numeral(perc).divide(100)).format('0.0%') + '</span></td>';
            row += '</tr>';
            $(tierBody).append(row);
        }


        this.buildRightSpacerRow = function (body, withBorder) {
            if (withBorder) {
                $(body).append("<tr><td colspan='7' class='spacerRow borderLeft borderThick borderRight borderGrey'></td></tr>");
            }
            else {
                $(body).append("<tr><td colspan='7' class='spacerRow'></td></tr>");
            }
            
        }


        this.getPolicyValues = function (polShortName) {
            var idx = self.fundingCapacity().polValues.findIndex(function (el) { return el.shortName == polShortName });

            var pols = {
                wellCap: '',
                polLimit: ''
            }


            if (idx >= 0) {    
                if (numeral(self.fundingCapacity().polValues[idx].policyLimit).value() != -999) {
                    pols.polLimit = numeral(self.fundingCapacity().polValues[idx].policyLimit).format('0.0') + '%';
                }

                if (numeral(self.fundingCapacity().polValues[idx].wellCap).value() != -999) {
                    pols.wellCap = numeral(self.fundingCapacity().polValues[idx].wellCap).format('0.0') + '%';
                }
            }

            return pols;

        }


        this.compositionComplete = function () {
            //Render report YAY!!!! 

            //rowspan keeper trackerofs
            var tier1RowSpan = 0;
            var tier2RowSpan = 0;
            var tier3RowSpan = 0;

            //Add header for tier 1 basic suplus box
            self.buildBlockHeader(self.bService.thisType().leftS1HeaderTitle(), '#tier1Body', 1)
            tier1RowSpan += 1;

            //First add spacer row to tier 1 body
            self.buildLeftSpacerRow('#tier1Body');
            tier1RowSpan += 1;

            //Next loop through Overnight funds
            tier1RowSpan += self.addRowsOfItems(self.bService.sortedLeftS1OvntFunds(), '#tier1Body', true);

            //After doing overnights add another spacer row
            self.buildLeftSpacerRow('#tier1Body');
            tier1RowSpan += 1;

            //Build collateral value headers
            var collatHeadRow = '<tr>';
            collatHeadRow += '<td class="tierBorder borderLeft emptySpacingCell">';
            collatHeadRow += '<td class=""</td>';
            collatHeadRow += '<td class="collatHeadLabel">' + self.bService.thisType().collateralTypesHTMLHashHack.treasuryAgencyBonds.name() + '</td>';
            collatHeadRow += '<td class="collatHeadLabel">' + self.bService.thisType().collateralTypesHTMLHashHack.mBSCMOAgency.name() + '</td>';
            collatHeadRow += '<td class="collatHeadLabel">' + self.bService.thisType().collateralTypesHTMLHashHack.mBSCMOPvt.name() + '</td>';
            collatHeadRow += '<td class=""></td>';
            collatHeadRow += '<td class="tierBorder borderRight"></td>';
            collatHeadRow += '</tr>';
            $('#tier1Body').append(collatHeadRow);
            tier1RowSpan += 1;


            //Buld out collaterval value percetnages and market values
            var collatPercRow = '<tr>';
            collatPercRow += '<td class="tierBorder borderLeft emptySpacingCell">';
            collatPercRow += '<td class="labelsRightLeftTable boldCell">Collateral Value</td>';   
            collatPercRow += '<td class="">' + numeral(numeral(self.bService.thisType().collateralTypesHTMLHashHack.treasuryAgencyBonds.collateralValue()).divide(100)).format('0%') + '</td>';
            collatPercRow += '<td class="">' + numeral(numeral(self.bService.thisType().collateralTypesHTMLHashHack.mBSCMOAgency.collateralValue()).divide(100)).format('0%') + '</td>';
            collatPercRow += '<td class="">' + numeral(numeral(self.bService.thisType().collateralTypesHTMLHashHack.mBSCMOPvt.collateralValue()).divide(100)).format('0%') + '</td>';
            collatPercRow += '<td class=""></td>';
            collatPercRow += '<td class="tierBorder borderRight"></td>';
            collatPercRow += '</tr>';
            $('#tier1Body').append(collatPercRow);
            tier1RowSpan += 1;

            var collatAmtRow = '<tr>';
            collatAmtRow += '<td class="tierBorder borderLeft emptySpacingCell">';
            collatAmtRow += '<td class="labelsRightLeftTable boldCell">' + self.bService.thisType().leftS1SecColTotalSecMktValTitle() + '</td>';
            collatAmtRow += '<td class="">' + numeral(self.bService.thisType().leftS1SecColTotalSecMktValUSTAgency()).format('0,0') + '</td>';
            collatAmtRow += '<td class="">' + numeral(self.bService.thisType().leftS1SecColTotalSecMktValMBSAgency()).format('0,0') + '</td>';
            collatAmtRow += '<td class="">' + numeral(self.bService.thisType().leftS1SecColTotalSecMktValMBSPvt()).format('0,0') + '</td>';
            collatAmtRow += '<td class=""></td>';
            collatAmtRow += '<td class="tierBorder borderRight"></td>';
            collatAmtRow += '</tr>';
            $('#tier1Body').append(collatAmtRow);
            tier1RowSpan += 1;

            //Adding spacer row
            self.buildLeftSpacerRow('#tier1Body');
            tier1RowSpan += 1;
            //Adding header for Less securites pledged too title
            
            var pledgeTitle = '<tr>';
            pledgeTitle += '<td class="tierBorder borderLeft emptySpacingCell">';
            pledgeTitle += '<td class="labelsRightLeftTable boldCell italicCell">' + self.bService.thisType().leftS1SecColLessSecPledgedTitle() + '</td>'
            pledgeTitle += '<td class=""></td>';
            pledgeTitle += '<td class=""></td>';
            pledgeTitle += '<td class=""></td>';
            pledgeTitle += '<td class=""></td>';
            pledgeTitle += '<td class="tierBorder borderRight"></td>';
            pledgeTitle += '</tr>';
            $('#tier1Body').append(pledgeTitle);
            tier1RowSpan += 1;
            //Now loop through pledges and spit them out in order

            for (var i = 0; i < self.bService.sortedLeftS1SecCols().length; i++) {
                var rec = self.bService.sortedLeftS1SecCols()[i];
                var row = '<tr>';
                row += '<td class="emptySpacingCell tierBorder borderLeft"</td>';
                row += '<td class="labelsRightLeftTable">' + rec.title() + '</td>';
                row += '<td >' + numeral(rec.uSTAgency()).format('0,0') + '</td>';
                row += '<td >' + numeral(rec.mBSAgency()).format('0,0') + '</td>';
                row += '<td >' + numeral(rec.mBSPvt()).format('0,0') + '</td>';
                row += '<td class=""></td>';
                row += '<td class="tierBorder borderRight"></td>';
                row += '</tr>';
                //Add row
                $('#tier1Body').append(row);
            }
            tier1RowSpan += self.bService.sortedLeftS1SecCols().length;
            //Add Avbvailanle unecumbered total row

            var availUncumRow = '<tr>';
            availUncumRow += '<td class="emptySpacingCell tierBorder borderLeft"</td>';
            availUncumRow += '<td class="labelsRightLeftTable borderTop boldCell">' + self.bService.thisType().leftS1SecColTotalSecColTitle() + '</td>';
            availUncumRow += '<td class="borderGreyTop">' + numeral(self.bService.thisType().leftS1SecColTotalSecColUSTAgency()).format('0,0') + '</td>';
            availUncumRow += '<td class="borderGreyTop">' + numeral(self.bService.thisType().leftS1SecColTotalSecColMBSAgency()).format('0,0') + '</td>';
            availUncumRow += '<td class="borderGreyTop">' + numeral(self.bService.thisType().leftS1SecColTotalSecColMBSPvt()).format('0,0') + '</td>';
            availUncumRow += '<td class="extraPadLeft">' + numeral(self.bService.thisType().leftS1BlankTotalAmount()).format('0,0')  + '</td>';
            availUncumRow += '<td class="tierBorder borderRight"></td>';
            availUncumRow += '</tr>';
            $('#tier1Body').append(availUncumRow);
            tier1RowSpan += 1;
            //Add spacer row
            self.buildLeftSpacerRow('#tier1Body');
            tier1RowSpan += 1;
            //loop through  liquid assets and dump them out

            console.log(self.bService.sortedLeftS1LiqAssets());
            tier1RowSpan += self.addRowsOfItems(self.bService.sortedLeftS1LiqAssets(), '#tier1Body', false);

            //Do total liquid assestr rows
            self.addSingleRow(self.bService.thisType().leftS1LiqAssetTotalTitle(), self.bService.thisType().leftS1LiqAssetTotalAmount(), "totalRow", '#tier1Body', "");
            tier1RowSpan += 1;
            //Add TWO COUNT EM TWO spacer rows
            self.buildLeftSpacerRow('#tier1Body');
            self.buildLeftSpacerRow('#tier1Body');
            tier1RowSpan += 2;

            //Now do volatile liablies section row
            self.addSingleRow(self.bService.thisType().leftS2HeaderTitle(), "", "totalRow", '#tier1Body', "");
            tier1RowSpan += 1;
            //Mat unsecured
            self.addSingleRow(self.bService.thisType().leftS2MatUnsecLiabTitle(), self.bService.thisType().leftS2MatUnsecLiabAmount() * -1, "", '#tier1Body', "");
            tier1RowSpan += 1;
            //Dep coverage
            self.addSingleRow(self.bService.thisType().leftS2TotalDepTitle(), numeral(self.bService.thisType().leftS2TotalDepAmount()).value() * -1, "", '#tier1Body', "");
            tier1RowSpan += 1;
            //Now do all customs ones before doing the other row
            tier1RowSpan += self.addRowsOfItems(self.bService.sortedLeftS2VolLiabs(), '#tier1Body', true);

            //Other Coverage
            self.addSingleRow(self.bService.thisType().leftS2AddLiqResTitle(), self.bService.thisType().leftS2AddLiqResAmount() * -1, "", '#tier1Body', "");
            tier1RowSpan += 1;
            //Add TWO COUNT EM TWO spacer rows
            self.buildLeftSpacerRow('#tier1Body');
            self.buildLeftSpacerRow('#tier1Body');
            tier1RowSpan += 2;

            self.buildFooterRow(self.bService.thisType().leftS2SubHeaderTitle(), self.bService.thisType().midS1BasicSurplusAmount(), self.bService.thisType().midS1BasicSurplusPercent(), 1, '#tier1Body')
            tier1RowSpan += 1;

            //Now set thhe row span for the weird policy and shade gradient table

            //Hacky joys due to layout
            //take total rows divide by two
            var tier1topRows = parseInt(tier1RowSpan / 2);
            var tier1BottomRows = tier1RowSpan - tier1topRows;

            $('#tier1BodyRowSpanTop').attr("rowspan", tier1topRows);

            //Take that tier1 top rows and find the row after it
            if (self.basicSurplusReportViewData().includePolicies()) {
                //Take that tier1 top rows and find the row after it
                $('#tier1Body tr').eq(tier1topRows).append('<td id="tier1BodyRowSpanBottom" class="tierBorder borderBottom" style="vertical-align: bottom; padding:0px;" rowspan="' + tier1BottomRows + '"></td>') //0 indexed so all good
            }
            else {
                //Take that tier1 top rows and find the row after it
                $('#tier1Body tr').eq(tier1topRows).append('<td id="tier1BodyRowSpanBottom" class="" style="vertical-align: bottom; padding:0px;" rowspan="' + tier1BottomRows + '"></td>') //0 indexed so all good
            }

            var totAssets = self.bService.sortedMidS1Miscs().filter(function (val) {
                return val.fieldName() == 'midS1MiscsTotalAsset';
            });

            var formAmount = numeral(totAssets[0].amount()).format('0,0');
            var innerHTML = '<table class="gradientNoise" style="text-align: center; padding-left:10px; font-size: 12px;"><tr><td> <b>TOTAL ASSETS</b> </td></tr><tr><td>' + formAmount + '</td></tr> </table>'

          
            $('#tier1BodyRowSpanTop').append(innerHTML);
            var tier1Pols = self.getPolicyValues('bsmin')
            innerHTML = '<table class="gradientNoise" style="text-align: center; padding-left:10px;">';
            innerHTML += '<tr><td colspan="2" class="boldCell" style="font-size: 12px">POLICY LIMITS (min)</td></tr>';
            innerHTML += '<tr>';
            innerHTML += '<td class="boldCell">Well Cap.</td>';
            innerHTML += '<td class="boldCell">&lt;Well Cap.</td>';
            innerHTML += '</tr>';
            innerHTML += '<tr style="font-size: 12px; height: 19px;">'
            innerHTML += '<td class="boldCell">' + tier1Pols.polLimit + '</td>';
            innerHTML += '<td class="boldCell"  style="width: 70px;">' + tier1Pols.wellCap + '</td>';
            innerHTML += '<tr>'

            if (self.basicSurplusReportViewData().includePolicies()) {
                $('#tier1BodyRowSpanBottom').append(innerHTML);
            }
        

            //Tier 2
            //Add header for tieq 1 basic suplus box
            self.buildBlockHeader(self.bService.thisType().leftS3HeaderTitle(), '#tier2Body', 2)
            tier2RowSpan += 1;
            self.buildLeftSpacerRow('#tier2Body');
            tier2RowSpan += 1;

            //Maxium borrowing line
            self.addSingleRow(self.bService.thisType().leftS3MaxBorLineFHLBTitle(), self.bService.thisType().leftS3MaxBorLineFHLBAmount(), "", '#tier2Body', "");
            tier2RowSpan += 1;

            //Loan Collateral FHLB
            self.addSingleRow(self.bService.thisType().leftS3LoanColFHLBTitle(), self.bService.thisType().leftS3LoanColFHLBAmount(), "", '#tier2Body', "");
            tier2RowSpan += 1;

            //Excess Loan Collateral
            self.addSingleRow(self.bService.thisType().leftS3ExcessLoanColTitle(), self.bService.thisType().leftS3ExcessLoanColAmount(), "", '#tier2Body', "");
            tier2RowSpan += 1;

            self.buildLeftSpacerRow('#tier2Body');
            self.buildLeftSpacerRow('#tier2Body');
            tier2RowSpan += 2;

            //LEser of A Or B
            self.addSingleRow(self.bService.thisType().leftS3MaxBorCapTitle(), self.bService.thisType().leftS3MaxBorCapAmount(), "", '#tier2Body', "");
            tier2RowSpan += 1;

            //Collateral Currently Encumber
            self.addSingleRow(self.bService.thisType().leftS3ColEncTitle(), self.bService.thisType().leftS3ColEncAmount(), "", '#tier2Body', "");
            tier2RowSpan += 1;

            //Custom ones right herrr
            tier2RowSpan += self.addRowsOfItems(self.bService.sortedLeftS3Loans(), '#tier2Body', true);

            //Remaining FHLB STUFF
            self.addSingleRow(self.bService.thisType().leftS3LoanTotalTitle(), self.bService.thisType().leftS3LoanTotalAmount(), "", '#tier2Body', "");
            tier2RowSpan += 1;

            self.buildLeftSpacerRow('#tier2Body');
            self.buildLeftSpacerRow('#tier2Body');
            tier2RowSpan += 2;

            self.buildFooterRow(self.bService.thisType().leftS3SubHeaderTitle(), self.bService.thisType().midS1BasicSurplusFHLBAmount(), self.bService.thisType().midS1BasicSurplusFHLBPercent(), 2, '#tier2Body')
            tier2RowSpan += 1;



            var tier2topRows = parseInt(tier2RowSpan / 2);
            var tier2BottomRows = tier2RowSpan - tier2topRows;

            $('#tier2BodyRowSpanTop').attr("rowspan", tier2topRows);


            if (self.basicSurplusReportViewData().includePolicies()) {
                //Take that tier1 top rows and find the row after it
                $('#tier2Body tr').eq(tier2topRows).append('<td id="tier2BodyRowSpanBottom" class="tierBorder borderBottom" style="vertical-align: bottom; padding:0px;" rowspan="' + tier2BottomRows + '"></td>') //0 indexed so all good
            }
            else {
                //Take that tier1 top rows and find the row after it
                $('#tier2Body tr').eq(tier2topRows).append('<td id="tier2BodyRowSpanBottom" class="" style="vertical-align: bottom; padding:0px;" rowspan="' + tier2BottomRows + '"></td>') //0 indexed so all good
            }

            //Take that tier1 top rows and find the row after it
           

            //Now set thhe row span for the weird policy and shade gradient table
            $('#tier2BodyRowSpan').attr("rowspan", tier2RowSpan);
            var tier2Pols = self.getPolicyValues('bsminFHLB');

            innerHTML = '<table class="gradientNoise" style="text-align: center; padding-left:10px;">';
            innerHTML += '<tr style="font-size: 12px">'
            innerHTML += '<td class="boldCell">' + tier2Pols.polLimit + '</td>';
            innerHTML += '<td class="boldCell" style="width: 70px;">' + tier2Pols.wellCap + '</td>';
            innerHTML += '<tr>'


            if (self.basicSurplusReportViewData().includePolicies()) {
                $('#tier2BodyRowSpanBottom').append(innerHTML);
            }

            //Tier 3
            //Add header for tieq 1 basic suplus box
            self.buildBlockHeader(self.bService.thisType().leftS4HeaderTitle(), '#tier3Body', 3)
            tier3RowSpan += 1;

            self.buildLeftSpacerRow('#tier3Body');
            tier3RowSpan += 1;

            //Breaking out logic here becuase its dumb

              //Maximum Borad Authroriexe
            if (self.bService.thisType().leftS4BrokDepType() == 'None') {
                self.addSingleRow(self.bService.thisType().leftS4BrokDepCapTitle(), self.bService.thisType().leftS4BrokDepCapAmount(), "", '#tier3Body', "N/A");
            }
            else {
                if (self.bService.thisType().leftS4BrokDepCapAmount() == 0) {
                    self.addSingleRow(self.bService.thisType().leftS4BrokDepCapTitle(), self.bService.thisType().leftS4BrokDepCapAmount(), "", '#tier3Body', "0");
                }
                else {
                    self.addSingleRow(self.bService.thisType().leftS4BrokDepCapTitle(), self.bService.thisType().leftS4BrokDepCapAmount(), "", '#tier3Body', "");
                }

                
            }
            tier3RowSpan += 1;


             //Current Brokered Deposit Balances
            if (self.bService.thisType().leftS4BrokDepBalAmount() == 0) {
                self.addSingleRow(self.bService.thisType().leftS4BrokDepBalTitle(), self.bService.thisType().leftS4BrokDepBalAmount(), "", '#tier3Body', "0");
            }
            else {
                self.addSingleRow(self.bService.thisType().leftS4BrokDepBalTitle(), self.bService.thisType().leftS4BrokDepBalAmount(), "", '#tier3Body', "");
            }
            tier3RowSpan += 1;
         

            //Custom ones right herrr
            tier3RowSpan += self.addRowsOfItems(self.bService.sortedLeftS4BrokDeps(), '#tier3Body', true);
            
            //Remaining Capactity
            if (self.bService.thisType().leftS4BrokDepTotalAmount() == 0){
                self.addSingleRow(self.bService.thisType().leftS4BrokDepTotalTitle(), self.bService.thisType().leftS4BrokDepTotalAmount(), "", '#tier3Body', "0")
            }
            else {
                self.addSingleRow(self.bService.thisType().leftS4BrokDepTotalTitle(), self.bService.thisType().leftS4BrokDepTotalAmount(), "", '#tier3Body', "")
            }
           
            tier3RowSpan += 1;

            self.buildLeftSpacerRow('#tier3Body');
            self.buildLeftSpacerRow('#tier3Body');
            tier3RowSpan += 2;

            self.buildFooterRow(self.bService.thisType().leftS4SubHeaderTitle(), self.bService.thisType().midS1BasicSurplusBrokAmount(), self.bService.thisType().midS1BasicSurplusBrokPercent(), 3, '#tier3Body')
            tier3RowSpan += 3;

            //Add one beuycase of math??? HACK HACK HACK HACK IF YOU EVER SEE THIS THIS IS HACK HACK HACK ATO-913
            tier3RowSpan += 2;
            var tier3topRows = parseInt(tier3RowSpan / 2);
            var tier3BottomRows = tier3RowSpan - tier3topRows;

            $('#tier3BodyRowSpanTop').attr("rowspan", tier3topRows);


            if (self.basicSurplusReportViewData().includePolicies()) {
                //Take that tier1 top rows and find the row after it
                $('#tier3Body tr').eq(tier3topRows).append('<td id="tier3BodyRowSpanBottom" class="tierBorder borderBottom" style="vertical-align: bottom; padding:0px;" rowspan="' + tier3BottomRows + '"></td>') //0 indexed so all good
            }
            else {
                //Take that tier1 top rows and find the row after it
                $('#tier3Body tr').eq(tier3topRows).append('<td id="tier3BodyRowSpanBottom" class="" style="vertical-align: bottom; padding:0px;" rowspan="' + tier3BottomRows + '"></td>') //0 indexed so all good
            }

            

            //Now set thhe row span for the weird policy and shade gradient table
            $('#tier3BodyRowSpan').attr("rowspan", tier2RowSpan);
            var tier3Pols = self.getPolicyValues('bsminBrokered');
            

            innerHTML = '<table class="gradientNoise" style="text-align: center; padding-left:10px;">';
            innerHTML += '<tr style="font-size: 12px">'
            innerHTML += '<td class="boldCell">' + tier3Pols.polLimit  + '</td>';
            innerHTML += '<td class="boldCell" style="width: 70px;">' + tier3Pols.wellCap + '</td>';
            innerHTML += '<tr>'


            if (self.basicSurplusReportViewData().includePolicies()) {
                $('#tier3BodyRowSpanBottom').append(innerHTML);
            }

            //Onto the Right Table now this one should be a little simpler yet more code
            
            //add apcer to sync up with left table
            self.buildRightSpacerRow('#rightTableBody', false);
            //Start with header
            $('#rightTableBody').append('<tr><td id="otherLiqHeader" class=" borderRight borderBrown"colspan="7">' + self.bService.thisType().rightS1HeaderTitle() + '</td></tr>')

            //Sub head Other investmetns
            $('#rightTableBody').append('<tr><td class="rightSubTableHeader borderLeft borderThick borderRight borderGrey" colspan="7">' + self.bService.thisType().rightS1SubHeaderTitle() + '</td></tr>');

            //Headers of market value and what not
            var mktValueHeadRow = '<tr class="mktValueHeadRow">';
            mktValueHeadRow += '<td class="borderLeft borderThick"></td>';
            mktValueHeadRow += '<td class="borderBottom borderGrey"></td>';
            mktValueHeadRow += '<td class="borderBottom borderGrey">Market Value</td>';
            mktValueHeadRow += '<td class="borderBottom borderGrey">Pledged</td>';
            mktValueHeadRow += '<td class="borderBottom borderGrey">Available</td>';
            mktValueHeadRow += '<td></td>';
            mktValueHeadRow += '<td class="emptySpacingCell borderRight borderGrey"><td>';
            mktValueHeadRow += '<tr>';

            $('#rightTableBody').append(mktValueHeadRow);

            //Actual Market Values now
            for (var i = 0; i < self.bService.sortedRightS1OtherLiqs().length; i++) {
                var rec = self.bService.sortedRightS1OtherLiqs()[i];
                var row = '<tr class="">';
                row += '<td class="borderLeft borderThick"></td>';
                row += '<td>' + rec.title() +  '</td>';
                row += '<td>' + numeral(rec.marketValue()).format('0,0') +  '</td>';
                row += '<td>' + numeral(rec.pledged()).format('0,0') +  '</td>';
                row += '<td>' + numeral(rec.available()).format('0,0') +  '</td>';
                row += '<td></td>';
                row += '<td class="emptySpacingCell borderRight borderGrey"><td>';
                row += '<tr>';
                $('#rightTableBody').append(row);
            }


            //Do totl row now
            var totMktValueRow = '<tr class="">';
            totMktValueRow += '<td class="borderLeft borderThick"></td>';
            totMktValueRow += '<td class="borderTop borderGrey boldCell">' + self.bService.thisType().rightS1OtherLiqTotalTitle() + '</td>';
            totMktValueRow += '<td class="borderTop borderGrey boldCell">' + numeral(self.bService.thisType().rightS1OtherLiqTotalMarketValue()).format('0,0') + '</td>';
            totMktValueRow += '<td class="borderTop borderGrey boldCell">' + numeral(self.bService.thisType().rightS1OtherLiqTotalPledged()).format('0,0') + '</td>';
            totMktValueRow += '<td class="borderTop borderGrey boldCell">' + numeral(self.bService.thisType().rightS1OtherLiqTotalAvailable()).format('0,0') + '</td>';
            totMktValueRow += '<td></td>';
            totMktValueRow += '<td class="emptySpacingCell borderRight borderGrey"><td>';
            totMktValueRow += '<tr>';
            $('#rightTableBody').append(totMktValueRow);

            //add apcer to sync up with left table
            self.buildRightSpacerRow('#rightTableBody', true);

            //Sub head Other investmetns
            $('#rightTableBody').append('<tr><td class="rightSubTableHeader borderLeft borderThick" colspan="6">' + self.bService.thisType().rightS2HeaderTitle() + '</td><td class="emptySpacingCell borderRight borderGrey"><td></tr>');
            self.buildRightSpacerRow('#rightTableBody', true);
           
            //Headers of market value and what not
            var securedHeadRow = '<tr class="mktValueHeadRow">';
            securedHeadRow += '<td class="borderLeft borderThick"></td>';
            securedHeadRow += '<td class="borderBottom borderGrey">' + self.bService.thisType().rightS2SubHeaderTitle() + '</td>';
            securedHeadRow += '<td class="borderBottom borderGrey">Line</td>';
            securedHeadRow += '<td class="borderBottom borderGrey">Outstanding</td>';
            securedHeadRow += '<td class="borderBottom borderGrey">Available</td>';
            securedHeadRow += '<td></td>';
            securedHeadRow += '<td class="emptySpacingCell borderRight borderGrey"><td>';
            securedHeadRow += '<tr>';
            $('#rightTableBody').append(securedHeadRow);


            //Actual Market Values now
            for (var i = 0; i < self.bService.sortedRightS2SecBors().length; i++) {
                var rec = self.bService.sortedRightS2SecBors()[i];
                var row = '<tr class="">';
                row += '<td class="borderLeft borderThick"></td>';
                row += '<td>' + rec.title() + '</td>';
                row += '<td>' + numeral(rec.line()).format('0,0') + '</td>';
                row += '<td>' + numeral(rec.outstanding()).format('0,0') + '</td>';
                row += '<td>' + numeral(rec.available()).format('0,0') + '</td>';
                row += '<td></td>';
                row += '<td class="emptySpacingCell borderRight borderGrey"><td>';
                row += '<tr>';
                $('#rightTableBody').append(row);
            }

            self.buildRightSpacerRow('#rightTableBody', true);

            //Headers of unsecured
            var unsecuredHeadRow = '<tr class="mktValueHeadRow">';
            unsecuredHeadRow += '<td class="borderLeft borderThick"></td>';
            unsecuredHeadRow += '<td class="borderBottom borderGrey">' + self.bService.thisType().rightS3HeaderTitle() + '</td>';
            unsecuredHeadRow += '<td class="borderBottom borderGrey"></td>';
            unsecuredHeadRow += '<td class="borderBottom borderGrey"></td>';
            unsecuredHeadRow += '<td class="borderBottom borderGrey"></td>';
            unsecuredHeadRow += '<td></td>';
            unsecuredHeadRow += '<td class="emptySpacingCell borderRight borderGrey"><td>';
            unsecuredHeadRow += '<tr>';
            $('#rightTableBody').append(unsecuredHeadRow);


            for (var i = 0; i < self.bService.sortedRightS3UnsecBors().length; i++) {
                var rec = self.bService.sortedRightS3UnsecBors()[i];
                var row = '<tr class="">';
                row += '<td class="borderLeft borderThick"></td>';
                row += '<td>' + rec.title() + '</td>';
                row += '<td>' + numeral(rec.line()).format('0,0') + '</td>';
                row += '<td>' + numeral(rec.outstanding()).format('0,0') + '</td>';
                row += '<td>' + numeral(rec.available()).format('0,0') + '</td>';
                row += '<td></td>';
                row += '<td class="emptySpacingCell borderRight borderGrey"><td>';
                row += '<tr>';
                $('#rightTableBody').append(row);
            }

            //tot se4curities
            var totSecurities = '<tr class="">';
            totSecurities += '<td class="borderLeft borderThick"></td>';
            totSecurities += '<td class="borderTop borderGrey boldCell">' + self.bService.thisType().rightS3UnsecBorTotalTitle() + '</td>';
            totSecurities += '<td class="borderTop borderGrey boldCell">' + numeral(self.bService.thisType().rightS3UnsecBorTotalLine()).format('0,0') + '</td>';
            totSecurities += '<td class="borderTop borderGrey boldCell">' + numeral(self.bService.thisType().rightS3UnsecBorTotalOutstanding()).format('0,0') + '</td>';
            totSecurities += '<td class="borderTop borderGrey boldCell">' + numeral(self.bService.thisType().rightS3UnsecBorTotalAvailable()).format('0,0') + '</td>';
            totSecurities += '<td></td>';
            totSecurities += '<td class="emptySpacingCell borderRight borderGrey"><td>';
            totSecurities += '<tr>';
            $('#rightTableBody').append(totSecurities);

            self.buildRightSpacerRow('#rightTableBody', true);

            $('#rightTableBody').append('<tr><td class="rightSubTableHeader borderLeft borderThick" colspan="6">' + self.bService.thisType().rightS4HeaderTitle() + '</td><td class="emptySpacingCell borderRight borderGrey"><td></tr>');
            

            //Headers
            var unRealHead = '<tr class="mktValueHeadRow">'; //haha funny var name didnt realize til after
            unRealHead += '<td class="borderLeft borderThick"></td>';
            unRealHead += '<td class="borderBottom borderGrey"></td>';
            unRealHead += '<td class="borderBottom borderGrey">' + self.bService.thisType().rightS4Column1Title() + '</td>';
            unRealHead += '<td class="borderBottom borderGrey" style="width: 100px;">' + self.bService.thisType().rightS4Column2Title() + '</td>';
            unRealHead += '<td class="borderBottom borderGrey">' + self.bService.thisType().rightS4Column3Title() + '</td>';
            unRealHead += '<td></td>';
            unRealHead += '<td class="emptySpacingCell borderRight borderGrey"><td>';
            unRealHead += '<tr>';

            $('#rightTableBody').append(unRealHead);

            //All unrealize scens and stuff

            for (var i = 0; i < self.bService.sortedRightS4UnrealGL().length; i++) {
                var rec = self.bService.sortedRightS4UnrealGL()[i];
                var row = '<tr class="">';
                row += '<td class="borderLeft borderThick"></td>';
                row += '<td>' + rec.title() + '</td>';
                row += '<td>' + numeral(rec.totalPortfolio()).format('0,0') + '</td>';
                row += '<td>' + numeral(rec.tier1BasicSurplus()).format('0,0') + '</td>';
                row += '<td>' + numeral(rec.percOfAssets() /100).format('0.0%') + '</td>';
                row += '<td></td>';
                row += '<td class="emptySpacingCell borderRight borderGrey"><td>';
                row += '<tr>';
                $('#rightTableBody').append(row);
            }

            self.buildRightSpacerRow('#rightTableBody', true);
            $('#rightTableBody').append('<tr><td class="rightSubTableHeader borderLeft borderThick" colspan="6">' + self.bService.thisType().rightS6HeaderTitle() + '</td><td class="emptySpacingCell borderRight borderGrey"><td></tr>');
            
            self.buildRightSpacerRow('#rightTableBody', true);
            //Footnotes

            for (var i = 0; i < self.bService.sortedRightS6Footnotes().length; i++) {
                var rec = self.bService.sortedRightS6Footnotes()[i];

                var borderbottomClass = '';
                if (i == self.bService.sortedRightS6Footnotes().length - 1) {
                    borderbottomClass = 'borderBottom borderGrey';;
                }

                var row = '<tr class="">';
                row += '<td class="borderLeft borderThick ' + borderbottomClass + '"></td>';
                row += '<td colspan="4" style="text-align: left;" class="' + borderbottomClass + '">' + (i + 1) + '. ' + rec.text() + '</td>';
                row += '<td class="' + borderbottomClass + '"></td>';
                row += '<td colspan="1" class="emptySpacingCell borderRight borderGrey borderBottom "' + borderbottomClass + '"><td>';
                row += '<tr>';
                $('#rightTableBody').append(row);
            }
            
            //Export To Pdf
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
              //  $('.report-view').css('padding-bottom', '15px');
              //  $('.report-view').height($('.report-view').height());
                hiqPdfConverter.startConversion();
            }
        }

        this.fundingCapacity = ko.observable();

        //Data Handling
        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            global.loadingReport(true);
            // load current basic surplus
            return Q.all([
                globalContext.getBasicSurplusReportViewDataById(id, self.basicSurplusReportViewData)
            ]).then(function () {
                return globalContext.getInventoryOfLiquidityResourcesFundingCapacity(self.fundingCapacity, self.basicSurplusReportViewData().basicSurplusAdminId(), self.basicSurplusReportViewData().institutionDatabaseName()).fin(function () {
                    //Aliases and Organization!
                    return bService.activate(self.basicSurplusReportViewData().basicSurplusAdminId(), self.basicSurplusReportViewData().institutionDatabaseName());
                });
            }).then(function () {
                return bService.compositionComplete();
            });
        };

        return this;
    };

    return vm;
});
