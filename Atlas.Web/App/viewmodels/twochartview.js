﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var ctor = function (id) {
        this.twoChartId = id
        this.twoChart = ko.observable();
        this.twoChartViewData = ko.observable();
        this.leftHighChartOptions = ko.observable();
        this.rightHighChartOptions = ko.observable();
        this.leftPercChangeLabel = ko.observable();
        this.leftYr2ColLabel = ko.observable("YEAR 2");
        this.rightYr2ColLabel = ko.observable("YEAR 2");
        this.rightPercChangeLabel = ko.observable();
        this.charting = charting;

        this.leftChartName = ko.observable();
        this.rightChartName = ko.observable();
        this.formatNumber = function (num, numType) {
            //numType is0 for dollar, 1 for percent
            var ret = 0;
            if (num == null) {
                ret = "";
            } else if (numType == 0) {
                ret = Math.round((num / 1000));
                ret = numeral(ret).format("0,0");
            } else if (numType == 1) {
                ret = numeral(num).format("0.00%");
            } else if (numType == 2) {
                ret = numeral(num).format("0.0") + "%";
            }
            return ret;
        };

        this.compositionComplete = function () {
            var self = this;

            if (self.twoChartViewData().errors.length > 0 || self.twoChartViewData().warnings.length) {
                globalContext.reportErrors(self.twoChartViewData().errors, self.twoChartViewData().warnings);
            }

            var charts = ["leftChartViewModel", "rightChartViewModel"];
            //charts.push('leftData');
            //charts.push('rightData');
            var seriesSide = {};

            var minVal = 100000000;
            var maxVal = 0;

            for (var i = 0; i < charts.length; i++) {
                var side = charts[i];
                var chart = 'rightChart';

                if (side == 'leftChartViewModel') {
                    chart = 'leftChart';
                }
                seriesSide[chart] = {};

                var groupEveryYear = 12;


                if (self.twoChart()[chart]().isQuarterly()) {
                    groupEveryYear = 4;
                }

                var scenLookUp = {};
                var tblData = [];

                var series = [];
                var seriesLookUp = {};

                for (var y = 0; y < self.twoChart()[chart]().chartScenarioTypes().length; y++) {

                    tblData[self.twoChart()[chart]().chartScenarioTypes()[y].priority()] = {
                        name: self.twoChart()[chart]().chartScenarioTypes()[y].scenarioType().name(),
                        years: []
                    }

                    scenLookUp[self.twoChart()[chart]().chartScenarioTypes()[y].scenarioType().name()] = self.twoChart()[chart]().chartScenarioTypes()[y].priority();

                    var scenProper = charting.scenarioProperName(self.twoChart()[chart]().chartScenarioTypes()[y].scenarioType().name());
                    var base = false;
                    if (self.twoChart()[chart]().chartScenarioTypes()[y].scenarioType().name().toUpperCase() == 'BASE') {
                        base = true;
                    }

                    var marker = {
                        symbol: "url(http://" + window.location.host + "/Content/images/AtlasGraphIcons/" + charting.scenarioIdLookUp(scenProper) + ".png)"
                    }
                    if (self.twoChart()[chart]().chartScenarioTypes()[y].scenarioType().name().toUpperCase() == 'BASE') {
                        base = true;
                        marker: { };
                    }

                    series.push({
                        name: scenProper,
                        type: (base ? 'column' : 'line'),
                        data: [],
                        pointStart: 0,
                        pointPadding: -.2,
                        marker: marker,
                        zIndex: (base ? 0 : 1),
                        color: charting.scenarioIdLookUp(scenProper).seriesColor,
                        marker: charting.scenarioIdLookUp(scenProper).marker
                    });

                    seriesLookUp[scenProper] = y;

                }




                for (var scen in self.twoChartViewData()[side].chartData) {
                    var scenProper = charting.scenarioProperName(scen);
                    var monthCounter = 0;
                    var year = 1;
                    var total = 0;



                    for (var z = 0; z < self.twoChartViewData()[side].chartData[scen].length; z++) {
                        monthCounter += 1;
                        total += self.twoChartViewData()[side].chartData[scen][parseFloat(z)];
                        series[seriesLookUp[scenProper]].data.push({ y: self.twoChartViewData()[side].chartData[scen][parseFloat(z)].value, date: self.twoChartViewData()[side].chartData[scen][parseFloat(z)].month });

                        if (minVal > self.twoChartViewData()[side].chartData[scen][parseFloat(z)].value) {
                            minVal = self.twoChartViewData()[side].chartData[scen][parseFloat(z)].value;
                        }

                        if (maxVal < self.twoChartViewData()[side].chartData[scen][parseFloat(z)].value) {
                            maxVal = self.twoChartViewData()[side].chartData[scen][parseFloat(z)].value;
                        }


                        if (monthCounter == groupEveryYear || z == self.twoChartViewData()[side].chartData[scen].length - 1) {
                            tblData[scenLookUp[scenProper]].years.push(total);
                            total = 0;
                            monthCounter = 0;
                            year += 1;
                        }
                    }
                }
                series = charting.scenarioIconStagger(series);
                console.log(series);
                seriesSide[chart].series = series;
                seriesSide[chart].isQuarterly = self.twoChart()[chart]().isQuarterly();
                seriesSide[chart].name = self.twoChart()[chart]().name();


                //Write Table Headers
                var headerRow = '<tr><th></th>';
                for (var z = 0; z < tblData.length; z++) {
                    headerRow += '<th>' + tblData[z].name + '</th>';
                }
                headerRow += '</tr>';
                //Add To The Three Tables
                //$('#' + chart + 'Table thead').append(headerRow);

                //Loop Through now and add row for each scenario
                for (var x = 0; x < tblData[0].years.length; x++) {
                    var leftRow = '<tr><td>Year ' + (x + 1).toString() + '</td>';
                    for (var z = 0; z < tblData.length; z++) {
                        leftRow += '<td>' + numeral(tblData[z].years[x] / 1000).format() + '</td>';
                    }
                    leftRow += '</tr>';
                    //$('#' + chart + 'Table tbody').append(leftRow);
                }

            }

            //log the precent change data for now until UI is done
            console.log("LEFT");
            console.table(self.twoChartViewData().leftPercentChangeData);
            console.log("RIGHT");
            console.table(self.twoChartViewData().rightPercentChangeData);

            var units = charting.simCompareChartMinMax(minVal, maxVal);
            for (var s in seriesSide) {
                var c = $('#' + s).highcharts(charting.sharedAxisChart(seriesSide[s].name, seriesSide[s].isQuarterly, seriesSide[s].series, units, s + "y_axis"));
                if (s == "leftChart") {
                    charting.moveSharedAxis(["middleTitle", s + "Title"], "middle_svg", "middle_axis_col");
                }
                charting.redrawSharedAxisChart(s, s + "y_axis");
                //$('#' + s).highcharts().update({
                //    id: s + "y_axis",
                //    yAxis: {
                //        labels: {
                //            enabled: false
                //        }
                //    }
                //}, true);
            }
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                setTimeout(function () { hiqPdfConverter.startConversion(); }, 1000);
            }
        };

        this.activate = function () {
            global.loadingReport(true);
            var self = this;
            return globalContext.getTwoChartViewDataById(self.twoChartId, self.twoChartViewData).then(function () {
                return globalContext.getTwoChartById(self.twoChartId, self.twoChart).then(function () {
                    switch (self.twoChart().leftChart().percChangeComparator()) {
                        case "year2Year1":
                            self.leftPercChangeLabel("Yr 2 % Change From Yr 1 " + self.twoChartViewData().leftChartViewModel.compareScenName);
                            break;
                        case "year2Year2":
                            self.leftPercChangeLabel("Yr 2 % Change From Yr 2 " + self.twoChartViewData().leftChartViewModel.compareScenName);
                            break;
                        case "24Month":
                            self.leftYr2ColLabel("24 Month Cumulative");
                            self.leftPercChangeLabel("24 Month Cumulative % Change From " + self.twoChartViewData().leftChartViewModel.compareScenName);
                            break;
                    }
                    switch (self.twoChart().rightChart().percChangeComparator()) {
                        case "year2Year1":
                            self.rightPercChangeLabel("Yr 2 % Change From Yr 1 " + self.twoChartViewData().rightChartViewModel.compareScenName);
                            break;
                        case "year2Year2":
                            self.rightPercChangeLabel("Yr 2 % Change From Yr 2 " + self.twoChartViewData().rightChartViewModel.compareScenName);
                            break;
                        case "24Month":
                            self.rightYr2ColLabel("24 Month Cumulative");
                            self.rightPercChangeLabel("24 Month Cumulative % Change From " + self.twoChartViewData().rightChartViewModel.compareScenName);
                            break;
                    }
                    self.twoChartViewData().errors = [];
                    self.twoChartViewData().warnings = [];
                    for (var i = 0; i < self.twoChartViewData().leftChartViewModel.errors.length; i++) {
                        self.twoChartViewData().errors.push(self.twoChartViewData().leftChartViewModel.errors[i]);
                    }
                    for (var i = 0; i < self.twoChartViewData().leftChartViewModel.warnings.length; i++) {
                        self.twoChartViewData().warnings.push(self.twoChartViewData().leftChartViewModel.warnings[i]);
                    }
                    for (var i = 0; i < self.twoChartViewData().rightChartViewModel.errors.length; i++) {
                        self.twoChartViewData().errors.push(self.twoChartViewData().rightChartViewModel.errors[i]);
                    }
                    for (var i = 0; i < self.twoChartViewData().rightChartViewModel.warnings.length; i++) {
                        self.twoChartViewData().warnings.push(self.twoChartViewData().rightChartViewModel.warnings[i]);
                    }
                    return true;
                });
            })
        };
    };

    return ctor;
});
