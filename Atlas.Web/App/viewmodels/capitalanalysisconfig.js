﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var ca = ko.observable();
    var capitalAnalysisVm = function (capitalAnalysis) {
        var self = this;
        var curPage = "CapitalAnalysisConfig";
        ca = capitalAnalysis;
        this.capitalAnalysis = capitalAnalysis;

        function onChangesCanceled() {
            self.sortHistoricalDateOffSets();
        }

        this.cancel = function () {
        };

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.ratioTypes = ko.observableArray();

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.compositionComplete = function () {
            //throw in an auto save in case a policies get renamed so when the dropdown values update it doesn't need a save
            if (self.capitalAnalysis().id() > 0) {
                globalContext.saveChangesQuiet();
            }
        };

        this.activate = function () {
            globalContext.logEvent("Viewed", curPage);
            app.on('application:cancelChanges', onChangesCanceled);
            global.loadingReport(true);
            return Q.all([
                globalContext.getAvailableBanks(self.institutions),
                globalContext.getCapitalAnalysisOptions(self.ratioTypes, self.capitalAnalysis().institutionDatabaseName()),
            ]).then(function () {
                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub = self.capitalAnalysis().institutionDatabaseName.subscribe(function (newValue) {
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                    globalContext.getCapitalAnalysisOptions(self.ratioTypes, newValue);
                });
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.capitalAnalysis().institutionDatabaseName()).then(function () {
                    global.loadingReport(false);
                });
            });
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        //historical Date Offset Block
        this.sortedHistoricalOffSets = ko.observableArray(self.capitalAnalysis().historicalBufferOffsets());

        this.sortHistoricalDateOffSets = function () {
            self.sortedHistoricalOffSets(self.capitalAnalysis().historicalBufferOffsets().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.capitalAnalysis().historicalBufferOffsets().sort(function (a, b) { return a.priority() - b.priority(); });

        this.removeHistoricalDateOffSet = function (coreFundOffSet) {

            var msg = 'Delete offset "' + coreFundOffSet.offSet() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    coreFundOffSet.entityAspect.setDeleted();
                    capitalAnalysis().historicalBufferOffsets().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortHistoricalDateOffSets();

                    if (capitalAnalysis().historicalBufferOffsets().length < 4) {
                        $('#addHistoricalDateOffsetButton').removeClass('disabled');
                    }
                }
            }

        };

        this.addHistoricalDateOffSet = function (offSet) {
            var newCoreFundOffSet = globalContext.createCapitalAnalysisBufferOffSet();
            if (capitalAnalysis().historicalBufferOffsets().length == 0) {
                newCoreFundOffSet.offSet(0);
            } else {
                newCoreFundOffSet.offSet(capitalAnalysis().historicalBufferOffsets()[capitalAnalysis().historicalBufferOffsets().length - 1].offSet() + 1);
            }
            
            newCoreFundOffSet.priority(capitalAnalysis().historicalBufferOffsets().length);
            capitalAnalysis().historicalBufferOffsets().push(newCoreFundOffSet);
            self.sortHistoricalDateOffSets();
        };


        self.sortHistoricalDateOffSets();


        return this;
    };

    return capitalAnalysisVm;
});
