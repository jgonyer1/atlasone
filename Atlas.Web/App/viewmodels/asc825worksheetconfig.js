﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var ascVm = function (asc) {
        var self = this;

        this.asc = asc;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset(), self.asc().simulationTypeId(), false)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }

        this.activate = function (id) {
            globalContext.logEvent("Viewed", "ASC825worksheetconfig");
            global.loadingReport(true);
            return globalContext.getAvailableSimulations(self.simulationTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset()).then(function () {
                return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset(), self.asc().simulationTypeId(), false).then(function () {
                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        //AsOfDateOffset Handling (Single-Institution Template)

                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.asc().institutionDatabaseName()).then(function () {

                            self.instSub = self.asc().institutionDatabaseName.subscribe(function (newValue) {
                                globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                            });

                            self.aodSub = self.asc().asOfDateOffset.subscribe(function (newValue) {
                                self.refreshSim();
                            });

                            self.simSub = self.asc().simulationTypeId.subscribe(function (newValue) {
                                self.refreshScen();
                            });
                            global.loadingReport(false);
                        });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.simSub.dispose();
        };

        return this;
    };

    return ascVm;
});
