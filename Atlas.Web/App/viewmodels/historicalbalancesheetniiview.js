﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var global = require('services/global');
    var curPage = "HistBalSheetNiiView";
    var lineItem = function (name, cells, total, zeroLineItem) {
        this.name = name;
        this.isTotal = total;
        this.cells = cells;
        this.zeroLineItem = zeroLineItem == undefined ? false : zeroLineItem;
    }
    var lineItemCell = function (data, options) {
        this.charting = charting;
        this.data = data;
        this.spacerCell = (options && options.spacerCell) || false;
        this.totalCell = (options && options.total) || false;
        this.colSpan = (options && options.colSpan) || 1;
    }

    var chartDataObj = function (names, ys, color, dataLabel, dataVal) {
        this.upColor = color;
        if (color.length > 0) {
            this.color = color;
        }
        this.dataLabels = {
            enabled: false,
            formatter: function () {
                //return this.dataLabel;
            }
        };
        this.style = {
            color: '#ffffff',
            fontWeight: 'bold',
            textShadow: '0px 0px 3px black'
        };
        this.pointPadding = 0;

        this.data = [];

        for (i = 0; i < names.length; i++) {
            this.data.push({
                name: names[i],
                y: ys[i],
                thisLabel: dataLabel,
                thisVal: dataVal
            });
        }
    }

    function dateDifference(d1, d2) {
        var m = moment(d1);
        var years = m.diff(d2, 'years');
        m.add(-years, 'years');
        var months = m.diff(d2, 'months');
        m.add(-months, 'months');
        var days = m.diff(d2, 'days');

        return { years: years, months: months, days: days };
    }

    function createChartObjs(chartD) {
        var accCollection = {};
        var scenCollection = {};
        var categories = [];
        var min = 0;
        var max = 0;
        var simulationSpans = [];
        var faultOutError = false;

        //for (var m = 1; m < asOfDates.length; m++) {
        //    var monthDiff = dateDifference(asOfDates[0], asOfDates[m]);
        //    var spanstr = [];
        //    if (Math.abs(monthDiff.years) > 0)
        //        spanstr.push(Math.abs(monthDiff.years) + " Year");
        //    if (Math.abs(monthDiff.months) > 0)
        //        spanstr.push(Math.abs(monthDiff.months) + " Mo");

        //    spanstr = spanstr.join(" ") + " Change";
        //    simulationSpans.push(spanstr);
        //}
        var cObj = {
            title: {
                text: chartD.chartName
            },

            xAxis: {
                categories: chartD.categories
            },

            yAxis: {
                allowDecimals: false,
                //min: 0,
                //max: 0,
                title: {
                    text: ''
                },
                labels: {
                    enabled: true
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + numeral(this.y).format('$0,0') + '<br/>' +
                        'Total: ' + numeral(this.point.stackTotal).format('$0,0');
                }
            },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    animation: false
                },
                series: {
                    animation: false
                }
            },
            series: chartD.series,
        };
        return cObj;
    }

    var histNiiVm = function (id) {
        var self = this;
        self.histNiiViewData = ko.observable();

        //hold the data for the balance sheet simulations, starts with one empty cell
        this.balSheetHeader = ko.observableArray([
            {
                name: "",
                aod: "",
                colspan: 1,
                spacerCell: true
            }

        ]);
        this.plainCompScenName = ko.observable();
        this.lineItems = [new lineItem("sim header", [], false, false)];
        this.simulationData = ko.observableArray();
        this.totalCellsWide = 1;
        this.numBalSheetItems = 0;
        this.niiLineItems = [];
        this.niiYearOneItems = [];
        this.niiYearTwoItems = [];
        this.niiCumItems = [];
        this.niiSimHeaders = [];
        this.niiSimHeadersYear2 = [];
        //this.niiSimHeaders = [];
        this.niiAODHeader = [];
        this.year2CompTitle = ko.observable();
        this.charting = charting;

        this.compositionComplete = function () {
            if (self.histNiiViewData().errors.length == 0) {
                var leftC = createChartObjs(self.histNiiViewData().leftChart);
                var rightC = createChartObjs(self.histNiiViewData().rightChart);

                leftC.yAxis.labels.enabled = false;

                //figure out overall Max
                var max, min;
                for (var i = 0; i < leftC.series.length; i++) {

                    leftC.series[i].color = charting.scenarioIdLookUp(leftC.series[i].name).seriesColor;
                    leftC.series[i].marker = charting.scenarioIdLookUp(leftC.series[i].name).marker;
                    for (var k = 0; k < leftC.series[i].data.length; k++) {
                        if (leftC.series[i].data[k] > max || max == undefined) {
                            max = leftC.series[i].data[k];
                        }

                        if (leftC.series[i].data[k] < min || min == undefined) {
                            min = leftC.series[i].data[k];
                        }

                        leftC.series[i].data[k] = { y: leftC.series[i].data[k], date: self.histNiiViewData().leftChart.categories[k] };

                    }
                }
                for (var i = 0; i < rightC.series.length; i++) {
                    rightC.series[i].color = charting.scenarioIdLookUp(rightC.series[i].name).seriesColor;
                    rightC.series[i].marker = charting.scenarioIdLookUp(rightC.series[i].name).marker;
                    for (var k = 0; k < rightC.series[i].data.length; k++) {
                        if (rightC.series[i].data[k] > max || max == undefined) {
                            max = rightC.series[i].data[k];
                        }

                        if (rightC.series[i].data[k] < min || min == undefined) {
                            min = rightC.series[i].data[k];
                        }

                        rightC.series[i].data[k] = { y: rightC.series[i].data[k], date: self.histNiiViewData().rightChart.categories[k] };
                    }
                }

                leftC.series = charting.scenarioIconStagger(leftC.series);

                rightC.series = charting.scenarioIconStagger(rightC.series);

                //leftC.yAxis.max = max;
                //rightC.yAxis.max = max;

                var units = charting.simCompareChartMinMax(min, max);
                //$('#' + s).highcharts(charting.simulationCompareChart(seriesSide[s].name, seriesSide[s].isQuarterly, seriesSide[s].series, units));
                //var finishedLeftC = charting.simulationCompareChart(leftC.title.text, self.histNiiViewData().leftChart.isQuarterly, leftC.series, units);
                var finishedLeftC = charting.sharedAxisChart(leftC.title.text, self.histNiiViewData().leftChart.isQuarterly, leftC.series, units, "left_y_axis");
                finishedLeftC.legend = { margin: 0 };
                finishedLeftC.chart.spacingBottom = 0;
                finishedLeftC.yAxis.labels.enabled = true;



                finishedLeftC.yAxis.tickLength = 0;
                //finishedLeftC.chart.paddingLeft = 45;
                //finishedLeftC.chart.width = 540;
                finishedLeftC.yAxis.title.text = null;
                //var finishedRightC = charting.simulationCompareChart(rightC.title.text, self.histNiiViewData().rightChart.isQuarterly, rightC.series, units);
                var finishedRightC = charting.sharedAxisChart(rightC.title.text, self.histNiiViewData().rightChart.isQuarterly, rightC.series, units, "right_y_axis");
                finishedRightC.legend = {margin: 0};
                //finishedRightC.chart.width = 585;
                
                finishedRightC.chart.spacingBottom = 0;
                finishedRightC.yAxis.tickLength = 0;
                finishedRightC.yAxis.title.text = null;
                //finishedRightC.yAxis.labels.enabled = false;
                document.getElementById("left_chart_name").innerHTML = self.histNiiViewData().leftChart.chartName;
                document.getElementById("right_chart_name").innerHTML = self.histNiiViewData().rightChart.chartName;
                $("#left_chart_container").highcharts(finishedLeftC);

                charting.moveSharedAxis(["middleTitle", "left_chart_name"], "middle_svg", "middle_axis_col");
                charting.redrawSharedAxisChart("left_chart_container", "left_y_axis");
                $("#right_chart_container").highcharts(finishedRightC);
                charting.redrawSharedAxisChart("right_chart_container", "right_y_axis");

            }

            $('#reportLoaded').text('1');
            global.loadingReport(false);
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage);  
            }
            else {
                hiqPdfConverter.startConversion();
            }
        };

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getHistoricalBalanceSheetNIIViewDataById(id, self.histNiiViewData).then(function () {
                globalContext.reportErrors(self.histNiiViewData().errors, self.histNiiViewData().warnings);
                if (self.histNiiViewData().errors.length > 0) {
                    return false;
                }
                if (self.histNiiViewData().compareScenarioName.indexOf(" as of") == -1) {
                    self.plainCompScenName(self.histNiiViewData().compareScenarioName);
                } else {
                    self.plainCompScenName(self.histNiiViewData().compareScenarioName.substring(0, self.histNiiViewData().compareScenarioName.indexOf(" as of")));
                }

                if (self.histNiiViewData().year2Comp == "year2year1") {
                    self.year2CompTitle("Yr 1 " + self.plainCompScenName());
                } else {
                    self.year2CompTitle("Yr 2 " + self.plainCompScenName());
                }


                self.topFootnotes = ko.observableArray(self.histNiiViewData().historicalBalanceSheetData.historicalBalanceSheetNIITopFootNotes);
                self.bottomFootnotes = ko.observableArray(self.histNiiViewData().historicalBalanceSheetData.historicalBalanceSheetNIIBottomFootNotes);

                var lineItemCollection = { "sim header": [], "Total Assets": [], "Total Liabilities & Equity": [], "Balance Sheet Spread": [] };
                var balFormatStr = '0,0';
                var rateFormatStr = '0.0';
                var intRateFormatStr = '0.00';
                var poaFormatStr = '0';
                var compSimAod = '';
                var totalHistoricalCount = 1;

                var totalAssetsCellCollection = [];
                var totalLiabsCellCollection = [];
                var spreadCollection = [];

                for (var simNumber = 0; simNumber < self.histNiiViewData().balanceSheetData.length; simNumber++) {
                    var sim = self.histNiiViewData().balanceSheetData[simNumber];
                    var prevSim = self.histNiiViewData().balanceSheetData[0];
                    var prevSimName = prevSim.simDate.split('_')[0];
                    var prevSimAod = prevSim.simDate.split('_')[1];
                    var simName = sim.simDate.split('_')[0];
                    var simAod = sim.simDate.split('_')[1];

                    if (simNumber == 0) {
                        compSimAod = moment(simAod).format('MM/DD/YYYY');
                        totalHistoricalCount += 3;
                    } else {
                        
                    }
                    var simHeader = [];
                    var cols = 3;
                    if (simNumber == 1) {
                        cols = 5;
                    } else if (simNumber == 2) {
                        cols = 6;
                    }


                    var item = ko.observable({
                        name: simName == "Base" ? "" : simName,
                        aod: simNumber == 0 ? moment(simAod).format('M/DD/YYYY') : moment(prevSimAod).format('M/DD/YYYY')+" vs. " + moment(simAod).format('M/DD/YYYY'),
                        colspan: cols,
                        spacerCell: false
                    });
                    if (simNumber > 0) {
                        self.balSheetHeader.push(
                            {
                                name: "",
                                aod: "",
                                colspan: 1,
                                spacerCell: true
                            }
                        );
                    }
                    self.balSheetHeader.push(item);

                    if (simNumber == 0) {
                        simHeader = ["Balance", "Rate", "% of<br>Assets"];
                    } else if (simNumber == 1) {
                        lineItemCollection["sim header"].push(new lineItemCell("", { spacerCell: true }));
                        simHeader = ["Balance", "Rate", "% of<br>Assets", "Balance &#916;", "Rate &#916;",];
                    }
                    else {
                        lineItemCollection["sim header"].push(new lineItemCell("", { spacerCell: true }));
                        simHeader = ["Balance", "Rate", "% of<br>Assets", "Balance &#916;",
                            "Rate &#916;", "% of<br>Assets &#916;"];
                    }
                    for (var i = 0; i < simHeader.length; i++) {
                        lineItemCollection["sim header"].push(new lineItemCell(simHeader[i]));
                    }

                    //add 3 cells for the first simulation, add 7 for every subsequent simulation
                    self.totalCellsWide += 3;
                    //totals

                    if (simNumber == 0) {
                        totalAssetsCellCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalAssets[0].total) / 1000).format(balFormatStr), { total: true }));
                        totalAssetsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalAssets[0].rate).format(intRateFormatStr) + '%', { total: true }));
                        totalAssetsCellCollection.push(new lineItemCell('100%', { total: true }));

                        totalLiabsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalLiabs[0].total / 1000).format(balFormatStr), { total: true }));
                        totalLiabsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalLiabs[0].rate).format(intRateFormatStr) + '%', { total: true }));
                        totalLiabsCellCollection.push(new lineItemCell('100%', { total: true }));

                        spreadCollection.push(new lineItemCell(""));
                        spreadCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalAssets[0].rate - self.histNiiViewData().totalLiabs[0].rate).format(intRateFormatStr) + '%'));
                        spreadCollection.push(new lineItemCell(""));
                    } else if (simNumber > 0) {

                        totalAssetsCellCollection.push(new lineItemCell("", { spacerCell: true }));
                        totalAssetsCellCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalAssets[simNumber].total) / 1000).format(balFormatStr), { total: true }));
                        totalAssetsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalAssets[simNumber].rate).format(intRateFormatStr) + '%', { total: true }));
                        totalAssetsCellCollection.push(new lineItemCell('100%', { total: true }));
                        totalAssetsCellCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalAssets[0].total - self.histNiiViewData().totalAssets[simNumber].total) / 1000).format(balFormatStr), { total: true }));
                        totalAssetsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalAssets[0].rate - self.histNiiViewData().totalAssets[simNumber].rate).format(intRateFormatStr) + '%', { total: true }));

                        totalLiabsCellCollection.push(new lineItemCell("", { spacerCell: true }));
                        totalLiabsCellCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalLiabs[simNumber].total) / 1000).format(balFormatStr), { total: true }));
                        totalLiabsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalLiabs[simNumber].rate).format(intRateFormatStr) + '%', { total: true }));
                        totalLiabsCellCollection.push(new lineItemCell('100%', { total: true }));
                        totalLiabsCellCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalLiabs[0].total - self.histNiiViewData().totalLiabs[simNumber].total) / 1000).format(balFormatStr), { total: true }));
                        totalLiabsCellCollection.push(new lineItemCell(numeral(self.histNiiViewData().totalLiabs[0].rate - self.histNiiViewData().totalLiabs[simNumber].rate).format(intRateFormatStr) + '%', { total: true }));

                        spreadCollection.push(new lineItemCell("", { spacerCell: true }));
                        spreadCollection.push(new lineItemCell(""));
                        spreadCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalAssets[simNumber].rate) - (self.histNiiViewData().totalLiabs[simNumber].rate)).format(intRateFormatStr) + '%'));
                        spreadCollection.push(new lineItemCell(""));
                        spreadCollection.push(new lineItemCell(""));
                        spreadCollection.push(new lineItemCell(numeral((self.histNiiViewData().totalAssets[0].rate - self.histNiiViewData().totalAssets[simNumber].rate) - (self.histNiiViewData().totalLiabs[0].rate - self.histNiiViewData().totalLiabs[simNumber].rate)).format(intRateFormatStr) + '%'));

                        if (simNumber == 1) {
                            self.totalCellsWide += 6;
                        } else if (simNumber == 2) {
                            self.totalCellsWide += 7;
                            totalAssetsCellCollection.push(new lineItemCell('0%', { total: true }));
                            totalLiabsCellCollection.push(new lineItemCell('0%', { total: true }));
                            spreadCollection.push(new lineItemCell(""));
                        }
                    }




                    for (var als in sim) {
                        if (als != "simDate") {
                            var lineItems = sim[als];

                            if (simNumber == 0) {
                                self.numBalSheetItems++;
                            }

                            for (var itemNum in lineItems) {
                                var thisItem = lineItems[itemNum];
                                var thisCellCollection;

                                var bal = thisItem.endBal;
                                var rate = thisItem.endRate;
                                var poa = (thisItem.endBal / self.histNiiViewData().totalAssets[simNumber].total) * 100;

                                if (simNumber == 0)
                                    
                                    self.lineItems.push(new lineItem(thisItem.accountName, [], false, true));

                                if (lineItemCollection[thisItem.accountName] == undefined) {
                                    lineItemCollection[thisItem.accountName] = [];
                                }

                                if (numeral(bal / 1000).format() != "0") {
                                    self.lineItems[self.lineItems.length - 1].zeroLineItem = false;
                                }

                                thisCellCollection = [
                                    new lineItemCell(numeral(bal / 1000).format(balFormatStr)),
                                    new lineItemCell(numeral(rate).format(intRateFormatStr) + '%'),
                                    new lineItemCell(numeral(poa).format(poaFormatStr) + '%')
                                ];

                                if (simNumber > 0) {
                                    var balDelta = self.histNiiViewData().balanceSheetData[0][als][itemNum].endBal - bal;
                                    var rateDelta = self.histNiiViewData().balanceSheetData[0][als][itemNum].endRate - rate;
                                    var poaDelta = ((self.histNiiViewData().balanceSheetData[0][als][itemNum].endBal / self.histNiiViewData().totalAssets[0].total) * 100) - poa;

                                    thisCellCollection.splice(0, 0, new lineItemCell("", { spacerCell: true }));
                                    //thisCellCollection.splice(2, 0, new lineItemCell(numeral(balDelta / 1000).format(balFormatStr)));
                                    //thisCellCollection.splice(4, 0, new lineItemCell(numeral(rateDelta).format(rateFormatStr) + '%'));
                                    //thisCellCollection.push(new lineItemCell("", { spacer: true }));
                                    thisCellCollection.push(new lineItemCell(numeral(balDelta / 1000).format(balFormatStr)));
                                    thisCellCollection.push(new lineItemCell(numeral(rateDelta).format(intRateFormatStr) + '%'));
                                }
                                if (simNumber > 1) {
                                    thisCellCollection.push(new lineItemCell(numeral(poaDelta).format(poaFormatStr) + '%'));
                                }
                                for (var i = 0; i < thisCellCollection.length; i++) {
                                    lineItemCollection[thisItem.accountName].push(thisCellCollection[i]);
                                }
                            }
                        }
                        if (simNumber == 0) {
                            if (als == 'assets') {
                                self.lineItems.push(new lineItem("Total Assets", [], true, false));
                            } else if (als == 'liabilities') {
                                self.lineItems.push(new lineItem("Total Liabilities & Equity", [], true, false));
                            }
                        }
                    }
                }
                for (var t = 0; t < totalAssetsCellCollection.length; t++) {
                    lineItemCollection["Total Assets"].push(totalAssetsCellCollection[t]);
                    lineItemCollection["Total Liabilities & Equity"].push(totalLiabsCellCollection[t]);
                    lineItemCollection["Balance Sheet Spread"].push(spreadCollection[t]);
                }
                self.lineItems.push(new lineItem("Balance Sheet Spread", [], false, false));

                var yearOneNiiCollection = {};
                var yearTwoNiiCollection = {};
                var cumNiiCollection = {}
                var niiHeaders = [];
                var niiHeadersYear2 = [];
                var niiHeaders2YearCumulative = [];
                var niiAODs = [];

                for (var a = 0; a < self.histNiiViewData().asOfDates.length; a++) {
                    var cspan = a == 0 ? 2 : 4;
                    //cspan = 2;
                    if (a > 0) {
                        niiAODs.push(new lineItemCell("", { spacerCell: true }));
                        niiAODs.push(new lineItemCell(self.histNiiViewData().asOfDates[0] + " vs. " + self.histNiiViewData().asOfDates[a], { colSpan: cspan }));
                    } else {
                        niiAODs.push(new lineItemCell(self.histNiiViewData().asOfDates[a], { colSpan: cspan }));
                    }
                    //niiAODs.push(new lineItemCell(self.histNiiViewData().asOfDates[a], { colSpan: cspan }));
                    niiAODs.push(new lineItemCell("", { spacerCell: true }));

                    if (a != self.histNiiViewData().asOfDates.length - 1) {
                        //niiAODs.push(new lineItemCell("", { spacer: true }));
                    }
                    if (a > 0) {
                        //niiAODs.push(new lineItemCell("", { spacer: true }));
                        //niiAODs.push(new lineItemCell(" vs. CURRENT", { colSpan: cspan }));

                    }
                }

                for (var scenNum in self.histNiiViewData().nii) {
                    var scenario = self.histNiiViewData().nii[scenNum];
                    var scenName = scenario.scenario.indexOf("Base") > -1 ? "Base" : scenario.scenario;



                    if (yearOneNiiCollection[scenName] == undefined) {
                        yearOneNiiCollection[scenName] = [];
                    }
                    if (yearTwoNiiCollection[scenName] == undefined) {
                        yearTwoNiiCollection[scenName] = [];
                    }
                    if (cumNiiCollection[scenName] == undefined) {
                        cumNiiCollection[scenName] = [];
                    }
                    var simCount = 0;
                    for (var d in scenario.data) {
                        var thisData = scenario.data[d];
                        if (thisData.yearGroup == "Year1") {
                            yearOneNiiCollection[scenName].push(thisData.endBal);
                        } else if (thisData.yearGroup == "Year2") {
                            yearTwoNiiCollection[scenName].push(thisData.endBal);

                        }
                    }

                }
                var compScenarioName = self.histNiiViewData().compareScenarioName;
                if (compScenarioName.indexOf("Base") > -1) {
                    compScenarioName = "Base";
                }

                //just find out which index the comparative scenario is at
                var compareScenarioIndex = -1;
                var scenArr = [];
                for (var scen in yearOneNiiCollection) {
                    scenArr.push(scen);
                    if (scen.toLowerCase() == compScenarioName.toLowerCase()) {
                        compareScenarioIndex = scenArr.length - 1;
                    }
                }

                var scenCounter = 0;

                for (var scen in yearOneNiiCollection) {
                    //scenArr.push(scen);
                    var arr = [];
                    for (simNum in yearOneNiiCollection[scen]) {
                        cumNiiCollection[scen].push(yearOneNiiCollection[scen][simNum] + yearTwoNiiCollection[scen][simNum]);
                        var deltaFromYr1 = "";
                        if (scenCounter != compareScenarioIndex) {
                            deltaFromYr1 = ((yearOneNiiCollection[scen][simNum] - yearOneNiiCollection[scenArr[compareScenarioIndex]][simNum]) / Math.abs(yearOneNiiCollection[scenArr[compareScenarioIndex]][simNum])) * 100;
                        }

                        if (simNum == 0) {
                            arr.push(new lineItemCell(numeral(yearOneNiiCollection[scen][simNum] / 1000).format(balFormatStr)));
                            //arr.push(new lineItemCell(""));
                            arr.push(new lineItemCell(deltaFromYr1.length == 0 ? "" : numeral(deltaFromYr1).format(rateFormatStr) + '%'));
                            arr.push(new lineItemCell("", { spacerCell: true }));

                            //add to the nii headers
                            if (scenCounter == 0) {
                                niiHeaders.push(new lineItemCell("NII"));
                                niiHeaders.push(new lineItemCell("% &#916; from<br />Yr 1 " + self.plainCompScenName()));
                                niiHeaders.push(new lineItemCell("", { spacerCell: true }));

                                //Year 2 Headers
                                //niiHeadersYear2.push(new lineItemCell(""));
                                //niiHeadersYear2.push(new lineItemCell(""), {spacer: true});
                                if (self.histNiiViewData().year2Comp == "year2year1") {
                                    niiHeadersYear2.push(new lineItemCell("% &#916; from<br />Yr 1 " + self.plainCompScenName()));
                                } else {
                                    niiHeadersYear2.push(new lineItemCell("% &#916; from<br />Yr 2 " + self.plainCompScenName()));
                                }
                                niiHeadersYear2.push(new lineItemCell("", {spacerCell: true}));
                                //niiHeadersYear2.push(new lineItemCell(""));

                                //Cumulative headrs
                                niiHeaders2YearCumulative.push(new lineItemCell(""));
                                niiHeaders2YearCumulative.push(new lineItemCell(""));
                                niiHeaders2YearCumulative.push(new lineItemCell("% &#916; from<br />" + self.plainCompScenName()));
                                niiHeaders2YearCumulative.push(new lineItemCell(""));
                                niiHeaders2YearCumulative.push(new lineItemCell(""));
                            }
                        } else {
                            var earDelta = "";
                            if (scenCounter != compareScenarioIndex) {
                                earDelta = ((yearOneNiiCollection[scen][0] - yearOneNiiCollection[scenArr[compareScenarioIndex]][0]) / Math.abs(yearOneNiiCollection[scenArr[compareScenarioIndex]][0])) - ((yearOneNiiCollection[scen][simNum] - yearOneNiiCollection[scenArr[compareScenarioIndex]][simNum]) / Math.abs(yearOneNiiCollection[scenArr[compareScenarioIndex]][simNum]));
                                earDelta = earDelta * 100;
                            }


                            arr.push(new lineItemCell("", { spacerCell: true }));
                            arr.push(new lineItemCell(numeral((yearOneNiiCollection[scen][simNum]) / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(deltaFromYr1.length == 0 ? "" : numeral(deltaFromYr1).format(rateFormatStr) + '%'));
                            
                            arr.push(new lineItemCell(numeral((yearOneNiiCollection[scen][0] - yearOneNiiCollection[scen][simNum]) / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(earDelta.length == 0 ? "" : numeral(earDelta).format(rateFormatStr) + '%'));
                            arr.push(new lineItemCell(""));
                            //arr.push(new lineItemCell(""));

                            //headers again
                            if (scenCounter == 0) {
                                niiHeaders.push(new lineItemCell("", { spacerCell: true }));
                                //niiHeaders.push(new lineItemCell("", { spacer: true }));
                                niiHeaders.push(new lineItemCell("NII"));
                                niiHeaders.push(new lineItemCell("% &#916; from<br />Yr 1 " + self.plainCompScenName()));
                                //niiHeaders.push(new lineItemCell(""));
                                niiHeaders.push(new lineItemCell("NII &#916;"));
                                niiHeaders.push(new lineItemCell("EAR &#916;"));
                                niiHeaders.push(new lineItemCell(""));

                                niiHeadersYear2.push(new lineItemCell("", { spacerCell: true }));
                                niiHeadersYear2.push(new lineItemCell(""));
                                niiHeadersYear2.push(new lineItemCell(""));
                                if (self.histNiiViewData().year2Comp == "year2year1") {
                                    niiHeadersYear2.push(new lineItemCell("% &#916; from<br />Yr 1 " + self.plainCompScenName()));
                                } else {
                                    niiHeadersYear2.push(new lineItemCell("% &#916; from<br />Yr 2 " + self.plainCompScenName()));
                                }
                                niiHeadersYear2.push(new lineItemCell(""));
                                niiHeadersYear2.push(new lineItemCell(""));
                                niiHeadersYear2.push(new lineItemCell(""));
                            }
                        }
                    }
                    self.niiYearOneItems.push(new lineItem(scen, arr, false));
                    scenCounter++;
                }

                self.niiAODHeader.push(new lineItem("", niiAODs, false));

                self.niiSimHeaders.push(new lineItem("Year 1 NII Projections", niiHeaders, false));
                self.niiSimHeadersYear2.push(new lineItem("Year 2  NII Projections", niiHeadersYear2, false));

                scenCounter = 0;
                scenArr = [];

                //just find out which index the comparative scenario is at
                //not sure if this is needed a second time, maybe year1 and year2 collections could be ordered differently?

                compareScenarioIndex = -1;

                scenArr = [];
                for (var scen in yearTwoNiiCollection) {
                    scenArr.push(scen);
                    if (scen.toLowerCase() == compScenarioName.toLowerCase()) {
                        compareScenarioIndex = scenArr.length - 1;
                    }
                }

                for (var scen in yearTwoNiiCollection) {
                    //scenArr.push(scen);
                    var arr = [];
                    for (simNum in yearTwoNiiCollection[scen]) {
                        var deltaFromYr1 = "";
                        if (self.histNiiViewData().year2Comp == "year2year1") {
                            deltaFromYr1 = ((yearTwoNiiCollection[scen][simNum] - yearOneNiiCollection[scenArr[compareScenarioIndex]][simNum]) / Math.abs(yearOneNiiCollection[scenArr[compareScenarioIndex]][simNum])) * 100;
                        } else if (self.histNiiViewData().year2Comp == "year2year2") {
                            deltaFromYr1 = ((yearTwoNiiCollection[scen][simNum] - yearTwoNiiCollection[scenArr[compareScenarioIndex]][simNum]) / Math.abs(yearTwoNiiCollection[scenArr[compareScenarioIndex]][simNum])) * 100;
                        }
                        if (simNum == 0) {
                            arr.push(new lineItemCell(numeral((yearTwoNiiCollection[scen][simNum]) / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(deltaFromYr1.length == 0 || (self.histNiiViewData().year2Comp == "year2year2" && scen.toLowerCase() == compScenarioName.toLowerCase()) ? "" : numeral(deltaFromYr1).format(rateFormatStr) + '%'));
                            arr.push(new lineItemCell(""));
                        } else {
                            var earDelta = "";
                            if (!(self.histNiiViewData().year2Comp == "year2year2" && scen.toLowerCase() == compScenarioName.toLowerCase())) {
                                if (self.histNiiViewData().year2Comp == "year2year1") {
                                    earDelta = ((yearTwoNiiCollection[scen][0] - yearOneNiiCollection[scenArr[compareScenarioIndex]][0]) / Math.abs(yearOneNiiCollection[scenArr[compareScenarioIndex]][0])) * 100;
                                    earDelta -= deltaFromYr1;
                                    earDelta = earDelta;
                                } else {
                                    earDelta = ((yearTwoNiiCollection[scen][0] - yearTwoNiiCollection[scenArr[compareScenarioIndex]][0]) / Math.abs(yearTwoNiiCollection[scenArr[compareScenarioIndex]][0])) * 100;
                                    earDelta -= deltaFromYr1;
                                    earDelta = earDelta;
                                }
                                //earDelta = ((yearTwoNiiCollection[scen][0] - yearOneNiiCollection[scenArr[compareScenarioIndex]][0]) / Math.abs(yearOneNiiCollection[scenArr[compareScenarioIndex]][0])) - (deltaFromYr1 / 100);//((yearTwoNiiCollection[scen][simNum] - yearOneNiiCollection[scenArr[0]][simNum]) / Math.abs(yearTwoNiiCollection[scenArr[0]][0]));

                            }

                            arr.push(new lineItemCell(""));
                            arr.push(new lineItemCell(numeral(yearTwoNiiCollection[scen][simNum] / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(deltaFromYr1.length == 0 || (self.histNiiViewData().year2Comp == "year2year2" && scen.toLowerCase() == compScenarioName.toLowerCase()) ? "" : numeral(deltaFromYr1).format(rateFormatStr) + '%'));
                            
                            arr.push(new lineItemCell(numeral((yearTwoNiiCollection[scen][0] - yearTwoNiiCollection[scen][simNum]) / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(earDelta.length == 0 ? "" : numeral(earDelta).format(rateFormatStr) + "%"));
                            arr.push(new lineItemCell(""));
                            //arr.push(new lineItemCell(""));
                        }
                    }
                    self.niiYearTwoItems.push(new lineItem(scen, arr, false));
                    scenCounter++;
                }
                scenCounter = 0;
                compareScenarioIndex = -1;

                scenArr = [];
                for (var scen in cumNiiCollection) {
                    scenArr.push(scen);
                    if (scen.toLowerCase() == compScenarioName.toLowerCase()) {
                        compareScenarioIndex = scenArr.length - 1;
                    }
                }
                for (var scen in cumNiiCollection) {
                    //scenArr.push(scen);
                    var arr = [];
                    for (simNum in cumNiiCollection[scen]) {

                        var deltaFromYr1 = "";
                        if (scenCounter != compareScenarioIndex) {
                            deltaFromYr1 = ((cumNiiCollection[scen][simNum] - cumNiiCollection[scenArr[compareScenarioIndex]][simNum]) / Math.abs(cumNiiCollection[scenArr[compareScenarioIndex]][simNum])) * 100;
                        }

                        if (simNum == 0) {
                            arr.push(new lineItemCell(numeral(cumNiiCollection[scen][simNum] / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(deltaFromYr1.length == 0 ? "" : numeral(deltaFromYr1).format(rateFormatStr) + '%'));
                            arr.push(new lineItemCell(""));
                        } else {
                            var earDelta = "";
                            if (scenCounter != compareScenarioIndex) {
                                //earDelta = ((cumNiiCollection[scen][simNum] - cumNiiCollection[scenArr[compareScenarioIndex]][simNum]) / Math.abs(cumNiiCollection[scenArr[compareScenarioIndex]][simNum])) - ((cumNiiCollection[scen][0] - cumNiiCollection[scenArr[compareScenarioIndex]][0]) / Math.abs(cumNiiCollection[scenArr[compareScenarioIndex]][0]));
                                //earDelta = earDelta * 100;
                                earDelta = ((cumNiiCollection[scen][0] - cumNiiCollection[scenArr[compareScenarioIndex]][0]) / Math.abs(cumNiiCollection[scenArr[compareScenarioIndex]][0])) * 100;
                                earDelta -= deltaFromYr1;
                            }


                            arr.push(new lineItemCell("", { spacerCell: true }));

                            arr.push(new lineItemCell(numeral((cumNiiCollection[scen][simNum]) / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(deltaFromYr1.length == 0 ? "" : numeral(deltaFromYr1).format(rateFormatStr) + '%'));
                            
                            arr.push(new lineItemCell(numeral((cumNiiCollection[scen][0] - cumNiiCollection[scen][simNum]) / 1000).format(balFormatStr)));
                            arr.push(new lineItemCell(earDelta.length == 0 ? "" : numeral(earDelta).format(rateFormatStr) + '%'));
                            arr.push(new lineItemCell(""));
                            //arr.push(new lineItemCell(""));

                        }
                    }
                    self.niiCumItems.push(new lineItem(scen, arr, false));
                    scenCounter++;
                }

                for (var lItem in self.lineItems) {
                    var thisItem = self.lineItems[lItem];
                    thisItem.cells = lineItemCollection[thisItem.name];
                }
                self.lineItems[0].name = "";
                var n = 0;
            });
        };
    };

    return histNiiVm;

});
