﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/app'
],
function (globalContext, router, app) {
    var vm = function () {
        var self = this;

        //Vars
        this.asOfDates = ko.observableArray();
        this.asOfDate = ko.observable();
        this.profile = ko.observable();
        this.simulations = ko.observableArray();
        this.scenarios = ko.observableArray();
        this.showScenUserIds = ko.observable(false);

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        //Data Handling
        this.activate = function (id) {
            //Pre-Setup
            return globalContext.getUserProfile(self.profile).then(function () {
                return globalContext.getAsOfDates(self.asOfDates, self.profile().databaseName);
            }).then(function () {
                //Subscriptions
                self.asOfDateSub = self.asOfDate.subscribe(function (newValue) {
                    self.updatePushLog(newValue);
                });

                //Set our asOfDate based off profile atlasAsOfDate, which will conveniently also fire off our sub
                self.asOfDate(self.profile().asOfDate);
            });
        };

        //Heavy Lifting
        this.updatePushLog = function (newValue) {
            var pushData = ko.observableArray();

            return globalContext.getPushLog(newValue, pushData).then(function () {
                self.simulations.removeAll();
                self.scenarios.removeAll();

                //Build a simData dictionary based off pushData, and push each unique simulation once
                var simData = {};
                pushData().forEach(function (row) {
                    if (!simData[row.simName]) {
                        simData[row.simName] = true;
                        self.simulations.push({
                            name: row.simName,
                            lastPush: row.lastSimPush,
                            pushId: row.simPushId
                        });
                    }
                });

                //Next, build a scenData dictionary based off pushData, with a "column" for each simulation
                var scenData = {};
                pushData().forEach(function (row) {
                    //Our pushArray will at first just have simulation names - we'll replace these with real data later
                    var pushArray = ko.observableArray();
                    for (var simName in simData)
                        pushArray.push(simName);

                    //Insert our scenario into the hash - if it already exists, no worries
                    if (!scenData[row.scenName]) {
                        scenData[row.scenName] = {
                            name: row.scenName,
                            pushArray: pushArray
                        };
                    }

                    //We'll only have each simulation-scenario combo once, so no need to worry about duplicates
                    //We'll also have each simulation at least once, so no need to worry about -1 index either
                    pushArray = scenData[row.scenName].pushArray;
                    pushArray()[pushArray.indexOf(row.simName)] = {
                        lastPush: row.lastScenPush,
                        pushId: row.scenPushId,
                        pushColor: (row.lastSimPush != row.lastScenPush) ? '#FF0000' : '#009933'
                    };
                });

                //Now push the scenData values onto our actual scenarios array that will be used by the html table
                for (var scenName in scenData) {
                    var scen = scenData[scenName];

                    //While we're at it blank out empty pushArray entries
                    for (var i = 0; i < scen.pushArray().length; i++) {
                        if (!scen.pushArray()[i].lastPush) {
                            scen.pushArray()[i] = {
                                lastPush: '',
                                pushId: '',
                                pushColor: '#000000'
                            };
                        }
                    }

                    //Finally push the scenario - hooray!
                    self.scenarios.push(scen);
                }
            });
        };

        //Disposal
        this.deactivate = function (isClose) {
            self.asOfDateSub.dispose();
        };

        return this;
    };

    return vm;
});
