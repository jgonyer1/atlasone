﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ascVm = function (id) {
        var self = this;
        this.ascViewData = ko.observable();

        this.compositionComplete = function () {
            if (self.ascViewData().errors.length > 0) {
                globalContext.reportErrors(self.ascViewData().errors, self.ascViewData().warnings);
                return true;
            }
            var tbl = self.ascViewData().tableData;
            var monthLabels = {};
            var lblTbl = self.ascViewData().monthLabels;

            for (var i = 0; i < lblTbl.length; i++) {
                var rec = lblTbl[i];
                monthLabels[rec.month] = i;

                $('#ascHead').append('<th>' + rec.label.replace(' ', '<br>') +  '</th>');
            }

            var cats = {};

            //Build JSON Object before we output Data
            for (var i = 0; i < tbl.length; i++) {
                var rec = tbl[i];
                if (cats[rec.name] == undefined) {
                    cats[rec.name] = {
                        'Cashflow': new Array(lblTbl.length),
                        'Functional Cost': new Array(lblTbl.length),
                        'Discount Rate': new Array(lblTbl.length),
                        'Periods Out': new Array(lblTbl.length),
                        'Present Value': new Array(lblTbl.length),
                        'Economic Value': new Array(lblTbl.length),
                        'Duration': new Array(lblTbl.length),
                        'Modified Duration': new Array(lblTbl.length),
                        stBal: 0
                    }
                }
       
                //Set Up Array Values
                cats[rec.name]['Cashflow'][monthLabels[rec.month]] = rec.mVCashflow;
                cats[rec.name]['Functional Cost'][monthLabels[rec.month]] = rec.mVFuncCost;

                if (rec.mVCashflow == 0) {
                    cats[rec.name]['Discount Rate'][monthLabels[rec.month]] = 0;

                }
                else
                {
                    cats[rec.name]['Discount Rate'][monthLabels[rec.month]] = rec.mVDiscount;
                }
                
                cats[rec.name]['Periods Out'][monthLabels[rec.month]] = rec.mVPeriodOut;
                cats[rec.name]['Present Value'][monthLabels[rec.month]] = rec.mVPValue;
                cats[rec.name]['Economic Value'][monthLabels[rec.month]] = rec.mVMValue;
                cats[rec.name]['Duration'][monthLabels[rec.month]] = rec.mVDur;
                cats[rec.name]['Modified Duration'][monthLabels[rec.month]] = rec.mVModDur;

                cats[rec.name].stBal += numeral(rec.stBal).value();

            }


            for (var c in cats) {
                if (cats[c].stBal != 0) {
                    $('#ascBody').append('<tr><td>' + c + '</td><td colspan = "' + lblTbl.length + '"</td></tr>');
                    $('#ascBody').append('<tr class="spacerRow"><td></td><td colspan = "' + lblTbl.length + '"</td></tr>');
                    $('#ascBody').append('<tr><td>----Economic Value Info----</td><td colspan = "' + lblTbl.length + '"</td></tr>');

                    for (var d in cats[c]) {
                        if (d != 'stBal') {
                            var row = '<tr><td>' + d + '</td>';
                            for (var z = 0; z < cats[c][d].length; z++) {
                                if (numeral(cats[c][d][z]).value() == 0) {
                                    row += '<td>--</td>'
                                }
                                else {
                                    switch (d) {
                                        case 'Cashflow':
                                        case 'Present Value':
                                        case 'Economic Value':
                                        case 'Functional Cost':

                                            row += '<td>' + numeral(cats[c][d][z] / 1000).format('0,0') + '</td>'
                                            break;
                                        default:
                                            row += '<td>' + numeral(cats[c][d][z]).format('0.00') + '</td>'
                                            break;
                                    }
                                }

                            }
                            row += '</tr>';
                            $('#ascBody').append(row);
                        }


                    }
                }

            }

            $("#worksheet-view").parent().height($("#worksheet-view").parent().height());
            $("#worksheet-view").height($("#worksheet-view").height());

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }

        }



        this.activate = function () {
            globalContext.logEvent("Viewed", "ASC825worksheetview");
            global.loadingReport(true);
            return globalContext.getASC825WorksheetViewDataById(id, self.ascViewData).then(function () {

            });
        };
    };

    return ascVm;

});
