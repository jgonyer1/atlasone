﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/app',
    'viewmodels/navigateawaymodal'
],
function (globalContext, router, app, naModal) {
    var vm = function () {
        var self = this;
        var curPage = "LiabilityPricingConfig";
        //Vars
        this.liabilityPricing = ko.observable();
        this.modelSetup = ko.observable();
        this.historicmodel = ko.observable();
        this.webRateDates = ko.observableArray();

        this.advancedIndexes = ko.observableArray(['Atlanta', 'Boston', 'Chicago', 'Cincinnati', 'Dallas', 'Des Moines', 'Indianapolis', 'New York', 'Pittsburgh', 'San Francisco', 'Seattle', 'Topeka']);
        this.nonMatOptions = ko.observableArray(['DDA', 'Public DDA', 'Online DDA', 'NOW', 'Public NOW', 'Online NOW', 'Brokered NOW', 'Savings', 'Public Savings', 'Online Savings', 'MMDA', 'Public MMDA', 'Online MMDA', 'Brokered Reciprocal MMDA', 'Brokered One-Way MMDA', 'Brokered MMDA', 'Other Non-Mat Deposit'])
        this.rateCurveOptions = ko.observableArray(['LIBOR/Swap', 'Treasury', 'FHLB', 'Brokered CD']);
        this.timeDepositOptions = ko.observableArray();


        for (var i = 1; i < 32; i++){
            this.timeDepositOptions().push(i + ' Day');
        }

        for (var i = 1; i < 121; i++) {
            this.timeDepositOptions().push(i + ' Month');
        }

        this.isSaving = ko.observable(false);
        //Navigation Helpers
        this.hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges();
        });

        this.canSave = ko.pureComputed(function () {
            return self.hasChanges() && !self.isSaving();
        });

        this.canDeactivate = function () {
            if (! self.hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { self.save(); }
            );
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        this.cancel = function () {
            self.isSaving(true);
            globalContext.cancelChanges();
            self.sortNonMats();
            self.sortTimeDeps();
            self.sortHistTimeDeps();
            self.isSaving(false);
        };


        this.save = function () {
            self.isSaving(true);
            return globalContext.saveChanges().fin(complete);

            function complete() {
                self.isSaving(false);
            }
        };





        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            //Get Eve Config
            //global.loadingReport(true);
            return globalContext.getLiabilityPricing(self.liabilityPricing).fin(function () {
                return globalContext.getModelSetup(self.modelSetup).then(function () {
                    return globalContext.getHistoricModelSetups(self.modelSetup().id(), self.historicmodel).then(function () {
                        //Build Sort Methods In Here

                        //Non Maturity Section
                        self.sortedNonMats = ko.observableArray(self.liabilityPricing().nonMaturityDepositRates());

                        self.sortNonMats = function () {
                            self.sortedNonMats(self.liabilityPricing().nonMaturityDepositRates().sort(function (a, b) { return a.priority() - b.priority(); }));
                        };

                        self.liabilityPricing().nonMaturityDepositRates().sort(function (a, b) { return a.priority() - b.priority(); });


                        self.removeNonMaturityRate = function (nm) {

                            var msg = 'Delete Non Maturity Rate "' + nm.name() + '" ?';
                            var title = 'Confirm Delete';
                            return app.showMessage(msg, title, ['No', 'Yes'])
                                .then(confirmDelete);

                            function confirmDelete(selectedOption) {
                                if (selectedOption === 'Yes') {
                                    nm.entityAspect.setDeleted();
                                    self.liabilityPricing().nonMaturityDepositRates().forEach(function (anEveScenarioType, index) {
                                        anEveScenarioType.priority(index);
                                    });
                                    self.sortNonMats();
                                }
                            }

                        };

                        self.addNonMaturityRate = function () {
                            var newNm = globalContext.createNonMaturityDepositRate();
                            newNm.type('DDA');
                            newNm.name('DDA');
                            newNm.rate(0);
                            newNm.priority(self.liabilityPricing().nonMaturityDepositRates().length);
                            self.liabilityPricing().nonMaturityDepositRates().push(newNm);
                            self.sortNonMats();
                        };

                        self.sortNonMats();



                        //Time Deposit Specials Section

                        self.sortedTimeDeps = ko.observableArray(self.liabilityPricing().timeDepositSpecialRates());

                        self.sortTimeDeps = function () {
                            self.sortedTimeDeps(self.liabilityPricing().timeDepositSpecialRates().sort(function (a, b) { return a.priority() - b.priority(); }));
                        };

                        self.liabilityPricing().timeDepositSpecialRates().sort(function (a, b) { return a.priority() - b.priority(); });


                        self.sortTimeDeps();

                        self.removeTimeDepositSpecial = function (nm) {

                            var msg = 'Delete Time Deposit Special Rate "' + nm.name() + '" ?';
                            var title = 'Confirm Delete';
                            return app.showMessage(msg, title, ['No', 'Yes'])
                                .then(confirmDelete);

                            function confirmDelete(selectedOption) {
                                if (selectedOption === 'Yes') {
                                    nm.entityAspect.setDeleted();
                                    self.liabilityPricing().timeDepositSpecialRates().forEach(function (anEveScenarioType, index) {
                                        anEveScenarioType.priority(index);
                                    });
                                    self.sortTimeDeps();
                                }
                            }

                        };

                        self.addTimeDepositSpecial = function () {
                            var newNm = globalContext.createTimeDepositSpecialRate();
                            newNm.term('1 Day');
                            newNm.name('1 Day Special');
                            newNm.rate(0);
                            newNm.priority(self.liabilityPricing().timeDepositSpecialRates().length);
                            self.liabilityPricing().timeDepositSpecialRates().push(newNm);
                            self.sortTimeDeps();
                        };




                        //Time Deposit Specials Section

                        self.sortedTimeDeps = ko.observableArray(self.liabilityPricing().timeDepositSpecialRates());

                        self.sortTimeDeps = function () {
                            self.sortedTimeDeps(self.liabilityPricing().timeDepositSpecialRates().sort(function (a, b) { return a.priority() - b.priority(); }));
                        };

                        self.liabilityPricing().timeDepositSpecialRates().sort(function (a, b) { return a.priority() - b.priority(); });


                        self.sortTimeDeps();

                        self.removeTimeDepositSpecial = function (nm) {

                            var msg = 'Delete Time Deposit Special Rate "' + nm.name() + '" ?';
                            var title = 'Confirm Delete';
                            return app.showMessage(msg, title, ['No', 'Yes'])
                                .then(confirmDelete);

                            function confirmDelete(selectedOption) {
                                if (selectedOption === 'Yes') {
                                    nm.entityAspect.setDeleted();
                                    self.liabilityPricing().timeDepositSpecialRates().forEach(function (anEveScenarioType, index) {
                                        anEveScenarioType.priority(index);
                                    });
                                    self.sortTimeDeps();
                                }
                            }

                        };

                        self.addTimeDepositSpecial = function () {
                            var newNm = globalContext.createTimeDepositSpecialRate();
                            newNm.term('1 Day');
                            newNm.name('1 Day Special');
                            newNm.rate(0);
                            newNm.priority(self.liabilityPricing().timeDepositSpecialRates().length);
                            self.liabilityPricing().timeDepositSpecialRates().push(newNm);
                            self.sortTimeDeps();
                        };



                        //Historical Time Deposit Specials Section

                        self.sortedHistTimeDeps = ko.observableArray(self.liabilityPricing().historicalTimeDepositSpecialRates());

                        self.sortHistTimeDeps = function () {
                            self.sortedHistTimeDeps(self.liabilityPricing().historicalTimeDepositSpecialRates().sort(function (a, b) { return a.priority() - b.priority(); }));
                        };

                        self.liabilityPricing().historicalTimeDepositSpecialRates().sort(function (a, b) { return a.priority() - b.priority(); });


                        self.sortHistTimeDeps();

                        self.removeHistoricalTimeDepositSpecial = function (nm) {


                            var msg = 'Delete Historical Time Deposit Special Rate "' + nm.name() + '" ?';
                            var title = 'Confirm Delete';
                            return app.showMessage(msg, title, ['No', 'Yes'])
                                .then(confirmDelete);

                            function confirmDelete(selectedOption) {
                                if (selectedOption === 'Yes') {
                                    nm.entityAspect.setDeleted();
                                    self.liabilityPricing().historicalTimeDepositSpecialRates().forEach(function (anEveScenarioType, index) {
                                        anEveScenarioType.priority(index);
                                    });
                                    self.sortHistTimeDeps();
                                }

                            }

                        };

                        self.addHistoricalTimeDepositSpecial = function () {
                            var newNm = globalContext.createHistoricalTimeDepositSpecialRate();
                            newNm.term('1 Day');
                            newNm.name('1 Day Special');
                            newNm.rate(0);
                            newNm.priority(self.liabilityPricing().historicalTimeDepositSpecialRates().length);
                            self.liabilityPricing().historicalTimeDepositSpecialRates().push(newNm);
                            self.sortHistTimeDeps();
                        };


                        self.typeChange = function (nm) {
                            if (nm.term != undefined) {
                                nm.name(nm.term() + ' Special');
                            }
                            else {
                                nm.name(nm.type());
                            }



                        }


                        //self.cancel = function () {
                        //    self.sortNonMats();
                        //    self.sortTimeDeps();
                        //    self.sortHistTimeDeps();
                        //};


                        return globalContext.getWebRateAsOfDate(self.webRateDates).then(function () {
                            //global.loadingReport(false);
                        });
                    });
                });
            });
        };
        return this;
    };

    return vm;
});
