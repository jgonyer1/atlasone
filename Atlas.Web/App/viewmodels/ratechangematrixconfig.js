﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var rateChangeVm = function (rateChange) {
        var self = this;

        this.rateChange = rateChange;

        function onChangesCanceled() {
            self.sortScenarioTypes();
        }

        this.cancel = function () {
        };

        //Scenario Block For Sorting, Adding, and Deleting From Scenario List
        this.sortedScenarioTypes = ko.observableArray(self.rateChange().rateChangeMatrixScenarios());

        this.sortScenarioTypes = function () {
            self.sortedScenarioTypes(self.rateChange().rateChangeMatrixScenarios().sort(function (a, b) { return a.priority() - b.priority(); }));
        };

        this.rateChange().rateChangeMatrixScenarios().sort(function (a, b) { return a.priority() - b.priority(); });

        this.addScenarioType = function (scenarioType) {
            var newRateChangeScenario = globalContext.createRateChangeMatrixScenario();
            newRateChangeScenario.scenarioTypeId(scenarioType.id);
            newRateChangeScenario.priority(rateChange().rateChangeMatrixScenarios().length);
            rateChange().rateChangeMatrixScenarios().push(newRateChangeScenario);
            self.sortScenarioTypes();
        };

        this.dontAddScenarioType = function (data, event) {
            event.stopPropagation();
        };


        this.removeScenarioType = function (rateChangeScenario) {

            var msg = 'Delete scenario "' + rateChangeScenario.scenarioType().name() + '" ?';
            var title = 'Confirm Delete';
            return app.showMessage(msg, title, ['No', 'Yes'])
                .then(confirmDelete);

            function confirmDelete(selectedOption) {
                if (selectedOption === 'Yes') {
                    rateChangeScenario.entityAspect.setDeleted();
                    rateChange().rateChangeMatrixScenarios().forEach(function (anEveScenarioType, index) {
                        anEveScenarioType.priority(index);
                    });
                    self.sortScenarioTypes();

                }
            }

        };

        self.sortScenarioTypes();

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (eveScenarioType, index) {
                eveScenarioType.priority(index);
            });
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.scenarioTypes = ko.observableArray();

        this.institutions = ko.observableArray();


        this.refreshScen = function () {
            globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.rateChange().institutionDatabaseName(), self.rateChange().asOfDateOffset(), self.rateChange().simulationTypeId(), true)
        }

        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.rateChange().institutionDatabaseName(), self.rateChange().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }


        this.activate = function (id) {
            global.loadingReport(true);
            app.on('application:cancelChanges', onChangesCanceled);

            return globalContext.getAvailableBanks(self.institutions).then(function () {
                return globalContext.getAvailableSimulations(self.simulationTypes, self.rateChange().institutionDatabaseName(), self.rateChange().asOfDateOffset()).then(function () {
                    return globalContext.getAvailableSimulationScenarios(self.scenarioTypes, self.rateChange().institutionDatabaseName(), self.rateChange().asOfDateOffset(), self.rateChange().simulationTypeId(), true).then(function () { 
                    self.availableScenarioTypes = ko.computed(function () {
                        var scenarioTypeIdsInUse = self.rateChange().rateChangeMatrixScenarios().map(function (scenario) {
                            return scenario.scenarioTypeId();
                        });
                        var result = [];
                        self.scenarioTypes().forEach(function (scenarioType) {
                            if (scenarioType.name != 'Base') {
                                result.push({
                                    id: scenarioType.id(),
                                    name: scenarioType.name(),
                                    inUse: scenarioTypeIdsInUse.indexOf(scenarioType.id()) != -1
                                });
                            }

                        });
                        return result;
                    });

                    //AsOfDateOffset Handling (Single-Institution Template)
                    //AsOfDateOffset Handling (Single-Institution Template)
                    self.instSub = self.rateChange().institutionDatabaseName.subscribe(function (newValue) {
                        globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                        self.refreshSim();
                    });

                    self.aodSub = self.rateChange().asOfDateOffset.subscribe(function (newValue) {
                        self.refreshSim();
                    });

                    self.simSub = self.rateChange().simulationTypeId.subscribe(function (newValue) {
                        self.refreshScen();
                    });

                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.rateChange().institutionDatabaseName()).then(function () {
                        global.loadingReport(false);

                    });
                });
                });
            })
        };

        this.deactivate = function () {
            app.off('application:cancelChanges', onChangesCanceled);
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
            self.aodSub.dispose();
            self.simSub.dispose();
        };

        return this;
    };

    return rateChangeVm;
});
