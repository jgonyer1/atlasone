﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app',
    'services/liquidityservice'
],
    function (globalContext, global, app, loveService) {
        var invVm = function (inv, rep) {
            var self = this;
            var curPage = "InventoryLiquidityResourcesConfig";
            this.inv = inv;

            this.currentItemsToMove = ko.observableArray();

            this.activeDetail = ko.observable({ name: ko.observable('POOP'), upSections: ko.observableArray(), downSections: ko.observableArray(), availableDetails: ko.observableArray() });

            this.tierOptions = ['Tier 1 Liquidity', 'Tier 2 Liquidity', 'Tier 3 Liquidity', 'Other Liquidity', 'Custom'];

            this.wholesaleOptions = ['% of Assets', '% of Deposits', '$ Amount'];

            this.sortedTiers = ko.observableArray();

            this.sortedWholesale = ko.observableArray();

            this.sortWholesale = function () {
                self.sortedWholesale(self.inv().inventoryLiquidityResourcesWholesaleFunding().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            this.sortTiers = function () {
                self.sortedTiers(self.inv().inventoryLiquidityResourcesTier().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            this.sortDetails = function (l) {
                l.sortedDetails(l.inventoryLiquidityResourcesTierDetail().sort(function (a, b) { return a.priority() - b.priority(); }));
            }

            this.removeTier = function () {

                var msg = 'Delete ' + this.name() + ' Tier?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        var detsLen = item.inventoryLiquidityResourcesTierDetail().length;

                        //Loop through and delete all details
                        for (var d = detsLen - 1; d >= 0; d--) {
                            //Looop through and delete al scens
                            var scenLen = item.inventoryLiquidityResourcesTierDetail()[d].inventoryLiquidityResourcesTierDetailCollection().length;
                            for (var z = scenLen - 1; z >= 0; z--) {
                                item.inventoryLiquidityResourcesTierDetail()[d].inventoryLiquidityResourcesTierDetailCollection()[z].entityAspect.setDeleted();
                            }

                            item.inventoryLiquidityResourcesTierDetail()[d].entityAspect.setDeleted();
                        }

                        item.entityAspect.setDeleted();
                        self.sortTiers();

                    }
                }
            };

            this.addTier = function () {
                self.addTierWithDefault('Custom');
            }

            this.addTierWithDefault = function (name) {
                var det = globalContext.createInventoryLiquidityResourcesTier();

                det.priority(self.inv().inventoryLiquidityResourcesTier().length);
                det.sortedDetails = ko.observableArray();
                det.name(name);
                self.inv().inventoryLiquidityResourcesTier.push(det);
                self.sortTiers();
                return true;
            }

            //When a tier changes we blow awawy what was in the colelctions of the details
            this.tierTypeChange = function () {
                //Take thiz and loop through all detail values and blow them away
                var detsLen = this.inventoryLiquidityResourcesTierDetail().length;

                //Loop through and delete all details
                for (var d = detsLen - 1; d >= 0; d--) {
                    //Looop through and delete al scens
                    var scenLen = this.inventoryLiquidityResourcesTierDetail()[d].inventoryLiquidityResourcesTierDetailCollection().length;
                    for (var z = scenLen - 1; z >= 0; z--) {
                        this.inventoryLiquidityResourcesTierDetail()[d].inventoryLiquidityResourcesTierDetailCollection()[z].entityAspect.setDeleted();
                    }

                    this.inventoryLiquidityResourcesTierDetail()[d].entityAspect.setDeleted();
                }

                self.sortDetails(this);


            }


            this.addDetailWithParam = function (parent, name) {
                var scen = globalContext.createInventoryLiquidityResourcesTierDetail();

                scen.priority(parent.inventoryLiquidityResourcesTierDetail().length);
                scen.availableDetails = ko.observableArray();
                scen.name(name);

                scen.upSections = ko.observableArray();
                scen.downSections = ko.observableArray();

                parent.inventoryLiquidityResourcesTierDetail.push(scen);
                self.updateAvailableFields(scen);
                self.sortDetails(parent);
            }

            this.addDetail = function () {
                console.log('in here');
                self.addDetailWithParam(this, "");
            }

            this.removeDetail = function () {
                var msg = 'Delete ' + this.name() + ' Detail?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        var scenLen = item.inventoryLiquidityResourcesTierDetailCollection().length;
                        for (var z = scenLen - 1; z >= 0; z--) {
                            item.inventoryLiquidityResourcesTierDetailCollection()[z].entityAspect.setDeleted();
                        }

                        var parent = self.inv().inventoryLiquidityResourcesTier().filter(function (par, arr, index) { return par.id() == item.inventoryLiquidityResourcesTierId() })[0]

                        item.entityAspect.setDeleted();

                        parent.inventoryLiquidityResourcesTierDetail().forEach(function (d, index) {
                            d.priority(index);
                        });

                        self.sortDetails(parent);
                    }
                }
            }

            this.configureCollection = function (data) {

                self.activeDetail(data);
                $('#configureDetail').modal('show')
            }

            this.addToUpCollection = function () {
                //Loop through items and add them to the up section of active detail
                self.addToCollection(true);
            };


            this.addToDownCollection = function () {
                //Loop through items and add them to the up section of active detail
                self.addToCollection(false);

            };

            this.addToCollection = function (up) {



                for (var z = 0; z < self.currentItemsToMove().length; z++) {

                    var fieldName = self.currentItemsToMove()[z];
                    //Take fieldName and find it in the fields
                    var idx = self.basicSurplusFields().findIndex(function (el) { return el.fieldName == fieldName });

                    var newCol = globalContext.createInventoryLiquidityResourcesTierDetailCollection();

                    newCol.name(fieldName);
                    newCol.addNumber(up);
                    newCol.sourceRow = self.basicSurplusFields()[idx];
                    self.activeDetail().inventoryLiquidityResourcesTierDetailCollection.push(newCol);

                }

                self.updateAvailableFields(self.activeDetail());

                self.currentItemsToMove.removeAll();
            };

            this.removeCollectionItem = function () {
                var msg = 'Delete ' + this.sourceRow.title + ' Detail?';
                var item = this;
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {

                        item.entityAspect.setDeleted();
                        self.activeDetail().inventoryLiquidityResourcesTierDetailCollection.remove(item);


                        self.sortUpandDown(self.activeDetail());
                        self.updateAvailableFields(self.activeDetail());
                    }
                }
            }


            this.thisReport = rep;

            this.cancel = function () {
            };

            this.availBasicSurplus = ko.observableArray();
            this.institutions = ko.observableArray();
            this.global = global;
            this.asOfDateOffsets = ko.observableArray();
            this.policyOffsets = ko.observableArray();

            this.setDefaultBasicSurplus = function () {
                var aod = self.asOfDateOffsets()[self.inv().asOfDateOffset()].asOfDateDescript;

                if (!self.inv().overrideBasicSurplus()) {
                    self.inv().basicSurplusId(self.policyOffsets()[0][aod][0].bsId);
                }
            }


            this.sortUpandDown = function (l) {
                l.upSections(l.inventoryLiquidityResourcesTierDetailCollection().filter(function (item, arr, index) { return item.addNumber() }));
                l.downSections(l.inventoryLiquidityResourcesTierDetailCollection().filter(function (item, arr, index) { return !item.addNumber() }));
            };

            this.updateAvailableFields = function (l) {

                //Always easier jsut ro reset it ehre
                loveService.setTierDetails(l.inventoryLiquidityResourcesTier().name(), l, self.basicSurplusFields);

                //Now need to sort the collection based off of plus minus sections     
                self.sortUpandDown(l);

            }

            this.configureModalClose = function () {
                loveService.calculateTierDetails(self.activeDetail(), self.inv);
            }

            this.setTierTotals = function () {
                //update each tier respectivlvt
                loveService.calculateTier('Tier 1 Liquidity', self.basicSurplusTierTotals().tier1Amount, self.basicSurplusTierTotals().tier1CumAmount, self.basicSurplusTierTotals().tier1BasicSurplusAmount, self.inv);
                loveService.calculateTier('Tier 2 Liquidity', self.basicSurplusTierTotals().tier2Amount, self.basicSurplusTierTotals().tier2CumAmount, self.basicSurplusTierTotals().tier2BasicSurplusAmount, self.inv);
                loveService.calculateTier('Tier 3 Liquidity', self.basicSurplusTierTotals().tier3Amount, self.basicSurplusTierTotals().tier3CumAmount, self.basicSurplusTierTotals().tier3BasicSurplusAmount, self.inv);
                loveService.calculateTier('Other Liquidity', self.basicSurplusTierTotals().otherLiqAmount, self.basicSurplusTierTotals().otherLiqCumAmount, 0, self.inv);
            }

            this.setAllDetailTotals = function () {
                for (var i = 0; i < self.inv().inventoryLiquidityResourcesTier().length; i++) {
                    for (var z = 0; z < self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail().length; z++) {
                        self.updateAvailableFields(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z]);

                        //now update totaling becuase we gots to
                        loveService.calculateTierDetails(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z], self.inv);
                    }
                }
            }


            this.basicSurplusTierTotals = ko.observableArray();
            this.basicSurplusFields = ko.observableArray();
            this.fundingCapaciy = ko.observable();

            this.getBasicSurplusFields = function () {
                globalContext.getInventoryLiquidityResourcesFields(self.basicSurplusFields, self.inv().basicSurplusId(), self.inv().institutionDatabaseName());
                globalContext.getInventoryLiquidityResourcesTierTotals(self.basicSurplusTierTotals, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).then(function () { self.setTierTotals() });
                globalContext.getInventoryOfLiquidityResourcesFundingCapacity(self.fundingCapaciy, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).then(function () { self.calcFundingTable(); self.calcFinalFundingTable() });
            }

            this.calcFundingTable = function () {
                loveService.calculateFundingCapacity(self.fundingCapaciy(), self.inv().inventoryLiquidityResourcesWholesaleFunding, self.inv);
            }

            this.calcFinalFundingTable = function () {
                loveService.calculateFinalFundingTable(self.fundingCapaciy(), self.inv().inventoryLiquidityResourcesWholesaleFunding, self.inv);
            }


            this.sortHistoricalDateOffSets = function () {
                self.sortedHistoricalOffSets(self.inv().inventoryLiquidityResourcesDateOffsets().sort(function (a, b) { return a.priority() - b.priority(); }));
            };

            this.removeHistoricalDateOffSet = function (offSet) {

                var msg = 'Delete offset "' + offSet.offset() + '" ?';
                var title = 'Confirm Delete';
                return app.showMessage(msg, title, ['No', 'Yes'])
                    .then(confirmDelete);

                function confirmDelete(selectedOption) {
                    if (selectedOption === 'Yes') {
                        offSet.entityAspect.setDeleted();
                        self.inv().inventoryLiquidityResourcesDateOffsets().forEach(function (anEveScenarioType, index) {
                            anEveScenarioType.priority(index);
                        });
                        self.sortHistoricalDateOffSets();

                    }
                }

            };

            this.addHistoricalDateOffSet = function () {
                var newCoreFundOffSet = globalContext.createInventoryLiquidityResourcesDateOffset();

                newCoreFundOffSet.offset(0);
                newCoreFundOffSet.priority(self.inv().inventoryLiquidityResourcesDateOffsets().length);
                self.inv().inventoryLiquidityResourcesDateOffsets().push(newCoreFundOffSet);

                self.updateOffsetFields(newCoreFundOffSet);

                self.sortHistoricalDateOffSets();
            };

            this.updateOffsetFields = function (offSet) {
                //find offset among policies and see what we can find
                if (self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offset()]['asOfDateDescript']] != undefined) {
                    offSet.basicSurplusAdminId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offset()]['asOfDateDescript']][0]["bsId"]));
                }
            }

            this.offsetChange = function () {
                self.updateOffsetFields(this);
            }

            this.restoreDefaultCheck = function () {
                if (!this.override()) {
                    self.updateOffsetFields(this);
                }

                return true;
            }

            this.resetOnInstChange = function () {
                for (var i = 0; i < self.inv().inventoryLiquidityResourcesDateOffsets().length; i++) {
                    self.inv().inventoryLiquidityResourcesDateOffsets()[i].override(false);
                    self.updateOffsetFields(self.inv().inventoryLiquidityResourcesDateOffsets()[i]);
                }
            }

            this.wholeSaleSubs = [];

            this.basicSurplusAdminOffsets = ko.observableArray();
            this.policyOffsets = ko.observableArray();
            this.sortedHistoricalOffSets = ko.observableArray();

            this.subWholesale = function (row) {
                self.wholeSaleSubs.push(
                    row.overide.subscribe(function (newValue) {
                        self.calcFundingTable();
                        self.calcFinalFundingTable();
                    }));

                self.wholeSaleSubs.push(
                    row.policy.subscribe(function (newValue) {
                        self.calcFundingTable();
                        self.calcFinalFundingTable();
                        console.log(newValue);
                        console.log(this);
                        //Onky in olicy we calculate percentage regardless of  override or now
                        loveService.calculateFundingPercentage({ polLimit: newValue }, self.fundingCapaciy().assLiabs[0], row);
                    }));

                self.wholeSaleSubs.push(
                    row.percentageType.subscribe(function (newValue) {
                        self.calcFundingTable();
                        self.calcFinalFundingTable();
                        //Onky in olicy we calculate percentage regardless of  override or now
                        loveService.calculateFundingPercentage({ polLimit: row.policy() }, self.fundingCapaciy().assLiabs[0], row);
                    }));

                self.wholeSaleSubs.push(
                    row.outstanding.subscribe(function (newValue) {
                        self.calcFundingTable();
                        self.calcFinalFundingTable();
                    }));

                self.wholeSaleSubs.push(
                    row.amountPerPolicy.subscribe(function (newValue) {
                        self.calcFundingTable();
                        self.calcFinalFundingTable();
                    }));

                self.wholeSaleSubs.push(
                    row.availableFunding.subscribe(function (newValue) {
                        self.calcFinalFundingTable();
                    }));

                self.wholeSaleSubs.push(
                    row.availableFundingPerPolicy.subscribe(function (newValue) {
                        self.calcFinalFundingTable();
                    }));

                self.wholeSaleSubs.push(
                    row.availableFundingPerTotalWholesalePolicy.subscribe(function (newValue) {
                        self.calcFinalFundingTable();
                    }));

                
            }


            this.balLabelOverrideClicked = function (data, name) {
                if (!self.inv().overrideBalanceSheetLabel() && data.inventoryLiquidityResourcesTier().name() == "On Balance Sheet Liquidity") {
                    console.log('caillllinggg');
                    name('Liquid Assets + Other Investments');
                }
            }

            this.resetBalSheetValue = function (data) {

                console.log(data);
                loveService.calculateTierDetails(data, self.inv);
            }

            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage); 
                global.loadingReport(true);
                return globalContext.getAvailableBanks(self.institutions).then(function () {
                    return globalContext.getPolicyOffsets(self.policyOffsets, self.inv().institutionDatabaseName()).then(function () {
                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.inv().institutionDatabaseName()).then(function () {
                            return globalContext.getBasicSurplusAdminsByAsOfDate(self.availBasicSurplus, self.inv().institutionDatabaseName(), self.asOfDateOffsets()[self.inv().asOfDateOffset()].asOfDateDescript).then(function () {

                                self.inv().asOfDateOffset(0);
                                self.setDefaultBasicSurplus();

                                return globalContext.getInventoryLiquidityResourcesFields(self.basicSurplusFields, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).fin(function () {
                                    return globalContext.getInventoryLiquidityResourcesTierTotals(self.basicSurplusTierTotals, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).fin(function () {
                                        return globalContext.getInventoryOfLiquidityResourcesFundingCapacity(self.fundingCapaciy, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).fin(function () {
                                            return globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, self.inv().institutionDatabaseName()).then(function () {

                                                //if new do all of this stuff
                                                if (self.inv().id() < 0) {
                                                    //if new create the four static tiers
                                                    self.addTierWithDefault('Tier 1 Liquidity');
                                                    self.addTierWithDefault('Tier 2 Liquidity');
                                                    self.addTierWithDefault('Tier 3 Liquidity');
                                                    self.addTierWithDefault('Other Liquidity');

                                                    self.addTierWithDefault('On Balance Sheet Liquidity');
                                                    //Set priioty of this guy becase of customs this need to be on bottom
                                                    self.inv().inventoryLiquidityResourcesTier()[self.inv().inventoryLiquidityResourcesTier().length - 1].priority(100);
                                                    self.inv().inventoryLiquidityResourcesTier()[self.inv().inventoryLiquidityResourcesTier().length - 1].name('On Balance Sheet Liquidity');

                                                    //Also add first detail 
                                                    self.addDetailWithParam(self.inv().inventoryLiquidityResourcesTier()[self.inv().inventoryLiquidityResourcesTier().length - 1], "Liquid Assets + Other Investments");




                                                    //Now vreate the the four whole sale funding stuff
                                                    self.createWholesaleFunding("FHLB", 0);
                                                    self.createWholesaleFunding("Brokered", 1);
                                                    self.createWholesaleFunding("Other Wholesale", 2);
                                                    self.createWholesaleFunding("Total Wholesale", 3);

                                                    //Defaul all includes to true 
                                                    self.inv().includePolicy(true);
                                                    self.inv().includeVol(true);
                                                    self.inv().includeBalSheet(true);
                                                } else {//we can only do this if it's not new
                                                    self.inv().inventoryLiquidityResourcesDateOffsets().forEach(function (item , ind, arr) {
                                                        self.updateOffsetFields(item);
                                                    });
                                                }


                                                self.sortHistoricalDateOffSets();
                                                //Set default basic surplus now that everything is loaded

                                                self.setDefaultBasicSurplus();

                                                //now that is et basic surplus i can retrieve the basicSurplusFields
                                                self.setTierTotals();
                                                self.calcFundingTable();
                                                self.calcFinalFundingTable();

                                                //AsOfDateOffset Handling (Single-Institution Template)
                                                self.instSub = self.inv().institutionDatabaseName.subscribe(function (newValue) {
                                                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                                                    globalContext.getBasicSurplusAdminOffsets(self.basicSurplusAdminOffsets, newValue);
                                                    globalContext.getPolicyOffsets(self.policyOffsets, newValue);
                                                    globalContext.getBasicSurplusAdminsByAsOfDate(self.availBasicSurplus, self.inv().institutionDatabaseName(), self.asOfDateOffsets()[self.inv().asOfDateOffset()].asOfDateDescript);
                                                    globalContext.getPolicyOffsets(self.policyOffsets, self.inv().institutionDatabaseName()).then(function () {

                                                        self.inv().overrideBasicSurplus(false);
                                                        self.setDefaultBasicSurplus();
                                                    });
                                                });

                                                //Date offset handling
                                                self.dateOffsetSub = self.inv().asOfDateOffset.subscribe(function (newValue) {
                                                    globalContext.getBasicSurplusAdminsByAsOfDate(self.availBasicSurplus, self.inv().institutionDatabaseName(), self.asOfDateOffsets()[self.inv().asOfDateOffset()].asOfDateDescript).then(function () {
                                                        self.setDefaultBasicSurplus();
                                                    });
                                                });

                                                self.overrideBasicSurplus = self.inv().overrideBasicSurplus.subscribe(function (newValue) {
                                                    self.setDefaultBasicSurplus();
                                                });

                                                self.basicSurplusSub = self.inv().basicSurplusId.subscribe(function (newValue) {
                                                    self.getBasicSurplusFields();
                                                });

                                                //First sort the tiers
                                                self.sortTiers();

                                                //Loop through all inventoryLiquidityResourcesTier
                                                for (var i = 0; i < self.inv().inventoryLiquidityResourcesTier().length; i++) {
                                                    self.inv().inventoryLiquidityResourcesTier()[i].sortedDetails = ko.observableArray();
                                                    self.sortDetails(self.inv().inventoryLiquidityResourcesTier()[i]);

                                                    for (var z = 0; z < self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail().length; z++) {
                                                        //Load possivle tiers and based off of vbalues disable them as available
                                                        self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].availableDetails = ko.observableArray();

                                                        self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].sortedDetails = ko.observableArray();

                                                        self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].upSections = ko.observableArray();
                                                        self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].downSections = ko.observableArray();

                                                        self.updateAvailableFields(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z]);

                                                        //now update totaling becuase we gots to
                                                        loveService.calculateTierDetails(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z], self.inv);
                                                    }
                                                }

                                                //To do go through and update tier totals
                                                self.setTierTotals();
                                                self.sortWholesale();
                                                if (self.inv().id() > 0) {
                                                    globalContext.saveChangesQuiet();
                                                }

                                                //Subscrive to all the fields ina ll the rows in wholesale funding
                                                for (var z = 0; z < self.inv().inventoryLiquidityResourcesWholesaleFunding().length; z++) {
                                                    var row = self.inv().inventoryLiquidityResourcesWholesaleFunding()[z];
                                                    self.subWholesale(row);
                                                   
                                                }
                                                global.loadingReport(false);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            };

            this.createWholesaleFunding = function (name, priority) {
                var lr = globalContext.createInventoryLiquidityResourcesWholesaleFunding();
                lr.priority(priority);
                lr.name(name);
                if (lr.name() == 'Other Wholesale') {
                    lr.policy(-999);
                    lr.percentageType('$ Amount');
                }

                self.inv().inventoryLiquidityResourcesWholesaleFunding.push(lr);
                self.sortWholesale();
            }

            this.detached = function (view, parent) {
                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub.dispose();
                self.dateOffsetSub.dispose();
                self.overrideBasicSurplus.dispose();
                self.basicSurplusSub.dispose();

                for (var z = 0; z < self.wholeSaleSubs.length; z++) {
                    self.wholeSaleSubs[z].dispose();
                }
            };

            this.sortableAfterMove = function (arg) {
                arg.sourceParent().forEach(function (eveScenarioType, index) {
                    eveScenarioType.priority(index);
                });
            };

        };

        return invVm;
    });
