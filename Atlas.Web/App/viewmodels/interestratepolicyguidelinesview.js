﻿define([
    'services/globalcontext',
    'durandal/app',
    'scripts/moment',
    'services/charting',
    'services/global'
],
function (globalContext, app, moment, charting, global) {
    var vm = function (id) {
        var self = this;
        var curPage = "InterestRatePolicyGuidelinesView";
        //Vars
        this.thisType = ko.observable();
        this.policies = {};
        this.colors = ['#80FFFF',
            '#009933', '#2BAA2B',
            '#80CC1A', '#D5EE09',
            '#FFFF00', '#FFEA00',
            '#FFC000', '#FF7F00',
            '#FF4000', '#FF0000',
            '#FFFFFF'];
        this.chartDates = [];
        this.nIIComparativeTypes = ko.observableArray([
            { key: "year1Year1", name: "Year 1 to Year 1 Base" },
            { key: "year2Year1", name: "Year 2 to Year 1 Base" },
            { key: "year2Year2", name: "Year 2 to Year 2 Base" },
            { key: "24Month", name: "24 Month % Change from Base" }
        ]);
        this.nIIScenarioTypeLabels = ko.observableArray([
            { key: "core", name: "Core" },
            { key: "alternative", name: "Alternative" },
            { key: "shock", name: "Shock" },
            { key: "custom", name: "Custom" },
            { key: "none", name: "None" }
        ]);
        this.eVEComparativeTypes = ko.observableArray([
            { key: "postShock", name: "Post-Shock EVE/NEV Ratio" },
            { key: "bpChange", name: "BP Change in EVE/NEV Ratio" },
            { key: "eveChange", name: "EVE/NEV % Change from 0 Shock" }
        ]);

        //Sorting
        this.sortTypes = function () {
            self.sortedDates(self.sortedDates().sort(function (a, b) {
                return a.priority - b.priority;
            }));
            self.sortedNIIComparatives(self.sortedNIIComparatives().sort(function (a, b) {
                return a.priority - b.priority;
            }));
            self.sortedEVEComparatives(self.sortedEVEComparatives().sort(function (a, b) {
                return a.priority - b.priority;
            }));
            self.sortedNIIComparatives().forEach(function (type) {
                type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                    return a.priority - b.priority;
                }));
                type.sortedTypes().forEach(function (type) {
                    type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                        return a.priority - b.priority;
                    }));
                });
            });
            self.sortedEVEComparatives().forEach(function (type) {
                type.sortedTypes(type.sortedTypes().sort(function (a, b) {
                    return a.priority - b.priority;
                }));
            });
        };

        //Data Handling
        this.activate = function () {
            global.loadingReport(true);
            //Pre-Setup
            return globalContext.getInterestRatePolicyGuidelinesViewDataById(id, self.thisType).then(function () {
                var thisPolicy;
                if (self.thisType().warnings.length > 0) {
                    globalContext.reportErrors(self.thisType().errors, self.thisType().warnings);
                }

                //Sorting
                self.sortedDates = ko.observableArray(self.thisType().interestRatePolicyGuidelines.dates);
                self.sortedNIIComparatives = ko.observableArray(self.thisType().interestRatePolicyGuidelines.nIIComparatives);
                self.sortedEVEComparatives = ko.observableArray(self.thisType().interestRatePolicyGuidelines.eVEComparatives);
                self.thisType().interestRatePolicyGuidelines.nIIComparatives.forEach(function (type) {
                    type.sortedTypes = ko.observableArray(type.scenarioTypes);
                    type.scenarioTypes.forEach(function (type) {
                        type.sortedTypes = ko.observableArray(type.scenarios);
                    });
                });
                self.thisType().interestRatePolicyGuidelines.eVEComparatives.forEach(function (type) {
                    type.sortedTypes = ko.observableArray(type.scenarios);
                });
                self.sortTypes();

                //Reformat dates (for charts)
                self.thisType().dates.forEach(function (date) {
                    self.chartDates.push(moment(date).format('MM/YY'));
                });

                //Map out policy names
                self.nIIComparativeTypes().forEach(function (type) {
                    self.nIIComparativeTypes[type.key] = type.name;
                });
                self.nIIScenarioTypeLabels().forEach(function (type) {
                    self.nIIScenarioTypeLabels[type.key] = type.name;
                });
                self.eVEComparativeTypes().forEach(function (type) {
                    self.eVEComparativeTypes[type.key] = type.name;
                });

                //Map out the policy data
                self.thisType().policies.forEach(function (policy) {
                    if (policy) {
                        self.policies["Policy" + policy.id] = { niiPolicies: {}, evePolicies: {} };
                        policy.nIIPolicyParents.forEach(function (type) {
                            self.policies["Policy" + policy.id].niiPolicies[type.shortName] = type; //NIIPolicyParent
                            type.nIIPolicies.forEach(function (subType) {
                                type["Scen" + subType.scenarioTypeId] = subType; //NIIPolicy
                            });
                        });
                        policy.evePolicyParents.forEach(function (type) {
                            self.policies["Policy" + policy.id].evePolicies[type.shortName] = type; //EVEPolicyParent
                            type.evePolicies.forEach(function (subType) {
                                type["Scen" + subType.scenarioTypeId] = subType; //EVEPolicy
                            });
                        });
                    }
                });

                //Get the real policy data
                //Comparative
                self.sortedNIIComparatives().forEach(function (type) {
                    type.longName = self.nIIComparativeTypes[type.type];

                    //Scenario Type
                    type.sortedTypes().forEach(function (subScenType) {
                        subScenType.longName = self.nIIScenarioTypeLabels[subScenType.type];

                        //Scenario
                        subScenType.sortedTypes().forEach(function (subType) {
                            subType.policyData = ko.observableArray();
                            self.thisType().policies.forEach(function (policy) {
                                //Note: Policy may validly be "null"
                                //Changed 6/29/2015 by JG. If a scenarioType has a 0 currentRatio it's not being shown on the Policy config screen, same logic should apply here.
                                if (policy
                                && self.policies["Policy" + policy.id]
                                && self.policies["Policy" + policy.id].niiPolicies[type.type]
                                && self.policies["Policy" + policy.id].niiPolicies[type.type]["Scen" + subType.scenarioTypeId]
                                && self.policies["Policy" + policy.id].niiPolicies[type.type]["Scen" + subType.scenarioTypeId].currentRatio != 0) {
                                    thisPolicy = self.policies["Policy" + policy.id].niiPolicies[type.type]["Scen" + subType.scenarioTypeId];
                                    subType.policyData.push({ policyLimit: thisPolicy.policyLimit, currentRatio: thisPolicy.currentRatio, riskColor: self.colors[thisPolicy.riskAssessment] });
                                }
                                //else
                                //    subType.policyData.push({ policyLimit: 0, currentRatio: 0, riskColor: self.colors[11] });
                            });
                        });
                    });
                });

                //Comparative
                self.sortedEVEComparatives().forEach(function (type) {
                    type.longName = self.eVEComparativeTypes[type.type];

                    //Scenario
                    type.sortedTypes().forEach(function (subType) {
                        subType.policyData = ko.observableArray();
                        self.thisType().policies.forEach(function (policy) {
                            //Note: Policy may validly be "null"
                            if (policy
                            && self.policies["Policy" + policy.id]
                            && self.policies["Policy" + policy.id].evePolicies[type.type]
                            && self.policies["Policy" + policy.id].evePolicies[type.type]["Scen" + subType.scenarioTypeId]) {
                                thisPolicy = self.policies["Policy" + policy.id].evePolicies[type.type]["Scen" + subType.scenarioTypeId];
                                subType.policyData.push({ policyLimit: thisPolicy.policyLimit, currentRatio: thisPolicy.currentRatio, riskColor: self.colors[thisPolicy.riskAssessment] });
                            }
                            else
                                subType.policyData.push({ policyLimit: 0, currentRatio: 0,  riskColor: self.colors[11] });
                        });
                    });
                });
            });
        };

        this.compositionComplete = function () {
            var series;
            var seriesData;

            //Draw graphs
            //Comparative
            self.sortedNIIComparatives().forEach(function (type) {
                series = [];

                //Scenario Type
                type.sortedTypes().forEach(function (subScenType) {
                    //Scenario
                    subScenType.sortedTypes().forEach(function (subType) {
                        if (subType.graph) {
                            seriesData = [];
                            subType.policyData().forEach(function (data) {
                                seriesData.push(data.currentRatio);
                            });

                            if (seriesData.length > 0) {
                                series.push({
                                    name: subType.scenarioType.name,
                                    data: seriesData
                                });
                            }
                        }
                    });
                });

                if (series.length > 0) {
                    $('#' + type.type).highcharts(charting.interestRatePolicyGuidelinesChart(type.longName,
                        self.chartDates,
                        //['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                        series
                        //[{
                        //    name: 'Tokyo',
                        //    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5 /*, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6 */]
                        //}, {
                        //    name: 'New York',
                        //    data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0 /*, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5 */]
                        //}, {
                        //    name: 'Berlin',
                        //    data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0 /*, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0 */]
                        //}, {
                        //    name: 'London',
                        //    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2 /*, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8 */]
                        //}]
                    ));
                }
            });

            //Comparative
            self.sortedEVEComparatives().forEach(function (type) {
                series = [];

                //Scenario
                type.sortedTypes().forEach(function (subType) {
                    if (subType.graph) {
                        seriesData = [];
                        subType.policyData().forEach(function (data) {
                            seriesData.push(data.currentRatio);
                        });
                        series.push({
                            name: subType.scenarioType.name,
                            data: seriesData
                        });
                    }
                });

                if (series.length > 0) {
                    $('#' + type.type).highcharts(charting.interestRatePolicyGuidelinesChart(type.longName, self.chartDates, series));
                }
            });
            $("interest-rate-view").height($("interest-rate-view").height());
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage); 
            }
            else {
                hiqPdfConverter.startConversion();
            }
        };

        return this;
    };

    return vm;
});
