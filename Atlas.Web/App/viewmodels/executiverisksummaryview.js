﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var execVm = function (id) {
        var self = this;
        var curPage = "ExecRiskSummaryView";
        this.execViewData = ko.observable();

        this.higherVal = function (int1, int2) {
            if (int1 > int2) {
                return int1;
            }
            else {
                return int2;
            }
        }

      

        this.generateBannerRow = function (name, ra, showPols) {
            var row = "<tr style='text-align: left !important;' class='bannerRow'>";

            var colSpanVal = 2 + this.execViewData().histDates.length;
            if (this.execViewData().showPolicies) {
                colSpanVal += 1;
            }

            colSpanVal = colSpanVal * 2;

            row += "<td colspan='" + colSpanVal + "'>";

            //Add image if we are showing it
            if (showPols && this.execViewData().showPolicies && ra != 0) {
                row += "<img class='execSumGauge' height='32' width='65' src= '../Content/images/risk/" + ra + ".jpg' />";
            }

            row += name;

            if (numeral(ra).value() > 0) {
                row += " - " + this.riskLabel(ra);
            }

            row += "</td></tr>";

            return row;
        }

        this.generateFirstSectionSecondaryHeaderCells = function () {
            var cells = "";

            if (this.execViewData().showPolicies) {
                cells += "<td class='headerCell policy'>Policy</td>";
            }

            var lastYear = moment(this.execViewData().histDates[0]).year();

            for (var a = 0; a < this.execViewData().histDates.length; a++) {
                if (this.execViewData().asofDate && a == 0) {
                    cells += "<td class='headerCell'></td>";
                }
                else {
                    var labelVal = moment(this.execViewData().histDates[a]).format("MMM");
                    //if not first and is every other then format with year above it

                    if (moment(this.execViewData().histDates[a]).year() != lastYear || a == 0) {
                        //Add year label
                        labelVal = moment(this.execViewData().histDates[a]).format("YYYY") + "&nbsp;" + labelVal;
                        //set lat year var so that if year for whatever reasonc hanges again comparing to the right thing
                        lastYear = moment(this.execViewData().histDates[a]).year();
                    }
                    cells += "<td class='headerCell'>" + labelVal + "</td>";
                }
            }

            return cells;
        }

        this.generateEmptyCells = function () {
            var cells = "";
            cells += "<td class='empty'></td>";
            if (this.execViewData().showPolicies) {
                cells += "<td class='empty'></td>";
            }

            for (var a = 0; a < this.execViewData().histDates.length; a++) {
                cells += "<td class='empty'></td>";
            }

            return cells;
        }

        this.generateEmptyCellsSecondaryHeader = function () {
            var cells = "";
            if (this.execViewData().showPolicies) {
                cells += "<td class='empty'></td>";
            }

            for (var a = 0; a < this.execViewData().histDates.length; a++) {
                cells += "<td class='empty'></td>";
            }

            return cells;
        }

        this.generatePrimaryHeaderRow = function (leftSection, rightSection) {
            var row = "<tr class='primary'>";

            var colSpan = 1;
            if (this.execViewData().showPolicies) {
                colSpan = 2;
            }

            if (leftSection != null) {
                if (leftSection.hideLabel) {
                    row += "<td colspan='" + colSpan + "'>&nbsp;</td>";
                }
                else {
                    row += "<td colspan='" + colSpan + "'>" + leftSection.name + "</td>";
                }

                for (var a = 0; a < this.execViewData().histDates.length; a++) {
                    row += "<td class='empty'></td>";
                }
            }
            else {
                row += this.generateEmptyCells();
            }

            if (rightSection != null) {
                if (rightSection.hideLabel) {
                    row += "<td colspan='" + colSpan + "'>&nbsp;</td>";
                }
                else {
                    row += "<td style='text-align:left;' colspan='" + colSpan + "'>" + rightSection.name + "</td>";
                }
                for (var a = 0; a < this.execViewData().histDates.length; a++) {
                    row += "<td class='empty'></td>";
                }
            }
            else {
                row += this.generateEmptyCells();
            }

            return row;
        }

        this.generateSecondaryHeaderRow = function (leftSection, rightSection, sectionIdx) {
            var row = "<tr class='secondaryHeader'>";

            if (leftSection != null) {
                if (leftSection.name == 'Core Funding Utilization' || leftSection.hideLabel) {
                    leftSection.name = "&nbsp;";
                }

                row += "<td>" + leftSection.name + "</td>";

                //Only spit out other headers if first in report section
                if (sectionIdx == 0 && (leftSection.hideLabel == false || rightSection.hideLabel == false)) {
                    row += this.generateFirstSectionSecondaryHeaderCells();
                }
                else {
                    row += this.generateEmptyCellsSecondaryHeader();
                }
            }
            else {
                row += this.generateEmptyCells();
            }

            if (rightSection != null) {
                if (rightSection.name == 'Core Funding Utilization' || rightSection.hideLabel) {
                    rightSection.name = "&nbsp;";
                }

                row += "<td class='headerCell2'>" + rightSection.name + "</td>";

                // only spit out other headers if first in report section
                if (sectionIdx == 0 && ((leftSection != null && leftSection.hideLabel == false) || (rightSection != null && rightSection.hideLabel == false))) {
                    row += this.generateFirstSectionSecondaryHeaderCells();
                }
                else {
                    row += this.generateEmptyCellsSecondaryHeader();
                }
            }
            else {
                row += this.generateEmptyCells();
            }

            row += "</tr>";

            if (((leftSection != null && leftSection.hideLabel == true) && (rightSection != null && rightSection.hideLabel == true))) {
                row = '';
            }

            return row;
        }

        this.generateDetailSide = function (det, length, idx, showPols) {
            var cells = "";

            if (det.format == "$0,0") {
                det.current = det.current / 1000;
                det.name = det.name + " ($ MILLIONS)";
            }

            cells += "<td>" + det.name + "</td>";

            var hasPol = false;
            if (this.execViewData().showPolicies) {
                if (showPols && det.policy != "" && det.policy > -999) {
                    hasPol = true;
                    if (det.format == "BP") {
                        cells += "<td >" + numeral(det.policy).format('0,0') + "BP</td>";
                    }
                    else {
                        cells += "<td class='policy'>" + numeral(det.policy / (det.format == '$0,0' ? 1000 : (det.format == '0,0' ? 1 : 100))).format(det.format) + "</td>";
                    }
                }
                else {
                    hasPol = false;
                    cells += "<td class='policy'></td>";
                }
            }

            var currentHighlight = "";

            if (idx == length) {
                currentHighlight = "currentRounded";
            }
            else {
                currentHighlight = "currentSquare";
            }

            var riskPolClass = "";

            if (this.execViewData().showRA) {
                if (showPols && det.risk != "" && det.risk > 0 && this.execViewData().showPolicies && hasPol) {
                    riskPolClass = "solidRisk" + det.risk;
                }
                else if (det.risk != "" && det.risk > 0) {
                    riskPolClass = "borderRisk" + det.risk;
                }
            }

            if (det.current != "" && det.current != null && det.current != -999) {
                if (det.format == "BP") {
                    cells += "<td class='" + currentHighlight + "'><span class='" + riskPolClass + "'>" + numeral(det.current * 10000).format('0,0') + "BP</span></td>";
                }
                else {
                    cells += "<td class='" + currentHighlight + "'><span class='" + riskPolClass + "'>" + numeral(det.current).format(det.format) + "</span></td>";
                }

            }
            else {
                cells += "<td class='" + currentHighlight + "'><span></span></td>";
            }

            for (var a = 1; a < this.execViewData().histDates.length; a++) {

                if (det.format == "$0,0") {
                    det["hist" + a] = det["hist" + a] / 1000;
                }

                if (det["hist" + a] != ""  && det["hist" + a] != null && det["hist" + a] != -999) {
                    if (det.format == "BP") {
                        cells += "<td class='normal'>" + numeral(det["hist" + a] * 10000).format('0,0') + "BP</td>";
                    }
                    else {
                        cells += "<td class='normal'>" + numeral(det["hist" + a]).format(det.format) + "</td>";
                    }

                } else {
                    cells += "<td class='normal'></td>";
                }
            }

            return cells;
        }

        this.generateDetailRow = function (leftDet, rightDet, leftLength, rightLength, idx, showLeftPols, showRightPols) {
            var row = "<tr class='detRow'>";

            if (leftDet != null) {
                row += this.generateDetailSide(leftDet, leftLength, idx, showLeftPols);
            }
            else {
                row += this.generateEmptyCells();
            }

            if (rightDet != null) {
                row += this.generateDetailSide(rightDet, rightLength, idx, showRightPols);
            }
            else {
                row += this.generateEmptyCells();
            }

            row += "</tr>";

            return row;
        }

        this.addLiqMasterSection = function () {
            $('#execSummary').append('<tbody id="liqTable" ></tbody><tr style="height:5px;"><td colspan="16"></td></tr>')
        }

        this.addCapMasterSection = function () {
            $('#execSummary').append('<tbody id="capTable" ></tbody>')
        }

        this.addIRRMasterSection = function () {
            $('#execSummary').append('<tbody id="irrTable" ></tbody><tr style="height:5px;"><td colspan="16"></td></tr>')
        }

        this.compositionComplete = function () {
            //check order of main sections and add main sections before adding to them
            //NOTE: Not the best way to do this but since this is mostly static did not want to waste time on it

            if (this.execViewData().liqOrder == 0) {
                this.addLiqMasterSection();
            }
            else if (this.execViewData().capOrder == 0) {
                this.addCapMasterSection();
            }
            else if (this.execViewData().iRROrder == 0) {
                this.addIRRMasterSection();
            }

            if (this.execViewData().liqOrder == 1) {
                this.addLiqMasterSection();
            }
            else if (this.execViewData().capOrder == 1) {
                this.addCapMasterSection();
            }
            else if (this.execViewData().iRROrder == 1) {
                this.addIRRMasterSection();
            }

            if (this.execViewData().liqOrder == 2) {
                this.addLiqMasterSection();
            }
            else if (this.execViewData().capOrder == 2) {
                this.addCapMasterSection();
            }
            else if (this.execViewData().iRROrder == 2) {
                this.addIRRMasterSection();
            }

            // loop through the highest value of left or right side
            var size = this.higherVal(this.execViewData().leftLiqSection.length, this.execViewData().rightLiqSection.length);

            if (!this.execViewData().hideLiq) {
                $("#liqTable").addClass("risk-" + this.execViewData().liqRA);
                $("#liqTable").append(this.generateBannerRow("LIQUIDITY", this.execViewData().liqRA, this.execViewData().showBSPolicies));

                // now loop through and do one table row per side if one side does not exist label cells as empty
                for (var i = 0; i < size; i++) {
                    //first in loop need to generate section headers
                    var leftSection = null;
                    var rightSection = null;

                    //check left and then do it and then check right and do it
                    if (this.execViewData().leftLiqSection.length - 1 >= i) {
                        leftSection = this.execViewData().leftLiqSection[i];
                    }

                    if (this.execViewData().rightLiqSection.length - 1 >= i) {
                        rightSection = this.execViewData().rightLiqSection[i];
                    }

                    $("#liqTable").append(this.generateSecondaryHeaderRow(leftSection, rightSection, i, false, false));

                    //Now get higest value of chldren under each section
                    var childSize = this.higherVal((leftSection == null ? 0 : leftSection.table.length), (rightSection == null ? 0 : rightSection.table.length));

                    for (var c = 0; c < childSize; c++) {
                        var leftDet = null;
                        var rightDet = null;

                        var leftLength = 0;
                        var rightLength = 0;

                        var leftLength = 0;
                        if (this.execViewData().leftLiqSection.length - 1 >= i) {
                            leftLength = this.execViewData().leftLiqSection[i].table.length - 1;
                            if (this.execViewData().leftLiqSection[i].table.length - 1 >= c) {
                                leftDet = this.execViewData().leftLiqSection[i].table[c];
                            }
                        }

                        if (this.execViewData().rightLiqSection.length - 1 >= i) {
                            rightLength = this.execViewData().rightLiqSection[i].table.length - 1;
                            if (this.execViewData().rightLiqSection[i].table.length - 1 >= c) {
                                rightDet = this.execViewData().rightLiqSection[i].table[c];
                            }
                        }

                        $("#liqTable").append(this.generateDetailRow(leftDet, rightDet, leftLength, rightLength, c, this.execViewData().showBSPolicies, this.execViewData().showBSPolicies));
                    }
                }
            }

            // loop through the highest value of left or right side
            size = this.higherVal(this.execViewData().leftIRRSection.length, this.execViewData().rightIRRSection.length);
            if (!this.execViewData().hideIRR) {
                $("#irrTable").addClass("risk-" + this.execViewData().irrRA);
                $("#irrTable").append(this.generateBannerRow("INTEREST RATE RISK ", this.execViewData().irrRA, this.execViewData().showIRRPolicies));

                //now loop through and do one table row per side if one side does not exist label cells as empty
                for (var i = 0; i < size; i++) {
                    //first in loop need to generate section headers
                    var leftSection = null;
                    var rightSection = null;

                    //check left and then do it and then check right and do it
                    if (this.execViewData().leftIRRSection.length - 1 >= i) {
                        leftSection = this.execViewData().leftIRRSection[i];
                    }

                    if (this.execViewData().rightIRRSection.length - 1 >= i) {
                        rightSection = this.execViewData().rightIRRSection[i];
                    }

                    $("#irrTable").append(this.generatePrimaryHeaderRow(leftSection, rightSection));

                    //Now get higest value of chldren under each section
                    var subSize = this.higherVal((leftSection == null ? 0 : leftSection.subSections.length), (rightSection == null ? 0 : rightSection.subSections.length));

                    for (var s = 0; s < subSize; s++) {
                        var leftSub = null;
                        var rightSub = null;

                        if (this.execViewData().leftIRRSection.length - 1 >= i) {

                            if (this.execViewData().leftIRRSection[i].subSections.length - 1 >= s) {
                                leftSub = this.execViewData().leftIRRSection[i].subSections[s];
                            }
                        }

                        if (this.execViewData().rightIRRSection.length - 1 >= i) {
                            if (this.execViewData().rightIRRSection[i].subSections.length - 1 >= s) {
                                rightSub = this.execViewData().rightIRRSection[i].subSections[s];
                            }
                        }

                        $("#irrTable").append(this.generateSecondaryHeaderRow(leftSub, rightSub, s));

                        var childSize = this.higherVal((leftSub == null ? 0 : leftSub.table.length), (rightSub == null ? 0 : rightSub.table.length));

                        for (var c = 0; c < childSize; c++) {
                            var leftDet = null;
                            var rightDet = null;

                            var leftLength = 0;
                            var rightLength = 0;

                            if (this.execViewData().leftIRRSection.length - 1 >= i) {
                                if (this.execViewData().leftIRRSection[i].subSections.length - 1 >= s) {
                                    leftLength = this.execViewData().leftIRRSection[i].subSections[s].table.length - 1;
                                    if (this.execViewData().leftIRRSection[i].subSections[s].table.length - 1 >= c) {
                                        leftDet = this.execViewData().leftIRRSection[i].subSections[s].table[c];
                                    }
                                }
                            }

                            if (this.execViewData().rightIRRSection.length - 1 >= i) {
                                if (this.execViewData().rightIRRSection[i].subSections.length - 1 >= s) {
                                    rightLength = this.execViewData().rightIRRSection[i].subSections[s].table.length - 1;
                                    if (this.execViewData().rightIRRSection[i].subSections[s].table.length - 1 >= c) {

                                        rightDet = this.execViewData().rightIRRSection[i].subSections[s].table[c];
                                    }
                                }
                            }

                            $("#irrTable").append(this.generateDetailRow(leftDet, rightDet, leftLength, rightLength, c, (leftSub != null ? leftSub.showPol : false), (rightSub != null ? rightSub.showPol : false)));
                        }
                    }
                }
            }

            //Loop through the highest value of left or right side
            size = this.higherVal(this.execViewData().leftCapSection.length, this.execViewData().rightCapSection.length);

            if (!this.execViewData().hideCap) {
                $("#capTable").addClass("risk-" + this.execViewData().capRA);
                $("#capTable").append(this.generateBannerRow("CAPITAL", this.execViewData().capRA, this.execViewData().showCapPolicies));

                if (size > 0) {
                    $("#capTable").append(this.generateSecondaryHeaderRow((this.execViewData().leftCapSection.length ? { name: "",hideLabel: false } : null), (this.execViewData().rightCapSection.length ? { name: "", hideLabel:false } : null), 0));
                }

                //now loop through and do one table row per side if one side does not exist label cells as empty
                for (var i = 0; i < size; i++) {
                    //first in loop need to generate section headers
                    var leftSection = null;
                    var rightSection = null;

                    //check left and then do it and then check right and do it
                    if (this.execViewData().leftCapSection.length - 1 >= i) {
                        leftSection = this.execViewData().leftCapSection[i];
                    }

                    if (this.execViewData().rightCapSection.length - 1 >= i) {
                        rightSection = this.execViewData().rightCapSection[i];
                    }

                    $("#capTable").append(this.generateDetailRow(leftSection, rightSection, this.execViewData().leftCapSection.length - 1, this.execViewData().rightCapSection.length - 1, i, this.execViewData().showCapPolicies, this.execViewData().showCapPolicies));
                }
            }

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage);  
            }
            else {

            }
        }

        this.riskLabel = function (ra) {
            switch (ra) {
                case 1:
                    return "LOW RISK";
                    break;
                case 2:
                    return "LOW-MODERATE RISK"
                    break;
                case 3:
                    return "MODERATE RISK";
                    break;
                case 4:
                    return "MODERATE-HIGH RISK"
                    break;
                case 5:
                    return "HIGH RISK";
                    break;
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getExecutiveSummaryViewDataById(id, self.execViewData).then(function () {

            });
        };
    };

    return execVm;
});