﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app',
    'services/liquidityservice',
    'services/charting',
    'services/global'
],
function (globalContext, global, app, loveService, charting, global) {
    var invVm = function (inv, rep) {
        var self = this;
        var curPage = "InventoryLiquidityResourcesView";
        this.inv = inv;

        this.sortedTiers = ko.observableArray();

        this.sortedWholesale = ko.observableArray();

        this.sortWholesale = function () {
            self.sortedWholesale(self.inv().inventoryLiquidityResourcesWholesaleFunding().sort(function (a, b) { return a.priority() - b.priority(); }));
        }

        this.sortTiers = function () {
            self.sortedTiers(self.inv().inventoryLiquidityResourcesTier().sort(function (a, b) { return a.priority() - b.priority(); }));
        }


        this.sortDetails = function (l) {
            l.sortedDetails(l.inventoryLiquidityResourcesTierDetail().sort(function (a, b) { return a.priority() - b.priority(); }));
        }

        this.thisReport = rep;

        this.cancel = function () {
        };

        this.availBasicSurplus = ko.observableArray();
        this.institutions = ko.observableArray();
        this.global = global;
        this.asOfDateOffsets = ko.observableArray();
        this.policyOffsets = ko.observableArray();

        this.setDefaultBasicSurplus = function () {
            var aod = self.asOfDateOffsets()[self.inv().asOfDateOffset()].asOfDateDescript;
 
            if (!self.inv().overrideBasicSurplus()){
                self.inv().basicSurplusId(self.policyOffsets()[0][aod][0].bsId);
            }
        }

        this.updateOffsetFields = function (offSet) {
            //find offset among policies and see what we can find
            if (self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offset()]['asOfDateDescript']] != undefined) {
                offSet.basicSurplusAdminId(parseInt(self.policyOffsets()[0][self.asOfDateOffsets()[offSet.offset()]['asOfDateDescript']][0]["bsId"]));
            }
        }


        this.sortUpandDown = function (l) {
            l.upSections(l.inventoryLiquidityResourcesTierDetailCollection().filter(function (item, arr, index) { return item.addNumber() }));
            l.downSections(l.inventoryLiquidityResourcesTierDetailCollection().filter(function (item, arr, index) { return !item.addNumber() }));     
        };

        this.updateAvailableFields = function (l) {

            //Always easier jsut ro reset it ehre
            loveService.setTierDetails(l.inventoryLiquidityResourcesTier().name(), l, self.basicSurplusFields);

            //Now need to sort the collection based off of plus minus sections     
            self.sortUpandDown(l);

        }

        this.setTierTotals = function () {
            //update each tier respectivlvt
            loveService.calculateTier('Tier 1 Liquidity', self.basicSurplusTierTotals().tier1Amount, self.basicSurplusTierTotals().tier1CumAmount, self.basicSurplusTierTotals().tier1BasicSurplusAmount, self.inv);
            loveService.calculateTier('Tier 2 Liquidity', self.basicSurplusTierTotals().tier2Amount, self.basicSurplusTierTotals().tier2CumAmount, self.basicSurplusTierTotals().tier2BasicSurplusAmount, self.inv);
            loveService.calculateTier('Tier 3 Liquidity', self.basicSurplusTierTotals().tier3Amount, self.basicSurplusTierTotals().tier3CumAmount, self.basicSurplusTierTotals().tier3BasicSurplusAmount, self.inv);
            loveService.calculateTier('Other Liquidity', self.basicSurplusTierTotals().otherLiqAmount, self.basicSurplusTierTotals().otherLiqCumAmount, 0, self.inv);
        }

        this.setAllDetailTotals = function () {
            for (var i = 0; i < self.inv().inventoryLiquidityResourcesTier().length; i++) {
                for (var z = 0; z < self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail().length; z++) {
                    self.updateAvailableFields(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z]);

                    //now update totaling becuase we gots to
                    loveService.calculateTierDetails(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z], self.inv);
                }
            }
        }

        this.basicSurplusTierTotals = ko.observableArray();
        this.basicSurplusFields = ko.observableArray();
        this.fundingCapacity = ko.observable();
        this.historicBasicSurpluses = ko.observable();

        this.getBasicSurplusFields = function () {
            globalContext.getInventoryLiquidityResourcesFields(self.basicSurplusFields, self.inv().basicSurplusId(), self.inv().institutionDatabaseName());
            globalContext.getInventoryLiquidityResourcesTierTotals(self.basicSurplusTierTotals, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).then(function () { self.setTierTotals() });
            globalContext.getInventoryOfLiquidityResourcesFundingCapacity(self.fundingCapacity, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).then(function () { self.calcFundingTable(); self.calcFinalFundingTable() });
        }

        this.calcFundingTable = function () {
            loveService.calculateFundingCapacity(self.fundingCapacity(), self.inv().inventoryLiquidityResourcesWholesaleFunding, self.inv);
        }

        this.calcFinalFundingTable = function () {
            loveService.calculateFinalFundingTable(self.fundingCapacity(), self.inv().inventoryLiquidityResourcesWholesaleFunding, self.inv);
        }

        this.buildSpacerRow = function (drawVolArea, idx, length, doNotDoLast) {
            //if the chart is on a 
            var row = '<tr class="spacerRow">';
            if (idx == length && !doNotDoLast) {
                row = '<tr class="spacerRowLast spacerRowSmall">';
            }


            row += '<td colspan="4" class="border-right-thick border-grey"></td>';
            row += '<td colspan="2" class="border-right-thick border-grey"></td>';
            row += '<td colspan="3" class="background-off-yellow"></td>';

            if (drawVolArea) {
                row += ' <td id="volText"  class="background-off-yellow border-right-thick border-bottom-thick border-grey" colspan="2" rowspan="' + self.includeCount + '" ></td>'
            }
            row += '</tr>';

            $('#tierTable > tbody').append(row);
        }

        this.buildDetailRow = function(detail, parentIdx, idx, totalLength){
            var color = charting.chartColorsPalette2[parentIdx];

            var styleLeft = "border-left: 2px solid " + color + ";";
            var styleRight = "border-right: 2px solid " + color + ";";
            var styleBottom = "border-bottom: 2px solid " + color + ";";

            //if we are at end of the details for the iter create styles that will put them on bottom and round the bottom right
            if (idx == totalLength) {
                //add border bottom to both left and right
                styleLeft += styleBottom;
                styleRight += styleBottom;

                //Add radius to style right
                styleRight += " border-bottom-right-radius: 10px;"

            }


            var row = '<tr class="detailRow">';

            row += '<td class="leftCell" style="' + styleLeft + '">' + detail.name() + '</td>';
            row += '<td class="rightCell" style="' + styleRight + '">' + numeral(detail.amount()).format('0,0') + '</td>';
            row += '<td colspan="2" class="border-right-thick border-grey"></td>';
            row += '<td colspan="2" class="border-right-thick border-grey"></td>';
            row += '<td colspan="3" class="background-off-yellow"></td>';
            row += '</tr>';
            $('#tierTable > tbody').append(row);

        }

        this.buildTierRow = function (tier, idx) {
            //Set color accordingly based off of index
            var color = charting.chartColorsPalette2[idx];
            var textColor = charting.chartColorsTextPalette2[idx];

            var tierName = tier.name();
            if (tier.name() == 'Custom') {
                tierName = tier.customName();
            }
            var row = '<tr class="tierRow" style="background-color: ' + color + '; color: ' + textColor + '">';
            //Name add index + 1 then the name
            row += '<td colspan="2" class="tierLabel">' + (idx + 1) + '. ' + tierName + '</td>';            
            row += '<td class="rightCellWithPad">' + numeral(tier.amount()).format('0,0') + '</td>';
            row += '<td class="rightCellWithPad border-right-thick border-grey">' + numeral(numeral(tier.amount()).value() / (numeral(self.fundingCapacity().assLiabs[0].assetTotal).value() / 1000)).format('0.0%'); + '</td>';
            row += '<td class="rightCellWithPad">' + numeral(tier.cumulativeAmount()).format('0,0') + '</td>';
            row += '<td class="rightCellWithPad border-right-thick border-grey">' + numeral(numeral(tier.cumulativeAmount()).value() / (numeral(self.fundingCapacity().assLiabs[0].assetTotal).value() / 1000)).format('0.0%'); + '</td>';

            if (idx <= 2) {
                row += '<td class="rightCellWithPad">' + numeral(tier.basicSurplusAmount()).format('0,0') + '</td>';
                row += '<td class="rightCellWithPad">' + numeral(numeral(tier.basicSurplusAmount()).value() / (numeral(self.fundingCapacity().assLiabs[0].assetTotal).value() / 1000)).format('0.0%'); + '</td>';
                row += '<td class="empty min-empty background-off-yellow"></td>';
            }
            else {
                row += '<td colspan="3" class="empty min-empty background-off-yellow"></td>';
            }
            //end row
            row += '</tr>';

            //If we are including policies need to do child row underneath previous row that is very thin
            if (self.inv().includePolicy() && idx <= 2) {
                var pol1ToUse = 'bsminLiqAssets';
                var pol2ToUse = 'bsmin';

                if (tier.name() == 'Tier 2 Liquidity') {
                    pol1ToUse = '';
                    pol2ToUse = 'bsminFHLB';
                }
                else if (tier.name() == 'Tier 3 Liquidity') {
                    pol1ToUse = '';
                    pol2ToUse = 'bsminBrokered';
                }

                row += '<tr class="policyRow"  style="background-color: ' + color + '; color: ' + textColor + '">';
                row += '<td colspan="2" class="empty"></td>';
                row += '<td colspan="2" class="border-right-thick border-grey">' + self.generatePolicyMinimumText(pol1ToUse) + ' </td>';
                row += '<td colspan="2" class="border-right-thick border-grey"></td>';


                row += '<td colspan="2">' + self.generatePolicyMinimumText(pol2ToUse) + ' </td>';
                row += '<td  class="background-off-yellow"></td>' //empty cell
                // if at tier 3 draw area for include vols
                if (idx == 2) {
                    //this has uniqie id becuase it needs to get generated regardless and at end will put in text if they have it checked off in config
                    row += ' <td id="volText" class="background-off-yellow border-right-thick border-bottom-thick border-grey" colspan="2" rowspan="' + self.includeCount + '" ></td>'
                }
                row += '</tr>';
            }

            $('#tierTable > tbody').append(row);
        }

        this.generatePolicyMinimumText = function (polShortName) {
            var idx = self.fundingCapacity().polValues.findIndex(function (el) { return el.shortName == polShortName });
            if (idx >= 0) {
                if (numeral(self.fundingCapacity().polValues[idx].policyLimit).value() != -999){
                    return "Policy Minimum: " + numeral(self.fundingCapacity().polValues[idx].policyLimit).format('0.0') + '%';
                }
                else {
                    return "";
                }
            }
            else {
                return "";
            }
 
        }

        var extraRowsNeeded = 0;
        var includeCount = 0;

        this.compositionComplete = function () {
            //one more save here for the wholesale funding policy changes that will come through if copying from AT
            globalContext.saveChangesQuiet().then(function () {
            


                //Always defaulted and will lawyas have at least 4 tiers so we know we only need first two
                var detailCount = self.sortedTiers()[0].sortedDetails().length + self.sortedTiers()[1].sortedDetails().length;
                detailCount += 6; //6 represensts the spacer rows and the actual tier rows for this report

                //If i am incouding policues add two more rows to that
                if (self.inv().includePolicy()) {
                    detailCount += 2;
                }

                //Update the rowspan for the chart td so it fits the space accordingly
                $('#setRowSpanChart').attr("rowspan", detailCount);

                //We need to get count of other liquidity and if policies is checked off, need at least 6 for spacing purposes????
                self.includeCount = 3; //For spacer rows and the last two tiers by detault, need to update to make sure to include any added tiers with details
                self.includeCount += self.sortedTiers()[3].sortedDetails().length; //all of oter liquidity items do not CARE ABOUT ON BALANCE SHEET ITEMS
                //Loop through any custom tiers and add a spacer and tier rorw as well as details for each one
                console.log(self.sortedTiers().length);
                if (self.sortedTiers().length > 5) {
                    for (var i = 4; i < self.sortedTiers().length - 1; i++) {
                        self.includeCount += 2 + self.sortedTiers()[i].sortedDetails().length; //spacer row for it as well as tier row and then details
                    }

                    //Now add on the extra "tiers"
                    self.includeCount += (self.sortedTiers().length - 4);

                }
                else {
                    self.includeCount += 2; //add for space right after other liquidity
                }

                if (self.inv().includePolicy()) {
                    self.includeCount += 1;
                }

                if (self.inv().includeVol()) {
                    if (self.includeCount < 6) {
                        extraRowsNeeded = 6 - self.includeCount;
                        self.includeCount = 6;
                    }
                }

                //splice out on balance sheet item
                //var idx = self.sortedTiers().findIndex(function (el) {return el.name() == 'On Balance Sheet Liquidity' });

                //Loop through values and build out tiers and then details
                for (var i = 0; i < self.sortedTiers().length - 1; i++) {

                    self.buildTierRow(self.sortedTiers()[i], i);

                    //Loop through details and add those below in a fancy border box!
                    for (var z = 0; z < self.sortedTiers()[i].sortedDetails().length; z++) {
                        self.buildDetailRow(self.sortedTiers()[i].sortedDetails()[z], i, z, self.sortedTiers()[i].sortedDetails().length - 1)
                    }

                    //check to see if we need to draw vol area
                    if (i == 2 && !self.inv().includePolicy()) {
                        self.buildSpacerRow(true, i, self.sortedTiers().length - 2, (extraRowsNeeded > 0 ? true : false)); //subtract 2 beucase we do not use on balance sheet in this aprt
                    }
                    else {
                        self.buildSpacerRow(false, i, self.sortedTiers().length - 2, (extraRowsNeeded > 0 ? true : false));
                    }


                }

                //Check for extra rows needed for includevol
                if (self.inv().includeVol() && extraRowsNeeded > 0) {
                    //TODO: generate those empty rows under 
                    for (var i = 1; i <= extraRowsNeeded; i++) {
                        self.buildSpacerRow(false, i, extraRowsNeeded, false);
                    }

                }


                var chartSeries = [{
                    name: 'Tier 3',
                    color: charting.chartColorsPalette2[2],
                    data: [],
                    yAxis: 0
                }, {
                    name: 'Tier 2',
                    color: charting.chartColorsPalette2[1],
                    data: [],
                    yAxis: 0
                }, {
                    name: 'Tier 1',
                    color: charting.chartColorsPalette2[0],
                    data: [],
                    yAxis: 0
                },
                {
                    type: 'line',
                    name: 'Tier 3 Contribution',
                    color: charting.chartColorsPalette2[2],
                    data: [],
                    borderColor: '#ffffff',
                    borderWidth: 5,
                    zIndex: 10,
                    marker: {
                        enabled: false
                    },
                    yAxis: 0
                }, {
                    type: 'line',
                    name: 'Tier 2 Contribution',
                    color: charting.chartColorsPalette2[1],
                    data: [],
                    borderColor: '#000000',
                    borderWidth: 5,
                    zIndex: 10,
                    marker: {
                        enabled: false
                    },
                    yAxis: 0
                }, {
                    type: 'line',
                    name: 'Tier 1 Contribution',
                    color: charting.chartColorsPalette2[0],
                    data: [],
                    borderColor: '#fff',
                    borderWidth: 5,
                    zIndex: 10,
                    marker: {
                        enabled: false
                    },
                    yAxis: 0
                }
                ];



                var cats = [];

                for (var i = 0; i < self.historicBasicSurpluses().length; i++) {
                    chartSeries[2].data.push(numeral(self.historicBasicSurpluses()[i].percTier1).value());
                    chartSeries[1].data.push(numeral(self.historicBasicSurpluses()[i].percTier2).value() - numeral(self.historicBasicSurpluses()[i].percTier1).value());
                    chartSeries[0].data.push(numeral(self.historicBasicSurpluses()[i].percTier3).value() - numeral(self.historicBasicSurpluses()[i].percTier2).value());


                    chartSeries[5].data.push(numeral(self.historicBasicSurpluses()[i].percTier1).value());
                    chartSeries[4].data.push(numeral(self.historicBasicSurpluses()[i].percTier2).value());
                    chartSeries[3].data.push(numeral(self.historicBasicSurpluses()[i].percTier3).value());

                    cats.push(moment(self.historicBasicSurpluses()[i].asOfDate).format('MMM-YY'));
                }

                //Before we re3nder chart calculate height of cell so we can set it before render
                var height = $('#setRowSpanChart').height();
                var width = $('#setRowSpanChart').width();

                $('#basicSurplusChart').highcharts({
                    chart: {
                        type: 'column',
                        height: height,
                        width: width,
                        events: {
                            load() {
                                var series = this.series.filter(elem => elem.type === 'line')
                                series.forEach(series => {
                                    this.addSeries({
                                        type: 'line',
                                        data: series.userOptions.data,
                                        showInLegend: false,
                                        color: '#ffffff',
                                        enableMouseTracking: false,
                                        zIndex: 2,
                                        marker: {
                                            enabled: false,
                                            symbol: series.symbol,
                                            radius: series.options.marker.radius + 2
                                        },
                                        lineWidth: series.options.lineWidth + 3,
                                        yAxis: 0
                                    })
                                })
                            }
                        }
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: cats
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        labels: {
                            formatter: function () {
                                return this.value + '%';
                            }
                        },
                        tickAmount: 4
                    },
                    tooltip: {
                        // pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                        shared: true,
                        valueDecimals: 1,
                        valueSuffix: '%'
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        column: {
                            borderWidth: 0
                        }
                    },
                    series: chartSeries
                });

                //If vol is checked put text in the cell based off of id
                if (self.inv().includeVol()) {
                    $('#volText').html('<table id="volTable"> ' +
                        '<tbody> ' +
                        '<tr>' +
                        '<td> <b>BASIC SURPLUS CALCULATION</b></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> Cumulative Amount </td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> - Volatile Liabilities Coverage = </td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> <b><i> Basic Surplus </b></i> </td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td class="spacerRow"></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> <b> VOLATILE LIABILIES COVERAGE </b> </td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> Coverage = <u> ' + numeral(numeral(self.basicSurplusTierTotals().tier1Amount).value() - numeral(self.basicSurplusTierTotals().tier1BasicSurplusAmount).value()).format('$0,0') + '</u> </td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> ' + numeral((numeral(self.basicSurplusTierTotals().tier1Amount).value() - numeral(self.basicSurplusTierTotals().tier1BasicSurplusAmount).value()) / (numeral(self.fundingCapacity().assLiabs[0].assetTotal).value() / 1000)).format('0%') + ' of Assets </td>' +
                        '</tr>' +
                        '</tbody >' +
                        '</table > ');
                }

                //Populate wholesale funding table

                for (var z = 0; z < self.sortedWholesale().length; z++) {
                    var rec = self.sortedWholesale()[z];

                    var row = '<tr class="' + (rec.name() == 'Total Wholesale' ? 'border-top-wholesale' : '') + '">';

                    row += '<td><b>' + (rec.name() == 'Total Wholesale' ? 'TOTAL' : rec.name()) + '</b></td>';
                    row += '<td>' + numeral(rec.availableFunding()).format('0,0') + '</td>';
                    row += '<td>' + numeral(rec.availableFundingPerPolicy()).format('0,0') + '</td>';
                    row += '<td>' + numeral(rec.availableFundingPerTotalWholesalePolicy()).format('0,0') + '</td>';
                    row += '</tr>';

                    $('#wholesaleTable tbody').append(row);
                }

                //loop though sorted footnotes and displaty in special notes box!

                var sortedNotes = self.thisReport().footnotes().sort(function (a, b) { return a.priority() - b.priority(); });

                for (var z = 0; z < self.thisReport().footnotes().length; z++) {
                    $('#notesboxList').append('<li>' + self.thisReport().footnotes()[z].text() + '</li>');
                }

                if (self.inv().includeBalSheet()) {

                    //balance sheet value will always be last tier and firsst detail of the last tier
                    var val = self.sortedTiers()[self.sortedTiers().length - 1].sortedDetails()[0].amount();
                    var label = self.sortedTiers()[self.sortedTiers().length - 1].sortedDetails()[0].name();

                    var html = '<table id="balSheetTable"> ' +
                        '<tbody> ' +
                        '<tr>' +
                        '<td> <b>ON BALANCE SHEET LIQUIDITY</b></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> ' + label + ' =  <u>' + numeral(val).format('$0,0') + '</u> </td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td> ' + numeral(numeral(val).value() / (numeral(self.fundingCapacity().assLiabs[0].assetTotal).value() / 1000)).format('0%') + ' of Assets </td>' +
                        '</tr>';




                    //If including policies
                    if (self.inv().includePolicy()) {
                        html += '<tr>' +
                            '<td>' + self.generatePolicyMinimumText('bsminOnBalance') + '</td>' +
                            '</tr>';
                    }

                    $('#balSheetText').html(html);

                }

                global.loadingReport(false);
                $('#reportLoaded').text('1');
                //Export To Pdf
                if (typeof hiqPdfInfo == "undefined") {
                    globalContext.logEvent("Viewed", curPage); 
                }
                else {

                }
            });
            
        }

        this.activate = function (id) {
            global.loadingReport(true);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                return globalContext.getPolicyOffsets(self.policyOffsets, self.inv().institutionDatabaseName()).then(function () {
                    return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.inv().institutionDatabaseName()).then(function () {
                        return globalContext.getBasicSurplusAdminsByAsOfDate(self.availBasicSurplus, self.inv().institutionDatabaseName(), self.asOfDateOffsets()[self.inv().asOfDateOffset()].asOfDateDescript).then(function () {

                            self.inv().asOfDateOffset(0);
                             //Set default basic surplus now that everything is loaded
                            self.setDefaultBasicSurplus();
                            for (var i = 0; i < self.inv().inventoryLiquidityResourcesDateOffsets().length; i++) {
                                self.updateOffsetFields(self.inv().inventoryLiquidityResourcesDateOffsets()[i]);
                            }
                            globalContext.saveChangesQuiet();

                            return globalContext.getInventoryLiquidityResourcesFields(self.basicSurplusFields, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).fin(function (){
                                return globalContext.getInventoryLiquidityResourcesTierTotals(self.basicSurplusTierTotals, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).fin(function () {
                                    return globalContext.getInventoryOfLiquidityResourcesFundingCapacity(self.fundingCapacity, self.inv().basicSurplusId(), self.inv().institutionDatabaseName()).fin(function () {
                                        return globalContext.getInventoryOfLiquidityResourcesBasicSurplusHistoric(self.historicBasicSurpluses, self.inv().id(), self.inv().institutionDatabaseName()).fin(function () {

                                            //self.inv().inventoryLiquidityResourcesDateOffsets().forEach(function (item, ind, arr) {
                                            //    self.updateOffsetFields(item);
                                            //});

                                            self.calcFundingTable();
                                            self.calcFinalFundingTable();

                                            //First sort the tiers
                                            self.sortTiers();

                                            //Loop through all inventoryLiquidityResourcesTier
                                            for (var i = 0; i < self.inv().inventoryLiquidityResourcesTier().length; i++) {
                                                self.inv().inventoryLiquidityResourcesTier()[i].sortedDetails = ko.observableArray();
                                                self.sortDetails(self.inv().inventoryLiquidityResourcesTier()[i]);

                                                for (var z = 0; z < self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail().length; z++) {
                                                    //Load possivle tiers and based off of vbalues disable them as available
                                                    self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].availableDetails = ko.observableArray();

                                                    self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].sortedDetails = ko.observableArray();

                                                    self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].upSections = ko.observableArray();
                                                    self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z].downSections = ko.observableArray();

                                                    self.updateAvailableFields(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z]);

                                                    //now update totaling becuase we gots to
                                                    loveService.calculateTierDetails(self.inv().inventoryLiquidityResourcesTier()[i].inventoryLiquidityResourcesTierDetail()[z], self.inv);
                                                }
                                            }

                                            //To do go through and update tier totals
                                            self.setTierTotals();
                                            self.sortWholesale();
                                        });  
                                    });
                                });
                            });
                        });
                    });
                });
            });
        };
                          
        this.detached = function (view, parent) {

        };

    };

        return invVm;
});
