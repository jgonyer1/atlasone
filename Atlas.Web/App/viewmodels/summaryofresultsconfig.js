﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    //TODO this needs to register the subscriptions somewhere so we can clear them later on detached()
    ko.subscribable.fn.subscribeChanged = function (callback) {
        var oldValue;
        this.subscribe(function (_oldValue) {
            oldValue = _oldValue;
        }, this, 'beforeChange');

        this.subscribe(function (newValue) {
            callback(newValue, oldValue);
        });
    };
    var summaryResultsVm = function (sumResults) {
        var self = this;

        this.sumResults = sumResults;

        this.cancel = function () {
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.simulationTypes = ko.observableArray();

        this.institutions = ko.observableArray();

        this.ovrd = ko.observable(false);

        self.sumResults().ovrd.subscribeChanged(function (newValue, oldValue) {
            if (newValue != oldValue) {
                if (newValue) {
                    self.sumResults().nmdAvgLifeAssumption(self.modelsetup().avgLifeAssumption());
                    self.sumResults().extraText(self.modelsetup().avgLifeMessage());
                }
            }
        });

        this.nmdAvgLifeAssumptions = ko.observableArray(["5", "7.5", "Other"]);

        this.overrideChange = function () {
            if (!self.sumResults().ovrd()) {
                self.sumResults().nmdAvgLifeAssumption(self.modelsetup().avgLifeAssumption());
                self.sumResults().extraText(self.modelsetup().avgLifeMessage());
            }
            return true;
        };

        this.extraTextUpdate = function () {
            var lookup = {
                "5": "This assessment assumed a 5 year estimated life on the core deposit base",
                "7.5": "This assessment assumed a 7.5 year estimated life on the core deposit base",
                "Other": "Average lives of non-maturity deposits are based upon a [mm/dd/20yy] DCG deposit study. The aggregate average life based upon the current non-maturity deposit mix is [#.#] years. Please refer to the Deposit Study Tear Sheet for additional documentation"
            };
            self.sumResults().extraText(lookup[self.sumResults().nmdAvgLifeAssumption()]);
        };
        this.disableTextarea = function () {
            if (self.sumResults().nmdAvgLifeAssumption() == 'Other')
                return null;
            else return true;
        };

        this.extraTextUpdate();

        this.modelsetup = ko.observable();

        this.compositionComplete = function () {

        }


        this.refreshSim = function () {
            globalContext.getAvailableSimulations(self.simulationTypes, self.asc().institutionDatabaseName(), self.asc().asOfDateOffset()).then(function () {
                self.refreshScen();
            });
        }

        this.activate = function (id) {
            global.loadingReport(true);
            return globalContext.getModelSetup(self.modelsetup).then(function () {
                return globalContext.getSimulationTypes(self.simulationTypes).then(function () {
                    self.overrideChange();
                    if (self.sumResults().simulationTypeId() == 0) {
                        //default to EVE simulation
                        self.sumResults().simulationTypeId(2);
                    } else {
                        globalContext.saveChangesQuiet();
                    }

                    return globalContext.getAvailableBanks(self.institutions).then(function () {
                        //self.extraText = ko.computed(function () {
                        //    var lookup = {
                        //        "5": "This assessment assumed a 5 year estimated life on the core deposit base.",
                        //        "7.5": "This assessment assumed a 7.5 year estimated life on the core deposit base.",
                        //        "Other": "Average lives of non-maturity deposits are based upon a [mm/dd/2077] DCG deposit study. The aggregate average life based upon the current non-maturity deposit mix is [#.#] years. Please refer to the Deposit Study Tear Sheet for additional documentation."
                        //    };
                        //
                        //    return lookup[self.nmdAvgLifeAssumptions()];
                        //});

                        //where we're going we don't need footnotes
                        $('#footnotes').html('');

                        //AsOfDateOffset Handling (Single-Institution Template)
                        self.instSub = self.sumResults().institutionDatabaseName.subscribe(function (newValue) {
                            globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                        });
                        return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.sumResults().institutionDatabaseName()).then(function () {
                            global.loadingReport(false);
                        });
                    });
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return summaryResultsVm;
});
