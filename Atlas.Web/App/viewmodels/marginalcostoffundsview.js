﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var mcfVm = function (id) {
        var self = this;
        this.mcf = ko.observable();

        this.compositionComplete = function () {
            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
            }
            else {
                hiqPdfConverter.startConversion();
            }
        }

        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getMarginalCostOfFundsViewDataById(id, self.mcf).then(function () {
                globalContext.reportErrors(self.mcf().errors, self.mcf().warnings);
                self.reducInc = ko.computed(function () {
                    if (self.mcf().mcf.isReduction()) {
                        return "reduction";
                    } else {
                        return "increase";
                    }
                });
                self._runoffs = self.mcf().mcf.runoffs().split(',');
                self._rateChanges = self.mcf().mcf.rateChanges().split(',');
            });
        };
    };

    return mcfVm;

});
