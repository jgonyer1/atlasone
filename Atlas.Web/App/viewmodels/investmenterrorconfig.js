﻿define([
    'services/globalcontext',
    'services/global',
    'durandal/app'
],
function (globalContext, global, app) {
    var ivpVm = function (ivp) {
        var self = this;
        var curPage = "InvestmentErrorConfig";
        this.ivp = ivp;

        this.cancel = function () {
        };

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.institutions = ko.observableArray();

        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage); 
            global.loadingReport(true);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub = self.ivp().institutionDatabaseName.subscribe(function (newValue) {
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                });
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.ivp().institutionDatabaseName()).then(function () {
                    global.loadingReport(false);
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return ivpVm;
});
