﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var charting = require('services/charting');
    var app = require('durandal/app');
    var global = require('services/global');

    var eveVm = function (eve) {
        var self = this;
        var curPage = "EveView";
        this.eveViewData = ko.observable();
        this.hasErrors = ko.observable(false);
        this.eve_nev = ko.observable("");
        this.colors = ['#80FFFF',
                '#009933', '#2BAA2B',
                '#80CC1A', '#D5EE09',
                '#FFFF00', '#FFEA00',
                '#FFC000', '#FF7F00',
                '#FF4000', '#FF0000'];
        this.showNCUA = ko.observable(false);
        this.showNCUAError = ko.observable(false);
        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getEveViewDataById(eve, self.eveViewData).then(function () {
                globalContext.reportErrors(self.eveViewData().errors, self.eveViewData().warnings);
                if (self.eveViewData().errors.length > 0) {
                    self.hasErrors(true);
                }
                if (self.eveViewData().creditUnion) {
                    self.eve_nev("NEV");
                } else {
                    self.eve_nev("EVE");
                }

                var has0Shock = false;
                var hasPlus300 = false;
                for (var sc = 0; sc < self.eveViewData().scens.length; sc++) {
                    if (self.eveViewData().scens[sc].name == "0 Shock") {
                        has0Shock = true;
                    }
                    if (self.eveViewData().scens[sc].name == "+300BP") {
                        hasPlus300 = true;
                    }
                }
                if (self.eveViewData().reportFormat == "2" && has0Shock && hasPlus300) {
                    self.showNCUA(true);
                }
                else if (self.eveViewData().reportFormat == "2" && (!has0Shock || !hasPlus300)) {
                    self.showNCUAError(true);
                }
            }).catch(function () {
                // eat this error
            });
        };

        var getScenarioPolicies = function (scenName) {
            var ret = { eveChange: { policyLimit: -999 }, bpChange: { policyLimit: -999 }, postShock: { policyLimit: -999 }};
            for (var x = 0; x < self.eveViewData().eveChangeLimit.length; x++) {
                if (self.eveViewData().eveChangeLimit[x].name == scenName) {
                    ret.eveChange = self.eveViewData().eveChangeLimit[x];
                    break;
                }
            }

            for (var x = 0; x < self.eveViewData().eveRatioLimit.length; x++) {
                if (self.eveViewData().eveRatioLimit[x].name == scenName) {
                    ret.postShock = self.eveViewData().eveRatioLimit[x];
                    break;
                }
            }

            for (var x = 0; x < self.eveViewData().eveChangeRatioLimit.length; x++) {
                if (self.eveViewData().eveChangeRatioLimit[x].name == scenName) {
                    ret.bpChange = self.eveViewData().eveChangeRatioLimit[x];
                    break;
                }
            }

            return ret;
        };


        var highlightVert = function (scenName) {
            if (scenName == '0 Shock' || (scenName == '+300BP' && self.eveViewData().reportFormat == "2")) {
               return 'highlightVert';
            }
            else {
                return "";
            }
        }

        var generateSpacerRow = function (addVertClass) {
            var spacerRow = "<tr><td class='spacerRow'></td><td class='spacerRow'></td>";
            for (var s = 0; s < self.eveViewData().scens.length; s++) {
                var vertClass = highlightVert(self.eveViewData().scens[s].name);

                if (!addVertClass) {
                    vertClass = '';
                }
                spacerRow += "<td class='spacerRow spacerCell'></td>";
                spacerRow += "<td class='spacerRow " + vertClass + "'></td>";
            }
            spacerRow += "</tr>";

            return spacerRow;
        }

        var formatPolicy = function (pol, format, post) {
            if (pol == -999) {
                return "";
            }
            else {
                return numeral(pol).format(format) + post;
            }
        }

        var policyColor = function (pol, ratio, scen) {
            
            return 'rgb(239,8,8)';
            //Old code no longer needed
            /*if (pol != -999) {

                if (numeral(ratio).value() < 0) {
                    //If it falls in that range return red otherwise return regulare scen color
                    if (numeral(pol).value() < 0 && numeral(pol).value() >= numeral(ratio).value()) {
                        return 'rgb(239,8,8)';
                    }
                    else {
                        return scenColor(scen);
                    }
                }
                else {
                    if (numeral(pol).value() >= 0 && numeral(pol).value() <= numeral(ratio).value()) {
                        return 'rgb(239,8,8)';
                    }
                    else {
                        return scenColor(scen);
                    }
                }

                if (Math.abs(numeral(ratio).value()) >= Math.abs(numeral(pol).value())) {

                }
            }
            else {
                return scenColor(scen);
            }*/
        };

        var scenColor = function (curScen) {
            var seriesColor = 'rgb(0,32,96)';//set to up if 0 shock or down change it

            if (curScen.indexOf('-')) {
                seriesColor = 'rgb(255,127,0)';
            }

            if (curScen == '0 Shock') {
                seriesColor = 'rgb(168,205,242)';
            }

            return seriesColor;

        }
        this.compositionComplete = function () {
            if (self.hasErrors()) {
                return true;
            }

            if (!self.eveViewData()) return;

            //If credit union add header for fun!
            if (self.eveViewData().creditUnion) {
                $("#riskSecondaryHeader").html("Post Shock NEV/EVA Ratio");
            }

            //Set up hash table of breakouts
            var tempDt = {
                assets: {},
                liabs: {}
            }

            //Loop through eve data and pivot table to have scenarios as columns

            for (var i = 0; i < self.eveViewData().eveScens.length; i++) {
                var obj = self.eveViewData().eveScens[i];
                var al = 'assets';
                if (!obj.isAsset) {
                    al = 'liabs';
                }

                if (tempDt[al][obj.accountName] == undefined) {
                    tempDt[al][obj.accountName] = {
                        scens: {},
                        bookValue: 0,
                        total: false
                    }
                }

                tempDt[al][obj.accountName].scens[obj.scenarioName] = {
                    marketValue: numeral(obj.marketValue).value()
                };

                prevIsAsset = al;

            }

            //loop thorugh hash table and add records to new hash table so that we can insert the total rows in correct sports scenario sorting is what made this weird
            var dt = {
                assets: {},
                liabs: {}
            }
            var prevIsAsset = undefined;
            for (var al in tempDt) {

                for (var acc in tempDt[al]) {
                    dt[al][acc] = tempDt[al][acc];
                }

                var totalName = (al == 'assets' ? 'Total Assets' : 'Total Liabilities');
                if (dt[al][totalName] == undefined) {
                    dt[al][totalName] = {
                        scens: {},
                        bookValue: 0,
                        total: true
                    }

                    //create place hodlers for scneaiors for totaling row
                    for (var s = 0; s < self.eveViewData().scens.length; s++) {
                        var scenText = self.eveViewData().scens[s].name;
                        var zeroShockClass = '';
                        if (al != 'assets') {
                            scenText = '';
                        }
                        if (self.eveViewData().scens[s].name == '0 Shock') {
                            zeroShockClass = 'zeroshock';
                        }
                        dt[al][totalName].scens[self.eveViewData().scens[s].name] = { marketValue: 0 };
                    }
                }
            }

            //Take book data and add to hash tabl that was just build 
            for (var a = 0; a < self.eveViewData().eveBook.length; a++) {
                var obj = self.eveViewData().eveBook[a];

                var al = 'assets';
                if (!obj.isAsset) {
                    al = 'liabs';
                }

                if (dt[al][obj.accountName] == undefined) {
                    dt[al][obj.accountName] =
                        {
                            scens: {},
                            bookValue: 0,
                            total: false
                        }

                }

                dt[al][obj.accountName].bookValue = numeral(obj.bookValue).value();

            }


            //records for totaling already exists and this just adds them up
            for (var al in dt) {
                //Now add totals rows for liabs and assets
                var totalName = (al == 'assets' ? 'Total Assets' : 'Total Liabilities');

                //perform totaling
                for (var acc in dt[al]) {
                    if (!dt[al][acc].total) {
                        for (var s = 0; s < self.eveViewData().scens.length; s++) {
                            dt[al][totalName].scens[self.eveViewData().scens[s].name].marketValue += numeral(dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue).value();
                        }
                        dt[al][totalName].bookValue += numeral(dt[al][acc].bookValue).value();
                    }

                }
            }

            //This will loop through the scenarios and find the footnoteUpScen and footnoteDownsen to update footnote on bottom of risk grid
            var footnoteUpScenario = '';
            var footnoteDownScenario = '';

            for (var s = 0; s < self.eveViewData().scens.length; s++) {

                if (self.eveViewData().scens[s].name == '+200BP') {
                    footnoteUpScenario = '+200';
                }

                //if no scneario fround yet for up use 100
                if (self.eveViewData().scens[s].name == '+100BP' && footnoteUpScenario == '') {
                    footnoteUpScenario = '+100';
                }

                if (self.eveViewData().scens[s].name == '-200BP') {
                    footnoteDownScenario = '-200';
                }

                //if no scneario fround yet for up use 100
                if (self.eveViewData().scens[s].name == '-100BP' && footnoteDownScenario == '') {
                    footnoteDownScenario = '-100';
                }

            }

            //set foornotescens
            $('#footnoteScens').text(footnoteUpScenario + '/' + footnoteDownScenario);

            //Craete variables and prepare to dump data out for asets and liabs

            //Captures Eve Table element via Vanilla JS
            var eveTableElement = document.getElementById("eveTable");
            var eveTableBody = "<tbody>";

            //We need to write out the assets row first with scenaior headers

            var counter = 0;
            var outputHeaderRow = true;
            var header = "<tr class='eveHeaderRow'><td>ASSETS</td><td class='scenarioHeader bookHeader'>BOOK</td>";
            for (var s = 0; s < self.eveViewData().scens.length; s++) {
                var scenClass = "upScenario";

                if (self.eveViewData().scens[s].name.indexOf("-")) {
                    scenClass = "downScenario";
                }

                if (self.eveViewData().scens[s].name == '0 Shock') {
                    scenClass = 'zeroShockScenario'
                }


                header += "<td class='spacerCell'></td>";
                header += "<td class='scenarioHeader " + scenClass + "'>" + self.eveViewData().scens[s].name + "</td>";
            }
            header += "</tr>";

            eveTableBody += header;
            var prevAl = 'assets';

            console.log(dt);
            //Loop through hash table and build out assests and liabs data table
            for (var al in dt) {

                if (prevAl != al) {

                    //All these cells will be empty besides main header
                    header = "<tr class='eveHeaderRow'><td>LIABILITIES</td><td></td>";
                    for (var s = 0; s < self.eveViewData().scens.length; s++) {
                        header += "<td class='spacerCell'></td>";
                        var vertClass = highlightVert(self.eveViewData().scens[s].name);

                        header += "<td class='" + vertClass + "'></td>";
                    }
                    header += "</tr>";

                    eveTableBody += header;
                }

                for (var acc in dt[al]) {
                    var totclass = (dt[al][acc].total) ? 'eveTotalRow eveBoldRow' : '';
                    var accRow = '<tr class="' + totclass + '"><td style="width: 200px">' + acc + '</td>';

                    accRow += '<td>' + numeral(dt[al][acc].bookValue / 1000).format('0,0') + '</td>';

                    for (var s = 0; s < self.eveViewData().scens.length; s++) {
                        var scenName = self.eveViewData().scens[s].name;
                        //Always add a spacer to pre scenario cell
                        accRow += '<td class="spacerCell"></td>';
                        var zeroShockClass = '';

                        var vertClass = highlightVert(scenName);

                        if (dt[al][acc].scens[scenName] == undefined) {
                            accRow += '<td class="' + vertClass + '"/></td>';
                        }
                        else {
                            //special stuff for nmd's
                            if (acc == "Non-Maturity Deposit") {
                                let mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue;
                                //switch (scenName) {
                                //    case "-100BP":
                                //        mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue;
                                //        break;
                                //    case "0 Shock":
                                //        mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue * 0.99;
                                //        break;
                                //    case "+100BP":
                                //        mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue * 0.98;
                                //        break;
                                //    case "+200BP":
                                //        mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue * 0.97;
                                //        break;
                                //    case "+300BP":
                                //        mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue * 0.96;
                                //        break;
                                //    case "+400BP":
                                //        mkVal = dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue * 0.95;
                                //        break;
                                //}
                                accRow += '<td class="' + vertClass + '">' + numeral(mkVal / 1000).format('0,0') + '</td>';
                            } else {
                                accRow += '<td class="' + vertClass + '">' + numeral(dt[al][acc].scens[self.eveViewData().scens[s].name].marketValue / 1000).format('0,0') + '</td>';
                            }
                            

                        }

                    }

                    accRow += '</tr>';
                    eveTableBody += accRow;
                }


                //Out put percent change from zeroshock
                //Only calc if 0 Shock is in report


                var totName = (al == 'assets' ? 'Total Assets' : 'Total Liabilities');
                if (dt[al][totName].scens['0 Shock'].marketValue != 0) {
                    var percRow = '<tr class="eveBoldRow"><td>% Change from 0 Shock</td>';


                    percRow += '<td>' + numeral((dt[al][totName].bookValue - dt[al][totName].scens['0 Shock'].marketValue) / dt[al][totName].scens['0 Shock'].marketValue).format('-0.00%') + '</td>';

                    //Loop thhrough all scenrions and calc
                    for (var s in dt[al][totName].scens) {
                        percRow += '<td class="spaceCell"></td>';

                        var vertClass = highlightVert(s);

                        if (s == '0 Shock') {
                            percRow += '<td class="' + vertClass + '"></td>';
                        }
                        else {
                            percRow += '<td class="' + vertClass + '">' + numeral((dt[al][totName].scens[s].marketValue - dt[al][totName].scens['0 Shock'].marketValue) / dt[al][totName].scens['0 Shock'].marketValue).format('-0.00%') + '</td>';
                        }


                    }
                    percRow += '</tr>';
                    eveTableBody += percRow;
                }

                counter += 1;
                prevAl = al;
            }

            //Adfter done this loop add spacer row
            eveTableBody += generateSpacerRow(true);

            var eveValueSeries = [
                {
                    name: 'Economic Value of Equity',
                    data: []
                }

            ];

            //var eveValueHash = {};
            var eveValueCats = [];

            var economicValueOfEquity = numeral((dt['assets']['Total Assets'].bookValue - dt['liabs']['Total Liabilities'].bookValue)).value();

            //Economic Value of Equity(EVE) Row
            var eveRow = '<tr class="eveBoldRow"><td>ECONOMIC VALUE OF EQUITY (EVE)</td>';
            if (self.eveViewData().creditUnion) {
                eveRow = '<tr class="eveBoldRow"><td>NET ECONOMIC VALUE (NEV)</td>';
            }

            eveRow += '<td>' + numeral(economicValueOfEquity / 1000).format('0,0') + '</td>';
            for (var s = 0; s < self.eveViewData().scens.length; s++) {
                var val = (dt['assets']['Total Assets'].scens[self.eveViewData().scens[s].name].marketValue - dt['liabs']['Total Liabilities'].scens[self.eveViewData().scens[s].name].marketValue);
                eveRow += '<td class="spaceCell"></td>';
                var vertClass = highlightVert(self.eveViewData().scens[s].name);

                eveRow += '<td class="' + vertClass + '">' + numeral(val / 1000).format('0,0') + '</td>';


                eveValueSeries[0].data.push(numeral(val).value());
                eveValueCats.push(self.eveViewData().scens[s].name);
                /*eveValueHash[self.eveViewData().scens[s].name] = {
                    eveValue: val,
                    eveChangeFromZeroShock: 0,
                    eveRatio: (val / dt['assets']['Total Assets'].scens[self.eveViewData().scens[s].name].marketValue) * 100,
                    eveBPChangeFromZeroShock: 0,
                    eveChangeLimit: 0,
                    eveRatioLimit: 0,
                    eveRatioChangeLimit: 0
                }*/
            }
            eveRow += '</tr>';
            eveTableBody += eveRow;

            //Adfter done this loop add spacer row
            eveTableBody += generateSpacerRow(true);

            //Add What we jsut generated to HTML dom
            eveTableBody += "</tbody>";
            eveTableElement.innerHTML += eveTableBody;

            //Calculate bp chang and 0 shoxk here
            /* for (var s in eveValueHash) {
                 if (s != '0 Shock') {
                     eveValueHash[s].eveChangeFromZeroShock = ((eveValueHash[s].eveValue - eveValueHash['0 Shock'].eveValue) / Math.abs(eveValueHash['0 Shock'].eveValue)) * 100;
                     eveValueHash[s].eveBPChangeFromZeroShock = ( eveValueHash[s].eveRatio - eveValueHash['0 Shock'].eveRatio);
                 }
             }*/


            //Builds out stuff we will need for charting
            var eveChangeMin = 0;
            var eveChangeMax = 0;
            var eveChangeCats = [];
            var eveChangeSeries;

            var eveRatioMin = 0;
            var eveRatioMax = 0;
            var eveRatioCats = [];
            var eveRatioSeries = [
                {
                    name: self.eveViewData().creditUnion ? "NEV" : "EVE",
                    type: "column",
                    color: '#428BCA',
                    data: [],
                    showInLegend: false
                },
                {
                    name: 'Policy Limit',
                    type: 'errorbar',
                    color: 'rgb(239,8,8)',
                    linkedTo: null,
                    whiskerWidth: 3,
                    whiskerLength: '120%',
                    data: [],
                    showInLegend: false
                }];

            var eveChangeRatioMin = 0;
            var eveChangeRatioMax = 0;
            var eveChangeRatioCats = [];
            var eveChangeRatioSeries = [
                {
                    name: self.eve_nev(),
                    type: "column",
                    color: '#428BCA',
                    data: [],
                    showInLegend: false
                },
                {
                    name: 'Policy Limit',
                    type: 'errorbar',
                    color: 'rgb(239,8,8)',
                    linkedTo: null,
                    whiskerWidth: 3,
                    whiskerLength: '120%',
                    data: [],
                    showInLegend: false
                }];

            //reset some row variables to create the bottom tbody 
            var evePolicyTableBody = "<tbody class='eveBreakoutBody'>";
            var ncuaPercChange;
            var ncuaRatio;

            //Set Eve Policies
            var curScen = "";
            var curScenPols = {};

            //Set order of ratios getting spit out
            var eveRatiosOrder = [
                {
                    postFix: '%',
                    mult: 100,
                    shortName: 'eveChange',
                    name: self.eve_nev() + " % Change from 0 Shock",
                    format: '0.0',
                    max: 0,
                    min: 0,
                    cats: [],
                    series: [
                        {
                            name: self.eve_nev(),
                            type: "column",
                            color: '#428BCA',
                            data: [],
                            showInLegend: false
                        }, {
                            name: 'Policy Limit',
                            type: 'errorbar',
                            color: 'rgb(239,8,8)',
                            linkedTo: null,
                            whiskerWidth: 3,
                            whiskerLength: '120%',
                            data: [],
                            showInLegend: false
                        }]
                },
                {
                    postFix: '%',
                    mult: 100,
                    shortName: 'postShock',
                    name: self.eve_nev() + " Ratio (" + self.eve_nev() + "/EVA)",
                    format: '0.0',
                    min: 0,
                    max: 0,
                    cats: [],
                    series: [
                        {
                            name: self.eveViewData().creditUnion ? "NEV" : "EVE",
                            type: "column",
                            color: '#428BCA',
                            data: [],
                            showInLegend: false
                        },
                        {
                            name: 'Policy Limit',
                            type: 'errorbar',
                            color: 'rgb(239,8,8)',
                            linkedTo: null,
                            whiskerWidth: 3,
                            whiskerLength: '120%',
                            data: [],
                            showInLegend: false
                        }]
                },
                {
                    postFix: '',
                    mult: 10000,
                    shortName: 'bpChange',
                    name: self.eve_nev() + " Ratio BP Change from 0 Shock",
                    format: '0,0',
                    min: 0,
                    max: 0,
                    cats: [],
                    series: [
                        {
                            name: self.eve_nev(),
                            type: "column",
                            color: '#428BCA',
                            data: [],
                            showInLegend: false
                        },
                        {
                            name: 'Policy Limit',
                            type: 'errorbar',
                            color: 'rgb(239,8,8)',
                            linkedTo: null,
                            whiskerWidth: 3,
                            whiskerLength: '120%',
                            data: [],
                            showInLegend: false
                        }]

                }];

            for (var e = 0; e < eveRatiosOrder.length; e++) {
                var eveCur = eveRatiosOrder[e];
                var ratioRow = "<tr class='eveBoldRow'><td>" + eveCur.name + "</td>";

                //Eve nev ratio row
                if (e == 1) {
                    if (dt['assets']['Total Assets'].bookValue != 0) {
                        ratioRow += "<td>" + numeral(economicValueOfEquity / dt['assets']['Total Assets'].bookValue).format('0.0%') + "</td>";
                    }
                    else {
                        ratioRow += "<td></td>";
                    }
                }
                else {
                    ratioRow += "<td></td>";
                }
               
                var polRow = "<tr class='eveItalicRow'><td>Policy Limits</td><td></td>";

                var foundPol = false;
                for (var sc = 0; sc < self.eveViewData().scens.length; sc++) {
                    curScen = self.eveViewData().scens[sc].name;

                    //Load policies for all three eve policies
                    curScenPols = getScenarioPolicies(curScen);

                    if (curScen == "+300BP" && eveCur.shortName == 'eveChange') {
                        ncuaPercChange = self.eveViewData().eveRatios[eveCur.shortName + curScen] * 100;
                    }

                    //Captur +300BP seems to be important
                    if (curScen == "+300BP" && eveCur.shortName == 'postShock') {
                        ncuaRatio = self.eveViewData().eveRatios[eveCur.shortName + curScen] * 100;
                    }

                    ratioRow += '<td class="spaceCell"></td>';
                    polRow += '<td class="spaceCell"></td>';

                    if (curScen != "0 Shock" || (eveCur.shortName == "postShock")) {
                        ratioRow += "<td>" + numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen] * eveCur.mult).format(eveCur.format) +  eveCur.postFix + "</td>";
                    }
                    else {
                        ratioRow += "<td></td>";
                    }


                    if (curScen != "0 Shock" || (eveCur.shortName == "postShock")) {
                        polRow += "<td>" + formatPolicy(curScenPols[eveCur.shortName].policyLimit, eveCur.format, eveCur.postFix) + "</td>";
                    }
                    else {
                        polRow += "<td></td>";
                    }


                    if (curScenPols[eveCur.shortName].policyLimit != -999) {
                        foundPol = true;
                    }

                    //Now populate series of current ratio

                    if (curScen != "0 Shock" || (eveCur.shortName == "postShock")) {
                        var seriesColor = scenColor(curScen);
                        eveRatiosOrder[e].cats.push(curScen);

                        if (eveRatiosOrder[e].min > numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen]).value() * eveCur.mult ) {
                            eveRatiosOrder[e].min = numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen]).value() * eveCur.mult;
                        }
                        if (eveRatiosOrder[e].max < numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen]).value() * eveCur.mult) {
                            eveRatiosOrder[e].max = numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen]).value() * eveCur.mult;
                        }

                        eveRatiosOrder[e].series[0].data.push({ y: numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen]).value() * eveCur.mult, color: seriesColor });

                        if (curScenPols[eveCur.shortName].policyLimit == -999 || !self.eveViewData().policyLimits) {
                            eveRatiosOrder[e].series[1].data.push([null, null]);
                        }
                        else {
                            eveRatiosOrder[e].series[1].data.push({ high: numeral(curScenPols[eveCur.shortName].policyLimit).value(), low: numeral(curScenPols[eveCur.shortName].policyLimit).value(), color: policyColor(curScenPols[eveCur.shortName].policyLimit, numeral(self.eveViewData().eveRatios[eveCur.shortName + curScen]).value(), curScen) });
                        }

                    }

                }

                ratioRow += '</tr>';
                polRow += '</tr>';

                evePolicyTableBody += ratioRow;

                if (self.eveViewData().policyLimits && foundPol) {
                    evePolicyTableBody += polRow;
                    eveRatiosOrder[e].series[1].showInLegend = true;
                }

                evePolicyTableBody += generateSpacerRow(false);
            }   

            console.log(eveRatiosOrder);

            evePolicyTableBody += "</tbody>";
            eveTableElement.innerHTML += evePolicyTableBody;
            
            //flag ncua table shading
            if (ncuaPercChange != undefined && ncuaRatio != undefined && self.eveViewData().reportFormat == "2") {
                if (ncuaPercChange > -40 && ncuaRatio >= 7) {
                    document.getElementById("ncua_low_row").classList.add("ncua-highlight");
                } else if (ncuaPercChange > -65 && ncuaRatio >= 4) {
                    document.getElementById("ncua_moderate_row").classList.add("ncua-highlight");
                }
                else if (ncuaPercChange > -85 && ncuaRatio >= 2) {
                    document.getElementById("ncua_high_row").classList.add("ncua-highlight");
                }
                else{
                    document.getElementById("ncua_extreme_row").classList.add("ncua-highlight");
                }
            }


            //Here is where we will render the charts
            if (self.eveViewData().reportFormat != "2") {
                $('#eveChangeChartTitle').text(eveRatiosOrder[0].name);
                $('#eveChangeChart').highcharts(charting.createEvePolicyChartOptions(function () { return numeral(this.value).format('0.0') + '%'; }, eveRatiosOrder[0].cats, eveRatiosOrder[0].series, eveRatiosOrder[0].min, eveRatiosOrder[0].max, function () { return numeral(this.y).format('0.0') + '%'; }, 0, function () { return self.eve_nev() + ": " + numeral(this.y).format('0.0') + '%'; }))
            }

            //If standard render ratio chart
            if (self.eveViewData().reportFormat == "0") {
                $('#postShockChartTitle').text(eveRatiosOrder[1].name);
                $('#postShockChart').highcharts(charting.createEvePolicyChartOptions(function () { return numeral(this.value).format('0.0') + '%'; }, eveRatiosOrder[1].cats, eveRatiosOrder[1].series, eveRatiosOrder[1].min, eveRatiosOrder[1].max, function () { return numeral(this.y).format('0.0') + '%'; }, 1, function () { return self.eve_nev() + ": " + numeral(this.y).format('0.0') + '%'; }))
            }

            //If standard render ratio chart
            if (self.eveViewData().reportFormat == "0") {
                $('#bpChangeChartTitle').text(eveRatiosOrder[2].name);
                $('#bpChangeChart').highcharts(charting.createEvePolicyChartOptions(function () { return numeral(this.value).format('0,0'); }, eveRatiosOrder[2].cats, eveRatiosOrder[2].series, eveRatiosOrder[2].min, eveRatiosOrder[2].max, function () { return numeral(this.y).format('0') + ''; }, 2, function () { return self.eve_nev() + ": " + numeral(this.y).format('0') + ''; }));
            }
            
            //end eve policy table body


            //if (!self.eveViewData().riskSummaryGrid) {
            if (self.eveViewData().reportFormat != "1") {
                $('#riskSummaryHolder').hide();
            }
            else {
                //Calculate What cell to highlight
                var downToUse = '';
                var upToUse = '';

                console.log(self.eveViewData().scens.filter(function (s) { return s.name == '-100BP' }));

                if (self.eveViewData().scens.filter(s => s.name == '-200BP').length > 0) {
                    downToUse = '-200BP';
                }
                else if (self.eveViewData().scens.filter(s => s.name == '-100BP').length > 0) {
                    downToUse = '-100BP';
                }
                if (self.eveViewData().scens.filter(s => s.name == '+200BP').length > 0) {
                    upToUse = '+200BP';
                }
                else if (self.eveViewData().scens.filter(s => s.name == '+100BP').length > 0) {
                    upToUse = '+100BP';
                }
                console.log(upToUse);
                console.log(downToUse);

                //if there's no down to use, set it to 0 Shock
                if (downToUse == '') {
                    downToUse = '0 Shock';
                }

                //Hide If Cannot perform calculation
                if (upToUse == '' || downToUse == '') {
                    $('#riskSummaryHolder').hide();
                    toastr.warning("Could not complete Change From Zero Shock calculation. Scenarios not configured properly.");
                } else {
                    var minRatio = self.eveViewData().eveRatios['postShock' + downToUse];
                    var minScenario = downToUse;
                    if (self.eveViewData().eveRatios['postShock' + downToUse] > self.eveViewData().eveRatios['postShock' + upToUse]) {
                        minRatio = self.eveViewData().eveRatios['postShock' + upToUse];
                        minScenario = upToUse;
                    }


             
                    var maxChangeFromZeroShock = Math.abs(minRatio - self.eveViewData().eveRatios['postShock' + '0 Shock']);

                    var x = 0;
                    var y = 0;

   
                    maxChangeFromZeroShock = Math.abs(maxChangeFromZeroShock * 10000);
                    minRatio = minRatio * 100;
                    if (minRatio < 4) {
                        x = 4;
                    }
                    else if (minRatio <= 6) {
                        x = 3;
                    }
                    else if (minRatio <= 10) {
                        x = 2;
                    }
                    else {
                        x = 1;
                    }

                    if (maxChangeFromZeroShock < 100) {
                        y = 1;
                    }
                    else if (maxChangeFromZeroShock < 200) {
                        y = 2;
                    }
                    else if (maxChangeFromZeroShock < 400) {
                        y = 3;
                    }
                    else {
                        y = 4;
                    }

                    $('#' + x + '-' + y).addClass('highlightCell');
                    $('.minScenario').text(minScenario.replace('BP', ''));
                }
            }

            global.loadingReport(false);
            $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage);  
                if (self.showNCUAError()) {
                    app.showMessage("Incorrect report configuration for NCUA. Please include 0 Shock and +300BP", "Warning", ["OK"]);
                }
            }
            else {
                hiqPdfConverter.startConversion();
            }
           
        };


        



    }


    return eveVm;
});
