﻿define([
    'services/globalcontext',
    'services/global',
    'plugins/router',
    'durandal/app',
    './inputmodal',
    'viewmodels/basicsurplusadmin'
],
function (globalContext, global, router, app, inputModal, naModal) {
    var vm = function () {
        var self = this;
        var curPage = "BasicSurpluses";
        //Vars
        this.global = global;
        this.basicSurplusAdmins = ko.observableArray();
        this.basicSurplusAdminsNewItems = ko.observableArray(); //TODO HACK manager.rejectChanges() is not clearing new items off our array, so track them manually
        this.isSaving = ko.observable(false);
        this.profileInformationId = ko.observable();
        this.profile = ko.observable();

        //View
        this.connectClass = "scenario-container";

        //Navigation Helpers
        this.hasChanges = ko.pureComputed(function () {
            return globalContext.hasChanges() || self.isSaving();
        });

        this.canSave = ko.pureComputed(function () {
            return self.hasChanges() && !self.isSaving();
        });

        this.canDeactivate = function () {
            if (! this.hasChanges()) return true;

            return naModal.show(
                function () { globalContext.cancelChanges(); },
                function () { save(); }
            );
        };

        //Navigation
        this.goBack = function () {
            router.navigateBack();
        };

        this.goToAdmin = function (type, event) {
            router.navigate("#/basicsurplusadmin/" + type.id());
        };

        this.cancel = function () {
            self.isSaving(true);
            globalContext.cancelChanges();
            self.isSaving(false);

            //TODO HACK manager.rejectChanges() is not clearing new items off our array, so we'll just make cancelChanges forcefully truncate the array
            self.basicSurplusAdmins.removeAll(self.basicSurplusAdminsNewItems());
            self.basicSurplusAdminsNewItems.removeAll();

            self.sortTypes();
        };

        this.save = function (data, event, quiet) {
            self.isSaving(true);
            return (quiet)
                ? globalContext.saveChangesQuiet().fin(complete)
                : globalContext.saveChanges().fin(complete);

            function complete() {
                self.isSaving(false);
            }
        };

        //Sorting
        this.sortTypes = function () {
            self.sortedTypes(self.sortedTypes().sort(function (a, b) {
                return a.priority() - b.priority();
            }));
        };

        this.sortableAfterMove = function (arg) {
            arg.sourceParent().forEach(function (type, index) {
                type.priority(index);
            });
        };

        //Add / Remove
        this.addType = function () {
            var that = this;

            //Signal that we're adding a new type (disables buttons) - cleared on save
            self.isSaving(true);

            var title = 'New Basic Surplus Package';

            inputModal.show({
                title: title,
                message: 'Please choose a name for the new Basic Surplus report package',
                hideCancel: false,
                validation: function (value) {
                    var fvalue = value.toLowerCase().trim();
                    var sortedTypes = that.sortedTypes();

                    for (var i = 0; i < sortedTypes.length; i++) {
                        if (sortedTypes[i].name().toLowerCase().trim() == fvalue) {
                            return {
                                success: false,
                                title: title,
                                message: 'There is a conflicting package with name ' + value
                            };
                        }
                    }

                    return { success: true };
                }
            }).then(function (basicSurplusTypeName) {
                var defaulted = false;
                if (basicSurplusTypeName === null) {
                    self.cancel();
                    return;
                } else if (self.basicSurplusAdmins().length == 0) {
                    //workaround simple hack for now. they should always have at least one bs with this name for rolling purposes
                    basicSurplusTypeName = "Basic Surplus";
                    defaulted = true;
                }

                var newType = globalContext.createBasicSurplusAdmin();
                newType.priority(self.sortedTypes().length);
                newType.institutionDatabaseName(self.profile().databaseName);
                newType.informationId(self.profileInformationId());
                self.basicSurplusAdmins.push(newType);
                self.basicSurplusAdminsNewItems.push(newType); //TODO HACK manager.rejectChanges() is not clearing new items off our array, so track them manually
                self.sortTypes();

                //Just automatically save and advance
                newType.name(basicSurplusTypeName);
                self.save().then(function () {
                    if (defaulted) {
                        app.showMessage("Basic Surplus name set to 'Basic Surplus' for system consistency purposes.", "", ["OK"]).then(function () {
                            self.goToAdmin(newType);
                        });
                    } else {
                        self.goToAdmin(newType);
                    }
                    
                    
                });

                return newType;
            });
        };

        this.copyType = function (type) {
            //Signal that we're adding a new type (disables buttons) - cleared on save
            self.isSaving(true);

            var newId = ko.observable();
            var title = 'New Basic Surplus Package';

            inputModal.show({
                title: title,
                message: 'Please choose a name for the new Basic Surplus report package',
                hideCancel: false,
                validation: function (value) {
                    var fvalue = value.toLowerCase().trim();
                    var sortedTypes = self.sortedTypes();

                    for (var i = 0; i < sortedTypes.length; i++) {
                        if (sortedTypes[i].name().toLowerCase().trim() == fvalue) {
                            return {
                                success: false,
                                title: title,
                                message: 'There is a conflicting package with name ' + value
                            };
                        }
                    }

                    return { success: true };
                }
            }).then(function(basicSurplusTypeName) { 
                return globalContext.copyBasicSurplus(type.id(), newId, basicSurplusTypeName).then(function () {
                    return globalContext.getBasicSurplusAdmins(self.basicSurplusAdmins).then(function () {
                        //New Type
                        var newType;
                        self.basicSurplusAdmins().forEach(function (type) {
                            if (type.id() == newId()) {
                                newType = type;
                                return;
                            }
                        });

                        //Just do a save to show the message (and clear isSaving)
                        return self.save().then(function () {
                            self.goToAdmin(newType);
                        });
                    });
                });
            });
        };

        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);
            //Pre-Setup
            return Q.all([
                globalContext.getBasicSurplusAdmins(self.basicSurplusAdmins),
                globalContext.getUserProfile(self.profile),
                globalContext.getProfileInformationId(self.profileInformationId)
            ]).then(function () {
                //Sorting
                self.sortedTypes = ko.observableArray(self.basicSurplusAdmins());
                self.sortTypes();
            });
        };

        return this;
    };

    return vm;
});
