﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var lookbackservice = require('services/lookbackservice');
    var global = require('services/global');
    var vm = function (id) {
        var self = this;
        var curPage = "LookbackReportView";
        this.lbReport = ko.observable();
        this.headerRows = {};
        this.rows = {};
        this.fullTable = "";
        this.lbLookbacks = ko.observableArray();
        this.policyOffsets = ko.observable();
        this.modelSetup = ko.observable();
        this.scenarioTypes = ko.observable();
        this.profile = ko.observable();
        
        this.compositionComplete = function () {
            
        };

        this.activate = function () {
            global.loadingReport(true);
            function formatBal(bal) {
                return numeral(bal / 1000).format('0,00');
            }

            //this report is different from just about every other report in that we're going to do the defualting of flags here, on the client side. 
            //Should work...
            return globalContext.getUserProfile(self.profile).then(function () {
                return globalContext.getScenarioTypes(self.scenarioTypes).then(function () {
                    return globalContext.getPolicyOffsets(self.policyOffsets, "").then(function () {
                        return globalContext.getModelSetup(self.modelSetup).then(function () {
                            return globalContext.getLBLookbacksByReportId(id, self.lbLookbacks).then(function () {
                                var i = 0;
                                var lbObs;
                                //update the lookbacks with defaults if appropriate
                                for (i = 0; i < self.lbLookbacks().length; i++) {
                                    var lb = self.lbLookbacks()[i];

                                    //simulationTypeId
                                    if (!lb.overrideSimulationTypeId()) {
                                        var aod = self.profile().asOfDate;
                                        if (self.policyOffsets()[0][aod] != undefined) {
                                            lb.simulationTypeId(self.policyOffsets()[0][aod][0].niiId);
                                        }
                                    }

                                    //scenarioTypeId
                                    if (!lb.overrideScenarioTypeId()) {
                                        var aod = self.profile().asOfDate;
                                        if (self.policyOffsets()[0][aod] && self.policyOffsets()[0][aod][0] != undefined) {
                                            var arr = self.scenarioTypes().filter(function (obj) {
                                                return obj.id() == "1"
                                            });
                                            if (self.policyOffsets()[0][aod][0].bsSimulation == self.policyOffsets()[0][aod][0].niiId) {
                                                lb.scenarioTypeId(self.policyOffsets()[0][aod][0].bsScenario);
                                            } else if (arr.length > 0) {
                                                lb.scenarioTypeId("1");
                                            } else {
                                                lb.scenarioTypeId(self.scenarioTypes()[0].id());
                                            }
                                        }
                                    }

                                    //tax equiv
                                    if (!lb.lookbackGroups()[0].overrideTaxEquivalent()) {
                                        var msTE = self.modelSetup().reportTaxEquivalent();
                                        lb.lookbackGroups()[0].taxEquivalent(msTE);
                                    }
                                }
                                i = 0;
                                var lbPromises = [];
                                for (i = 0; i < self.lbLookbacks().length; i++) {
                                    lbPromises.push(lookbackservice.createDataForLookbackGroup(ko.observable(self.lbLookbacks()[i]), true, null, {override: true, value: self.lbLookbacks()[i].lookbackGroups()[0].rateVol()}));
                                }
                                Q.all(lbPromises).then(function () {
                                    return globalContext.saveChangesQuiet().then(function () {

                                        return globalContext.getLookbackReportViewDataById(id, self.lbReport).then(function () {
                                            var dataObj = {};
                                            var lbViewData = self.lbReport()[0];
                                            var lbSectionsLen = lbViewData.sections.length;
                                            var lbgProperties = [];
                                            var rowClasses = ["total-row", "section-header", "sub", "master"];
                                            //this is a super hack but we can fix it later
                                            var netRowTotalTitles = ["Total Assets/Interest Income", "Total Interest Income", "Total Liab & Equity/Int Exp", "Total Interest Expense", "Total Assets/Net Interest Income", "Net Interest Income"];
                                            var thisSection;
                                            var sectionInfo = {};
                                            var curLbgId;
                                            var curGroup;
                                            var s = 0, i = 0;

                                            //order the lookback sections
                                            lbViewData.lbReport.sections.sort(function (a, b) { return a.priority - b.priority; });
                                            //get some overal info about each report section
                                            //The first lookback in the section will dictate some stuff like rate/vol
                                            for (s = 0; s < lbViewData.lbReport.sections.length; s++) {
                                                thisSection = lbViewData.lbReport.sections[s];
                                                curLbgId = lbViewData.sections[s][0][0].lBLookbackGroupId;
                                                curGroup = lbViewData.lbgData.filter(function (item, idx, arr) {
                                                    return item.groupId == curLbgId;
                                                })[0];
                                                if (curGroup != null && curGroup != undefined) {
                                                    sectionInfo[thisSection.name] = { rateVol: curGroup.rateVol };
                                                }
                                            }
                                            

                                            for (var r in lbViewData.lbgData) {
                                                var thisData = lbViewData.lbgData[r];
                                                lbgProperties.push({
                                                    rateVol: thisData.rateVol,
                                                    dates: moment(thisData.startDate).format('MM/DD/YYYY') + ' - ' + moment(thisData.endDate).format('MM/DD/YYYY')
                                                });
                                                var n = 0;
                                            }
                                            var lbgCount = 0;
                                            for (var section = 0; section < lbSectionsLen; section++) {
                                                 thisSection = lbViewData.sections[section];
                                                var sectionName = lbViewData.lbReport.sections[section].name;
                                                
                                                if (dataObj[sectionName] == undefined) {
                                                    dataObj[sectionName] = {};
                                                }

                                                for (var lookbackGroup in thisSection) {
                                                    var thisLBG = thisSection[lookbackGroup];
                                                    var dateLabel = lbgProperties[lbgCount].dates;
                                                    var groupId = lbViewData.lbgData[lbgCount].groupId;
                                                    for (var dRow in thisLBG) {
                                                        var thisRow = thisLBG[dRow];
                                                        if (dataObj[sectionName][thisRow.title] == undefined) {
                                                            dataObj[sectionName][thisRow.title] = {};
                                                        }
                                                        //if (dataObj[sectionName][thisRow.title][dateLabel] == undefined) {
                                                        //    dataObj[sectionName][thisRow.title][dateLabel] = { rateVol: lbgProperties[lbgCount].rateVol };
                                                        //}
                                                        if (dataObj[sectionName][thisRow.title][groupId] == undefined) {
                                                            dataObj[sectionName][thisRow.title][groupId] = { rateVol: lbgProperties[lbgCount].rateVol };
                                                        }

                                                        //now add each data field / metadata field for the date stamp
                                                        for (var d in thisRow) {
                                                            dataObj[sectionName][thisRow.title][groupId][d] = thisRow[d];
                                                        }
                                                    }
                                                    var n = 0;

                                                    lbgCount++;
                                                }
                                            }

                                            
                                            lbgCount = 0;
                                            var sectionLbgCount = 0;
                                            var classes = ["blueGreen", "darkGreen","yellow"];
                                                var sectionCount = 0;
                                                var rateVol;
                                            for (var sectionName in dataObj) {
                                                for (var key in dataObj[sectionName]) {
                                                    firstKey = key;
                                                    break;
                                                }
                                                sectionLbgCount = 0;
                                                rateVol = sectionInfo[sectionName].rateVol;

                                                //we only show 2 or 4 lookbacks depending on rateVol so cap the calculation there
                                                var numLookbacks = Object.keys(dataObj[sectionName][firstKey]).length;
                                                if (rateVol && numLookbacks > 2) {
                                                    numLookbacks = 2;
                                                } else if (!rateVol && numLookbacks > 4) {
                                                    numLookbacks = 4;
                                                }
                                                var tableClassNum = numLookbacks * (rateVol ? 10 : 4);
                                                var curClass = classes[sectionCount % classes.length];
                                                //we need a whole new table for each section
                                                if (sectionCount > 0) {
                                                    self.fullTable += '<div class="section-divider"></div>';
                                                }
                                                self.fullTable += '<div class="section-wrapper '+ curClass +' wrapper-'+ tableClassNum +'">';
                                                self.fullTable += '<h4 class="atlasHeader '+ curClass +'">'+ sectionName +'</h1>';
                                                self.fullTable += '<table class="dcg-table lb-table-'+ tableClassNum +'">';
                                                self.fullTable += '<thead>';
                                                self.fullTable += '<tr class="head-row-1"><th></th>';
                                                var secondRow = '<tr class="head-row-2"><td></td>';
                                                var thirdRow = '<tr class="head-row-3"><td></td>';
                                                var firstKey;
                                                

                                                //create the head row
                                                for (var lbg in dataObj[sectionName][firstKey]) {
                                                    var rv = dataObj[sectionName].rateVol;
                                                    var colspan = rateVol ? 10:4;

                                                    if (sectionLbgCount > 0) {
                                                        self.fullTable += '<th class="spacer-cell"></th>';
                                                        secondRow += '<td class="spacer-cell"></td>';
                                                        thirdRow += '<td class="spacer-cell"></td>';
                                                    }

                                                    //limit the columns if rateVol
                                                    if ((rateVol && sectionLbgCount < 2) || (!rateVol && sectionLbgCount < 4)) {
                                                        if (!rateVol) {
                                                            self.fullTable += '<th colspan="' + colspan + '" class="lb-date-header">' + lbgProperties[lbgCount].dates + '</th>';
                                                            secondRow += '<td colspan="2"></td><td colspan="2" style="text-align: right; padding-right: 16px;">Variance</td>';
                                                            thirdRow += '<td>Model</td><td>Actuals</td><td>$</td><td>%</td>';
                                                            //max width was 60px;
                                                            
                                                        } else {
                                                            self.fullTable += '<th colspan="' + colspan + '" class="lb-date-header"><span>' + lbgProperties[lbgCount].dates + '</span></th>';
                                                            secondRow += '<td colspan="3" class="rate-vol-head light-red"><span class="light-red">MODEL</span></td>';
                                                            secondRow += '<td colspan="3" class="rate-vol-head brown"><span class="brown">ACTUALS</span></td>';
                                                            secondRow += '<td colspan="4" class="rate-vol-head black"><span class="black">Variance</span></td>';
                                                            thirdRow += '<td>Avg Bal</td><td>Avg Rate</td><td>Inc/Exp</td>';
                                                            thirdRow += '<td>Avg Bal</td><td>Avg Rate</td><td>Inc/Exp</td>';

                                                            //variance cols
                                                            thirdRow += '<td>Rate</td><td>Volume</td><td>Total</td><td>%</td>';
                                                        }
                                                        lbgCount++;
                                                    }
                                                    sectionLbgCount++;
                                                }
                                                if (tableClassNum != 16 && tableClassNum != 20) {
                                                    secondRow += '<td class="filler-cell"></td>';
                                                    thirdRow += '<td class="filler-cell"></td>';
                                                    self.fullTable += '<th class="filler-cell"></td>';
                                                }
                                                secondRow += '</tr>';
                                                thirdRow += '</tr>';
                                                self.fullTable += '</tr>';
                                                self.fullTable += secondRow;
                                                self.fullTable += thirdRow;
                                                self.fullTable += '</thead>';
                                                self.fullTable += '<tbody>';
                                                var rowCount = 0;
                                                //put the body rows in
                                                for (var row in dataObj[sectionName]) {
                                                    var thisRow = '<tr class="[REPLACE_W_ROWCLASS]"><td><span>' + row + '</span></td>';
                                                    var lbgCounter = 0;
                                                    //put each lookback group data in the row
                                                    //for (var lbgId in dataObj[sectionName][row]) {
                                                    for (var groupIdx in lbViewData.lbgData) {
                                                        var lbgId = lbViewData.lbgData[groupIdx].groupId;
                                                        var theseProps = dataObj[sectionName][row][lbgId];
                                                        var thisRowClass = rowClasses[theseProps.rowType + 1];
                                                        if (theseProps.rowType == -1 && netRowTotalTitles.indexOf(row) == -1) {
                                                            thisRowClass = "";
                                                        }
                                                        //give the row class
                                                        thisRow = thisRow.replace('class="[REPLACE_W_ROWCLASS]"', 'class="' + thisRowClass + (rowCount == (Object.keys(dataObj[sectionName]).length - 1) ? " last-row" : "")+ '"');

                                                        //limit the number of groups
                                                        if ((rateVol && lbgCounter < 2) || (!rateVol && lbgCounter < 4)) {
                                                            if (theseProps.rowType != 0) {
                                                                if (lbgCounter > 0) {
                                                                    thisRow += '<td class="spacer-cell"></td>';
                                                                }
                                                                if (!rateVol) {
                                                                    thisRow += '<td>' + formatBal(theseProps.proj_IncExp); +'</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.actual_IncExp) + '</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.variance_Val) + '</td>';
                                                                    thisRow += '<td>' + numeral(theseProps.variance_Perc).format('0.00') + '%</td>';
                                                                } else {
                                                                    thisRow += '<td>' + formatBal(theseProps.proj_AvgBal) + '</td>';
                                                                    thisRow += '<td>' + numeral(theseProps.proj_AvgRate).format('0.00') + '%</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.proj_IncExp) + '</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.actual_AvgBal) + '</td>';
                                                                    thisRow += '<td>' + numeral(theseProps.actual_AvgRate).format('0.00') + '%</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.actual_IncExp) + '</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.variance_ValDueToRate) + '</td>'
                                                                    thisRow += '<td>' + formatBal(theseProps.variance_ValDueToVol) + '</td>';
                                                                    thisRow += '<td>' + formatBal(theseProps.variance_Val) + '</td>';
                                                                    thisRow += '<td>' + numeral(theseProps.variance_Perc).format('0.00') + '%</td>';
                                                                }

                                                            } else {
                                                                thisRow += '<td colspan="' + colspan + '"></td>';
                                                            }
                                                        }
                                                        lbgCounter++;
                                                    }
                                                    self.fullTable += thisRow

                                                    self.fullTable += '</tr>';
                                                    rowCount++;
                                                }
                                                self.fullTable += '</tbody>';
                                                self.fullTable += '</table>';
                                                self.fullTable += '</div>';
                                                sectionCount++;
                                            }

                                            $('#lookback-report-view').append(self.fullTable);
                                            global.loadingReport(false);
                                            $('#reportLoaded').text('1');
                                            if (typeof hiqPdfInfo == "undefined") {
                                                globalContext.logEvent("Viewed", curPage); 
                                            }
                                            else {
                                                hiqPdfConverter.startConversion();
                                            }
                                        });
                                    });
                                });
                                
                            });
                        });
                    });
                });
            });
            
        //end of activate               
        };
    };

    return vm;

});
