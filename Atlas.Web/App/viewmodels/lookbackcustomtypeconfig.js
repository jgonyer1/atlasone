﻿define([
    'services/globalcontext',
    'plugins/router',
    'durandal/system',
    'durandal/app',
    'services/logger',
    'services/model'
],
    function (globalContext, router, system, app, logger, model) {
        var vm = function () {
            var self = this;
            var curPage = "LookbackCustomTypeConfig";
            this.type = ko.observable();
            this.isSaving = ko.observable(false);

            //Navigation Helpers
            this.hasChanges = ko.pureComputed(function () {
                return globalContext.hasChanges();
            });

            this.canSave = ko.pureComputed(function () {
                return self.hasChanges() && !self.isSaving();
            });

            this.canDeactivate = function () {
                if (self.hasChanges()) {
                    var msg = 'Do you want to leave and cancel?';
                    return app.showMessage(msg, 'Navigate Away', ['No', 'Yes'])
                        .then(function (selectedOption) {
                            if (selectedOption === 'Yes') {
                                self.cancel();
                            }
                            return selectedOption;
                        });
                }
                return true;
            };
            //Navigation
            this.goBack = function () {
                router.navigate("#/lookbackadminconfig");
            };

            this.cancel = function () {
                self.isSaving(true);
                globalContext.cancelChanges();
                self.isSaving(false);
            };

            this.save = function () {
                self.isSaving(true);
                return globalContext.saveChanges().fin(complete);

                function complete() {
                    self.isSaving(false);
                }
            };
            this.canSave = ko.pureComputed(function () {
                return self.hasChanges() && !self.isSaving();
            });

            this.activate = function (id) {
                globalContext.logEvent("Viewed", curPage); 
                return globalContext.getLBCustomTypeById(id, self.type).then(function () {

                });
            }
            
        };    

        
   
        return vm;
    });
