﻿define([
    'services/globalcontext',
    'services/global',
    'plugins/router',
    'durandal/app',
    'viewmodels/navigateawaymodal',
    'services/basicsurplusservice',
],
function (globalContext, global, router, app, naModal, bService) {
    var vm = function () {
        var self = this;
        var curPage = "BasicSurplusAdmin";
        this.bService = bService;

        //Data Handling
        this.activate = function (id) {
            globalContext.logEvent("Viewed", curPage);
            //Simply call the bservice activate function
            return bService.activate(id, "");
        };

        this.compositionComplete = function () {
            router.restoreNavigationParams();
            bService.compositionComplete();
        };

        this.save = function (data, event, quiet) {
            self.bService.save(data, event, quiet);
        };

        this.goToDetail = function (type) {
            router.setNavigationParams({ tabs: ['settings', 'liabSec'] });
            bService.goToDetail(type);
        };

        this.canDeactivate = function () {
            if (! self.bService.hasChanges()) return true;

            return naModal.show(
                function () { self.bService.cancel(); },
                function () { self.bService.save(); }
            );
        };

        //Disposal
        this.deactivate = function (isClose) {
            bService.deactivate(isClose);
        };

        return this;
    };

    return vm;
});
