﻿define(function (require) {
    var globalContext = require('services/globalcontext');
    var global = require('services/global');
    var ivpVm = function (id) {
        var self = this;
        var curPage = "InvestmentDetailView";
        this.ivpViewData = ko.observable();

        this.compositionComplete = function () {
          
            var tbl = self.ivpViewData().tblData;
            if (self.ivpViewData().errors.length > 0 || self.ivpViewData().warnings.length > 0) {
                globalContext.reportErrors(self.ivpViewData().errors, self.ivpViewData().warnings);
            }
           

           for (var i = 0; i < tbl.length; i++) {
               var rec = tbl[i];
               var row = '<tr>';
               row += '<td>' + rec.cusip + '</td>';
               row += '<td>' + numeral(rec.parValue).format('0,0') + '</td>';
               row += '<td>' + numeral(rec.bookValue).format('0,0') + '</td>';
               row += '<td>' + numeral(rec.bookPrice).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.marketPrice).format('0.00') + '</td>';
               row += '<td>' + rec.basisintent + '</td>';
               row += '<td>' + numeral(rec.yield).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.currentCoupon).format('0.00') + '</td>';
               row += '<td>' + rec.assetClassSubCode + '</td>';
               row += '<td>' + rec.basisCallFlag + '</td>';
               row += '<td>' + rec.taxable + '</td>';


               if (rec.collateralType != null) {
                   row += '<td>' + rec.collateralType + '</td>';
               }
               else {
                   row += '<td>-</td>';
               }

               if (rec.loanAge != null) {
                   row += '<td>' + rec.loanAge + '</td>';
               }
               else {
                   row += '<td>-</td>';
               }

              
               row += '<td>' + moment(rec.maturityDate).format('MM/DD/YYYY') + '</td>';
               row += '<td>' + numeral(rec.originalTerm).format() + '</td>';

               if (rec.originationDate != null) {
                   row += '<td>' + moment(rec.originationDate).format('MM/DD/YYYY') + '</td>';
               }
               else {
                   row += '<td>-</td>';
               }
               
               row += '<td>' + numeral(rec.paymentFreq).format() + '</td>';
               row += '<td>' + numeral(rec.pPHistCPR1Mo).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.pPHistCPR3Mo).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.pPHistCPRLife).format('0.00') + '</td>';
               row += '<td>' + rec.securitySubType + '</td>';
               row += '<td>' + numeral(rec.wAC).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.wAM).format() + '</td>';

               row += '<td>' + rec.couponType + '</td>';
               row += '<td>' + rec.masterIndex + '</td>';
               row += '<td>' + numeral(rec.mastermargin).format('0.00') + '</td>';

               if (rec.masterDate != null) {
                   row += '<td>' + moment(rec.masterDate).format('MM/DD/YYYY') + '</td>';
               }
               else {
                   row += '<td>-</td>';
               }
             
               row += '<td>' + numeral(rec.masterResetFreq).format('0') + '</td>';
               row += '<td>' + numeral(rec.masterPerCap).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.masterLifeCap).format('0.00') + '</td>';
               row += '<td>' + numeral(rec.masterLifeFloor).format('0.00') + '</td>';

               row += '</tr>';

               $('#invBody').append(row);
           }

           

           
           global.loadingReport(false);
           $('#reportLoaded').text('1');
            //Export To Pdf
            if (typeof hiqPdfInfo == "undefined") {
                globalContext.logEvent("Viewed", curPage); 
            }
            else {
                $('#report-view').parent().height($('#report-view').parent().height());
                $('#report-view').height($('#report-view').height());
                var h = globalContext.getMultiPageHeight($('.report-view').height(), $('#footnoteContainer').height());
                $('.report-view').height(h-700);
                $('#footnoteContainer').height($('#footnoteContainer').height());
                hiqPdfConverter.startConversion();
            }



        }



        this.activate = function () {
            global.loadingReport(true);
            return globalContext.getInvestmentDetailViewDataById(id, self.ivpViewData).then(function () {

            });
        };
    };

    return ivpVm;

});
