﻿define(['services/globalcontext',
        'plugins/router',
        'durandal/system',
        'durandal/app',
        'services/logger',
        'services/model'
],
function (globalContext, router, system, app, logger, model) {
    var definedGroupings = ko.observable();
    var curPage = "DefinedGroupings";
    function activate(id) {
        globalContext.logEvent("Viewed", curPage);  
        //Load All Of The Defined Groupings
        return globalContext.getDefinedGroupings(definedGroupings).then(function () {});
    }

    var goAddDefinedGrouping = function () {
        router.navigate('#/definedgroupingsadd');
    };

    var goToDefinedGroupingEdit = function () {
        router.navigate('#/definedgroupingsconfig/' + this.id());
    };
            
    var deleteDefinedGrouping = function (dg) {
        var responses = ["OK", "Cancel"];
        app.showMessage("Do you want to delete defined grouping " + dg.name() + "?", "Alert", responses).then(function (r) {
            if (r == responses[0]) {
                _deleteDefinedGrouping(dg);
            } else {
                return false;
            }
        });
    };

    var _deleteDefinedGrouping = function (dg) {
        //Did this server side because of FK constraints
        globalContext.deleteDefinedGrouping(dg.id()).fin(function (){
            globalContext.getDefinedGroupings(definedGroupings);
        });
    }

    var vm = {
        definedGroupings: definedGroupings,
        goAddDefinedGrouping: goAddDefinedGrouping,
        goToDefinedGroupingEdit: goToDefinedGroupingEdit,
        deleteDefinedGrouping: deleteDefinedGrouping,
        activate: activate
    };

    return vm;
});
