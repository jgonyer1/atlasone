﻿define([
    'services/globalcontext',
    'services/global'
],
function (globalContext, global) {
    var ascVm = function (asc) {
        var self = this;

        this.asc = asc;

        this.cancel = function () {
        };

        this.institutions = ko.observableArray();

        this.global = global;
        this.asOfDateOffsets = ko.observableArray();

        this.activate = function (id) {
            globalContext.logEvent("Viewed", "AssumptionMethodsConfig");
            global.loadingReport(true);
            return globalContext.getAvailableBanks(self.institutions).then(function () {
                //AsOfDateOffset Handling (Single-Institution Template)
                self.instSub = self.asc().institutionDatabaseName.subscribe(function (newValue) {
                    globalContext.getAsOfDateOffsets(self.asOfDateOffsets, newValue);
                });
                return globalContext.getAsOfDateOffsets(self.asOfDateOffsets, self.asc().institutionDatabaseName()).then(function () {
                    global.loadingReport(false);
                });
            });
        };

        this.detached = function (view, parent) {
            //AsOfDateOffset Handling (Single-Institution Template)
            self.instSub.dispose();
        };

        return this;
    };

    return ascVm;
});
