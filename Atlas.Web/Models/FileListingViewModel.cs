﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HtmlEditor.Models
{
    public class FileListingViewModel
    {
        public List<Atlas.Institution.Model.File> Files { get; set; }
        public string CKEditorFuncNum { get; set; }
    }
}
