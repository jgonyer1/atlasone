﻿using Newtonsoft.Json;

namespace Atlas.Web.Models
{
    public class PDFExportStatus
    {
        [JsonConstructor]
        private PDFExportStatus() { }

        public PDFExportStatus(bool isSuccess, string reportName, string message = null, int? reportIndex = 0)
        {
            IsSuccess = isSuccess;
            ReportName = reportName;
            Message = message;
            ReportIndex = reportIndex;
        }

        [JsonProperty]
        public bool IsSuccess { get; private set; }

        [JsonProperty]
        public string ReportName { get; private set; }

        [JsonProperty]
        public string Message { get; private set; }

        [JsonProperty]
        public int? ReportIndex { get; private set; }

        [JsonProperty]
        public int TotalReportCount { get; set; }

        [JsonProperty]
        public bool IsExportFatal { get; set; }

        [JsonProperty]
        public bool IsExportComplete { get; set; }
    }
}