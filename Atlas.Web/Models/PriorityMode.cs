﻿namespace Atlas.Web.Models
{
    public enum PriorityMode
    {
        Equal,
        First,
        Last,
        Previous,
        Next
    }
}