﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atlas.Institution.Model;

namespace Atlas.Web.ViewModels
{

    public class NetIncome
    {
        public ChartScenarioType ChartScenarioType { get; set; }
        public Simulation Simulation { get; set; }
        public Scenario Scenario { get; set; }
        public DateTime Month { get; set; }
        public double Value { get; set; }
    }

    public class SeriesResult
    {
        public double Min { get; set; }
        public double Max { get; set; }
        public List<object> SeriesJsonObjects { get; set; }
        public List<string> TableHeaders { get; set; }
        public List<List<double>> TableRows { get; set; }
    }

    public class Row
    {
        public string Name { get; set; }
        public List<string> Values { get; set; }
    }

    public class TableDataPoint
    {
        public ChartScenarioType ChartScenarioType { get; set; }
        public int Year { get; set; }
        public double Value { get; set; }
    }

}