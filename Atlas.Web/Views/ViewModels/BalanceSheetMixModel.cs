﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atlas.Institution.Model;

namespace Atlas.Web.ViewModels
{

    public class BalanceSheetMixData
    {
        public string TierName { get; set; }
        public Boolean IsMasterTier { get; set; }
        public double CurrentPortfolio { get; set; }
        public double PriorPortfolio { get; set; }
        public double ChangeInPortfolio { get; set; }

        public BalanceSheetMixData(string tn, Boolean isMaster){
            TierName = tn;
            IsMasterTier = isMaster;
        }
    }
}