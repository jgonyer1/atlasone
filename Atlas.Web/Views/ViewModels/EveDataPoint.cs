﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Atlas.Institution.Model;

namespace Atlas.Web.ViewModels
{
    public class EveDataPoint
    {
        public string InstitutionDbName { get; set; }
        public int BalanceSheetGroupId { get; set; }
        public BalanceSheetGroup BalanceSheetGroup { get; set; }
        public int ColumnId { get; set; }
        public double Value { get; set; }
    }
}