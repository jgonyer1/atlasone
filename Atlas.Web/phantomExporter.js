﻿

var page = require('webpage').create();

var fs = require('fs');

var dbName = '{{dbName}}';
var packageId = '{{packageId}}';
var firstReportInPackageURL = '{{firstURL}}';

page.viewportSize = {
    width: 9000,
    height: 1161
};

page.paperSize = {
    height: 794,
    width: 1000,
    orientation: 'landscape',
    margin: { top: 30, right: 0, bottom: 30, left: 0 },
    padding: { top: 0, right: 0, bottom: 0, left: 0 }
};

// This will fix some things that I'll talk about in a second
//page.settings.dpi = "72";

//------------------------------------
//	Read Cookies From File and adds them to phantom browser
//------------------------------------	
var cookieJson = JSON.parse(fs.read('{{folderPath}}cookies_.txt'));

for (var i = 0; i < cookieJson.length; i++) {
    var tempCookie = {};

    tempCookie["Name"] = cookieJson[i]["Name"];
    tempCookie["Domain"] = cookieJson[i]["Domain"];
    tempCookie["Value"] = cookieJson[i]["Value"];
    tempCookie["Path"] = cookieJson[i]["Path"];
    tempCookie["Secure"] = cookieJson[i]["Secure"];
    tempCookie["Shareable"] = cookieJson[i]["Shareable"];
    tempCookie["HasKeys"] = cookieJson[i]["HasKeys"];
    tempCookie["Expires"] = cookieJson[i]["Expires"];
    // Here we are adding the relevant values as needed and in the proper format

    var tempADD = {
        "domain": tempCookie["Domain"],
        "expires": tempCookie["Expires"],
        "httponly": tempCookie["HttpOnly"],
        "name": tempCookie["Name"],
        "path": tempCookie["Path"],
        "secure": tempCookie["Secure"],
        "value": tempCookie["Value"],
        "shareable": tempCookie["Shareable"],
        "domain": tempCookie["Domain"],
        "hasKeys": tempCookie["HasKeys"]
    };


    // Finally, we add the cookie. phantom.addCookie returns true if successful
    phantom.addCookie(tempADD);
}

var ph = phantom;

console.log('Loaded Cookies');
//-----------------------------------------------
//	Opens page we want and then includes jquery so we can refrence the dom in page.evaluate
//--------------------------------------------
page.open(firstReportInPackageURL, function (status) {
    //page.includeJs("https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js", function() {
    if (status == 'success') {
        var script1 = "function(){ window.hiqPdfInfo = 'FYU'; hiqPdfConverter = { startConversion: function() { var doShit = 1;}}}";
        page.evaluateJavaScript(script1);
        //Keep checking condition once true it continues
        function waitFor(testFx, onReady, timeOutMillis) {
            var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 999999, //< Default Max Timout is 3s
                start = new Date().getTime(),
                condition = false,
                interval = setInterval(function () {
                    //console.log(new Date().getTime() - start);
                    //console.log(maxtimeOutMillis)
                    if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                        // If not time-out yet and condition not yet fulfilled
                        condition = (typeof (testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
                    } else {
                        if (!condition) {
                            // If condition still not fulfilled (timeout but condition is 'false')
                            console.log("'waitFor()' timeout");
                            console.log('Time out');
                            //phantom.exit(1);
                        } else {
                            // Condition fulfilled (timeout and/or condition is 'true')
                            console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                            typeof (onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                            clearInterval(interval); //< Stop this interval
                        }
                    }
                }, 250); //< repeat check every 250ms						
        };

        function continueFor(reportCounter, callBack) {
            waitFor(function () {
                // Check in the page if a specific element is now visible
                return page.evaluate(
                    function () {
                        $('html').css('transform', 'scaleX(1.1) scaleY(.96)');
                        if ($('#reportLoaded').text() == '1') {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                );
            },
                function () {


                    page.render('{{folderPath}}' + reportCounter + '.pdf', { format: 'pdf' });

                    var cont = page.evaluate(
                        function () {
                            if (!$('#nextReport').is(':disabled')) {
                                //reset report loaded and go to next report
                                $('#reportLoaded').text('0');
                                $('#nextReport').click();
                                return true;
                            }
                            else {
                                return false
                            }
                        }
                    );

                    if (cont) {
                        reportCounter += 1;
                        return continueFor(reportCounter, callBack);
                    }
                    else {
                        callBack();
                        return true;
                    }
                }

                , 999999);
        }

        //pass in call back so wehn all done we can just exit program
        continueFor(0, function () {
            phantom.exit();
        });
        
    } 
  
});
