var gulp = require('gulp');
var durandal = require('gulp-durandal');

gulp.task('build', function () {
    durandal({
        baseDir: 'Atlas.Web/App',
        main: 'main.js',
        output: 'main-built.js',
        almond: true,
        minify: true,
        rjsConfigAdapter: function (rjsConfig) {
            rjsConfig.deps = ['text'];
            return rjsConfig;
        }
    })
    .pipe(gulp.dest('Atlas.Web/App'));
});

